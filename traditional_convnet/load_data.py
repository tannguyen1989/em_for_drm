import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import gzip
import cPickle

import numpy as np

import random

import time

import theano

import copy

import os
import pickle


# from guppy import hpy; h=hpy()

def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

class DATA(object):
    """
    Class of DATA. Should be used to load data

    """
    def __init__(self, dataset_name, data_mode, data_dir, Nlabeled, Ni, Cin, H, W, seed, preprocess=False):
        self.dataset_name = dataset_name
        self.data_mode = data_mode
        self.data_dir = data_dir
        self.Cin = Cin
        self.Nlabeled = Nlabeled
        self.Ni= Ni
        self.H = H
        self.W = W
        self.seed = seed
        self.preprocess = preprocess

        random.seed(self.seed)
        np.random.seed(self.seed)

        if self.dataset_name == 'MNIST':
            self.load_mnist()

        elif self.dataset_name == 'CIFAR10':
            self.load_cifar10(preprocess=self.preprocess)

        else:
            self.load_general_data()

    def load_general_data(self):
        self.dtrain = []
        self.dtest = []
        self.dvalid = []
        self.train_label = []
        self.test_label = []
        self.valid_label = []

    def load_mnist(self):
        f = gzip.open(self.data_dir, 'rb')
        train_set, valid_set, test_set = cPickle.load(f)
        f.close()
        self.dtrain = (train_set[0])[0:self.Ni]
        self.train_label = (train_set[1])[0:self.Ni]

        self.dtest = test_set[0]
        Ntest = np.shape(self.dtest)[0]
        self.test_label = test_set[1]

        self.dvalid = valid_set[0]
        Nvalid = np.shape(self.dvalid)[0]
        self.valid_label = valid_set[1]

        if not self.data_mode == 'all':
            if self.data_mode == 'semisupervised':
                print 'Creating a semisupervised dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)

                train_label_semisupervised = np.ones_like(self.train_label) * (np.max(self.train_label)+1)
                train_label_semisupervised[train_digit_indx] = self.train_label[train_digit_indx]
                self.train_label = train_label_semisupervised
            else:
                print 'Creating a small dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)
                print train_digit_indx[0:20]
                self.dtrain = self.dtrain[train_digit_indx]
                self.train_label = self.train_label[train_digit_indx]

        if self.data_mode == 'small':
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Nlabeled, self.Cin, self.H, self.W))
        else:
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Ni, self.Cin, self.H, self.W))

        self.dtrain = np.float32(self.dtrain)
        self.train_label = np.int32(self.train_label)

        self.dtest = np.reshape(self.dtest, newshape=(Ntest, self.Cin, self.H, self.W))
        self.dtest = np.float32(self.dtest)
        self.test_label = np.int32(self.test_label)

        self.dvalid = np.reshape(self.dvalid, newshape=(Nvalid, self.Cin, self.H, self.W))
        self.dvalid = np.float32(self.dvalid)
        self.valid_label = np.int32(self.valid_label)

        print 'Shape of training dataset'
        print np.shape(self.dtrain)
        print 'Shape of training labels'
        print np.shape(self.train_label)
        print 'Shape of test dataset'
        print np.shape(self.dtest)
        print 'Shape of test labels'
        print np.shape(self.test_label)
        print 'Shape of validation dataset'
        print np.shape(self.dvalid)
        print 'Shape of validation labels'
        print np.shape(self.valid_label)

    def load_cifar10(self, preprocess=False):
        if preprocess==False:
            print('No Preprocessing on CIFAR10')
            train_set_1 = unpickle(self.data_dir + "/data_batch_1")
            train_set_2 = unpickle(self.data_dir + "/data_batch_2")
            train_set_3 = unpickle(self.data_dir + "/data_batch_3")
            train_set_4 = unpickle(self.data_dir + "/data_batch_4")
            train_set_5 = unpickle(self.data_dir + "/data_batch_5")
            test_set = unpickle(self.data_dir + "/test_batch")
            train_set = train_set_1
            train_set['data'] = np.concatenate((train_set['data'],train_set_2['data'],train_set_3['data'],train_set_4['data'], train_set_5['data']),axis=0)
            train_set['labels'] = np.concatenate((train_set['labels'],train_set_2['labels'],train_set_3['labels'],train_set_4['labels'], train_set_5['labels']),axis=0)

            self.dtrain = train_set['data']
            self.train_label = train_set['labels']
            self.dtest = test_set['data']
            self.test_label = test_set['labels']
            self.dvalid = train_set_5['data']
            self.valid_label = train_set_5['labels']
        else:
            print('Preprocessing on CIFAR10')
            train_set = np.load(os.path.join(self.data_dir, "pylearn2_gcn_whitened", "train.npy"))
            test_set = np.load(os.path.join(self.data_dir, "pylearn2_gcn_whitened", "test.npy"))

            train_set_1_temp = unpickle(self.data_dir + "/data_batch_1")
            train_set_2_temp = unpickle(self.data_dir + "/data_batch_2")
            train_set_3_temp = unpickle(self.data_dir + "/data_batch_3")
            train_set_4_temp = unpickle(self.data_dir + "/data_batch_4")
            train_set_5_temp = unpickle(self.data_dir + "/data_batch_5")
            test_set_temp = unpickle(self.data_dir + "/test_batch")
            train_set_labels = np.concatenate((train_set_1_temp['labels'],train_set_2_temp['labels'],train_set_3_temp['labels'],train_set_4_temp['labels'], train_set_5_temp['labels']),axis=0)
            test_set_labels = test_set_temp['labels']

            self.dtrain = train_set
            self.train_label = train_set_labels
            self.dtest = test_set
            self.test_label = test_set_labels
            self.dvalid = train_set[40000:50000]
            self.valid_label = train_set_labels[40000:50000]

        Ntest = np.shape(self.dtest)[0]
        Nvalid = np.shape(self.dvalid)[0]

        if not self.data_mode == 'all':
            if self.data_mode == 'semisupervised':
                print 'Creating a semisupervised dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)

                train_label_semisupervised = np.ones_like(self.train_label) * (np.max(self.train_label)+1)
                train_label_semisupervised[train_digit_indx] = self.train_label[train_digit_indx]
                self.train_label = train_label_semisupervised
            else:
                print 'Creating a small dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)
                print train_digit_indx[0:20]
                self.dtrain = self.dtrain[train_digit_indx]
                self.train_label = self.train_label[train_digit_indx]

        if self.data_mode == 'small':
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Nlabeled, self.Cin, self.H, self.W))
        else:
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Ni, self.Cin, self.H, self.W))

        self.dtrain = np.float32(self.dtrain)
        self.train_label = np.int32(self.train_label)

        self.dtest = np.reshape(self.dtest, newshape=(Ntest, self.Cin, self.H, self.W))
        self.dtest = np.float32(self.dtest)
        self.test_label = np.int32(self.test_label)

        self.dvalid = np.reshape(self.dvalid, newshape=(Nvalid, self.Cin, self.H, self.W))
        self.dvalid = np.float32(self.dvalid)
        self.valid_label = np.int32(self.valid_label)

        print 'Shape of training dataset'
        print np.shape(self.dtrain)
        print 'Shape of training labels'
        print np.shape(self.train_label)
        print 'Shape of test dataset'
        print np.shape(self.dtest)
        print 'Shape of test labels'
        print np.shape(self.test_label)
        print 'Shape of validation dataset'
        print np.shape(self.dvalid)
        print 'Shape of validation labels'
        print np.shape(self.valid_label)