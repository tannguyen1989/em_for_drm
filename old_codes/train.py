__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import os
import timeit

import numpy as np

import theano
import theano.tensor as T

import copy
import cPickle

from scipy.linalg import norm
import time


# from guppy import hpy; h=hpy()

class train(object):
    """
    Training algorithm class

    """
    def __init__(self, batch_size, train_data, test_data, valid_data, train_label, test_label, valid_label, model, output_dir, param_dir='params'):
        '''

        :param data_mode:
        :param em_mode:
        :param update_mode:
        :param stop_mode:
        :param train_method:
        :param Ni:
        :param Cin:
        :param H:
        :param W:
        :param batch_size:
        :param seed:
        :param output_dir:
        :param data_dir:
        :return:
        '''
        self.batch_size = batch_size
        self.d = train_data
        self.dtest = test_data
        self.dvalid = valid_data
        self.label = train_label
        self.labeltest = test_label
        self.labelvalid = valid_label

        self.model = model
        self.output_dir = output_dir
        self.param_dir = os.path.join(self.output_dir, param_dir)
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        if not os.path.exists(self.param_dir):
            os.makedirs(self.param_dir)

    def train_Softmax(self, max_epochs, classification_dir):
        self.classification_dir = os.path.join(self.output_dir, classification_dir)
        if not os.path.exists(self.classification_dir):
            os.makedirs(self.classification_dir)
        N, Cin, h, w = np.shape(self.d)
        n_batches = N/self.batch_size
        N_valid, Cin_valid, h_valid, w_valid = np.shape(self.dvalid)
        n_valid_batches = N_valid/self.batch_size
        N_test, Cin_test, h_test, w_test = np.shape(self.dtest)
        n_test_batches = N_test/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_dat_test = theano.shared(self.dtest)
        shared_dat_valid = theano.shared(self.dvalid)
        shared_label = theano.shared(self.label)
        shared_label_test = theano.shared(self.labeltest)
        shared_label_valid = theano.shared(self.labelvalid)

        index = T.iscalar()

        train_model = theano.function([index], self.model.output_var,
                                          updates=self.model.updates, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                  self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                     givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                             self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

        validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})

        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)

        done_looping = False

        patience = 10000  # look as this many examples regardless
        patience_increase = 2  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.995  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_params = None
        best_validation_loss = np.inf
        test_score_vec = []
        iter = 0

        start_time = time.clock()

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1
            print 'Epoch %d' %epoch

            for minibatch_index in xrange(n_batches):
                # print minibatch_index
                iter = iter + 1
                output_val = train_model(minibatch_index)

                if (iter + 1) % validation_frequency == 0:
                    validation_losses = [validate_model(i) for i
                                     in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_batches, \
                           this_validation_loss * 100.))

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                        # save best validation score and iteration number
                        best_validation_loss = this_validation_loss
                        best_iter = iter
                        best_params = self.model.params

                        # test it on the test set
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)
                        print(('     epoch %i, minibatch %i/%i, test error of best '
                               'model %f %%') %
                              (epoch, minibatch_index + 1, n_batches,
                               test_score * 100.))

                if patience <= iter:
                    done_looping = True
                    break

            test_losses = [test_model(i) for i in xrange(n_test_batches)]
            test_score_epoch = np.mean(test_losses)
            test_score_vec.append(test_score_epoch)
            epoch_vec.append(epoch)
            fig = figure()
            plot(epoch_vec[1:], test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.classification_dir, 'Error-vs-Epoch-EG.png'))
            close(fig)
            plot_classification_file = os.path.join(self.classification_dir, 'plot_classification.npz')
            np.savez(plot_classification_file, epoch_vec=epoch_vec, test_score_vec=test_score_vec)

            param_file = os.path.join(self.classification_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, W=output_val[0], b=output_val[1])

        end_time = time.clock()
        print('Optimization complete.')
        print('Best validation score of %f %% obtained at iteration %i,'\
              'with test performance %f %%' %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))
        print >> sys.stderr, ('The code for file ' +
                              os.path.split(__file__)[1] +
                              ' ran for %.2fm' % ((end_time - start_time) / 60.))

        save_file = open(os.path.join(self.output_dir, 'cnn_mean_removed.pkl'), 'wb')  # this will overwrite current content
        cPickle.dump(best_params, save_file, -1)
        save_file.close()
        fig = figure()
        plot(epoch_vec[1:], test_score_vec)
        legend()
        xlabel('Epoch')
        ylabel('Error Rate')
        title('Error-vs-Epoch-EG')
        fig.savefig(os.path.join(self.classification_dir, 'Error-vs-Epoch-EG.png'))
        close(fig)

    def train_unsupervised(self, max_epochs, verbose, tol, stop_mode):
        '''
        EM learning algorithm
        :return:
        '''
        print 'start EM training'

        N, Cin, H, W = np.shape(self.d)
        n_batches = N/self.batch_size

        shared_dat = theano.shared(self.d)

        num_layer = len(self.model.layers)

        dPS_vec = []
        dLC_vec = []
        dMU_vec = []
        NL = None
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)
        iter = 0

        for i in xrange(num_layer):
            ## Concatenate differences in parameter values
            dPS_vec.append([])
            dLC_vec.append([])
            dMU_vec.append([])

        index = T.iscalar()

        do_one_EM = theano.function([index], self.model.output_var,
                                          updates=self.model.updates, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        logLs_output = []
        for layer in self.model.layers:
            logLs_output.append(layer.logLs)

        compute_logLs = theano.function([index], logLs_output,
                                          updates=[], on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        # debug = theano.function([index], self.model.conv1.amps_new,
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        start_time = timeit.default_timer()

        newNL = 0
        for minibatch_index in xrange(n_batches):
            logLs_output_val = compute_logLs(minibatch_index)
            for i in xrange(num_layer):
                num_pixel = self.model.layers[i].w * self.model.layers[i].h *self.model.layers[i].Cin
                newNL = newNL + np.sum(logLs_output_val[i])/(np.size(logLs_output_val[i])*num_pixel)

        newNL = -newNL/n_batches
        NegLogLs_vec.append(newNL)
        print("Initial NLL=", newNL)

        done_looping = False

        while (epoch < max_epochs) and (not done_looping):

            epoch = epoch + 1
            epoch_vec.append(epoch)
            print 'Epoch %d' %epoch

            LC = []
            PS = []
            MU = []
            newLC = []
            newPS = []
            newMU = []

            for layer in self.model.layers:
                LC.append(copy.copy(layer.lambda_covs.get_value()))
                PS.append(copy.copy(layer.psis.get_value()))
                MU.append(copy.copy(layer.means.get_value()))

            for minibatch_index in xrange(n_batches):
                print minibatch_index
                iter = iter + 1
                output_val = do_one_EM(minibatch_index)

            newNL = 0
            for minibatch_index in xrange(n_batches):
                logLs_output_val = compute_logLs(minibatch_index)
                for i in xrange(num_layer):
                    num_pixel = self.model.layers[i].w * self.model.layers[i].h *self.model.layers[i].Cin
                    newNL = newNL + np.sum(logLs_output_val[i])/(np.size(logLs_output_val[i])*num_pixel)

            newNL = -newNL/n_batches
            NegLogLs_vec.append(newNL)

            for i in xrange(num_layer):
                newLC.append(output_val[6 + 7*i])
                newPS.append(output_val[5 + 7*i])
                newMU.append(output_val[2 + 7*i])
                dLC_vec[i].append(norm(newLC[i] - LC[i])/norm(LC[i]))
                dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))
                dPS_vec[i].append(tol/2)

            # print amps_val

            # elif self.em_mode == 'soft':
            #     if epoch > 5 and dLC1 < tol and dPS1 < tol and dMU1 < tol and dLC2 < tol and dPS2 < tol and dMU2 < tol:
            #         break
            # else:
            #     if epoch > 5 and dMU1 < tol and dLC1 < tol and dMU2 < tol and dLC2 < tol:
            #         break


            means_val = []
            lambdas_val = []
            betas_val = []
            psis_val = []
            amps_val = []
            lambda_covs_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 7*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 7*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 7*i],dtype=theano.config.floatX))
                psis_val.append(np.asarray(output_val[5 + 7*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 7*i],dtype=theano.config.floatX))
                lambda_covs_val.append(np.asarray(output_val[6 + 7*i],dtype=theano.config.floatX))

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val,
                     psis_val=psis_val, lambda_covs_val=lambda_covs_val, amps_val=amps_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLC_vec=dLC_vec, dMU_vec=dMU_vec)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_EM_DRM_BatchSize_%i.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec[1:], dLC_vec[i], 'bo-', label='Lambda_Covs_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[2:], dLC_vec[i][1:], 'bo-', label='Lambda_Covs_Layer%i'%(i + 1))
                plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i_StartEpoch2.png' % (i + 1, self.batch_size)))
                close()

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newNL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newNL

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newNL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print '\n'
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print '\n'

        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLC_vec=dLC_vec, dMU_vec=dMU_vec)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_EM_DRM_BatchSize_%i.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[1:], dLC_vec[i], 'bo-', label='Lambda_Covs_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[2:], dLC_vec[i][1:], 'bo-', label='Lambda_Covs_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i_StartEpoch2.png' % (i + 1, self.batch_size)))
            close()

    def train_supervised(self, max_epochs, verbose, tol, stop_mode):
        N, Cin, h, w = np.shape(self.d)
        D = h*w
        n_batches = N/self.batch_size
        N_valid, Cin_valid, h_valid, w_valid = np.shape(self.dvalid)
        n_valid_batches = N_valid/self.batch_size
        N_test, Cin_test, h_test, w_test = np.shape(self.dtest)
        n_test_batches = N_test/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_dat_test = theano.shared(self.dtest)
        shared_dat_valid = theano.shared(self.dvalid)
        shared_label = theano.shared(self.label)
        shared_label_test = theano.shared(self.labeltest)
        shared_label_valid = theano.shared(self.labelvalid)

        num_layer = len(self.model.layers)

        index = T.iscalar()

        do_one_EG = theano.function([index], self.model.output_var,
                                          updates=self.model.updates, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                  self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                     givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                             self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

        validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})

        # debug_one_EG = theano.function([index], [self.model.conv1.debugval, self.model.conv2.debugval],
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
        #                                           self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})


        dPS_vec = []
        dLC_vec = []
        dMU_vec = []
        NL = None
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)

        for i in xrange(num_layer):
            # Concatenate differences in all parameter values
            dPS_vec.append([])
            dLC_vec.append([])
            dMU_vec.append([])

        logLs_output = []
        for layer in self.model.layers:
            logLs_output.append(layer.logLs)

        compute_logLs = theano.function([index], logLs_output,
                                          updates=[], on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        newNL = 0
        for minibatch_index in xrange(n_batches):
            logLs_output_val = compute_logLs(minibatch_index)
            for i in xrange(num_layer):
                num_pixel = self.model.layers[i].w * self.model.layers[i].h *self.model.layers[i].Cin
                newNL = newNL + np.sum(logLs_output_val[i])/(np.size(logLs_output_val[i])*num_pixel)

        newNL = -newNL/n_batches
        NegLogLs_vec.append(newNL)
        print("Initial NLL=", newNL)

        done_looping = False

        patience = 20000  # look as this many examples regardless
        patience_increase = 3  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.999  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_params = None
        best_validation_loss = np.inf
        best_iter = 0
        test_score = 0.0
        test_score_epoch = 0.0
        test_score_vec = []
        iter = 0

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1
            print 'Epoch %d' %epoch
            LC = []
            PS = []
            MU = []
            newLC = []
            newPS = []
            newMU = []

            for layer in self.model.layers:
                LC.append(copy.copy(layer.lambda_covs.get_value()))
                PS.append(copy.copy(layer.psis.get_value()))
                MU.append(copy.copy(layer.means.get_value()))

            for minibatch_index in xrange(n_batches):
                print minibatch_index
                iter = iter + 1
                # [sum_var1, sum_var2] = debug_one_EG(minibatch_index)
                # print 'start debugging'
                # print 'sum_var1'
                # print sum_var1
                # print 'sum_var2'
                # print sum_var2
                # print np.shape(sum_var1)
                # print np.max(sum_var1)
                # print np.min(sum_var1)
                # print np.shape(sum_var2)
                # print np.max(sum_var2)
                # print np.min(sum_var2)
                # time.sleep(60)
                output_val = do_one_EG(minibatch_index)

                if (iter + 1) % validation_frequency == 0:
                    validation_losses = [validate_model(i) for i
                                     in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_batches, \
                           this_validation_loss * 100.))

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                        # save best validation score and iteration number
                        best_validation_loss = this_validation_loss
                        best_iter = iter
                        best_params = self.model.params

                        # test it on the test set
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)
                        print(('     epoch %i, minibatch %i/%i, test error of best '
                               'model %f %%') %
                              (epoch, minibatch_index + 1, n_batches,
                               test_score * 100.))

                if patience <= iter:
                    done_looping = True
                    break

            test_losses = [test_model(i) for i in xrange(n_test_batches)]
            test_score_epoch = np.mean(test_losses)
            test_score_vec.append(test_score_epoch)
            epoch_vec.append(epoch)
            fig = figure()
            plot(epoch_vec[1:], test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.output_dir, 'Error-vs-Epoch-EG.png'))
            close(fig)
            plot_classification_file = os.path.join(self.output_dir, 'plot_classification.npz')
            np.savez(plot_classification_file, epoch_vec=epoch_vec, test_score_vec=test_score_vec)

            newNL = 0
            for minibatch_index in xrange(n_batches):
                logLs_output_val = compute_logLs(minibatch_index)
                for i in xrange(num_layer):
                    num_pixel = self.model.layers[i].w * self.model.layers[i].h *self.model.layers[i].Cin
                    newNL = newNL + np.sum(logLs_output_val[i])/(np.size(logLs_output_val[i])*num_pixel)

            newNL = -newNL/n_batches
            NegLogLs_vec.append(newNL)

            for i in xrange(num_layer):
                newLC.append(output_val[6 + 7*i])
                newPS.append(output_val[5 + 7*i])
                newMU.append(output_val[2 + 7*i])
                dLC_vec[i].append(norm(newLC[i] - LC[i])/norm(LC[i]))
                dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))
                dPS_vec[i].append(tol/2)

            if epoch == 0 and verbose:
                print("Initial NLL=", newNL)

            # print amps_val

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newNL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newNL

            # elif self.em_mode == 'soft':
            #     if epoch > 5 and dLC1 < tol and dPS1 < tol and dMU1 < tol and dLC2 < tol and dPS2 < tol and dMU2 < tol:
            #         break
            # else:
            #     if epoch > 5 and dMU1 < tol and dLC1 < tol and dMU2 < tol and dLC2 < tol:
            #         break


            means_val = []
            lambdas_val = []
            betas_val = []
            psis_val = []
            amps_val = []
            lambda_covs_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 7*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 7*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 7*i],dtype=theano.config.floatX))
                psis_val.append(np.asarray(output_val[5 + 7*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 7*i],dtype=theano.config.floatX))
                lambda_covs_val.append(np.asarray(output_val[6 + 7*i],dtype=theano.config.floatX))

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val,
                     psis_val=psis_val, lambda_covs_val=lambda_covs_val, amps_val=amps_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLC_vec=dLC_vec, dMU_vec=dMU_vec)

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newNL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('Optimization complete.')
        print('Best validation score of %f %% obtained at iteration %i,'\
              'with test performance %f %%' %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))


        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLC_vec=dLC_vec, dMU_vec=dMU_vec)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_EM_DRM_BatchSize_%i.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[1:], dLC_vec[i], 'bo-', label='Lambda_Covs_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[2:], dLC_vec[i][1:], 'bo-', label='Lambda_Covs_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i_StartEpoch2.png' % (i + 1, self.batch_size)))
            close()

        save_file = open(os.path.join(self.output_dir, 'cnn_mean_removed.pkl'), 'wb')  # this will overwrite current content
        cPickle.dump(best_params, save_file, -1)
        save_file.close()
        fig = figure()
        plot(epoch_vec[1:], test_score_vec)
        legend()
        xlabel('Epoch')
        ylabel('Error Rate')
        title('Error-vs-Epoch-EG')
        fig.savefig(os.path.join(self.output_dir, 'Error-vs-Epoch-EG.png'))
        close(fig)

    def train_unsupervised_TopDown(self, max_epochs, verbose, tol, stop_mode):
        '''
        EM learning algorithm
        :return:
        '''
        print 'start EM training'

        N, Cin, H, W = np.shape(self.d)
        n_batches = N/self.batch_size

        shared_dat = theano.shared(self.d)

        num_layer = len(self.model.layers)

        dLA_vec = []
        dMU_vec = []
        NL = None
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)
        iter = 0

        for i in xrange(num_layer):
            ## Concatenate differences in parameter values
            dLA_vec.append([])
            dMU_vec.append([])

        index = T.iscalar()

        do_one_iter = theano.function([index], self.model.output_var,
                                          updates=self.model.updates, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        compute_logLs = theano.function([index], self.model.cost,
                                          updates=[], on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        newNL = 0
        for minibatch_index in xrange(n_batches):
            logLs_output_val = compute_logLs(minibatch_index)
            newNL = newNL + logLs_output_val

        newNL = newNL/n_batches
        NegLogLs_vec.append(newNL)
        print("Initial NLL=", newNL)


        # debug = theano.function([index], self.model.conv1.amps_new,
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        start_time = timeit.default_timer()

        done_looping = False

        while (epoch < max_epochs) and (not done_looping):

            epoch = epoch + 1
            epoch_vec.append(epoch)
            print 'Epoch %d' %epoch

            LA = []
            MU = []
            newLA = []
            newMU = []

            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))
                MU.append(copy.copy(layer.means.get_value()))

            for minibatch_index in xrange(n_batches):
                print minibatch_index
                iter = iter + 1
                output_val = do_one_iter(minibatch_index)

            newNL = 0
            for minibatch_index in xrange(n_batches):
                logLs_output_val = compute_logLs(minibatch_index)
                newNL = newNL + logLs_output_val

            newNL = newNL/n_batches
            NegLogLs_vec.append(newNL)

            for i in xrange(num_layer):
                newLA.append(output_val[3 + 5*i])
                newMU.append(output_val[2 + 5*i])
                dLA_vec[i].append(norm(newLA[i] - LA[i])/norm(LA[i]))
                dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 5*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 5*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 5*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 5*i],dtype=theano.config.floatX))

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, amps_val=amps_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_EM_DRM_BatchSize_%i.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambdas_Layer%i'%(i + 1))
                plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i_StartEpoch2.png' % (i + 1, self.batch_size)))
                close()

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newNL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newNL

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newNL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print '\n'
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print '\n'

        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_EM_DRM_BatchSize_%i.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_EM_DRM_Layer%i_BatchSize_%i_StartEpoch2.png' % (i + 1, self.batch_size)))
            close()