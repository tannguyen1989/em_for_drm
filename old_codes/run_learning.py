__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')

from train_model_v4 import *
from probe_model_v4 import *
from old_codes.load_data_stable import DATA, DATA_Simulated
from train_v4 import train

if __name__ == '__main__':
    data_mode = 'all'
    em_mode = 'hard'
    stop_mode = 'NLL'
    train_method = 'EG_FRM_unsupervised'
    debug_mode = 'OFF'
    use_non_lin = 'None'

    Nlabeled = 50000
    Ni = 50000

    Cin = 1 # depth of filters
    H = 28 # width of input images
    W = 28 # height of input images
    learning_rate = 0.01
    moment_coeff = 0.0

    tol = 1e-15
    verbose = True
    batch_size = 50000
    max_epochs = 200

    seed = 2

    reg_coeff = 0.5 # for semisupervised

    output_dir = '/home/ubuntu/research_results/EM_results/FRM_Simulated_Data_EG_unsupervised_b500_D6_K5_numfilter5_M1_use50000labeleddata_50000unlabeleddata_060516'
    data_dir = '/home/ubuntu/repos/em_for_drm/data/mnist.pkl.gz'
    param_dir = os.path.join(output_dir,'params','EM_results_epoch_6.npz')

    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/FRM_Debug'
    # data_dir = '/Users/heatherseeba/repos/em_drm/data/mnist.pkl.gz'

    if train_method == 'EM_unsupervised':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed)
        mnist_model = EM_SRM_unsupervised(d=mnist.dtrain[0:1000], batch_size=batch_size, Cin=Cin, W=W, H=H, em_mode=em_mode, use_non_lin=use_non_lin, seed=seed, moment_coeff=moment_coeff)
        train_mnist_model = train(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                model=mnist_model, output_dir=output_dir)
        train_mnist_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode)

    elif train_method == 'EG_unsupervised':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed)
        mnist_model = EG_SRM_unsupervised(d=mnist.dtrain[0:1000], learning_rate=learning_rate, batch_size=batch_size,
                                          Cin=Cin, W=W, H=H, em_mode=em_mode, use_non_lin=use_non_lin, seed=seed)
        train_mnist_model = train(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                model=mnist_model, output_dir=output_dir)
        train_mnist_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode)

    elif train_method == 'EG_supervised':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed)
        mnist_model = EG_SRM_supervised(d=mnist.dtrain[0:1000], learning_rate=learning_rate, batch_size=batch_size, Cin=Cin, W=W, H=H,
                                        em_mode=em_mode, use_non_lin=use_non_lin, seed=seed, param_dir=param_dir, use_mode='finetune')
        train_mnist_model = train(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                model=mnist_model, output_dir=os.path.join(output_dir,'Finetune_End_to_End_using_3000_data'))
        train_mnist_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode, debug_mode=debug_mode)

    elif train_method == 'EG_semisupervised':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed)
        mnist_model = EG_SRM_semisupervised(d=mnist.dtrain[0:1000], learning_rate=learning_rate, batch_size=batch_size, Cin=Cin, W=W, H=H,
                                        em_mode=em_mode, use_non_lin=use_non_lin, seed=seed, param_dir=param_dir, reg_coeff=reg_coeff, use_mode='train')
        train_mnist_model = train(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                model=mnist_model, output_dir=os.path.join(output_dir,'supervised_End_to_End_using_50K_data'))
        train_mnist_model.train_semisupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode, debug_mode=debug_mode)

    elif train_method == 'EG_unsupervised_with_extra_G':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed)
        mnist_model = EG_SRM_unsupervised_with_extra_G(d=mnist.dtrain[0:1000], learning_rate=learning_rate, batch_size=batch_size,
                                          Cin=Cin, W=W, H=H, em_mode=em_mode, use_non_lin=use_non_lin, seed=seed)
        train_mnist_model = train(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                model=mnist_model, output_dir=output_dir)
        train_mnist_model.train_unsupervised_with_extra_G(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode)

    elif train_method == 'EM_FRM_unsupervised':
        data = DATA_Simulated(Ni=Ni, D=50, K=10, M=1, PPCA=False, lock_psis=True, seed=seed, mode='balanced')
        model = EM_FRM_unsupervised(data=data, batch_size=batch_size, D=50, seed=seed)
        train_model = train(batch_size=batch_size, train_data=data.dtrain, test_data=data.dtest, valid_data=data.dvalid,
                                train_label=data.train_label, test_label=data.test_label, valid_label=data.valid_label,
                                model=model, output_dir=output_dir)
        train_model.train_unsupervised_for_FRM_knowing_correct_params(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                                      correct_lambdas=data.correct_lambdas,
                                                                      correct_means=data.correct_means,
                                                                      correct_lambda_covs=data.correct_lambda_covs,
                                                                      correct_psis=data.correct_psis,
                                                                      correct_covs=data.correct_covs,
                                                                      correct_pis=data.pi)

    elif train_method == 'EG_FRM_unsupervised':
        data = DATA_Simulated(Ni=Ni, D=6, K=10, M=1, PPCA=False, lock_psis=True, seed=seed, mode='balanced')
        model = EG_FRM_unsupervised(data=data, learning_rate=learning_rate, batch_size=batch_size, D=6, seed=seed)
        train_model = train(batch_size=batch_size, train_data=data.dtrain, test_data=data.dtest, valid_data=data.dvalid,
                                train_label=data.train_label, test_label=data.test_label, valid_label=data.valid_label,
                                model=model, output_dir=output_dir)
        train_model.train_unsupervised_for_FRM_knowing_correct_params(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                                      correct_lambdas=data.correct_lambdas,
                                                                      correct_means=data.correct_means,
                                                                      correct_lambda_covs=data.correct_lambda_covs,
                                                                      correct_psis=data.correct_psis,
                                                                      correct_covs=data.correct_covs,
                                                                      correct_pis=data.pi)

    elif train_method == 'Softmax_FRM':
        data = DATA_Simulated(Ni=Ni, D=20, K=10, M=1, PPCA=False, lock_psis=True, seed=seed, mode='balanced')
        model = train_Softmax_EM_FRM(learning_rate=learning_rate, batch_size=batch_size, D=20,
                                                 em_mode=em_mode, use_non_lin=use_non_lin, seed=seed, param_dir=param_dir)
        train_model = train(batch_size=batch_size, train_data=data.dtrain, test_data=data.dtest, valid_data=data.dvalid,
                                train_label=data.train_label, test_label=data.test_label, valid_label=data.valid_label,
                                model=model, output_dir=output_dir)
        train_model.train_Softmax_for_FRM(max_epochs=max_epochs, classification_dir='classification_Softmax_Using_50000_data')

    else:
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed)
        mnist_probe_model = train_Softmax_EM_SRM(learning_rate=learning_rate, batch_size=batch_size, Cin=Cin, W=W, H=H,
                                                 em_mode=em_mode, use_non_lin=use_non_lin, seed=seed, param_dir=param_dir)
        train_mnist_probe_model = train(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                model=mnist_probe_model, output_dir=output_dir)
        train_mnist_probe_model.train_Softmax(max_epochs=max_epochs, classification_dir='classification_Softmax_Using_50000_data')

###
