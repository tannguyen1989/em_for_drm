_author__ = 'minhtannguyen'

#######################################################################################################################
# get rid of no rendering channel
# do both soft and hard on c
#######################################################################################################################


import matplotlib as mpl
mpl.use('Agg')

import numpy as np
np.set_printoptions(threshold='nan')

from theano.tensor.nnet import conv2d
from theano.tensor.nnet.neighbours import images2neibs
from theano.tensor.signal import pool

from swmfa import *


# from lasagne.layers import InputLayer, FeatureWTALayer

class CRM(SwMofa):
    """
    EM Algorithm for the Convolutional Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes
    'data_4D: data in 4-D (N,D,H,W)

    """
    def __init__(self, data_4D, labels, K, M, W, H, w, h, Cin, Ni,
                 lambdas_val_init=None, psis_val_init=None, means_val_init=None, amps_val_init=None,
                 PPCA=False, lock_psis=True,
                 em_mode='soft_and_noisy', use_non_lin='None',
                 rs_clip = 0.0,
                 max_condition_number=1.e3,
                 init_ppca=False):

        # required
        self.K = K # number of clusters
        self.M = M # latent dimensionality
        self.data_4D = data_4D
        self.labels = labels
        self.Ni = Ni # no. of images
        self.w = w # width of filters
        self.h = h # height of filters
        self.Cin = Cin # number of channels in the image
        self.D = self.h * self.w * self.Cin # patch size
        self.W = W # width of image
        self.H = H # height of image
        self.Np = (self.H - self.h + 1)*(self.W - self.w + 1) # no. of patches per image
        self.N = self.Ni * self.Np # total no. of patches and total no. of hidden units

        self.means_val_init = means_val_init
        self.lambdas_val_init = lambdas_val_init
        self.psis_val_init = psis_val_init
        self.amps_val_init = amps_val_init

        # options
        self.use_non_lin = use_non_lin
        self.em_mode = em_mode
        self.PPCA = PPCA
        self.lock_psis = lock_psis
        self.rs_clip = rs_clip
        self.max_condition_number = max_condition_number
        assert rs_clip >= 0.0

        # patchification
        print 'Patchification'
        self.data = images2neibs(ten4=self.data_4D, neib_shape=(self.h, self.w), neib_step=(1,1), mode='valid')
        self.data = T.reshape(self.data,newshape=(self.Ni, self.Cin, self.Np, self.h * self.w))
        self.data = self.data.dimshuffle(0,2,1,3)
        self.data = T.reshape(self.data, newshape=(self.Ni, self.Np, self.D))
        self.data = T.reshape(self.data, newshape=(self.N, self.D))
        self.dataT = self.data.T

        # empty arrays to be filled
        self.betas = theano.shared(value=np.zeros((self.K, self.M, self.D), dtype=theano.config.floatX),
                                         name='betas', borrow=True) # corresponds to the filters in DCNs
        self.latents = theano.shared(value=np.zeros((self.K, self.M, self.N), dtype=theano.config.floatX),
                                         name='latents', borrow=True) # a.k.a, hidden variables
        self.latent_covs = theano.shared(value=np.zeros((self.K, self.M, self.M, self.N), dtype=theano.config.floatX),
                                         name='latent_covs', borrow=True) # covariance of hidden variables
        self.rs = theano.shared(value=np.zeros((self.K, self.N), dtype=theano.config.floatX),
                                         name='rs', borrow=True) # responsibilities

        self._initialize(init_ppca)

    def _initialize(self, init_ppca):
        #
        # initialize pi's, means, lambdas, psis, lambda_covs, covs, and inv_covs
        #

        # initialize the pi's (a.k.a the priors)
        # if initial values for pi's are not provided, randomly initialize pi's
        if self.amps_val_init == None:
            amps_val = np.random.rand(self.K)
            amps_val /= np.sum(amps_val)

        else:
            amps_val = self.amps_val_init

        self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                      name='amps', borrow=True)

        # initialize the means
        # if initial values for means are not provided, randomly initialize means
        if self. means_val_init == None:
            means_val = np.asarray(np.abs(np.random.randn(self.K, self.Cin, self.h, self.w)), dtype=theano.config.floatX)
            means_val = np.reshape(means_val, newshape=(self.K, self.D))
        else:
            means_val = self.means_val_init

        self.means = theano.shared(value=np.asarray(means_val, dtype=theano.config.floatX),
                                         name='means', borrow=True) # means

        # initialize psis
        # if initial values for psis are not provided, randomly initialize psis
        if self.psis_val_init == None:
            if self.em_mode == 'soft_and_noisy':
                psis_val = np.asarray(np.abs(np.random.randn(self.K, self.D)), dtype=theano.config.floatX)
            else:
                psis_val = np.asarray(np.abs(np.zeros((self.K, self.D))), dtype=theano.config.floatX)
        else:
            psis_val = self.psis_val_init

        self.psis = theano.shared(value=np.asarray(psis_val, dtype=theano.config.floatX), name='psis', borrow=True) # noise covariance

        # initialize the lambdas
        # if initial values for lambdas are not provided, randomly initialize lambdas
        if self.lambdas_val_init == None:
            lambdas_value = np.random.randn(self.K,self.D,self.M) / \
                np.sqrt(self.max_condition_number)
        else:
            lambdas_value = self.lambdas_val_init

        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas', borrow=True)

        # Set initial lambda_covs, covs and compute inv_covs
        covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        lambda_covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        inv_covs_val = 0. * covs_val

        if self.em_mode == 'soft_and_noisy':
            # compute lambda_covs, covs, and inv_covs using matrix inversion lemma
            for k in xrange(self.K):
                covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T) + \
                    np.diag(self.psis_val_init[k])
                lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                psiI = inv(np.diag(self.psis_val_init[k]))
                lam  = lambdas_value[k]
                lamT = lam.T
                step = inv(np.eye(self.M) + np.dot(lamT,np.dot(psiI,lam)))
                step = np.dot(step,np.dot(lamT,psiI))
                step = np.dot(psiI,np.dot(lam,step))
                inv_covs_val[k] = psiI - step
        else:
            # compute lambda_covs, covs, and inv_covs
            for k in range(self.K):
                covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                inv_covs_val[k] = np.linalg.pinv(lambda_covs_val[k])

        self.covs = theano.shared(value=np.asarray(covs_val, dtype=theano.config.floatX),
                                  name='covs', borrow=True)

        self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val, dtype=theano.config.floatX),
                                  name='lambda_covs', borrow=True)

        self.inv_covs = theano.shared(value=np.asarray(inv_covs_val, dtype=theano.config.floatX),
                                  name='inv_covs', borrow=True)

    def take_EM_step(self):
        """
        Do one E step and then do one M step.
        """
        self._E_step()
        self._M_step()

    def _E_step(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """

        # Bottom-Up

        # compute the lambda dagger
        if self.em_mode == 'soft_and_noisy':
            self.betas, updates4 = theano.scan(fn=lambda l, ic: T.dot(l.T, ic),
                                     outputs_info=None,
                                     sequences=[self.lambdas, self.inv_covs])
        else:
            self.betas, updates4 = theano.scan(fn=lambda l: T.dot(1.0/(T.dot(l.T,l)),l.T),
                                     outputs_info=None,
                                     sequences=self.lambdas)

        betas = T.reshape(self.betas[:,0,:], newshape=(self.K, self.Cin, self.h, self.w))

        # compute E[z|x] using eq. 13
        latents_conv = conv2d(
            input=self.data_4D,
            filters=betas,
            filter_shape=(self.K, self.Cin, self.h, self.w),
            image_shape=(self.Ni, self.Cin, self.H, self.W),
            filter_flip=False
        )

        betameans = T.sum(self.betas[:,0,:]*self.means, axis=1)

        self.latents = latents_conv - betameans.dimshuffle('x',0,'x','x')
        self.latents = self.latents.dimshuffle(1,0,2,3)
        self.latents = T.reshape(self.latents, newshape=(self.K,self.N))
        self.latents = self.latents.dimshuffle(0,'x',1)

        if self.use_non_lin == 'ReLU':
            # Apply ReLU on latents
            self.latents = self.latents * (self.latents > 0.) + 0. * self.latents * (self.latents < 0.)
            #

        # compute E[zz'|x] using eq. 14
        # step 1 computes E[z|x](E[z|x])'
        # step 2 computes beta_j*lambda_j
        # step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
        #                              outputs_info=None,
        #                              sequences=self.latents)

        # compute the latents_covs
        step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
                                            outputs_info=None,
                                            sequences=self.latents)
        step2, updates7 = theano.scan(fn=lambda b, l: T.dot(b, l),
                                      outputs_info=None,
                                      sequences=[self.betas, self.lambdas])
        self.latent_covs, updates8_ss = theano.scan(fn=lambda s2, s1: T.eye(self.M,self.M)[:,:,None] - s2[:,:,None] + s1,
                                       outputs_info=None,
                                       sequences=[step2, step1])
        if self.em_mode == 'soft_and_noisy':
            self.logLs, self.rs = self.compute_noisy_logLs_rs() # compute the rs for soft EM
        elif self.em_mode == 'soft_no_noise':
            self.logLs = self.compute_noiseless_logLs()
            Ls = T.exp(self.logLs)
            Ls_4D = T.reshape(Ls, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
            Ls_4D = Ls_4D.dimshuffle(1,0,2,3)
            Ls_4D_pooled = pool.max_pool_2d_same_size(input=Ls_4D, patch_size=(2,2))
            Ls_4D_pooled = Ls_4D_pooled.dimshuffle(1,0,2,3)
            self.rs = Ls_4D_pooled/(T.sum(Ls_4D_pooled, axis=0) + 1e-8)
            self.rs = T.reshape(self.rs, newshape=(self.K, self.N))

        else:
            self.logLs = self.compute_noiseless_logLs() # compute the logLs for hard EM

            # # coordinate descent code: max over channel and within feature map. The ones that survive are the max in
            # # their channels and 2x2 regions. Set the survive one to 1, set the suppressed one to 0.
            # logLs_max_channel = T.max(self.logLs, axis=0)
            # channel_mask = T.ge(self.logLs, logLs_max_channel)
            # channel_mask = T.cast(channel_mask, theano.config.floatX)
            # channel_mask = T.reshape(channel_mask, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
            # channel_mask = channel_mask.dimshuffle(1,0,2,3)
            #
            # logLs_4D = T.reshape(self.logLs, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
            # logLs_4D = logLs_4D.dimshuffle(1,0,2,3)
            # spatial_mask = pool.max_pool_2d_same_size(input=logLs_4D, patch_size=(2,2))
            # spatial_mask = T.lt(spatial_mask, 0.0)
            # spatial_mask = T.cast(spatial_mask, theano.config.floatX)
            #
            # self.rs = channel_mask * spatial_mask
            # self.rs = T.reshape(self.rs.dimshuffle(1,0,2,3), newshape=(self.K, self.N))
            #
            # self.logLs = 0.5*T.sum(self.logLs * self.rs, axis=0)

            # self.rs_conv = T.reshape(self.logLs, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
            # self.rs_conv = self.rs_conv.dimshuffle(1,0,2,3)
            # self.rs_conv_new_2d = T.reshape(self.rs_conv.dimshuffle(1,0,2,3), newshape=(self.K, self.N))
            # self.rs_conv_new_2d_max = T.max(self.rs_conv_new_2d, axis=0)
            # self.rs_conv_new_2d_temp = T.ge(self.rs_conv_new_2d,self.rs_conv_new_2d_max)
            # self.rs_conv_new_2d_temp = T.cast(self.rs_conv_new_2d_temp, theano.config.floatX)
            # self.rs_conv_new_temp = T.reshape(self.rs_conv_new_2d_temp, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
            # self.rs_conv_new_temp = self.rs_conv_new_temp.dimshuffle(1,0,2,3)
            # self.rs_conv_new = pool.max_pool_2d_same_size(input=self.rs_conv_new_temp, patch_size=(2,2))
            # self.rs = T.reshape(self.rs_conv_new.dimshuffle(1,0,2,3), newshape=(self.K, self.N))
            # self.logLs = 0.5*T.sum(self.logLs * self.rs, axis=0)

            self.rs_conv = T.reshape(self.logLs, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
            self.rs_conv = self.rs_conv.dimshuffle(1,0,2,3)
            self.rs_conv_new_2d = T.reshape(self.rs_conv.dimshuffle(1,0,2,3), newshape=(self.K, self.N))
            self.rs_conv_new_2d_max = T.max(self.rs_conv_new_2d, axis=0)
            self.rs_conv_new_2d_temp = T.ge(self.rs_conv_new_2d,self.rs_conv_new_2d_max)
            self.rs = T.cast(self.rs_conv_new_2d_temp, theano.config.floatX)
            self.logLs = 0.5*T.sum(self.logLs * self.rs, axis=0)

        # mask the latents by the responsibilities. This masked latents will be sent to the next layer.
        latents_rs = self.latents[:,0,:] * self.rs
        latents_rs = T.reshape(latents_rs, (self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        latents_rs = T.transpose(latents_rs, (1,0,2,3))
        self.latents_rs = pool.pool_2d(input=latents_rs,
                                            ds=(2,2), ignore_border=True, mode='average_exc_pad')
        self.latents_rs = self.latents_rs * 4.0

        # compute the pi's
        self.sumrs = T.sum(self.rs, axis=1)
        self.amps_new, updates18 = theano.scan(fn=lambda s: s / float(self.N),
                                               outputs_info=None,
                                               sequences=self.sumrs)

        self.indx_good_cluster = T.cast((self.amps_new > 1.0/self.N).nonzero(), 'int32')[0]

    def _Top_Down(self):
        # Top-Down
        lambdalatents_E, updates910 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[self.lambdas, self.latents])
        xT = lambdalatents_E + self.means[:,:,None]
        xT = self.rs[:,None,:] * xT
        self.xT = T.sum(xT, axis=0)

    def _M_step(self):
        """
        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """


        # make latents_tilde (according to the equations at the top of page 7 in Hinton's paper)
        latents_tilde = T.concatenate([self.latents,
                                       theano.shared(value=np.ones((self.K, 1 ,self.N),
                                                                   dtype=theano.config.floatX),borrow=True)],
                                      axis=1)

        # make latents_covs_tilde (according to the equations at the top of page 7 in Hinton's paper)
        col1 = T.concatenate([self.latent_covs, (self.latents[:,:,None,:]).dimshuffle((0,2,1,3))], axis=1)
        latent_covs_tilde = T.concatenate([col1, latents_tilde[:,:,None,:]], axis=2)

        # compute lambdas_tilde (equation 15 in Hinton's paper)
        dataT = T.tile(self.dataT[None,:,:], reps=[self.K,1,1])
        lambdas_tilde, updates22 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixPinv()(T.dot(lt_covs, resp))),
                                   outputs_info=None,
                                   sequences=[latents_tilde[self.indx_good_cluster], self.rs[self.indx_good_cluster], latent_covs_tilde[self.indx_good_cluster], dataT[self.indx_good_cluster]])

        # update lambdas and means
        lambdas_new = lambdas_tilde[:,:,0:self.M]
        self.lambdas_new = T.set_subtensor(self.lambdas[self.indx_good_cluster], lambdas_new)
        means_new = lambdas_tilde[:,:,self.M]
        self.means_new = T.set_subtensor(self.means[self.indx_good_cluster], means_new)

        if self.em_mode == 'soft_and_noisy':
            # compute psis
            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[lambdas_tilde, latents_tilde[self.indx_good_cluster]])

            psis, update23 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                                         outputs_info=None,
                                         sequences=[self.rs[self.indx_good_cluster], dataT[self.indx_good_cluster], lambdalatents, self.sumrs[self.indx_good_cluster]])

            maxpsi, updates14 = theano.scan(fn=lambda p: T.max(p),outputs_info=None, sequences=psis)

            maxlam, updates15 = theano.scan(fn=lambda l: T.max(T.sum(l * l, axis=0)),
                                 outputs_info=None,
                                 sequences=self.lambdas_new[self.indx_good_cluster])
            minpsi, updates16 = theano.scan(fn=lambda maxp, maxl: T.max([maxp, maxl]) / self.max_condition_number,
                                 outputs_info=None,
                                 sequences=[maxpsi, maxlam])
            self.psis_new, updates17   = theano.scan(fn=lambda p, minp: T.clip(p, minp, np.Inf),
                                      outputs_info=None,
                                      sequences=[psis, minpsi])
            # make all elements in each rows of psis the same
            if self.PPCA:
                self.psis_new, updates19 = theano.scan(fn=lambda psn: T.mean(psn)*T.ones(self.D),
                                                       outputs_info=None,
                                                       sequences=self.psis_new)

            # make psi for each cluster the same
            if self.lock_psis:
                self.psis_new = T.dot(self.sumrs[self.indx_good_cluster],self.psis_new)/T.sum(self.sumrs[self.indx_good_cluster])
                self.psis_new = (self.psis_new).dimshuffle('x',0)
                self.psis_new = T.tile(self.psis_new,(self.K, 1))
        else:
            self.psis_new = self.psis

        # update covs
        self._update_covs()

    def _update_covs(self):
        """
        Update covs and inv_covs
        """

        # total cov = lambda*lambda' + psi
        self.lambda_covs_new, updates20 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas_new])

        if self.em_mode == 'soft_and_noisy':
            self.covs_new, updates1 = theano.scan(fn=lambda lc, p: lc + diag(p),
                                              outputs_info=None,
                                              sequences=[self.lambda_covs_new, self.psis_new])

            # compute the inv_covs using the matrix inversion lemma
            self.inv_covs_new, updates2 = theano.scan(fn=lambda l, p: MatrixInverse()(diag(p)) - T.dot(MatrixInverse()(diag(p)),T.dot(l,T.dot(MatrixInverse()(T.eye(self.M) + T.dot(T.transpose(l),T.dot(MatrixInverse()(diag(p)),l))),T.dot(T.transpose(l),MatrixInverse()(diag(p)))))),
                                              outputs_info=None,
                                              sequences=[self.lambdas_new, self.psis_new])
        else:
            self.covs_new, updates1 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas_new])

            self.inv_covs_new, updates2 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l))/T.sqr(T.sum(T.sqr(l))),
                                              outputs_info=None,
                                              sequences=self.lambdas_new)


    def compute_noisy_logLs_rs(self):
        """
        This function calculates responsibilities when there is noise. The calculation is done for each datum
        under each component.
        """
        # compute the logr responsibilities
        logrs, updates3 = theano.scan(fn=lambda a, c, m, ic: T.log(a) - float(0.5 * np.log(2 * np.pi) * self.D) -
                                                             0.5 * 2 * T.sum(T.log(diag(Cholesky()(c)))) -
                                                             0.5 * T.sum((self.data - m).T * T.dot(ic, (self.data - m).T),
                                                                         axis=0),
                            outputs_info=None,
                            sequences=[self.amps, self.covs, self.means, self.inv_covs])
        # compute the logLs
        L = self._log_sum(logrs)

        # normalize the logrs
        logrs -= L[None, :]
        # clip the logrs
        if self.rs_clip > 0.0:
            logrs = T.clip(logrs,T.log(self.rs_clip),np.Inf)

        return L, T.exp(logrs) # return the rs

    def compute_noiseless_logLs(self):
        """
        This function calculates log likelihoods when there is no noise. The calculation is done for each datum
        under each component.
        """
        # compute the orthogonal vector from data points to clusters
        ortho_vec, updates333 = theano.scan(fn=lambda m,lam, z: (self.data - m).T - T.dot(lam, z),
                            outputs_info=None,
                            sequences=[self.means, self.lambdas, self.latents])

        # compute the log-likelihood
        L, updates337 = theano.scan(fn=lambda d: -T.sum(d*d, axis=0),
                            outputs_info=None,
                            sequences=ortho_vec)
        return L # return the log likelihoods


    def _log_sum(self,loglikes):
        """
        Calculate sum of log likelihoods
        """
        a = T.max(loglikes, axis=0)
        return a + T.log(T.sum(T.exp(loglikes - a[None, :]), axis=0))