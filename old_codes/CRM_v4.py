__author__ = 'minhtannguyen'

#######################################################################################################################
# get rid of no rendering channel
# do both soft and hard on c
#######################################################################################################################


import matplotlib as mpl
mpl.use('Agg')

import numpy as np
np.set_printoptions(threshold='nan')

from theano.tensor.nnet import conv2d
from theano.tensor.nnet.neighbours import images2neibs
from theano.tensor.signal import pool

from swmfa import *


# from lasagne.layers import InputLayer, FeatureWTALayer

class CRM(SwMofa):
    """
    EM Algorithm for the Convolutional Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes
    'data_4D: data in 4-D (N,D,H,W)

    """
    def __init__(self, data_4D, labels, K, M, W, H, w, h, Cin, Ni,
                 lambdas_val_init=None, means_val_init=None, amps_val_init=None,
                 PPCA=False, lock_psis=True,
                 em_mode='hard', use_non_lin='None',
                 rs_clip = 0.0,
                 max_condition_number=1.e3,
                 init_ppca=False):

        # required
        self.K = K # number of clusters
        self.M = M # latent dimensionality
        self.data_4D = data_4D
        self.labels = labels
        self.Ni = Ni # no. of images
        self.w = w # width of filters
        self.h = h # height of filters
        self.Cin = Cin # number of channels in the image
        self.D = self.h * self.w * self.Cin # patch size
        self.W = W # width of image
        self.H = H # height of image
        self.Np = (self.H - self.h + 1)*(self.W - self.w + 1) # no. of patches per image
        self.N = self.Ni * self.Np # total no. of patches and total no. of hidden units

        self.means_val_init = means_val_init
        self.lambdas_val_init = lambdas_val_init
        self.amps_val_init = amps_val_init

        # options
        self.use_non_lin = use_non_lin
        self.em_mode = em_mode
        self.PPCA = PPCA
        self.lock_psis = lock_psis
        self.rs_clip = rs_clip
        self.max_condition_number = max_condition_number
        assert rs_clip >= 0.0

        # patchification
        print 'Patchification'
        self.data = images2neibs(ten4=self.data_4D, neib_shape=(self.h, self.w), neib_step=(1,1), mode='valid')
        self.data = T.reshape(self.data,newshape=(self.Ni, self.Cin, self.Np, self.h * self.w))
        self.data = self.data.dimshuffle(0,2,1,3)
        self.data = T.reshape(self.data, newshape=(self.Ni, self.Np, self.D))
        self.data = T.reshape(self.data, newshape=(self.N, self.D))

        self._initialize(init_ppca)

    def _initialize(self, init_ppca):
        #
        # initialize pi's, means, lambdas, psis, lambda_covs, covs, and inv_covs
        #

        # initialize the pi's (a.k.a the priors)
        # if initial values for pi's are not provided, randomly initialize pi's
        if self.amps_val_init == None:
            amps_val = np.random.rand(self.K)
            amps_val /= np.sum(amps_val)

        else:
            amps_val = self.amps_val_init

        self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                      name='amps', borrow=True)

        # initialize the means
        # if initial values for means are not provided, randomly initialize means
        if self. means_val_init == None:
            means_val = np.asarray(np.abs(np.random.randn(self.K, self.Cin, self.h, self.w)), dtype=theano.config.floatX)
            means_val = np.reshape(means_val, newshape=(self.K, self.D))
        else:
            means_val = self.means_val_init

        self.means = theano.shared(value=np.asarray(means_val, dtype=theano.config.floatX),
                                         name='means', borrow=True) # means

        # initialize the lambdas
        # if initial values for lambdas are not provided, randomly initialize lambdas
        if self.lambdas_val_init == None:
            lambdas_value = np.random.randn(self.K,self.D,self.M) / \
                np.sqrt(self.max_condition_number)
        else:
            lambdas_value = self.lambdas_val_init

        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas', borrow=True)

    def take_EM_step(self):
        """
        Do one E step and then do one M step.
        """
        self._E_step()
        self._M_step()

    def _E_step(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """

        # Bottom-Up

        # compute the lambda dagger
        self.betas, _ = theano.scan(fn=lambda l: T.dot(1.0/(T.dot(l.T,l)),l.T),
                                 outputs_info=None,
                                 sequences=self.lambdas)

        betas = T.reshape(self.betas[:,0,:], newshape=(self.K, self.Cin, self.h, self.w))

        # compute E[z|x] using eq. 13
        latents_conv = conv2d(
            input=self.data_4D,
            filters=betas,
            filter_shape=(self.K, self.Cin, self.h, self.w),
            image_shape=(self.Ni, self.Cin, self.H, self.W),
            filter_flip=False
        )

        betameans = T.sum(self.betas[:,0,:]*self.means, axis=1)

        self.latents = latents_conv - betameans.dimshuffle('x',0,'x','x')
        self.latents = self.latents.dimshuffle(1,0,2,3)
        self.latents = T.reshape(self.latents, newshape=(self.K,self.N))
        self.latents = self.latents.dimshuffle(0,'x',1)

        if self.use_non_lin == 'ReLU':
            # Apply ReLU on latents
            self.latents = self.latents * (self.latents > 0.) + 0. * self.latents * (self.latents < 0.)
            #

         # compute the latents_covs
        step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
                                            outputs_info=None,
                                            sequences=self.latents)
        step2, updates7 = theano.scan(fn=lambda b, l: T.dot(b, l),
                                      outputs_info=None,
                                      sequences=[self.betas, self.lambdas])
        self.latent_covs, updates8_ss = theano.scan(fn=lambda s2, s1: T.eye(self.M,self.M)[:,:,None] - s2[:,:,None] + s1,
                                       outputs_info=None,
                                       sequences=[step2, step1])

        self.logLs = self.compute_noiseless_logLs() # compute the logLs for hard EM. In this
                                                    # logLs are distances from the data point to the clusters

        # max over a
        neg_norm = self.compute_data_negative_norm()
        self.diff = self.logLs - neg_norm
        max_over_a_mask = T.cast(T.gt(self.diff, 0.), theano.config.floatX)
        max_over_a_mask = T.reshape(max_over_a_mask, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        max_over_a_mask = max_over_a_mask.dimshuffle(1,0,2,3) #(N, K, H-h+1, W-w+1)

        # # # for max over t
        # # max over t
        # max_over_t_input = T.reshape(self.logLs, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1)) # convert logLs into the shape of (K, N, H-h+1, W-w+1)
        # max_over_t_input = max_over_t_input.dimshuffle(1,0,2,3) #(N, K, H-h+1, W-w+1)
        #
        # self.max_over_t_mask = T.grad(T.sum(pool.pool_2d(input=max_over_t_input, ds=(2,2), ignore_border=True, mode='max')),
        #              wrt=max_over_t_input) # argmax across t
        # self.max_over_t_mask = T.cast(self.max_over_t_mask, theano.config.floatX)
        #
        # # soft over c
        # max_val = T.max(pool.pool_2d(input=max_over_t_input, ds=(2,2), ignore_border=True, mode='max'),axis=1)
        # max_val_var = max_val.repeat(2, axis=1).repeat(2, axis=2)
        # max_val_var = max_val_var.dimshuffle(0,'x',1,2)
        # soft_over_c = T.exp(max_over_t_input - max_val_var) * self.max_over_t_mask # change this line when switch from no max over a to max over a by * max_over_a_mask
        # sum_t_var = pool.pool_2d(input=soft_over_c, ds=(2,2), ignore_border=True, mode='average_exc_pad')
        # sum_t_var = sum_t_var * 4.0
        # sum_c_var = T.sum(sum_t_var, axis=1)
        # sum_var = sum_c_var.repeat(2, axis=1).repeat(2, axis=2)
        # sum_var = sum_var.dimshuffle(0,'x',1,2)
        # self.soft_c_mask = soft_over_c/(sum_var + 1e-8)

        # for soft over c and t
        max_over_t_input = T.reshape(self.logLs, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1)) # convert logLs into the shape of (K, N, H-h+1, W-w+1)
        max_over_t_input = max_over_t_input.dimshuffle(1,0,2,3) #(N, K, H-h+1, W-w+1)

        max_val = T.max(pool.pool_2d(input=max_over_t_input, ds=(2,2), ignore_border=True, mode='max'),axis=1)
        max_val_var = max_val.repeat(2, axis=1).repeat(2, axis=2)
        max_val_var = max_val_var.dimshuffle(0,'x',1,2)
        soft_over_c = T.exp(max_over_t_input - max_val_var) * max_over_a_mask # change this line when switch from no max over a to max over a by * max_over_a_mask
        sum_t_var = pool.pool_2d(input=soft_over_c, ds=(2,2), ignore_border=True, mode='average_exc_pad')
        sum_t_var = sum_t_var * 4.0
        sum_c_var = T.sum(sum_t_var, axis=1)
        sum_var = sum_c_var.repeat(2, axis=1).repeat(2, axis=2)
        sum_var = sum_var.dimshuffle(0,'x',1,2)
        self.soft_c_mask = soft_over_c/(sum_var + 1e-8)

        # compute the responsibilities
        self.rs = self.soft_c_mask
        self.rs = T.reshape(self.rs.dimshuffle(1,0,2,3), newshape=(self.K, self.N))

        self.logLs = 0.5*T.sum(self.logLs * self.rs, axis=0)

        # used to sum over channel

        # latents_rs = self.latents[:,0,:] * self.rs
        latents_rs = self.rs
        latents_rs = T.reshape(latents_rs, (self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        latents_rs = T.transpose(latents_rs, (1,0,2,3))

        self.latents_rs = pool.pool_2d(input=latents_rs, ds=(2,2),
                                       ignore_border=True, mode='average_exc_pad')
        self.latents_rs = self.latents_rs * 4.0

        # # For Deep
        # latents_rs2 = self.latents[:,0,:]
        # latents_rs2 = T.reshape(latents_rs2, (self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        # latents_rs2 = T.transpose(latents_rs2, (1,0,2,3))
        # self.latents_rs2 = latents_rs2
        #
        # self.latents_rs2 = pool.pool_2d(input=latents_rs2, ds=(2,2),
        #                                ignore_border=True, mode='max')
        # self.latents_rs2 = self.latents_rs2 * 4.0

        # compute the pi's
        self.sumrs = T.sum(self.rs, axis=1)
        self.amps_new = self.sumrs/float(self.N/4)

        self.indx_good_cluster = T.cast((self.amps_new > 1.0/self.N).nonzero(), 'int32')[0]

    def _Top_Down(self, z, rs):
        # Top-Down
        # input is of size Ni x K x (H-h+1)/2 x (W-w+1)/2
        #

        rs_deconv = (T.reshape(rs, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))).dimshuffle(1,0,2,3)
        latents_unpooled = z.repeat(2, axis=2).repeat(2, axis=3)
        latents_unpooled = latents_unpooled * T.cast(T.neq(rs_deconv, 0), theano.config.floatX)
        lambdas_deconv = (T.reshape(self.lambdas[:,:,0], newshape=(self.K, self.Cin, self.h, self.w))).dimshuffle(1,0,2,3)
        lambdas_deconv = lambdas_deconv[:,:,::-1,::-1]
        means_deconv = (T.reshape(self.means, newshape=(self.K, self.Cin, self.h, self.w))).dimshuffle(1,0,2,3)
        means_deconv = means_deconv[:,:,::-1,::-1]

        data_reconstructed_lambdas = conv2d(
            input=latents_unpooled,
            filters=lambdas_deconv,
            filter_shape=(self.Cin, self.K, self.h, self.w),
            image_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
            filter_flip=False,
            border_mode='full'
        )
        data_reconstructed_means = conv2d(
            input=rs_deconv,
            filters=means_deconv,
            filter_shape=(self.Cin, self.K, self.h, self.w),
            image_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
            filter_flip=False,
            border_mode='full'
        )

        self.data_reconstructed = data_reconstructed_lambdas + data_reconstructed_means

    def Sample_Images(self, z, rs):
        # Top-Down
        # input is of size Ni x K x (H-h+1)/2 x (W-w+1)/2
        #

        latents_unpooled = z.repeat(2, axis=2).repeat(2, axis=3)
        latents_unpooled = latents_unpooled * rs
        lambdas_deconv = (T.reshape(self.lambdas[:,:,0], newshape=(self.K, self.Cin, self.h, self.w))).dimshuffle(1,0,2,3)
        lambdas_deconv = lambdas_deconv[:,:,::-1,::-1]
        means_deconv = (T.reshape(self.means, newshape=(self.K, self.Cin, self.h, self.w))).dimshuffle(1,0,2,3)
        means_deconv = means_deconv[:,:,::-1,::-1]

        data_sampled_lambdas = conv2d(
            input=latents_unpooled,
            filters=lambdas_deconv,
            filter_shape=(self.Cin, self.K, self.h, self.w),
            image_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
            filter_flip=False,
            border_mode='full'
        )
        data_sampled_means = conv2d(
            input=rs,
            filters=means_deconv,
            filter_shape=(self.Cin, self.K, self.h, self.w),
            image_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
            filter_flip=False,
            border_mode='full'
        )

        self.data_sampled = data_sampled_lambdas + data_sampled_means

    def compute_noiseless_logLs(self):
        """
        This function calculates log likelihoods when there is no noise. The calculation is done for each datum
        under each component.
        """
        # compute the orthogonal vector from data points to clusters
        ortho_vec, _ = theano.scan(fn=lambda m,lam, z, x: (x - m).T - T.dot(lam, z),
                            outputs_info=None,
                            sequences=[self.means, self.lambdas, self.latents],
                            non_sequences=self.data)

        # compute the log-likelihood
        L, _ = theano.scan(fn=lambda d: -T.sum(d*d, axis=0),
                            outputs_info=None,
                            sequences=ortho_vec)
        return L # return the log likelihoods

    def compute_data_negative_norm(self):
        '''
        This functions calculates the norm of each patch
        :return:
        '''

        neg_norm = -T.sum(self.data * self.data, axis=1)

        return neg_norm

    def _log_sum(self,loglikes):
        """
        Calculate sum of log likelihoods
        """
        a = T.max(loglikes, axis=0)
        return a + T.log(T.sum(T.exp(loglikes - a[None, :]), axis=0))

########################################################################################################################
# M step #
########################################################################################################################
    def _M_step(self):
        """
        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """

        # make latents_tilde (according to the equations at the top of page 7 in Hinton's paper)
        latents_tilde = T.concatenate([self.latents,
                                       theano.shared(value=np.ones((self.K, 1 ,self.N),
                                                                   dtype=theano.config.floatX),borrow=True)],
                                      axis=1)

        # make latents_covs_tilde (according to the equations at the top of page 7 in Hinton's paper)
        col1 = T.concatenate([self.latent_covs, (self.latents[:,:,None,:]).dimshuffle((0,2,1,3))], axis=1)
        latent_covs_tilde = T.concatenate([col1, latents_tilde[:,:,None,:]], axis=2)

        # compute lambdas_tilde (equation 15 in Hinton's paper)
        lambdas_tilde, updates22 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixPinv()(T.dot(lt_covs, resp))),
                                   outputs_info=None,
                                   sequences=[latents_tilde[self.indx_good_cluster], self.rs[self.indx_good_cluster], latent_covs_tilde[self.indx_good_cluster]],
                                   non_sequences=self.data.T)

        # update lambdas and means
        lambdas_new = lambdas_tilde[:,:,0:self.M]
        self.lambdas_new = T.set_subtensor(self.lambdas[self.indx_good_cluster], lambdas_new)
        means_new = lambdas_tilde[:,:,self.M]
        self.means_new = T.set_subtensor(self.means[self.indx_good_cluster], means_new)

        # update covs
        self._update_covs()

    def _update_covs(self):
        """
        Update covs and inv_covs
        """

        self.covs_new, updates1 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                  outputs_info=None,
                                                  sequences=[self.lambdas_new])

        self.inv_covs_new, updates2 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l))/T.sqr(T.sum(T.sqr(l))),
                                          outputs_info=None,
                                          sequences=self.lambdas_new)

########################################################################################################################
# For G10 step #
########################################################################################################################
    def _compute_new_logLs(self, latents, rs):
        ortho_vec, _ = theano.scan(fn=lambda m,lam, z, x: (x - m).T - T.dot(lam, z),
                            outputs_info=None,
                            sequences=[self.means, self.lambdas, latents],
                            non_sequences=self.data)

        # compute the log-likelihood
        self.logLs_new, _ = theano.scan(fn=lambda d: -T.sum(d*d, axis=0),
                            outputs_info=None,
                            sequences=ortho_vec)

        self.logLs_new = 0.5*T.sum(self.logLs_new * rs, axis=0)
