__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from CRM_v1 import CRM
import gzip
import cPickle
import os
import sys
import time
import timeit

import itertools

import numpy as np
from scipy import misc

import theano
import theano.tensor as T

from PIL import Image, ImageDraw

from scipy.linalg import inv, norm

def getParams(mix, x, d):
    # output learned parameters
    output_final = theano.function([], [mix.means, mix.lambdas, mix.psis,
                                        mix.covs, mix.inv_covs, mix.amps,
                                        mix.lambda_covs, mix.rs, mix.logLs,
                                        mix.latents, mix.sumrs, mix.betas],
                                     on_unused_input='ignore',
                                     givens={x:d})

    means_val, lambdas_val, psis_val, covs_val, inv_covs_val, amps_val, \
    lambda_covs_val, rs_val, logLs_val, latents, sumrs, betas = output_final()

    # Converse the results into numpy arrays
    means_val = np.asarray(means_val,dtype=theano.config.floatX)
    lambdas_val = np.asarray(lambdas_val,dtype=theano.config.floatX)
    psis_val = np.asarray(psis_val,dtype=theano.config.floatX)
    covs_val = np.asarray(covs_val,dtype=theano.config.floatX)
    inv_covs_val = np.asarray(inv_covs_val,dtype=theano.config.floatX)
    amps_val = np.asarray(amps_val,dtype=theano.config.floatX)
    lambda_covs_val = np.asarray(lambda_covs_val,dtype=theano.config.floatX)
    rs_val = np.asarray(rs_val,dtype=theano.config.floatX)
    logLs_val = np.asarray(logLs_val,dtype=theano.config.floatX)
    sumrs = np.asarray(sumrs,dtype=theano.config.floatX)
    # betas = np.asarray(betas,dtype=theano.config.floatX)
    # latents = np.asarray(latents,dtype=theano.config.floatX)

    return means_val, lambdas_val, psis_val, covs_val, inv_covs_val, amps_val, lambda_covs_val, rs_val, logLs_val, sumrs

def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

def naive_sample_from_model(lambdas, means, rs, z, Ni, Cin, H, W, K, h, w, output_dir):
    Nh = H - h + 1
    Nw = W - w + 1
    d = np.zeros((Ni, Cin, H, W), dtype=theano.config.floatX)

    rs = np.reshape(rs, newshape=(K, Ni, H - h + 1, W - w + 1))
    rs = rs.transpose((1,0,2,3))

    z = z[:,0,:]
    z = np.reshape(z, newshape=(K-1, Ni, H - h + 1, W - w + 1))
    z = z.transpose((1,0,2,3))

    lambdas = np.reshape(lambdas, newshape=(K,h,w))
    means = np.reshape(means, newshape=(K,h,w))

    for n in xrange(Ni):
        for k in xrange(1,K):
            for hindx in xrange(Nh):
                for windx in xrange(Nw):
                    d[n, : , hindx:hindx + h, windx:windx + w] \
                        = d[n, : , hindx:hindx + h, windx:windx + w] + \
                          rs[n,k,hindx,windx]*(lambdas[k]*z[n,k - 1,hindx,windx] + means[k])

    d = d.transpose((0,3,2,1))
    print np.shape(d)
    for i in xrange(Ni):
        imshow(d[i,:,:,0], cmap='gray')
        axis('off')
        out_path = os.path.join(output_dir, 'DirectSampledImage_%i.jpg'%i)
        savefig(out_path)

    return None

def sample_from_model(param_file, output_dir, Ni, Cin, H, W, K, h, w):
    Nh = H - h + 1
    Nw = W - w + 1

    with load(param_file) as params:
        means_val = params['means_val']
        lambdas_val = params['lambdas_val']
        lambda_covs_val= params['lambda_covs_val']
        covs_val = params['covs_val']
        inv_covs_val = params['inv_covs_val']
        psis_val = params['psis_val']
        amps_val = params['amps_val']

    lambdas_val = np.reshape(lambdas_val, newshape=(K,h,w))
    means_val = np.reshape(means_val, newshape=(K,h,w))

    # Initialize the data
    d = np.zeros((Ni, Cin, H, W), dtype=theano.config.floatX)

    rs = np.zeros((Ni, K, Nh, Nw), dtype=theano.config.floatX)
    z = np.zeros((Ni, K, Nh, Nw), dtype=theano.config.floatX)

    # Synthesize data
    # Note: Haven't added noise to zero patches

    lw_bound_h = -(h - 1)
    lw_bound_w = -(w - 1)
    up_bound_h = h
    up_bound_w = w

    # set which patches to render
    matA = np.zeros((Ni, Nh, Nw))
    for n in xrange(Ni):
        coorVec = list(itertools.product(range(H - h + 1), repeat=2))
        szVec = np.size(coorVec)
        while szVec > 0:
            token_val = np.random.randint(0,2)
            if token_val == 0:
                continue
            else:
                indx_choice = np.random.randint(0,shape(coorVec)[0])
                coor = coorVec[indx_choice]
                matA[n, coor[0], coor[1]] = 1
                for i in xrange(lw_bound_h, up_bound_h, 1):
                    for j in xrange(lw_bound_w, up_bound_w, 1):
                        if (coor[0] + i, coor[1] + j) in coorVec:
                            coorVec.remove((coor[0] + i, coor[1] + j))
                szVec = np.size(coorVec)


    for n in xrange(Ni):
        for hindx in xrange(Nh):
            for windx in xrange(Nw):
                if matA[n, hindx, windx] > 0:
                    k = np.random.choice(range(K - 1)) + 1
                    rs[n,k,hindx,windx] = 1
                    z[n,k,hindx,windx] = np.random.randn()
                    d[n, : , hindx:hindx + h, windx:windx + w] = lambdas_val[k]*z[n,k,hindx,windx] + means_val[k]
                else:
                    rs[n,0,hindx,windx] = 1

    d = d.transpose((0,3,2,1))
    print np.shape(d)
    for i in xrange(Ni):
        imshow(d[i,:,:,0], cmap='gray')
        out_path = os.path.join(output_dir, 'SampledImage_%i.jpg'%i)
        savefig(out_path)

    return None

if __name__ == '__main__':
    which_dataset = 'mnist'
    data_mode = 'all'
    em_mode = 'hard'
    update_mode = 'hinton'
    mode = 'NLL'
    run_mode = 'direct_sampling'
     # set internal parameters
    Ni = 50000
    Nsample = 200
    h = 4 # height of filters
    w = 4 # width of filters
    K = 21 # no. filters
    if which_dataset == 'mnist':
        Cin = 1 # depth of filters
        H = 28 # width of input image
        W = 28 # height of input image
    else:
        Cin = 3 # depth of filters
        H = 32 # width of input image
        W = 32 # height of input image

    M = 1 # dimension of z
    D = h * w * Cin # patch size
    Np = (H-h+1)*(W-w+1)

    maxiter = 20
    tol = 1e-4
    verbose = True
    batch_size = 1000
    max_epochs = 5

    seed = 20
    np.random.seed(seed)

    output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/EM_results_wh4x4_K21_b1000'
    sample_dir = os.path.join(output_dir, 'samples')
    cifar_dir = '/home/ubuntu/repos/psychophysics/data/cifar10'
    param_file = output_dir + '/EM_resuls.npz'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    if not os.path.exists(sample_dir):
        os.makedirs(sample_dir)

    # Load the dataset
    if which_dataset == 'mnist':
        dataset = '../data/mnist.pkl.gz'
        f = gzip.open(dataset, 'rb')
        train_set, valid_set, test_set = cPickle.load(f)
        f.close()
        train_dat = train_set[0]
        train_label = train_set[1]
        test_dat = test_set[0]
        test_label = test_set[1]

        print 'Shape of MNIST training dataset'
        print np.shape(train_dat)
        print 'Shape of MNIST training labels'
        print np.shape(train_label)
        print 'Shape of MNIST test dataset'
        print np.shape(test_dat)
        print 'Shape of MNIST test labels'
        print np.shape(test_label)
    else:
        train_set_1 = unpickle(cifar_dir + "/data_batch_1")
        train_set_2 = unpickle(cifar_dir + "/data_batch_2")
        train_set_3 = unpickle(cifar_dir + "/data_batch_3")
        train_set_4 = unpickle(cifar_dir + "/data_batch_4")
        train_set_5 = unpickle(cifar_dir + "/data_batch_5")
        test_set = unpickle(cifar_dir + "/test_batch")
        train_set = train_set_1
        train_set['data'] = np.concatenate((train_set['data'],train_set_2['data'],train_set_3['data'],train_set_4['data'], train_set_5['data']),axis=0)
        train_set['labels'] = np.concatenate((train_set['labels'],train_set_2['labels'],train_set_3['labels'],train_set_4['labels'], train_set_5['labels']),axis=0)
        train_dat = train_set['data']
        train_label = train_set['labels']
        test_dat = test_set['data']
        test_label = test_set['labels']

        print 'Shape of CIFAR10 training dataset'
        print np.shape(train_dat)
        print 'Shape of CIFAR10 training labels'
        print np.shape(train_label)
        print 'Shape of CIFAR10 test dataset'
        print np.shape(test_dat)
        print 'Shape of CIFAR10 test labels'
        print np.shape(test_label)

    if not data_mode == 'all':
        # select digits
        train_digit_indx = nonzero((train_label == 1) + (train_label == 7) + (train_label == 4))
        train_dat = train_dat[train_digit_indx[0]]
        train_label = train_label[train_digit_indx[0]]
        print 'Size of the selected training dataset from MNIST'
        print np.size(train_digit_indx)

        test_digit_indx = nonzero((test_label == 1) + (test_label == 7) + (test_label == 4))
        test_dat = test_dat[test_digit_indx[0]]
        test_label = test_label[test_digit_indx[0]]
        print 'Size of the selected test dataset from MNIST'
        print np.shape(test_digit_indx)
        Ni = np.shape(train_dat)[0] # number of input images

    if which_dataset == 'mnist':
        d = np.reshape(train_dat[0:Ni], newshape=(Ni,1,28,28))
    else:
        d = np.reshape(train_dat[0:Ni], newshape=(Ni,3,32,32))
        d = np.float32(d)

    if run_mode == 'train':
        x_temp = T.tensor4('x_temp')

        means_val_init = np.asarray(np.abs(np.random.randn(K, Cin, h, w)), dtype=theano.config.floatX)
        means_val_init = np.reshape(means_val_init, newshape=(K, D))

        psis_val_init = np.asarray(np.abs(np.random.randn(K, D)), dtype=theano.config.floatX)

        mix_temp = CRM(x=x_temp, dat=d[0:1000], K=K, M=M, W=W, H=H, w=w, h=h, Cin=Cin, Ni=Ni,
                       amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                       covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                       PPCA=False, lock_psis=True, update_mode=update_mode, em_mode=em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        print 'Compute some stuffs'
        data_val = mix_temp.createPatches()
        data_valT = data_val.T
        xmin, xmax = np.min(data_valT[0]), np.max(data_valT[0])
        ymin, ymax = np.min(data_valT[1]), np.max(data_valT[1])

        x = T.tensor4('x')

        means_val_init = np.zeros((K,D), dtype=theano.config.floatX)
        for i in xrange(K-1):
            indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
            means_val_init[i+1] = np.mean(data_val[indx_img], axis=0)

        print 'initial mean'
        print means_val_init

        mix = CRM(x=x, dat=d, K=K, M=M, W=W, H=H, w=w, h=h, Cin=Cin, Ni=batch_size,
                  amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                  covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                  PPCA=False, lock_psis=True, update_mode=update_mode, em_mode=em_mode,
                  rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        print 'start training'
        start_time = time.time()

        betas_val, latents_val, latent_covs_val, means_val, covs_val,\
        inv_covs_val, lambdas_val, psis_val, lambda_covs_val, amps_val, epochs, \
        dLC_vec, dPS_vec, dMU_vec, NegLogLs_vec = mix.batch_fit(x=x, dat=d, K=K, batch_size=batch_size,
                                                                tol=tol, max_epochs=max_epochs, verbose=verbose, mode=mode)

        stop_time = time.time()
        print 'train takes %f seconds' %(stop_time - start_time)

        means_val = np.asarray(means_val,dtype=theano.config.floatX)
        lambdas_val = np.asarray(lambdas_val,dtype=theano.config.floatX)
        betas_val = np.asarray(betas_val,dtype=theano.config.floatX)
        psis_val = np.asarray(psis_val,dtype=theano.config.floatX)
        covs_val = np.asarray(covs_val,dtype=theano.config.floatX)
        inv_covs_val = np.asarray(inv_covs_val,dtype=theano.config.floatX)
        amps_val = np.asarray(amps_val,dtype=theano.config.floatX)
        lambda_covs_val = np.asarray(lambda_covs_val,dtype=theano.config.floatX)

        out_path = os.path.join(output_dir, 'EM_resuls.npz')
        print 'Saving vars to npz file...'
        np.savez(out_path, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, psis_val=psis_val,
                 covs_val=covs_val, inv_covs_val=inv_covs_val, lambda_covs_val=lambda_covs_val, amps_val=amps_val)

        # Converse the results into numpy arrays

        fig1 = figure()
        plot(epochs, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % batch_size)
        fig1.savefig(os.path.join(output_dir,'Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i.png' % batch_size))

        fig2 = figure()
        plot(epochs, dLC_vec, 'bo-', label='Lambda_Covs')
        plot(epochs, dMU_vec, 'go-', label='Mu')
        legend()
        xlabel('Epoch')
        ylabel('Relative Difference')
        title('Convergence_of_Parameters_from_my_MFA_BatchSize_%i' % batch_size)
        fig2.savefig(os.path.join(output_dir, 'Convergence_of_Parameters_from_my_MFA_BatchSize_%i.png' % batch_size))

        for i in xrange(1,K):
            filter = np.reshape(betas_val[i-1], (h, w))
            fig = figure()
            imshow(filter, cmap = cm.Greys_r)
            axis('off')
            savefig(os.path.join(output_dir, 'filter_%d.png' %i))
            close()

            lambda_mat = np.reshape(lambdas_val[i], (h, w))
            fig = figure()
            imshow(lambda_mat, cmap = cm.Greys_r)
            axis('off')
            savefig(os.path.join(output_dir, 'lambda_%d.png' %i))
            close()

            mean_mat = np.reshape(means_val[i], (h, w))
            fig = figure()
            imshow(mean_mat, cmap = cm.Greys_r)
            axis('off')
            savefig(os.path.join(output_dir, 'mean_%d.png' %i))
            close()

        print '\nDone Train Mode'

        print amps_val
        print np.shape(lambdas_val)

        print np.shape(epochs)
        print np.shape(NegLogLs_vec)
    elif run_mode == 'sampling':
        sample_from_model(param_file=param_file, output_dir=sample_dir, Ni=Nsample, Cin=Cin, H=H, W=W, K=K, h=h, w=w)
    elif run_mode == 'direct_sampling':
        with load(param_file) as params:
            means_val_init = params['means_val']
            lambdas_val_init = params['lambdas_val']
            lambda_covs_val_init= params['lambda_covs_val']
            covs_val_init = params['covs_val']
            inv_covs_val_init = params['inv_covs_val']
            psis_val_init = params['psis_val']
            amps_val_init = params['amps_val']
            # betas_val_init = params['betas_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = CRM(x=x, dat=input_dat, K=K, M=M, W=W, H=H, w=w, h=h, Cin=Cin, Ni=num_input,
                  amps_val_init=amps_val_init, lambdas_val_init=lambdas_val_init, lambda_covs_val_init=lambda_covs_val_init,
                  covs_val_init=covs_val_init, inv_covs_val_init=inv_covs_val_init, means_val_init=means_val_init, psis_val_init=psis_val_init,
                  PPCA=False, lock_psis=True, update_mode=update_mode, em_mode=em_mode,
                  rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        rs_val = mix._get_Rs(x=x, dat=input_dat)
        acts_val = mix._get_activities(x=x, dat=input_dat)

        naive_sample_from_model(lambdas=lambdas_val_init, means=means_val_init, rs=rs_val, z=acts_val,
                                 Ni=Nsample, Cin=Cin, H=H, W=W, K=K, h=h, w=w, output_dir=sample_dir)

    else:
        with load(param_file) as params:
            means_val_init = params['means_val']
            lambdas_val_init = params['lambdas_val']
            lambda_covs_val_init= params['lambda_covs_val']
            covs_val_init = params['covs_val']
            inv_covs_val_init = params['inv_covs_val']
            psis_val_init = params['psis_val']
            amps_val_init = params['amps_val']
            # betas_val_init = params['betas_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = d[0:20] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = CRM(x=x, dat=input_dat, K=K, M=M, W=W, H=H, w=w, h=h, Cin=Cin, Ni=num_input,
                  amps_val_init=amps_val_init, lambdas_val_init=lambdas_val_init, lambda_covs_val_init=lambda_covs_val_init,
                  covs_val_init=covs_val_init, inv_covs_val_init=inv_covs_val_init, means_val_init=means_val_init, psis_val_init=psis_val_init,
                  PPCA=False, lock_psis=True, update_mode=update_mode, em_mode=em_mode,
                  rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        print 'Start computing activations'

        acts_val = mix._get_activities(x=x, dat=input_dat)
        acts_val = acts_val[:,0,:]
        acts_val = np.reshape(acts_val, newshape=(K-1,num_input,Np))
        # acts_val = np.reshape(acts_val, newshape=(K-1,num_input,H-h+1, W-w+1))
        acts_val = acts_val.transpose((1,0,2))
        acts_mean = np.mean(acts_val, axis=(0,2))
        acts_var = np.var(acts_val, axis=(0,2))

        print 'Shape acts_mean'
        print np.shape(acts_mean)

        print 'Shape acts_var'
        print np.shape(acts_var)

        acts_val = (acts_val - acts_mean[None,:,None])/acts_var[None,:,None]
        acts_val = acts_val - np.min(acts_val, axis=(0,2))[None,:,None]
        max_acts_val = np.max(acts_val, axis=(0,2))

        print 'Shape of max_acts_val'
        print np.shape(max_acts_val)

        print 'Shape of acts_val'
        print np.shape(acts_val)

        print 'Done computing activations'

        max_indx = np.argmax(acts_val, axis=2)
        print 'Shape of max_indx'
        print np.shape(max_indx)

        for k in xrange(K-1):
            filter_dir = os.path.join(output_dir, 'attention_filter%i_each_image'%(k+1))
            if not os.path.exists(filter_dir):
                os.makedirs(filter_dir)

            for n in xrange(num_input):
                im_mat = d[n, 0, :, :]
                img = Image.fromarray(im_mat)
                draw = ImageDraw.Draw(img)
                x_indx = max_indx[n,k]%(W-w+1)
                y_indx = max_indx[n,k]/(W-w+1)
                outline = acts_val[n,k,max_indx[n,k]]/max_acts_val[k]
                draw.rectangle([(x_indx, y_indx),(x_indx + w - 1, y_indx + h - 1)], outline=outline)
                new_data = np.asarray(img)
                new_data = np.tile(new_data,(3,1,1))
                new_data = new_data.transpose((1,2,0))
                imshow(new_data)
                savefig(os.path.join(filter_dir, 'image%i.png'%(n+1)))
                close()

        del mix, x, acts_val, input_dat, max_indx, num_input

        # Find the max activation in all images
        x = T.tensor4('x')
        input_dat = d[0:50000] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = CRM(x=x, dat=input_dat, K=K, M=M, W=W, H=H, w=w, h=h, Cin=Cin, Ni=num_input,
                  amps_val_init=amps_val_init, lambdas_val_init=lambdas_val_init, lambda_covs_val_init=lambda_covs_val_init,
                  covs_val_init=covs_val_init, inv_covs_val_init=inv_covs_val_init, means_val_init=means_val_init, psis_val_init=psis_val_init,
                  PPCA=False, lock_psis=True, update_mode=update_mode, em_mode=em_mode,
                  rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        print 'Start computing activations'

        acts_val = mix._get_activities(x=x, dat=input_dat)
        acts_val = acts_val[:,0,:]
        acts_val = np.reshape(acts_val, newshape=(K-1,num_input,Np))
        # acts_val = np.reshape(acts_val, newshape=(K-1,num_input,H-h+1, W-w+1))
        acts_val = acts_val.transpose((1,0,2))
        acts_mean = np.mean(acts_val, axis=(0,2))
        acts_var = np.var(acts_val, axis=(0,2))

        print 'Shape acts_mean'
        print np.shape(acts_mean)

        print 'Shape acts_var'
        print np.shape(acts_var)

        acts_val = (acts_val - acts_mean[None,:,None])/acts_var[None,:,None]
        acts_val = acts_val - np.min(acts_val, axis=(0,2))[None,:,None]
        max_acts_val = np.max(acts_val, axis=(0,2))

        print 'Shape of max_acts_val'
        print np.shape(max_acts_val)

        print 'Shape of acts_val'
        print np.shape(acts_val)

        print 'Done computing activations'

        # Find the max activation in each image

        N_best_attn = 100
        for k in xrange(K-1):
            filter_dir = os.path.join(output_dir, 'attention_filter%i_whole_dataset'%(k+1))
            if not os.path.exists(filter_dir):
                os.makedirs(filter_dir)

            max_indices = np.unravel_index(acts_val[:,k,:].argsort(axis=None)[-N_best_attn:][::-1], acts_val[:,k,:].shape)
            r = max_indices[0]
            c = max_indices[1]

            new_img = Image.new('RGB', (8000, 6000))

            for n in xrange(N_best_attn):
                im_mat = d[r[n], 0, :, :]
                img = Image.fromarray(im_mat)
                draw = ImageDraw.Draw(img)
                x_indx = c[n]%(W-w+1)
                y_indx = c[n]/(W-w+1)
                outline = acts_val[r[n],k,c[n]]/max_acts_val[k]
                draw.rectangle([(x_indx, y_indx),(x_indx + w - 1, y_indx + h - 1)], outline=outline)
                new_data = np.asarray(img)
                new_data = np.tile(new_data,(3,1,1))
                new_data = new_data.transpose((1,2,0))
                imshow(new_data)
                savefig(os.path.join(filter_dir, 'attn%i.png'%(n+1)))
                close()
                this_img = Image.open(os.path.join(filter_dir, 'attn%i.png'%(n+1)))
                x_loc = (n%10)*800
                y_loc = (n/10)*600
                new_img.paste(this_img,(x_loc,y_loc))

            imshow(new_img)
            savefig(os.path.join(filter_dir, 'top_attn_for_filter_%i.pdf'%(k+1)))

        new_img = Image.new('RGB', (4000, 1200))
        print K
        for k in xrange(K-1):
            this_img = Image.open(os.path.join(output_dir, 'filter_%i.png'%(k+1)))
            x_loc = (k%5)*800
            y_loc = (k/5)*600
            new_img.paste(this_img,(x_loc,y_loc))

        imshow(new_img)
        axis('off')
        savefig(os.path.join(output_dir, 'all_filters.pdf'))

        new_img = Image.new('RGB', (4000, 1200))
        print K
        for k in xrange(K-1):
            this_img = Image.open(os.path.join(output_dir, 'mean_%i.png'%(k+1)))
            x_loc = (k%5)*800
            y_loc = (k/5)*600
            new_img.paste(this_img,(x_loc,y_loc))

        imshow(new_img)
        axis('off')
        savefig(os.path.join(output_dir, 'all_means.pdf'))

