__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

import os

from PIL import Image

import numpy as np

import theano

import pickle


# from guppy import hpy; h=hpy()


def plot_NLL(training_list, output_dir, legend_list, plot_name):
    num_training = len(training_list)
    epoch_vec = []
    NegLogLs_vec = []

    for training_name in training_list:
        file_name = os.path.join(training_name, 'for_Plotting.npz')
        val = np.load(file_name)
        epoch_vec.append(val['epoch_vec'])
        NegLogLs_vec.append(val['NegLogLs_vec'])


    fig = figure()
    ax = plt.subplot(111)
    color=iter(cm.rainbow(np.linspace(0,1,num_training)))
    for i in xrange(num_training):
        c=next(color)
        stop_indx = min(len(epoch_vec[i]), 600)
        ax.plot(epoch_vec[i][0:stop_indx], NegLogLs_vec[i][0:stop_indx], 'o-', linewidth=3, c=c, label=legend_list[i])
        legend(prop={'size':9})
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_linewidth(3)
        ax.spines['left'].set_linewidth(3)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_fontsize(14)
        ax.yaxis.label.set_fontsize(14)
        ax.set_title('Negative Log-Likelihood vs. Epoch', fontsize=14)
        xlabel('Epoch')
        ylabel('Negative Log-Likelihood (bit/pixel')
        ax.tick_params(axis='both', which='major', labelsize=14, length=8, width=2)
        ax.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
        ax.set_xticks(epoch_vec[i][0:stop_indx:50],minor=False)

    fig.savefig(os.path.join(output_dir, plot_name))
    close()

def plot_Parameters_Convergence(training_list, output_dir, legend_list, plot_name):
    num_training = len(training_list)
    epoch_vec = []
    dLC_vec=[]
    dMU_vec=[]

    for training_name in training_list:
        file_name = os.path.join(training_name, 'for_Plotting.npz')
        val = np.load(file_name)
        epoch_vec.append(val['epoch_vec'])
        if 'dLA_vec' in val.keys():
            dLC_vec.append(val['dLA_vec'])
        else:
            dLC_vec.append(val['dLC_vec'])
        dMU_vec.append(val['dMU_vec'])

    fig = figure()
    ax1 = plt.subplot(211)
    color=iter(cm.rainbow(np.linspace(0,1,num_training)))
    for i in xrange(num_training):
        c=next(color)
        stop_indx = min(len(epoch_vec[i]), 600)
        ax1.plot(epoch_vec[i][1:stop_indx], dLC_vec[i][0,0:stop_indx-1], 'o-', linewidth=3, c=c, label=legend_list[i])
        legend(prop={'size':10})
        ax1.spines['right'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        ax1.spines['bottom'].set_linewidth(3)
        ax1.spines['left'].set_linewidth(3)
        ax1.yaxis.set_ticks_position('left')
        ax1.xaxis.set_ticks_position('bottom')
        ax1.xaxis.label.set_fontsize(14)
        ax1.yaxis.label.set_fontsize(14)
        ax1.set_title('Relative Difference in Lambda Covariance vs. Epoch', fontsize=14)
        xlabel('Epoch')
        ylabel('Relative Difference')
        ax1.tick_params(axis='both', which='major', labelsize=14, length=8, width=2)
        ax1.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
        ax1.set_xticks(epoch_vec[i][0:stop_indx:50],minor=False)

    ax2 = plt.subplot(212)
    color=iter(cm.rainbow(np.linspace(0,1,num_training)))
    for i in xrange(num_training):
        c=next(color)
        stop_indx = min(len(epoch_vec[i]), 200)
        ax2.plot(epoch_vec[i][1:stop_indx], dMU_vec[i][0,0:stop_indx-1], 'o-', linewidth=3, c=c, label=legend_list[i])
        legend(prop={'size':10})
        ax2.spines['right'].set_visible(False)
        ax2.spines['top'].set_visible(False)
        ax2.spines['bottom'].set_linewidth(3)
        ax2.spines['left'].set_linewidth(3)
        ax2.yaxis.set_ticks_position('left')
        ax2.xaxis.set_ticks_position('bottom')
        ax2.xaxis.label.set_fontsize(14)
        ax2.yaxis.label.set_fontsize(14)
        ax2.set_title('Relative Difference in Means vs. Epoch', fontsize=14)
        xlabel('Epoch')
        ylabel('Relative Difference')
        ax2.tick_params(axis='both', which='major', labelsize=14, length=8, width=2)
        ax2.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
        ax2.set_xticks(epoch_vec[i][0:stop_indx:20],minor=False)

    fig.savefig(os.path.join(output_dir, plot_name))
    close()

def plot_Parameters_Convergence_No_Factor(training_list, output_dir, legend_list, plot_name):
    num_training = len(training_list)
    epoch_vec = []
    dLA_vec=[]

    for training_name in training_list:
        file_name = os.path.join(training_name, 'for_Plotting.npz')
        val = np.load(file_name)
        epoch_vec.append(val['epoch_vec'])
        dLA_vec.append(val['dLA_vec'])

    fig = figure()
    ax1 = plt.subplot(111)
    color=iter(cm.rainbow(np.linspace(0,1,num_training)))
    for i in xrange(num_training):
        c=next(color)
        stop_indx = min(len(epoch_vec[i]), 600)
        ax1.plot(epoch_vec[i][1:stop_indx], dLA_vec[i][0,0:stop_indx-1], 'o-', linewidth=3, c=c, label=legend_list[i])
        legend(prop={'size':9})
        ax1.spines['right'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        ax1.spines['bottom'].set_linewidth(3)
        ax1.spines['left'].set_linewidth(3)
        ax1.yaxis.set_ticks_position('left')
        ax1.xaxis.set_ticks_position('bottom')
        ax1.xaxis.label.set_fontsize(14)
        ax1.yaxis.label.set_fontsize(14)
        ax1.set_title('Relative Difference in Lambda vs. Epoch', fontsize=14)
        xlabel('Epoch')
        ylabel('Relative Difference')
        ax1.tick_params(axis='both', which='major', labelsize=14, length=8, width=2)
        ax1.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
        ax1.set_xticks(epoch_vec[i][0:stop_indx:50],minor=False)

    fig.savefig(os.path.join(output_dir, plot_name))
    close()

def plot_error(training_list, output_dir, legend_list, plot_name, y_uplim=0.1, old_indx=[], convnet_indx=[], max_indx=200, indx_step=20, legend_size=18):
    num_training = len(training_list)
    epoch_vec = []
    test_score_vec = []

    for training_name in training_list:
        file_name = os.path.join(training_name, 'plot_classification.npz')
        val = np.load(file_name)
        epoch_vec.append(val['epoch_vec'])
        test_score_vec.append(val['test_score_vec'])
        print training_name
        print 'Best Test Score Error'
        print np.min(val['test_score_vec'])
        print 'Best Epoch'
        print np.argmin(val['test_score_vec']) + 1
        # print 'Min epoch'
        # print np.min(val['epoch_vec'])
        # print 'Converge at Epochs'
        # print (x for x in val['epoch_vec'] if val['test_score_vec'][x] < 0.015).next() + 1


    fig = figure()
    ax = plt.subplot(111)
    color=iter(cm.rainbow(np.linspace(0,1,num_training)))
    for i in xrange(num_training):
        c=next(color)
        if epoch_vec[i][0] == 0:
            stop_indx = min(len(epoch_vec[i]), max_indx)
            if i in convnet_indx:
                ax.plot(epoch_vec[i][1:stop_indx], test_score_vec[i][0:stop_indx-1], '--', markersize=2, linewidth=5, c=c, label=legend_list[i])
            elif i in old_indx:
                ax.plot(epoch_vec[i][1:stop_indx], test_score_vec[i][0:stop_indx-1], '-', markersize=2, linewidth=5, c=c, label=legend_list[i])
            else:
                ax.plot(epoch_vec[i][1:stop_indx], test_score_vec[i][0:stop_indx-1], '-', markersize=2, linewidth=5, c=c, label=legend_list[i])

        else:
            stop_indx = min(len(epoch_vec[i]), max_indx)
            if i in convnet_indx:
                ax.plot(epoch_vec[i][0:stop_indx], test_score_vec[i][0:stop_indx], '--', markersize=2, linewidth=5, c=c, label=legend_list[i])
            elif i in old_indx:
                ax.plot(epoch_vec[i][0:stop_indx], test_score_vec[i][0:stop_indx], '-', markersize=2, linewidth=5, c=c, label=legend_list[i])
            else:
                ax.plot(epoch_vec[i][0:stop_indx], test_score_vec[i][0:stop_indx], '-', markersize=2, linewidth=5, c=c, label=legend_list[i])


        legend(prop={'size':legend_size})
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_linewidth(3)
        ax.spines['left'].set_linewidth(3)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_fontsize(24)
        ax.yaxis.label.set_fontsize(24)
        #ax.set_title('Error vs. Epoch', fontsize=24)
        xlabel('Epoch')
        ylabel('Test Error')
        ax.tick_params(axis='both', which='major', labelsize=20, length=8, width=2)
        ax.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
        ylim([0, y_uplim])
        ax.set_xticks(epoch_vec[i][0:stop_indx:indx_step],minor=False)

    fig.savefig(os.path.join(output_dir, plot_name))
    close()

def plot_betas_lambdas(param_dir, output_dir, filter_shape):
    pkl_file = open(param_dir, 'rb')
    params = pickle.load(pkl_file)
    all_lambdas_val = params['lambdas_val']
    all_betas_val = params['betas_val']
    pkl_file.close()

    num_layer  = len(filter_shape)
    print(num_layer)
    output_dir = os.path.join(output_dir, 'betas_lambdas')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i in xrange(num_layer):
        lambdas_val = all_lambdas_val[i]
        betas_val = all_betas_val[i]

        K = np.shape(lambdas_val)[0]
        Cin, h, w = filter_shape[i]

        nrows = int(np.floor(K/8.0))
        new_img_betas = Image.new('RGB', (6400, 600*nrows))
        new_img_lambdas = Image.new('RGB', (6400, 600*nrows))

        for k in xrange(0, K):
            betas_mat = np.reshape(betas_val[k-1,0,:], (Cin, h, w))
            betas_mat = betas_mat.transpose((1,2,0))
            fig = figure()
            if Cin == 1:
                imshow(betas_mat[:,:,0], cmap = cm.Greys_r)
            elif Cin == 3:
                imshow(betas_mat)
            else:
                betas_mat = np.mean(betas_mat, axis=2)
                imshow(betas_mat, cmap = cm.Greys_r)

            axis('off')
            savefig(os.path.join(output_dir, 'filter_%i_at_layer_%i.png' %(k,num_layer - i)))
            close()
            this_img_filter = Image.open(os.path.join(output_dir, 'filter_%i_at_layer_%i.png'%(k,num_layer - i)))
            x_loc = (k%8)*800
            y_loc = (k/8)*600
            new_img_betas.paste(this_img_filter,(x_loc,y_loc))

            lambda_mat = np.reshape(lambdas_val[k], (Cin, h, w))
            lambda_mat = lambda_mat.transpose((1,2,0))
            fig = figure()
            if Cin == 1:
                imshow(lambda_mat[:,:,0], cmap = cm.Greys_r)
            elif Cin == 3:
                imshow(lambda_mat)
            else:
                lambda_mat = np.mean(lambda_mat, axis=2)
                imshow(lambda_mat, cmap = cm.Greys_r)

            axis('off')
            savefig(os.path.join(output_dir, 'lambda_%i_at_layer_%i.png' %(k,num_layer - i)))
            close()
            this_img_lambda = Image.open(os.path.join(output_dir, 'lambda_%i_at_layer_%i.png' %(k,num_layer - i)))
            x_loc = (k%8)*800
            y_loc = (k/8)*600
            new_img_lambdas.paste(this_img_lambda,(x_loc,y_loc))

        imshow(new_img_betas)
        axis('off')
        savefig(os.path.join(output_dir, 'all_filters_at_layer_%i.pdf'%(num_layer - i)))
        close()

        imshow(new_img_lambdas)
        axis('off')
        savefig(os.path.join(output_dir, 'all_lambdas_at_layer_%i.pdf'%(num_layer - i)))
        close()

def plot_betas_means_lambdas(param_dir, output_dir, filter_shape):
    with load(param_dir) as params:
        all_means_val = params['means_val']
        all_lambdas_val = params['lambdas_val']
        all_betas_val = params['betas_val']

    num_layer  = len(filter_shape)
    print num_layer
    output_dir = os.path.join(output_dir, 'betas_means_lambdas')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i in xrange(num_layer):
        means_val = all_means_val[i]
        lambdas_val = all_lambdas_val[i]
        betas_val = all_betas_val[i]

        K = np.shape(means_val)[0]
        Cin, h, w = filter_shape[i]
        print K
        nrows = int(np.floor(K/5.0))
        new_img_betas = Image.new('RGB', (4000, 600*nrows))
        new_img_lambdas = Image.new('RGB', (4000, 600*nrows))
        new_img_means = Image.new('RGB', (4000, 600*nrows))

        for k in xrange(0, K):
            betas_mat = np.reshape(betas_val[k-1,0,:], (Cin, h, w))
            betas_mat = betas_mat.transpose((1,2,0))
            fig = figure()
            if Cin == 1:
                imshow(betas_mat[:,:,0], cmap = cm.Greys_r)
            else:
                betas_mat = np.mean(betas_mat, axis=2)
                imshow(betas_mat, cmap = cm.Greys_r)

            axis('off')
            savefig(os.path.join(output_dir, 'filter_%i_at_layer_%i.png' %(k,i+1)))
            close()
            this_img_filter = Image.open(os.path.join(output_dir, 'filter_%i_at_layer_%i.png'%(k,i+1)))
            x_loc = (k%5)*800
            y_loc = (k/5)*600
            new_img_betas.paste(this_img_filter,(x_loc,y_loc))

            lambda_mat = np.reshape(lambdas_val[k], (Cin, h, w))
            lambda_mat = lambda_mat.transpose((1,2,0))
            fig = figure()
            if Cin == 1:
                imshow(lambda_mat[:,:,0], cmap = cm.Greys_r)
            else:
                lambda_mat = np.mean(lambda_mat, axis=2)
                imshow(lambda_mat, cmap = cm.Greys_r)

            axis('off')
            savefig(os.path.join(output_dir, 'lambda_%i_at_layer_%i.png' %(k,i+1)))
            close()
            this_img_lambda = Image.open(os.path.join(output_dir, 'lambda_%i_at_layer_%i.png' %(k,i+1)))
            x_loc = (k%5)*800
            y_loc = (k/5)*600
            new_img_lambdas.paste(this_img_lambda,(x_loc,y_loc))

            mean_mat = np.reshape(means_val[k], (Cin, h, w))
            mean_mat = mean_mat.transpose((1,2,0))
            fig = figure()
            if Cin == 1:
                imshow(mean_mat[:,:,0], cmap = cm.Greys_r)
            else:
                mean_mat = np.mean(mean_mat, axis=2)
                imshow(mean_mat, cmap = cm.Greys_r)

            axis('off')
            savefig(os.path.join(output_dir, 'mean_%i_at_layer_%i.png' %(k,i+1)))
            close()
            this_img_mean = Image.open(os.path.join(output_dir, 'mean_%i_at_layer_%i.png' %(k,i+1)))
            x_loc = (k%5)*800
            y_loc = (k/5)*600
            new_img_means.paste(this_img_mean,(x_loc,y_loc))

        imshow(new_img_betas)
        axis('off')
        savefig(os.path.join(output_dir, 'all_filters_at_layer_%i.pdf'%(i+1)))
        close()

        imshow(new_img_lambdas)
        axis('off')
        savefig(os.path.join(output_dir, 'all_lambdas_at_layer_%i.pdf'%(i+1)))
        close()

        imshow(new_img_means)
        axis('off')
        savefig(os.path.join(output_dir, 'all_means_at_layer_%i.pdf'%(i+1)))
        close()

def plot_filter_DCN(param_dir, output_dir, filter_shape):
    with load(param_dir) as params:
        all_W = params['W']
        all_n = params['b']

    num_layer  = len(all_W)
    print num_layer
    output_dir = os.path.join(output_dir, 'filter')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i in xrange(num_layer):
        W = all_W[i]

        K = np.shape(W)[0]
        Cin, h, w = filter_shape[i]
        print K
        nrows = int(np.floor(K/5.0))
        new_img_W = Image.new('RGB', (4000, 600*nrows))

        for k in xrange(0, K):
            W_mat = np.reshape(W[k], (Cin, h, w))
            W_mat = W_mat.transpose((1,2,0))
            fig = figure()
            if Cin == 1:
                imshow(W_mat[:,:,0], cmap = cm.Greys_r)
            else:
                W_mat = np.mean(W_mat, axis=2)
                imshow(W_mat, cmap = cm.Greys_r)

            axis('off')
            savefig(os.path.join(output_dir, 'filter_%i_at_layer_%i.png' %(k,i+1)))
            close()
            this_img_filter = Image.open(os.path.join(output_dir, 'filter_%i_at_layer_%i.png'%(k,i+1)))
            x_loc = (k%5)*800
            y_loc = (k/5)*600
            new_img_W.paste(this_img_filter,(x_loc,y_loc))


        imshow(new_img_W)
        axis('off')
        savefig(os.path.join(output_dir, 'all_filters_at_layer_%i.pdf'%(i+1)))
        close()

def reconstruct_from_model(reconst_dir, model, dat, Ni):
    getReconstruction = theano.function([model.x], model.conv1.data_reconstructed)
    I_hat = getReconstruction(dat)
    I_hat = I_hat.transpose((0,2,3,1))

    for i in xrange(Ni):
        imshow(I_hat[i,:,:,0], cmap='gray')
        axis('off')
        out_path = os.path.join(reconst_dir, 'ReconstructedImage_%i.jpg'%i)
        savefig(out_path)
        close()

    sub_x_size = int(sqrt(Ni))
    sub_y_size = int(sqrt(Ni))

    img = Image.new('RGB', (800*sub_x_size, 600*sub_y_size))
    for i in xrange(Ni):
        this_img = Image.open(os.path.join(reconst_dir, 'ReconstructedImage_%i.jpg'%i))
        x_loc = (i%sub_x_size)*800
        y_loc = (i/sub_y_size)*600
        img.paste(this_img,(x_loc,y_loc))

    fig = imshow(img)
    axis('off')
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    savefig(os.path.join(reconst_dir, 'ReconstructedImages.pdf'))
    close()
    print 'Done'
    return None

def sample_from_model(sample_dir, model, Ni, z, rs):
    getSample = theano.function([model.z, model.rs], model.conv1.data_sampled)
    I_hat = getSample(z, rs)
    I_hat = I_hat.transpose((0,2,3,1))

    for i in xrange(Ni):
        imshow(I_hat[i,:,:,0], cmap='gray')
        axis('off')
        out_path = os.path.join(sample_dir, 'SampledImage_%i.jpg'%i)
        savefig(out_path)
        close()

    sub_x_size = int(sqrt(Ni))
    sub_y_size = int(sqrt(Ni))

    img = Image.new('RGB', (800*sub_x_size, 600*sub_y_size))
    for i in xrange(Ni):
        this_img = Image.open(os.path.join(sample_dir, 'SampledImage_%i.jpg'%i))
        x_loc = (i%sub_x_size)*800
        y_loc = (i/sub_y_size)*600
        img.paste(this_img,(x_loc,y_loc))

    fig = imshow(img)
    axis('off')
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    savefig(os.path.join(sample_dir, 'SampledImages.pdf'))
    close()
    print 'Done'
    return None


if __name__ == '__main__':
    training_root = '/Volumes/TanNguyen/temp_results/'
    # data_dir = '/Users/heatherseeba/repos/em_drm/data/mnist.pkl.gz'
    data_dir = '/Users/minhtannguyen/repos/em_drm/data/mnist.pkl.gz'

    # # Reconstruction code
    # model_dir_name = 'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616'
    # model_dir = os.path.join(training_root, model_dir_name)
    #
    # param_in_use = 'EM_results_epoch_218.npz'
    # param_dir = os.path.join(model_dir, 'params', param_in_use)
    #
    # em_mode = 'hard'
    # use_non_lin = 'None'
    #
    # Cin = 1 # depth of filters
    # H = 28 # width of input images
    # W = 28 # height of input images
    # Ni = 16 # no. of reconstructed samples
    # K = 20
    # h = 5
    # w = 5

    # # Reconstructed Images
    # reconst_dir = os.path.join(model_dir, 'reconstructed_images')
    # if not os.path.exists(reconst_dir):
    #     os.makedirs(reconst_dir)
    #
    # model = Reconstruct_Samples_SRM(batch_size=Ni, Cin=Cin, W=W, H=H, em_mode=em_mode,
    #                                 use_non_lin=use_non_lin, param_dir=param_dir)
    #
    # mnist = DATA(dataset_name='MNIST', data_mode='all', data_dir=data_dir, Nlabeled=Ni, Ni=Ni, Cin=Cin, H=H, W=W, seed=5)
    #
    # dat = mnist.dtrain
    #
    # reconstruct_from_model(reconst_dir=reconst_dir, model=model, dat=dat, Ni=Ni)
    #
    # # Sampled Images
    # sample_dir = os.path.join(model_dir, 'sampled_images')
    # if not os.path.exists(sample_dir):
    #     os.makedirs(sample_dir)
    #
    # model = Sample_from_SRM(batch_size=Ni, Cin=Cin, W=W, H=H, em_mode=em_mode,
    #                                 use_non_lin=use_non_lin, param_dir=param_dir)
    #
    # z_val = np.asarray(np.random.randn(Ni, K, (H-h+1)/2, (W-w+1)/2), dtype=np.float32)
    # print 'Shape of z_val'
    # print np.shape(z_val)
    #
    # A = T.tensor4('A')
    # rs = T.grad(T.sum(T.max(pool.pool_2d(input=A, ds=(2,2), ignore_border=True, mode='max'), axis=1)), wrt=A)
    # getrs = theano.function([A], rs)
    #
    # rs_val = getrs(np.asarray(np.random.randn(Ni, K, H-h+1, W-w+1), dtype=np.float32))
    #
    # print 'Shape of rs_val'
    # print np.shape(rs_val)
    # print rs_val
    #
    # I_hat = sample_from_model(sample_dir=sample_dir, model=model, Ni=Ni, z=z_val, rs=rs_val)

    # training_names = ['DRM/DRM_EG_unsupervised_2_layers_5x5x20_3x3x50_b125_hard_max_t_TopDown_lr_0_01_050816',
    #                   'DRM/DRM_EG_unsupervised_2_layers_5x5x20_3x3x50_b125_hard_max_t_TopDown_lr_0_001_050816',
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = ['b125_hard_max_t_TopDown_lr_0_01',
    #                'b125_hard_max_t_TopDown_lr_0_001']
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Unsupervised_Learning_DRM_2_layers_5x5x20_3x3x50_b125_051016.pdf')
    # plot_Parameters_Convergence(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Unsupervised_Learning_DRM_2_layers_5x5x20_3x3x50_b125_051016.pdf')

    #
    #
    # # training_names = ['SRM-EM-Coordinate-Descent-Spatial-Channel/EM_unsupervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_041316/classification_lr_0_0001_good_one',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EM_unsupervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_041316/classification_lr_0_001_good_one',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_unsupervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_041216/classification',
    # #
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EM_unsupervised_1_layer_5x5x20_b500_coor_descent_max_relu_041716/classification',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_unsupervised_1_layer_5x5x20_b500_coor_descent_max_relu_lr_0_1_041716/classification',
    # #
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_lr_0_0001_041716/classification',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_lr_0_001_041416/classification',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_lr_0_1_041316/classification',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_relu_lr_0_001_041716/classification',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_relu_lr_0_1_041716/classification',
    # #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/classification']
    # #
    # # legend_list = ['EM_unsupervised_noReLU+Softmax_lr_0_0001','EM_unsupervised_noReLU+Softmax_lr_0_001',
    # #                'EG_unsupervised_noReLU+Softmax_lr_0_001',
    # #                'EM_unsupervised_ReLU+Softmax_lr_0_001', 'EG_unsupervised_ReLU+Softmax_lr_0_001',
    # #
    # #                'EG_supervised_noReLU_lr_0_0001','EG_supervised_noReLU_lr_0_001','EG_supervised_noReLU_lr_0_1',
    # #                'EG_supervised_ReLU_lr_0_001', 'EG_supervised_ReLU_lr_0_1',
    # #                '1-layer-Convnet']
    #
    # # training_names = [
    # #                   # 'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_abs_no_relu_lr_0_001__new_impl_042816',
    # #                   # 'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_abs_no_relu_lr_0_0001__new_impl_042816',
    # #                   # 'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_Kx2x2_array_no_relu_lr_0_001_042816',
    # #                   'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_001_old_impl_050216/supervised_End_to_End_using_50K_data',
    # #                   'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_old_impl_050216/supervised_End_to_End_using_50K_data',
    # #                   'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_lr_0_1_051016/supervised_End_to_End_using_50K_data',
    # #                   'SRM/SRM_EG_supervised_5x5x20_b500_max_t_uniform_c_lr_0_1_051016/supervised_End_to_End_using_50K_data',
    # #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/classification']
    #
    # training_names = [
    #     # 'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_001_old_impl_050216/supervised_End_to_End_using_50K_data',
    #     #               'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_old_impl_050216/supervised_End_to_End_using_50K_data',
    #     #               'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_lr_0_1_051016/supervised_End_to_End_using_50K_data',
    #     #               'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_lr_0_01_051016/supervised_End_to_End_using_50K_data',
    #     #               'SRM/SRM_EG_supervised_5x5x20_b500_max_pool_latents_lr_0_1_051116/supervised_End_to_End_using_50K_data',
    #                   'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_new_send_rs_lr_0_1_051116/supervised_End_to_End_using_50K_data',
    #                   'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_max_a_new_send_rs_lr_0_1_051116/supervised_End_to_End_using_50K_data',
    #                 'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_new_sendz_lr_0_1_051316/supervised_End_to_End_using_50K_data',
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_No_ReLU_051116',
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316'
    # ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                # 'EG_supervised_max_c_sum_t_lr_0_001',
    #                # 'EG_supervised_max_c_sum_t_lr_0_01',
    #                # 'EG_supervised_soft_c_max_t_lr_0_1',
    #                # 'EG_supervised_soft_c_max_t_lr_0_01',
    #                # 'EG_supervised_max_pool_latents_lr_0_1',
    #                 'EG_supervised_soft_c_max_t_sentrs_lr_0_1',
    #     'EG_supervised_soft_c_max_t_max_a_sentrs_lr_0_1',
    #     'EG_supervised_soft_c_max_t_sentz_lr_0_1',
    #                '1-layer-Convnet-NoReLU',
    #     '1-layer-Convnet-WithReLU'
    #
    #                ]

    # legend_list = [
    #                # 'EG_supervised_max_c_lr_0_001',
    #                # 'EG_supervised_max_c_0_0001',
    #                # 'EG_supervised_max_c_max_t_lr_0_001',
    #                'EG_supervised_max_c_sum_t_lr_0_001',
    #                'EG_supervised_max_c_sum_t_lr_0_01',
    #                'EG_supervised_soft_c_max_t',
    #                'EG_supervised_uniform_c_max_t',
    #                '1-layer-Convnet'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_stable_051316.pdf')
    #
    # training_names = [
    #     # 'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_001_old_impl_050216/supervised_End_to_End_using_50K_data',
    #     #               'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_old_impl_050216/supervised_End_to_End_using_50K_data',
    #     #               'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_lr_0_1_051016/supervised_End_to_End_using_50K_data',
    #     #               'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_lr_0_01_051016/supervised_End_to_End_using_50K_data',
    #     #               'SRM/SRM_EG_supervised_5x5x20_b500_max_pool_latents_lr_0_1_051116/supervised_End_to_End_using_50K_data',
    #     # 'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_new_attemp_fix_Lambdas_lr_0_1_051316/supervised_End_to_End_using_50K_data',
    #     'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_new_lr_0_1_051316/supervised_End_to_End_using_50K_data',
    #     'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_max_a_new_lr_0_1_051116/supervised_End_to_End_using_50K_data',
    #     'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_old_impl_050216/supervised_End_to_End_using_50K_data',
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_No_ReLU_051116',
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316'
    # ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                # 'EG_supervised_max_c_sum_t_lr_0_001',
    #                # 'EG_supervised_max_c_sum_t_lr_0_01',
    #                # 'EG_supervised_soft_c_max_t_lr_0_1',
    #                # 'EG_supervised_soft_c_max_t_lr_0_01',
    #                # 'EG_supervised_max_pool_latents_lr_0_1',
    #     # 'EG_supervised_soft_c_max_t_fix_Lambda_lr_0_1',
    #     'EG_supervised_soft_c_max_t_lr_0_1',
    #     'EG_supervised_soft_c_max_t_max_a_lr_0_1',
    #     'EG_supervised_max_c_sum_t_lr_0_01',
    #                '1-layer-Convnet-NoReLU',
    #     '1-layer-Convnet-WithReLU'
    #
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_unstable_051316.pdf')
    #
    # training_names = [
    #                   'Debug_Results/Debug_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_new_impl_050316/classification',
    #                   'SRM/SRM_EG_unsupervised_5x5x20_b500_max_t_soft_c_new_lr_0_1_051116/classification_Softmax_Using_50K_data',
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_No_ReLU_051116',
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316']
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_max_c_sum_t',
    #                'EG_unsupervised_soft_c_max_t',
    #                '1-layer-Convnet-NoReLU',
    #     '1-layer-Convnet-WithReLU'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_unsupervised_051316.pdf')


    #
    # training_names = [
    #     # 'Debug_Results/Debug_EM_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_abs_no_relu_lr_042816/classification',
    #     #               'Debug_Results/Debug_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_abs_no_relu_lr_0_01_042816/classification',
    #     #               'Debug_Results/Debug_EM_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_Kx2x2_array_no_relu_042816/classification',
    #     #               'Debug_Results/Debug_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_Kx2x2_array_no_relu_042816/classification',
    #                   'Debug_Results/Debug_EM_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_new_impl_050316/classification',
    #                   'Debug_Results/Debug_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_new_impl_050316/classification',
    #                   'SRM/SRM_EG_unsupervised_5x5x20_b500_max_t_soft_c_lr_0_01_051016/classification_Softmax_Using_50K_data',
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/classification']
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     # 'EM_unsupervised_max_c',
    #     #            'EG_unsupervised_max_c',
    #     #            'EM_unsupervised_max_c_max_t',
    #                'EG_unsupervised_max_c_max_t',
    #                'EM_unsupervised_max_c_sum_t',
    #                'EG_unsupervised_max_c_sum_t',
    #                'EG_unsupervised_soft_c_max_t',
    #                '1-layer-Convnet'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_Unsupervised_051116.pdf')
    #
    # training_names = ['Debug_Results/Debug_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_abs_no_relu_lr_0_01_042816/classification_Softmax_1K_data',
    #                   'Debug_Results/Debug_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_abs_no_relu_lr_0_01_042816/classification_Softmax_Using_50K_data',
    #                   # 'Debug_Results/Debug_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_abs_no_relu_lr_0_01_042816/supervised_finetune_End_to_End_using_1K_data',
    #                   # 'Debug_Results/Debug_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_abs_no_relu_lr_0_01_042816/supervised_finetune_End_to_End_using_50K_data',
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/classification']
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = ['EG_unsupervised_max_c + FinetuneSoftmax_Using_1K_Data',
    #                'EG_unsupervised_max_c + FinetuneSoftmax_Using_50K_Data',
    #                # 'EG_unsupervised_max_c + FinetuneEndtoEnd_Using_1K_Data',
    #                # 'EG_unsupervised_max_c + FinetuneEndtoEnd_Using_50K_Data',
    #                '1-layer-Convnet'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_Semisupervised_050716.pdf')
    #
    # training_names = [
    #                   'Debug_Results/Debug_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_new_impl_050316/classification',
    #                   'Debug_Results/Debug_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_old_impl_050216/supervised_End_to_End_using_50K_data',
    #                   'SRM/SRM_EG_unsupervised_5x5x20_b500_max_t_soft_c_lr_0_01_051016/classification_Softmax_Using_50K_data',
    #                   'SRM/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_lr_0_1_051016/supervised_End_to_End_using_50K_data',
    #                   'SRM/SRM_EG_supervised_5x5x20_b500_max_t_uniform_c_lr_0_1_051016/supervised_End_to_End_using_50K_data',
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/classification']
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_max_c_sum_t',
    #                'EG_supervised_max_c_sum_t',
    #                'EG_unsupervised_soft_c_max_t',
    #                'EG_supervised_soft_c_max_t',
    #                'EG_supervised_uniform_c_max_t',
    #                '1-layer-Convnet'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_for_MICrONS_051116.pdf')
    #
    # training_names = [
    #                   'DRM/DRM_EG_supervised_2_layers_5x5x20_5x5x50_b125_hard_max_t_max_a_soft_c_lr_0_01_050716/supervised_End_to_End_using_50K_data',
    #                   'DRM/DRM_EG_supervised_2_layers_5x5x20_5x5x50_b125_hard_max_t_max_a_soft_c_lr_0_001_050716/supervised_End_to_End_using_50K_data',
    #                   'DRM/DRM_EG_supervised_2_layers_5x5x20_5x5x50_b125_hard_max_t_soft_c_lr_0_01_050716/supervised_End_to_End_using_50K_data',
    #                   'DRM/DRM_EG_supervised_2_layers_5x5x20_5x5x50_b125_hard_max_t_soft_c_lr_0_001_050716/supervised_End_to_End_using_50K_data',
    #                   'DRM/DRM_EG_supervised_2_layers_5x5x20_5x5x50_b125_hard_max_t_soft_c_ReLU_lr_0_01_050716/supervised_End_to_End_using_50K_data',
    #                   'DRM/DRM_EG_supervised_2_layers_5x5x20_5x5x50_b125_hard_max_t_soft_c_ReLU_lr_0_001_050716/supervised_End_to_End_using_50K_data',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_125/classification',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_125_noReLU_041416/classification']
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'max_t_max_a_soft_c_lr_0_01',
    #                'max_t_max_a_soft_c_lr_0_001',
    #                'max_t_soft_c_lr_0_01',
    #                'max_t_soft_c_lr_0_001',
    #                'max_t_soft_c_ReLU',
    #                'max_t_soft_c_ReLU',
    #                '2-layer-Convnet',
    #                '2-layer-Convnet-noReLU'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_supervised_2_layers_5x5x20_5x5x50_b125_051016.pdf')
    # # training_names = ['SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_lr_0_0001_041716/classification',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_lr_0_001_041416/classification',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_lr_0_1_041316/classification',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_relu_lr_0_001_041716/classification',
    # #                   'SRM-EM-Coordinate-Descent-Spatial-Channel/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_relu_lr_0_1_041716/classification',
    # #                   'SRM-EM-Coordinate-Descent-Channel-Spatial/EG_supervised_1_layer_5x5x20_b500_coor_descent_channel_sptial_max_no_relu_lr_0_1_041716/classification',
    # #                   'SRM-EM-Coordinate-Descent-Channel-Spatial/EG_supervised_1_layer_5x5x20_b500_coor_descent_channel_sptial_max_no_relu_lr_0_001_041716/classification',
    # #                   'SRM-EM-Coordinate-Descent-Channel-Spatial/EG_supervised_1_layer_5x5x20_b500_coor_descent_channel_sptial_max_relu_lr_0_1_041816/classification',
    # #                   'SRM-EM-Coordinate-Descent-Channel-Spatial/EG_supervised_1_layer_5x5x20_b500_coor_descent_channel_sptial_max_relu_lr_0_001_041816/classification',
    # #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/classification', ]
    # #
    # # training_list = []
    # # for name in training_names:
    # #     training_list.append(os.path.join(training_root, name))
    # #
    # # legend_list = ['EG_Coordinate_Descent_Spatial_Channel_supervised_noReLU_lr_0_0001','EG_Coordinate_Descent_Spatial_Channel_supervised_noReLU_lr_0_001','EG__Coordinate_Descent_Spatial_Channel_supervised_noReLU_lr_0_1',
    # #                'EG_Coordinate_Descent_Spatial_Channel_supervised_ReLU_lr_0_001', 'EG_Coordinate_Descent_Spatial_Channel_supervised_ReLU_lr_0_1',
    # #                'EG_Coordinate_Descent_Channel_Spatial_supervised_noReLU_lr_0_1','EG_Coordinate_Descent_Channel_Spatial_supervised_noReLU_lr_0_001',
    # #                'EG_Coordinate_Descent_Channel_Spatial_supervised_ReLU_lr_0_1','EG_Coordinate_Descent_Channel_Spatial_supervised_ReLU_lr_0_001',
    # #                '1-layer-Convnet']
    # # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_Supervised_End_to_End.pdf')
    # #
    # training_names = ['EG_unsupervised_1_layer_5x5x20_coor_descent_sptial_channel_max_no_relu_n1000_nobatch_lr_0_1_041816/classification',
    #                   'EM_unsupervised_1_layer_5x5x20_coor_descent_sptial_channel_max_no_relu_n1000_nobatch_041816/classification',
    #
    #                   'LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/classification']
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = ['EG-1000datapoint','EM-1000datapoint',
    #                '1-layer-Convnet']
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_Unsupervised+Softmax_Compare_EM_EG.pdf')
    #
    # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_lr_0_001_041416/params/EM_results_epoch_30.npz'
    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/EG_supervised_1_layer_5x5x20_b500_coor_descent_max_no_relu_lr_0_001_041416'
    # filter_shape = [(1,5,5),]
    #
    # plot_betas_means_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    #
    # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/params/LeNet5_results_epoch_200.npz'
    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316'
    # filter_shape = [(1,5,5),]
    # plot_filter_DCN(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)








    # # supervised training
    # training_names = [
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_old_impl_050216/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_sendrs_use50000labeleddata_lr_1_051416/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_max_t_soft_c_ReLUonz_sendrs_lr_1_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_max_t_soft_c_max_a_sendrs_lr_1_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_max_t_soft_c_max_a_sendrs_lr_0_5_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_max_t_soft_c_max_a_sendrs_lr_0_5_init_1000_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_soft_t_soft_c_sendrs_lr_1_init_1000_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_No_ReLU_051116',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316'
    # ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = ['EG_max_c_sum_t',
    #                 'EG_supervised_soft_c_max_t_sentrs_lr_1_use50000labeleddata',
    #                'EG_supervised_soft_c_max_t_ReLUonz_sentrs_lr_1_use50000labeleddata',
    #                 'EG_supervised_soft_c_max_t_max_a_sentrs_lr_1_use50000labeleddata_init1000',
    #                'EG_supervised_soft_c_max_t_max_a_sentrs_lr_0_5_use50000labeleddata_init100',
    #                'EG_supervised_soft_c_max_t_max_a_sentrs_lr_0_5_use50000labeleddata_init1000',
    #                'EG_supervised_soft_c_soft_t_sentrs_lr_1_use50000labeleddata_init1000',
    #                'EG_supervised_soft_c_soft_t_max_a_sentrs_lr_1_use50000labeleddata_init1000',
    #                '1-layer-Convnet-NoReLU_use50000labeleddata',
    #     '1-layer-Convnet-withReLU_use50000labeleddata'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_use50000labeleddata_051516.pdf')
    #
    # # unsupervised training
    # training_names = [
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_1_layer_5x5x20_b500_hard_no_noise_max_over_channel_then_sum_over_latents_no_relu_lr_0_01_new_impl_050316/classification',
    #                   'SRM/SRM_EG_unsupervised_5x5x20_b500_max_t_soft_c_new_lr_0_1_051116/classification_Softmax_Using_50K_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_max_t_soft_c_lr_1_051416/classification_Softmax_Using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_No_ReLU_051116',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316']
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_max_c_sum_t',
    #                'EG_unsupervised_soft_c_max_t_sendrsz',
    #                'EG_unsupervised_soft_c_max_t_sendrs',
    #                '1-layer-Convnet-NoReLU',
    #     '1-layer-Convnet-WithReLU'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_unsupervised_use50000labeleddata_051516.pdf')
    #
    # # semisupervised training
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_0_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_0_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_2_0_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_2_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_3_0_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316',
    #                   #'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use1000labeleddata_with_ReLU_051616'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'Semisupervised 1000labeleddata 50000unlabeleddata_soft_t_soft_c_max_a_lr_0_5',
    #                'Semisupervised 1000labeleddata 50000unlabeleddata_soft_t_soft_c_max_a_lr_1_0',
    #                'Semisupervised 1000labeleddata 50000unlabeleddata_soft_t_soft_c_max_a_lr_1_5',
    #                'Semisupervised 1000labeleddata 50000unlabeleddata_soft_t_soft_c_max_a_lr_2_0',
    #                'Semisupervised 1000labeleddata 50000unlabeleddata_soft_t_soft_c_max_a_lr_2_5',
    #                'Semisupervised 1000labeleddata 50000unlabeleddata_soft_t_soft_c_max_a_lr_3_0',
    #     '1-layer-Convnet-WithReLU-50000data',
    #     #'1-layer-Convnet-WithReLU-1000data'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_CrossValid_semisupervised_use10000labeleddata_use50000unlabeleddata_051516.pdf')
    #
    # # semisupervised training
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use100labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_0_5_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use100labeleddata_50000unlabeleddata_soft_t_soft_c_sendrs_lr_0_5_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use100labeleddata_50000unlabeleddata_max_t_soft_c_max_a_sendrs_lr_0_5_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use100labeleddata_50000unlabeleddata_max_t_soft_c_sendrs_lr_0_5_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use100labeleddata_with_ReLU_051616',
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'SRM soft t soft c max a',
    #     'SRM soft t soft c',
    #     'SRM max t soft c max a',
    #     'SRM max t soft c',
    #     '1-layer ReLU Convnet 50000 labels',
    #     '1-layer ReLU Convnet 100 labels',
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_semisupervised_use100labeleddata_use50000unlabeleddata_051516.pdf')
    #
    # # semisupervised training
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b600_use600labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b600_use600labeleddata_50000unlabeleddata_soft_t_soft_c_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b600_use600labeleddata_50000unlabeleddata_max_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b600_use600labeleddata_50000unlabeleddata_max_t_soft_c_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use600labeleddata_with_ReLU_051616'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'SRM soft t soft c max a',
    #     'SRM soft t soft c',
    #     'SRM max t soft c max a',
    #     'SRM max t soft c',
    #     '1-layer ReLU Convnet 50000 labels',
    #     '1-layer ReLU Convnet 600 labels'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_semisupervised_use600labeleddata_use50000unlabeleddata_051516.pdf')
    #
    #  # semisupervised training
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_max_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_max_t_soft_c_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use1000labeleddata_with_ReLU_051616'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'SRM soft t soft c max a',
    #     'SRM soft t soft c',
    #     'SRM max t soft c max a',
    #     'SRM max t soft c',
    #     '1-layer ReLU Convnet 50000 labels',
    #     '1-layer ReLU Convnet 1000 labels'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_semisupervised_use1000labeleddata_use50000unlabeleddata_051516.pdf')
    #
    # # semisupervised training
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use3000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use3000labeleddata_50000unlabeleddata_soft_t_soft_c_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use3000labeleddata_50000unlabeleddata_max_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use3000labeleddata_50000unlabeleddata_max_t_soft_c_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use3000labeleddata_with_ReLU_051616'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'SRM soft t soft c max a',
    #     'SRM soft t soft c',
    #     'SRM max t soft c max a',
    #     'SRM max t soft c',
    #     '1-layer ReLU Convnet 50000 labels',
    #     '1-layer ReLU Convnet 3000 labels'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_semisupervised_use3000labeleddata_use50000unlabeleddata_051516.pdf')
    #
    # training_names = [
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/classification_Softmax_Using_100_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/classification_Softmax_Using_600_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/classification_Softmax_Using_1000_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/classification_Softmax_Using_3000_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/classification_Softmax_Using_50000_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/Finetune_End_to_End_using_100_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/Finetune_End_to_End_using_600_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/Finetune_End_to_End_using_1000_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/Finetune_End_to_End_using_3000_data',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/Finetune_End_to_End_using_50000_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'Softmax - 100 labeled data',
    #     'Softmax - 600 labeled data',
    #     'Softmax - 1000 labeled data',
    #     'Softmax - 3000 labeled data',
    #     'Softmax - 50000 labeled data',
    #     'End-to-end - 100 labeled data',
    #     'End-to-end - 600 labeled data',
    #     'End-to-end - 1000 labeled data',
    #     'End-to-end - 3000 labeled data',
    #     'End-to-end - 50000 labeled data',
    #     'Supervised',
    #     '1-layer ReLU Convnet'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_unsupervised_finetune_50000unlabeleddata_051516.pdf')
    #
    # # semisupervised training (small # labeled data)
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_supervised_5x5x20_b500_use3000labeleddata_3000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use3000labeleddata_3000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use3000labeleddata_with_ReLU_051616'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'Supervised',
    #     'Semisupervised',
    #     '1-layer ReLU Convnet 3000 labels'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_semisupervised_use3000labeleddata_use3000unlabeleddata_051516.pdf')
    #
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_supervised_5x5x20_b500_use1000labeleddata_1000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_1000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use1000labeleddata_with_ReLU_051616'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'Supervised',
    #     'Semisupervised',
    #     '1-layer ReLU Convnet 1000 labels'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_semisupervised_use1000labeleddata_use1000unlabeleddata_051516.pdf')
    #
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_supervised_5x5x20_b600_use600labeleddata_600unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b600_use600labeleddata_600unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use600labeleddata_with_ReLU_051616'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #             'Supervised',
    #             'Semisupervised',
    #             '1-layer ReLU Convnet 600 labels'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_semisupervised_use600labeleddata_use600unlabeleddata_051516.pdf')
    #
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_supervised_5x5x20_b100_use100labeleddata_100unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_0_5_init_1000_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b100_use100labeleddata_100unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_0_5_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use100labeleddata_with_ReLU_051616'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'Supervised',
    #     'Semisupervised',
    #     '1-layer ReLU Convnet 100 labels'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_semisupervised_use100labeleddata_use100unlabeleddata_051516.pdf')
    #
    # training_names = [
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use50000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'Supervised',
    #     'Semisupervised',
    #     '1-layer ReLU Convnet'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_semisupervised_use50000labeleddata_use50000unlabeleddata_051516.pdf')
    #
    # training_names = [
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/Finetune_End_to_End_using_50000_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use50000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'Pretraining + Finetune End-to-End',
    #     'Supervised',
    #     'Semisupervised',
    #     '1-layer Convnet'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_semisupervised_2stage_use50000labeleddata_use50000unlabeleddata_051516.pdf')


    # # supervised training
    # training_names = [
    #                   'NIPS/DRM/supervised/DRM_EG_supervised_5x5x20_5x5x50_b125_50000labeleddata_max_t_soft_c_sendz_rs_lr_0_1_init_1000_051716/supervised_End_to_End_using_50K_data',
    #                 'NIPS/DRM/supervised/DRM_EG_supervised_5x5x20_5x5x50_b125_50000labeleddata_max_t_soft_c_sendmaxz_rs_lr_0_1_init_1000_051716/supervised_End_to_End_using_50K_data',
    #                 'NIPS/DRM/supervised/DRM_EG_supervised_5x5x20_5x5x50_b125_50000labeleddata_max_t_soft_c_sendavgz_rs_lr_0_1_init_1000_051716/supervised_End_to_End_using_50K_data',
    #                 'NIPS/DRM/supervised/DRM_EG_supervised_5x5x20_5x5x50_b125_50000labeleddata_max_t_soft_c_send_maxz_rs_lr_1_0_init_1000_051716/supervised_End_to_End_using_50K_data',
    #     'NIPS/DRM/supervised/DRM_EG_supervised_5x5x20_5x5x50_b125_50000labeleddata_max_t_soft_c_send_avgz_rs_lr_1_0_init_1000_051716/supervised_End_to_End_using_50K_data',
    #     'NIPS/DRM/supervised/DRM_EG_supervised_5x5x20_5x5x50_b125_50000labeleddata_soft_t_soft_c_send_maxz_rs_lr_1_0_init_1000_051716/supervised_End_to_End_using_50K_data',
    #     'NIPS/DRM/supervised/DRM_EG_supervised_5x5x20_5x5x50_b125_50000labeleddata_soft_t_soft_c_send_avgz_rs_lr_1_0_init_1000_051716/supervised_End_to_End_using_50K_data',
    #     'NIPS/DRM/supervised/DRM_EG_supervised_5x5x20_5x5x50_b125_50000labeleddata_soft_t_soft_c_sendzrs_rs_lr_1_0_init_1000_051716/supervised_End_to_End_using_50K_data',
    #     'NIPS/DRM/supervised/DRM_EG_supervised_5x5x20_5x5x50_b125_50000labeleddata_max_t_soft_c_sendmaxz_rs_lr_0_1_init_1000_051716/supervised_End_to_End_using_50K_data',
    #     'NIPS/DRM/supervised/EG_supervised_2_layer_5x5x20_3x3x50_b200_coor_descent_max_no_relu_lr_0_01_041416/classification',
    #                   'NIPS/LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_125_noReLU_041416/classification'
    # ]
    #
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = ['max_t_soft_c_send_z_rs_lr_0_1',
    #             'max_t_soft_c_send_maxz_rs_lr_0_1_init_1000',
    #                'max_t_soft_c_sendavgz_rs_lr_0_1',
    #                'max_t_soft_c_send_maxz_rs_lr_1_0',
    #                'max_t_soft_c_send_avgz_rs_lr_1_0',
    #                'soft_t_soft_c_send_maxz_rs_lr_1_0',
    #                'soft_t_soft_c_send_avgz_rs_lr_1_0',
    #                'soft_t_soft_c_sendzrs_rs',
    #                'max_t_soft_c_sendmaxz_rs_lr_0_1',
    #                'coor_descent_max_no_relu_lr_0_01',
    #                '2-layer-Convnet-NoReLU_use50000labeleddata',
    #     '2-layer-Convnet-withReLU_use50000labeleddata'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_supervised_use50000labeleddata_051516.pdf')

    # # supervised training
    # training_names = [
    #     'NIPS/DRM/supervised/EG_supervised_2_layer_5x5x20_3x3x50_b200_coor_descent_max_no_relu_lr_0_01_041416/classification',
    #                   'NIPS/LeNet5/LeNet5_supervised_2_layer_5x5x20_3x3x50_lr_0_1_b_500_041416/classification',
    # ]
    #
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'Supervised',
    #                '2-layer Convnet'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_supervised_use50000labeleddata.pdf')




    # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/params/EM_results_epoch_218.npz'
    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616'
    # filter_shape = [(1,5,5),]
    #
    # plot_betas_means_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    #
    # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051516/supervised_End_to_End_using_50K_data/params/EM_results_epoch_70.npz'
    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051516/supervised_End_to_End_using_50K_data'
    # filter_shape = [(1,5,5),]
    #
    # plot_betas_means_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    #
    # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use50000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data/params/EM_results_epoch_194.npz'
    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use50000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data'
    # filter_shape = [(1,5,5),]
    #
    # plot_betas_means_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    #
    # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use50000labeleddata_with_ReLU_051616/params/LeNet5_results_epoch_200.npz'
    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use50000labeleddata_with_ReLU_051616'
    # filter_shape = [(1,5,5),]
    # plot_filter_DCN(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)



    # # semisupervised training
    # training_names = [
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use100labeleddata_50000unlabeleddata_max_t_soft_c_sendrs_lr_1_init_1000_lambda_1_60000train_051816/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use600labeleddata_50000unlabeleddata_soft_t_soft_c_sendrs_lr_1_init_1000_lambda_1_60000train_051816/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_max_t_soft_c_sendrs_lr_1_init_1000_lambda_1_5_60000train_051816/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use3000labeleddata_50000unlabeleddata_max_t_soft_c_sendrs_lr_1_init_1000_lambda_0_5_60000train_051816/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use100labeleddata_with_ReLU_051616',
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'SRM soft t soft c 3000',
    #     'SRM soft t soft c 1000'
    #     'SRM soft t soft c 600',
    #     'SRM soft t soft c 100',
    #     '1-layer ReLU Convnet 50000 labels',
    #     '1-layer ReLU Convnet 100 labels'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='temp.pdf')















    # training_names = [
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_sendrs_use3000labeleddata_lr_1_051416/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use3000labeleddata_max_t_soft_c_max_a_sendrs_lr_1_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use3000labeleddata_No_ReLU_051416'
    # ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'EG_supervised_soft_c_max_t_sentrs_lr_1_use3000labeleddata',
    #     'EG_supervised_soft_c_max_t_max_a_sentrs_lr_1_use3000labeleddata',
    #                '1-layer-Convnet-NoReLU_use3000labeleddata'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_use3000labeleddata_051516.pdf')
    #
    # training_names = [
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_sendrs_use1000labeleddata_lr_1_051416/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use1000labeleddata_max_t_soft_c_max_a_sendrs_lr_1_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use1000labeleddata_No_ReLU_051416'
    # ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'EG_supervised_soft_c_max_t_sentrs_lr_1_use1000labeleddata',
    #     'EG_supervised_soft_c_max_t_max_a_sentrs_lr_1_use1000labeleddata',
    #                '1-layer-Convnet-NoReLU_use1000labeleddata'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_use1000labeleddata_051516.pdf')
    #
    # training_names = [
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_max_t_soft_c_sendrs_use600labeleddata_lr_1_051416/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b600_use600labeleddata_max_t_soft_c_max_a_sendrs_lr_1_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use1000labeleddata_No_ReLU_051416'
    # ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'EG_supervised_soft_c_max_t_sentrs_lr_1_use600labeleddata',
    #     'EG_supervised_soft_c_max_t_max_a_sentrs_lr_1_use600labeleddata',
    #                '1-layer-Convnet-NoReLU_use1000labeleddata'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_supervised_use600labeleddata_051516.pdf')

    # ###################################################################################################################
    # # Plotting for SRM No-Factor Model #
    # ###################################################################################################################
    #
    # # Plot convergence of NLL and parameters in unsupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/SRM/unsupervised/SRM_nofactor_unsupervised_TopDown_N50000_b500_soft_c_max_t_max_a_062016',
    #                   'NoFactorModels/SRM/unsupervised/SRM_nofactor_unsupervised_TopDown_N50000_b500_soft_c_max_t_noReLU_062016'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_soft_c_max_t_max_a',
    #                'EG_unsupervised_soft_c_max_t',
    #                ]
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Unsupervised_Learning_SRM_062016.pdf')
    # plot_Parameters_Convergence_No_Factor(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Unsupervised_Learning_SRM_062016.pdf')
    #
    # # Plot classification error in unsupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/SRM/unsupervised/SRM_nofactor_unsupervised_TopDown_N50000_b500_soft_c_max_t_max_a_062016/classification_Softmax_Using_50000_data',
    #                   'NoFactorModels/SRM/unsupervised/SRM_nofactor_unsupervised_TopDown_N50000_b500_soft_c_max_t_noReLU_062016/classification_Softmax_Using_50000_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/classification',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_No_ReLU_051116/classification'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_soft_c_max_t_max_a',
    #                'EG_unsupervised_soft_c_max_t',
    #                '1-layer LeNet with ReLU',
    #                '1-layer LeNet no ReLU'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_SRM_unsupervised_062016.png')
    #
    # # Plot classification error in supervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/SRM/supervised/SRM_nofactor_supervised_N50000_b500_soft_c_max_t_max_a_lr_1_0_062016/Train_supervised_using_50K',
    #                   'NoFactorModels/SRM/supervised/SRM_nofactor_supervised_N50000_b500_sof_c_max_t_lr_1_0_noReLU_062016/Train_supervised_using_50K',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316/classification',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_No_ReLU_051116/classification'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_supervised_soft_c_max_t_max_a',
    #                'EG_supervised_soft_c_max_t',
    #                '1-layer LeNet with ReLU',
    #                '1-layer LeNet no ReLU'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_SRM_supervised_062016.png')
    #
    # # Plot convergence of NLL and parameters in semisupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/SRM/semisupervised/SRM_nofactor_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_1_0_regcoeff_1_0_062016/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/SRM/semisupervised/SRM_nofactor5x5x20_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_1_0_regcoeff_1_0_062016/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_semisupervised_soft_c_max_t_max_a_1000unlabeled',
    #                'EG_semisupervised_soft_c_max_t_1000unlabeled',
    #                ]
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Semisupervised_Learning_SRM_062016.pdf')
    # plot_Parameters_Convergence_No_Factor(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Semisupervised_Learning_SRM_062016.pdf')
    #
    #
    # # Plot classification error in semisupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/SRM/semisupervised/SRM_nofactor_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_1_0_regcoeff_1_0_062016/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/SRM/semisupervised/SRM_nofactor5x5x20_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_1_0_regcoeff_1_0_062016/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use1000labeleddata_with_ReLU_051616/',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_use1000labeleddata_No_ReLU_051416/'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_semisupervised_soft_c_max_t_max_a_1000unlabeled',
    #                'EG_semisupervised_soft_c_max_t_1000unlabeled',
    #                '1-layer LeNet with ReLU 1000 labels',
    #                '1-layer LeNet no ReLU 1000 labels'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_SRM_semisupervised_062016.png')

    # ###################################################################################################################
    # # Plotting for DRM No-Factor Model #
    # ###################################################################################################################
    # # Plot classification error in supervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_supervised_5x5x20_5x5x50_Ni50000_b500_soft_c_max_t_max_a_lr_1_0_062016/Train_supervised_using_50K',
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_supervised_5x5x20_5x5x50_Ni50000_b500_soft_c_max_t_lr_1_0_062016/Train_supervised_using_50K',
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_1_0_062116/Train_supervised_using_50K',
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_5_062116/Train_supervised_using_50K',
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062116/Train_supervised_using_50K',
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_lr_1_0_062116/Train_supervised_using_50K',
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_lr_0_5_062116/Train_supervised_using_50K',
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_lr_0_1_062116/Train_supervised_using_50K',
    #                   'NIPS/LeNet5/LeNet5_supervised_2_layer_5x5x20_3x3x50_lr_0_1_b_500_041416/classification',
    #                   'NIPS/LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_125_noReLU_041416/classification'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_supervised_soft_c_max_t_max_a',
    #                'EG_supervised_soft_c_max_t',
    #                'EG_supervised_soft_c_max_t_max_a_noSoftmax_c_lr_1_0',
    #                'EG_supervised_soft_c_max_t_max_a_noSoftmax_c_lr_0_5',
    #                'EG_supervised_soft_c_max_t_max_a_noSoftmax_c_lr_0_1',
    #                'EG_supervised_soft_c_max_t_noSoftmax_c_lr_1_0',
    #                'EG_supervised_soft_c_max_t_noSoftmax_c_lr_0_5',
    #                'EG_supervised_soft_c_max_t_noSoftmax_c_lr_0_1',
    #                '2-layer LeNet with ReLU',
    #                '2-layer LeNet no ReLU'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_supervised_062116.png')
    #
    # # Plot convergence of NLL and parameters in semisupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_1_0_regcoeff_1_0_062016/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_1_0_regcoeff_1_0_062016/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_0_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_0_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_1_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_1_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled',
    #                'EG_semisupervised_soft_c_max_t_1000labeled',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_0_5',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_1_0',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_1_5',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_2_0',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_2_5',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_noSoftmax_c_reg_0_5',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_noSoftmax_c_reg_1_0',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_noSoftmax_c_reg_1_5'
    #                ]
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Semisupervised_Learning_DRM_062116.pdf')
    # plot_Parameters_Convergence_No_Factor(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Semisupervised_Learning_DRM_062116.pdf')
    #
    #
    # # Plot classification error in semisupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_1_0_regcoeff_1_0_062016/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_1_0_regcoeff_1_0_062016/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_0_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_0_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_1_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_1_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled',
    #                'EG_semisupervised_soft_c_max_t_1000labeled',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_0_5',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_1_0',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_1_5',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_2_0',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_noSoftmax_c_reg_2_5',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_noSoftmax_c_reg_0_5',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_noSoftmax_c_reg_1_0',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_noSoftmax_c_reg_1_5'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_DRM_semisupervised_062116.png')
    #
    # ###################################################################################################################
    # # Plotting for DRM No-Factor Model Send Activations to Softmax #
    # ###################################################################################################################
    # # Plot classification error in supervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062216/Train_supervised_using_50K',
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_1_0_062116/Train_supervised_using_50K',
    #                   'NIPS/LeNet5/LeNet5_supervised_2_layer_5x5x20_3x3x50_lr_0_1_b_500_041416/classification',
    #                   'NIPS/LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_125_noReLU_041416/classification'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_supervised_soft_c_max_t_max_a_send_activations',
    #                'EG_supervised_soft_c_max_t_max_a_send_probabilities',
    #                '2-layer LeNet with ReLU',
    #                '2-layer LeNet no ReLU'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_supervised_062216.png')
    #
    # # Plot convergence of NLL and parameters in semisupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_0_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_0_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_0_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_0_5_send_probabilities',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_0_send_probabilities',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_5_send_probabilities',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_0_send_probabilities',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_5_send_probabilities'
    #                ]
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Semisupervised_Learning_DRM_062216.pdf')
    # plot_Parameters_Convergence_No_Factor(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Semisupervised_Learning_DRM_062216.pdf')
    #
    #
    # # Plot classification error in semisupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_0_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_0_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_0_062116/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_5_062116/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_0_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_0_5_send_probabilities',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_0_send_probabilities',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_5_send_probabilities',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_0_send_probabilities',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_5_send_probabilities'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_DRM_semisupervised_062216.png')
    #
    # # Plot convergence of NLL and parameters in unsupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_5x5x20_5x5x50_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062116'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_soft_c_max_t_max_a',
    #                ]
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Unsupervised_Learning_DRM_062116.pdf')
    # plot_Parameters_Convergence_No_Factor(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Unsupervised_Learning_DRM_062216.pdf')
    #
    # # Plot classification error in unsupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_5x5x20_5x5x50_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062116/classification_Softmax_Using_50000_data_send_activations_to_Softmax',
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_5x5x20_5x5x50_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062116/classification_Softmax_Using_50000_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_2_layer_5x5x20_3x3x50_lr_0_1_b_500_041416/classification',
    #                   'NIPS/LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_125_noReLU_041416/classification'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_soft_c_max_t_max_a_send_activations',
    #                'EG_unsupervised_soft_c_max_t_max_a_send_probabilities',
    #                '2-layer LeNet with ReLU',
    #                '2-layer LeNet no ReLU'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_unsupervised_062216.png')
    #
    # ####################################################################################################################
    # # Plot classification error in supervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062216/Train_supervised_using_50K',
    #                   'NoFactorModels/DRM/supervised/DRM_nofactor_5x5x20_5x5x50_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_lr_0_1_062216/Train_supervised_using_50K',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051516/supervised_End_to_End_using_50K_data',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use50000labeleddata_with_ReLU_062416',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use50000labeleddata_no_ReLU_062416'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_DRM_supervised_soft_c_max_t_max_a_send_activations',
    #                'EG_DRM_supervised_soft_c_max_t_send_activations',
    #                'EG_SRM_Factor_Model_supervised_soft_c_soft_t_max_a_send_probabilities',
    #                '2-layer LeNet with ReLU',
    #                '2-layer LeNet no ReLU'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_supervised_compared_with_Factor_Model_062216.png', convnet_indx=[3, 4], old_indx=[2,])
    #
    # # Plot convergence of NLL and parameters in semisupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_0_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_0_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_1_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_1_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_2_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_2_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_60000train_051816/supervised_End_to_End_using_50K_data'
    # ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_0_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_0_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_1_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_1_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_2_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_2_5_send_activations',
    #                'EG_SRM_Factor_Model_supervised_soft_c_soft_t_max_a_1000labeled_reg_1_5_send_probabilities'
    #                ]
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Semisupervised_Learning_DRM_compared_with_Factor_Model_062216.pdf')
    # plot_Parameters_Convergence_No_Factor(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Semisupervised_Learning_DRM_compared_with_Factor_Model_062216.pdf')
    #
    #
    # # Plot classification error in semisupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = ['LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use1000labeleddata_with_ReLU_062416',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use1000labeleddata_no_ReLU_062416',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_0_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_1_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_regcoeff_2_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_0_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_1_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_1_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_2_0_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NoFactorModels/DRM/semisupervised/DRM_nofactor_5x5x20_5x5x50_semisupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_regcoeff_2_5_062216/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use1000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_60000train_051816/supervised_End_to_End_using_50K_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = ['2-layer LeNet with ReLU trained on 1000 labeled data',
    #                '2-layer LeNet no ReLU trained on 1000 labeled data',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_0_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_1_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_max_a_1000labeled_reg_2_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_0_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_1_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_1_5_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_2_0_send_activations',
    #                'EG_semisupervised_soft_c_max_t_1000labeled_reg_2_5_send_activations',
    #                'EG_SRM_Factor_Model_semisupervised_soft_c_soft_t_max_a_1000labeled_reg_1_5_send_rs'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_DRM_compared_with_Factor_Model_semisupervised_062216.png',
    #            convnet_indx=[0,1])
    #
    # # Plot convergence of NLL and parameters in unsupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_5x5x20_5x5x50_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062116',
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_5x5x20_5x5x50_unsupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_062116'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_soft_c_max_t_max_a',
    #                'EG_unsupervised_soft_c_max_t'
    #                ]
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Unsupervised_Learning_DRM_062116.pdf')
    # plot_Parameters_Convergence_No_Factor(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Unsupervised_Learning_DRM_062216.pdf')
    #
    # # Plot classification error in unsupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_5x5x20_5x5x50_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062116/classification_Softmax_Using_50000_data_send_activations_to_Softmax',
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_5x5x20_5x5x50_unsupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_062116/classification_Softmax_Using_50000_data_send_activations_to_Softmax',
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/classification_Softmax_Using_50000_data',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use50000labeleddata_with_ReLU_062416',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use50000labeleddata_no_ReLU_062416'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_soft_c_max_t_max_a_send_activations',
    #                'EG_unsupervised_soft_c_max_t_send_activations',
    #                'EG_SRM_Factor_Model_unsupervised_soft_c_soft_t_max_a_send_probabilities',
    #                '2-layer LeNet with ReLU',
    #                '2-layer LeNet no ReLU'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_unsupervised_compared_with_Factor_Model_062216.png', convnet_indx=[3,4], max_indx=600)
    # # Plot convergence of NLL and parameters in unsupervised training
    # output_dir = os.path.join(training_root, 'figures')
    #
    # training_names = [
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_unsupervised_5x5x20_5x5x50_Ni50000_b500_soft_c_max_t_max_a_lr_1_0_062016',
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_unsupervised_5x5x20_5x5x50_Ni50000_b500_soft_c_max_t_lr_1_0_062016',
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_5x5x20_5x5x50_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062116',
    #                   'NoFactorModels/DRM/unsupervised/DRM_nofactor_5x5x20_5x5x50_unsupervised_Ni50000_Nlabel1000_b500_soft_c_max_t_lr_0_1_062116'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'EG_unsupervised_soft_c_max_t_max_a',
    #                'EG_unsupervised_soft_c_max_t',
    #                'EG_unsupervised_soft_c_max_t_max_a_noSoftmax_c',
    #                'EG_unsupervised_soft_c_max_t_noSoftmax_c'
    #                ]
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Unsupervised_Learning_DRM_062116.pdf')
    # plot_Parameters_Convergence_No_Factor(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Unsupervised_Learning_DRM_062116.pdf')
    #
    #
    # ###################################################################################################################
    # ###################################################################################################################
    # # Figures for IARPA Visit #
    # ###################################################################################################################
    # ###################################################################################################################
    #
    # ##############################
    # # Training Results for MNIST #
    # ##############################
    # output_dir = os.path.join(training_root, 'IARPA_Visit_2_Layer_No_Factor_Model', 'figures')
    #
    #
    #
    # # Plot classification error in semisupervised training - 100 labeled data and 60K unlabeled data
    # print('\nclassification error in semisupervised training - 100 labeled data and 60K unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 600 labeled data and 60K unlabeled data
    # print('\nclassification error in semisupervised training - 600 labeled data and 60K unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 1K labeled data and 60K unlabeled data
    # print('\nclassification error in semisupervised training - 1K labeled data and 60K unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 3K labeled data and 60K unlabeled data
    # print('\nclassification error in semisupervised training - 3K labeled data and 60K unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 100 labeled data and 100 unlabeled data
    # print('\nclassification error in semisupervised training - 100 labeled data and 100 unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni100_Nlabel100_b100_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni100_Nlabel100_b100_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni100_Nlabel100_b100_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni100_Nlabel100_b100_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni100_Nlabel100_b100_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni100_Nlabel100_b100.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 600 labeled data and 600 unlabeled data
    # print('\nclassification error in semisupervised training - 600 labeled data and 600 unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni600_Nlabel600_b600_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni600_Nlabel600_b600_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni600_Nlabel600_b600_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni600_Nlabel600_b600_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni600_Nlabel600_b600_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni600_Nlabel600_b600.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 1000 labeled data and 1000 unlabeled data
    # print('\nclassification error in semisupervised training - 1000 labeled data and 1000 unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b1000_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b1000_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b1000_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b1000_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b1000_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b1000.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 1000 labeled data and 1000 unlabeled data
    # print('\nclassification error in semisupervised training - 1000 labeled data and 1000 unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni1000_Nlabel1000_b500.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 3000 labeled data and 3000 unlabeled data
    # print('\nclassification error in semisupervised training - 3000 labeled data and 3000 unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b3000_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b3000_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b3000_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b3000_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b3000_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b3000.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 3000 labeled data and 3000 unlabeled data
    # print('\nclassification error in semisupervised training - 3000 labeled data and 3000 unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni3000_Nlabel3000_b500.pdf',
    #            convnet_indx=[])
    #
    #
    #
    # # Plot classification error in semisupervised training - 50K labeled data and 50K unlabeled data
    # print('\nclassification error in semisupervised training - 50K labeled data and 50K unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - 2-layer DRM
    # print('\nclassification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/MNIST_DRM_nofactor_unsupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062916/Finetune_supervised_using_50K_labeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/supervised/MNIST_DRM_nofactor_supervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062916/Train_supervised_using_50K',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use50000labeleddata_with_ReLU_062916'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'Pretraining + Finetune End-to-End\n(Error Rate: 0.85%)',
    #                'Supervised\n(Error Rate: 1.1%)',
    #                'Semisupervised\n(Error Rate: 0.75%)',
    #                '2-layer Convnet\n(Error Rate: 0.99%)'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_nofactor_sup_unsup_semisup_5x5x20_5x5x50_Ni50000_Nlabel50000.pdf', convnet_indx=[3,], old_indx=[])
    #
    # # Plot classification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - SRM
    #
    # training_names = [
    #                   'NIPS/SRM/unsupervised+finetune/SRM_EG_unsupervised_5x5x20_b500_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051616/Finetune_End_to_End_using_50000_data',
    #                   'NIPS/SRM/supervised/SRM_EG_supervised_5x5x20_b500_use50000labeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_051516/supervised_End_to_End_using_50K_data',
    #                   'NIPS/SRM/semisupervised/SRM_EG_semisupervised_5x5x20_b500_use50000labeleddata_50000unlabeleddata_soft_t_soft_c_max_a_sendrs_lr_1_init_1000_lambda_1_5_051616/supervised_End_to_End_using_50K_data',
    #                   'NIPS/LeNet5/LeNet5_supervised_1_layer_5x5x20_lr_0_1_b_500_041316'
    #                 ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'Pretraining + Finetune End-to-End\n(Error Rate: 1.17%)',
    #     'Supervised\n(Error Rate: 1.21%)',
    #     'Semisupervised\n(Error Rate: 1.27%)',
    #     '1-layer Convnet\n(Error Rate: 1.30%)'
    #                ]
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_SRM_factor_sup_unsup_semisup_5x5x20_Ni50000_Nlabel50000.pdf', convnet_indx=[3,], old_indx=[])
    #
    #
    #
    # # # Plot filters - 2-layer no-factor DRM - MNIST
    # # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/MNIST_DRM_nofactor_unsupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062916/params/EM_results_epoch_1000.pkl'
    # # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/MNIST_DRM_nofactor_unsupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062916'
    # # filter_shape = [(20,5,5),(1,5,5)]
    # #
    # # plot_betas_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    # #
    # # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data/params/EM_results_epoch_198.pkl'
    # # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916'
    # # filter_shape = [(20,5,5),(1,5,5)]
    # #
    # # plot_betas_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    # #
    # # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/supervised/MNIST_DRM_nofactor_supervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062916/Train_supervised_using_50K/params/EM_results_epoch_444.pkl'
    # # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/supervised/MNIST_DRM_nofactor_supervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062916'
    # # filter_shape = [(20,5,5),(1,5,5)]
    # #
    # # plot_betas_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    # #
    # # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use50000labeleddata_with_ReLU_062916/params/LeNet5_results_epoch_274.npz'
    # # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use50000labeleddata_with_ReLU_062916'
    # # filter_shape = [(1,5,5),(20,5,5)]
    # # plot_filter_DCN(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    #
    # ##############################
    # # Training Results for CIFAR #
    # ##############################
    # # Plot classification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - 2-layer DRM
    # print('\nclassification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - CIFAR10 2-layer DRM\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/supervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_070316/Train_supervised_using_50K',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/supervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_001_070316/Train_supervised_using_50K',
    #                   'LeNet5/CIFAR10_supervised_2_layer_5x5x64_5x5x64_lr_0_01_b_500_use50000labeleddata_ReLU_070316'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'lr=0.01',
    #                'lr=0.001',
    #                '2-layer Convnet lr=0.01'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_CIFAR10_DRM_nofactor_sup_5x5x64_5x5x64_Ni50000_Nlabel50000.pdf', convnet_indx=[2,], old_indx=[], y_uplim=1.0, max_indx=1000)
    #
    # # Plot classification error in semisupervised training - 50K labeled data and 50K unlabeled data - 2-layer DRM
    # print('\nclassification error in semisupervised training - 50K labeled data and 50K unlabeled data - CIFAR10 2-layer DRM\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_0_5_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_1_0_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_1_5_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_0_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_5_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'LeNet5/CIFAR10_supervised_2_layer_5x5x64_5x5x64_lr_0_01_b_500_use50000labeleddata_ReLU_070316'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                 '2-layer Convnet'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_CIFAR10_DRM_nofactor_semisup_5x5x64_5x5x64_Ni50000_Nlabel50000.pdf', convnet_indx=[5,], old_indx=[], y_uplim=1.0, max_indx=500)
    #
    # # Plot classification error in semisupervised training - 4K labeled data and 50K unlabeled data - 2-layer DRM
    # print('\nclassification error in semisupervised training - 4K labeled data and 50K unlabeled data - CIFAR10 2-layer DRM\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_0_5_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_1_0_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_1_5_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_0_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_5_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'LeNet5/CIFAR10_supervised_2_layer_5x5x64_5x5x64_lr_0_01_b_500_use4000labeleddata_ReLU_070316'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                 '2-layer Convnet'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_CIFAR10_DRM_nofactor_semisup_5x5x64_5x5x64_Ni50000_Nlabel4000.pdf', convnet_indx=[5,], old_indx=[], y_uplim=1.0, max_indx=500, indx_step=50)
    #
    # # Plot classification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - 2-layer DRM
    # print('\nclassification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - CIFAR10 2-layer DRM\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_070316/Finetune_supervised_using_50K_labeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/supervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_070316/Train_supervised_using_50K',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_5_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'LeNet5/CIFAR10_supervised_2_layer_5x5x64_5x5x64_lr_0_01_b_500_use50000labeleddata_ReLU_070316'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'Pretraining + Finetune End-to-End\n(Error Rate: 27.17%)',
    #                'Supervised\n(Error Rate: 26.64%)',
    #                'Semisupervised\n(Error Rate: 24.60%)',
    #                '2-layer Convnet\n(Error Rate: 27.17%)'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_CIFAR10_DRM_nofactor_sup_unsup_semisup_5x5x64_5x5x64_Ni50000_Nlabel50000.pdf', convnet_indx=[3,], old_indx=[],y_uplim=1.0, max_indx=250, indx_step=50)
    #
    # # Plot convergence of NLL and parameters in unsupervised training
    # training_names = [
    #     'IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/MNIST_DRM_nofactor_unsupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062916',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_070316'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'MNIST',
    #     'CIFAR10'
    #                ]
    #
    # plot_NLL(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Negative_Log_Likelihood_in_Unsupervised_Learning_2_layer_DRM_070316.pdf')
    # plot_Parameters_Convergence_No_Factor(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Convergence_of_Parameters_in_Unsupervised_Learning_2_layer_DRM_070316.pdf')
    #
    # # # Plot filters - 2-layer no-factor DRM - CIFAR10
    # # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_070316/params/EM_results_epoch_653.pkl'
    # # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_070316'
    # # filter_shape = [(64,5,5),(3,5,5)]
    # #
    # # plot_betas_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    # #
    # # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_5_070316/semisupervised_End_to_End_using_50K_unlabeled_data/params/EM_results_epoch_640.pkl'
    # # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_5_070316'
    # # filter_shape = [(64,5,5),(3,5,5)]
    # #
    # # plot_betas_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)
    # #
    # # param_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/supervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_070316/Train_supervised_using_50K/params/EM_results_epoch_188.pkl'
    # # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/IARPA_Visit_2_Layer_No_Factor_Model/supervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_070316'
    # # filter_shape = [(64,5,5),(3,5,5)]
    # #
    # # plot_betas_lambdas(param_dir=param_dir, output_dir=output_dir, filter_shape=filter_shape)

    # output_dir = os.path.join(training_root, 'IARPA_Visit_2_Layer_No_Factor_Model', 'figures')
    # ###############################################
    # # Training Results for MNIST using MS Top-down#
    # ###############################################
    #
    # # Plot classification error in semisupervised training - 50K labeled data and 50K unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 50K labeled data and 50K unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_070616.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 1K labeled data and 50K unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 1K labeled data and 50K unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_070616.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 1K labeled data and 50K unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 1K labeled data and 50K unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel1000_b500_070616.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 100 labeled data and 50K unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 100 labeled data and 50K unlabeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   #'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   #'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 #'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 #'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel100_b500_070616.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - 2-layer DRM
    # print('\nclassification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/MNIST_DRM_nofactor_unsupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062916/Finetune_supervised_using_50K_labeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/MNIST_DRM_nofactor_ms_unsupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_070616/Finetune_supervised_using_50K_labeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/supervised/MNIST_DRM_nofactor_supervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_062916/Train_supervised_using_50K',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_ms_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use50000labeleddata_with_ReLU_062916'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'Pretraining + Finetune End-to-End\n(Error Rate: 0.85%)',
    #                'Pretraining_MS + Finetune End-to-End\n(Error Rate: 0.92%)',
    #                'Supervised\n(Error Rate: 1.1%)',
    #                'Semisupervised\n(Error Rate: 0.75%)',
    #                'Semisupervised-MSMP\n(Error Rate: 0.54%)',
    #                '2-layer Convnet\n(Error Rate: 0.99%)'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_DRM_nofactor_sup_unsup_semisup_5x5x20_5x5x50_Ni50000_Nlabel50000_070616.pdf', convnet_indx=[5,], old_indx=[], legend_size=10)
    #
    # ###############################################
    # # Training Results for CIFAR using MS Top-down#
    # ###############################################
    #
    # # Plot classification error in semisupervised training - 50K labeled data and 50K unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 50K labeled data and 50K unlabeled CIFAR10 data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_0_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_1_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_1_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=1.0, plot_name='Error_vs_Epoch_CIFAR10_DRM_nofactor_ms_semisupervised_5x5x64_5x5x64_Ni50000_Nlabel50000_b500_070616.pdf',
    #            convnet_indx=[], max_indx=1000)
    #
    # # Plot classification error in semisupervised training - 4K labeled data and 50K unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 4K labeled data and 50K unlabeled CIFAR10 data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_0_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_1_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_1_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_0_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=1.0, plot_name='Error_vs_Epoch_CIFAR10_DRM_nofactor_ms_semisupervised_5x5x64_5x5x64_Ni50000_Nlabel4000_b500_070616.pdf',
    #            convnet_indx=[], max_indx=1000)
    #
    # # Plot classification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - 2-layer DRM
    # print('\nclassification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - CIFAR10 2-layer DRM\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/unsupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_unsupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_070316/Finetune_supervised_using_50K_labeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/supervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_supervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_070316/Train_supervised_using_50K',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_2_5_070316/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_01_reg_0_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data',
    #                   'LeNet5/CIFAR10_supervised_2_layer_5x5x64_5x5x64_lr_0_01_b_500_use50000labeleddata_ReLU_070316'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'Pretraining + Finetune End-to-End\n(Error Rate: 27.17%)',
    #                'Supervised\n(Error Rate: 26.64%)',
    #                'Semisupervised\n(Error Rate: 24.60%)',
    #                'Semisupervised-MSMP\n(Error Rate: 25.09%)',
    #                '2-layer Convnet\n(Error Rate: 27.17%)'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_CIFAR10_DRM_nofactor_sup_unsup_semisup_5x5x64_5x5x64_Ni50000_Nlabel50000_070616.pdf', convnet_indx=[4,], old_indx=[],y_uplim=1.0, max_indx=1000, indx_step=50, legend_size=10)
    #
    # # Plot classification error in supervised training, unsup + pretrain, and semi-sup using all 50K labeled data - 2-layer DRM
    # print('\nclassification error for supervised using all 50K labeled data\n')
    #
    # training_names = [
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/supervised/MNIST_DRM_nofactor_supervised_Conv_Small_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_lr_decay_0_1_at_epoch80_200_071116/Train_using_50K_labeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/supervised/MNIST_DRM_nofactor_supervised_Conv_Small_4_layer_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_lr_decay_0_1_at_epoch35_120_071116/Train_using_50K_labeled_data',
    #                   'LeNet5/LeNet5_supervised_2_layer_5x5x20_5x5x50_lr_0_1_b_500_use50000labeleddata_with_ReLU_062916'
    #                   ]
    #
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                'Supervised_5layers\n(Error Rate: 1.1%)',
    #                'Supervised_4layers\n(Error Rate: 0.75%)',
    #                '2-layer Convnet\n(Error Rate: 0.99%)'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, plot_name='Error_vs_Epoch_sup_Ni50000_Nlabel50000_071216.pdf', convnet_indx=[5,], old_indx=[], legend_size=10)

    # ################################################################
    # # Training Results for MNIST using 2-layer DRM with init_Bengio#
    # ################################################################
    # output_dir = os.path.join(training_root, 'IARPA_Visit_2_Layer_No_Factor_Model', 'figures')
    #
    # # Plot classification error in semisupervised training - 50K labeled data and 100 unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 60K labeled data and 100 unlabeled data\n')
    #
    # training_names = ['IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_0_05_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_0_1_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_0_25_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [ 'lambda = 0.05',
    #                 'lambda = 0.1',
    #                 'lambda = 0.25',
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_init_Bengio_072016.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 50K labeled data and 600 unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 60K labeled data and 600 unlabeled data\n')
    #
    # training_names = ['IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_0_05_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_0_1_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_0_25_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [ 'lambda = 0.05',
    #                 'lambda = 0.1',
    #                 'lambda = 0.25',
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel600_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_init_Bengio_072016.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 50K labeled data and 1000 unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 60K labeled data and 1000 unlabeled data\n')
    #
    # training_names = ['IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_05_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_1_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_25_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [ 'lambda = 0.05',
    #                 'lambda = 0.1',
    #                 'lambda = 0.25',
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_init_Bengio_072016.pdf',
    #            convnet_indx=[])
    #
    # # Plot classification error in semisupervised training - 50K labeled data and 3000 unlabeled data
    # print('\nclassification error in semisupervised training using ms top-down - 60K labeled data and 3000 unlabeled data\n')
    #
    # training_names = ['IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_05_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_1_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_25_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_0_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_1_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_0_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data',
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [ 'lambda = 0.05',
    #                 'lambda = 0.1',
    #                 'lambda = 0.25',
    #                 'lambda = 0.5',
    #                 'lambda = 1.0',
    #                 'lambda = 1.5',
    #                 'lambda = 2.0',
    #                 'lambda = 2.5',
    #                ]
    #
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.5, plot_name='Error_vs_Epoch_MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel3000_b500_soft_c_max_t_max_a_lr_0_1_reg_2_5_init_Bengio_072016.pdf',
    #            convnet_indx=[])
    #
    # Plot classification error in semisupervised training - 50K labeled data and 50000 unlabeled data

    # Plot for Rich
    #CIFAR10
    # output_dir = os.path.join(training_root, 'IARPA_Visit_2_Layer_No_Factor_Model', 'figures')
    #
    # training_names = [
    #                   'results_for_Rich/CIFAR10_DRM_nofactor_semisupervised_CIFAR10_Conv_Large_6_Layers_Ni50000_Nlabel4000_b250_soft_c_max_t_max_a_lr_0_02_lr_final_0_00001_maxepoch_500_reg_0_5_clipgrad_-inf_inf_init_Bengio_072616/Train_using_50K_unlabeled_data',
    #                   'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/CIFAR10_DRM_nofactor_ms_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_0_5_070616/semisupervised_End_to_End_using_50K_unlabeled_data'
    #                   ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #                 '6-layer DRM',
    #                 '2-layer DRM'
    #                ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.9, plot_name='Error_vs_Epoch_CIFAR10_semisup_DRM_nofactor.pdf',
    #            convnet_indx=[])
    #
    # training_names = [
    #     'results_for_Rich/CIFAR10_Conv_Large_9_Layers_nofactor_supervised_Ni60000_Nlabel60000_b200_lr_init_0_2_lr_final_0_0001_maxepoch_500_clipgrad_-inf_inf_init_Bengio_bnBU_08416/Train',
    #     'results_for_Rich/CIFAR10_Conv_Large_9_Layers_nofactor_semisupervised_Ni60000_Nlabel60000_b100_lr_init_0_2_lr_final_0_0001_maxepoch_500_clipgrad_-inf_inf_init_Bengio_bnBU_reg_0_5_08416/Train',
    #     'results_for_Rich/CIFAR10_Conv_Large_9_Layers_nofactor_semisupervised_Ni60000_Nlabel4000_b100_lr_init_0_2_lr_final_0_0001_maxepoch_500_clipgrad_-inf_inf_init_Bengio_bnBU_reg_0_5_08416/Train',
    # ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'supervised: 60K labeled images \n(Error Rate: 12.53%)',
    #     'semisupervised: 60K labeled images \n(Error Rate: 11.47%)',
    #     'semisupervised: 4K labeled images \n(Error Rate: 34.54%)'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.9, legend_size=14,
    #            plot_name='Error_vs_Epoch_CIFAR10_9_layer_DRM_nofactor.pdf',
    #            convnet_indx=[])

    #MNIST
    training_root = '/Volumes/TanNguyen/temp_results/110816/'
    output_dir = os.path.join(training_root)

    training_names = [
        'MNIST_Conv_Small_5_Layers_nofactor_semisupervised_Ni60000_Nlabel100_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_5_HiddenLayer_bnBU_MSMPTopDown_nonlin_abs_081116/Train']
    training_list = []
    for name in training_names:
        training_list.append(os.path.join(training_root, name))

    legend_list = [
        'Hidden',
    ]

    plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.9,
               plot_name='Error_vs_Epoch_MNIST_sup_DRM_nofactor.pdf',
               convnet_indx=[])

    # training_names = [
    #     'results_for_Rich/MNIST_Conv_Small_5_Layers_nofactor_semisupervised_Ni60000_Nlabel1000_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_5_bnBU_08616/Train',
    #     'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel1000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data'
    # ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     '5-layer DRM + BN',
    #     '2-layer DRM'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.9,
    #            plot_name='Error_vs_Epoch_MNIST_semisup_NL_1000_DRM_nofactor.pdf',
    #            convnet_indx=[])
    #
    # training_names = [
    #     'results_for_Rich/MNIST_Conv_Small_5_Layers_nofactor_semisupervised_Ni60000_Nlabel100_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_5_bnBU_08616/Train',
    #     'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni60000_Nlabel100_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_init_Bengio_072016/Semisupervised_End_to_End_using_60K_unlabeled_data'
    # ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     '5-layer DRM + BN',
    #     '2-layer DRM'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.9,
    #            plot_name='Error_vs_Epoch_MNIST_semisup_NL_100_DRM_nofactor.pdf',
    #            convnet_indx=[])
    #
    # training_names = [
    #     'results_for_Rich/MNIST_Conv_Small_5_Layers_nofactor_semisupervised_Ni60000_Nlabel60000_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_5_bnBU_08616/Train',
    #     'IARPA_Visit_2_Layer_No_Factor_Model/semisupervised/MNIST_DRM_nofactor_semisupervised_5x5x20_5x5x50_Ni50000_Nlabel50000_b500_soft_c_max_t_max_a_lr_0_1_reg_0_5_062916/semisupervised_End_to_End_using_50K_unlabeled_data'
    # ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     '5-layer DRM + BN',
    #     '2-layer DRM'
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.9,
    #            plot_name='Error_vs_Epoch_MNIST_semisup_NL_60000_DRM_nofactor.pdf',
    #            convnet_indx=[])
    #
    # training_names = [
    #     'results_for_Rich/MNIST_Conv_Small_5_Layers_nofactor_supervised_Ni60000_Nlabel60000_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_bnBU_08616/Train',
    #     'results_for_Rich/MNIST_Conv_Small_5_Layers_nofactor_semisupervised_Ni60000_Nlabel60000_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_5_bnBU_08616/Train',
    #     'results_for_Rich/MNIST_Conv_Small_5_Layers_nofactor_semisupervised_Ni60000_Nlabel1000_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_5_bnBU_08616/Train',
    #     'results_for_Rich/MNIST_Conv_Small_5_Layers_nofactor_semisupervised_Ni60000_Nlabel100_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_5_bnBU_08616/Train'
    #
    # ]
    # training_list = []
    # for name in training_names:
    #     training_list.append(os.path.join(training_root, name))
    #
    # legend_list = [
    #     'sup NL=60K NU=60K \n(Error Rate: 0.68%)',
    #     'semisup NL=60K NU=60K \n(Error Rate: 0.9%)',
    #     'semisup NL=1K NU=60K \n(Error Rate: 2.14%)',
    #     'semisup NL=100 NU=60K \n(Error Rate: 0.91%)',
    # ]
    #
    # plot_error(training_list=training_list, output_dir=output_dir, legend_list=legend_list, y_uplim=0.9, legend_size=14,
    #            plot_name='Error_vs_Epoch_MNIST_5_layer_DRM_nofactor.pdf',
    #            convnet_indx=[])



