__author__ = 'minhtannguyen'

import numpy as np
import matplotlib.pyplot as pl

from swmfa import *

import theano
import theano.tensor as T

from matplotlib.patches import Ellipse

def do_test(seed, PPCA, tol=1e-4, verbose=True):

    maxiter = 400

    np.random.seed(seed)

    d = np.random.randn(3, 250)
    d[0,:] *= 5
    d[1,:] *= 1

    b = np.random.randn(3,250)
    b[0,:] *= 5
    b[0,:] -= 5
    b[1,:] *= 1
    b[1,:] += 10 - 0.5 * b[0,:]

    d = np.concatenate((d,b),axis=1)

    xmin, xmax = np.min(d[0]), np.max(d[0])
    ymin, ymax = np.min(d[1]), np.max(d[1])

    D, N = d.shape
    fig=pl.figure()
    pl.plot(d[0,:],d[1,:],'ko',alpha=0.25)

    K, M = 2, 2

    init_obj = kmeans_init(d.T, K, N)
    init_obj.compute_kmeans()

    psis_val_init=np.tile(np.var(d.T,axis=0)[None,:],(K,1))

    x = T.matrix('x')
    means = (init_obj.means).get_value()
    #means = np.random.randn(3,3)

    ####################################################################################
    # When lock_psis=False #
    ####################################################################################

    # mix1 = Mofa(x1.T, K, M, D, N, means, psis_val_init, PPCA, lock_psis=False)
    #
    # output_func1 = theano.function([x1] , [mix1.means, mix1.covs, mix1.psis], on_unused_input='ignore')
    # means_val_init, covs_val_init, psis_val_init = output_func1(d)
    #
    # pl.plot(means_val_init[:,0],means_val_init[:,1],'rx',ms=15,label='Initialization')
    # plot_2d_ellipses(0,1, means_val_init, covs_val_init, K, edgecolor='r')
    #
    # mix1.take_EM_step()
    #
    # updates = []
    # updates.append((mix1.means, mix1.means_new))
    # updates.append((mix1.lambdas, mix1.lambdas_new))
    # updates.append((mix1.psis, mix1.psis_new))
    # updates.append((mix1.covs, mix1.covs_new))
    # updates.append((mix1.inv_covs, mix1.inv_covs_new))
    # updates.append((mix1.amps, mix1.amps_new))
    #
    # output_func2 = theano.function([], [mix1.logLs, mix1.rs, mix1.betas, mix1.latents, mix1.latent_covs],
    #                                  updates=updates, on_unused_input='ignore',
    #                                  givens={x1:d})
    # L = None
    # for i in xrange(maxiter):
    #     logLs_val, rs_val, betas_val, latents_val, latent_covs_val = output_func2()
    #     newL = np.sum(logLs_val)
    #     if i == 0 and verbose:
    #         print("Initial NLL=", -newL)
    #
    #     if L!=None:
    #         dL = np.abs((newL-L)/L)
    #         if i > 5 and dL < tol:
    #             break
    #     L = newL
    #
    # if i < maxiter - 1:
    #     if verbose:
    #         print("EM converged after {0} iterations".format(i))
    #         print("Final NLL={0}".format(-newL))
    # else:
    #     print("Warning:EM didn't converge after {0} iterations".format(i))
    #
    # output_func3 = theano.function([], [mix1.means, mix1.lambdas, mix1.psis, mix1.covs, mix1.inv_covs, mix1.amps], on_unused_input='ignore',
    #                                  givens={x1:d})
    #
    # means_val, lambdas_val, psis_val, covs_val, inv_covs_val, amps_val = output_func3()
    #
    # plot_2d_ellipses(0,1,means_val, covs_val, K, edgecolor='b')
    # pl.plot(means_val[:,0], means_val[:,1],'bx',ms=15,label='Psi free')

    ##################################################################################
    # When lock_psis=True #
    ##################################################################################

    mix = SimpleSwMofa(x.T, K, M, D, N, means, psis_val_init, PPCA, lock_psis=False)
    output_init = theano.function([x] , [mix.means, mix.covs, mix.psis], on_unused_input='ignore')
    means_val_init, covs_val_init, psis_val_init = output_init(d)

    pl.plot(means_val_init[:,0],means_val_init[:,1],'rx',ms=15,label='Initialization')
    plot_2d_ellipses(0,1, means_val_init, covs_val_init, K, edgecolor='r')

    mix.take_EM_step()

    updates = []
    updates.append((mix.means, mix.means_new))
    updates.append((mix.lambdas, mix.lambdas_new))
    updates.append((mix.psis, mix.psis_new))
    updates.append((mix.covs, mix.covs_new))
    updates.append((mix.inv_covs, mix.inv_covs_new))
    updates.append((mix.amps, mix.amps_new))
    updates.append((mix.lambda_covs, mix.lambda_covs_new))

    output_one_iter = theano.function([], [mix.logLs, mix.rs, mix.betas, mix.latents, mix.latent_covs, mix.psis_new, mix.sumrs],
                                     updates=updates, on_unused_input='ignore',
                                     givens={x:d})
    L = None
    for i in xrange(maxiter):
        logLs_val, rs_val, betas_val, latents_val, latent_covs_val, psis_val, sumrs_val = output_one_iter()
        newL = np.sum(logLs_val)
        if i == 0 and verbose:
            print("Initial NLL=", -newL)

        if L!=None:
            dL = np.abs((newL-L)/L)
            if i > 5 and dL < tol:
                break
        L = newL

    if i < maxiter - 1:
        if verbose:
            print("EM converged after {0} iterations".format(i))
            print("Final NLL={0}".format(-newL))
    else:
        print("Warning:EM didn't converge after {0} iterations".format(i))

    output_final = theano.function([], [mix.means, mix.lambdas, mix.psis, mix.covs, mix.inv_covs, mix.amps, mix.lambda_covs, mix.rs], on_unused_input='ignore',
                                     givens={x:d})

    means_val, lambdas_val, psis_val, covs_val, inv_covs_val, amps_val, lambda_covs_val, rs_val = output_final()

    indx = np.argmax(rs_val,0)
    indx0 = np.nonzero(indx==0)
    indx1 = np.nonzero(indx==1)
    indx2 = np.nonzero(indx==2)

    pl.plot(d[0,indx0],d[1,indx0],'yo',alpha=0.25)
    pl.plot(d[0,indx1],d[1,indx1],'mo',alpha=0.25)
    pl.plot(d[0,indx2],d[1,indx2],'co',alpha=0.25)

    plot_2d_ellipses(0,1, means=means_val, covs=covs_val, K=2, edgecolor='g')
    plot_2d_ellipses_psis(0,1, means=means_val, psis=psis_val, K=2, edgecolor='k')
    plot_2d_ellipses(0,1, means=means_val, covs=lambda_covs_val, K=2, edgecolor='b')
    pl.plot(means_val[:,0],means_val[:,1],'gx',ms=15,label='Psi fixed')
    pl.title(r'Data $(D, N) = ({0}, {1})$, Model $(K, M) = ({2}, {3})$'.format(D,N,K,M))
    pl.legend()
    pl.xlim(xmin, xmax)
    pl.ylim(ymin, ymax)
    fig.savefig('my_swmofa_%02d.png' % seed)

    #print mix.lambdas
    #print mix.lambdas.sum()
    print "test.py: done seed %d" % seed
    return None

def plot_2d_ellipses(d1,d2, means, covs, K, **kwargs):
    """
    Make a 2D plot of the model projected onto axes
    d1 and d2.
    """
    for k in range(K):
        mean = means[k,(d1, d2)]
        cov = covs[k][((d1, d2),(d1, d2)), ((d1, d1), (d2, d2))]
        _plot_2d_ellipse(mean, cov, **kwargs)

def plot_2d_ellipses_psis(d1,d2, means, psis, K, **kwargs):
    """
    Make a 2D plot of the model projected onto axes
    d1 and d2.
    """
    for k in range(K):
        mean = means[k,(d1, d2)]
        psi = np.diag(psis[k])[((d1, d2),(d1, d2)), ((d1, d1), (d2, d2))]
        _plot_2d_ellipse(mean, psi, **kwargs)

def _plot_2d_ellipse(mu, cov, ax=None, **kwargs):
    """
    Plot the error ellipse at a point given it's covariance matrix.
    """
    # some sane defaults
    facecolor = kwargs.pop('facecolor', 'none')
    edgecolor = kwargs.pop('edgecolor', 'k')

    x, y = mu
    U, S, V = np.linalg.svd(cov)
    theta = np.degrees(np.arctan2(U[1, 0], U[0, 0]))
    ellipsePlot = Ellipse(xy=[x, y],
                          width=2 * np.sqrt(S[0]),
                          height=2 * np.sqrt(S[1]),
                          angle=theta,
            facecolor=facecolor, edgecolor=edgecolor, **kwargs)

    if ax is None:
        ax = pl.gca()
    ax.add_patch(ellipsePlot)

if __name__ == "__main__":
    for seed in range(7):
        do_test(seed, False)