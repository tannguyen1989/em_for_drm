import numpy as np
import matplotlib.pyplot as pl

from python.mofa import *

def do_test(seed, PPCA, init_ppca):
    np.random.seed(seed)

    d = np.random.randn(3, 250)
    d[0,:] *= 5
    d[1,:] *= 1
    d[1,:] += 3 + 0.5 * d[0,:]

    b = np.random.randn(3,250)
    b[0,:] *= 5
    b[0,:] -= 5
    b[1,:] *= 1
    b[1,:] += 10 - 0.5 * b[0,:]

    d = np.concatenate((d,b),axis=1)

    b = np.random.randn(3,130)
    b[0,:] *= 1
    b[0,:] += 5
    b[1,:] *= 5
    b[1,:] += 12 + 0.5 * b[0,:]

    d = np.concatenate((d,b),axis=1)
    xmin, xmax = np.min(d[0]), np.max(d[0])
    ymin, ymax = np.min(d[1]), np.max(d[1])

    D, N = d.shape
    fig=pl.figure()
    pl.plot(d[0,:],d[1,:],'ko',alpha=0.25)

    K, M = 3, 2

    # mix1 = Mofa(d.T, K, M, PPCA, lock_psis=False, init_ppca=init_ppca)
    # pl.plot(mix1.means[:,0],mix1.means[:,1],'rx',ms=15,label='Initialization')
    # mix1.plot_2d_ellipses(0,1,edgecolor='r')
    #
    # mix1.run_em()
    #
    # mix1.plot_2d_ellipses(0,1,edgecolor='b')
    # pl.plot(mix1.means[:,0],mix1.means[:,1],'bx',ms=15,label='Psi free')

    mix = Mofa(d.T, K, M, PPCA, lock_psis=True, init_ppca=init_ppca)
    pl.plot(mix.means[:,0],mix.means[:,1],'rx',ms=15,label='Initialization')
    mix.plot_2d_ellipses(0,1,edgecolor='r')

    mix.run_em()

    indx = np.argmax(mix.rs,0)
    indx0 = np.nonzero(indx==0)
    indx1 = np.nonzero(indx==1)
    indx2 = np.nonzero(indx==2)

    pl.plot(d[0,indx0],d[1,indx0],'yo',alpha=0.25)
    pl.plot(d[0,indx1],d[1,indx1],'mo',alpha=0.25)
    pl.plot(d[0,indx2],d[1,indx2],'co',alpha=0.25)

    mix.plot_2d_ellipses(0,1,edgecolor='g')
    mix.plot_2d_lambda_covs(0,1,edgecolor='b')
    mix.plot_2d_psis(0,1,edgecolor='k')
    pl.plot(mix.means[:,0],mix.means[:,1],'gx',ms=15,label='Psi fixed')
    pl.title(r'Data $(D, N) = ({0}, {1})$, Model $(K, M) = ({2}, {3})$'.format(D,N,K,M))
    pl.legend()
    pl.xlim(xmin, xmax)
    pl.ylim(ymin, ymax)
    fig.savefig('mofa_ex_%02d.png' % seed)

    #print mix.lambdas
    #print mix.lambdas.sum()
    print "test.py: done seed %d" % seed
    return None

if __name__ == "__main__":
    for seed in range(10):
        do_test(seed, PPCA=True, init_ppca=False)


