__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')

from train_model_no_factor import *
from probe_model_no_factor import *
from old_codes.load_data_stable import DATA
from old_codes.load_data_for_60K_semi_sup_stable import DATA_60K
from old_codes.train_no_factor_stable import TrainNoFactorDRM

if __name__ == '__main__':
    data_mode = 'semisupervised'
    em_mode = 'hard'
    stop_mode = 'NLL'
    train_method = 'EG_DRM_semisupervised'
    debug_mode = 'OFF'
    preprocess_mode = True
    init_Bengio = False

    Nlabeled = 4000
    Ni = 50000

    Cin = 3 # depth of filters
    H = 32 # width of input images
    W = 32 # height of input images
    learning_rate = 0.01
    moment_coeff = 0.0

    lr_decay = 0.01
    epoch_to_reduce_lr = 200

    tol = 1e-10
    verbose = True
    batch_size = 500
    max_epochs = 500

    seed = 2

    reg_coeff = 0.5 # for semisupervised

    output_dir = '/home/ubuntu/research_results/EM_results/CIFAR10_DRM_nofactor_5x5x64_5x5x64_semisupervised_Ni50000_Nlabel4000_b500_soft_c_max_t_max_a_lr_0_01_reg_0_5_070316'
    # data_dir = '/home/ubuntu/repos/em_for_drm/data/mnist.pkl.gz'
    data_dir = '/home/ubuntu/repos/em_for_drm/data/cifar10'
    param_dir = os.path.join(output_dir,'params','EM_results_epoch_653.pkl')

    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results'
    # data_dir = '/Users/heatherseeba/repos/em_drm/data/cifar10'

    if train_method == 'EG_SRM_supervised':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed)
        mnist_model = EG_SRM_supervised(learning_rate=learning_rate, batch_size=batch_size, Cin=Cin, W=W, H=H,
                                        em_mode=em_mode, seed=seed, param_dir=param_dir, use_mode='train')
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                             train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                             model=mnist_model, output_dir=os.path.join(output_dir,'Train_supervised_using_50K'))
        train_mnist_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode, debug_mode=debug_mode)

    elif train_method == 'EG_SRM_unsupervised_Topdown':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed)
        mnist_model = EG_SRM_unsupervised_TopDown(learning_rate=learning_rate, batch_size=batch_size,
                                          Cin=Cin, W=W, H=H, em_mode=em_mode, seed=seed)
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                             train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                             model=mnist_model, output_dir=output_dir)
        train_mnist_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode)

    elif train_method == 'EG_SRM_semisupervised':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed)
        mnist_model = EG_SRM_semisupervised(learning_rate=learning_rate, batch_size=batch_size, Cin=Cin, W=W, H=H,
                                        em_mode=em_mode, seed=seed, param_dir=param_dir, reg_coeff=reg_coeff, use_mode='train')
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                             train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                             model=mnist_model, output_dir=os.path.join(output_dir,'semisupervised_End_to_End_using_50K_unlabeled_data'))
        train_mnist_model.train_semisupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode, debug_mode=debug_mode)

    elif train_method == 'EG_DRM_supervised':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)
        mnist_model = EG_DRM_supervised(batch_size=batch_size, Cin=Cin, W=W, H=H,
                                        em_mode=em_mode, seed=seed, param_dir=param_dir, use_mode='train', init_Bengio=init_Bengio)
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                             train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                             model=mnist_model, output_dir=os.path.join(output_dir,'Train_using_50K_labeled_data'))
        train_mnist_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode, debug_mode=debug_mode, lr=learning_rate)

    elif train_method == 'EG_DRM_unsupervised_Topdown':
        mnist = DATA(dataset_name='CIFAR10', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)
        mnist_model = EG_DRM_unsupervised_TopDown(learning_rate=learning_rate, batch_size=batch_size,
                                          Cin=Cin, W=W, H=H, em_mode=em_mode, seed=seed)
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                             train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                             model=mnist_model, output_dir=output_dir)
        train_mnist_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode)

    elif train_method == 'EG_DRM_unsupervised_MS_Topdown':
        mnist = DATA(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)
        mnist_model = EG_DRM_unsupervised_MS_TopDown(learning_rate=learning_rate, batch_size=batch_size,
                                          Cin=Cin, W=W, H=H, em_mode=em_mode, seed=seed)
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                             train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                             model=mnist_model, output_dir=output_dir)
        train_mnist_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode)

    elif train_method == 'EG_DRM_semisupervised':
        mnist = DATA(dataset_name='CIFAR10', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)
        mnist_model = EG_DRM_semisupervised(batch_size=batch_size, Cin=Cin, W=W, H=H,
                                        em_mode=em_mode, seed=seed, reg_coeff=reg_coeff, init_Bengio=init_Bengio)
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                             train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                             model=mnist_model, output_dir=os.path.join(output_dir,'Semisupervised_End_to_End_using_60K_unlabeled_data'))
        train_mnist_model.train_semisupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode, debug_mode=debug_mode, lr_init=learning_rate)

    elif train_method == 'EG_DRM_semisupervised_MS':
        mnist = DATA_60K(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)
        mnist_model = EG_DRM_semisupervised_MS(learning_rate=learning_rate, batch_size=batch_size, Cin=Cin, W=W, H=H,
                                        em_mode=em_mode, seed=seed, reg_coeff=reg_coeff)
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                             train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                             model=mnist_model, output_dir=os.path.join(output_dir,'semisupervised_End_to_End_using_50K_unlabeled_data'))
        train_mnist_model.train_semisupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode, debug_mode=debug_mode)

    else:
        mnist = DATA(dataset_name='CIFAR10', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin, H=H, W=W, seed=seed, preprocess=preprocess_mode)
        mnist_probe_model = train_Softmax_EM_DRM(learning_rate=learning_rate, batch_size=batch_size, Cin=Cin, W=W, H=H,
                                                 em_mode=em_mode, seed=seed, param_dir=param_dir)
        train_mnist_probe_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest, valid_data=mnist.dvalid,
                                                   train_label=mnist.train_label, test_label=mnist.test_label, valid_label=mnist.valid_label,
                                                   model=mnist_probe_model, output_dir=output_dir)
        train_mnist_probe_model.train_Softmax(max_epochs=max_epochs, classification_dir='classification_Softmax_Using_50000_data_send_activations_to_Softmax')
