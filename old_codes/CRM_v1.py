__author__ = 'minhtannguyen'

#######################################################################################################################
# still have no rendering channel
# only do hard on c
#######################################################################################################################


import matplotlib as mpl
mpl.use('Agg')

import numpy as np
np.set_printoptions(threshold='nan')

from theano.tensor.nnet import conv2d
from theano.tensor.nnet.neighbours import images2neibs
from theano.tensor.signal import pool

from swmfa import *


# from lasagne.layers import InputLayer, FeatureWTALayer

class CRM(SwMofa):
    """
    EM Algorithm for the Convolutional Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes
    'data_4D: data in 4-D (N,D,H,W)

    """
    def __init__(self, data_4D, labels, K, M, W, H, w, h, Cin, Ni, amps_val_init=None,
                 lambdas_val_init=None, lambda_covs_val_init=None,
                 covs_val_init=None, inv_covs_val_init=None,
                 psis_val_init=None, means_val_init=None,
                 PPCA=False,lock_psis=False,
                 update_mode='Hinton', em_mode='hard', E_mode='convolution',
                 rs_clip = 0.0,
                 max_condition_number=1.e3,
                 init=True,init_ppca=False):

        # required
        self.K     = K # number of clusters
        self.M     = M # latent dimensionality
        self.data_4D  = data_4D
        self.labels = labels
        self.Ni     = Ni # no. of images
        self.w     = w # width of filters
        self.h     = h # height of filters
        self.Cin   = Cin # number of channels in the image
        self.D     = self.h * self.w * self.Cin # patch size
        self.W     = W # width of image
        self.H     = H # height of image
        self.Np = (self.H - self.h + 1)*(self.W - self.w + 1) # no. of patches per image
        self.N = self.Ni * self.Np # total no. of patches
        self.Nl = self.N # total no. of hidden units
        self.update_mode  = update_mode
        self.lambdas_val_init = lambdas_val_init
        self.amps_val_init = amps_val_init
        self.em_mode = em_mode
        self.E_mode = E_mode

        # options
        self.PPCA                 = PPCA
        self.lock_psis            = lock_psis
        self.rs_clip              = rs_clip
        self.max_condition_number = max_condition_number
        assert rs_clip >= 0.0

        # patchification
        print 'Patchification'
        self.data = images2neibs(ten4=self.data_4D, neib_shape=(self.h, self.w), neib_step=(1,1), mode='valid')
        self.data = T.reshape(self.data,newshape=(self.Ni, self.Cin, self.Np, self.h * self.w))
        self.data = self.data.dimshuffle(0,2,1,3)
        self.data = T.reshape(self.data, newshape=(self.Ni, self.Np, self.D))
        self.data = T.reshape(self.data, newshape=(self.N, self.D))
        self.dataT = self.data.T

        # initialize the means
        # if initial values for means are not provided, use K-means to initialize the means
        if means_val_init == None:
            self.means_val_init = np.asarray(np.abs(np.random.randn(self.K, self.Cin, self.h, self.w)), dtype=theano.config.floatX)
            self. means_val_init = np.reshape(self.means_val_init, newshape=(self.K, self.D))
        else:
            self.means_val_init = means_val_init

        self.means_val_init[0] = 0.0 # don't care the first cluster

        # initialize psis
        # if initial values for psis are not provided, use variance of the data to initialize psis
        if psis_val_init == None:
            self.psis_val_init = np.asarray(np.abs(np.random.randn(self.K, self.D)), dtype=theano.config.floatX)
        else:
            self.psis_val_init = psis_val_init

        # empty arrays to be filled
        self.betas       = theano.shared(value=np.zeros((self.K - 1, self.M, self.D), dtype=theano.config.floatX),
                                         name='betas', borrow=True) # corresponds to the filters in DCNs
        self.latents     = theano.shared(value=np.zeros((self.K - 1, self.M, self.N), dtype=theano.config.floatX),
                                         name='latents', borrow=True) # a.k.a, hidden variables
        self.latent_covs = theano.shared(value=np.zeros((self.K - 1, self.M, self.M, self.N), dtype=theano.config.floatX),
                                         name='latent_covs', borrow=True) # covariance of hidden variables
        self.means       = theano.shared(value=np.asarray(self.means_val_init, dtype=theano.config.floatX),
                                         name='means', borrow=True) # means
        self.rs          = theano.shared(value=np.zeros((self.K, self.Nl), dtype=theano.config.floatX),
                                         name='rs', borrow=True) # responsibilities
        self.psis = theano.shared(value=np.asarray(self.psis_val_init, dtype=theano.config.floatX), name='psis', borrow=True)

        # initialize
        # if init = True, use _initialize to initialize the parameters.
        # otherwise, use ..._val_init from the input list
        if init:
            self._initialize(init_ppca)
        else:
            self.amps = theano.shared(value=np.asarray(amps_val_init, dtype=theano.config.floatX), name='amps', borrow=True)
            self.lambdas = theano.shared(value=np.asarray(lambdas_val_init, dtype=theano.config.floatX), name='lambdas', borrow=True)
            self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val_init, dtype=theano.config.floatX), name='lambdas_covs', borrow=True)
            self.covs = theano.shared(value=np.asarray(covs_val_init, dtype=theano.config.floatX), name='covs', borrow=True)
            self.inv_covs = theano.shared(value=np.asarray(inv_covs_val_init, dtype=theano.config.floatX), name='inv_covs', borrow=True)

    def _initialize(self, init_ppca, maxiter=200, tol=1e-4):

        # Randomly assign factor loadings
        if self.lambdas_val_init == None:
            lambdas_value = np.random.randn(self.K,self.D,self.M) / \
                np.sqrt(self.max_condition_number)
        else:
            lambdas_value = self.lambdas_val_init

        lambdas_value[0] = 0.0*lambdas_value[0] # don't care the first cluster
        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas', borrow=True)

        # Set initial lambda_covs, covs and compute inv_covs
        covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        lambda_covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        inv_covs_val = 0. * covs_val

        if self.em_mode == 'soft':
            # compute covs and inv_covs using matrix inversion lemma
            for k in xrange(1,self.K):
                covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T) + \
                    np.diag(self.psis_val_init[k])
                lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                psiI = inv(np.diag(self.psis_val_init[k]))
                lam  = lambdas_value[k]
                lamT = lam.T
                step = inv(np.eye(self.M) + np.dot(lamT,np.dot(psiI,lam)))
                step = np.dot(step,np.dot(lamT,psiI))
                step = np.dot(psiI,np.dot(lam,step))
                inv_covs_val[k] = psiI - step
        else:
            # compute the covs and inv_covs
            for k in range(1,self.K):
                covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                inv_covs_val[k] = np.linalg.pinv(lambda_covs_val[k])

        self.covs = theano.shared(value=np.asarray(covs_val, dtype=theano.config.floatX),
                                  name='covs', borrow=True)

        self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val, dtype=theano.config.floatX),
                                  name='lambda_covs', borrow=True)

        self.inv_covs = theano.shared(value=np.asarray(inv_covs_val, dtype=theano.config.floatX),
                                  name='inv_covs', borrow=True)

        # Randomly assign the amplitudes.
        if self.amps_val_init == None:
            amps_val = np.random.rand(self.K)
            amps_val /= np.sum(amps_val)
            self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                      name='amps', borrow=True)
        else:
            self.amps = theano.shared(value=np.asarray(self.amps_val_init, dtype=theano.config.floatX), name='amps', borrow=True)

    def take_EM_step(self):
        """
        Do one E step and then do one M step.
        """
        self._E_step()
        self._M_step()

    def _E_step(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """

        # Bottom-Up

        # compute the lambda dagger
        if self.em_mode == 'soft':
            self.betas, updates4 = theano.scan(fn=lambda l, ic: T.dot(l.T, ic),
                                     outputs_info=None,
                                     sequences=[self.lambdas[1:], self.inv_covs[1:]])
            betameans = T.sum(self.betas[:,0,:]*self.means[1:], axis=1)

            latents_conv = T.dot(self.betas[:,0,:], self.dataT)

            self.latents = latents_conv[:,None,:] - betameans.dimshuffle(0, 'x', 'x')

        elif self.E_mode == 'convolution':
            # use convolution to do compute the latents
            print 'Use convolution mode'
            self.betas, updates4 = theano.scan(fn=lambda l: T.dot(1.0/(T.dot(l.T,l)),l.T),
                                     outputs_info=None,
                                     sequences=self.lambdas[1:])

            betas = T.reshape(self.betas[:,0,:], newshape=(self.K-1, self.Cin, self.h, self.w))

            # compute E[z|x] using eq. 13
            latents_conv = conv2d(
                input=self.data_4D,
                filters=betas,
                filter_shape=(self.K-1, self.Cin, self.h, self.w),
                image_shape=(self.Ni, self.Cin, self.H, self.W),
                filter_flip=False
            )

            betameans = T.sum(self.betas[:,0,:]*self.means[1:], axis=1)

            self.latents = latents_conv - betameans.dimshuffle('x',0,'x','x')
            self.latents = self.latents.dimshuffle(1,0,2,3)
            self.latents = T.reshape(self.latents, newshape=(self.K-1,self.N))
            self.latents = self.latents.dimshuffle(0,'x',1)

        else:
            # use matrix multiplication to compute the latents
            self.betas, updates4 = theano.scan(fn=lambda l: T.dot(1.0/(T.dot(l.T,l)),l.T),
                                     outputs_info=None,
                                     sequences=self.lambdas[1:])

            betameans = T.sum(self.betas[:,0,:]*self.means[1:], axis=1)

            latents_conv = T.dot(self.betas[:,0,:], self.dataT)

            self.latents = latents_conv[:,None,:] - betameans.dimshuffle(0, 'x', 'x')

        # compute E[zz'|x] using eq. 14
        # step 1 computes E[z|x](E[z|x])'
        # step 2 computes beta_j*lambda_j
        # step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
        #                              outputs_info=None,
        #                              sequences=self.latents)

        # For latents to be positive
        self.latents = self.latents * (self.latents > 0.) + 0. * self.latents * (self.latents < 0.)
        #

        # compute the latents_covs
        step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
                                            outputs_info=None,
                                            sequences=self.latents)
        step2, updates7 = theano.scan(fn=lambda b, l: T.dot(b, l),
                                      outputs_info=None,
                                      sequences=[self.betas, self.lambdas[1:]])
        self.latent_covs, updates8_ss = theano.scan(fn=lambda s2, s1: T.eye(self.M,self.M)[:,:,None] - s2[:,:,None] + s1,
                                       outputs_info=None,
                                       sequences=[step2, step1])

        # compute log-likelihood
        self.logLs = self.compute_rs_or_logLs()

        # compute the responsibilities using logLs. For the hard EM, logLs is the negative distance from the data point to the cluster
        self.rs_conv = T.reshape(self.logLs, newshape=(self.K - 1, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        self.rs_conv = self.rs_conv.dimshuffle(1,0,2,3)

        ## coordinate descent code:
        self.rs_conv_new_2d = T.reshape(self.rs_conv.dimshuffle(1,0,2,3), newshape=(self.K - 1, self.Nl))
        self.rs_conv_new_2d_max = T.max(self.rs_conv_new_2d, axis=0)
        self.rs_conv_new_2d_temp = T.ge(self.rs_conv_new_2d,self.rs_conv_new_2d_max)
        self.rs_conv_new_2d_temp = T.cast(self.rs_conv_new_2d_temp, theano.config.floatX)
        self.rs_conv_new_temp1 = T.reshape(self.rs_conv_new_2d_temp, newshape=(self.K-1, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        self.rs_conv_new_temp1 = self.rs_conv_new_temp1.dimshuffle(1,0,2,3)

        self.rs_conv_new_temp2 = pool.max_pool_2d_same_size(input=self.rs_conv, patch_size=(2,2))
        self.rs_conv_new_temp2 = T.lt(self.rs_conv_new_temp2, 0.0)
        self.rs_conv_new_temp2 = T.cast(self.rs_conv_new_temp2, theano.config.floatX)

        self.rs_conv_new = self.rs_conv_new_temp1 * self.rs_conv_new_temp2


        # self.rs_conv = InputLayer(shape=(self.Ni, self.K-1, self.H - self.h + 1, self.W - self.w + 1), input_var=rs_conv, name=None)
        # rs_conv_layer = FeatureWTALayer(incoming=self.rs_conv, pool_size=20, axis=1)
        # rs_conv_new = rs_conv_layer.get_output_for(input=rs_conv)
        # self.rs_conv_new = T.neq(0,rs_conv_new)
        # # self.rs_conv_new = sw_maxpool_binary(input=self.rs_conv, ds=(2*self.h-1, 2*self.w-1), ignore_border=True, st=(1,1), padding=(self.h-1, self.w-1), mode='max')

        # coordinate descent code: Pool over spatial first - then Pool over channels
        # self.rs_conv_new_temp = pool.max_pool_2d_same_size(input=self.rs_conv, patch_size=(2,2))
        # self.rs_conv_new_2d = T.reshape(self.rs_conv_new_temp.dimshuffle(1,0,2,3), newshape=(self.K - 1, self.Nl))
        # self.rs_conv_new_2d_max = T.max(self.rs_conv_new_2d, axis=0)
        # self.rs_conv_new_2d_temp = T.ge(self.rs_conv_new_2d,self.rs_conv_new_2d_max)
        # self.rs_conv_new_2d_temp = T.cast(self.rs_conv_new_2d_temp, theano.config.floatX)
        # self.rs_conv_new = T.reshape(self.rs_conv_new_2d_temp, newshape=(self.K-1, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        # self.rs_conv_new = self.rs_conv_new.dimshuffle(1,0,2,3)
        #

        # coordinate descent code: Pool over channels first - then Pool over spatial
        # self.rs_conv_new_2d = T.reshape(self.rs_conv.dimshuffle(1,0,2,3), newshape=(self.K - 1, self.Nl))
        # self.rs_conv_new_2d_max = T.max(self.rs_conv_new_2d, axis=0)
        # self.rs_conv_new_2d_temp = T.ge(self.rs_conv_new_2d,self.rs_conv_new_2d_max)
        # self.rs_conv_new_2d_temp = T.cast(self.rs_conv_new_2d_temp, theano.config.floatX)
        # self.rs_conv_new_temp = T.reshape(self.rs_conv_new_2d_temp, newshape=(self.K-1, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        # self.rs_conv_new_temp = self.rs_conv_new_temp.dimshuffle(1,0,2,3)
        # self.rs_conv_new = pool.max_pool_2d_same_size(input=self.rs_conv_new_temp, patch_size=(2,2))
        #

        # full pool over channels and pool over spatial at the same time
        # self.rs_conv_new_2d = T.reshape(self.rs_conv.dimshuffle(1,0,2,3), newshape=(self.K - 1, self.Nl))
        # self.rs_conv_new_2d_max = T.max(self.rs_conv_new_2d)
        # self.rs_conv_new_2d_temp = T.ge(self.rs_conv_new_2d, self.rs_conv_new_2d_max)
        # self.rs_conv_new = T.reshape(self.rs_conv_new_2d_temp, newshape=(self.K-1, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        # self.rs_conv_new = self.rs_conv_new.dimshuffle(1,0,2,3)

        # self.rs_conv_new = sw_maxpool_binary(input=self.rs_conv, ds=(7, 7), ignore_border=True, st=(1,1), padding=(3, 3), mode='max')


        # exact rs_conv_new implementation
        # self.rs_conv_new = sw_maxpool_binary(input=self.rs_conv, ds=(2*self.h-1, 2*self.w-1), ignore_border=True, st=(1,1), padding=(self.h-1, self.w-1), mode='max')
        #
        #


        self.rs_sw_temp = T.concatenate([theano.shared(value=np.zeros((self.Ni, 1 ,self.H - self.h + 1, self.W - self.w + 1),
                                                                       dtype=theano.config.floatX),borrow=True), self.rs_conv_new],
                                        axis=1)
        self.rs_temp = T.reshape(self.rs_sw_temp.dimshuffle(1,0,2,3), newshape=(self.K, self.Nl))
        self.rs_nonzero_max = T.max(self.rs_temp[1:], axis=0)
        self.rs_zero_new = T.ge(0.0, self.rs_nonzero_max)
        self.rs_zero_new = T.cast(self.rs_zero_new, theano.config.floatX)
        self.rs = T.set_subtensor(self.rs_temp[0], self.rs_zero_new)

        # mask the latents by the responsibilities
        latents_rs = self.latents[:,0,:] * self.rs[1:]
        latents_rs = T.reshape(latents_rs, (self.K -1, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        latents_rs = T.transpose(latents_rs, (1,0,2,3))
        self.latents_rs = pool.pool_2d(input=latents_rs,
                                            ds=(2,2), ignore_border=True, mode='max')
        # self.latents_rs = self.latents_rs * 4.0

        # compute the masked logLs (only for rendered patches)
        self.logLs = 0.5*T.sum(self.logLs * self.rs[1:], axis=0)

        # compute the pi's
        self.sumrs = T.sum(self.rs, axis=1)
        self.amps_new, updates18 = theano.scan(fn=lambda s: s / float(self.Nl),
                                outputs_info=None,
                                sequences=self.sumrs)

        self.indx_good_cluster = T.cast((self.amps_new[1:] > 1.0/self.N).nonzero(), 'int32')[0]

    def _Top_Down(self):
        # Top-Down
        lambdalatents_E, updates910 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[self.lambdas[1:], self.latents])
        xT = lambdalatents_E + self.means[1:,:,None]
        xT = self.rs[1:,None,:] * xT
        self.xT = T.sum(xT, axis=0)

    def _M_step(self):
        """
        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """
        if self.update_mode == 'Ross_Fadely':
            # update means - implicitly included in eq. 15
            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[self.lambdas, self.latents])

            means_new, updates10 = theano.scan(fn=lambda resp, llt, s: T.sum(resp * (self.dataT - llt), axis=1) / s,
                                     outputs_info=None,
                                     sequences=[self.rs[1:], lambdalatents[1:], self.sumrs[1:]])

            self.means_new = T.set_subtensor(self.means[1:], means_new)

            # update lambdas using eq.15
            zeroed, updates11 = theano.scan(fn=lambda m: self.dataT - m[:,None],
                                 outputs_info=None,
                                 sequences=self.means_new)

            lambdas_new, updates12 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixInverse()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[self.latents[1:], self.rs[1:], self.latent_covs[1:], zeroed[1:]])

            self.lambdas_new = T.set_subtensor(self.lambdas[1:], lambdas_new)

            # update psis using eq. 16
            psis, updates13 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                               outputs_info=None,
                               sequences=[self.rs, zeroed, lambdalatents, self.sumrs])
        else:
            # make latents_tilde (according to the equations at the top of page 7 in Hinton's paper)
            latents_tilde = T.concatenate([self.latents,
                                           theano.shared(value=np.ones((self.K-1, 1 ,self.N),
                                                                       dtype=theano.config.floatX),borrow=True)],
                                          axis=1)

            # make latents_covs_tilde (according to the equations at the top of page 7 in Hinton's paper)
            col1 = T.concatenate([self.latent_covs, (self.latents[:,:,None,:]).dimshuffle((0,2,1,3))], axis=1)
            latent_covs_tilde = T.concatenate([col1, latents_tilde[:,:,None,:]], axis=2)

            # compute lambdas_tilde (equation 15 in Hinton's paper)
            dataT = T.tile(self.dataT[None,:,:], reps=[self.K-1,1,1])
            lambdas_tilde, updates22 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixPinv()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[latents_tilde[self.indx_good_cluster], self.rs[self.indx_good_cluster + 1], latent_covs_tilde[self.indx_good_cluster], dataT[self.indx_good_cluster]])

            # update lambdas and means
            lambdas_new = lambdas_tilde[:,:,0:self.M]
            self.lambdas_new = T.set_subtensor(self.lambdas[self.indx_good_cluster + 1], lambdas_new)
            means_new = lambdas_tilde[:,:,self.M]
            self.means_new = T.set_subtensor(self.means[self.indx_good_cluster + 1], means_new)

            # compute psis
            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[lambdas_tilde, latents_tilde[self.indx_good_cluster]])

            psis, update23 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                                         outputs_info=None,
                                         sequences=[self.rs[self.indx_good_cluster + 1], dataT[self.indx_good_cluster], lambdalatents, self.sumrs[self.indx_good_cluster + 1]])

        maxpsi, updates14 = theano.scan(fn=lambda p: T.max(p),outputs_info=None, sequences=psis)

        maxlam, updates15 = theano.scan(fn=lambda l: T.max(T.sum(l * l, axis=0)),
                             outputs_info=None,
                             sequences=self.lambdas_new[self.indx_good_cluster + 1])
        minpsi, updates16 = theano.scan(fn=lambda maxp, maxl: T.max([maxp, maxl]) / self.max_condition_number,
                             outputs_info=None,
                             sequences=[maxpsi, maxlam])
        self.psis_new, updates17   = theano.scan(fn=lambda p, minp: T.clip(p, minp, np.Inf),
                                  outputs_info=None,
                                  sequences=[psis, minpsi])
        # make all elements in each rows of psis the same
        if self.PPCA:
            self.psis_new, updates19 = theano.scan(fn=lambda psn: T.mean(psn)*T.ones(self.D),
                                                   outputs_info=None,
                                                   sequences=self.psis_new)

        # make psi for each cluster the same
        if self.lock_psis:
            self.psis_new = T.dot(self.sumrs[self.indx_good_cluster + 1],self.psis_new)/T.sum(self.sumrs[self.indx_good_cluster + 1])
            self.psis_new = (self.psis_new).dimshuffle('x',0)
            self.psis_new = T.tile(self.psis_new,(self.K, 1))

        # update covs
        self._update_covs()

    def _update_covs(self):
        """
        Update covs and inv_covs
        """

        # total cov = lambda*lambda' + psi
        self.lambda_covs_new, updates20 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas_new])

        if self.em_mode == 'soft':
            covs_new, updates1 = theano.scan(fn=lambda lc, p: lc + diag(p),
                                              outputs_info=None,
                                              sequences=[self.lambda_covs_new[1:], self.psis_new[1:]])
            self.covs_new = T.set_subtensor(self.covs[1:], covs_new)
            # compute the inv_covs using the matrix inversion lemma
            inv_covs_new, updates2 = theano.scan(fn=lambda l, p: MatrixInverse()(diag(p)) - T.dot(MatrixInverse()(diag(p)),T.dot(l,T.dot(MatrixInverse()(T.eye(self.M) + T.dot(T.transpose(l),T.dot(MatrixInverse()(diag(p)),l))),T.dot(T.transpose(l),MatrixInverse()(diag(p)))))),
                                              outputs_info=None,
                                              sequences=[self.lambdas_new[1:], self.psis_new[1:]])
            self.inv_covs_new = T.set_subtensor(self.inv_covs[1:], inv_covs_new)
        else:
            covs_new, updates1 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas_new[1:]])
            self.covs_new = T.set_subtensor(self.covs[1:], covs_new)
            inv_covs_new, updates2 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l))/T.sqr(T.sum(T.sqr(l))),
                                              outputs_info=None,
                                              sequences=self.lambdas_new[1:])
            self.inv_covs_new = T.set_subtensor(self.inv_covs[1:], inv_covs_new)


    def compute_rs_or_logLs(self):
        """
        This function calculate responsibilities for Soft EM and log likelihoods for Hard EM. The calculation is done for each datum
        under each component.
        """
        if self.em_mode == 'soft':
            # compute the responsibilities
            logrs, updates3 = theano.scan(fn=lambda a, c, m, ic: T.log(a) - float(0.5 * np.log(2 * np.pi) * self.D) -
                                                                 0.5 * 2 * T.sum(T.log(diag(Cholesky()(c)))) -
                                                                 0.5 * T.sum((self.data - m).T * T.dot(ic, (self.data - m).T),
                                                                             axis=0),
                                outputs_info=None,
                                sequences=[self.amps[1:], self.covs[1:], self.means[1:], self.inv_covs[1:]])
            L = self._log_sum(logrs)
            logrs -= L[None, :]
            if self.rs_clip > 0.0:
                logrs = T.clip(logrs,T.log(self.rs_clip),np.Inf)

            return T.exp(logrs) # return the responsibilities
        else:
            # compute the log likelihoods. In the hard EM, the log likelihoods are the distances from data points to clusters
            ortho_vec, updates333 = theano.scan(fn=lambda m,lam, z: (self.data - m).T - T.dot(lam, z),
                                outputs_info=None,
                                sequences=[self.means[1:], self.lambdas[1:], self.latents])

            L, updates337 = theano.scan(fn=lambda d: -T.sum(d*d, axis=0),
                                outputs_info=None,
                                sequences=ortho_vec)
            return L # return the log likelihoods

    # def _calc_probs(self):
    #     """
    #     Calculate log likelihoods, responsibilities for each datum
    #     under each component.
    #     """
    #     if self.em_mode == 'soft':
    #         logrs, updates3 = theano.scan(fn=lambda a, c, m, ic: T.log(a) - float(0.5 * np.log(2 * np.pi) * self.D) -
    #                                                              0.5 * 2 * T.sum(T.log(diag(Cholesky()(c)))) -
    #                                                              0.5 * T.sum((self.data - m).T * T.dot(ic, (self.data - m).T),
    #                                                                          axis=0),
    #                             outputs_info=None,
    #                             sequences=[self.amps[1:], self.covs[1:], self.means[1:], self.inv_covs[1:]])
    #         L = self._log_sum(logrs)
    #         logrs -= L[None, :]
    #         if self.rs_clip > 0.0:
    #             logrs = T.clip(logrs,T.log(self.rs_clip),np.Inf)
    #
    #         return T.exp(logrs)
    #     else:
    #         ortho_vec, updates333 = theano.scan(fn=lambda m,lam: (self.data - m).T - T.dot(T.dot(lam, lam.T),(self.data - m).T)/T.sum(lam*lam),
    #                             outputs_info=None,
    #                             sequences=[self.means[1:], self.lambdas[1:]])
    #
    #         rs, updates3 = theano.scan(fn=lambda d: -T.sum(d*d, axis=0),
    #                             outputs_info=None,
    #                             sequences=ortho_vec)
    #         return rs

    def _log_sum(self,loglikes):
        """
        Calculate sum of log likelihoods
        """
        a = T.max(loglikes, axis=0)
        return a + T.log(T.sum(T.exp(loglikes - a[None, :]), axis=0))