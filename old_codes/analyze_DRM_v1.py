__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

import os

from PIL import Image

import numpy as np

from theano.tensor.shared_randomstreams import RandomStreams
from old_codes.load_data_stable import DATA

import theano
import theano.tensor as T

from theano.misc.pkl_utils import load


# from guppy import hpy; h=hpy()

class Probe(object):
    '''
    This probe class is used to analyze trained models
    '''

    def __init__(self, model, output_dir):
        self.model = model
        self.output_dir = output_dir

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

    def reconstruct_images(self, input, Ni):
        getReconstruction = theano.function([self.model.x, self.model.is_train, self.model.momentum_bn], self.model.conv1.data_reconstructed, on_unused_input='warn')
        I_hat = getReconstruction(input, 0, 1.0)
        I_hat = I_hat.transpose((0, 2, 3, 1))
        I_hat = I_hat[0:Ni]
        return I_hat

    def sample_images(self, input, Ni):
        getSamples = theano.function([self.model.x, self.model.is_train, self.model.momentum_bn], self.model.conv1.data_reconstructed, on_unused_input='warn')
        I_hat = getSamples(input, 0, 1.0)
        I_hat = I_hat.transpose((0, 2, 3, 1))
        I_hat = I_hat[0:Ni]
        return I_hat


def plot_image_table(imgs, output_dir, file_name):
    N = imgs.shape[0]
    for i in xrange(N):
        imshow(imgs[i, :, :, 0], cmap='gray')
        axis('off')
        out_path = os.path.join(output_dir, '%s_%i.jpg' % (file_name,i))
        savefig(out_path)
        close()
    sub_x_size = int(sqrt(N))
    sub_y_size = int(sqrt(N))
    img = Image.new('RGB', (800 * sub_x_size, 600 * sub_y_size))
    for i in xrange(N):
        this_img = Image.open(os.path.join(output_dir, '%s_%i.jpg' % (file_name,i)))
        x_loc = (i % sub_x_size) * 800
        y_loc = (i / sub_y_size) * 600
        img.paste(this_img, (x_loc, y_loc))
    fig = imshow(img)
    axis('off')
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    savefig(os.path.join(output_dir, '%ss.pdf'%file_name))
    savefig(os.path.join(output_dir, '%ss.jpg' %file_name))
    close()


if __name__ == '__main__':
    N = 500
    Ni = 36

    training_name = 'MNIST_Conv_Small_5_Layers_nofactor_semisupervised_Ni60000_Nlabel60000_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_5_bnBU_08616'

    model_file_name = 'model_best.zip'

    root_dir = '/home/ubuntu/research_results/EM_results'

    model_dir = os.path.join(root_dir, training_name, 'Train/params', model_file_name)

    output_dir = os.path.join(root_dir, training_name, 'Train', 'model_analysis_results')

    data_dir = '/home/ubuntu/repos/em_for_drm/data/mnist.pkl.gz'

    mnist = DATA(dataset_name='MNIST', data_mode='all', data_dir=data_dir, Nlabeled=50000, Ni=50000, Cin=1, H=28, W=28,
                 seed=5)

    dat = mnist.dtrain[0:N]

    with open(model_dir, 'rb') as f:
        model = load(f)

    probe_no_Softmax = Probe(model=model, output_dir=output_dir)
    probe_no_Softmax.model.train_mode = 'semisupervised'

    reconst_dir = os.path.join(output_dir, 'reconstructed_images')
    if not os.path.exists(reconst_dir):
        os.makedirs(reconst_dir)

    # plot original images
    I = dat[0:Ni]
    I = I.transpose((0, 2, 3, 1))
    plot_image_table(imgs=I, output_dir=reconst_dir, file_name='OriginalImage')

    # # make reconstructed images with no Softmax
    # I_hat_no_Softmax = probe_no_Softmax.reconstruct_images(input=dat, Ni=Ni)
    # plot_image_table(imgs=I_hat_no_Softmax, output_dir=reconst_dir, file_name='ReconstructedImageNoSoftmax')
    #
    # # do reconstuction from the Softmax
    # probe_with_Softmax = Probe(model=model, output_dir=output_dir)
    # probe_with_Softmax.model.train_mode = 'semisupervised'
    #
    # one_hot_encoding =  T.extra_ops.to_one_hot(probe_with_Softmax.model.softmax_layer.y_pred, 10)
    # input_to_Top_Down = T.dot(one_hot_encoding,(probe_with_Softmax.model.softmax_layer.W).T)
    # input_to_Top_Down = input_to_Top_Down.dimshuffle(0,1,'x','x')
    #
    # probe_with_Softmax.model.layers[0]._E_step_Top_Down_Reconstruction(mu_cg=input_to_Top_Down, ta_hat=probe_with_Softmax.model.layers[0].masked_mat,
    #                                                                    layer_loc='intermediate', denoising=probe_with_Softmax.model.denoising)
    # for i in xrange(1, probe_with_Softmax.model.N_layer):
    #     probe_with_Softmax.model.layers[i]._E_step_Top_Down_Reconstruction(
    #         mu_cg=probe_with_Softmax.model.layers[i - 1].data_reconstructed,
    #         ta_hat=probe_with_Softmax.model.layers[i].masked_mat,
    #         layer_loc='intermediate', denoising=probe_with_Softmax.model.denoising)
    #
    # I_hat_with_Softmax = probe_with_Softmax.reconstruct_images(input=dat, Ni=Ni)
    # plot_image_table(imgs=I_hat_with_Softmax, output_dir=reconst_dir, file_name='ReconstructedImageWithSoftmax')


    # sample images from the model
    probe_Sample = Probe(model=model, output_dir=output_dir)
    probe_Sample.model.train_mode = 'semisupervised'
    
    seed = 2
    np.random.seed(seed)
    srng = RandomStreams(seed=np.random.randint(2 ** 30))
    class_index = srng.random_integers(size=(500,), low=0, high=9, ndim=None,
                                            dtype='int32')
    class_one_hot_encoding = T.extra_ops.to_one_hot(class_index, 10)
    probe_Sample.model.layers[0].mu_cg_gen = T.dot(class_one_hot_encoding, (probe_Sample.model.softmax_layer.W).T)
    probe_Sample.model.layers[0].mu_cg_gen = probe_Sample.model.layers[0].mu_cg_gen.dimshuffle(0, 1, 'x', 'x')

    #import pdb; pdb.set_trace()

    # probe_Sample.model.layers[0].uniform_mask_real_val_gen = probe_Sample.model.srng.uniform(size=probe_Sample.model.layers[0].latents_shape,
    #                                                                                          low=0.0, high=1.0, ndim=None, dtype=theano.config.floatX)
    #
    # probe_Sample.model.layers[0].uniform_mask_gen = T.grad(T.sum(
    #     T.max(pool.pool_2d(input=probe_Sample.model.layers[0].uniform_mask_real_val_gen, ds=(2, 2), ignore_border=True, mode='max'),
    #           axis=1)), wrt=probe_Sample.model.layers[0].uniform_mask_real_val_gen)

    probe_Sample.model.layers[0]._E_step_Top_Down_Reconstruction(mu_cg=probe_Sample.model.layers[0].mu_cg_gen,
                                                   ta_hat=probe_Sample.model.layers[0].masked_mat,
                                                   layer_loc='intermediate', denoising=probe_Sample.model.denoising)
    for i in xrange(1, probe_Sample.model.N_layer):
        # probe_Sample.model.layers[i].uniform_mask_real_val_gen = probe_Sample.model.srng.uniform(size=probe_Sample.model.layers[i].latents_shape, low=0.0, high=1.0, ndim=None, dtype=theano.config.floatX)
        # probe_Sample.model.layers[i].uniform_mask_gen = T.grad(T.sum(T.max(
        #     pool.pool_2d(input=probe_Sample.model.layers[i].uniform_mask_real_val_gen, ds=(2, 2), ignore_border=True, mode='max'),
        #     axis=1)), wrt=probe_Sample.model.layers[i].uniform_mask_real_val_gen)
        probe_Sample.model.layers[i]._E_step_Top_Down_Reconstruction(mu_cg=probe_Sample.model.layers[i - 1].data_reconstructed,
                                                       ta_hat=probe_Sample.model.layers[i].masked_mat,
                                                       layer_loc='intermediate', denoising=probe_Sample.model.denoising)

    I_sampled = probe_Sample.sample_images(input=dat, Ni=Ni)
    plot_image_table(imgs=I_sampled, output_dir=reconst_dir, file_name='SampledImageWithSoftmax')






        # # Sampled Images
    # sample_dir = os.path.join(model_dir, 'sampled_images')
    # if not os.path.exists(sample_dir):
    #     os.makedirs(sample_dir)
    #
    # model = Sample_from_SRM(batch_size=Ni, Cin=Cin, W=W, H=H, em_mode=em_mode,
    #                                 use_non_lin=use_non_lin, param_dir=param_dir)
    #
    # z_val = np.asarray(np.random.randn(Ni, K, (H-h+1)/2, (W-w+1)/2), dtype=np.float32)
    # print 'Shape of z_val'
    # print np.shape(z_val)
    #
    # A = T.tensor4('A')
    # rs = T.grad(T.sum(T.max(pool.pool_2d(input=A, ds=(2,2), ignore_border=True, mode='max'), axis=1)), wrt=A)
    # getrs = theano.function([A], rs)
    #
    # rs_val = getrs(np.asarray(np.random.randn(Ni, K, H-h+1, W-w+1), dtype=np.float32))
    #
    # print 'Shape of rs_val'
    # print np.shape(rs_val)
    # print rs_val
    #
    # I_hat = sample_from_model(sample_dir=sample_dir, model=model, Ni=Ni, z=z_val, rs=rs_val)

