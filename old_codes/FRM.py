__author__ = 'minhtannguyen'

#######################################################################################################################
# get rid of no rendering channel
# do both soft and hard on c
#######################################################################################################################


import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import numpy as np
np.set_printoptions(threshold='nan')

from scipy.linalg import inv

import theano
import theano.tensor as T

from theano.tensor.nlinalg import MatrixPinv, diag, MatrixInverse
from theano.tensor.slinalg import Cholesky


## from lasagne.layers import InputLayer, FeatureWTALayer

class FRM(object):
    """
    EM Algorithm for the Convolutional Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes
    'data_4D: data in 4-D (N,D,H,W)

    """

    def __init__(self, data, labels, K, D, M, Ni,
             lambdas_val_init=None, means_val_init=None, amps_val_init=None, psis_val_init=None,
             PPCA=False, lock_psis=True,
             rs_clip = 0.0,
             max_condition_number=1.e3):

        # required
        self.K     = K
        self.M     = M
        self.N     = Ni
        self.D     = D
        self.labels = labels

        self.means_val_init = means_val_init
        self.lambdas_val_init = lambdas_val_init
        self.amps_val_init = amps_val_init
        self.psis_val_init = psis_val_init

        # options
        self.PPCA = PPCA
        self.lock_psis = lock_psis
        self.rs_clip = rs_clip
        self.max_condition_number = max_condition_number
        assert rs_clip >= 0.0

        # set up data
        self.data = data

        self._initialize()

    def _initialize(self):
        #
        # initialize pi's, means, lambdas, psis, lambda_covs, covs, and inv_covs
        #

        # initialize the pi's (a.k.a the priors)
        # if initial values for pi's are not provided, randomly initialize pi's
        if self.amps_val_init == None:
            amps_val = np.random.rand(self.K)
            amps_val /= np.sum(amps_val)

        else:
            amps_val = self.amps_val_init

        self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                      name='amps', borrow=True)

        # initialize the means
        # if initial values for means are not provided, randomly initialize means
        if self. means_val_init == None:
            means_val = np.asarray(np.abs(np.random.randn(self.K, self.D)), dtype=theano.config.floatX)
        else:
            means_val = self.means_val_init

        self.means = theano.shared(value=np.asarray(means_val, dtype=theano.config.floatX),
                                         name='means', borrow=True) # means

        # initialize the lambdas
        # if initial values for lambdas are not provided, randomly initialize lambdas
        if self.lambdas_val_init == None:
            lambdas_value = np.random.randn(self.K,self.D,self.M) / \
                np.sqrt(self.max_condition_number)
        else:
            lambdas_value = self.lambdas_val_init

        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas', borrow=True)

        # initialize the psis
        # if initial values for psis are not provided, randomly initialize psis
        if self.psis_val_init == None:
            psis_value = np.asarray(np.abs(np.random.randn(self.K, self.D)), dtype=theano.config.floatX)
        else:
            psis_value = self.psis_val_init

        self.psis = theano.shared(value=np.asarray(psis_value, dtype=theano.config.floatX), name='psis', borrow=True)


    def take_EM_step(self):
        """
        Do one E step and then do one M step.
        """
        self._E_step()
        self._M_step()

    def _E_step(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """
        # compute the responsibilities and log-likelihood using eq. 12

        self._update_covs()
        self.logLs, self.rs = self._calc_probs()

        # compute the lambda dagger
        self.betas, _ = theano.scan(fn=lambda l, ic: T.dot(l.T, ic),
                                 outputs_info=None,
                                 sequences=[self.lambdas, self.inv_covs])

        # compute E[z|x] using eq. 13
        self.latents, _ = theano.scan(fn=lambda b, m: T.dot(b, self.data.T - m[:, None]),
                                   outputs_info=None,
                                   sequences=[self.betas, self.means])

        # compute E[zz'|x] using eq. 14
        # step 1 computes E[z|x](E[z|x])'
        # step 2 computes beta_j*lambda_j
        step1, _ = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
                                     outputs_info=None,
                                     sequences=self.latents)
        step2, _ = theano.scan(fn=lambda b, l: T.dot(b, l),
                                      outputs_info=None,
                                      sequences=[self.betas, self.lambdas])
        self.latent_covs, _ = theano.scan(fn=lambda s2, s1: T.eye(self.M,self.M)[:,:,None] - s2[:,:,None] + s1,
                                       outputs_info=None,
                                       sequences=[step2, step1])

        # compute the pi's
        self.sumrs = T.sum(self.rs,axis=1)
        self.amps_new, _ = theano.scan(fn=lambda s: s / float(self.N),
                                outputs_info=None,
                                sequences=self.sumrs)

    def _M_step(self):
        """
        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """

        latents_tilde = T.concatenate([self.latents,
                                           theano.shared(value=np.ones((self.K, 1 ,self.N),
                                                                       dtype=theano.config.floatX),borrow=True)],
                                          axis=1)

        col1 = T.concatenate([self.latent_covs, (self.latents[:,:,None,:]).dimshuffle((0,2,1,3))], axis=1)
        latent_covs_tilde = T.concatenate([col1, latents_tilde[:,:,None,:]], axis=2)
        lambdas_tilde, _ = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixPinv()(T.dot(lt_covs, resp))),
                                   outputs_info=None,
                                   sequences=[latents_tilde, self.rs, latent_covs_tilde],
                                   non_sequences=self.data.T)
        self.lambdas_new = lambdas_tilde[:,:,0:self.M]
        self.means_new = lambdas_tilde[:,:,self.M]
        lambdalatents, _ = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                    outputs_info=None,
                                    sequences=[lambdas_tilde, latents_tilde])
        psis, _ = theano.scan(fn=lambda resp, llt, s, z: T.dot((z - llt) * z, resp) / s,
                                     outputs_info=None,
                                     sequences=[self.rs, lambdalatents, self.sumrs],
                                     non_sequences=self.data.T)

        maxpsi, _ = theano.scan(fn=lambda p: T.max(p),outputs_info=None, sequences=psis)

        maxlam, _ = theano.scan(fn=lambda l: T.max(T.sum(l * l, axis=0)),
                             outputs_info=None,
                             sequences=self.lambdas_new)
        minpsi, _ = theano.scan(fn=lambda maxp, maxl: T.max([maxp, maxl]) / self.max_condition_number,
                             outputs_info=None,
                             sequences=[maxpsi, maxlam])
        self.psis_new, _ = theano.scan(fn=lambda p, minp: T.clip(p, minp, np.Inf),
                                  outputs_info=None,
                                  sequences=[psis, minpsi])

        # make all elements in each rows of psis the same
        if self.PPCA:
            self.psis_new, _ = theano.scan(fn=lambda psn: T.mean(psn)*T.ones(self.D),
                                                   outputs_info=None,
                                                   sequences=self.psis_new)
        # make psi for each cluster the same
        if self.lock_psis:
            self.psis_new = T.dot(self.sumrs,self.psis_new)/T.sum(self.sumrs)
            self.psis_new = (self.psis_new).dimshuffle('x',0)
            self.psis_new = T.tile(self.psis_new,(self.K, 1))

        # update covs
        # self._update_covs()

    def _calc_probs(self):
        """
        Calculate log likelihoods, responsibilites for each datum
        under each component.
        """

        logrs, _ = theano.scan(fn=lambda a, c, m, ic, x: T.log(a) - float(0.5 * np.log(2 * np.pi) * self.D) - 0.5 * 2 * T.sum(T.log(diag(Cholesky()(c)))) - 0.5 * T.sum((x - m).T * T.dot(ic, (x - m).T), axis=0),
                            outputs_info=None,
                            sequences=[self.amps, self.covs, self.means, self.inv_covs],
                            non_sequences=self.data)

        L = self._log_sum(logrs)
        logrs -= L[None, :]
        if self.rs_clip > 0.0:
            logrs = T.clip(logrs,T.log(self.rs_clip),np.Inf)

        return L, T.exp(logrs)

    def _log_sum(self,loglikes):
        """
        Calculate sum of log likelihoods
        """
        a = T.max(loglikes, axis=0)
        return a + T.log(T.sum(T.exp(loglikes - a[None, :]), axis=0))


    def _update_covs(self):
        """
        Update self.cov for responsibility, logL calc
        """

        # total cov = lambda*lambda' + psi
        self.lambda_covs, _ = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas])
        self.covs, _ = theano.scan(fn=lambda lc, p: lc + diag(p),
                                          outputs_info=None,
                                          sequences=[self.lambda_covs, self.psis])

       # compute the inv_covs using the matrix inversion lemma
        self.inv_covs, _ = theano.scan(fn=lambda l, p: diag(1.0/p) - T.dot(diag(1.0/p),T.dot(l,T.dot(1.0/(T.eye(self.M) + T.dot(T.transpose(l),T.dot(diag(1.0/p),l))),T.dot(T.transpose(l),diag(1.0/p))))),
                                          outputs_info=None,
                                          sequences=[self.lambdas, self.psis])

    def predict(self, x, dat):
        """
        Predict the labels of new data
        """
        self._E_step()
        getResults = theano.function([], [self.rs, self.means],
                                         updates=[], on_unused_input='ignore',
                                         givens={x:dat})
        rs_val, means_val = getResults()
        labels = np.argmax(rs_val,0)
        return labels, means_val, rs_val