__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')

from train_model import *
from probe_model_v4 import *
from old_codes.nn_functions_stable import LogisticRegressionForSemisupervised

if __name__ == '__main__':
    data_mode = 'all'
    em_mode = 'hard'
    stop_mode = 'NLL'
    train_method = 'Softmax'
    use_non_lin = 'None'

    Ni = 3
    Nsample = 3

    learning_rate = 0.001

    Cin1 = 1 # depth of filters
    H1 = 10 # width of input images
    W1 = 10 # height of input images
    K1 = 2
    h1 = 3
    w1 = 3
    M1 = 1
    D1 = Cin1*h1*w1

    # h2 = 3
    # w2 = 3
    # K2 = 2
    # M2 = 1
    # H2 = (H1 - h1 + 1)/2
    # W2 = (W1 - w1 + 1)/2
    # Cin2 = K1
    # D2 = Cin2*h2*w2

    maxiter = 20
    tol = 1e-8
    verbose = True
    batch_size = 2
    max_epochs = 400

    seed = 5
    np.random.seed(seed)

    # output_dir = '/home/ubuntu/research_results/EM_results/DRM_EG_unsupervised_2_layers_5x5x20_3x3x50_b250_hard_max_t_soft_c_TopDown_lr_0_001_051016'
    data_dir = '/home/ubuntu/repos/em_for_drm/data/mnist.pkl.gz'
    # param_dir = os.path.join(output_dir,'params','EM_results_epoch_57.npz')

    dtrain = np.asarray(np.reshape(np.arange(Ni*H1*W1), newshape=(Ni, Cin1, H1, W1)), dtype=np.float32)
    dtest = []
    dvalid = []
    train_label = [2,0,1]
    test_label = []
    valid_label = []

    amps_val_init_1 = np.random.rand(K1)
    amps_val_init_1 /= np.sum(amps_val_init_1)

    means_val_init_1 = np.reshape(np.arange(K1*Cin1*h1*w1), newshape=(K1, Cin1, h1, w1))
    means_val_init_1 = np.reshape(means_val_init_1, newshape=(K1, D1))

    lambdas_val_init_1 = np.reshape(np.arange(K1*Cin1*h1*w1), newshape=(K1, D1, M1))

    # amps_val_init_2 = np.random.rand(K2)
    # amps_val_init_2 /= np.sum(amps_val_init_2)
    #
    # means_val_init_2 = np.reshape(np.arange(K2*Cin2*h2*w2), newshape=(K2, Cin2, h2, w2))
    # means_val_init_2 = np.reshape(means_val_init_2, newshape=(K2, D2))
    #
    # lambdas_val_init_2 = np.reshape(np.arange(K2*Cin2*h2*w2), newshape=(K2, D2, M2))
    ###################################################################################################################
    # Theano computation
    ###################################################################################################################
    x = T.tensor4('x')
    y = T.ivector('y')

    conv1 = CRM(data_4D=x, labels=y, K=K1, M=M1, W=W1, H=H1, w=w1, h=h1, Cin=Cin1, Ni=Ni,
                     amps_val_init=amps_val_init_1, lambdas_val_init=lambdas_val_init_1, psis_val_init=None, means_val_init=means_val_init_1,
                     PPCA=False, lock_psis=True, em_mode=em_mode, use_non_lin=use_non_lin,
                     rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

    conv1._E_step()

    n_softmax = K1*(H1 - h1 + 1)*(W1 - w1 + 1)/4

    softmax_input = conv1.latents_rs.flatten(2)

    # classify the values of the fully-connected sigmoidal layer
    softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=2)

    cost = softmax_layer.negative_log_likelihood(y)

    get_result_layer1 = theano.function([x, y], [conv1.lambdas, conv1.betas, conv1.betas_4D, conv1.latents_conv,
                                              conv1.means, conv1.betameans, conv1.latents_4D, conv1.latents_2D,
                                              conv1.data, conv1.logLs_beforepool, conv1.max_over_t_input,
                                              conv1.max_over_t_mask, conv1.soft_over_c_input, conv1.soft_over_c,
                                              conv1.soft_c_mask, conv1.max_val_var, conv1.diff_val_var, conv1.sum_t_var,
                                              conv1.sum_var, conv1.sum_c_var, conv1.rs, conv1.logLs,
                                              conv1.latents_rs_before_pool, conv1.latents_rs_before_pool_reshape,
                                              conv1.latents_rs, conv1.sumrs, conv1.amps_new, conv1.neg_norm, conv1.diff,
                                              conv1.max_over_a_mask_2D, conv1.max_over_a_mask,
                                              softmax_layer.gammas, softmax_layer.y_pred, softmax_layer.gammas_semisupervised,
                                              cost, softmax_layer.Nlabeled], on_unused_input='ignore')

    [lambdas1, betas1, betas_4D1, latents_conv1, means1, betameans1, latents_4D1,
     latents_2D1, data1, logLs_beforepool1, max_over_t_input1,
     max_over_t_mask1, soft_over_c_input1, soft_over_c, soft_c_mask1, max_val_var1, diff_val_var1, sum_t_var1,
     sum_var1, sum_c_var1, rs1, logLs1, latents_rs_before_pool1, latents_rs_before_pool_reshape1, latents_rs1,
     sumrs1, amps_new1, neg_norm1, diff1, max_over_a_mask_2D1, max_over_a_mask1, gammas, y_pred, gammas_semisupervised,
     cost_val, Nlabeled] = get_result_layer1(dtrain, train_label)

    print 'Shape of Lambdas 1'
    print np.shape(lambdas1)
    print 'Value of Lambdas 1'
    print lambdas1

    print 'Shape of Betas 1'
    print np.shape(betas1)
    print 'Value of Betas 1'
    print betas1

    print 'Shape of Betas_4D 1'
    print np.shape(betas_4D1)
    print 'Value of Betas_4D 1'
    print betas_4D1

    print 'Shape of dtrain'
    print np.shape(dtrain)
    print 'Value of dtrain'
    print dtrain

    print 'Shape of Latents_Conv 1'
    print np.shape(latents_conv1)
    print 'Value of Latents_Conv 1'
    print latents_conv1

    print 'Shape of Means 1'
    print np.shape(means1)
    print 'Value of Means 1'
    print means1

    print 'Shape of Betasmeans 1'
    print np.shape(betameans1)
    print 'Value of Betameans 1'
    print betameans1

    print 'Shape of Latents_4D 1'
    print np.shape(latents_4D1)
    print 'Value of Latents_4D 1'
    print latents_4D1[:,0,:,:]

    print 'Shape of Latents_2D 1'
    print np.shape(latents_2D1)
    print 'Value of Latents_2D 1'
    print latents_2D1[0,:]

    print 'Value of data1'
    print data1[60:70]
    print 'Shape of data1'
    print np.shape(data1)

    print 'Value of max_over_t_mask1'
    print max_over_t_mask1[0,:,:,0:4]
    print 'Shape of max_over_t_mask1'
    print np.shape(max_over_t_mask1)

    print 'Value of soft_over_c_input1'
    print soft_over_c_input1[0,:,:,0:4]
    print 'Shape of soft_over_c_input1'
    print np.shape(soft_over_c_input1)

    print 'Value of max_over_t_input1'
    print max_over_t_input1[0,:,:,0:4]
    print 'Shape of max_over_t_input1'
    print np.shape(max_over_t_input1)

    print 'Value of max_val_var1'
    print max_val_var1[0,:,0:4]
    print 'Shape of max_val_var1'
    print np.shape(max_val_var1)

    print 'Value of diff_val_var1'
    print diff_val_var1[0,:,:,0:4]
    print 'Shape of diff_val_var1'
    print np.shape(diff_val_var1)

    print 'Value of sum_t_var1'
    print sum_t_var1[0,:,:,0:4]
    print 'Shape of sum_t_var1'
    print np.shape(sum_t_var1)

    print 'Value of sum_c_var1'
    print sum_c_var1
    print 'Shape of sum_c_var1'
    print np.shape(sum_c_var1)

    print 'Value of soft_over_c'
    print soft_over_c[0,:,:,0:8]
    print 'Shape of soft_over_c'
    print np.shape(soft_over_c)

    print 'Value of sum_var1'
    print sum_var1
    print 'Shape of sum_var1'
    print np.shape(sum_var1)

    print 'Value of soft_c_mask1'
    print soft_c_mask1
    print 'Shape of soft_c_mask1'
    print np.shape(soft_c_mask1)

    print 'Value of logLs1'
    print logLs1
    print 'Shape of logLs1'
    print np.shape(logLs1)

    print 'Value of rs1'
    print rs1[1,:]
    print 'Shape of rs1'
    print np.shape(rs1)

    print 'Shape of Latents_2D 1'
    print np.shape(latents_2D1)
    print 'Value of Latents_2D 1'
    print latents_2D1[1,:]

    print 'Value of latents_rs_before_pool1'
    print latents_rs_before_pool1
    print 'Shape of latents_rs_before_pool1'
    print np.shape(latents_rs_before_pool1)

    print 'Value of latents_rs_before_pool_reshape1'
    print latents_rs_before_pool_reshape1
    print 'Shape of latents_rs_before_pool_reshape1'
    print np.shape(latents_rs_before_pool_reshape1)

    print 'Value of latents_rs1'
    print latents_rs1
    print 'Shape of latents_rs1'
    print np.shape(latents_rs1)

    print 'Value of rs1'
    print rs1
    print 'Shape of rs1'
    print np.shape(rs1)

    print 'Value of sumrs1'
    print sumrs1
    print 'Shape of sumrs1'
    print np.shape(sumrs1)

    print 'Value of amps_new1'
    print amps_new1
    print 'Shape of amps_new1'
    print np.shape(amps_new1)

    print 'Value of data1'
    print data1[0:10]
    print 'Shape of data1'
    print np.shape(data1)

    print 'Shape of neg_norm1'
    print np.shape(neg_norm1)
    print 'Value of neg_norm1'
    print neg_norm1[0:10]

    print 'Value of logLs_beforepool1'
    print logLs_beforepool1[:,0:10]
    print 'Shape of logLs_beforepool1'
    print np.shape(logLs_beforepool1)

    print 'Shape of diff1'
    print np.shape(diff1)
    print 'Value of diff1'
    print diff1[:,0:10]

    print 'Shape of max_over_a_mask_2D1'
    print np.shape(max_over_a_mask_2D1)
    print 'Value of max_over_a_mask_2D1'
    print max_over_a_mask_2D1

    print 'Shape of max_over_a_mask11'
    print np.shape(max_over_a_mask1)
    print 'Value of max_over_a_mask1'
    print max_over_a_mask1

    print 'Value of latents_rs_before_pool1'
    print latents_rs_before_pool1[1]
    print 'Shape of latents_rs_before_pool1'
    print np.shape(latents_rs_before_pool1)

    print 'Value of rs1'
    print rs1[1]
    print 'Shape of rs1'
    print np.shape(rs1)

    print 'Value of y_pred'
    print y_pred
    print 'Shape of y_pred'
    print np.shape(y_pred)

    print 'Value of gammas'
    print gammas
    print 'Shape of gammas'
    print np.shape(gammas)

    print 'Value of gammas_semisupervised'
    print gammas_semisupervised
    print 'Shape of gammas_semisupervised'
    print np.shape(gammas_semisupervised)

    print 'Value of cost_val'
    print cost_val
    print 'Shape of cost_val'
    print np.shape(cost_val)

    print 'Value of Nlabeled'
    print Nlabeled


    ###################################################################################################################
    # Python computation
    ###################################################################################################################
    # pbetas1 = lambdas1.T/np.dot()

###