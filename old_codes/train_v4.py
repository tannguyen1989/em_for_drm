from __future__ import print_function
__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import os
import timeit

import numpy as np

import theano
import theano.tensor as T

import copy
import cPickle

from numpy.linalg import norm
import time

from train_model_v4 import FRM

from sklearn.metrics import confusion_matrix, precision_score

# from guppy import hpy; h=hpy()

class train(object):
    """
    Training algorithm class

    """
    def __init__(self, batch_size, train_data, test_data, valid_data, train_label, test_label, valid_label, model, output_dir, param_dir='params'):
        '''

        :param data_mode:
        :param em_mode:
        :param update_mode:
        :param stop_mode:
        :param train_method:
        :param Ni:
        :param Cin:
        :param H:
        :param W:
        :param batch_size:
        :param seed:
        :param output_dir:
        :param data_dir:
        :return:
        '''
        self.batch_size = batch_size
        self.d = train_data
        self.dtest = test_data
        self.dvalid = valid_data
        self.label = train_label
        self.labeltest = test_label
        self.labelvalid = valid_label

        self.model = model
        self.output_dir = output_dir
        self.param_dir = os.path.join(self.output_dir, param_dir)
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        if not os.path.exists(self.param_dir):
            os.makedirs(self.param_dir)

    def train_Softmax(self, max_epochs, classification_dir):
        self.classification_dir = os.path.join(self.output_dir, classification_dir)
        if not os.path.exists(self.classification_dir):
            os.makedirs(self.classification_dir)

        if len(np.shape(self.d)) == 4:
            N, Cin, H, W = np.shape(self.d)
        else:
            N = np.shape(self.d)[0]

        n_batches = N/self.batch_size

        if len(np.shape(self.dvalid)) == 4:
            N_valid, Cin_valid, h_valid, w_valid = np.shape(self.dvalid)
        else:
            N_valid = np.shape(self.dvalid)[0]

        n_valid_batches = N_valid/self.batch_size

        if len(np.shape(self.dtest)) == 4:
            N_test, Cin_test, h_test, w_test = np.shape(self.dtest)
        else:
            N_test = np.shape(self.dtest)[0]

        n_test_batches = N_test/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_dat_test = theano.shared(self.dtest)
        shared_dat_valid = theano.shared(self.dvalid)
        shared_label = theano.shared(self.label)
        shared_label_test = theano.shared(self.labeltest)
        shared_label_valid = theano.shared(self.labelvalid)

        index = T.iscalar()

        if len(np.shape(self.d)) == 4:
            train_model = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                      self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

            test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

            validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                             givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                     self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})
        else:
            train_model = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:],
                                                      self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

            test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

            validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                             givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                     self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})

        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)

        done_looping = False

        patience = 10000  # look as this many examples regardless
        patience_increase = 2  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.995  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_params = None
        best_validation_loss = np.inf
        test_score_vec = []
        iter = 0

        start_time = time.clock()

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1
            print('Epoch %d' %epoch)

            for minibatch_index in xrange(n_batches):
                # print minibatch_index
                iter = iter + 1
                output_val = train_model(minibatch_index)

                if iter % validation_frequency == 0:
                    validation_losses = [validate_model(i) for i
                                     in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_batches, \
                           this_validation_loss * 100.))

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                        # save best validation score and iteration number
                        best_validation_loss = this_validation_loss
                        best_iter = iter
                        best_params = self.model.params

                        # test it on the test set
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)
                        print(('     epoch %i, minibatch %i/%i, test error of best '
                               'model %f %%') %
                              (epoch, minibatch_index + 1, n_batches,
                               test_score * 100.))

                if patience <= iter:
                    done_looping = True
                    break

            test_losses = [test_model(i) for i in xrange(n_test_batches)]
            test_score_epoch = np.mean(test_losses)
            test_score_vec.append(test_score_epoch)
            epoch_vec.append(epoch)
            fig = figure()
            plot(epoch_vec[1:], test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.classification_dir, 'Error-vs-Epoch-EG.png'))
            close(fig)
            plot_classification_file = os.path.join(self.classification_dir, 'plot_classification.npz')
            np.savez(plot_classification_file, epoch_vec=epoch_vec, test_score_vec=test_score_vec)

            param_file = os.path.join(self.classification_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, W=output_val[0], b=output_val[1])

        end_time = time.clock()
        print('Optimization complete.')
        print('Best validation score of %f %% obtained at iteration %i,'\
              'with test performance %f %%' %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))
        print >> sys.stderr, ('The code for file ' +
                              os.path.split(__file__)[1] +
                              ' ran for %.2fm' % ((end_time - start_time) / 60.))

        save_file = open(os.path.join(self.output_dir, 'cnn_mean_removed.pkl'), 'wb')  # this will overwrite current content
        cPickle.dump(best_params, save_file, -1)
        save_file.close()
        fig = figure()
        plot(epoch_vec[1:], test_score_vec)
        legend()
        xlabel('Epoch')
        ylabel('Error Rate')
        title('Error-vs-Epoch-EG')
        fig.savefig(os.path.join(self.classification_dir, 'Error-vs-Epoch-EG.png'))
        close(fig)

    def train_Softmax_for_FRM(self, max_epochs, classification_dir):
        self.classification_dir = os.path.join(self.output_dir, classification_dir)
        if not os.path.exists(self.classification_dir):
            os.makedirs(self.classification_dir)

        if len(np.shape(self.d)) == 4:
            N, Cin, H, W = np.shape(self.d)
        else:
            N = np.shape(self.d)[0]

        n_batches = N/self.batch_size

        if len(np.shape(self.dvalid)) == 4:
            N_valid, Cin_valid, h_valid, w_valid = np.shape(self.dvalid)
        else:
            N_valid = np.shape(self.dvalid)[0]

        n_valid_batches = N_valid/self.batch_size

        if len(np.shape(self.dtest)) == 4:
            N_test, Cin_test, h_test, w_test = np.shape(self.dtest)
        else:
            N_test = np.shape(self.dtest)[0]

        n_test_batches = N_test/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_dat_test = theano.shared(self.dtest)
        shared_dat_valid = theano.shared(self.dvalid)
        shared_label = theano.shared(self.label)
        shared_label_test = theano.shared(self.labeltest)
        shared_label_valid = theano.shared(self.labelvalid)

        index = T.iscalar()

        if len(np.shape(self.d)) == 4:
            train_model = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                      self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

            test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

            validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                             givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                     self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})
        else:
            train_model = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:],
                                                      self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

            test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

            validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                             givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                     self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})

        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)

        iter_vec = []
        iter = 0
        iter_vec.append(iter)

        done_looping = False

        patience = 10000  # look as this many examples regardless
        patience_increase = 2  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.995  # a relative improvement of this much is
                                       # considered significant
        #validation_frequency = min(n_batches, patience / 2)
        validation_frequency = 1
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_params = None
        best_validation_loss = np.inf
        test_score_vec = []

        start_time = time.clock()

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1
            print('Epoch %d' %epoch)

            for minibatch_index in xrange(n_batches):
                # print minibatch_index
                iter = iter + 1
                output_val = train_model(minibatch_index)

                if iter % validation_frequency == 0:
                    validation_losses = [validate_model(i) for i
                                     in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_batches, \
                           this_validation_loss * 100.))

                    test_losses = [test_model(i) for i in xrange(n_test_batches)]
                    test_score_iter = np.mean(test_losses)
                    test_score_vec.append(test_score_iter)
                    iter_vec.append(iter)
                    fig = figure()
                    plot(iter_vec[1:], test_score_vec)
                    legend()
                    xlabel('Iter')
                    ylabel('Error Rate')
                    title('Error-vs-Iter-EG')
                    fig.savefig(os.path.join(self.classification_dir, 'Error-vs-Iter-EG.png'))
                    close(fig)

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                        # save best validation score and iteration number
                        best_validation_loss = this_validation_loss
                        best_iter = iter
                        best_params = self.model.params

                        # test it on the test set
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)
                        print(('     epoch %i, minibatch %i/%i, test error of best '
                               'model %f %%') %
                              (epoch, minibatch_index + 1, n_batches,
                               test_score * 100.))

                if patience <= iter:
                    done_looping = True
                    break

            # test_losses = [test_model(i) for i in xrange(n_test_batches)]
            # test_score_epoch = np.mean(test_losses)
            # test_score_vec.append(test_score_epoch)
            epoch_vec.append(epoch)
            # fig = figure()
            # plot(epoch_vec[1:], test_score_vec)
            # legend()
            # xlabel('Epoch')
            # ylabel('Error Rate')
            # title('Error-vs-Epoch-EG')
            # fig.savefig(os.path.join(self.classification_dir, 'Error-vs-Epoch-EG.png'))
            # close(fig)
            plot_classification_file = os.path.join(self.classification_dir, 'plot_classification.npz')
            np.savez(plot_classification_file, epoch_vec=epoch_vec, iter_vec=iter_vec, test_score_vec=test_score_vec)

            param_file = os.path.join(self.classification_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, W=output_val[0], b=output_val[1])

        end_time = time.clock()
        print('Optimization complete.')
        print('Best validation score of %f %% obtained at iteration %i,'\
              'with test performance %f %%' %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))
        print >> sys.stderr, ('The code for file ' +
                              os.path.split(__file__)[1] +
                              ' ran for %.2fm' % ((end_time - start_time) / 60.))

        save_file = open(os.path.join(self.output_dir, 'cnn_mean_removed.pkl'), 'wb')  # this will overwrite current content
        cPickle.dump(best_params, save_file, -1)
        save_file.close()
        fig = figure()
        plot(epoch_vec[1:], test_score_vec)
        legend()
        xlabel('Epoch')
        ylabel('Error Rate')
        title('Error-vs-Epoch-EG')
        fig.savefig(os.path.join(self.classification_dir, 'Error-vs-Epoch-EG.png'))
        close(fig)

    def train_unsupervised(self, max_epochs, verbose, tol, stop_mode):
        '''
        EM learning algorithm
        :return:
        '''
        print('start EM training')

        if len(np.shape(self.d)) == 4:
            N, Cin, H, W = np.shape(self.d)
        else:
            N = np.shape(self.d)[0]

        n_batches = N/self.batch_size

        shared_dat = theano.shared(self.d)

        num_layer = len(self.model.layers)

        dLA_vec = []
        dMU_vec = []
        NL = None
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)
        iter = 0

        for i in xrange(num_layer):
            ## Concatenate differences in parameter values
            dLA_vec.append([])
            dMU_vec.append([])

        index = T.iscalar()

        if len(np.shape(self.d)) == 4:
            do_one_iter = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

            compute_logLs = theano.function([index], self.model.cost,
                                              updates=[], on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})
        else:
            do_one_iter = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:]})

            compute_logLs = theano.function([index], self.model.cost,
                                              updates=[], on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:]})

        # debug = theano.function([index], self.model.conv1.psis,
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        start_time = timeit.default_timer()

        newNL = 0
        for minibatch_index in xrange(n_batches):
            logLs_output_val = compute_logLs(minibatch_index)
            newNL = newNL + logLs_output_val

        newNL = newNL/n_batches
        NegLogLs_vec.append(newNL)
        print("Initial NLL=", newNL)

        done_looping = False

        while (epoch < max_epochs) and (not done_looping):

            epoch = epoch + 1
            epoch_vec.append(epoch)
            print('Epoch %d' %epoch)

            LA = []
            MU = []
            newLA = []
            newMU = []

            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))
                MU.append(copy.copy(layer.means.get_value()))

            for minibatch_index in xrange(n_batches):
                print(minibatch_index)
                iter = iter + 1
                # debug_val = debug(minibatch_index)
                # print 'Psis'
                # print np.shape(debug_val)
                # print np.min(debug_val)
                # print np.max(debug_val)
                output_val = do_one_iter(minibatch_index)

            newNL = 0
            for minibatch_index in xrange(n_batches):
                logLs_output_val = compute_logLs(minibatch_index)
                newNL = newNL + logLs_output_val

            newNL = newNL/n_batches
            NegLogLs_vec.append(newNL)

            for i in xrange(num_layer):
                newLA.append(output_val[3 + 5*i])
                newMU.append(output_val[2 + 5*i])
                dLA_vec[i].append(norm(newLA[i] - LA[i])/norm(LA[i]))
                dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 5*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 5*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 5*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 5*i],dtype=theano.config.floatX))

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, amps_val=amps_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
            close()

            fig4 = figure()
            plot(epoch_vec[1:], NegLogLs_vec[1:])
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
            fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
                close()

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newNL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newNL

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newNL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('\n')
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print('\n')

        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
        close()

        fig4 = figure()
        plot(epoch_vec[1:], NegLogLs_vec[1:])
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
        fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
            close()

    def train_supervised(self, max_epochs, verbose, tol, stop_mode, debug_mode='OFF'):
        N, Cin, h, w = np.shape(self.d)
        n_batches = N/self.batch_size
        N_valid, Cin_valid, h_valid, w_valid = np.shape(self.dvalid)
        n_valid_batches = N_valid/self.batch_size
        N_test, Cin_test, h_test, w_test = np.shape(self.dtest)
        n_test_batches = N_test/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_dat_test = theano.shared(self.dtest)
        shared_dat_valid = theano.shared(self.dvalid)
        shared_label = theano.shared(self.label)
        shared_label_test = theano.shared(self.labeltest)
        shared_label_valid = theano.shared(self.labelvalid)

        num_layer = len(self.model.layers)

        index = T.iscalar()

        do_one_iter = theano.function([index], self.model.output_var,
                                          updates=self.model.updates, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                  self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                     givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                             self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

        validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})

        # debug_one_EG = theano.function([index], [self.model.conv1.debugval, self.model.conv2.debugval],
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
        #                                           self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})


        dLA_vec = []
        dMU_vec = []
        dLA_mat = []
        dMU_mat = []
        amps_val_mat = []
        NL = None
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)

        for i in xrange(num_layer):
            # Concatenate differences in all parameter values
            dLA_vec.append([])
            dMU_vec.append([])
            dLA_mat.append([])
            dMU_mat.append([])
            amps_val_mat.append([])

        compute_logLs = theano.function([index], self.model.cost,
                                          updates=[], on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                  self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        newNL = 0
        for minibatch_index in xrange(n_batches):
            logLs_output_val = compute_logLs(minibatch_index)
            newNL = newNL + logLs_output_val

        newNL = newNL/n_batches
        NegLogLs_vec.append(newNL)
        print("Initial NLL=", newNL)

        if debug_mode == 'ON':
            compute_misc = theano.function([index], self.model.misc,
                                              updates=[], on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                      self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

            get_init = theano.function([], self.model.init_var,
                                              updates=[], on_unused_input='ignore')

            init_val = get_init()

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(init_val[1 + 4*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(init_val[2 + 4*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(init_val[0 + 4*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(init_val[3 + 4*i],dtype=theano.config.floatX))

            means_val1 = means_val[0]
            lambdas_val1 = lambdas_val[0]
            betas_val1 = betas_val[0]
            amps_val1 = amps_val[0]

            means_norm1_mat = []
            lambdas_norm1_mat = []
            betas_norm1_mat = []
            amps_val1_mat = []

            means_norm1_mat.append(np.linalg.norm(means_val1, axis=1))
            lambdas_norm1_mat.append(np.linalg.norm(lambdas_val1[:,:,0], axis=1))
            betas_norm1_mat.append(np.linalg.norm(betas_val1[:,0,:], axis=1))
            amps_val1_mat.append(amps_val1)

            latents_norm1_mat = []
            rs_norm1_mat = []
            latents_rs_norm1_mat = []

            latents_norm1_vec = []
            rs_norm1_vec = []
            latents_rs_norm1_vec = []

            latents_spar1_mat = []
            rs_spar1_mat = []
            latents_rs_spar1_mat = []

            latents_spar1_vec = []
            rs_spar1_vec = []
            latents_rs_spar1_vec = []

        done_looping = False

        patience = 20000  # look as this many examples regardless
        patience_increase = 3  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.999  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_params = None
        best_validation_loss = np.inf
        best_iter = 0
        test_score = 0.0
        test_score_epoch = 0.0
        test_score_vec = []
        iter = 0


        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1
            print('Epoch %d' %epoch)
            LA = []
            MU = []
            newLA = []
            newMU = []

            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))
                MU.append(copy.copy(layer.means.get_value()))
                amps_val_mat.append(copy.copy(layer.amps.get_value()))

            for minibatch_index in xrange(n_batches):
                print(minibatch_index)
                iter = iter + 1
                # [sum_var1, sum_var2] = debug_one_EG(minibatch_index)
                # print 'start debugging'
                # print 'sum_var1'
                # print sum_var1
                # print 'sum_var2'
                # print sum_var2
                # print np.shape(sum_var1)
                # print np.max(sum_var1)
                # print np.min(sum_var1)
                # print np.shape(sum_var2)
                # print np.max(sum_var2)
                # print np.min(sum_var2)
                # time.sleep(60)
                output_val = do_one_iter(minibatch_index)

                if iter % validation_frequency == 0:
                    validation_losses = [validate_model(i) for i
                                     in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_batches, \
                           this_validation_loss * 100.))

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                        # save best validation score and iteration number
                        best_validation_loss = this_validation_loss
                        best_iter = iter
                        best_params = self.model.params

                        # test it on the test set
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)
                        print(('     epoch %i, minibatch %i/%i, test error of best '
                               'model %f %%') %
                              (epoch, minibatch_index + 1, n_batches,
                               test_score * 100.))

                if patience <= iter:
                    done_looping = True
                    break

            test_losses = [test_model(i) for i in xrange(n_test_batches)]
            test_score_epoch = np.mean(test_losses)
            test_score_vec.append(test_score_epoch)
            epoch_vec.append(epoch)
            fig = figure()
            plot(epoch_vec[1:], test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.output_dir, 'Error-vs-Epoch-EG.png'))
            close(fig)
            plot_classification_file = os.path.join(self.output_dir, 'plot_classification.npz')
            np.savez(plot_classification_file, epoch_vec=epoch_vec, test_score_vec=test_score_vec)

            if epoch == 1:
                legend_misc = range(np.shape(output_val[3])[0])
                for i in xrange(len(legend_misc)):
                    legend_misc[i] = str(legend_misc[i])
                legend_misc.append('average')

            newNL = 0

            if debug_mode == 'ON':
                latents_norm1 = 0
                rs_norm1 = 0
                latents_rs_norm1 = 0
                latents_spar1 = 0
                rs_spar1 = 0
                latents_rs_spar1 = 0

            for minibatch_index in xrange(n_batches):
                logLs_output_val = compute_logLs(minibatch_index)
                newNL = newNL + logLs_output_val
                if debug_mode == 'ON':
                    misc_val = compute_misc(minibatch_index)
                    latents_val1 = misc_val[0]
                    latents_norm1 = latents_norm1 + np.mean(latents_val1[:,0,:]**2, axis=1)
                    latents_spar1 = latents_spar1 + np.mean(latents_val1[:,0,:] != 0, axis=1)
                    rs_val1 = misc_val[1]
                    rs_norm1 = rs_norm1 + np.mean(rs_val1, axis=1)
                    rs_spar1 = rs_spar1 + np.mean(rs_val1 != 0, axis=1)
                    latents_rs_val1 = misc_val[2]
                    latents_rs_norm1 = latents_rs_norm1 + np.mean(np.mean(np.mean(latents_rs_val1**2, axis=0), axis=1), axis=1)
                    latents_rs_spar1 = latents_rs_spar1 + np.mean(np.mean(np.mean(latents_rs_val1 != 0, axis=0), axis=1), axis=1)

            newNL = newNL/n_batches
            NegLogLs_vec.append(newNL)

            if debug_mode == 'ON':
                latents_norm1 = sqrt(latents_norm1/n_batches)
                rs_norm1 = rs_norm1/n_batches
                latents_rs_norm1 = sqrt(latents_rs_norm1/n_batches)

                latents_norm1_mat.append(latents_norm1)
                rs_norm1_mat.append(rs_norm1)
                latents_rs_norm1_mat.append(latents_rs_norm1)

                latents_norm1_vec.append(sqrt(np.mean(latents_norm1**2)))
                rs_norm1_vec.append(np.mean(rs_norm1))
                latents_rs_norm1_vec.append(sqrt(np.mean(latents_rs_norm1**2)))

                latents_spar1 = latents_spar1/n_batches
                rs_spar1 = rs_spar1/n_batches
                latents_rs_spar1 = latents_rs_spar1/n_batches

                latents_spar1_mat.append(latents_spar1)
                rs_spar1_mat.append(rs_spar1)
                latents_rs_spar1_mat.append(latents_rs_spar1)

                latents_spar1_vec.append(np.mean(latents_spar1))
                rs_spar1_vec.append(np.mean(rs_spar1))
                latents_rs_spar1_vec.append(np.mean(latents_rs_spar1))

                fig=figure()
                plot(latents_norm1_mat)
                plot(latents_norm1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Latents Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Latents_Layer1.png'))
                close(fig)

                fig=figure()
                plot(rs_norm1_mat)
                plot(rs_norm1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Responsibilities Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Responsibilities_Layer1.png'))
                close(fig)

                fig=figure()
                plot(latents_rs_norm1_mat)
                plot(latents_rs_norm1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Latents masked by Responsibilities Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Latents_Masked_by_Responsibilities_Layer1.png'))
                close(fig)

                fig=figure()
                plot(latents_spar1_mat)
                plot(latents_spar1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Spar')
                title('Spar of Latents Layer1')
                fig.savefig(os.path.join(self.output_dir,'Spar_of_Latents_Layer1.png'))
                close(fig)

                fig=figure()
                plot(rs_spar1_mat)
                plot(rs_spar1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Spar')
                title('Spar of Responsibilities Layer1')
                fig.savefig(os.path.join(self.output_dir,'Spar_of_Responsibilities_Layer1.png'))
                close(fig)

                fig=figure()
                plot(latents_rs_spar1_mat)
                plot(latents_rs_spar1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Spar')
                title('Spar of Latents masked by Responsibilities Layer1')
                fig.savefig(os.path.join(self.output_dir,'Spar_of_Latents_Masked_by_Responsibilities_Layer1.png'))
                close(fig)

            for i in xrange(num_layer):
                newLA.append(output_val[3 + 5*i])
                newMU.append(output_val[2 + 5*i])
                dLA_vec[i].append(norm(newLA[i] - LA[i])/norm(LA[i]))
                dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))
                dLA_mat[i].append(norm(newLA[i][:,:,0] - LA[i][:,:,0], axis=1))
                dMU_mat[i].append(norm(newMU[i] - MU[i], axis=1))

            # print amps_val

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newNL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newNL

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 5*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 5*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 5*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 5*i],dtype=theano.config.floatX))

            means_val1 = means_val[0]
            lambdas_val1 = lambdas_val[0]
            betas_val1 = betas_val[0]
            amps_val1 = amps_val[0]

            if debug_mode == 'ON':
                means_norm1_mat.append(np.linalg.norm(means_val1, axis=1))
                lambdas_norm1_mat.append(np.linalg.norm(lambdas_val1[:,:,0], axis=1))
                betas_norm1_mat.append(np.linalg.norm(betas_val1[:,0,:], axis=1))
                amps_val1_mat.append(amps_val1)

                fig=figure()
                plot(means_norm1_mat)
                legend(legend_misc[0:-1],prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Means Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Means_Layer1.png'))
                close(fig)

                fig=figure()
                plot(lambdas_norm1_mat)
                legend(legend_misc[0:-1],prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Lambdas Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Lambdas_Layer1.png'))
                close(fig)

                fig=figure()
                plot(betas_norm1_mat)
                legend(legend_misc[0:-1],prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Betas Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Betas_Layer1.png'))
                close(fig)

                fig=figure()
                plot(amps_val1_mat)
                legend(legend_misc[0:-1],prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Pi cg')
                title('Pi cg Layer1')
                fig.savefig(os.path.join(self.output_dir,'Pi_cg_Layer1.png'))
                close(fig)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Training Cost')
            title('Convergence_of_Training_Cost_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Training_Cost_BatchSize_%i.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
                close()

            for i in xrange(num_layer):
                fig4 = figure()
                plot(dLA_mat[i])
                legend(legend_misc[0:-1],prop={'size':8})
                xlabel('Epoch')
                ylabel('Difference')
                title('Convergence_of_Lambdas_Layer%i' % (i + 1))
                fig4.savefig(os.path.join(self.output_dir, 'Convergence_of_Lambdas_Layer%i.png' % (i + 1)))
                close()

                fig5 = figure()
                plot(dMU_mat[i])
                legend(legend_misc[0:-1],prop={'size':8})
                xlabel('Epoch')
                ylabel('Difference')
                title('Convergence_of_Means_Layer%i' % (i + 1))
                fig5.savefig(os.path.join(self.output_dir, 'Convergence_of_Means_Layer%i.png' % (i + 1)))
                close()

                fig5 = figure()
                plot(amps_val_mat[i])
                legend(legend_misc[0:-1],prop={'size':8})
                xlabel('Epoch')
                ylabel('Pi(cg)')
                title('Convergence_of_Pi_cg_Layer%i' % (i + 1))
                fig5.savefig(os.path.join(self.output_dir, 'Convergence_of_Pi_cg_Layer%i.png' % (i + 1)))
                close()

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, amps_val=amps_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

            if debug_mode == 'ON':
                forPlotDebug_file = os.path.join(self.output_dir, 'for_Plotting_Debug.npz')
                np.savez(forPlotDebug_file, epoch_vec=epoch_vec,
                         latents_norm1_mat=latents_norm1_mat, latents_norm1_vec=latents_norm1_vec,
                         rs_norm1_mat=rs_norm1_mat, rs_norm1_vec=rs_norm1_vec,
                         latents_rs_norm1_mat=latents_rs_norm1_mat, latents_rs_norm1_vec=latents_rs_norm1_vec,
                         latents_spar1_mat=latents_spar1_mat, latents_spar1_vec=latents_spar1_vec,
                         rs_spar1_mat=rs_spar1_mat, rs_spar1_vec=rs_spar1_vec,
                         latents_rs_spar1_mat=latents_rs_spar1_mat, latents_rs_spar1_vec=latents_rs_spar1_vec,
                         means_norm1_mat=means_norm1_mat, lambdas_norm1_mat=lambdas_norm1_mat,
                         betas_norm1_mat=betas_norm1_mat, amps_val1_mat=amps_val1_mat,
                         dLA_mat=dLA_mat, dMU_mat=dMU_mat
                         )
            else:
                forPlotDebug_file = os.path.join(self.output_dir, 'for_Plotting_Debug.npz')
                np.savez(forPlotDebug_file, epoch_vec=epoch_vec, dLA_mat=dLA_mat, dMU_mat=dMU_mat, amps_val_mat=amps_val_mat)


        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newNL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('Optimization complete.')
        print('Best validation score of %f %% obtained at iteration %i,'\
              'with test performance %f %%' %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))


        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

        if debug_mode == 'ON':
            forPlotDebug_file = os.path.join(self.output_dir, 'for_Plotting_Debug.npz')
            np.savez(forPlotDebug_file, epoch_vec=epoch_vec,
                     latents_norm1_mat=latents_norm1_mat, latents_norm1_vec=latents_norm1_vec,
                     rs_norm1_mat=rs_norm1_mat, rs_norm1_vec=rs_norm1_vec,
                     latents_rs_norm1_mat=latents_rs_norm1_mat, latents_rs_norm1_vec=latents_rs_norm1_vec,
                     latents_spar1_mat=latents_spar1_mat, latents_spar1_vec=latents_spar1_vec,
                     rs_spar1_mat=rs_spar1_mat, rs_spar1_vec=rs_spar1_vec,
                     latents_rs_spar1_mat=latents_rs_spar1_mat, latents_rs_spar1_vec=latents_rs_spar1_vec,
                     means_norm1_mat=means_norm1_mat, lambdas_norm1_mat=lambdas_norm1_mat,
                     betas_norm1_mat=betas_norm1_mat, amps_val1_mat=amps_val1_mat,
                     dLA_mat=dLA_mat, dMU_mat=dMU_mat
                     )
        else:
            forPlotDebug_file = os.path.join(self.output_dir, 'for_Plotting_Debug.npz')
            np.savez(forPlotDebug_file, epoch_vec=epoch_vec, dLA_mat=dLA_mat, dMU_mat=dMU_mat, amps_val_mat=amps_val_mat)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Training Cost')
        title('Convergence_of_Training_Cost_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Training_Cost_BatchSize_%i.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
            close()

        save_file = open(os.path.join(self.output_dir, 'cnn_mean_removed.pkl'), 'wb')  # this will overwrite current content
        cPickle.dump(best_params, save_file, -1)
        save_file.close()
        fig = figure()
        plot(epoch_vec[1:], test_score_vec)
        legend()
        xlabel('Epoch')
        ylabel('Error Rate')
        title('Error-vs-Epoch-EG')
        fig.savefig(os.path.join(self.output_dir, 'Error-vs-Epoch-EG.png'))
        close(fig)

    def train_unsupervised_TopDown(self, max_epochs, verbose, tol, stop_mode):
        '''
        EM learning algorithm
        :return:
        '''
        print('start EM training')

        N, Cin, H, W = np.shape(self.d)
        n_batches = N/self.batch_size

        shared_dat = theano.shared(self.d)

        num_layer = len(self.model.layers)

        dLA_vec = []
        dMU_vec = []
        NL = None
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)
        iter = 0

        for i in xrange(num_layer):
            ## Concatenate differences in parameter values
            dLA_vec.append([])
            dMU_vec.append([])

        index = T.iscalar()

        do_one_iter = theano.function([index], self.model.output_var,
                                          updates=self.model.updates, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        compute_logLs = theano.function([index], self.model.cost,
                                          updates=[], on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        newNL = 0
        for minibatch_index in xrange(n_batches):
            logLs_output_val = compute_logLs(minibatch_index)
            newNL = newNL + logLs_output_val

        newNL = newNL/n_batches
        NegLogLs_vec.append(newNL)
        print("Initial NLL=", newNL)


        # debug = theano.function([index], self.model.conv1.amps_new,
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        start_time = timeit.default_timer()

        done_looping = False

        while (epoch < max_epochs) and (not done_looping):

            epoch = epoch + 1
            epoch_vec.append(epoch)
            print('Epoch %d' %epoch)

            LA = []
            MU = []
            newLA = []
            newMU = []

            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))
                MU.append(copy.copy(layer.means.get_value()))

            for minibatch_index in xrange(n_batches):
                print(minibatch_index)
                iter = iter + 1
                output_val = do_one_iter(minibatch_index)

            newNL = 0
            for minibatch_index in xrange(n_batches):
                logLs_output_val = compute_logLs(minibatch_index)
                newNL = newNL + logLs_output_val

            newNL = newNL/n_batches
            NegLogLs_vec.append(newNL)

            for i in xrange(num_layer):
                newLA.append(output_val[3 + 5*i])
                newMU.append(output_val[2 + 5*i])
                dLA_vec[i].append(norm(newLA[i] - LA[i])/norm(LA[i]))
                dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 5*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 5*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 5*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 5*i],dtype=theano.config.floatX))

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, amps_val=amps_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
            close()

            fig4 = figure()
            plot(epoch_vec[1:], NegLogLs_vec[1:])
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
            fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
                close()

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newNL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newNL

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newNL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('\n')
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print('\n')

        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
        close()

        fig4 = figure()
        plot(epoch_vec[1:], NegLogLs_vec[1:])
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
        fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
            close()

    def train_semisupervised(self, max_epochs, verbose, tol, stop_mode, debug_mode='OFF'):
        N, Cin, h, w = np.shape(self.d)
        n_batches = N/self.batch_size
        N_valid, Cin_valid, h_valid, w_valid = np.shape(self.dvalid)
        n_valid_batches = N_valid/self.batch_size
        N_test, Cin_test, h_test, w_test = np.shape(self.dtest)
        n_test_batches = N_test/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_dat_test = theano.shared(self.dtest)
        shared_dat_valid = theano.shared(self.dvalid)
        shared_label = theano.shared(self.label)
        shared_label_test = theano.shared(self.labeltest)
        shared_label_valid = theano.shared(self.labelvalid)

        num_layer = len(self.model.layers)

        index = T.iscalar()

        do_one_iter = theano.function([index], self.model.output_var,
                                          updates=self.model.updates, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                  self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                     givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                             self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

        validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})

        # debug_one_EG = theano.function([index], [self.model.conv1.debugval, self.model.conv2.debugval],
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
        #                                           self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})


        dLA_vec = []
        dMU_vec = []
        dLA_mat = []
        dMU_mat = []
        amps_val_mat = []
        NL = None
        NegLogLs_vec = []
        unsupervisedNegLogLs_vec = []
        supervisedNegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)

        for i in xrange(num_layer):
            # Concatenate differences in all parameter values
            dLA_vec.append([])
            dMU_vec.append([])
            dLA_mat.append([])
            dMU_mat.append([])
            amps_val_mat.append([])

        compute_NLL = theano.function([index], [self.model.cost, self.model.unsupervisedNLL, self.model.supervisedNLL],
                                          updates=[], on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                  self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        newTotalNLL = 0
        newUnsupervisedNLL = 0
        newSupervisedNLL = 0
        for minibatch_index in xrange(n_batches):
            totalNLL_val, unsupervisedNLL_val, supervisedNLL_val = compute_NLL(minibatch_index)
            newTotalNLL = newTotalNLL + totalNLL_val
            newUnsupervisedNLL = newUnsupervisedNLL + unsupervisedNLL_val
            newSupervisedNLL = newSupervisedNLL + supervisedNLL_val

        newTotalNLL = newTotalNLL/n_batches
        NegLogLs_vec.append(newTotalNLL)
        print("Initial Total_NLL=", newTotalNLL)

        newUnsupervisedNLL = newUnsupervisedNLL/n_batches
        unsupervisedNegLogLs_vec.append(newUnsupervisedNLL)

        newSupervisedNLL = newSupervisedNLL/n_batches
        supervisedNegLogLs_vec.append(newSupervisedNLL)

        if debug_mode == 'ON':
            compute_misc = theano.function([index], self.model.misc,
                                              updates=[], on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                      self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

            get_init = theano.function([], self.model.init_var,
                                              updates=[], on_unused_input='ignore')

            init_val = get_init()

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(init_val[1 + 4*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(init_val[2 + 4*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(init_val[0 + 4*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(init_val[3 + 4*i],dtype=theano.config.floatX))

            means_val1 = means_val[0]
            lambdas_val1 = lambdas_val[0]
            betas_val1 = betas_val[0]
            amps_val1 = amps_val[0]

            means_norm1_mat = []
            lambdas_norm1_mat = []
            betas_norm1_mat = []
            amps_val1_mat = []

            means_norm1_mat.append(np.linalg.norm(means_val1, axis=1))
            lambdas_norm1_mat.append(np.linalg.norm(lambdas_val1[:,:,0], axis=1))
            betas_norm1_mat.append(np.linalg.norm(betas_val1[:,0,:], axis=1))
            amps_val1_mat.append(amps_val1)

            latents_norm1_mat = []
            rs_norm1_mat = []
            latents_rs_norm1_mat = []

            latents_norm1_vec = []
            rs_norm1_vec = []
            latents_rs_norm1_vec = []

            latents_spar1_mat = []
            rs_spar1_mat = []
            latents_rs_spar1_mat = []

            latents_spar1_vec = []
            rs_spar1_vec = []
            latents_rs_spar1_vec = []

        done_looping = False

        patience = 20000  # look as this many examples regardless
        patience_increase = 3  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.999  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_params = None
        best_validation_loss = np.inf
        best_iter = 0
        test_score = 0.0
        test_score_epoch = 0.0
        test_score_vec = []
        iter = 0


        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1
            print('Epoch %d' %epoch)
            LA = []
            MU = []
            newLA = []
            newMU = []

            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))
                MU.append(copy.copy(layer.means.get_value()))
                amps_val_mat.append(copy.copy(layer.amps.get_value()))

            for minibatch_index in xrange(n_batches):
                print(minibatch_index)
                iter = iter + 1
                # [sum_var1, sum_var2] = debug_one_EG(minibatch_index)
                # print 'start debugging'
                # print 'sum_var1'
                # print sum_var1
                # print 'sum_var2'
                # print sum_var2
                # print np.shape(sum_var1)
                # print np.max(sum_var1)
                # print np.min(sum_var1)
                # print np.shape(sum_var2)
                # print np.max(sum_var2)
                # print np.min(sum_var2)
                # time.sleep(60)
                output_val = do_one_iter(minibatch_index)

                if iter % validation_frequency == 0:
                    validation_losses = [validate_model(i) for i
                                     in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_batches, \
                           this_validation_loss * 100.))

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                        # save best validation score and iteration number
                        best_validation_loss = this_validation_loss
                        best_iter = iter
                        best_params = self.model.params

                        # test it on the test set
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)
                        print(('     epoch %i, minibatch %i/%i, test error of best '
                               'model %f %%') %
                              (epoch, minibatch_index + 1, n_batches,
                               test_score * 100.))

                if patience <= iter:
                    done_looping = True
                    break

            test_losses = [test_model(i) for i in xrange(n_test_batches)]
            test_score_epoch = np.mean(test_losses)
            test_score_vec.append(test_score_epoch)
            epoch_vec.append(epoch)
            fig = figure()
            plot(epoch_vec[1:], test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.output_dir, 'Error-vs-Epoch-EG.png'))
            close(fig)
            plot_classification_file = os.path.join(self.output_dir, 'plot_classification.npz')
            np.savez(plot_classification_file, epoch_vec=epoch_vec, test_score_vec=test_score_vec)

            if epoch == 1:
                legend_misc = range(np.shape(output_val[3])[0])
                for i in xrange(len(legend_misc)):
                    legend_misc[i] = str(legend_misc[i])
                legend_misc.append('average')

            newTotalNLL = 0
            newUnsupervisedNLL = 0
            newSupervisedNLL = 0

            if debug_mode == 'ON':
                latents_norm1 = 0
                rs_norm1 = 0
                latents_rs_norm1 = 0
                latents_spar1 = 0
                rs_spar1 = 0
                latents_rs_spar1 = 0

            for minibatch_index in xrange(n_batches):
                totalNLL_val, unsupervisedNLL_val, supervisedNLL_val = compute_NLL(minibatch_index)
                newTotalNLL = newTotalNLL + totalNLL_val
                newUnsupervisedNLL = newUnsupervisedNLL + unsupervisedNLL_val
                newSupervisedNLL = newSupervisedNLL + supervisedNLL_val
                if debug_mode == 'ON':
                    misc_val = compute_misc(minibatch_index)
                    latents_val1 = misc_val[0]
                    latents_norm1 = latents_norm1 + np.mean(latents_val1[:,0,:]**2, axis=1)
                    latents_spar1 = latents_spar1 + np.mean(latents_val1[:,0,:] != 0, axis=1)
                    rs_val1 = misc_val[1]
                    rs_norm1 = rs_norm1 + np.mean(rs_val1, axis=1)
                    rs_spar1 = rs_spar1 + np.mean(rs_val1 != 0, axis=1)
                    latents_rs_val1 = misc_val[2]
                    latents_rs_norm1 = latents_rs_norm1 + np.mean(np.mean(np.mean(latents_rs_val1**2, axis=0), axis=1), axis=1)
                    latents_rs_spar1 = latents_rs_spar1 + np.mean(np.mean(np.mean(latents_rs_val1 != 0, axis=0), axis=1), axis=1)

            newTotalNLL = newTotalNLL/n_batches
            NegLogLs_vec.append(newTotalNLL)

            newUnsupervisedNLL = newUnsupervisedNLL/n_batches
            unsupervisedNegLogLs_vec.append(newUnsupervisedNLL)

            newSupervisedNLL = newSupervisedNLL/n_batches
            supervisedNegLogLs_vec.append(newSupervisedNLL)

            if debug_mode == 'ON':
                latents_norm1 = sqrt(latents_norm1/n_batches)
                rs_norm1 = rs_norm1/n_batches
                latents_rs_norm1 = sqrt(latents_rs_norm1/n_batches)

                latents_norm1_mat.append(latents_norm1)
                rs_norm1_mat.append(rs_norm1)
                latents_rs_norm1_mat.append(latents_rs_norm1)

                latents_norm1_vec.append(sqrt(np.mean(latents_norm1**2)))
                rs_norm1_vec.append(np.mean(rs_norm1))
                latents_rs_norm1_vec.append(sqrt(np.mean(latents_rs_norm1**2)))

                latents_spar1 = latents_spar1/n_batches
                rs_spar1 = rs_spar1/n_batches
                latents_rs_spar1 = latents_rs_spar1/n_batches

                latents_spar1_mat.append(latents_spar1)
                rs_spar1_mat.append(rs_spar1)
                latents_rs_spar1_mat.append(latents_rs_spar1)

                latents_spar1_vec.append(np.mean(latents_spar1))
                rs_spar1_vec.append(np.mean(rs_spar1))
                latents_rs_spar1_vec.append(np.mean(latents_rs_spar1))

                fig=figure()
                plot(latents_norm1_mat)
                plot(latents_norm1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Latents Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Latents_Layer1.png'))
                close(fig)

                fig=figure()
                plot(rs_norm1_mat)
                plot(rs_norm1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Responsibilities Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Responsibilities_Layer1.png'))
                close(fig)

                fig=figure()
                plot(latents_rs_norm1_mat)
                plot(latents_rs_norm1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Latents masked by Responsibilities Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Latents_Masked_by_Responsibilities_Layer1.png'))
                close(fig)

                fig=figure()
                plot(latents_spar1_mat)
                plot(latents_spar1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Spar')
                title('Spar of Latents Layer1')
                fig.savefig(os.path.join(self.output_dir,'Spar_of_Latents_Layer1.png'))
                close(fig)

                fig=figure()
                plot(rs_spar1_mat)
                plot(rs_spar1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Spar')
                title('Spar of Responsibilities Layer1')
                fig.savefig(os.path.join(self.output_dir,'Spar_of_Responsibilities_Layer1.png'))
                close(fig)

                fig=figure()
                plot(latents_rs_spar1_mat)
                plot(latents_rs_spar1_vec,linewidth=6)
                legend(legend_misc,prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Spar')
                title('Spar of Latents masked by Responsibilities Layer1')
                fig.savefig(os.path.join(self.output_dir,'Spar_of_Latents_Masked_by_Responsibilities_Layer1.png'))
                close(fig)

            for i in xrange(num_layer):
                newLA.append(output_val[3 + 5*i])
                newMU.append(output_val[2 + 5*i])
                dLA_vec[i].append(norm(newLA[i] - LA[i])/norm(LA[i]))
                dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))
                dLA_mat[i].append(norm(newLA[i][:,:,0] - LA[i][:,:,0], axis=1))
                dMU_mat[i].append(norm(newMU[i] - MU[i], axis=1))

            # print amps_val

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newTotalNLL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newTotalNLL

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 5*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 5*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 5*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 5*i],dtype=theano.config.floatX))

            means_val1 = means_val[0]
            lambdas_val1 = lambdas_val[0]
            betas_val1 = betas_val[0]
            amps_val1 = amps_val[0]

            if debug_mode == 'ON':

                means_norm1_mat.append(np.linalg.norm(means_val1, axis=1))
                lambdas_norm1_mat.append(np.linalg.norm(lambdas_val1[:,:,0], axis=1))
                betas_norm1_mat.append(np.linalg.norm(betas_val1[:,0,:], axis=1))
                amps_val1_mat.append(amps_val1)

                fig=figure()
                plot(means_norm1_mat)
                legend(legend_misc[0:-1],prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Means Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Means_Layer1.png'))
                close(fig)

                fig=figure()
                plot(lambdas_norm1_mat)
                legend(legend_misc[0:-1],prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Lambdas Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Lambdas_Layer1.png'))
                close(fig)

                fig=figure()
                plot(betas_norm1_mat)
                legend(legend_misc[0:-1],prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Norm')
                title('Norm of Betas Layer1')
                fig.savefig(os.path.join(self.output_dir,'Norm_of_Betas_Layer1.png'))
                close(fig)

                fig=figure()
                plot(amps_val1_mat)
                legend(legend_misc[0:-1],prop={'size':8})
                ylim(ymin=0)
                xlabel('Epoch')
                ylabel('Pi cg')
                title('Pi cg Layer1')
                fig.savefig(os.path.join(self.output_dir,'Pi_cg_Layer1.png'))
                close(fig)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Training Cost')
            title('Convergence_of_Training_Cost_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Training_Cost_BatchSize_%i.png' % self.batch_size))
            close()

            fig1 = figure()
            plot(epoch_vec, unsupervisedNegLogLs_vec)
            xlabel('Epoch')
            ylabel('Unsupervised NLL')
            title('Convergence_of_Unsupervised_NLL_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Unsupervised_NLL_BatchSize_%i.png' % self.batch_size))
            close()

            fig1 = figure()
            plot(epoch_vec, supervisedNegLogLs_vec)
            xlabel('Epoch')
            ylabel('Supervised NLL')
            title('Convergence_of_Supervised_NLL_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Supervised_NLL_BatchSize_%i.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
                close()

            for i in xrange(num_layer):
                fig4 = figure()
                plot(dLA_mat[i])
                legend(legend_misc[0:-1],prop={'size':8})
                xlabel('Epoch')
                ylabel('Difference')
                title('Convergence_of_Lambdas_Layer%i' % (i + 1))
                fig4.savefig(os.path.join(self.output_dir, 'Convergence_of_Lambdas_Layer%i.png' % (i + 1)))
                close()

                fig5 = figure()
                plot(dMU_mat[i])
                legend(legend_misc[0:-1],prop={'size':8})
                xlabel('Epoch')
                ylabel('Difference')
                title('Convergence_of_Means_Layer%i' % (i + 1))
                fig5.savefig(os.path.join(self.output_dir, 'Convergence_of_Means_Layer%i.png' % (i + 1)))
                close()

                # fig5 = figure()
                # plot(amps_val_mat[i])
                # legend(legend_misc[0:-1],prop={'size':8})
                # xlabel('Epoch')
                # ylabel('Pi(cg)')
                # title('Convergence_of_Pi_cg_Layer%i' % (i + 1))
                # fig5.savefig(os.path.join(self.output_dir, 'Convergence_of_Pi_cg_Layer%i.png' % (i + 1)))
                # close()

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, amps_val=amps_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec,
                     unsupervisedNegLogLs_vec=unsupervisedNegLogLs_vec, supervisedNegLogLs_vec=supervisedNegLogLs_vec)

            if debug_mode == 'ON':
                forPlotDebug_file = os.path.join(self.output_dir, 'for_Plotting_Debug.npz')
                np.savez(forPlotDebug_file, epoch_vec=epoch_vec,
                         latents_norm1_mat=latents_norm1_mat, latents_norm1_vec=latents_norm1_vec,
                         rs_norm1_mat=rs_norm1_mat, rs_norm1_vec=rs_norm1_vec,
                         latents_rs_norm1_mat=latents_rs_norm1_mat, latents_rs_norm1_vec=latents_rs_norm1_vec,
                         latents_spar1_mat=latents_spar1_mat, latents_spar1_vec=latents_spar1_vec,
                         rs_spar1_mat=rs_spar1_mat, rs_spar1_vec=rs_spar1_vec,
                         latents_rs_spar1_mat=latents_rs_spar1_mat, latents_rs_spar1_vec=latents_rs_spar1_vec,
                         means_norm1_mat=means_norm1_mat, lambdas_norm1_mat=lambdas_norm1_mat,
                         betas_norm1_mat=betas_norm1_mat, amps_val1_mat=amps_val1_mat,
                         dLA_mat=dLA_mat, dMU_mat=dMU_mat
                         )
            else:
                forPlotDebug_file = os.path.join(self.output_dir, 'for_Plotting_Debug.npz')
                np.savez(forPlotDebug_file, epoch_vec=epoch_vec, dLA_mat=dLA_mat, dMU_mat=dMU_mat, amps_val_mat=amps_val_mat)


        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newTotalNLL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('Optimization complete.')
        print('Best validation score of %f %% obtained at iteration %i,'\
              'with test performance %f %%' %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))


        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec,
                 unsupervisedNegLogLs_vec=unsupervisedNegLogLs_vec, supervisedNegLogLs_vec=supervisedNegLogLs_vec)

        if debug_mode == 'ON':
            forPlotDebug_file = os.path.join(self.output_dir, 'for_Plotting_Debug.npz')
            np.savez(forPlotDebug_file, epoch_vec=epoch_vec,
                     latents_norm1_mat=latents_norm1_mat, latents_norm1_vec=latents_norm1_vec,
                     rs_norm1_mat=rs_norm1_mat, rs_norm1_vec=rs_norm1_vec,
                     latents_rs_norm1_mat=latents_rs_norm1_mat, latents_rs_norm1_vec=latents_rs_norm1_vec,
                     latents_spar1_mat=latents_spar1_mat, latents_spar1_vec=latents_spar1_vec,
                     rs_spar1_mat=rs_spar1_mat, rs_spar1_vec=rs_spar1_vec,
                     latents_rs_spar1_mat=latents_rs_spar1_mat, latents_rs_spar1_vec=latents_rs_spar1_vec,
                     means_norm1_mat=means_norm1_mat, lambdas_norm1_mat=lambdas_norm1_mat,
                     betas_norm1_mat=betas_norm1_mat, amps_val1_mat=amps_val1_mat,
                     dLA_mat=dLA_mat, dMU_mat=dMU_mat
                     )
        else:
            forPlotDebug_file = os.path.join(self.output_dir, 'for_Plotting_Debug.npz')
            np.savez(forPlotDebug_file, epoch_vec=epoch_vec, dLA_mat=dLA_mat, dMU_mat=dMU_mat, amps_val_mat=amps_val_mat)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Training Cost')
        title('Convergence_of_Training_Cost_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Training_Cost_BatchSize_%i.png' % self.batch_size))
        close()

        fig1 = figure()
        plot(epoch_vec, unsupervisedNegLogLs_vec)
        xlabel('Epoch')
        ylabel('Unsupervised NLL')
        title('Convergence_of_Unsupervised_NLL_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Unsupervised_NLL_BatchSize_%i.png' % self.batch_size))
        close()

        fig1 = figure()
        plot(epoch_vec, supervisedNegLogLs_vec)
        xlabel('Epoch')
        ylabel('Supervised NLL')
        title('Convergence_of_Supervised_NLL_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Supervised_NLL_BatchSize_%i.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
            close()

        save_file = open(os.path.join(self.output_dir, 'cnn_mean_removed.pkl'), 'wb')  # this will overwrite current content
        cPickle.dump(best_params, save_file, -1)
        save_file.close()
        fig = figure()
        plot(epoch_vec[1:], test_score_vec)
        legend()
        xlabel('Epoch')
        ylabel('Error Rate')
        title('Error-vs-Epoch-EG')
        fig.savefig(os.path.join(self.output_dir, 'Error-vs-Epoch-EG.png'))
        close(fig)

    def train_unsupervised_with_extra_G(self, max_epochs, verbose, tol, stop_mode):
        '''
        EM learning algorithm
        :return:
        '''
        print('start EM training')

        N, Cin, H, W = np.shape(self.d)
        n_batches = N/self.batch_size

        shared_dat = theano.shared(self.d)

        num_layer = len(self.model.layers)

        dLA_vec = []
        dMU_vec = []
        NL = None
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)
        iter = 0

        for i in xrange(num_layer):
            ## Concatenate differences in parameter values
            dLA_vec.append([])
            dMU_vec.append([])

        index = T.iscalar()

        do_one_iter = theano.function([index], self.model.output_var,
                                          updates=self.model.updates, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        compute_logLs = theano.function([index], self.model.cost,
                                          updates=[], on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        # debug = theano.function([index], self.model.conv1.amps_new,
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        self.model._extra_G_step()

        do_one_extra_G = theano.function([index, self.model.latents, self.model.rs], self.model.output_var_new,
                                          updates=self.model.updates_new, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        start_time = timeit.default_timer()

        newNL = 0
        for minibatch_index in xrange(n_batches):
            logLs_output_val = compute_logLs(minibatch_index)
            newNL = newNL + logLs_output_val

        newNL = newNL/n_batches
        NegLogLs_vec.append(newNL)
        print("Initial NLL=", newNL)

        done_looping = False

        while (epoch < max_epochs) and (not done_looping):

            epoch = epoch + 1
            epoch_vec.append(epoch)
            print('Epoch %d' %epoch)

            LA = []
            MU = []
            newLA = []
            newMU = []

            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))
                MU.append(copy.copy(layer.means.get_value()))

            for minibatch_index in xrange(n_batches):
                print(minibatch_index)
                iter = iter + 1
                output_val = do_one_iter(minibatch_index)
                cost_batch = []
                cost_batch.append(output_val[7])

                for iG in xrange(100):
                    output_val_new = do_one_extra_G(minibatch_index, output_val[5], output_val[6])
                    cost_batch.append(output_val_new[0])

                if (iter % 20) == 1:
                    fig0 = figure()
                    plot(range(101), cost_batch)
                    xlabel('Iter')
                    ylabel('Batch Negative LogLikelihood')
                    title('Convergence_of_Batch_Negative_LogLikelihood_Iter_%i' % iter)
                    fig0.savefig(os.path.join(self.output_dir,'Convergence_of_Batch_Negative_LogLikelihood_Iter_%i.png' % iter))
                    close()


            newNL = 0
            for minibatch_index in xrange(n_batches):
                logLs_output_val = compute_logLs(minibatch_index)
                newNL = newNL + logLs_output_val

            newNL = newNL/n_batches
            NegLogLs_vec.append(newNL)

            for i in xrange(num_layer):
                newLA.append(output_val[3 + 8*i])
                newMU.append(output_val[2 + 8*i])
                dLA_vec[i].append(norm(newLA[i] - LA[i])/norm(LA[i]))
                dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 8*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 8*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 8*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 8*i],dtype=theano.config.floatX))

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, amps_val=amps_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
            close()

            fig4 = figure()
            plot(epoch_vec[1:], NegLogLs_vec[1:])
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
            fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
                close()

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newNL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newNL

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newNL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('\n')
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print('\n')

        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
        close()

        fig4 = figure()
        plot(epoch_vec[1:], NegLogLs_vec[1:])
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
        fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
            close()

    def train_unsupervised_for_FRM(self, max_epochs, verbose, tol, stop_mode):
        '''
        EM learning algorithm
        :return:
        '''
        print('start EM training')

        if len(np.shape(self.d)) == 4:
            N, Cin, H, W = np.shape(self.d)
        else:
            N = np.shape(self.d)[0]

        n_batches = N/self.batch_size

        shared_dat = theano.shared(self.d)

        num_layer = len(self.model.layers)

        dLA_vec = []
        dMU_vec = []
        dPS_vec =[]
        NL = None
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)
        iter = 0

        for i in xrange(num_layer):
            ## Concatenate differences in parameter values
            dLA_vec.append([])
            dMU_vec.append([])
            dPS_vec.append([])

        index = T.iscalar()

        if len(np.shape(self.d)) == 4:
            do_one_iter = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

            compute_logLs = theano.function([index], self.model.cost,
                                              updates=[], on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})
        else:
            do_one_iter = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:]})

            compute_logLs = theano.function([index], self.model.cost,
                                              updates=[], on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:]})

        # debug = theano.function([index], self.model.conv1.psis,
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        start_time = timeit.default_timer()

        newNL = 0
        for minibatch_index in xrange(n_batches):
            logLs_output_val = compute_logLs(minibatch_index)
            newNL = newNL + logLs_output_val

        newNL = newNL/n_batches
        NegLogLs_vec.append(newNL)
        print("Initial NLL=", newNL)

        done_looping = False

        while (epoch < max_epochs) and (not done_looping):

            epoch = epoch + 1
            epoch_vec.append(epoch)
            print('Epoch %d' %epoch)

            LA = []
            MU = []
            PS = []
            newLA = []
            newMU = []
            newPS = []

            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))
                MU.append(copy.copy(layer.means.get_value()))
                PS.append(copy.copy(layer.psis.get_value()))


            for minibatch_index in xrange(n_batches):
                print(minibatch_index)
                iter = iter + 1
                # debug_val = debug(minibatch_index)
                # print 'Psis'
                # print np.shape(debug_val)
                # print np.min(debug_val)
                # print np.max(debug_val)
                output_val = do_one_iter(minibatch_index)

            newNL = 0
            for minibatch_index in xrange(n_batches):
                logLs_output_val = compute_logLs(minibatch_index)
                newNL = newNL + logLs_output_val

            newNL = newNL/n_batches
            NegLogLs_vec.append(newNL)

            for i in xrange(num_layer):
                newLA.append(output_val[3 + 6*i])
                newMU.append(output_val[2 + 6*i])
                newPS.append(output_val[5 + 6*i])
                dLA_vec[i].append(norm(newLA[i] - LA[i])/norm(LA[i]))
                dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))
                dPS_vec[i].append(norm(newPS[i] - PS[i])/norm(PS[i]))

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []
            psis_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 6*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 6*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 6*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 6*i],dtype=theano.config.floatX))
                psis_val.append(np.asarray(output_val[5 + 6*i],dtype=theano.config.floatX))

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, amps_val=amps_val, psis_val=psis_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec, dPS_vec=dPS_vec)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
            close()

            fig4 = figure()
            plot(epoch_vec[1:], NegLogLs_vec[1:])
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
            fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dPS_vec[i], 'ro-', label='Ps_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                plot(epoch_vec[2:], dPS_vec[i][1:], 'ro-', label='Ps_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
                close()

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newNL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newNL

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newNL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('\n')
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print('\n')

        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec, dMU_vec=dMU_vec, dPS_vec=dPS_vec)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
        close()

        fig4 = figure()
        plot(epoch_vec[1:], NegLogLs_vec[1:])
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
        fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            plot(epoch_vec[1:], dPS_vec[i], 'ro-', label='Ps_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            plot(epoch_vec[2:], dPS_vec[i][1:], 'ro-', label='Ps_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch2.png' % (i + 1, self.batch_size)))
            close()

    def train_unsupervised_for_FRM_knowing_correct_params(self, max_epochs, verbose, tol, stop_mode, correct_lambdas,
                                                          correct_means, correct_lambda_covs, correct_psis,
                                                          correct_covs, correct_pis):
        '''
        EM learning algorithm
        :return:
        '''
        print('start EM training')

        N = np.shape(self.d)[0]

        n_batches = N/self.batch_size

        shared_dat = theano.shared(self.d, name='data', borrow=True)

        shared_dat_test = theano.shared(self.dtest, name='datatest', borrow=True)

        num_layer = len(self.model.layers)

        dLA_vec = []
        dMU_vec = []
        dPS_vec =[]
        dLC_vec = []
        dCO_vec = []
        dPI_vec = []
        NL = None
        NegLogLs_vec = []
        test_error_vec = []
        train_error_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)
        iter = 0

        for i in xrange(num_layer):
            # Concatenate differences in parameter values
            dLA_vec.append([])
            dMU_vec.append([])
            dPS_vec.append([])
            dLC_vec.append([])
            dCO_vec.append([])
            dPI_vec.append([])

        index = T.iscalar()

        do_one_iter = theano.function([index], self.model.output_var,
                                          updates=self.model.updates, on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size]})

        compute_logLs = theano.function([index], self.model.cost,
                                          updates=[], on_unused_input='ignore',
                                          givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size]})

        compute_test_error = theano.function([index], [self.model.conv1.rs, self.model.conv1.means],
                                          updates=[], on_unused_input='ignore',
                                          givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size]})

        compute_train_error = theano.function([index], [self.model.conv1.rs, self.model.conv1.means],
                                             updates=[], on_unused_input='ignore',
                                             givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size]})
        # debug = theano.function([index], self.model.conv1.psis,
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

        start_time = timeit.default_timer()

        newNL = 0
        test_error = 0
        train_error = 0
        for minibatch_index in xrange(n_batches):
            logLs_output_val = compute_logLs(minibatch_index)
            newNL = newNL + logLs_output_val
            rs_val_test, means_val_test = compute_test_error(minibatch_index)
            means_val_test = np.asarray(means_val_test, dtype=theano.config.floatX)
            rs_val_test = np.asarray(rs_val_test, dtype=theano.config.floatX)
            for i in xrange(self.model.K1):
                dist_vec = []
                for j in xrange(i, self.model.K1, 1):
                    dist_vec.append(norm(correct_means[i] - means_val_test[j]))

                right_order = np.argmin(dist_vec) + i

                rs_val_test[[right_order, i], :] = rs_val_test[[i, right_order], :]
                means_val_test[[right_order, i], :] = means_val_test[[i, right_order], :]

            labels_test = np.argmax(rs_val_test, 0)
            test_error_val = 1-precision_score(self.labeltest, labels_test, average='micro')
            test_error = test_error + test_error_val

            rs_val_train, means_val_train = compute_train_error(minibatch_index)
            means_val_train = np.asarray(means_val_train, dtype=theano.config.floatX)
            rs_val_train = np.asarray(rs_val_train, dtype=theano.config.floatX)
            for i in xrange(self.model.K1):
                dist_vec = []
                for j in xrange(i, self.model.K1, 1):
                    dist_vec.append(norm(correct_means[i] - means_val_train[j]))

                right_order = np.argmin(dist_vec) + i

                rs_val_train[[right_order, i], :] = rs_val_train[[i, right_order], :]
                means_val_train[[right_order, i], :] = means_val_train[[i, right_order], :]

            labels_train = np.argmax(rs_val_train, 0)
            train_error_val = 1 - precision_score(self.label, labels_train, average='micro')
            train_error = train_error + train_error_val

        newNL = newNL/n_batches
        NegLogLs_vec.append(newNL)
        print("Initial NLL=", newNL)

        test_error = np.mean(test_error)
        test_error_vec.append(test_error)
        print("Initial Test Error=", test_error)

        train_error = np.mean(train_error)
        train_error_vec.append(train_error)
        print("Initial Train Error=", train_error)

        done_looping = False

        while (epoch < max_epochs) and (not done_looping):

            epoch = epoch + 1
            epoch_vec.append(epoch)
            print('Epoch %d' %epoch)

            LA = []
            MU = []
            PS = []
            LC = []
            CO = []
            PI = []

            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))
                MU.append(copy.copy(layer.means.get_value()))
                PS.append(copy.copy(layer.psis.get_value()))
                LA_val = copy.copy(layer.lambdas.get_value())
                PS_val = copy.copy(layer.psis.get_value())
                LC_val = np.zeros((np.shape(correct_lambdas)[0], np.shape(correct_lambdas)[1], np.shape(correct_lambdas)[1]))
                CO_val = 0.0 * LC_val
                for k in xrange(np.shape(correct_lambdas)[0]):
                    LC_val[k] = np.dot(LA_val[k],LA_val[k].T)
                    CO_val[k] = LC_val[k] + np.diag(PS_val[k])

                LC.append(LC_val)
                CO.append(CO_val)
                PI_val = copy.copy(layer.amps.get_value())
                PI.append(PI_val)

            for n in xrange(num_layer):
                # Arrange clusters in a right order
                means_val = np.asarray(MU[n],dtype=theano.config.floatX)
                lambdas_val = np.asarray(LA[n],dtype=theano.config.floatX)
                psis_val = np.asarray(PS[n],dtype=theano.config.floatX)
                lambda_covs_val = np.asarray(LC[n],dtype=theano.config.floatX)
                covs_val = np.asarray(CO[n],dtype=theano.config.floatX)
                pis_val = np.asarray(PI[n],dtype=theano.config.floatX)
                for i in xrange(np.shape(correct_lambdas)[0]):
                    dist_vec = []
                    for j in xrange(i, np.shape(correct_lambdas)[0], 1):
                        dist_vec.append(norm(correct_means[i] - means_val[j]))

                    right_order = np.argmin(dist_vec) + i

                    means_val[[right_order, i],:] = means_val[[i, right_order],:]
                    lambdas_val[[right_order, i],:,:] = lambdas_val[[i, right_order],:,:]
                    psis_val[[right_order, i],:] = psis_val[[i, right_order],:]
                    lambda_covs_val[[right_order, i]] = lambda_covs_val[[i, right_order]]
                    covs_val[[right_order, i]] = covs_val[[i, right_order]]
                    pis_val[[right_order, i]] = pis_val[[i, right_order]]

                dLA_vec[n].append(norm(lambdas_val - correct_lambdas)/norm(correct_lambdas))
                dMU_vec[n].append(norm(means_val - correct_means)/norm(correct_means))
                dPS_vec[n].append(norm(psis_val - correct_psis)/norm(correct_psis))
                dLC_vec[n].append(norm(lambda_covs_val - correct_lambda_covs)/norm(correct_lambda_covs))
                dCO_vec[n].append(norm(covs_val - correct_covs)/norm(correct_covs))
                dPI_vec[n].append(norm(pis_val - correct_pis)/norm(correct_pis))

            for minibatch_index in xrange(n_batches):
                print(minibatch_index)
                iter = iter + 1
                output_val = do_one_iter(minibatch_index)

            newNL = 0
            test_error = 0
            train_error = 0
            for minibatch_index in xrange(n_batches):
                logLs_output_val = compute_logLs(minibatch_index)
                newNL = newNL + logLs_output_val
                rs_val_test, means_val_test = compute_test_error(minibatch_index)
                means_val_test = np.asarray(means_val_test, dtype=theano.config.floatX)
                rs_val_test = np.asarray(rs_val_test, dtype=theano.config.floatX)
                for i in xrange(self.model.K1):
                    dist_vec = []
                    for j in xrange(i, self.model.K1, 1):
                        dist_vec.append(norm(correct_means[i] - means_val_test[j]))

                    right_order = np.argmin(dist_vec) + i

                    rs_val_test[[right_order, i], :] = rs_val_test[[i, right_order], :]
                    means_val_test[[right_order, i], :] = means_val_test[[i, right_order], :]

                labels_test = np.argmax(rs_val_test, 0)
                test_error_val = 1 - precision_score(self.labeltest, labels_test, average='micro')
                test_error = test_error + test_error_val

                rs_val_train, means_val_train = compute_train_error(minibatch_index)
                means_val_train = np.asarray(means_val_train, dtype=theano.config.floatX)
                rs_val_train = np.asarray(rs_val_train, dtype=theano.config.floatX)
                for i in xrange(self.model.K1):
                    dist_vec = []
                    for j in xrange(i, self.model.K1, 1):
                        dist_vec.append(norm(correct_means[i] - means_val_train[j]))

                    right_order = np.argmin(dist_vec) + i

                    rs_val_train[[right_order, i], :] = rs_val_train[[i, right_order], :]
                    means_val_train[[right_order, i], :] = means_val_train[[i, right_order], :]

                labels_train = np.argmax(rs_val_train, 0)
                train_error_val = 1 - precision_score(self.label, labels_train, average='micro')
                train_error = train_error + train_error_val

            newNL = newNL / n_batches
            NegLogLs_vec.append(newNL)

            test_error = np.mean(test_error)
            test_error_vec.append(test_error)
            print("Test Error=", test_error)

            train_error = np.mean(train_error)
            train_error_vec.append(train_error)
            print("Train Error=", train_error)

            means_val = []
            lambdas_val = []
            betas_val = []
            amps_val = []
            psis_val = []

            for i in xrange(num_layer):
                means_val.append(np.asarray(output_val[2 + 6*i],dtype=theano.config.floatX))
                lambdas_val.append(np.asarray(output_val[3 + 6*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 6*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[4 + 6*i],dtype=theano.config.floatX))
                psis_val.append(np.asarray(output_val[5 + 6*i],dtype=theano.config.floatX))

            param_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, amps_val=amps_val, psis_val=psis_val)

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec,
                     dMU_vec=dMU_vec, dPS_vec=dPS_vec, dCO_vec=dCO_vec, dPI_vec=dPI_vec, test_error_vec=test_error_vec,
                     train_error_vec=train_error_vec)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
            close()

            fig4 = figure()
            plot(epoch_vec[1:], NegLogLs_vec[1:])
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
            fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec[:-1], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[:-1], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                plot(epoch_vec[:-1], dPS_vec[i], 'ro-', label='Ps_Layer%i'%(i + 1))
                plot(epoch_vec[:-1], dLC_vec[i], 'yo-', label='Lambda_Cov_Layer%i'%(i + 1))
                plot(epoch_vec[:-1], dCO_vec[i], 'ko-', label='Cov_Layer%i'%(i + 1))
                plot(epoch_vec[:-1], dPI_vec[i], 'mo-', label='Pi_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[1:-1], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
                plot(epoch_vec[1:-1], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                plot(epoch_vec[1:-1], dPS_vec[i][1:], 'ro-', label='Ps_Layer%i'%(i + 1))
                plot(epoch_vec[1:-1], dLC_vec[i][1:], 'yo-', label='Lambda_Cov_Layer%i'%(i + 1))
                plot(epoch_vec[1:-1], dCO_vec[i][1:], 'ko-', label='Cov_Layer%i'%(i + 1))
                plot(epoch_vec[1:-1], dPI_vec[i][1:], 'mo-', label='Pi_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
                close()

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newNL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newNL

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newNL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('\n')
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print('\n')

        forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
        np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec,
                 dMU_vec=dMU_vec, dPS_vec=dPS_vec, dCO_vec=dCO_vec, dPI_vec=dPI_vec, test_error_vec=test_error_vec,
                 train_error_vec=train_error_vec)

        fig1 = figure()
        plot(epoch_vec, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i.png' % self.batch_size))
        close()

        fig4 = figure()
        plot(epoch_vec[1:], NegLogLs_vec[1:])
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1' % self.batch_size)
        fig4.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_BatchSize_%i_StartEpoch1.png' % self.batch_size))
        close()

        for i in xrange(num_layer):
            fig2 = figure()
            plot(epoch_vec[:-1], dLA_vec[i], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[:-1], dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
            plot(epoch_vec[:-1], dPS_vec[i], 'ro-', label='Ps_Layer%i'%(i + 1))
            plot(epoch_vec[:-1], dLC_vec[i], 'yo-', label='Lambda_Cov_Layer%i'%(i + 1))
            plot(epoch_vec[:-1], dCO_vec[i], 'ko-', label='Cov_Layer%i'%(i + 1))
            plot(epoch_vec[:-1], dPI_vec[i], 'mo-', label='Pi_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
            fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
            close()

            fig3 = figure()
            plot(epoch_vec[1:-1], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i'%(i + 1))
            plot(epoch_vec[1:-1], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
            plot(epoch_vec[1:-1], dPS_vec[i][1:], 'ro-', label='Ps_Layer%i'%(i + 1))
            plot(epoch_vec[1:-1], dLC_vec[i][1:], 'yo-', label='Lambda_Cov_Layer%i'%(i + 1))
            plot(epoch_vec[1:-1], dCO_vec[i][1:], 'ko-', label='Cov_Layer%i'%(i + 1))
            plot(epoch_vec[1:-1], dPI_vec[i][1:], 'mo-', label='Pi_Layer%i'%(i + 1))
            legend()
            xlabel('Epoch')
            ylabel('Relative Difference')
            title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
            fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
            close()

        LA = []
        MU = []
        PS = []
        LC = []
        CO = []
        PI = []

        for layer in self.model.layers:
            LA.append(copy.copy(layer.lambdas.get_value()))
            MU.append(copy.copy(layer.means.get_value()))
            PS.append(copy.copy(layer.psis.get_value()))
            LA_val = copy.copy(layer.lambdas.get_value())
            PS_val = copy.copy(layer.psis.get_value())
            LC_val = np.zeros((np.shape(correct_lambdas)[0], np.shape(correct_lambdas)[1], np.shape(correct_lambdas)[1]))
            CO_val = 0.0 * LC_val
            for k in xrange(np.shape(correct_lambdas)[0]):
                LC_val[k] = np.dot(LA_val[k],LA_val[k].T)
                CO_val[k] = LC_val[k] + np.diag(PS_val[k])

            LC.append(LC_val)
            CO.append(CO_val)
            PI_val = copy.copy(layer.amps.get_value())
            PI.append(PI_val)

        for n in xrange(num_layer):
            # Arrange clusters in a right order
            means_val = np.asarray(MU[n],dtype=theano.config.floatX)
            lambdas_val = np.asarray(LA[n],dtype=theano.config.floatX)
            psis_val = np.asarray(PS[n],dtype=theano.config.floatX)
            lambda_covs_val = np.asarray(LC[n],dtype=theano.config.floatX)
            covs_val = np.asarray(CO[n],dtype=theano.config.floatX)
            pis_val = np.asarray(PI[n],dtype=theano.config.floatX)
            for i in xrange(np.shape(correct_lambdas)[0]):
                dist_vec = []
                for j in xrange(i, np.shape(correct_lambdas)[0], 1):
                    dist_vec.append(norm(correct_means[i] - means_val[j]))

                right_order = np.argmin(dist_vec) + i

                means_val[[right_order, i],:] = means_val[[i, right_order],:]
                lambdas_val[[right_order, i],:,:] = lambdas_val[[i, right_order],:,:]
                psis_val[[right_order, i],:] = psis_val[[i, right_order],:]
                lambda_covs_val[[right_order, i]] = lambda_covs_val[[i, right_order]]
                covs_val[[right_order, i]] = covs_val[[i, right_order]]
                pis_val[[right_order, i]] = pis_val[[i, right_order]]

        print('\nTotal Cov Matrix')
        print(norm(covs_val - correct_covs)/norm(correct_covs))
        print('\nLambdas Cov Matrix')
        print(norm(lambda_covs_val - correct_lambda_covs)/norm(correct_lambda_covs))
        print('\nPsi Matrix')
        print(norm(psis_val - correct_psis)/norm(correct_psis))
        print('\nMeans')
        print(norm(means_val - correct_means)/norm(correct_means))
        print('\nPis')
        print(norm(pis_val - correct_pis)/norm(correct_pis))
        print('\nCorrect Pis')
        print(correct_pis)
        print('\nLearned Pis')
        print(pis_val)

        xtest = T.fmatrix('xtest')
        ytest = T.ivector('ytest')

        test_model = FRM(data=xtest, labels=ytest, K=self.model.K1, D=self.model.D1, M=self.model.M1, Ni=self.model.batch_size,
                             lambdas_val_init=lambdas_val, means_val_init=means_val, amps_val_init=pis_val, psis_val_init=psis_val,
                             PPCA=False, lock_psis=True, rs_clip = 0.0, max_condition_number=1.e3)

        _, means_val_train, rs_val_train = test_model.predict(x=xtest, dat=self.d)
        means_val_train = np.asarray(means_val_train,dtype=theano.config.floatX)
        rs_val_train = np.asarray(rs_val_train,dtype=theano.config.floatX)
        for i in xrange(self.model.K1):
            dist_vec = []
            for j in xrange(i, self.model.K1, 1):
                dist_vec.append(norm(correct_means[i] - means_val_train[j]))

            right_order = np.argmin(dist_vec) + i

            rs_val_train[[right_order, i],:] = rs_val_train[[i, right_order],:]
            means_val_train[[right_order, i],:] = means_val_train[[i, right_order],:]

        labels_train = np.argmax(rs_val_train,0)
        error_score = 1 - precision_score(self.label, labels_train, average='micro')
        print('Error on Train Set is %f' %error_score)

        _, means_val_test, rs_val_test = test_model.predict(x=xtest, dat=self.dtest)
        means_val_test = np.asarray(means_val_test,dtype=theano.config.floatX)
        rs_val_test = np.asarray(rs_val_test,dtype=theano.config.floatX)
        for i in xrange(self.model.K1):
            dist_vec = []
            for j in xrange(i, self.model.K1, 1):
                dist_vec.append(norm(correct_means[i] - means_val_test[j]))

            right_order = np.argmin(dist_vec) + i

            rs_val_test[[right_order, i],:] = rs_val_test[[i, right_order],:]
            means_val_test[[right_order, i],:] = means_val_test[[i, right_order],:]

        labels_test = np.argmax(rs_val_test,0)
        error_score = 1-precision_score(self.labeltest, labels_test, average='micro')
        print('Accuracy on Test Set is %f' %error_score)
