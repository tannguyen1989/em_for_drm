__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from CRM_v1 import CRM
import gzip
import cPickle
import os
import sys
import time
import timeit

import numpy as np

import theano
import theano.tensor as T

import copy

from PIL import Image, ImageDraw

from scipy.linalg import norm

# from guppy import hpy; h=hpy()

from old_codes.nn_functions_stable import LogisticRegression

class EM_DRM(object):
    """
    Apply EM algorithm for RM to an arbitrary dataset

    """
    def __init__(self, data_mode, em_mode, update_mode, stop_mode, train_method, Ni, Cin, H, W, batch_size, seed,
                 output_dir, data_dir):

        self.data_mode = data_mode
        self.em_mode = em_mode
        self.update_mode = update_mode
        self.stop_mode = stop_mode
        self.train_method = train_method

         # set internal parameters
        self.Ni = Ni

        self.H = H
        self.W = W
        self.Cin = Cin

        self.batch_size = batch_size
        self.seed = seed

        self.output_dir = output_dir
        self.data_dir = data_dir
        self.param_dir = os.path.join(self.output_dir, 'params')
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        if not os.path.exists(self.param_dir):
            os.makedirs(self.param_dir)

        self.load_data()

    def load_data(self):
        self.d = []
        self.dtest = []
        self.dvalid = []
        self.train_label = []
        self.test_label = []
        self.valid_label = []

    def build_model(self):
        """
        Build a deep model
        :return:
        """
        print 'Train DRM'

        self.h1 = 5
        self.w1 = 5
        self.K1 = 21
        self.M1 = 1

        # self.h2 = 3
        # self.w2 = 3
        # self.K2 = 51
        # self.M2 = 1

        self.x = T.tensor4('x')
        self.y = T.ivector('y')


        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W, H=self.H, w=self.w1, h=self.h1, Cin=self.Cin, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                         covs_val_init=None, inv_covs_val_init=None, psis_val_init=None, means_val_init=None,
                         PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                         rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        self.conv1._E_step()

        # self.conv2 = ConvMofa(data_4D=self.conv1.latents_rs, labels=self.y, K=self.K2, M=self.M2, W=(self.W - self.w1 + 1)/2, H=(self.H - self.h1 + 1)/2, w=self.w2, h=self.h2, Cin=self.K1 - 1, Ni=self.batch_size,
        #                amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
        #                covs_val_init=None, inv_covs_val_init=None, psis_val_init=None, means_val_init=None,
        #                PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
        #                rs_clip = 0.0, max_condition_number=1.e3, init=True,init_ppca=False)
        # self.conv2._E_step()
        #
        # self.conv2._M_step()

        self.conv1._M_step()

        # self.layers = [self.conv1, self.conv2]

        self.layers = [self.conv1,]

    def batch_fit(self, max_epochs, learning_rate, tol, verbose):
        N, Cin, h, w = np.shape(self.d)
        n_batches = N/self.batch_size

        index = T.iscalar()
        shared_dat = theano.shared(self.d)

        if self.train_method == 'EM':
            print 'take EM'
            self.build_model()
            print 'finish taking EM'

            num_layer = len(self.layers)

            updates = []
            output_var = []
            dPS_vec = []
            dLC_vec = []
            dMU_vec = []
            NL = None
            NegLogLs_vec = []
            epoch_vec = []
            epoch = -1

            for layer in self.layers:
                # Concatenate updates
                updates.append((layer.means, layer.means_new))
                updates.append((layer.lambdas, layer.lambdas_new))
                updates.append((layer.psis, layer.psis_new))
                updates.append((layer.covs, layer.covs_new))
                updates.append((layer.inv_covs, layer.inv_covs_new))
                updates.append((layer.amps, layer.amps_new))
                updates.append((layer.lambda_covs, layer.lambda_covs_new))
                # Concatenate output_var
                output_var.append(layer.logLs)
                output_var.append(layer.rs)
                output_var.append(layer.betas)
                output_var.append(layer.latents)
                output_var.append(layer.latent_covs)
                output_var.append(layer.sumrs)
                output_var.append(layer.means_new)
                output_var.append(layer.covs_new)
                output_var.append(layer.inv_covs_new)
                output_var.append(layer.lambdas_new)
                output_var.append(layer.amps_new)
                output_var.append(layer.psis_new)
                output_var.append(layer.lambda_covs_new)
                # Concatenate differences in parameter values
                dPS_vec.append([])
                dLC_vec.append([])
                dMU_vec.append([])

            output_one_iter = theano.function([index], output_var,
                                              updates=updates, on_unused_input='ignore',
                                              givens={self.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:]})

            start_time = timeit.default_timer()

            done_looping = False

            while (epoch < max_epochs) and (not done_looping):

                epoch = epoch + 1
                print 'Epoch %d' %epoch

                LC = []
                PS = []
                MU = []
                newLC = []
                newPS = []
                newMU = []
                logLs_val_vec = []

                for layer in self.layers:
                    LC.append(copy.copy(layer.lambda_covs.get_value()))
                    PS.append(copy.copy(layer.psis.get_value()))
                    MU.append(copy.copy(layer.means.get_value()))
                    # Concatenate logLs using layers
                    logLs_val_vec.append([])

                for minibatch_index in xrange(n_batches):
                    print minibatch_index
                    output_val = output_one_iter(minibatch_index)
                    for i in xrange(num_layer):
                        logLs_val_vec[i].append(output_val[13*i])

                newNL = 0
                for i in xrange(num_layer):
                    logLs_val_vec_temp = np.asarray(logLs_val_vec[i])
                    newNL = newNL - np.mean(logLs_val_vec_temp)
                    newLC.append(output_val[12 + 13*i])
                    newPS.append(output_val[11 + 13*i])
                    newMU.append(output_val[6 + 13*i])
                    dLC_vec[i].append(norm(newLC[i] - LC[i])/norm(LC[i]))
                    dMU_vec[i].append(norm(newMU[i] - MU[i])/norm(MU[i]))
                    dPS_vec[i].append(tol/2)

                if epoch == 0 and verbose:
                    print("Initial NLL=", newNL)

                # print amps_val

                if self.stop_mode == 'NLL':
                    if NL!=None:
                        dNL = np.abs((newNL-NL)/NL)
                        if epoch > 5 and dNL < tol:
                            break
                    NL = newNL

                # elif self.em_mode == 'soft':
                #     if epoch > 5 and dLC1 < tol and dPS1 < tol and dMU1 < tol and dLC2 < tol and dPS2 < tol and dMU2 < tol:
                #         break
                # else:
                #     if epoch > 5 and dMU1 < tol and dLC1 < tol and dMU2 < tol and dLC2 < tol:
                #         break

                NegLogLs_vec.append(newNL)
                epoch_vec.append(epoch)

                means_val = []
                lambdas_val = []
                betas_val = []
                psis_val = []
                covs_val = []
                inv_covs_val = []
                amps_val = []
                lambda_covs_val = []

                # output_var.append(layer.logLs)
                # output_var.append(layer.rs)
                # output_var.append(layer.betas)
                # output_var.append(layer.latents)
                # output_var.append(layer.latent_covs)
                # output_var.append(layer.sumrs)
                # output_var.append(layer.means_new)
                # output_var.append(layer.covs_new)
                # output_var.append(layer.inv_covs_new)
                # output_var.append(layer.lambdas_new)
                # output_var.append(layer.amps_new)
                # output_var.append(layer.psis_new)
                # output_var.append(layer.lambda_covs_new)

                for i in xrange(num_layer):
                    means_val.append(np.asarray(output_val[6 + 13*i],dtype=theano.config.floatX))
                    lambdas_val.append(np.asarray(output_val[9 + 13*i],dtype=theano.config.floatX))
                    betas_val.append(np.asarray(output_val[2 + 13*i],dtype=theano.config.floatX))
                    psis_val.append(np.asarray(output_val[11 + 13*i],dtype=theano.config.floatX))
                    covs_val.append(np.asarray(output_val[7 + 13*i],dtype=theano.config.floatX))
                    inv_covs_val.append(np.asarray(output_val[8 + 13*i],dtype=theano.config.floatX))
                    amps_val.append(np.asarray(output_val[10 + 13*i],dtype=theano.config.floatX))
                    lambda_covs_val.append(np.asarray(output_val[12 + 13*i],dtype=theano.config.floatX))

                    fig2 = figure()
                    plot(epoch_vec, dLC_vec[i], 'bo-', label='Lambda_Covs_Layer%i'%(i + 1))
                    plot(epoch_vec, dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                    legend()
                    xlabel('Epoch')
                    ylabel('Relative Difference')
                    title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                    fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                    close()

                result_file = os.path.join(self.param_dir, 'EM_results_epoch_%i.npz' %epoch)
                np.savez(result_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, psis_val=psis_val,
                         covs_val=covs_val, inv_covs_val=inv_covs_val, lambda_covs_val=lambda_covs_val, amps_val=amps_val)

                fig1 = figure()
                plot(epoch_vec, NegLogLs_vec)
                xlabel('Epoch')
                ylabel('Negative LogLikelihood')
                title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % self.batch_size)
                fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i.png' % self.batch_size))
                close()


            if epoch < max_epochs - 1:
                if verbose:
                    print("EM converged after {0} epochs".format(epoch))
                    print("Final NLL={0}".format(newNL))
            else:
                print("Warning:EM didn't converge after {0} epochs".format(epoch))

            print '\n'
            print("--- %s seconds ---" % (timeit.default_timer() - start_time))
            print '\n'

            result_file = os.path.join(self.param_dir, 'for_Plotting.npz')
            np.savez(result_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLC_vec=dLC_vec, dMU_vec=dMU_vec)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Negative LogLikelihood')
            title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i.png' % self.batch_size))
            close()

            for i in xrange(num_layer):
                fig2 = figure()
                plot(epoch_vec, dLC_vec[i], 'bo-', label='Lambda_Covs_Layer%i'%(i + 1))
                plot(epoch_vec, dMU_vec[i], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i.png' % (i + 1, self.batch_size)))
                close()

                fig3 = figure()
                plot(epoch_vec[1:], dLC_vec[i][1:], 'bo-', label='Lambda_Covs_Layer%i'%(i + 1))
                plot(epoch_vec[1:], dMU_vec[i][1:], 'go-', label='Mu_Layer%i'%(i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig3.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_my_MFA_Layer%i_BatchSize_%i_StartEpoch1.png' % (i + 1, self.batch_size)))
                close()

        else:
            print 'take EG'
            self.take_EG_step(learning_rate=learning_rate)
            print 'finish taking EG'

            updates = []
            updates.append((self.means, self.means_new))
            updates.append((self.lambdas, self.lambdas_new))
            updates.append((self.psis, self.psis_new))
            updates.append((self.covs, self.covs_new))
            updates.append((self.inv_covs, self.inv_covs_new))
            updates.append((self.amps, self.amps_new))
            updates.append((self.lambda_covs, self.lambda_covs_new))
            updates.append((self.softmax_layer.params[0], self.W_new))
            updates.append((self.softmax_layer.params[1], self.b_new))

            output_one_iter = theano.function([index], [self.cost, self.logLs, self.rs, self.betas, self.latents,
                                                        self.latent_covs, self.sumrs,
                                                        self.means_new, self.covs_new, self.inv_covs_new,
                                                        self.lambdas_new, self.amps_new, self.psis_new, self.lambda_covs_new, self.data],
                                              updates=updates, on_unused_input='ignore',
                                              givens={x: shared_dat[index * batch_size: (index + 1) * batch_size,:,:,:],
                                                      y: shared_label[index * batch_size: (index + 1) * batch_size]})

            test_model = theano.function([index], self.softmax_layer.errors(y),
                                         givens={x: shared_dat_test[index * batch_size: (index + 1) * batch_size],
                                                 y: shared_label_test[index * batch_size: (index + 1) * batch_size]})

            validate_model = theano.function([index], self.softmax_layer.errors(y),
                                             givens={x: shared_dat_valid[index * batch_size: (index + 1) * batch_size],
                                                     y: shared_label_valid[index * batch_size: (index + 1) * batch_size]})


            done_looping = False
            NL = None
            NegLogLs_vec = []
            epochs = []
            epoch = -1
            LC = copy.copy(self.lambda_covs.get_value())
            PS = copy.copy(self.psis.get_value())
            MU = copy.copy(self.means.get_value())
            dPS_vec = []
            dLC_vec = []
            dMU_vec = []

            patience = 10000  # look as this many examples regardless
            patience_increase = 2  # wait this much longer when a new best is
                                   # found
            improvement_threshold = 0.995  # a relative improvement of this much is
                                           # considered significant
            validation_frequency = min(n_batches, patience / 2)
                                          # go through this many
                                          # minibatche before checking the network
                                          # on the validation set; in this case we
                                          # check every epoch

            best_params = None
            best_validation_loss = np.inf
            best_iter = 0
            test_score = 0.0
            test_score_epoch = 0.0
            test_score_vec = []

            while (epoch < max_epochs) and (not done_looping):
                epoch = epoch + 1
                print 'Epoch %d' %epoch
                logLs_val_vec = []
                for minibatch_index in xrange(n_batches):
                    print minibatch_index
                    iter = epoch*n_batches + minibatch_index

                    cost_val, logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, \
                    means_val, covs_val, inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, data_val \
                        = output_one_iter(minibatch_index)
                    logLs_val_vec.append(logLs_val)

                    if (iter + 1) % 1 == 0:
                        validation_losses = [validate_model(i) for i
                                         in xrange(n_valid_batches)]
                        this_validation_loss = np.mean(validation_losses)
                        print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                              (epoch, minibatch_index + 1, n_batches, \
                               this_validation_loss * 100.))

                        # if we got the best validation score until now
                        if this_validation_loss < best_validation_loss:

                            #improve patience if loss improvement is good enough
                            if this_validation_loss < best_validation_loss *  \
                               improvement_threshold:
                                patience = max(patience, iter * patience_increase)

                            # save best validation score and iteration number
                            best_validation_loss = this_validation_loss
                            best_iter = iter
                            best_params = self.params

                            # test it on the test set
                            test_losses = [test_model(i) for i in xrange(n_test_batches)]
                            test_score = numpy.mean(test_losses)
                            print(('     epoch %i, minibatch %i/%i, test error of best '
                                   'model %f %%') %
                                  (epoch, minibatch_index + 1, n_batches,
                                   test_score * 100.))

                        if (iter + 1) % batch_size == 0:
                            test_losses = [test_model(i) for i in xrange(n_test_batches)]
                            test_score_epoch = numpy.mean(test_losses)
                            test_score_vec.append(test_score_epoch)
                            epochs.append(epoch)

                    if patience <= iter:
                        done_looping = True
                        break

                # logLs_val_whole_dataset = cal_logLs(K=K, N=N, D=D, data=dat,
                #                                    covs=covs_val, means=means_val, inv_covs=inv_covs_val, pi=amps_val)
                # newL = np.sum(logLs_val_whole_dataset)
                logLs_val_vec = np.asarray(logLs_val_vec)
                newNL = -np.mean(logLs_val_vec)

                if epoch == 0 and verbose:
                    print("Initial NLL=", newNL)

                newLC = self.lambda_covs.get_value()
                newPS = self.psis.get_value()
                newMU = self.means.get_value()
                dLC = norm(newLC - LC)/norm(LC)
                # dPS = norm(newPS - PS)/norm(PS)
                dPS = tol/2
                dMU = norm(newMU - MU)/norm(MU)

                # print amps_val

                if mode == 'NLL':
                    if NL!=None:
                        dNL = np.abs((newNL-NL)/NL)
                        if epoch > 5 and dNL < tol:
                            break
                    NL = newNL
                elif self.em_mode == 'soft':
                    if epoch > 5 and dLC < tol and dPS < tol and dMU < tol:
                        break
                else:
                    if dMU < tol and dLC < tol:
                        break

                LC = copy.copy(newLC)
                PS = copy.copy(newPS)
                MU = copy.copy(newMU)
                dLC_vec.append(dLC)
                dPS_vec.append(dPS)
                dMU_vec.append(dMU)
                NegLogLs_vec.append(newNL)
                epochs.append(epoch)

                means_val = np.asarray(means_val,dtype=theano.config.floatX)
                lambdas_val = np.asarray(lambdas_val,dtype=theano.config.floatX)
                betas_val = np.asarray(betas_val,dtype=theano.config.floatX)
                psis_val = np.asarray(psis_val,dtype=theano.config.floatX)
                covs_val = np.asarray(covs_val,dtype=theano.config.floatX)
                inv_covs_val = np.asarray(inv_covs_val,dtype=theano.config.floatX)
                amps_val = np.asarray(amps_val,dtype=theano.config.floatX)
                lambda_covs_val = np.asarray(lambda_covs_val,dtype=theano.config.floatX)

                result_file = os.path.join(result_dir, 'EM_results_epoch_%i.npz' %epoch)
                np.savez(result_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, psis_val=psis_val,
                         covs_val=covs_val, inv_covs_val=inv_covs_val, lambda_covs_val=lambda_covs_val, amps_val=amps_val)

            if epoch < max_epochs - 1:
                if verbose:
                    print("EM converged after {0} epochs".format(epoch))
                    print("Final NLL={0}".format(newNL))
            else:
                print("Warning:EM didn't converge after {0} epochs".format(epoch))

            print('Optimization complete.')
            print('Best validation score of %f %% obtained at iteration %i,'\
                  'with test performance %f %%' %
                  (best_validation_loss * 100., best_iter + 1, test_score * 100.))

            save_file = open('cnn_mean_removed.pkl', 'wb')  # this will overwrite current content
            cPickle.dump(best_params, save_file, -1)
            save_file.close()
            fig = figure()
            plot(epochs, test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch-EG')
            fig.savefig('Error-vs-Epoch-EG.png')
            close(fig)

        return betas_val, latents_val, latent_covs_val, \
               means_val, covs_val, inv_covs_val, lambdas_val, psis_val, lambda_covs_val, amps_val, epochs, \
               dLC_vec, dPS_vec, dMU_vec, NegLogLs_vec


    def plot_betas_means_lambdas(self, param_dir):
        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            betas_val = params['betas_val']

        print np.shape(betas_val)
        print np.shape(lambdas_val)
        print np.shape(means_val)

        output_dir = os.path.join(self.output_dir, 'betas_means_lambdas')
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        nrows = (self.K - 1)/5 + (self.K - 1)%5
        new_img_betas = Image.new('RGB', (4000, 600*nrows))
        new_img_lambdas = Image.new('RGB', (4000, 600*nrows))
        new_img_means = Image.new('RGB', (4000, 600*nrows))

        for k in xrange(1, self.K):
            betas_mat = np.reshape(betas_val[k-1,0,:], (self.Cin, self.h, self.w))
            betas_mat = betas_mat.transpose((1,2,0))
            fig = figure()
            if self.Cin == 1:
                imshow(betas_mat, cmap = cm.Greys_r)
            else:
                imshow(betas_mat)

            axis('off')
            savefig(os.path.join(output_dir, 'filter_%d.png' %k))
            close()
            this_img_filter = Image.open(os.path.join(output_dir, 'filter_%i.png'%k))
            x_loc = ((k-1)%5)*800
            y_loc = ((k-1)/5)*600
            new_img_betas.paste(this_img_filter,(x_loc,y_loc))

            lambda_mat = np.reshape(lambdas_val[k], (self.Cin, self.h, self.w))
            lambda_mat = lambda_mat.transpose((1,2,0))
            fig = figure()
            if self.Cin == 1:
                imshow(lambda_mat, cmap = cm.Greys_r)
            else:
                imshow(lambda_mat)

            axis('off')
            savefig(os.path.join(output_dir, 'lambda_%d.png' %k))
            close()
            this_img_lambda = Image.open(os.path.join(output_dir, 'lambda_%i.png'%k))
            x_loc = ((k-1)%5)*800
            y_loc = ((k-1)/5)*600
            new_img_lambdas.paste(this_img_lambda,(x_loc,y_loc))

            mean_mat = np.reshape(means_val[k], (self.Cin, self.h, self.w))
            mean_mat = mean_mat.transpose((1,2,0))
            fig = figure()
            if self.Cin == 1:
                imshow(mean_mat, cmap = cm.Greys_r)
            else:
                imshow(mean_mat)

            axis('off')
            savefig(os.path.join(output_dir, 'mean_%d.png' %k))
            close()
            this_img_mean = Image.open(os.path.join(output_dir, 'mean_%i.png'%k))
            x_loc = ((k-1)%5)*800
            y_loc = ((k-1)/5)*600
            new_img_means.paste(this_img_mean,(x_loc,y_loc))

        imshow(new_img_betas)
        axis('off')
        savefig(os.path.join(output_dir, 'all_filters.pdf'))
        close()

        imshow(new_img_lambdas)
        axis('off')
        savefig(os.path.join(output_dir, 'all_lambdas.pdf'))
        close()

        imshow(new_img_means)
        axis('off')
        savefig(os.path.join(output_dir, 'all_means.pdf'))
        close()

    def sample_from_model(self, param_dir, Nsample = 200):
        sample_dir = os.path.join(self.output_dir, 'samples')
        if not os.path.exists(sample_dir):
            os.makedirs(sample_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = CRM(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                  amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                  covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                  PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                  rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        rs_val, z_val = mix._get_Rs_activities(x=x, dat=input_dat)

        print 'Shape of z'
        print np.shape(z_val)

        Nh = self.H - self.h + 1
        Nw = self.W - self.w + 1
        d = np.zeros((Nsample, self.Cin, self.H, self.W), dtype=theano.config.floatX)

        rs = np.reshape(rs_val, newshape=(self.K, Nsample, self.H - self.h + 1, self.W - self.w + 1))
        rs = rs.transpose((1,0,2,3))

        lambdas_val = np.reshape(lambdas_val, newshape=(self.K, self.h, self.w))
        means_val = np.reshape(means_val, newshape=(self.K, self.h, self.w))

        for n in xrange(Nsample):
            for k in xrange(1, self.K):
                for hindx in xrange(Nh):
                    for windx in xrange(Nw):
                        d[n, : , hindx:hindx + self.h, windx:windx + self.w] \
                            = d[n, : , hindx:hindx + self.h, windx:windx + self.w] + \
                              rs[n,k,hindx,windx]*(lambdas_val[k]*z_val[n,k - 1,hindx,windx] + means_val[k])

        d = d.transpose((0,2,3,1))
        print np.shape(d)
        for i in xrange(Nsample):
            imshow(d[i,:,:,0], cmap='gray')
            axis('off')
            out_path = os.path.join(sample_dir, 'SampledImage_%i.jpg'%i)
            savefig(out_path)
            close()

    def naive_sample_from_model(self, param_dir, Nsample = 200):
        sample_dir = os.path.join(self.output_dir, 'naive_samples')
        if not os.path.exists(sample_dir):
            os.makedirs(sample_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = CRM(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                  amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                  covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                  PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                  rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        rs_val, z_val = mix._get_Rs_activities(x=x, dat=input_dat)

        Nh = self.H - self.h + 1
        Nw = self.W - self.w + 1
        d = np.zeros((Nsample, self.Cin, self.H, self.W), dtype=theano.config.floatX)

        rs = np.reshape(rs_val, newshape=(self.K, Nsample, self.H - self.h + 1, self.W - self.w + 1))
        rs = rs.transpose((1,0,2,3))

        z_val = z_val[:,0,:]
        z_val = np.reshape(z_val, newshape=(self.K - 1, Nsample, self.H - self.h + 1, self.W - self.w + 1))
        z_val = z_val.transpose((1,0,2,3))

        lambdas_val = np.reshape(lambdas_val, newshape=(self.K, self.h, self.w))
        means_val = np.reshape(means_val, newshape=(self.K, self.h, self.w))

        for n in xrange(Nsample):
            for k in xrange(1, self.K):
                for hindx in xrange(Nh):
                    for windx in xrange(Nw):
                        d[n, : , hindx:hindx + self.h, windx:windx + self.w] \
                            = d[n, : , hindx:hindx + self.h, windx:windx + self.w] + \
                              rs[n,k,hindx,windx]*(lambdas_val[k]*z_val[n,k - 1,hindx,windx] + means_val[k])

        d = d.transpose((0,2,3,1))
        print np.shape(d)
        for i in xrange(Nsample):
            imshow(d[i,:,:,0], cmap='gray')
            axis('off')
            out_path = os.path.join(sample_dir, 'NaiveSampledImage_%i.jpg'%i)
            savefig(out_path)
            close()

    def naive_sample_from_model_Theano(self, param_dir, Nsample = 200):
        # Used to test the Top-Down pass
        sample_dir = os.path.join(self.output_dir, 'topdown_samples')
        if not os.path.exists(sample_dir):
            os.makedirs(sample_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = CRM(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                  amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                  covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                  PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                  rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)


        x_val = mix._get_samples(x=x, dat=input_dat)

        Nh = self.H - self.h + 1
        Nw = self.W - self.w + 1
        d = np.zeros((Nsample, self.Cin, self.H, self.W), dtype=theano.config.floatX)

        indx = 0
        for n in xrange(Nsample):
                for hindx in xrange(Nh):
                    for windx in xrange(Nw):
                        d[n, : , hindx:hindx + self.h, windx:windx + self.w] \
                            = d[n, : , hindx:hindx + self.h, windx:windx + self.w] + np.reshape(x_val[indx],(self.Cin, self.h, self.w))
                        indx = indx + 1

        d = d.transpose((0,2,3,1))
        print np.shape(d)
        for i in xrange(Nsample):
            imshow(d[i,:,:,0], cmap='gray')
            axis('off')
            out_path = os.path.join(sample_dir, 'TopDownSampledImage_%i.jpg'%i)
            savefig(out_path)
            close()

    def plot_attention(self, param_dir):
        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:20] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = CRM(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                  amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                  covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                  PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                  rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        print 'Start computing activations'

        acts_val = mix._get_activities(x=x, dat=input_dat)
        acts_val = acts_val[:,0,:]
        acts_val = np.reshape(acts_val, newshape=(self.K-1, num_input, self.Np))
        acts_val = acts_val.transpose((1,0,2))
        acts_mean = np.mean(acts_val, axis=(0,2))
        acts_var = np.var(acts_val, axis=(0,2))

        print 'Shape acts_mean'
        print np.shape(acts_mean)

        print 'Shape acts_var'
        print np.shape(acts_var)

        acts_val = (acts_val - acts_mean[None,:,None])/acts_var[None,:,None]
        acts_val = acts_val - np.min(acts_val, axis=(0,2))[None,:,None]
        max_acts_val = np.max(acts_val, axis=(0,2))

        print 'Shape of max_acts_val'
        print np.shape(max_acts_val)

        print 'Shape of acts_val'
        print np.shape(acts_val)

        print 'Done computing activations'

        max_indx = np.argmax(acts_val, axis=2)
        print 'Shape of max_indx'
        print np.shape(max_indx)

        for k in xrange(self.K-1):
            attention_dir = os.path.join(self.output_dir, 'attention_filter%i_each_image'%(k+1))
            if not os.path.exists(attention_dir):
                os.makedirs(attention_dir)

            for n in xrange(num_input):
                im_mat = self.d[n, 0, :, :]
                img = Image.fromarray(im_mat)
                draw = ImageDraw.Draw(img)
                x_indx = max_indx[n,k]%(self.W - self.w + 1)
                y_indx = max_indx[n,k]/(self.W - self.w + 1)
                outline = acts_val[n,k,max_indx[n,k]]/max_acts_val[k]
                draw.rectangle([(x_indx, y_indx),(x_indx + self.w - 1, y_indx + self.h - 1)], outline=outline)
                new_data = np.asarray(img)
                new_data = np.tile(new_data,(3,1,1))
                new_data = new_data.transpose((1,2,0))
                imshow(new_data)
                savefig(os.path.join(attention_dir, 'image%i.png'%(n+1)))
                close()

    def plot_top_attentions(self, param_dir, N_best_attn=100):
        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        x = T.tensor4('x')
        input_dat = self.d[0:self.Ni] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = CRM(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                  amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                  covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                  PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                  rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        print 'Start computing activations'

        acts_val = mix._get_activities(x=x, dat=input_dat)
        acts_val = acts_val[:,0,:]
        acts_val = np.reshape(acts_val, newshape=(self.K-1, num_input, self.Np))
        acts_val = acts_val.transpose((1,0,2))
        acts_mean = np.mean(acts_val, axis=(0,2))
        acts_var = np.var(acts_val, axis=(0,2))

        print 'Shape acts_mean'
        print np.shape(acts_mean)

        print 'Shape acts_var'
        print np.shape(acts_var)

        acts_val = (acts_val - acts_mean[None,:,None])/acts_var[None,:,None]
        acts_val = acts_val - np.min(acts_val, axis=(0,2))[None,:,None]
        max_acts_val = np.max(acts_val, axis=(0,2))

        print 'Shape of max_acts_val'
        print np.shape(max_acts_val)

        print 'Shape of acts_val'
        print np.shape(acts_val)

        print 'Done computing activations'

        for k in xrange(self.K - 1):
            filter_dir = os.path.join(self.output_dir, 'attention_filter%i_whole_dataset'%(k+1))
            if not os.path.exists(filter_dir):
                os.makedirs(filter_dir)

            max_indices = np.unravel_index(acts_val[:,k,:].argsort(axis=None)[-N_best_attn:][::-1], acts_val[:,k,:].shape)
            r = max_indices[0]
            c = max_indices[1]

            N_best_attn_per_dim = int(sqrt(N_best_attn))
            new_img = Image.new('RGB', (800*N_best_attn_per_dim, 600*N_best_attn_per_dim))

            for n in xrange(N_best_attn):
                im_mat = self.d[r[n], 0, :, :]
                img = Image.fromarray(im_mat)
                draw = ImageDraw.Draw(img)
                x_indx = c[n]%(self.W - self.w + 1)
                y_indx = c[n]/(self.W - self.w + 1)
                outline = acts_val[r[n],k,c[n]]/max_acts_val[k]
                draw.rectangle([(x_indx, y_indx),(x_indx + self.w - 1, y_indx + self.h - 1)], outline=outline)
                new_data = np.asarray(img)
                new_data = np.tile(new_data,(3,1,1))
                new_data = new_data.transpose((1,2,0))
                imshow(new_data)
                savefig(os.path.join(filter_dir, 'attn%i.png'%(n+1)))
                close()
                this_img = Image.open(os.path.join(filter_dir, 'attn%i.png'%(n+1)))
                x_loc = (n%N_best_attn_per_dim)*800
                y_loc = (n/N_best_attn_per_dim)*600
                new_img.paste(this_img,(x_loc,y_loc))

            imshow(new_img)
            savefig(os.path.join(filter_dir, 'top_attn_for_filter_%i.pdf'%(k+1)))
            close()

class EM_DRM_MNIST(EM_DRM):
    """
    Apply EM algorithm for RM to MNIST

    """
    def load_data(self):
        f = gzip.open(self.data_dir, 'rb')
        train_set, valid_set, test_set = cPickle.load(f)
        f.close()
        self.d = (train_set[0])[0:self.Ni]
        self.train_label = train_set[1][0:self.Ni]

        self.dtest = test_set[0]
        self.Ntest = np.shape(self.dtest)[0]
        self.test_label = test_set[1]

        self.dvalid = valid_set[0]
        self.Nvalid = np.shape(self.dvalid)[0]
        self.valid_label = valid_set[1]

        print 'Shape of training dataset'
        print np.shape(self.d)
        print 'Shape of training labels'
        print np.shape(self.train_label)
        print 'Shape of test dataset'
        print np.shape(self.dtest)
        print 'Shape of test labels'
        print np.shape(self.test_label)

        if not self.data_mode == 'all':
            # select digits
            train_digit_indx = nonzero((self.train_label == 1) + (self.train_label == 7) + (self.train_label == 4))
            self.d = self.d[train_digit_indx[0]]
            self.train_label = self.train_label[train_digit_indx[0]]
            print 'Size of the selected training dataset'
            print np.size(train_digit_indx)

            test_digit_indx = nonzero((self.test_label == 1) + (self.test_label == 7) + (self.test_label == 4))
            self.dtest = self.dtest[test_digit_indx[0]]
            self.test_label = self.test_label[test_digit_indx[0]]
            print 'Size of the selected test dataset'
            print np.shape(test_digit_indx)

            valid_digit_indx = nonzero((self.valid_label == 1) + (self.valid_label == 7) + (self.valid_label == 4))
            self.dvalid = self.dvalid[valid_digit_indx[0]]
            self.valid_label = self.valid_label[valid_digit_indx[0]]
            print 'Size of the selected valid dataset'
            print np.shape(valid_digit_indx)

            self.Ni = np.shape(self.d)[0] # number of input images

        self.d = np.reshape(self.d, newshape=(self.Ni, self.Cin, self.H, self.W))
        self.d = np.float32(self.d)
        self.train_label = np.int32(self.train_label)

        self.dtest = np.reshape(self.dtest, newshape=(self.Ntest, self.Cin, self.H, self.W))
        self.dtest = np.float32(self.dtest)
        self.test_label = np.int32(self.test_label)

        self.dvalid = np.reshape(self.dvalid, newshape=(self.Nvalid, self.Cin, self.H, self.W))
        self.dvalid = np.float32(self.dvalid)
        self.valid_label = np.int32(self.valid_label)

    def do_Classification(self, param_dir, batch_size, learning_rate, n_epochs, Nsample = 50000):
        """

        :param y:
        :param param_dir:
        :param Nsample:
        :return:
        """
        result_dir = os.path.join(self.output_dir, 'classification_results')
        if not os.path.exists(result_dir):
            os.makedirs(result_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        SplitFactorTrain = 50
        xtrain = T.tensor4('xtrain')
        ytrain = T.ivector('ytrain')

        mixtrain = CRM(x=xtrain, y=ytrain, dat=self.d[0:self.Ni / SplitFactorTrain], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Ni / SplitFactorTrain,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        ztrain = mixtrain._get_masked_activities(x=xtrain, dat=self.d[0:self.Ni/SplitFactorTrain])
        print 'Shape of ztrain'
        print np.shape(ztrain)

        del mixtrain, xtrain, ytrain

        for i in xrange(SplitFactorTrain - 1):
            xtrain = T.tensor4('xtrain')
            ytrain = T.ivector('ytrain')
            mixtrain = CRM(x=xtrain, y=ytrain, dat=self.d[(i + 1) * self.Ni / SplitFactorTrain:(i + 2) * self.Ni / SplitFactorTrain], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Ni / SplitFactorTrain,
                           amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                           covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                           PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                           rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

            ztrain_temp = mixtrain._get_masked_activities(x=xtrain, dat=self.d[(i+1)*self.Ni/SplitFactorTrain:(i+2)*self.Ni/SplitFactorTrain])
            ztrain = np.concatenate((ztrain, ztrain_temp), axis=0)
            print 'Shape of ztrain'
            print np.shape(ztrain)

            del mixtrain, xtrain, ytrain

        print 'Shape of Masked Train Acts'
        print np.shape(ztrain)

        np.save(os.path.join(result_dir,'latents_train.npy'), ztrain)

        #
        SplitFactorTest = 10
        xtest = T.tensor4('xtest')
        ytest = T.ivector('ytest')

        mixtest = CRM(x=xtest, y=ytest, dat=self.dtest[0:self.Ntest / SplitFactorTest], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Ntest / SplitFactorTest,
                      amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                      covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                      PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                      rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        ztest = mixtest._get_masked_activities(x=xtest, dat=self.dtest[0:self.Ntest/SplitFactorTest])
        print 'Shape of ztest'
        print np.shape(ztest)

        del xtest, mixtest, ytest

        for i in xrange(SplitFactorTest - 1):
            xtest = T.tensor4('xtest')
            ytest = T.ivector('ytest')
            mixtest = CRM(x=xtest, y=ytest, dat=self.dtest[(i + 1) * self.Ntest / SplitFactorTest:(i + 2) * self.Ntest / SplitFactorTest], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Ntest / SplitFactorTest,
                          amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                          covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                          PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                          rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

            ztest_temp = mixtest._get_masked_activities(x=xtest, dat=self.dtest[(i+1)*self.Ntest/SplitFactorTest:(i+2)*self.Ntest/SplitFactorTest])
            ztest = np.concatenate((ztest, ztest_temp), axis=0)
            print 'Shape of ztest'
            print np.shape(ztest)
            del mixtest, xtest, ytest

        print 'Shape of Masked Test Acts'
        print np.shape(ztest)
        np.save(os.path.join(result_dir,'latents_test.npy'), ztest)

        #
        SplitFactorValid = 10
        xvalid = T.tensor4('xvalid')
        yvalid = T.ivector('yvalid')

        mixvalid = CRM(x=xvalid, y=yvalid, dat=self.dvalid[0:self.Nvalid / SplitFactorValid], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Nvalid / SplitFactorValid,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        zvalid = mixvalid._get_masked_activities(x=xvalid, dat=self.dvalid[0:self.Nvalid/SplitFactorValid])
        print 'Shape of zvalid'
        print np.shape(zvalid)

        del xvalid, mixvalid, yvalid

        for i in xrange(SplitFactorValid - 1):
            xvalid = T.tensor4('xvalid')
            yvalid = T.ivector('yvalid')
            mixvalid = CRM(x=xvalid, y=yvalid, dat=self.dvalid[(i + 1) * self.Nvalid / SplitFactorValid:(i + 2) * self.Nvalid / SplitFactorValid], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Nvalid / SplitFactorValid,
                           amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                           covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                           PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                           rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

            zvalid_temp = mixvalid._get_masked_activities(x=xvalid, dat=self.dvalid[(i+1)*self.Nvalid/SplitFactorValid:(i+2)*self.Nvalid/SplitFactorValid])
            zvalid = np.concatenate((zvalid, zvalid_temp), axis=0)
            print 'Shape of zvalid'
            print np.shape(zvalid)
            del mixvalid, xvalid, yvalid

        print 'Shape of Masked Valid Acts'
        print np.shape(zvalid)
        np.save(os.path.join(result_dir,'latents_valid.npy'), zvalid)

        train_set_x = theano.shared(np.asarray(ztrain, dtype=theano.config.floatX),borrow=True)
        test_set_x = theano.shared(np.asarray(ztest, dtype=theano.config.floatX),borrow=True)
        valid_set_x = theano.shared(np.asarray(zvalid, dtype=theano.config.floatX),borrow=True)

        train_set_y = theano.shared(np.asarray(self.train_label, dtype=theano.config.floatX), borrow=True)
        test_set_y = theano.shared(np.asarray(self.test_label, dtype=theano.config.floatX), borrow=True)
        valid_set_y = theano.shared(np.asarray(self.valid_label, dtype=theano.config.floatX), borrow=True)

        train_set_y = T.cast(train_set_y, 'int32')
        test_set_y = T.cast(test_set_y, 'int32')
        valid_set_y = T.cast(valid_set_y, 'int32')

        # compute number of minibatches for training, validation and testing
        n_train_batches = train_set_x.get_value(borrow=True).shape[0]
        n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
        n_test_batches = test_set_x.get_value(borrow=True).shape[0]
        n_train_batches /= batch_size
        n_valid_batches /= batch_size
        n_test_batches /= batch_size

        # allocate symbolic variables for the data
        index = T.lscalar()  # index to a [mini]batch
        x = T.tensor4('x')   # the data is presented as rasterized images
        y = T.ivector('y')  # the labels are presented as 1D vector of
                            # [int] labels

        ######################
        # BUILD ACTUAL MODEL #
        ######################
        print '... building the model'

        softmax_input = x.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        softmax_layer = LogisticRegression(input=softmax_input, n_in=(self.K - 1)*(self.H - self.h + 1)*(self.W - self.w + 1)/4, n_out=10)

        # the cost we minimize during training is the NLL of the model
        cost = softmax_layer.negative_log_likelihood(y)

        # create a function to compute the mistakes that are made by the model
        test_model = theano.function([index], softmax_layer.errors(y),
                 givens={
                    x: test_set_x[index * batch_size: (index + 1) * batch_size],
                    y: test_set_y[index * batch_size: (index + 1) * batch_size]})

        validate_model = theano.function([index], softmax_layer.errors(y),
                givens={
                    x: valid_set_x[index * batch_size: (index + 1) * batch_size],
                    y: valid_set_y[index * batch_size: (index + 1) * batch_size]})

        # create a list of all model parameters to be fit by gradient descent
        params = softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(cost, params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.
        updates = []
        for param_i, grad_i in zip(params, grads):
            updates.append((param_i, param_i - learning_rate * grad_i))

        train_model = theano.function([index], cost, updates=updates,
              givens={
                x: train_set_x[index * batch_size: (index + 1) * batch_size],
                y: train_set_y[index * batch_size: (index + 1) * batch_size]})

        ###############
        # TRAIN MODEL #
        ###############
        print '... training'
        # early-stopping parameters
        patience = 20000  # look as this many examples regardless
        patience_increase = 2  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.995  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_train_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_params = None
        best_validation_loss = np.inf
        best_iter = 0
        test_score = 0.
        test_score_epoch = 0.
        test_score_vec = []
        epoch_vec = []
        start_time = time.clock()

        epoch = 0
        done_looping = False

        while (epoch < n_epochs) and (not done_looping):
            epoch = epoch + 1
            for minibatch_index in xrange(n_train_batches):

                iter = (epoch - 1) * n_train_batches + minibatch_index

                if iter % 100 == 0:
                    print 'training @ iter = ', iter

                cost_ij = train_model(minibatch_index)

                if (iter + 1) % 1 == 0:

                    # compute zero-one loss on validation set
                    validation_losses = [validate_model(i) for i
                                         in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_train_batches, \
                           this_validation_loss * 100.))

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                        # save best validation score and iteration number
                        best_validation_loss = this_validation_loss
                        best_iter = iter
                        best_params = params

                        # test it on the test set
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)
                        print(('     epoch %i, minibatch %i/%i, test error of best '
                               'model %f %%') %
                              (epoch, minibatch_index + 1, n_train_batches,
                               test_score * 100.))

                    if (iter + 1) % batch_size == 0:
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score_epoch = np.mean(test_losses)
                        test_score_vec.append(test_score_epoch)
                        epoch_vec.append(epoch)

                if patience <= iter:
                    done_looping = True
                    break

            fig = figure()
            plot(epoch_vec, test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch')
            fig.savefig(os.path.join(result_dir,'Error_vs_Epoch.png'))
            close(fig)


        end_time = time.clock()
        print('Optimization complete.')
        print('Best validation score of %f %% obtained at iteration %i,'\
              'with test performance %f %%' %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))
        print >> sys.stderr, ('The code for file ' +
                              os.path.split(__file__)[1] +
                              ' ran for %.2fm' % ((end_time - start_time) / 60.))

        save_file = open(os.path.join(result_dir,'cnn_mean_removed.pkl'), 'wb')  # this will overwrite current content
        cPickle.dump(best_params, save_file, -1)
        save_file.close()
        fig = figure()
        plot(epoch_vec, test_score_vec)
        legend()
        xlabel('Epoch')
        ylabel('Error Rate')
        title('Error-vs-Epoch-EM')
        fig.savefig(os.path.join(result_dir,'Error_vs_Epoch_EM.png'))
        close(fig)

class EM_SRM_CIFAR10(EM_DRM):
    """
    Apply EM algorithm for RM to CIFAR10

    """
    def unpickle(self, file):
        import cPickle
        fo = open(file, 'rb')
        dict = cPickle.load(fo)
        fo.close()
        return dict

    def load_data(self):
        train_set_1 = self.unpickle(self.data_dir + "/data_batch_1")
        train_set_2 = self.unpickle(self.data_dir + "/data_batch_2")
        train_set_3 = self.unpickle(self.data_dir + "/data_batch_3")
        train_set_4 = self.unpickle(self.data_dir + "/data_batch_4")
        train_set_5 = self.unpickle(self.data_dir + "/data_batch_5")
        test_set = self.unpickle(self.data_dir + "/test_batch")
        train_set = train_set_1
        train_set['data'] = np.concatenate((train_set['data'],train_set_2['data'],train_set_3['data'],train_set_4['data'], train_set_5['data']),axis=0)
        train_set['labels'] = np.concatenate((train_set['labels'],train_set_2['labels'],train_set_3['labels'],train_set_4['labels'], train_set_5['labels']),axis=0)
        self.train_dat = train_set['data']
        self.train_label = train_set['labels']
        self.test_dat = test_set['data']
        self.test_label = test_set['labels']

        print 'Shape of CIFAR10 training dataset'
        print np.shape(self.train_dat)
        print 'Shape of CIFAR10 training labels'
        print np.shape(self.train_label)
        print 'Shape of CIFAR10 test dataset'
        print np.shape(self.test_dat)
        print 'Shape of CIFAR10 test labels'
        print np.shape(self.test_label)

        if not self.data_mode == 'all':
            # select digits
            train_digit_indx = nonzero((self.train_label == 1) + (self.train_label == 7) + (self.train_label == 4))
            self.train_dat = self.train_dat[train_digit_indx[0]]
            self.train_label = self.train_label[train_digit_indx[0]]
            print 'Size of the selected training dataset'
            print np.size(train_digit_indx)

            test_digit_indx = nonzero((self.test_label == 1) + (self.test_label == 7) + (self.test_label == 4))
            self.test_dat = self.test_dat[test_digit_indx[0]]
            self.test_label = self.test_label[test_digit_indx[0]]
            print 'Size of the selected test dataset'
            print np.shape(test_digit_indx)
            Ni = np.shape(self.train_dat)[0] # number of input images

        self.d = np.reshape(self.train_dat[0:self.Ni], newshape=(self.Ni, self.Cin, self.H, self.W))
        self.d = np.float32(self.d)