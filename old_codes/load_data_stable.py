import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import gzip
import cPickle

import numpy as np

import random

import time

import theano

import copy
from old_codes.kmeans_init import kmeans_init

import os


# from guppy import hpy; h=hpy()

def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

class DATA(object):
    """
    Class of DATA. Should be used to load data

    """
    # TODO: use fuel to set up the datasets
    def __init__(self, dataset_name, data_mode, data_dir, Nlabeled, Ni, Cin, H, W, seed, preprocess=False):
        self.dataset_name = dataset_name
        self.data_mode = data_mode
        self.data_dir = data_dir
        self.Cin = Cin
        self.Nlabeled = Nlabeled
        self.Ni= Ni
        self.H = H
        self.W = W
        self.seed = seed
        self.preprocess = preprocess

        random.seed(self.seed)
        np.random.seed(self.seed)

        if self.dataset_name == 'MNIST':
            self.load_mnist()

        elif self.dataset_name == 'CIFAR10':
            self.load_cifar10(preprocess=self.preprocess)

        else:
            self.load_general_data()

    def load_general_data(self):
        self.dtrain = []
        self.dtest = []
        self.dvalid = []
        self.train_label = []
        self.test_label = []
        self.valid_label = []

    def load_mnist(self):
        f = gzip.open(self.data_dir, 'rb')
        train_set, valid_set, test_set = cPickle.load(f)
        f.close()
        self.dtrain = (train_set[0])[0:self.Ni]
        self.train_label = (train_set[1])[0:self.Ni]

        self.dtest = test_set[0]
        Ntest = np.shape(self.dtest)[0]
        self.test_label = test_set[1]

        self.dvalid = valid_set[0]
        Nvalid = np.shape(self.dvalid)[0]
        self.valid_label = valid_set[1]

        if not self.data_mode == 'all':
            if self.data_mode == 'semisupervised':
                print 'Creating a semisupervised dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)

                train_label_semisupervised = np.ones_like(self.train_label) * (np.max(self.train_label)+1)
                train_label_semisupervised[train_digit_indx] = self.train_label[train_digit_indx]
                self.train_label = train_label_semisupervised
            else:
                print 'Creating a small dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)
                print train_digit_indx[0:20]
                self.dtrain = self.dtrain[train_digit_indx]
                self.train_label = self.train_label[train_digit_indx]

        if self.data_mode == 'small':
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Nlabeled, self.Cin, self.H, self.W))
        else:
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Ni, self.Cin, self.H, self.W))

        self.dtrain = np.float32(self.dtrain)
        self.train_label = np.int32(self.train_label)

        self.dtest = np.reshape(self.dtest, newshape=(Ntest, self.Cin, self.H, self.W))
        self.dtest = np.float32(self.dtest)
        self.test_label = np.int32(self.test_label)

        self.dvalid = np.reshape(self.dvalid, newshape=(Nvalid, self.Cin, self.H, self.W))
        self.dvalid = np.float32(self.dvalid)
        self.valid_label = np.int32(self.valid_label)

        print 'Shape of training dataset'
        print np.shape(self.dtrain)
        print 'Shape of training labels'
        print np.shape(self.train_label)
        print 'Shape of test dataset'
        print np.shape(self.dtest)
        print 'Shape of test labels'
        print np.shape(self.test_label)
        print 'Shape of validation dataset'
        print np.shape(self.dvalid)
        print 'Shape of validation labels'
        print np.shape(self.valid_label)

    def load_cifar10(self, preprocess=False):
        if preprocess==False:
            print('No Preprocessing on CIFAR10')
            train_set_1 = unpickle(self.data_dir + "/data_batch_1")
            train_set_2 = unpickle(self.data_dir + "/data_batch_2")
            train_set_3 = unpickle(self.data_dir + "/data_batch_3")
            train_set_4 = unpickle(self.data_dir + "/data_batch_4")
            train_set_5 = unpickle(self.data_dir + "/data_batch_5")
            test_set = unpickle(self.data_dir + "/test_batch")
            train_set = train_set_1
            train_set['data'] = np.concatenate((train_set['data'],train_set_2['data'],train_set_3['data'],train_set_4['data'], train_set_5['data']),axis=0)
            train_set['labels'] = np.concatenate((train_set['labels'],train_set_2['labels'],train_set_3['labels'],train_set_4['labels'], train_set_5['labels']),axis=0)

            self.dtrain = train_set['data']
            self.train_label = train_set['labels']
            self.dtest = test_set['data']
            self.test_label = test_set['labels']
            self.dvalid = train_set_5['data']
            self.valid_label = train_set_5['labels']
        else:
            print('Preprocessing on CIFAR10')
            train_set = np.load(os.path.join(self.data_dir, "pylearn2_gcn_whitened", "train.npy"))
            test_set = np.load(os.path.join(self.data_dir, "pylearn2_gcn_whitened", "test.npy"))

            train_set_1_temp = unpickle(self.data_dir + "/data_batch_1")
            train_set_2_temp = unpickle(self.data_dir + "/data_batch_2")
            train_set_3_temp = unpickle(self.data_dir + "/data_batch_3")
            train_set_4_temp = unpickle(self.data_dir + "/data_batch_4")
            train_set_5_temp = unpickle(self.data_dir + "/data_batch_5")
            test_set_temp = unpickle(self.data_dir + "/test_batch")
            train_set_labels = np.concatenate((train_set_1_temp['labels'],train_set_2_temp['labels'],train_set_3_temp['labels'],train_set_4_temp['labels'], train_set_5_temp['labels']),axis=0)
            test_set_labels = test_set_temp['labels']

            self.dtrain = train_set
            self.train_label = train_set_labels
            self.dtest = test_set
            self.test_label = test_set_labels
            self.dvalid = train_set[40000:50000]
            self.valid_label = train_set_labels[40000:50000]

        Ntest = np.shape(self.dtest)[0]
        Nvalid = np.shape(self.dvalid)[0]

        if not self.data_mode == 'all':
            if self.data_mode == 'semisupervised':
                print 'Creating a semisupervised dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)

                train_label_semisupervised = np.ones_like(self.train_label) * (np.max(self.train_label)+1)
                train_label_semisupervised[train_digit_indx] = self.train_label[train_digit_indx]
                self.train_label = train_label_semisupervised
            else:
                print 'Creating a small dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)
                print train_digit_indx[0:20]
                self.dtrain = self.dtrain[train_digit_indx]
                self.train_label = self.train_label[train_digit_indx]

        if self.data_mode == 'small':
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Nlabeled, self.Cin, self.H, self.W))
        else:
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Ni, self.Cin, self.H, self.W))

        self.dtrain = np.float32(self.dtrain)
        self.train_label = np.int32(self.train_label)

        self.dtest = np.reshape(self.dtest, newshape=(Ntest, self.Cin, self.H, self.W))
        self.dtest = np.float32(self.dtest)
        self.test_label = np.int32(self.test_label)

        self.dvalid = np.reshape(self.dvalid, newshape=(Nvalid, self.Cin, self.H, self.W))
        self.dvalid = np.float32(self.dvalid)
        self.valid_label = np.int32(self.valid_label)

        print 'Shape of training dataset'
        print np.shape(self.dtrain)
        print 'Shape of training labels'
        if self.data_mode == 'all':
            print np.shape(self.train_label)
        else:
            print np.shape(train_digit_indx)

        print 'Shape of test dataset'
        print np.shape(self.dtest)
        print 'Shape of test labels'
        print np.shape(self.test_label)
        print 'Shape of validation dataset'
        print np.shape(self.dvalid)
        print 'Shape of validation labels'
        print np.shape(self.valid_label)

class DATA_Simulated(object):
    """
    Class of DATA. Should be used to load data

    """
    def __init__(self, Ni, D, K, M, PPCA, lock_psis, seed, mode):
        print 'Generate Simulated Data'
        self.N= Ni
        self.D = D
        self.K = K
        self.M = M
        self.seed = seed
        self.mode = mode
        
        self.PPCA = PPCA
        self.lock_psis = lock_psis

        # set random seed
        self.seed = seed
        np.random.seed(self.seed)
        
        # make data
        correct_first_mean = 10.* np.asarray(np.random.randn(1,self.D), dtype=theano.config.floatX)
        correct_first_lambda = np.asarray(np.random.randn(1, self.D, self.M), dtype=theano.config.floatX)

        # set pi and number of samples per class
        if self.mode == 'balanced':
            print 'balanced data'
            time.sleep(2)
            self.pi = np.asarray((1./float(self.K))*np.ones((self.K,1),dtype=theano.config.floatX))
        else:
            alpha = np.ones((self.K,1), dtype=np.uint8)
            alpha = hstack(alpha)
            pi = np.random.dirichlet(alpha=alpha, size=1)
            self.pi = np.asarray(pi, dtype=theano.config.floatX)

        self.pi = hstack(self.pi)

        self.NperClass = np.uint32(np.round(float(self.N)*self.pi))

        print self.NperClass

        self.N = np.sum(self.NperClass)

        # set lambdas, means, and psis
        # synthesize latents and data
        # permute data and labels
        self.correct_lambdas = correct_first_lambda
        self.correct_means = correct_first_mean
        self.correct_psis = np.zeros((self.K, self.D), dtype=theano.config.floatX)
        correct_psis_val = 32.*np.asarray(np.abs(np.random.randn(1, self.D)), dtype=theano.config.floatX)
        self.correct_psis[0] = correct_psis_val
        self.z = np.asarray(np.random.randn(self.M,self.NperClass[0]), dtype=theano.config.floatX)
        self.ztest = np.asarray(np.random.randn(self.M,self.NperClass[0]), dtype=theano.config.floatX)
        #self.zvalid = np.asarray(np.random.randn(self.M,self.NperClass[0]), dtype=theano.config.floatX)
        self.zvalid = self.ztest
        self.d = np.dot(self.correct_lambdas[0], self.z) + np.tile(self.correct_means[0], (self.NperClass[0],1)).T
        self.dtest = np.dot(self.correct_lambdas[0], self.ztest) + np.tile(self.correct_means[0], (self.NperClass[0],1)).T
        #self.dvalid = np.dot(self.correct_lambdas[0], self.zvalid) + np.tile(self.correct_means[0], (self.NperClass[0],1)).T
        self.dvalid = self.dtest
        self.correct_labels = np.zeros((self.NperClass[0],1))
        self.correct_labels_test = np.zeros((self.NperClass[0],1))
        #self.correct_labels_valid = np.zeros((self.NperClass[0],1))
        self.correct_labels_valid = self.correct_labels_test
        for i in xrange(1, self.K, 1):
            self.correct_lambdas = np.concatenate((self.correct_lambdas, np.asarray(np.random.randn(1, self.D, self.M), dtype=theano.config.floatX)))
            self.correct_means = np.concatenate((self.correct_means, 10.* np.asarray(np.random.randn(1,self.D), dtype=theano.config.floatX)))
            self.correct_psis[i] = correct_psis_val
            z = np.asarray(np.random.randn(self.M,self.NperClass[i]), dtype=theano.config.floatX)
            ztest = np.asarray(np.random.randn(self.M,self.NperClass[i]), dtype=theano.config.floatX)
            #zvalid = np.asarray(np.random.randn(self.M,self.NperClass[i]), dtype=theano.config.floatX)
            zvalid = ztest
            d = np.dot(self.correct_lambdas[i], z) + np.tile(self.correct_means[i], (self.NperClass[i],1)).T
            dtest = np.dot(self.correct_lambdas[i], ztest) + np.tile(self.correct_means[i], (self.NperClass[i],1)).T
            #dvalid = np.dot(self.correct_lambdas[i], zvalid) + np.tile(self.correct_means[i], (self.NperClass[i],1)).T
            dvalid = dtest
            self.z = np.concatenate((self.z,z), axis=1)
            self.ztest = np.concatenate((self.ztest,ztest), axis=1)
            #self.zvalid = np.concatenate((self.zvalid,zvalid), axis=1)
            self.zvalid = self.ztest
            self.d = np.concatenate((self.d,d), axis=1)
            self.dtest = np.concatenate((self.dtest,dtest), axis=1)
            #self.dvalid = np.concatenate((self.dvalid,dvalid), axis=1)
            self.dvalid = self.dtest
            self.correct_labels = np.concatenate((self.correct_labels, i*np.ones((self.NperClass[i],1))), axis=0)
            self.correct_labels_test = np.concatenate((self.correct_labels_test, i*np.ones((self.NperClass[i],1))), axis=0)
            #self.correct_labels_valid = np.concatenate((self.correct_labels_valid, i*np.ones((self.NperClass[i],1))), axis=0)
            self.correct_labels_valid = self.correct_labels_test

        self.d = self.d + np.dot(np.diag(np.sqrt(self.correct_psis[1])),np.asarray(np.random.randn(self.D,self.N),
                                                                                dtype=theano.config.floatX))
        permuted_index = np.random.permutation(self.N)
        self.d[:, [range(self.N), permuted_index]] = self.d[:, [permuted_index, range(self.N)]]
        self.dtrain = self.d.T
        self.correct_labels[[range(self.N), permuted_index],:] = self.correct_labels[[permuted_index, range(self.N)],:]
        self.train_label = np.int32(self.correct_labels[:,0])
        print 'train_label'
        print np.shape(self.train_label)
        self.dtrain = np.float32(self.dtrain)
        self.train_label = np.int32(self.train_label)


        self.dtest = self.dtest + np.dot(np.diag(np.sqrt(self.correct_psis[1])),np.asarray(np.random.randn(self.D,self.N),
                                                                                dtype=theano.config.floatX))
        permuted_index_test = np.random.permutation(self.N)
        self.dtest[:, [range(self.N), permuted_index_test]] = self.dtest[:, [permuted_index_test, range(self.N)]]
        self.dtest = self.dtest.T
        self.correct_labels_test[[range(self.N), permuted_index_test],:] = self.correct_labels_test[[permuted_index_test, range(self.N)],:]
        self.test_label = np.int32(self.correct_labels_test[:,0])
        print 'test_label'
        print np.shape(self.test_label)
        self.dtest = np.float32(self.dtest)
        self.test_label = np.int32(self.test_label)


        # self.dvalid = self.dvalid + np.dot(np.diag(np.sqrt(self.correct_psis[1])),np.asarray(np.random.randn(self.D,self.N),
        #                                                                         dtype=theano.config.floatX))
        # permuted_index_valid = np.random.permutation(self.N)
        # self.dvalid[:, [range(self.N), permuted_index_valid]] = self.dvalid[:, [permuted_index_valid, range(self.N)]]
        # self.dvalid = self.dvalid.T
        # self.correct_labels_valid[[range(self.N), permuted_index_valid],:] = self.correct_labels_valid[[permuted_index_valid, range(self.N)],:]
        # self.valid_label = np.int32(self.correct_labels_valid[:,0])
        # print 'valid_label'
        # print np.shape(self.valid_label)
        self.dvalid = copy.copy(self.dtest)
        self.valid_label = copy.copy(self.test_label)

        # compute the correct covariances
        self.correct_lambda_covs = np.zeros((self.K, self.D, self.D), dtype=theano.config.floatX)
        self.correct_covs = np.zeros((self.K, self.D, self.D), dtype=theano.config.floatX)
        for i in xrange(self.K):
            self.correct_lambda_covs[i] = np.dot(self.correct_lambdas[i],(self.correct_lambdas[i]).T)
            self.correct_covs[i] = self.correct_lambda_covs[i] + np.diag(self.correct_psis[i])

        # compute the correct inverse covariances
        self.inv_correct_covs = 0. * self.correct_covs

        for k in xrange(self.K):
            self.inv_correct_covs[k] = self._invert_cov(k)

        # compute correct log-likelihood
        self.logLs = self.cal_logLs(covs=self.correct_covs, means=self.correct_means, inv_covs=self.inv_correct_covs)

        # compute the initial means using kmeans
        init_obj = kmeans_init((self.d).T, self.K, self.N)
        init_obj.compute_kmeans()
        self.means_val_init = (init_obj.means).get_value()
        self.means_val_init = np.float32(self.means_val_init)

        # compute the initial psis
        self.psis_val_init=np.tile(np.var((self.d).T,axis=0)[None,:],(self.K,1))
        self.psis_val_init = np.float32(self.psis_val_init)

        print 'Shape of Training_Data'
        print np.shape(self.dtrain)

        print 'Shape of Test_Data'
        print np.shape(self.dtest)

    def _log_multi_gauss(self, k, covs, means, inv_covs, D):
        """
        Gaussian log likelihood of the data for component k.
        """
        sgn, logdet = np.linalg.slogdet(covs[k])
        #assert sgn > 0
        X1 = (D - means[k]).T
        X2 = np.dot(inv_covs[k], X1)
        p = -0.5 * np.sum(X1 * X2, axis=0)
        return -0.5 * np.log(2 * np.pi) * self.D - 0.5 * logdet + p

    def _log_sum(self,loglikes):
        """
        Calculate sum of log likelihoods
        """
        loglikes = np.atleast_2d(loglikes)
        a = np.max(loglikes, axis=0)
        return a + np.log(np.sum(np.exp(loglikes - a[None, :]), axis=0))

    def cal_logLs(self, covs, means, inv_covs):
        """
        Calculate log likelihoods for each datum
        under each component.
        """
        logrs = np.zeros((self.K, self.N))
        for k in range(self.K):
            logrs[k] = np.log(self.pi[k]) + self._log_multi_gauss(k=k, covs=covs, means=means, inv_covs=inv_covs, D=self.d.T)

        # here lies some ghetto log-sum-exp...
        # nothing like a little bit of overflow to make your day better!
        L = self._log_sum(logrs)
        return L
    
    def _invert_cov(self,k):
        """
        Calculate inverse covariance of mofa or ppca model,
        using inversion lemma
        """
        psiI = inv(np.diag(self.correct_psis[k]))
        lam  = self.correct_lambdas[k]
        lamT = lam.T
        step = inv(np.eye(self.M) + np.dot(lamT,np.dot(psiI,lam)))
        step = np.dot(step,np.dot(lamT,psiI))
        step = np.dot(psiI,np.dot(lam,step))
        return psiI - step
