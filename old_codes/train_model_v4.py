__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from CRM_v4 import CRM

from FRM import FRM

from old_codes.nn_functions_stable import LogisticRegression, LogisticRegressionForSemisupervised

import theano.tensor as T

import theano


# from guppy import hpy; h=hpy()

########################################################################################################################
                                                # EM Method #
########################################################################################################################

class EM_SRM_unsupervised(object):
    """
    Apply EM algorithm for RM to an arbitrary dataset

    """
    def __init__(self, d, batch_size, Cin, W, H, em_mode, use_non_lin, seed, moment_coeff):

        self.em_mode = em_mode
        self.use_non_lin = use_non_lin

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.batch_size = batch_size

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.D1 = self.h1 * self.w1 * self.Cin1 # patch size

        self.seed = seed
        np.random.seed(self.seed)

        # Just for testing
        import convmfa_for_debug
        self.d = d
        x_temp = T.tensor4('x_temp')
        y_temp = T.ivector('y_temp')

        means_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.Cin1, self.h1, self.w1)), dtype=theano.config.floatX)
        means_val_init = np.reshape(means_val_init, newshape=(self.K1 + 1, self.D1))

        psis_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.D1)), dtype=theano.config.floatX)

        mix_temp = convmfa_for_debug.ConvMofa(x=x_temp, y=y_temp, dat=self.d, K=self.K1 + 1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=np.shape(self.d)[0],
                                              amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                                              covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                                              PPCA=False, lock_psis=True, update_mode='Hinton', em_mode=self.em_mode,
                                              rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        data_val = mix_temp.createPatches()

        means_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
        for i in xrange(self.K1):
            indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
            means_val_init[i] = np.mean(data_val[indx_img], axis=0)

        del mix_temp, data_val, x_temp, y_temp, psis_val_init

        ####

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=means_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        self.cost = -(T.mean(self.conv1.logLs)*4)/self.D1

        self.conv1._M_step()

        self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, moment_coeff*layer.means + (1-moment_coeff)*layer.means_new))
            self.updates.append((layer.lambdas, moment_coeff*layer.lambdas + (1-moment_coeff)*layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4

########################################################################################################################
class EM_DRM_unsupervised(object):
    """
    Apply EM algorithm for RM to an arbitrary dataset

    """
    def __init__(self, d, batch_size, Cin, W, H, em_mode, use_non_lin, seed):

        self.em_mode = em_mode
        self.use_non_lin = use_non_lin

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.batch_size = batch_size

        self.h1 = 11
        self.w1 = 11
        self.K1 = 21
        self.M1 = 1

        self.h2 = 6
        self.w2 = 6
        self.K2 = 51
        self.M2 = 1

        self.seed = seed
        np.random.seed(self.seed)

        # Just for testing
        import convmfa_for_debug
        self.d = d
        self.D = self.h1 * self.w1 * self.Cin1 # patch size
        x_temp = T.tensor4('x_temp')
        y_temp = T.ivector('y_temp')

        means_val_init = np.asarray(np.abs(np.random.randn(self.K1, self.Cin1, self.h1, self.w1)), dtype=theano.config.floatX)
        means_val_init = np.reshape(means_val_init, newshape=(self.K1, self.D))

        psis_val_init = np.asarray(np.abs(np.random.randn(self.K1, self.D)), dtype=theano.config.floatX)

        mix_temp = convmfa_for_debug.ConvMofa(x=x_temp, y=y_temp, dat=self.d[0:1000], K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=1000,
                                              amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                                              covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                                              PPCA=False, lock_psis=True, update_mode='Hinton', em_mode=self.em_mode,
                                              rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        data_val = mix_temp.createPatches()

        means_val_init = np.zeros((self.K1, self.D), dtype=theano.config.floatX)
        for i in xrange(self.K1-1):
            indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
            means_val_init[i+1] = np.mean(data_val[indx_img], axis=0)

        ####

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y,
                         K=self.K1, M=self.M1, W=self.W1, H=self.H1,
                         w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=means_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        self.conv2 = CRM(data_4D=self.conv1.latents_rs, labels=self.y,
                         K=self.K2, M=self.M2, W=(self.W1 - self.w1 + 1) / 2, H=(self.H1 - self.h1 + 1) / 2,
                         w=self.w2, h=self.h2, Cin=self.K1 - 1, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=None,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv2._E_step()

        self.conv2._M_step()

        self.conv1._M_step()

        self.layers = [self.conv1, self.conv2]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.psis, layer.psis_new))
            self.updates.append((layer.covs, layer.covs_new))
            self.updates.append((layer.inv_covs, layer.inv_covs_new))
            self.updates.append((layer.amps, layer.amps_new))
            self.updates.append((layer.lambda_covs, layer.lambda_covs_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4
            self.output_var.append(layer.psis_new) #5
            self.output_var.append(layer.lambda_covs_new) #6

class EG_SRM_supervised(object):
    '''
    EG
    '''
    def  __init__(self, d, learning_rate, batch_size, Cin, W, H, em_mode, use_non_lin, seed, param_dir, use_mode='train'):

        self.em_mode = em_mode
        self.use_non_lin = use_non_lin
        self.use_mode = use_mode

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.batch_size = batch_size

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.D1 = self.h1 * self.w1 * self.Cin1 # patch size

        self.seed = seed
        np.random.seed(self.seed)

        if self.use_mode == 'train':
            # Initialize the means
            import convmfa_for_debug
            self.d = d
            x_temp = T.tensor4('x_temp')
            y_temp = T.ivector('y_temp')

            means_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.Cin1, self.h1, self.w1)), dtype=theano.config.floatX)
            means_val_init = np.reshape(means_val_init, newshape=(self.K1 + 1, self.D1))

            psis_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.D1)), dtype=theano.config.floatX)

            mix_temp = convmfa_for_debug.ConvMofa(x=x_temp, y=y_temp, dat=self.d, K=self.K1 + 1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=np.shape(self.d)[0],
                                                  amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                                                  covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                                                  PPCA=False, lock_psis=True, update_mode='Hinton', em_mode=self.em_mode,
                                                  rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

            data_val = mix_temp.createPatches()

            means_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
            for i in xrange(self.K1):
                indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
                means_val_init[i] = np.mean(data_val[indx_img], axis=0)

            del mix_temp, data_val, x_temp, y_temp, psis_val_init

            ####

            self.x = T.tensor4('x')
            self.y = T.ivector('y')

            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None, means_val_init=means_val_init,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        else:
            with load(param_dir) as params:
                means_val_init = params['means_val'][0]
                lambdas_val_init = params['lambdas_val'][0]
                amps_val_init = params['amps_val'][0]

            self.x = T.tensor4('x')
            self.y = T.ivector('y')

            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=amps_val_init, lambdas_val_init=lambdas_val_init, means_val_init=means_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        n_softmax = self.K1*(self.H1 - self.h1 + 1)*(self.W1 - self.w1 + 1)/4

        softmax_input = self.conv1.latents_rs.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegression(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        # self.cost = self.softmax_layer.negative_log_likelihood(self.y) - T.mean(self.conv1.logLs)
        self.cost = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv1.lambdas, self.conv1.means] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[0]
        self.conv1.means_new = self.conv1.means - learning_rate * grads[1]
        self.softmax_layer.W_new = self.softmax_layer.params[0] - learning_rate * grads[2]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - learning_rate * grads[3]

        self.layers = [self.conv1,]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4
            # Concatenate misc
            self.misc.append(layer.latents) #0
            self.misc.append(layer.rs) #1
            self.misc.append(layer.latents_rs) #2
            # Concatenate init_var
            self.init_var.append(layer.betas) #0
            self.init_var.append(layer.means) #1
            self.init_var.append(layer.lambdas) #2
            self.init_var.append(layer.amps) #3

        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

########################################################################################################################
class EG_DRM_supervised(object):
    '''
    EG
    '''
    def  __init__(self, d, learning_rate, batch_size, Cin, W, H, em_mode, use_non_lin, seed):

        self.em_mode = em_mode
        self.use_non_lin = use_non_lin

        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1

        self.h2 = 5
        self.w2 = 5
        self.K2 = 50
        self.M2 = 1

        self.seed = seed
        np.random.seed(self.seed)

        # Initialize the means
        import convmfa_for_debug
        self.d = d
        self.D = self.h1 * self.w1 * self.Cin1 # patch size
        x_temp = T.tensor4('x_temp')
        y_temp = T.ivector('y_temp')

        means_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.Cin1, self.h1, self.w1)), dtype=theano.config.floatX)
        means_val_init = np.reshape(means_val_init, newshape=(self.K1 + 1, self.D))

        psis_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.D)), dtype=theano.config.floatX)

        mix_temp = convmfa_for_debug.ConvMofa(x=x_temp, y=y_temp, dat=self.d, K=self.K1 + 1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=1000,
                                              amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                                              covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                                              PPCA=False, lock_psis=True, update_mode='Hinton', em_mode=self.em_mode,
                                              rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        data_val = mix_temp.createPatches()

        means_val_init = np.zeros((self.K1, self.D), dtype=theano.config.floatX)
        for i in xrange(self.K1):
            indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
            means_val_init[i] = np.mean(data_val[indx_img], axis=0)

        del mix_temp, data_val, x_temp, y_temp
        ####

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None, means_val_init=means_val_init,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        self.conv2 = CRM(data_4D=self.conv1.latents_rs2, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=None,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)
        self.conv2._E_step()

        n_softmax = self.K2*(self.H2 - self.h2 + 1)*(self.W2 - self.w2 + 1)/4

        softmax_input = self.conv2.latents_rs.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegression(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        self.cost = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv2.lambdas, self.conv2.means] + [self.conv1.lambdas, self.conv1.means] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv2.lambdas_new = self.conv2.lambdas - learning_rate * grads[0]
        self.conv2.means_new = self.conv2.means - learning_rate * grads[1]
        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[2]
        self.conv1.means_new = self.conv1.means - learning_rate * grads[3]
        self.softmax_layer.W_new = self.softmax_layer.params[0] - learning_rate * grads[4]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - learning_rate * grads[5]

        # update covs
        # self.conv2._update_covs()
        # self.conv1._update_covs()


        self.layers = [self.conv1, self.conv2]

        # self.layers = [self.conv1,]

        # self.updates = []
        # self.output_var = []
        #
        # for layer in self.layers:
        #     # Concatenate updates
        #     self.updates.append((layer.means, layer.means_new))
        #     self.updates.append((layer.lambdas, layer.lambdas_new))
        #     self.updates.append((layer.psis, layer.psis_new))
        #     self.updates.append((layer.covs, layer.covs_new))
        #     self.updates.append((layer.inv_covs, layer.inv_covs_new))
        #     self.updates.append((layer.amps, layer.amps_new))
        #     self.updates.append((layer.lambda_covs, layer.lambda_covs_new))
        #     # Concatenate output_var
        #     self.output_var.append(layer.logLs) #0
        #     self.output_var.append(layer.betas) #1
        #     self.output_var.append(layer.means_new) #2
        #     self.output_var.append(layer.lambdas_new) #3
        #     self.output_var.append(layer.amps_new) #4
        #     self.output_var.append(layer.psis_new) #5
        #     self.output_var.append(layer.lambda_covs_new) #6
        #
        # self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        # self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))
        #
        self.updates = []
        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4
            # Concatenate misc
            self.misc.append(layer.latents) #0
            self.misc.append(layer.rs) #1
            self.misc.append(layer.latents_rs) #2
            # Concatenate init_var
            self.init_var.append(layer.betas) #0
            self.init_var.append(layer.means) #1
            self.init_var.append(layer.lambdas) #2
            self.init_var.append(layer.amps) #3

        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

########################################################################################################################
class EG_SRM_unsupervised(object):
    '''
    EG
    '''
    def  __init__(self, d, learning_rate, batch_size, Cin, W, H, em_mode, use_non_lin, seed):

        self.em_mode = em_mode
        self.use_non_lin = use_non_lin

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.batch_size = batch_size
        self.learning_rate = learning_rate

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.D1 = self.h1 * self.w1 * self.Cin1 # patch size

        self.seed = seed
        np.random.seed(self.seed)

        # Just for testing
        import convmfa_for_debug
        self.d = d
        x_temp = T.tensor4('x_temp')
        y_temp = T.ivector('y_temp')

        means_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.Cin1, self.h1, self.w1)), dtype=theano.config.floatX)
        means_val_init = np.reshape(means_val_init, newshape=(self.K1 + 1, self.D1))

        psis_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.D1)), dtype=theano.config.floatX)

        mix_temp = convmfa_for_debug.ConvMofa(x=x_temp, y=y_temp, dat=self.d, K=self.K1 + 1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=np.shape(self.d)[0],
                                              amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                                              covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                                              PPCA=False, lock_psis=True, update_mode='Hinton', em_mode=self.em_mode,
                                              rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        data_val = mix_temp.createPatches()

        means_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
        for i in xrange(self.K1):
            indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
            means_val_init[i] = np.mean(data_val[indx_img], axis=0)

        del mix_temp, data_val, x_temp, y_temp, psis_val_init
        ####

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=means_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        # the cost we minimize during training is the NLL of the model
        self.cost = -(T.mean(self.conv1.logLs)*4)/self.D1

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv1.lambdas, self.conv1.means]

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[0]
        self.conv1.means_new = self.conv1.means - learning_rate * grads[1]

        self.layers = [self.conv1,]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4

########################################################################################################################
class EG_DRM_unsupervised(object):
    '''
    EG
    '''
    def  __init__(self, d, learning_rate, batch_size, Cin, W, H, em_mode, use_non_lin, seed):

        self.em_mode = em_mode
        self.use_non_lin = use_non_lin

        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.D1 = self.h1 * self.w1 * self.Cin1 # patch size

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1

        self.h2 = 5
        self.w2 = 5
        self.K2 = 50
        self.M2 = 1

        self.D2 = self.h2 * self.w2 * self.Cin2 # patch size

        self.seed = seed
        np.random.seed(self.seed)

        # Just for testing
        import convmfa_for_debug
        self.d = d
        x_temp = T.tensor4('x_temp')
        y_temp = T.ivector('y_temp')

        means_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.Cin1, self.h1, self.w1)), dtype=theano.config.floatX)
        means_val_init = np.reshape(means_val_init, newshape=(self.K1 + 1, self.D1))

        psis_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.D1)), dtype=theano.config.floatX)

        mix_temp = convmfa_for_debug.ConvMofa(x=x_temp, y=y_temp, dat=self.d[0:1000], K=self.K1 + 1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=1000,
                                              amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                                              covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                                              PPCA=False, lock_psis=True, update_mode='Hinton', em_mode=self.em_mode,
                                              rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        data_val = mix_temp.createPatches()

        means_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
        for i in xrange(self.K1):
            indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
            means_val_init[i] = np.mean(data_val[indx_img], axis=0)

        del mix_temp, data_val, x_temp, y_temp, psis_val_init

        ####

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=means_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        self.conv2 = CRM(data_4D=self.conv1.latents_rs, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=None,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)
        self.conv2._E_step()


        # the cost we minimize during training is the NLL of the model
        self.cost = -0.5*((T.mean(self.conv1.logLs)*4)/self.D1 + (T.mean(self.conv2.logLs)*4)/self.D2)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv2.lambdas, self.conv2.means] + [self.conv1.lambdas, self.conv1.means]

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv2.lambdas_new = self.conv2.lambdas - learning_rate * grads[0]
        self.conv2.means_new = self.conv2.means - learning_rate * grads[1]
        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[2]
        self.conv1.means_new = self.conv1.means - learning_rate * grads[3]

        self.layers = [self.conv1, self.conv2]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4

########################################################################################################################
class EG_DRM_unsupervised_TopDown(object):
    '''
    EG
    '''
    def  __init__(self, d, learning_rate, batch_size, Cin, W, H, em_mode, use_non_lin, seed):

        self.em_mode = em_mode
        self.use_non_lin = use_non_lin

        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1

        self.h2 = 3
        self.w2 = 3
        self.K2 = 50
        self.M2 = 1

        self.seed = seed
        np.random.seed(self.seed)

        # Initialize the means
        import convmfa_for_debug
        self.d = d
        self.D = self.h1 * self.w1 * self.Cin1 # patch size
        x_temp = T.tensor4('x_temp')
        y_temp = T.ivector('y_temp')

        means_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.Cin1, self.h1, self.w1)), dtype=theano.config.floatX)
        means_val_init = np.reshape(means_val_init, newshape=(self.K1 + 1, self.D))

        psis_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.D)), dtype=theano.config.floatX)

        mix_temp = convmfa_for_debug.ConvMofa(x=x_temp, y=y_temp, dat=self.d, K=self.K1 + 1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=1000,
                                              amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                                              covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                                              PPCA=False, lock_psis=True, update_mode='Hinton', em_mode=self.em_mode,
                                              rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        data_val = mix_temp.createPatches()

        means_val_init = np.zeros((self.K1, self.D), dtype=theano.config.floatX)
        for i in xrange(self.K1):
            indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
            means_val_init[i] = np.mean(data_val[indx_img], axis=0)

        del mix_temp, data_val, x_temp, y_temp, psis_val_init

        ####

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=means_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        self.conv2 = CRM(data_4D=self.conv1.latents_rs, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=None,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)
        self.conv2._E_step()
        self.conv2._Top_Down(self.conv2.latents_rs, self.conv2.rs)
        self.conv1._Top_Down(self.conv2.data_reconstructed, self.conv1.rs)

        # the cost we minimize during training is the NLL of the model
        self.cost = ((self.x - self.conv1.data_reconstructed)**2).sum()

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv2.lambdas, self.conv2.means] + [self.conv1.lambdas, self.conv1.means]

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv2.lambdas_new = self.conv2.lambdas - learning_rate * T.clip(grads[0], -1.0, 1.0)
        self.conv2.means_new = self.conv2.means - learning_rate * T.clip(grads[1], -1.0, 1.0)
        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * T.clip(grads[2], -1.0, 1.0)
        self.conv1.means_new = self.conv1.means - learning_rate * T.clip(grads[3], -1.0, 1.0)
        # self.conv1.psis_new = self.conv1.psis
        # self.conv2.psis_new = self.conv2.psis

        # # update covs
        # self.conv2._update_covs()
        # self.conv1._update_covs()


        self.layers = [self.conv1, self.conv2]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # self.updates.append((layer.psis, layer.psis_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4
            #self.output_var.append(layer.psis_new)

########################################################################################################################
class EG_SRM_semisupervised(object):
    '''
    EG
    '''
    def  __init__(self, d, learning_rate, batch_size, Cin, W, H, em_mode, use_non_lin, seed, param_dir, reg_coeff, use_mode='train'):

        self.em_mode = em_mode
        self.use_non_lin = use_non_lin
        self.use_mode = use_mode

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.batch_size = batch_size

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.D1 = self.h1 * self.w1 * self.Cin1 # patch size

        self.seed = seed
        np.random.seed(self.seed)

        if self.use_mode == 'train':
            # Initialize the means
            import convmfa_for_debug
            self.d = d
            x_temp = T.tensor4('x_temp')
            y_temp = T.ivector('y_temp')

            means_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.Cin1, self.h1, self.w1)), dtype=theano.config.floatX)
            means_val_init = np.reshape(means_val_init, newshape=(self.K1 + 1, self.D1))

            psis_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.D1)), dtype=theano.config.floatX)

            mix_temp = convmfa_for_debug.ConvMofa(x=x_temp, y=y_temp, dat=self.d, K=self.K1 + 1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=np.shape(self.d)[0],
                                                  amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                                                  covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                                                  PPCA=False, lock_psis=True, update_mode='Hinton', em_mode=self.em_mode,
                                                  rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

            data_val = mix_temp.createPatches()

            means_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
            for i in xrange(self.K1):
                indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
                means_val_init[i] = np.mean(data_val[indx_img], axis=0)

            del mix_temp, data_val, x_temp, y_temp, psis_val_init

            ####

            self.x = T.tensor4('x')
            self.y = T.ivector('y')

            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None, means_val_init=means_val_init,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        else:
            with load(param_dir) as params:
                means_val_init = params['means_val'][0]
                lambdas_val_init = params['lambdas_val'][0]
                amps_val_init = params['amps_val'][0]

            self.x = T.tensor4('x')
            self.y = T.ivector('y')

            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=amps_val_init, lambdas_val_init=lambdas_val_init, means_val_init=means_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        n_softmax = self.K1*(self.H1 - self.h1 + 1)*(self.W1 - self.w1 + 1)/4

        softmax_input = self.conv1.latents_rs.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        # self.cost = self.softmax_layer.negative_log_likelihood(self.y) - T.mean(self.conv1.logLs)
        self.cost = self.softmax_layer.negative_log_likelihood(self.y) - reg_coeff*(T.mean(self.conv1.logLs)*4)/self.D1
        self.unsupervisedNLL = -(T.mean(self.conv1.logLs)*4)/self.D1
        self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv1.lambdas, self.conv1.means] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[0]
        self.conv1.means_new = self.conv1.means - learning_rate * grads[1]
        self.softmax_layer.W_new = self.softmax_layer.params[0] - learning_rate * grads[2]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - learning_rate * grads[3]

        self.layers = [self.conv1,]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4
            # Concatenate misc
            self.misc.append(layer.latents) #0
            self.misc.append(layer.rs) #1
            self.misc.append(layer.latents_rs) #2
            # Concatenate init_var
            self.init_var.append(layer.betas) #0
            self.init_var.append(layer.means) #1
            self.init_var.append(layer.lambdas) #2
            self.init_var.append(layer.amps) #3

        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

class EG_SRM_unsupervised_with_extra_G(object):
    '''
    EG
    '''
    def  __init__(self, d, learning_rate, batch_size, Cin, W, H, em_mode, use_non_lin, seed):

        self.em_mode = em_mode
        self.use_non_lin = use_non_lin

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.batch_size = batch_size
        self.learning_rate = learning_rate

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.D1 = self.h1 * self.w1 * self.Cin1 # patch size

        self.seed = seed
        np.random.seed(self.seed)

        # Just for testing
        import convmfa_for_debug
        self.d = d
        x_temp = T.tensor4('x_temp')
        y_temp = T.ivector('y_temp')

        means_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.Cin1, self.h1, self.w1)), dtype=theano.config.floatX)
        means_val_init = np.reshape(means_val_init, newshape=(self.K1 + 1, self.D1))

        psis_val_init = np.asarray(np.abs(np.random.randn(self.K1 + 1, self.D1)), dtype=theano.config.floatX)

        mix_temp = convmfa_for_debug.ConvMofa(x=x_temp, y=y_temp, dat=self.d, K=self.K1 + 1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=np.shape(self.d)[0],
                                              amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                                              covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                                              PPCA=False, lock_psis=True, update_mode='Hinton', em_mode=self.em_mode,
                                              rs_clip = 0.0, max_condition_number=1.e3, init=True, init_ppca=False)

        data_val = mix_temp.createPatches()

        means_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
        for i in xrange(self.K1):
            indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
            means_val_init[i] = np.mean(data_val[indx_img], axis=0)

        del mix_temp, data_val, x_temp, y_temp, psis_val_init
        ####

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None, means_val_init=means_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        # the cost we minimize during training is the NLL of the model
        self.cost = -(T.mean(self.conv1.logLs)*4)/self.D1

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv1.lambdas, self.conv1.means]

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[0]
        self.conv1.means_new = self.conv1.means - learning_rate * grads[1]

        self.layers = [self.conv1,]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4
            # Concatenate extra_output_var
            self.output_var.append(layer.latents) #5
            self.output_var.append(layer.rs) #6
            self.output_var.append(self.cost) #7

    def _extra_G_step(self):
        self.latents = T.tensor3('z')
        self.rs = T.matrix('rs')

        self.conv1._compute_new_logLs(latents=self.latents, rs=self.rs)
        # the cost we minimize during training is the NLL of the model
        self.cost_new = -(T.mean(self.conv1.logLs_new)*4)/self.D1

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost_new, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv1.lambdas_new = self.conv1.lambdas - 2*self.learning_rate * grads[0]
        self.conv1.means_new = self.conv1.means - 2*self.learning_rate * grads[1]

        # self.layers = [self.conv1,]

        self.updates_new = []
        self.output_var_new = []

        for layer in self.layers:
            # Concatenate updates
            self.updates_new.append((layer.means, layer.means_new))
            self.updates_new.append((layer.lambdas, layer.lambdas_new))
            # Concatenate output_var
            self.output_var_new.append(self.cost_new) #0
            self.output_var_new.append(layer.betas) #1
            self.output_var_new.append(layer.means_new) #2
            self.output_var_new.append(layer.lambdas_new) #3
            self.output_var_new.append(layer.amps_new) #4


#######################################################################################################################
# For FRM #
#######################################################################################################################
class EM_FRM_unsupervised(object):
    """
    Apply EM algorithm for RM to an arbitrary dataset.

    """
    def __init__(self, data, batch_size, D, seed):

        self.D1 = D

        self.batch_size = batch_size

        self.K1 = 10
        self.M1 = 1

        self.seed = seed
        np.random.seed(self.seed)

        # data_init = np.reshape(data, newshape=(dimdata[0],np.prod(dimdata[1:])))
        # means_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
        # psis_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
        # for i in xrange(self.K1):
        #     indx_img = np.random.randint(0,np.shape(data_init)[0],size=100)
        #     means_val_init[i] = np.mean(data_init[indx_img], axis=0)
        #     psis_val_init[i] = np.var(data_init[indx_img], axis=0)
        #
        # amps_val_init = np.ones((self.K1,))
        # amps_val_init /= np.sum(amps_val_init)

        ####
        self.x = T.fmatrix('x')
        self.y = T.ivector('y')

        self.conv1 = FRM(data=self.x, labels=self.y, K=self.K1, D=self.D1, M=self.M1, Ni=self.batch_size,
                         lambdas_val_init=None, means_val_init=data.means_val_init, amps_val_init=None, psis_val_init=data.psis_val_init,
                         PPCA=False, lock_psis=True, rs_clip = 0.0, max_condition_number=1.e3)

        self.conv1._E_step()

        self.cost = -T.mean(self.conv1.logLs)/self.D1

        self.conv1._M_step()

        self.layers = [self.conv1]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.psis, layer.psis_new))
            self.updates.append((layer.amps, layer.amps_new))

            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4
            self.output_var.append(layer.psis_new) #5

class EG_FRM_unsupervised(object):
    """
    Apply EM algorithm for RM to an arbitrary dataset

    """
    def __init__(self, data, learning_rate, batch_size, D, seed):

        self.D1 = D

        self.batch_size = batch_size
        self.learning_rate = learning_rate

        self.K1 = 10
        self.M1 = 1

        self.seed = seed
        np.random.seed(self.seed)

        # data_init = np.reshape(data, newshape=(dimdata[0],np.prod(dimdata[1:])))
        # means_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
        # psis_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
        # for i in xrange(self.K1):
        #     indx_img = np.random.randint(0,np.shape(data_init)[0],size=100)
        #     means_val_init[i] = np.mean(data_init[indx_img], axis=0)
        #     psis_val_init[i] = np.var(data_init[indx_img], axis=0)
        #
        # amps_val_init = np.ones((self.K1,))
        # amps_val_init /= np.sum(amps_val_init)

        ####

        self.x = T.fmatrix('x')
        self.y = T.ivector('y')

        self.conv1 = FRM(data=self.x, labels=self.y, K=self.K1, D=self.D1, M=self.M1, Ni=self.batch_size,
                         lambdas_val_init=None, means_val_init=data.means_val_init, amps_val_init=None, psis_val_init=data.psis_val_init,
                         PPCA=False, lock_psis=True, rs_clip = 0.0, max_condition_number=1.e3)

        self.conv1._E_step()

        self.cost = -T.mean(self.conv1.logLs)/self.D1

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv1.lambdas, self.conv1.means, self.conv1.psis]

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[0]
        self.conv1.means_new = self.conv1.means - learning_rate * grads[1]
        self.conv1.psis_new = self.conv1.psis - learning_rate * grads[2]

        self.layers = [self.conv1,]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.means, layer.means_new))
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.psis, layer.psis_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.means_new) #2
            self.output_var.append(layer.lambdas_new) #3
            self.output_var.append(layer.amps_new) #4
            self.output_var.append(layer.psis_new) #5

# class EG_FRM_unsupervised_knowing_correct_params(object):
#     """
#     Apply EM algorithm for RM to an arbitrary dataset
#
#     """
#     def __init__(self, data, learning_rate, batch_size, D, em_mode, use_non_lin, seed):
#
#         self.em_mode = em_mode
#         self.use_non_lin = use_non_lin
#
#         self.D1 = D
#
#         self.batch_size = batch_size
#         self.learning_rate = learning_rate
#
#         self.K1 = 20
#         self.M1 = 1
#
#         self.seed = seed
#         np.random.seed(self.seed)
#
#         ####
#
#         dimdata = np.shape(data)
#         print 'dimdata'
#         print dimdata
#         data_init = np.reshape(data, newshape=(dimdata[0],np.prod(dimdata[1:])))
#         means_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
#         psis_val_init = np.zeros((self.K1, self.D1), dtype=theano.config.floatX)
#         for i in xrange(self.K1):
#             indx_img = np.random.randint(0,np.shape(data_init)[0],size=100)
#             means_val_init[i] = np.mean(data_init[indx_img], axis=0)
#             psis_val_init[i] = np.var(data_init[indx_img], axis=0)
#
#         amps_val_init = np.ones((self.K1,))
#         amps_val_init /= np.sum(amps_val_init)
#
#         ####
#
#         if len(dimdata) == 4:
#             self.x = T.tensor4('x')
#         else:
#             self.x = T.matrix('x')
#         self.y = T.ivector('y')
#
#         self.conv1 = FRM(data=self.x, labels=self.y, K=self.K1, D=self.D1, M=self.M1, Ni=self.batch_size,
#                  lambdas_val_init=None, means_val_init=None, amps_val_init=None, psis_val_init=None,
#                  PPCA=False, lock_psis=True,
#                  em_mode='hard', use_non_lin='None',
#                  rs_clip = 0.0,
#                  max_condition_number=1.e3,
#                  init_ppca=False)
#
#         self.conv1._E_step()
#
#         self.cost = -T.mean(self.conv1.logLs)/self.D1
#
#         # create a list of all model parameters to be fit by gradient descent
#         self.params = [self.conv1.lambdas, self.conv1.means, self.conv1.psis]
#
#         # create a list of gradients for all model parameters
#         grads = T.grad(self.cost, self.params)
#
#         # train_model is a function that updates the model parameters by
#         # SGD Since this model has many parameters, it would be tedious to
#         # manually create an update rule for each model parameter. We thus
#         # create the updates list by automatically looping over all
#         # (params[i],grads[i]) pairs.
#
#         self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[0]
#         self.conv1.means_new = self.conv1.means - learning_rate * grads[1]
#         self.conv1.psis_new = self.conv1.psis - learning_rate * grads[2]
#
#         self.layers = [self.conv1,]
#
#         # self.layers = [self.conv1,]
#
#         self.updates = []
#         self.output_var = []
#
#         for layer in self.layers:
#             # Concatenate updates
#             self.updates.append((layer.means, layer.means_new))
#             self.updates.append((layer.lambdas, layer.lambdas_new))
#             self.updates.append((layer.psis, layer.psis_new))
#             self.updates.append((layer.amps, layer.amps_new))
#             # Concatenate output_var
#             self.output_var.append(layer.logLs) #0
#             self.output_var.append(layer.betas) #1
#             self.output_var.append(layer.means_new) #2
#             self.output_var.append(layer.lambdas_new) #3
#             self.output_var.append(layer.amps_new) #4
#             self.output_var.append(layer.psis_new) #5
#             self.output_var.append(layer.lambda_covs_new) #6
