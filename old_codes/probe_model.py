__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from CRM_v4 import CRM

from old_codes.nn_functions_stable import LogisticRegression

import theano.tensor as T


# from guppy import hpy; h=hpy()

class train_Softmax_EM_SRM(object):
    """
    Apply EM algorithm for RM to an arbitrary dataset

    """
    def __init__(self, learning_rate, batch_size, Cin, W, H, em_mode, use_non_lin, seed, param_dir):

        with load(param_dir) as params:
            means_val_init = params['means_val'][0]
            lambdas_val_init = params['lambdas_val'][0]
            psis_val_init = params['psis_val'][0]
            amps_val_init = params['amps_val'][0]


        self.em_mode = em_mode
        self.use_non_lin = use_non_lin
        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=amps_val_init, lambdas_val_init=lambdas_val_init, psis_val_init=psis_val_init, means_val_init=means_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        n_softmax = self.K1*(self.H1 - self.h1 + 1)*(self.W1 - self.w1 + 1)/4

        softmax_input = self.conv1.latents_rs.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegression(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        self.cost = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.softmax_layer.W_new = self.softmax_layer.params[0] - learning_rate * grads[0]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - learning_rate * grads[1]

        # self.layers = [self.conv1,]

        self.updates = []

        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

        self.output_var = []
        self.output_var.append(self.softmax_layer.W_new) #0
        self.output_var.append(self.softmax_layer.b_new) #1

class train_Softmax_EM_DRM(object):
    """
    Apply EM algorithm for RM to an arbitrary dataset

    """
    def __init__(self, learning_rate, batch_size, Cin, W, H, em_mode, use_non_lin, seed, param_dir):

        with load(param_dir) as params:
            means_val_init = params['means_val']
            lambdas_val_init = params['lambdas_val']
            psis_val_init = params['psis_val']
            amps_val_init = params['amps_val']


        self.em_mode = em_mode
        self.use_non_lin = use_non_lin
        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.h2 = 3
        self.w2 = 3
        self.K2 = 50
        self.M2 = 1

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1


        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0], psis_val_init=psis_val_init[0], means_val_init=means_val_init[0],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step()

        self.conv2 = CRM(data_4D=self.conv1.latents_rs, labels=self.y, K=self.K2, M=self.M2, W=(self.W1 - self.w1 + 1) / 2, H=(self.H1 - self.h1 + 1) / 2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1], psis_val_init=psis_val_init[1], means_val_init=means_val_init[1],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, use_non_lin=self.use_non_lin,
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)
        self.conv2._E_step()

        n_softmax = self.K2*(self.H2 - self.h2 + 1)*(self.W2 - self.w2 + 1)/4

        softmax_input = self.conv2.latents_rs.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegression(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        self.cost = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.softmax_layer.W_new = self.softmax_layer.params[0] - learning_rate * grads[0]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - learning_rate * grads[1]

        # self.layers = [self.conv1,]

        self.updates = []

        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

        self.output_var = []
        self.output_var.append(self.softmax_layer.W_new) #0
        self.output_var.append(self.softmax_layer.b_new) #1