__author__ = 'minhtannguyen'

import numpy as np

import matplotlib.pyplot as pl
from matplotlib.patches import Ellipse

from scipy.linalg import inv

from six.moves import xrange
import theano
from theano import Op, Apply
import theano.tensor as T
from theano.gradient import grad_not_implemented
from theano.gradient import grad_undefined

import numpy



def plot_2d_ellipses(d1,d2, means, covs, K, **kwargs):
    """
    Make a 2D plot of the model projected onto axes
    d1 and d2.
    """
    for k in range(K):
        mean = means[k,(d1, d2)]
        cov = covs[k][((d1, d2),(d1, d2)), ((d1, d1), (d2, d2))]
        _plot_2d_ellipse(mean, cov, **kwargs)

def plot_2d_ellipses_psis(d1,d2, means, psis, K, **kwargs):
    """
    Make a 2D plot of the model projected onto axes
    d1 and d2.
    """
    for k in range(K):
        mean = means[k,(d1, d2)]
        psi = np.diag(psis[k])[((d1, d2),(d1, d2)), ((d1, d1), (d2, d2))]
        _plot_2d_ellipse(mean, psi, **kwargs)

def _plot_2d_ellipse(mu, cov, ax=None, **kwargs):
    """
    Plot the error ellipse at a point given it's covariance matrix.
    """
    # some sane defaults
    facecolor = kwargs.pop('facecolor', 'none')
    edgecolor = kwargs.pop('edgecolor', 'k')

    x, y = mu
    U, S, V = np.linalg.svd(cov)
    theta = np.degrees(np.arctan2(U[1, 0], U[0, 0]))
    ellipsePlot = Ellipse(xy=[x, y],
                          width=2 * np.sqrt(S[0]),
                          height=2 * np.sqrt(S[1]),
                          angle=theta,
            facecolor=facecolor, edgecolor=edgecolor, **kwargs)

    if ax is None:
        ax = pl.gca()
    ax.add_patch(ellipsePlot)

#######################################################################
    # set of methods used to compute the log-likehood of the correct model
    #
    # copy the comments from the mofa.py by Ross Fadely, so you will find
    # some funny comments
#######################################################################
def _invert_cov(k, M, psis, lambdas):
    """
    Calculate inverse covariance of mofa or ppca model,
    using inversion lemma
    """
    psiI = inv(np.diag(psis[k]))
    lam  = lambdas[k]
    lamT = lam.T
    step = inv(np.eye(M) + np.dot(lamT,np.dot(psiI,lam)))
    step = np.dot(step,np.dot(lamT,psiI))
    step = np.dot(psiI,np.dot(lam,step))
    return psiI - step

def _log_multi_gauss(k, d, covs, means, inv_covs, data):
    """
    Gaussian log likelihood of the data for component k.
    """
    sgn, logdet = np.linalg.slogdet(covs[k])
    #assert sgn > 0
    X1 = (data - means[k]).T
    X2 = np.dot(inv_covs[k], X1)
    p = -0.5 * np.sum(X1 * X2, axis=0)
    return -0.5 * np.log(2 * np.pi) * d - 0.5 * logdet + p

def _log_sum(loglikes):
    """
    Calculate sum of log likelihoods
    """
    loglikes = np.atleast_2d(loglikes)
    a = np.max(loglikes, axis=0)
    return a + np.log(np.sum(np.exp(loglikes - a[None, :]), axis=0))

def cal_logLs(K, N, D, data, covs, means, inv_covs, pi):
    """
    Calculate log likelihoods for each datum
    under each component.
    """
    logrs = np.zeros((K, N))
    for k in range(K):
        logrs[k] = np.log(pi[k]) + _log_multi_gauss(k=k, d=D ,covs=covs, means=means, inv_covs=inv_covs, data=data.T)

    # here lies some ghetto log-sum-exp...
    # nothing like a little bit of overflow to make your day better!
    L = _log_sum(logrs)
    return L

def computeVar(x, batch_size):
    n = x.shape[0]
    Nb = n/batch_size
    A = x[0:2*batch_size]
    meanA = np.mean(A, axis=0)
    nA = 2*batch_size
    MA = np.var(A, axis=0) * nA
    for i in range(2,Nb):
        B = x[i*batch_size:(i+1)*batch_size]
        meanB = np.mean(B, axis=0)
        MB = np.var(B, axis=0) * batch_size
        delta = meanB - meanA
        meanA = meanA + delta*batch_size/(nA+batch_size)
        MA = MA + MB + np.square(delta)*nA*batch_size/(nA+batch_size)
        nA = nA + batch_size

    return MA/n

class Images2Neibs_3D(Op):
    def __init__(self, mode='valid'):
        """
        :type mode: str
        :param mode: Possible values:
            'valid': Requires an input that is a multiple of the
                pooling factor (in each direction)
            'ignore_borders': Same as valid, but will ignore the borders
                if the shape(s) of the input
                is not a multiple of the pooling factor(s)
            'wrap_centered' : ?? TODO comment
        :return:
            Reshapes the input as a 2D tensor where each row is an
            pooling example
        """
        if mode not in ['valid', 'wrap_centered', 'ignore_borders']:
            raise NotImplementedError("Only the mode valid, ignore_borders"
                                      " and wrap_centered have been"
                                      " implemented for the op Images2Neibs")
        self.mode = mode

    def __eq__(self, other):
        return type(self) == type(other) and self.mode == other.mode

    def __hash__(self):
        return hash(type(self)) ^ hash(self.mode)

    def __str__(self):
        return self.__class__.__name__ + "{%s}" % self.mode

    def __setstate__(self, d):
        self.__dict__.update(d)
        if not hasattr(self, "mode"):
            self.mode = 'valid'

    def make_node(self, ten4, neib_shape, neib_step=None):
        """
        :param ten4:     a list of lists of images
                         ten4 is of shape (list 1 dim, list 2 dim,
                                           row, col)
        :param neib_shape: (r,c) where r is the height of the neighborhood
                        in rows and c is the width of the neighborhood
                        in columns
        :param neib_step: (dr,dc) where dr is the number of rows to
                          skip between patch and dc is the number of
                          columns. When None, this is the same as
                          neib_shape(patch are disjoint)
        output:
            a 2D matrix, written using the following pattern
            idx = 0
            for i in xrange(list 1 dim)
                for j in xrange(list 2 dim)
                    for k in <image column coordinates>
                        for l in <image row coordinates>
                            output[idx,:]
                                 = flattened version of ten4[i,j,l:l+r,k:k+c]
                            idx += 1
            (note: the op isn't necessarily implemented internally with these
            for loops, they're just the easiest way to describe the output
            pattern)
        """
        ten4 = T.as_tensor_variable(ten4)
        neib_shape = T.as_tensor_variable(neib_shape)
        if neib_step is None:
            neib_step = neib_shape
        else:
            neib_step = T.as_tensor_variable(neib_step)

        assert ten4.ndim == 4
        assert neib_shape.ndim == 1
        assert neib_step.ndim == 1

        return Apply(self, [ten4, neib_shape, neib_step],
                     [T.matrix(dtype=ten4.type.dtype)])

    def c_code_cache_version(self):
        return (5,)

    def perform(self, node, inp, out_):
        ten4, neib_shape, neib_step = inp
        z, = out_
        # GpuImages2Neibs should not run this perform in DebugMode
        if type(self) != Images2Neibs_3D:
            raise theano.gof.utils.MethodNotDefined()

        def CEIL_INTDIV(a, b):
            if a % b:
                return (a // b) + 1
            else:
                return a // b

        grid_c = -1  # number of patch in height
        grid_d = -1  # number of patch in width
        assert ten4.ndim == 4
        assert neib_shape.ndim == 1
        assert neib_shape.shape[0] == 2
        assert neib_step.ndim == 1
        assert neib_step.shape[0] == 2
        c, d = neib_shape
        step_x, step_y = neib_step
        mode = self.mode

        if mode == "wrap_centered":
            if (c % 2 != 1) or (d % 2 != 1):
                raise TypeError(
                    "Images2Neibs:"
                    " in mode wrap_centered need patch with odd shapes")

            if (ten4.shape[2] < c) or (ten4.shape[3] < d):
                raise TypeError(
                    "Images2Neibs: in wrap_centered mode, don't support"
                    " image shapes smaller then the patch shapes:"
                    " neib_shape=(%d,%d), ten4[2:]=[%d,%d]" %
                    (c, d, ten4.shape[2], ten4.shape[3]))
            grid_c = CEIL_INTDIV(ten4.shape[2], step_x)
            grid_d = CEIL_INTDIV(ten4.shape[3], step_y)

        elif mode == "valid":
            if (ten4.shape[2] < c) or (((ten4.shape[2] - c) % step_x) != 0):
                raise TypeError(
                    "neib_shape[0]=%d, neib_step[0]=%d and"
                    " ten4.shape[2]=%d not consistent" %
                    (c, step_x, ten4.shape[2]))
            if (ten4.shape[3] < d) or (((ten4.shape[3] - d) % step_y) != 0):
                raise TypeError(
                    "neib_shape[1]=%d, neib_step[1]=%d and"
                    " ten4.shape[3]=%d not consistent" %
                    (d, step_y, ten4.shape[3]))
            # number of patch in height
            grid_c = 1 + ((ten4.shape[2] - c) // step_x)
            # number of patch in width
            grid_d = 1 + ((ten4.shape[3] - d) // step_y)
        elif mode == "ignore_borders":
            # number of patch in height
            grid_c = 1 + ((ten4.shape[2] - c) // step_x)
            # number of patch in width
            grid_d = 1 + ((ten4.shape[3] - d) // step_y)
        else:
            raise TypeError("Images2Neibs: unknow mode '%s'" % mode)

        z_dim0 = grid_c * grid_d * ten4.shape[0]
        z_dim1 = c * d * ten4.shape[1]
        z[0] = numpy.empty((z_dim0, z_dim1), dtype=node.outputs[0].dtype)

        nb_batch = ten4.shape[0]
        nb_stack = ten4.shape[1]
        height = ten4.shape[2]
        width = ten4.shape[3]

        wrap_centered_idx_shift_x = c // 2
        wrap_centered_idx_shift_y = d // 2
        for n in range(nb_batch):
            # loop over the number of patch in height
            for a in range(grid_c):
                # loop over the number of patch in width
                for b in range(grid_d):
                    z_row = b + grid_d * (a + grid_c * n)
                    for s in range(nb_stack):
                        for i in range(c):
                            ten4_2 = i + a * step_x
                            if mode == "wrap_centered":
                                ten4_2 -= wrap_centered_idx_shift_x
                                if ten4_2 < 0:
                                    ten4_2 += height
                                elif ten4_2 >= height:
                                    ten4_2 -= height
                            for j in range(d):
                                ten4_3 = j + b * step_y
                                if mode == "wrap_centered":
                                    ten4_3 -= wrap_centered_idx_shift_y
                                    if ten4_3 < 0:
                                        ten4_3 += width
                                    elif ten4_3 >= width:
                                        ten4_3 -= width
                                z_col = j + d * (i + c * s)

                                z[0][z_row, z_col] = ten4[n, s, ten4_2, ten4_3]

    def infer_shape(self, node, input_shape):
        in_shape = input_shape[0]
        c, d = node.inputs[1]
        step_x, step_y = node.inputs[2]
        if self.mode == 'wrap_centered':
            grid_c = T.ceil_intdiv(in_shape[2], step_x)
            grid_d = T.ceil_intdiv(in_shape[3], step_y)
        elif self.mode == 'valid':
            grid_c = 1 + ((in_shape[2] - c) // step_x)
            grid_d = 1 + ((in_shape[3] - d) // step_y)
        elif self.mode == 'ignore_borders':
            grid_c = 1 + ((in_shape[2] - c) // step_x)
            grid_d = 1 + ((in_shape[3] - d) // step_y)
        z_dim0 = grid_c * grid_d * in_shape[0]
        z_dim1 = c * d * in_shape[1]
        return [(z_dim0, z_dim1)]

    def c_code(self, node, name, inp, out, sub):
        ten4, neib_shape, neib_step = inp
        z, = out

        fail = sub['fail']
        mode = self.mode
        return """
#ifndef CEIL_INTDIV
#define CEIL_INTDIV(a, b) ((a/b) + ((a %% b) ? 1: 0))
#endif
        int grid_c = -1; //number of patch in height
        int grid_d = -1; //number of patch in width
        {
        if (PyArray_NDIM(%(ten4)s) != 4)
        {
            PyErr_Format(PyExc_TypeError, "ten4 wrong rank");
            %(fail)s;
        }
        if (PyArray_NDIM(%(neib_shape)s) != 1)
        {
            PyErr_Format(PyExc_TypeError, "neib_shape wrong rank");
            %(fail)s;
        }
        if ( (PyArray_DIMS(%(neib_shape)s))[0] != 2)
        {
            PyErr_Format(PyExc_TypeError, "neib_shape wrong shape ; has to"
                                          " contain 2 elements");
            %(fail)s;
        }
        if (PyArray_NDIM(%(neib_step)s) != 1)
        {
            PyErr_Format(PyExc_TypeError, "neib_step wrong rank");
            %(fail)s;
        }
        if ( (PyArray_DIMS(%(neib_step)s))[0] != 2)
        {
            PyErr_Format(PyExc_TypeError,
                         "neib_step wrong step ; has to contain 2 elements");
            %(fail)s;
        }
        // (c,d) = neib_shape
        const npy_intp c = (npy_intp) *(dtype_%(neib_shape)s*) PyArray_GETPTR1(%(neib_shape)s, 0);
        const npy_intp d = (npy_intp) *(dtype_%(neib_shape)s*) PyArray_GETPTR1(%(neib_shape)s, 1);
        // (step_x,step_y) = neib_step
        const npy_intp step_x = (npy_intp) *(dtype_%(neib_step)s*) PyArray_GETPTR1(%(neib_step)s, 0);
        const npy_intp step_y = (npy_intp) *(dtype_%(neib_step)s*) PyArray_GETPTR1(%(neib_step)s, 1);
        if ( "%(mode)s" == "wrap_centered") {
            if (c%%2!=1 || d%%2!=1){
                PyErr_Format(PyExc_TypeError,
                             "Images2Neibs: in mode wrap_centered"
                             " need patch with odd shapes");
                %(fail)s;
            }
            if ( (PyArray_DIMS(%(ten4)s))[2] < c ||
                 (PyArray_DIMS(%(ten4)s))[3] < d)
            {
                PyErr_Format(PyExc_TypeError,
                    "Images2Neibs: in wrap_centered mode, don't support image"
                    " shapes smaller then the patch shapes:"
                    " neib_shape=(%%ld,%%ld), ten4[2:]=[%%ld,%%ld]",
                    (long int)c, (long int)d,
                    (long int)(PyArray_DIMS(%(ten4)s)[2]),
                    (long int)(PyArray_DIMS(%(ten4)s)[3]));
                %(fail)s;
            }
            grid_c = CEIL_INTDIV(((PyArray_DIMS(%(ten4)s))[2]),step_x);
            grid_d = CEIL_INTDIV(((PyArray_DIMS(%(ten4)s))[3]),step_y);
        }else if ( "%(mode)s" == "valid") {
            if ( ((PyArray_DIMS(%(ten4)s))[2] < c) ||
                 ( (((PyArray_DIMS(%(ten4)s))[2]-c) %% step_x)!=0))
            {
                PyErr_Format(PyExc_TypeError,
                             "neib_shape[0]=%%ld, neib_step[0]=%%ld and"
                             " ten4.shape[2]=%%ld not consistent",
                             (long int)c, (long int)step_x,
                             (long int)(PyArray_DIMS(%(ten4)s)[2]));
                %(fail)s;
            }
            if ( ((PyArray_DIMS(%(ten4)s))[3] < d) ||
                 ( (((PyArray_DIMS(%(ten4)s))[3]-d) %% step_y)!=0))
            {
                PyErr_Format(PyExc_TypeError,
                             "neib_shape[1]=%%ld, neib_step[1]=%%ld and"
                             " ten4.shape[3]=%%ld not consistent",
                             (long int)d, (long int)step_y,
                             (long int)(PyArray_DIMS(%(ten4)s)[3]));
                %(fail)s;
            }
            //number of patch in height
            grid_c = 1+(((PyArray_DIMS(%(ten4)s))[2]-c)/step_x);
            //number of patch in width
            grid_d = 1+(((PyArray_DIMS(%(ten4)s))[3]-d)/step_y);
        }else if ( "%(mode)s" == "ignore_borders") {
            //number of patch in height
            grid_c = 1+(((PyArray_DIMS(%(ten4)s))[2]-c)/step_x);
            //number of patch in width
            grid_d = 1+(((PyArray_DIMS(%(ten4)s))[3]-d)/step_y);
        }else{
            PyErr_Format(PyExc_TypeError,
                         "Images2Neibs: unknow mode '%(mode)s'");
            %(fail)s;
        }
        // new dimensions for z
        const npy_intp z_dim1 = c * d * (PyArray_DIMS(%(ten4)s))[1];
        const npy_intp z_dim0 =  grid_c
                            * grid_d
                            * (PyArray_DIMS(%(ten4)s))[0];
        if ((NULL == %(z)s)
            || ((PyArray_DIMS(%(z)s))[0] != z_dim0 )
            || ((PyArray_DIMS(%(z)s))[1] != z_dim1 )
        )
        {
            Py_XDECREF(%(z)s);
            npy_intp dims[2];
            dims[0] = z_dim0;
            dims[1] = z_dim1;
            %(z)s = (PyArrayObject*) PyArray_EMPTY(2,
                dims,
                PyArray_TYPE((PyArrayObject*) py_%(ten4)s),
                0);
            if (!%(z)s)
            {
                PyErr_SetString(PyExc_MemoryError, "failed to alloc z output");
                %(fail)s;
            }
        }
        }
        { // NESTED SCOPE
        const int nb_batch = (PyArray_DIMS(%(ten4)s))[0];
        const int nb_stack = (PyArray_DIMS(%(ten4)s))[1];
        const int height = (PyArray_DIMS(%(ten4)s))[2];
        const int width = (PyArray_DIMS(%(ten4)s))[3];
        // (c,d) = neib_shape
        const npy_intp c = (npy_intp) *(dtype_%(neib_shape)s*) PyArray_GETPTR1(%(neib_shape)s, 0);
        const npy_intp d = (npy_intp) *(dtype_%(neib_shape)s*) PyArray_GETPTR1(%(neib_shape)s, 1);
        // (step_x,step_y) = neib_step
        const npy_intp step_x = (npy_intp) *(dtype_%(neib_step)s*) PyArray_GETPTR1(%(neib_step)s, 0);
        const npy_intp step_y = (npy_intp) *(dtype_%(neib_step)s*) PyArray_GETPTR1(%(neib_step)s, 1);
        const int wrap_centered_idx_shift_x = c/2;
        const int wrap_centered_idx_shift_y = d/2;
        // Oh this is messed up...
        for (int n = 0; n < nb_batch; n++)              // loop over batches
            for (int a = 0; a < grid_c; a++)        // loop over the number of patch in height
                for (int b = 0; b < grid_d; b++)    // loop over the number of patch in width
                {
                    int z_row = b + grid_d * (a + grid_c * n);
                    for (int s = 0; s < nb_stack; s++)          // loop over stacks
                        for (int i = 0; i < c; i++)     // loop over c
                        {
                            int ten4_2 = i + a * step_x;
                            if ( "%(mode)s" == "wrap_centered" ){
                                ten4_2 -= wrap_centered_idx_shift_x;
                                if ( ten4_2 < 0 ) ten4_2 += height;
                                else if (ten4_2 >= height) ten4_2 -= height;
                            }
                            for (int j = 0; j < d; j++)  // loop over d
                            {
                                int ten4_3 = j + b * step_y;
                                if ( "%(mode)s" == "wrap_centered" ){
                                    ten4_3 -= wrap_centered_idx_shift_y;
                                    if ( ten4_3 < 0 ) ten4_3 += width;
                                    else if (ten4_3 >= width) ten4_3 -= width;
                                }
                                int z_col = j + d * (i + c * s);
                                dtype_%(z)s* curr_z = (dtype_%(z)s*) PyArray_GETPTR2(%(z)s, z_row, z_col);
                                *curr_z = *( (dtype_%(ten4)s*) PyArray_GETPTR4(%(ten4)s, n, s, ten4_2, ten4_3));
                                //printf("\\n(%%i,%%i,%%i,%%i) --> (%%i,%%i)",
                                //       n, s, ten4_2, ten4_3, z_row, z_col);
                                //printf("%%f ", *curr_z);
                            }
                        }
                }
        } // END NESTED SCOPE
        """ % locals()


def images2neibs_3D(ten4, neib_shape, neib_step=None, mode='valid'):
    """
    Function :func:`images2neibs <theano.sandbox.neighbours.images2neibs>`
    allows to apply a sliding window operation to a tensor containing
    images
    or other two-dimensional objects.
    The sliding window operation loops
    over points in input data and stores a rectangular neighbourhood of
    each point.
    It is possible to assign a step of selecting patches (parameter
    `neib_step`).
    :param ten4:     A 4-dimensional tensor which represents
                     a list of lists of images.a list of lists of images.
                     It should have shape (list 1 dim, list 2 dim,
                     row, col). The first two dimensions can be
                     useful to store different channels and batches.
    :type ten4:      A 4d tensor-like.
    :param neib_shape: A tuple containing two
                    values: height and width of the neighbourhood.
                    It should have shape (r,c) where r is the height of the
                    neighborhood in rows and c is the width of the neighborhood
                    in columns
    :type neib_shape: A 1d tensor-like of 2 values.
    :param neib_step: (dr,dc) where dr is the number of rows to
                      skip between patch and dc is the number of
                      columns. The parameter should be a tuple of two elements:
                      number
                      of rows and number of columns to skip each iteration.
                      Basically, when the step is 1, the neighbourhood of every
                      first element is taken and every possible rectangular
                      subset is returned. By default it is equal to
                      `neib_shape` in other words, the
                      patches are disjoint. When the step is greater than
                      `neib_shape`, some elements are omitted. When None, this
                      is the same as
                      neib_shape(patch are disjoint)
                      .. note:: Currently the step size should be chosen in the way that the
                         corresponding dimension :math:`i` (width or height) is equal to
                         :math:`n * step\_size_i + neib\_shape_i` for some :math:`n`
    :type neib_step: A 1d tensor-like of 2 values.
    :param mode:
        Possible values:
        ``valid``
           Requires an input that is a multiple of the
           pooling factor (in each direction)
        ``ignore_borders``
           Same as valid, but will ignore the borders
           if the shape(s) of the input
           is not a multiple of the pooling factor(s)
        ``wrap_centered``
           ?? TODO comment
    :type mode: str
    :return:
        Reshapes the input as a 2D tensor where each row is an
        pooling example. Pseudo-code of the output:
          .. code-block:: python
             idx = 0
             for i in xrange(list 1 dim):
                 for j in xrange(list 2 dim):
                     for k in <image column coordinates>:
                         for l in <image row coordinates>:
                             output[idx,:]
                                  = flattened version of ten4[i,j,l:l+r,k:k+c]
                             idx += 1
          .. note:: The operation isn't necessarily implemented internally with
             these for loops, they're just the easiest way to describe the
             output pattern.
    Example:

    .. code-block:: python

        # Defining variables
        images = T.tensor4('images')
        neibs = images2neibs(images, neib_shape=(5, 5))

        # Constructing theano function
        window_function = theano.function([images], neibs)

        # Input tensor (one image 10x10)
        im_val = np.arange(100.).reshape((1, 1, 10, 10))

        # Function application
        neibs_val = window_function(im_val)

    .. note:: The underlying code will construct a 2D tensor of disjoint
       patches 5x5. The output has shape 4x25.
    """
    return Images2Neibs_3D(mode)(ten4, neib_shape, neib_step)
############################################################################################
############################################################################################
############################################################################################
""" Ops for downsampling images.

Planned:
DownsampleFactorMax, DownsampleAvg, DownsampleSoftmax.

"""
# This file should move along with conv.py
from six.moves import xrange
import six.moves.builtins as builtins

import numpy
import theano
from theano import gof, Op, tensor, Variable, Apply

def sw_maxpool(input, ds, ignore_border=False, st=None, padding=(0, 0),
                mode='max'):
    """
    Takes as input a N-D tensor, where N >= 2. It downscales the input image by
    the specified factor, by keeping only the maximum value of non-overlapping
    patches of size (ds[0],ds[1])
    :type input: N-D theano tensor of input images.
    :param input: input images. Max pooling will be done over the 2 last
        dimensions.
    :type ds: tuple of length 2
    :param ds: factor by which to downscale (vertical ds, horizontal ds).
        (2,2) will halve the image in each dimension.
    :type ignore_border: bool
    :param ignore_border: When True, (5,5) input with ds=(2,2)
        will generate a (2,2) output. (3,3) otherwise.
    :type st: tuple of lenght 2
    :param st: stride size, which is the number of shifts
        over rows/cols to get the the next pool region.
        if st is None, it is considered equal to ds
        (no overlap on pooling regions)
    :param padding: (pad_h, pad_w), pad zeros to extend beyond four borders
            of the images, pad_h is the size of the top and bottom margins,
            and pad_w is the size of the left and right margins.
    :type padding: tuple of two ints
    :param mode: 'max', 'sum', 'average_inc_pad' or 'average_exc_pad'.
        Operation executed on each window.  `max` and `sum` always exclude
        the padding in the computation. `average` gives you the choice to
        include or exclude it.
    :type mode: string
    """
    op = DownsampleFactorMax(ds, ignore_border, st=st, padding=padding,
                             mode=mode)
    output = op(input)
    return output

class DownsampleFactorMax(Op):
    """For N-dimensional tensors, consider that the last two
    dimensions span images.  This Op downsamples these images by
    taking the max, sum or average over different patch.
    """
    __props__ = ('ds', 'ignore_border', 'st', 'padding', 'mode')

    @staticmethod
    def out_shape(imgshape, ds, ignore_border=False, st=None, padding=(0, 0)):
        """Return the shape of the output from this op, for input of given
        shape and flags.
        :param imgshape: the shape of a tensor of images. The last two elements
            are interpreted as the number of rows, and the number of cols.
        :type imgshape: tuple, list, or similar of integer or
            scalar Theano variable.
        :param ds: downsample factor over rows and columns
                   this parameter indicates the size of the pooling region
        :type ds: list or tuple of two ints
        :param st: the stride size. This is the distance between the pooling
                   regions. If it's set to None, in which case it equlas ds.
        :type st: list or tuple of two ints
        :param ignore_border: if ds doesn't divide imgshape, do we include an
            extra row/col of partial downsampling (False) or ignore it (True).
        :type ignore_border: bool
        :param padding: (pad_h, pad_w), pad zeros to extend beyond four borders
            of the images, pad_h is the size of the top and bottom margins,
            and pad_w is the size of the left and right margins.
        :type padding: tuple of two ints
        :rtype: list
        :returns: the shape of the output from this op, for input of given
            shape.  This will have the same length as imgshape, but with last
            two elements reduced as per the downsampling & ignore_border flags.
        """
        if len(imgshape) < 2:
            raise TypeError('imgshape must have at least two elements '
                            '(rows, cols)')

        if st is None:
            st = ds
        r, c = imgshape[-2:]
        r += padding[0] * 2
        c += padding[1] * 2

        if ignore_border:
            out_r = (r - ds[0]) // st[0] + 1
            out_c = (c - ds[1]) // st[1] + 1
            if isinstance(r, theano.Variable):
                nr = tensor.maximum(out_r, 0)
            else:
                nr = numpy.maximum(out_r, 0)
            if isinstance(c, theano.Variable):
                nc = tensor.maximum(out_c, 0)
            else:
                nc = numpy.maximum(out_c, 0)
        else:
            if isinstance(r, theano.Variable):
                nr = tensor.switch(tensor.ge(st[0], ds[0]),
                                   (r - 1) // st[0] + 1,
                                   tensor.maximum(0, (r - 1 - ds[0])
                                                  // st[0] + 1) + 1)
            elif st[0] >= ds[0]:
                nr = (r - 1) // st[0] + 1
            else:
                nr = max(0, (r - 1 - ds[0]) // st[0] + 1) + 1

            if isinstance(c, theano.Variable):
                nc = tensor.switch(tensor.ge(st[1], ds[1]),
                                   (c - 1) // st[1] + 1,
                                   tensor.maximum(0, (c - 1 - ds[1])
                                                  // st[1] + 1) + 1)
            elif st[1] >= ds[1]:
                nc = (c - 1) // st[1] + 1
            else:
                nc = max(0, (c - 1 - ds[1]) // st[1] + 1) + 1

        rval = list(imgshape[:-2]) + [nr, nc]
        return rval

    def __init__(self, ds, ignore_border=False, st=None, padding=(0, 0),
                 mode='max'):
        """ Take the max, sum or average or different input patches.
        :param ds: downsample factor over rows and column.
                   ds indicates the pool region size.
        :type ds: list or tuple of two ints
        :param ignore_border: if ds doesn't divide imgshape, do we include
            an extra row/col of partial downsampling (False) or
            ignore it (True).
        :type ignore_border: bool
        : param st: stride size, which is the number of shifts
            over rows/cols to get the the next pool region.
            if st is None, it is considered equal to ds
            (no overlap on pooling regions)
        : type st: list or tuple of two ints or None
        :param padding: (pad_h, pad_w), pad zeros to extend beyond four borders
            of the images, pad_h is the size of the top and bottom margins,
            and pad_w is the size of the left and right margins.
        :type padding: tuple of two ints
        :param mode: 'max', 'sum', 'average_inc_pad', 'average_exc_pad'.
            ('average_inc_pad' excludes the padding from the count,
            'average_exc_pad' include it)
        """
        self.ds = tuple(ds)
        if not all([isinstance(d, int) for d in ds]):
            raise ValueError(
                "DownsampleFactorMax downsample parameters must be ints."
                " Got %s" % str(ds))
        if st is None:
            st = ds
        assert isinstance(st, (tuple, list))
        self.st = tuple(st)
        self.ignore_border = ignore_border
        self.padding = tuple(padding)
        if self.padding != (0, 0) and not ignore_border:
            raise NotImplementedError(
                'padding works only with ignore_border=True')
        if self.padding[0] >= self.ds[0] or self.padding[1] >= self.ds[1]:
            raise NotImplementedError(
                'padding_h and padding_w must be smaller than strides')
        if mode not in ['max', 'average_inc_pad', 'average_exc_pad', 'sum']:
            raise ValueError(
                "DownsampleFactorMax mode parameter only support 'max', 'sum',"
                " 'average_inc_pad' and 'average_exc_pad'. Got %s" % mode)
        self.mode = mode

    def make_node(self, x):
        if x.type.ndim != 4:
            raise TypeError()
        # TODO: consider restricting the dtype?
        x = tensor.as_tensor_variable(x)
        return gof.Apply(self, [x], [x.type()])

    def perform(self, node, inp, out):
        x, = inp
        z, = out
        if len(x.shape) != 4:
            raise NotImplementedError(
                'DownsampleFactorMax requires 4D input for now')
        z_shape = self.out_shape(x.shape, self.ds, self.ignore_border, self.st,
                                 self.padding)
        if (z[0] is None) or (z[0].shape != z_shape):
            z[0] = numpy.empty(z_shape, dtype=x.dtype)
        zz = z[0]
        # number of pooling output rows
        pr = zz.shape[-2]
        # number of pooling output cols
        pc = zz.shape[-1]
        ds0, ds1 = self.ds
        st0, st1 = self.st
        pad_h = self.padding[0]
        pad_w = self.padding[1]
        img_rows = x.shape[-2] + 2 * pad_h
        img_cols = x.shape[-1] + 2 * pad_w
        inc_pad = self.mode == 'average_inc_pad'

        # pad the image
        if self.padding != (0, 0):
            y = numpy.zeros(
                (x.shape[0], x.shape[1], img_rows, img_cols),
                dtype=x.dtype)
            y[:, :, pad_h:(img_rows-pad_h), pad_w:(img_cols-pad_w)] = x
        else:
            y = x
        func = numpy.max
        if self.mode == 'sum':
            func = numpy.sum
        elif self.mode != 'max':
            func = numpy.average

        for n in xrange(x.shape[0]):
            for k in xrange(x.shape[1]):
                for r in xrange(pr):
                    row_st = r * st0
                    row_end = builtins.min(row_st + ds0, img_rows)
                    if not inc_pad:
                        row_st = builtins.max(row_st, self.padding[0])
                        row_end = builtins.min(row_end, x.shape[-2] + pad_h)
                    for c in xrange(pc):
                        col_st = c * st1
                        col_end = builtins.min(col_st + ds1, img_cols)
                        if not inc_pad:
                            col_st = builtins.max(col_st, self.padding[1])
                            col_end = builtins.min(col_end,
                                                      x.shape[-1] + pad_w)
                        zz[n, k, r, c] = func(y[
                            n, k, row_st:row_end, col_st:col_end])
                        if x[n,k,r,c] < zz[n,k,r,c]:
                            zz[n,k,r,c] = 0

        print 'end testing'

    def infer_shape(self, node, in_shapes):
        shp = self.out_shape(in_shapes[0], self.ds,
                             self.ignore_border, self.st, self.padding)
        return [shp]

    # def c_headers(self):
    #     return ['<algorithm>']
    #
    # def c_code(self, node, name, inp, out, sub):
    #     if self.mode not in ('max', 'sum', 'average_exc_pad', 'average_inc_pad'):
    #         raise theano.gof.utils.MethodNotDefined()
    #     x, = inp
    #     z, = out
    #     fail = sub['fail']
    #     ignore_border = int(self.ignore_border)
    #     ds0, ds1 = self.ds
    #     st0, st1 = self.st
    #     pd0, pd1 = self.padding
    #     ccode = """
    #     int typenum = PyArray_ObjectType((PyObject*)%(x)s, 0);
    #     int z_r, z_c; // shape of the output
    #     int r, c; // shape of the padded_input
    #     if(PyArray_NDIM(%(x)s)!=4)
    #     {
    #         PyErr_SetString(PyExc_ValueError, "x must be a 4d ndarray");
    #         %(fail)s;
    #     }
    #     r = PyArray_DIMS(%(x)s)[2];
    #     c = PyArray_DIMS(%(x)s)[3];
    #     r += %(pd0)s * 2;
    #     c += %(pd1)s * 2;
    #     if (%(pd0)s != 0 && %(pd1)s != 0 && !%(ignore_border)s)
    #         {
    #           PyErr_SetString(PyExc_ValueError,
    #             "padding must be (0,0) when ignore border is False");
    #           %(fail)s;
    #         }
    #     if (%(ignore_border)s)
    #     {
    #         // '/' in C is different from '/' in python
    #         if (r - %(ds0)s < 0)
    #         {
    #           z_r = 0;
    #         }
    #         else
    #         {
    #           z_r = (r - %(ds0)s) / %(st0)s + 1;
    #         }
    #         if (c - %(ds1)s < 0)
    #         {
    #           z_c = 0;
    #         }
    #         else
    #         {
    #           z_c = (c - %(ds1)s) / %(st1)s + 1;
    #         }
    #     }
    #     else
    #     {
    #         // decide how many rows the output has
    #         if (%(st0)s >= %(ds0)s)
    #         {
    #             z_r = (r - 1) / %(st0)s + 1;
    #         }
    #         else
    #         {
    #             z_r = std::max(0, (r - 1 - %(ds0)s) / %(st0)s + 1) + 1;
    #         }
    #         // decide how many columns the output has
    #         if (%(st1)s >= %(ds1)s)
    #         {
    #             z_c = (c - 1) / %(st1)s + 1;
    #         }
    #         else
    #         {
    #             z_c = std::max(0, (c - 1 - %(ds1)s) / %(st1)s + 1) + 1;
    #         }
    #     }
    #     // memory allocation of z if necessary
    #     if ((!%(z)s)
    #       || *PyArray_DIMS(%(z)s)!=4
    #       ||(PyArray_DIMS(%(z)s)[0] != PyArray_DIMS(%(x)s)[0])
    #       ||(PyArray_DIMS(%(z)s)[1] != PyArray_DIMS(%(x)s)[1])
    #       ||(PyArray_DIMS(%(z)s)[2] != z_r)
    #       ||(PyArray_DIMS(%(z)s)[3] != z_c)
    #       )
    #     {
    #       if (%(z)s) Py_XDECREF(%(z)s);
    #       npy_intp dims[4] = {0,0,0,0};
    #       dims[0]=PyArray_DIMS(%(x)s)[0];
    #       dims[1]=PyArray_DIMS(%(x)s)[1];
    #       dims[2]=z_r;
    #       dims[3]=z_c;
    #       //TODO: zeros not necessary
    #       %(z)s = (PyArrayObject*) PyArray_ZEROS(4, dims, typenum,0);
    #     }
    #     // used for indexing a pool region inside the input
    #     int r_st, r_end, c_st, c_end;
    #     dtype_%(x)s collector; // temp var for the value in a region
    #     if (z_r && z_c)
    #     {
    #         for(int b=0; b<PyArray_DIMS(%(x)s)[0]; b++){
    #           for(int k=0; k<PyArray_DIMS(%(x)s)[1]; k++){
    #             for(int i=0; i< z_r; i++){
    #               r_st = i * %(st0)s;
    #               r_end = r_st + %(ds0)s;
    #               // skip the padding
    #               r_st = r_st < %(pd0)s ? %(pd0)s : r_st;
    #               r_end = r_end > (r - %(pd0)s) ? r - %(pd0)s : r_end;
    #               // from padded_img space to img space
    #               r_st -= %(pd0)s;
    #               r_end -= %(pd0)s;
    #               // handle the case where no padding, ignore border is True
    #               if (%(ignore_border)s)
    #               {
    #                 r_end = r_end > r ? r : r_end;
    #               }
    #               for(int j=0; j<z_c; j++){
    #                 c_st = j * %(st1)s;
    #                 c_end = c_st + %(ds1)s;
    #                 // skip the padding
    #                 c_st = c_st < %(pd1)s ? %(pd1)s : c_st;
    #                 c_end = c_end > (c - %(pd1)s) ? c - %(pd1)s : c_end;
    #                 dtype_%(z)s * z = (
    #                       (dtype_%(z)s*)(PyArray_GETPTR4(%(z)s, b, k, i, j)));
    #                 // change coordinates from padding_img space into img space
    #                 c_st -= %(pd1)s;
    #                 c_end -= %(pd1)s;
    #                 // handle the case where no padding, ignore border is True
    #                 if (%(ignore_border)s)
    #                 {
    #                   c_end = c_end > c ? c : c_end;
    #                 }
    #     """
    #     if self.mode == 'max':
    #         ccode += """
    #                 // use the first element as the initial value of collector
    #                 collector = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,r_st,c_st)))[0];
    #                 // go through the pooled region in the unpadded input
    #                 for(int m=r_st; m<r_end; m++)
    #                 {
    #                   for(int n=c_st; n<c_end; n++)
    #                   {
    #                     dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];
    #                     collector = (a > collector) ? a : collector;
    #                   }
    #                 }
    #                 dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,i,j)))[0];
    #                 if (collector <  a)
    #                 {
    #                   z[0] = 0;
    #                 }
    #                 else
    #                 {
    #                   z[0] = collector;
    #                 }
    #         """
    #     elif self.mode in ('sum', 'average_exc_pad', 'average_inc_pad'):
    #         ccode += """
    #                 // initialize the sum at zero
    #                 collector = ((dtype_%(x)s)(0));
    #                 // go through the pooled region in the unpadded input
    #                 for(int m=r_st; m<r_end; m++)
    #                 {
    #                   for(int n=c_st; n<c_end; n++)
    #                   {
    #                     dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];
    #                     collector += a;
    #                   }
    #                 }
    #         """
    #         if self.mode == "sum":
    #             ccode += """
    #                 z[0] = collector;
    #             """
    #         elif self.mode == 'average_inc_pad' and self.ignore_border:
    #             ccode += """
    #                 z[0] = collector / (%(ds0)s * %(ds1)s);
    #             """
    #         else:
    #             ccode += """
    #                 z[0] = collector / ((r_end-r_st)*(c_end-c_st));
    #             """
    #     ccode += """
    #               }
    #             }
    #           }
    #         }
    #     }
    #     """
    #     return ccode % locals()
    #
    # def c_code_cache_version(self):
    #     return (0, 6, 8, 3)

def sw_maxpool_binary(input, ds, ignore_border=False, st=None, padding=(0, 0),
                mode='max'):
    """
    Takes as input a N-D tensor, where N >= 2. It downscales the input image by
    the specified factor, by keeping only the maximum value of non-overlapping
    patches of size (ds[0],ds[1])
    :type input: N-D theano tensor of input images.
    :param input: input images. Max pooling will be done over the 2 last
        dimensions.
    :type ds: tuple of length 2
    :param ds: factor by which to downscale (vertical ds, horizontal ds).
        (2,2) will halve the image in each dimension.
    :type ignore_border: bool
    :param ignore_border: When True, (5,5) input with ds=(2,2)
        will generate a (2,2) output. (3,3) otherwise.
    :type st: tuple of lenght 2
    :param st: stride size, which is the number of shifts
        over rows/cols to get the the next pool region.
        if st is None, it is considered equal to ds
        (no overlap on pooling regions)
    :param padding: (pad_h, pad_w), pad zeros to extend beyond four borders
            of the images, pad_h is the size of the top and bottom margins,
            and pad_w is the size of the left and right margins.
    :type padding: tuple of two ints
    :param mode: 'max', 'sum', 'average_inc_pad' or 'average_exc_pad'.
        Operation executed on each window.  `max` and `sum` always exclude
        the padding in the computation. `average` gives you the choice to
        include or exclude it.
    :type mode: string
    """
    op = DownsampleFactorMax_Binary(ds, ignore_border, st=st, padding=padding,
                             mode=mode)
    output = op(input)
    return output

class DownsampleFactorMax_Binary(Op):
    """For N-dimensional tensors, consider that the last two
    dimensions span images.  This Op downsamples these images by
    taking the max, sum or average over different patch.
    """
    __props__ = ('ds', 'ignore_border', 'st', 'padding', 'mode')

    @staticmethod
    def out_shape(imgshape, ds, ignore_border=False, st=None, padding=(0, 0)):
        """Return the shape of the output from this op, for input of given
        shape and flags.
        :param imgshape: the shape of a tensor of images. The last two elements
            are interpreted as the number of rows, and the number of cols.
        :type imgshape: tuple, list, or similar of integer or
            scalar Theano variable.
        :param ds: downsample factor over rows and columns
                   this parameter indicates the size of the pooling region
        :type ds: list or tuple of two ints
        :param st: the stride size. This is the distance between the pooling
                   regions. If it's set to None, in which case it equlas ds.
        :type st: list or tuple of two ints
        :param ignore_border: if ds doesn't divide imgshape, do we include an
            extra row/col of partial downsampling (False) or ignore it (True).
        :type ignore_border: bool
        :param padding: (pad_h, pad_w), pad zeros to extend beyond four borders
            of the images, pad_h is the size of the top and bottom margins,
            and pad_w is the size of the left and right margins.
        :type padding: tuple of two ints
        :rtype: list
        :returns: the shape of the output from this op, for input of given
            shape.  This will have the same length as imgshape, but with last
            two elements reduced as per the downsampling & ignore_border flags.
        """
        if len(imgshape) < 2:
            raise TypeError('imgshape must have at least two elements '
                            '(rows, cols)')

        if st is None:
            st = ds
        r, c = imgshape[-2:]
        r += padding[0] * 2
        c += padding[1] * 2

        if ignore_border:
            out_r = (r - ds[0]) // st[0] + 1
            out_c = (c - ds[1]) // st[1] + 1
            if isinstance(r, theano.Variable):
                nr = tensor.maximum(out_r, 0)
            else:
                nr = numpy.maximum(out_r, 0)
            if isinstance(c, theano.Variable):
                nc = tensor.maximum(out_c, 0)
            else:
                nc = numpy.maximum(out_c, 0)
        else:
            if isinstance(r, theano.Variable):
                nr = tensor.switch(tensor.ge(st[0], ds[0]),
                                   (r - 1) // st[0] + 1,
                                   tensor.maximum(0, (r - 1 - ds[0])
                                                  // st[0] + 1) + 1)
            elif st[0] >= ds[0]:
                nr = (r - 1) // st[0] + 1
            else:
                nr = max(0, (r - 1 - ds[0]) // st[0] + 1) + 1

            if isinstance(c, theano.Variable):
                nc = tensor.switch(tensor.ge(st[1], ds[1]),
                                   (c - 1) // st[1] + 1,
                                   tensor.maximum(0, (c - 1 - ds[1])
                                                  // st[1] + 1) + 1)
            elif st[1] >= ds[1]:
                nc = (c - 1) // st[1] + 1
            else:
                nc = max(0, (c - 1 - ds[1]) // st[1] + 1) + 1

        rval = list(imgshape[:-2]) + [nr, nc]
        return rval

    def __init__(self, ds, ignore_border=False, st=None, padding=(0, 0),
                 mode='max'):
        """ Take the max, sum or average or different input patches.
        :param ds: downsample factor over rows and column.
                   ds indicates the pool region size.
        :type ds: list or tuple of two ints
        :param ignore_border: if ds doesn't divide imgshape, do we include
            an extra row/col of partial downsampling (False) or
            ignore it (True).
        :type ignore_border: bool
        : param st: stride size, which is the number of shifts
            over rows/cols to get the the next pool region.
            if st is None, it is considered equal to ds
            (no overlap on pooling regions)
        : type st: list or tuple of two ints or None
        :param padding: (pad_h, pad_w), pad zeros to extend beyond four borders
            of the images, pad_h is the size of the top and bottom margins,
            and pad_w is the size of the left and right margins.
        :type padding: tuple of two ints
        :param mode: 'max', 'sum', 'average_inc_pad', 'average_exc_pad'.
            ('average_inc_pad' excludes the padding from the count,
            'average_exc_pad' include it)
        """
        self.ds = tuple(ds)
        if not all([isinstance(d, int) for d in ds]):
            raise ValueError(
                "DownsampleFactorMax downsample parameters must be ints."
                " Got %s" % str(ds))
        if st is None:
            st = ds
        assert isinstance(st, (tuple, list))
        self.st = tuple(st)
        self.ignore_border = ignore_border
        self.padding = tuple(padding)
        if self.padding != (0, 0) and not ignore_border:
            raise NotImplementedError(
                'padding works only with ignore_border=True')
        if self.padding[0] >= self.ds[0] or self.padding[1] >= self.ds[1]:
            raise NotImplementedError(
                'padding_h and padding_w must be smaller than strides')
        if mode not in ['max', 'average_inc_pad', 'average_exc_pad', 'sum']:
            raise ValueError(
                "DownsampleFactorMax mode parameter only support 'max', 'sum',"
                " 'average_inc_pad' and 'average_exc_pad'. Got %s" % mode)
        self.mode = mode

    def make_node(self, x):
        if x.type.ndim != 4:
            raise TypeError()
        # TODO: consider restricting the dtype?
        x = tensor.as_tensor_variable(x)
        return gof.Apply(self, [x], [x.type()])

    def perform(self, node, inp, out):
        x, = inp
        z, = out
        if len(x.shape) != 4:
            raise NotImplementedError(
                'DownsampleFactorMax requires 4D input for now')
        z_shape = self.out_shape(x.shape, self.ds, self.ignore_border, self.st,
                                 self.padding)
        if (z[0] is None) or (z[0].shape != z_shape):
            z[0] = numpy.empty(z_shape, dtype=x.dtype)
        zz = z[0]
        # number of pooling output rows
        pr = zz.shape[-2]
        # number of pooling output cols
        pc = zz.shape[-1]
        ds0, ds1 = self.ds
        st0, st1 = self.st
        pad_h = self.padding[0]
        pad_w = self.padding[1]
        img_rows = x.shape[-2] + 2 * pad_h
        img_cols = x.shape[-1] + 2 * pad_w
        inc_pad = self.mode == 'average_inc_pad'

        # pad the image
        if self.padding != (0, 0):
            y = numpy.zeros(
                (x.shape[0], x.shape[1], img_rows, img_cols),
                dtype=x.dtype)
            y[:, :, pad_h:(img_rows-pad_h), pad_w:(img_cols-pad_w)] = x
        else:
            y = x
        func = numpy.max
        if self.mode == 'sum':
            func = numpy.sum
        elif self.mode != 'max':
            func = numpy.average

        for n in xrange(x.shape[0]):
            for r in xrange(pr):
                row_st = r * st0
                row_end = builtins.min(row_st + ds0, img_rows)
                if not inc_pad:
                    row_st = builtins.max(row_st, self.padding[0])
                    row_end = builtins.min(row_end, x.shape[-2] + pad_h)
                for c in xrange(pc):
                    col_st = c * st1
                    col_end = builtins.min(col_st + ds1, img_cols)
                    if not inc_pad:
                        col_st = builtins.max(col_st, self.padding[1])
                        col_end = builtins.min(col_end,
                                                  x.shape[-1] + pad_w)
                    temp_val = func(y[
                        n, :, row_st:row_end, col_st:col_end])
                    for k in xrange(x.shape[1]):
                        if x[n,k,r,c] < temp_val:
                            zz[n,k,r,c] = 0
                        else:
                            zz[n,k,r,c] = 1

    def infer_shape(self, node, in_shapes):
        shp = self.out_shape(in_shapes[0], self.ds,
                             self.ignore_border, self.st, self.padding)
        return [shp]

    # def c_headers(self):
    #     return ['<algorithm>']
    #
    # def c_code(self, node, name, inp, out, sub):
    #     print 'This run uses sw_maxpool_binary.'
    #     if self.mode not in ('max', 'sum', 'average_exc_pad', 'average_inc_pad'):
    #         raise theano.gof.utils.MethodNotDefined()
    #     x, = inp
    #     z, = out
    #     fail = sub['fail']
    #     ignore_border = int(self.ignore_border)
    #     ds0, ds1 = self.ds
    #     st0, st1 = self.st
    #     pd0, pd1 = self.padding
    #     ccode = """
    #     int typenum = PyArray_ObjectType((PyObject*)%(x)s, 0);
    #     int z_r, z_c; // shape of the output
    #     int r, c; // shape of the padded_input
    #     if(PyArray_NDIM(%(x)s)!=4)
    #     {
    #         PyErr_SetString(PyExc_ValueError, "x must be a 4d ndarray");
    #         %(fail)s;
    #     }
    #     r = PyArray_DIMS(%(x)s)[2];
    #     c = PyArray_DIMS(%(x)s)[3];
    #     r += %(pd0)s * 2;
    #     c += %(pd1)s * 2;
    #     if (%(pd0)s != 0 && %(pd1)s != 0 && !%(ignore_border)s)
    #         {
    #           PyErr_SetString(PyExc_ValueError,
    #             "padding must be (0,0) when ignore border is False");
    #           %(fail)s;
    #         }
    #     if (%(ignore_border)s)
    #     {
    #         // '/' in C is different from '/' in python
    #         if (r - %(ds0)s < 0)
    #         {
    #           z_r = 0;
    #         }
    #         else
    #         {
    #           z_r = (r - %(ds0)s) / %(st0)s + 1;
    #         }
    #         if (c - %(ds1)s < 0)
    #         {
    #           z_c = 0;
    #         }
    #         else
    #         {
    #           z_c = (c - %(ds1)s) / %(st1)s + 1;
    #         }
    #     }
    #     else
    #     {
    #         // decide how many rows the output has
    #         if (%(st0)s >= %(ds0)s)
    #         {
    #             z_r = (r - 1) / %(st0)s + 1;
    #         }
    #         else
    #         {
    #             z_r = std::max(0, (r - 1 - %(ds0)s) / %(st0)s + 1) + 1;
    #         }
    #         // decide how many columns the output has
    #         if (%(st1)s >= %(ds1)s)
    #         {
    #             z_c = (c - 1) / %(st1)s + 1;
    #         }
    #         else
    #         {
    #             z_c = std::max(0, (c - 1 - %(ds1)s) / %(st1)s + 1) + 1;
    #         }
    #     }
    #     // memory allocation of z if necessary
    #     if ((!%(z)s)
    #       || *PyArray_DIMS(%(z)s)!=4
    #       ||(PyArray_DIMS(%(z)s)[0] != PyArray_DIMS(%(x)s)[0])
    #       ||(PyArray_DIMS(%(z)s)[1] != PyArray_DIMS(%(x)s)[1])
    #       ||(PyArray_DIMS(%(z)s)[2] != z_r)
    #       ||(PyArray_DIMS(%(z)s)[3] != z_c)
    #       )
    #     {
    #       if (%(z)s) Py_XDECREF(%(z)s);
    #       npy_intp dims[4] = {0,0,0,0};
    #       dims[0]=PyArray_DIMS(%(x)s)[0];
    #       dims[1]=PyArray_DIMS(%(x)s)[1];
    #       dims[2]=z_r;
    #       dims[3]=z_c;
    #       //TODO: zeros not necessary
    #       %(z)s = (PyArrayObject*) PyArray_ZEROS(4, dims, typenum,0);
    #     }
    #     // used for indexing a pool region inside the input
    #     int r_st, r_end, c_st, c_end;
    #     dtype_%(x)s collector; // temp var for the value in a region
    #     if (z_r && z_c)
    #     {
    #         for(int b=0; b<PyArray_DIMS(%(x)s)[0]; b++){
    #           for(int k=0; k<PyArray_DIMS(%(x)s)[1]; k++){
    #             for(int i=0; i< z_r; i++){
    #               r_st = i * %(st0)s;
    #               r_end = r_st + %(ds0)s;
    #               // skip the padding
    #               r_st = r_st < %(pd0)s ? %(pd0)s : r_st;
    #               r_end = r_end > (r - %(pd0)s) ? r - %(pd0)s : r_end;
    #               // from padded_img space to img space
    #               r_st -= %(pd0)s;
    #               r_end -= %(pd0)s;
    #               // handle the case where no padding, ignore border is True
    #               if (%(ignore_border)s)
    #               {
    #                 r_end = r_end > r ? r : r_end;
    #               }
    #               for(int j=0; j<z_c; j++){
    #                 c_st = j * %(st1)s;
    #                 c_end = c_st + %(ds1)s;
    #                 // skip the padding
    #                 c_st = c_st < %(pd1)s ? %(pd1)s : c_st;
    #                 c_end = c_end > (c - %(pd1)s) ? c - %(pd1)s : c_end;
    #                 dtype_%(z)s * z = (
    #                       (dtype_%(z)s*)(PyArray_GETPTR4(%(z)s, b, k, i, j)));
    #                 // change coordinates from padding_img space into img space
    #                 c_st -= %(pd1)s;
    #                 c_end -= %(pd1)s;
    #                 // handle the case where no padding, ignore border is True
    #                 if (%(ignore_border)s)
    #                 {
    #                   c_end = c_end > c ? c : c_end;
    #                 }
    #     """
    #     if self.mode == 'max':
    #         ccode += """
    #                 // use the first element as the initial value of collector
    #                 collector = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,r_st,c_st)))[0];
    #                 // go through the pooled region in the unpadded input
    #                 for(int m=r_st; m<r_end; m++)
    #                 {
    #                   for(int n=c_st; n<c_end; n++)
    #                   {
    #                     dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];
    #                     collector = (a > collector) ? a : collector;
    #                   }
    #                 }
    #                 dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,i,j)))[0];
    #                 if (collector <  a)
    #                 {
    #                   z[0] = 0;
    #                 }
    #                 else
    #                 {
    #                   z[0] = 1;
    #                 }
    #         """
    #     elif self.mode in ('sum', 'average_exc_pad', 'average_inc_pad'):
    #         ccode += """
    #                 // initialize the sum at zero
    #                 collector = ((dtype_%(x)s)(0));
    #                 // go through the pooled region in the unpadded input
    #                 for(int m=r_st; m<r_end; m++)
    #                 {
    #                   for(int n=c_st; n<c_end; n++)
    #                   {
    #                     dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];
    #                     collector += a;
    #                   }
    #                 }
    #         """
    #         if self.mode == "sum":
    #             ccode += """
    #                 z[0] = collector;
    #             """
    #         elif self.mode == 'average_inc_pad' and self.ignore_border:
    #             ccode += """
    #                 z[0] = collector / (%(ds0)s * %(ds1)s);
    #             """
    #         else:
    #             ccode += """
    #                 z[0] = collector / ((r_end-r_st)*(c_end-c_st));
    #             """
    #     ccode += """
    #               }
    #             }
    #           }
    #         }
    #     }
    #     """
    #     return ccode % locals()
    #
    # def c_code_cache_version(self):
    #     return (0, 6, 8, 3)