__author__ = 'minhtannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import glob

import numpy as np
np.set_printoptions(threshold='nan')

import os

import time
import timeit
import itertools

from scipy.linalg import inv, norm
from scipy import misc

import theano
import theano.tensor as T

from theano.tensor.nlinalg import MatrixPinv, Det, diag, MatrixInverse
from theano.tensor.slinalg import Cholesky
from theano.tensor.nnet import conv
from theano.tensor.nnet.neighbours import images2neibs
from theano.tensor.signal import downsample

from sklearn.metrics import confusion_matrix, precision_score

import copy

from kmeans_init import kmeans_init, klp_kmeans
from plot_mofa import *
from swmfa import *

class ConvMofa(SwMofa):
    """
    EM Algorithm for Convolutional Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes

    """
    def __init__(self, x, dat, K, M, W, H, w, h, Cin, Ni, amps_val_init=None,
                 lambdas_val_init=None, lambda_covs_val_init=None,
                 covs_val_init=None, inv_covs_val_init=None,
                 psis_val_init=None, means_val_init=None,
                 PPCA=False,lock_psis=False,
                 update_mode='Hinton', em_mode='soft',
                 rs_clip = 0.0,
                 max_condition_number=1.e3,
                 init=True,init_ppca=False):

        # required
        self.K     = K
        self.M     = M
        self.data_4D  = x
        self.Ni     = Ni # no. of images
        self.w     = w
        self.h     = h
        self.Cin   = Cin
        self.D     = self.h * self.w * self.Cin # patch size
        self.W     = W
        self.H     = H
        self.Np = (self.H - self.h + 1)*(self.W - self.w + 1) # no. of patches per image
        self.N = self.Ni * self.Np # total no. of patches
        self.Nl = self.N # total no. of hidden units
        self.update_mode  = update_mode
        self.lambdas_val_init = lambdas_val_init
        self.amps_val_init = amps_val_init
        self.em_mode = em_mode

        # options
        self.PPCA                 = PPCA
        self.lock_psis            = lock_psis
        self.rs_clip              = rs_clip
        self.max_condition_number = max_condition_number
        assert rs_clip >= 0.0

        # patchification
        print 'Start patchification'
        start_time = time.time()
        self.data = images2neibs_3D(ten4=self.data_4D, neib_shape=(self.h, self.w), neib_step=(1,1), mode='valid')
        self.dataT = self.data.T
        createPatches = theano.function([], self.data,
                                         updates=[], on_unused_input='ignore',
                                         givens={x:dat})
        data_pat_val = createPatches()
        stop_time = time.time()
        print 'patchification takes %f' %(stop_time - start_time)

        # initialize means and psis
        print self.N
        print 'Start kmeans'
        start_time = time.time()
        # self.means_val_init = klp_kmeans(data=data_pat_val, cluster_num=self.K, alpha=0.001, epochs=200, batch=200, verbose=False, use_gpu=False)
        # means_val_init = np.asarray(np.abs(np.random.randn(self.K, self.Cin, self.h, self.w)), dtype=theano.config.floatX)
        # self.means_val_init = np.reshape(means_val_init, newshape=(self.K, self.D))
        self.means_val_init = means_val_init
        self.means_val_init[0] = 0.
        stop_time = time.time()
        print 'k-means takes %f' %(stop_time - start_time)

        # set mean of the closest cluster to the origin to 0
        mean_norms = np.linalg.norm(self.means_val_init, axis = 1)
        indx_min = np.argmin(mean_norms)
        self.means_val_init[[0, indx_min],:] = self.means_val_init[[indx_min,0],:]
        self.means_val_init[0] = 0.0

        print 'Start computing psis'
        start_time = time.time()
        # my_var_init = np.var(data_pat_val, axis=0)
        # my_var_init = np.asarray(np.sqrt(np.abs(np.random.randn(1, np.shape(data_pat_val)[1]))), dtype=theano.config.floatX)
        # my_var_init = my_var_init.reshape(-1)
        # my_var_init = psis_val_init
        # self.psis_val_init = np.tile(my_var_init[None,:],(self.K,1))
        self.psis_val_init = psis_val_init
        # del my_var_init
        del data_pat_val
        stop_time = time.time()
        print 'compute psis takes %f' %(stop_time - start_time)

        # empty arrays to be filled
        self.betas       = theano.shared(value=np.zeros((self.K, self.M, self.D), dtype=theano.config.floatX),
                                         name='betas', borrow=True)
        # self.betas       = self.betas + 1
        self.latents     = theano.shared(value=np.zeros((self.K, self.M, self.N), dtype=theano.config.floatX),
                                         name='latents', borrow=True)
        self.latent_covs = theano.shared(value=np.zeros((self.K, self.M, self.M, self.N), dtype=theano.config.floatX),
                                         name='latent_covs', borrow=True)
        self.means       = theano.shared(value=np.asarray(self.means_val_init, dtype=theano.config.floatX), name='means', borrow=True)
        self.rs          = theano.shared(value=np.zeros((self.K, self.Nl), dtype=theano.config.floatX),
                                         name='rs', borrow=True)

        # initialize
        # if init = True, use _initialize to initialize the parameters.
        # otherwise, use ..._val_init from the input list
        if init:
            self._initialize(init_ppca)
        else:
            self.amps = theano.shared(value=np.asarray(amps_val_init, dtype=theano.config.floatX), name='amps', borrow=True)
            self.lambdas = theano.shared(value=np.asarray(lambdas_val_init, dtype=theano.config.floatX), name='lambdas', borrow=True)
            self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val_init, dtype=theano.config.floatX), name='lambdas_covs', borrow=True)
            self.covs = theano.shared(value=np.asarray(covs_val_init, dtype=theano.config.floatX), name='covs', borrow=True)
            self.inv_covs = theano.shared(value=np.asarray(inv_covs_val_init, dtype=theano.config.floatX), name='inv_covs', borrow=True)

    def _initialize(self, init_ppca, maxiter=200, tol=1e-4):
        """
        Run K-means
        """

        # Randomly assign factor loadings
        # lambdas_value = np.random.randn(self.K,self.D,self.M) / \
        #     np.sqrt(self.max_condition_number)
        # lambdas_value[0] = 1e-15
        lambdas_value = self.lambdas_val_init
        lambdas_value[0] = 0.0*lambdas_value[0]
        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas', borrow=True)

        # Set (high rank) variance to variance of all data, along a dimension
        self.psis= theano.shared(value=np.asarray(self.psis_val_init, dtype=theano.config.floatX), name='psis', borrow=True)

        # Set initial lambda_covs, covs and compute inv_covs
        covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        lambda_covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        inv_covs_val = 0. * covs_val

        if self.em_mode == 'soft':
            # compute the inv_covs using matrix inversion lemma
            for k in range(self.K):
                covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T) + \
                    np.diag(self.psis_val_init[k])
                lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                psiI = inv(np.diag(self.psis_val_init[k]))
                lam  = lambdas_value[k]
                lamT = lam.T
                step = inv(np.eye(self.M) + np.dot(lamT,np.dot(psiI,lam)))
                step = np.dot(step,np.dot(lamT,psiI))
                step = np.dot(psiI,np.dot(lam,step))
                inv_covs_val[k] = psiI - step
        else:
            for k in range(self.K):
                covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)


        self.covs = theano.shared(value=np.asarray(covs_val, dtype=theano.config.floatX),
                                  name='covs', borrow=True)

        self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val, dtype=theano.config.floatX),
                                  name='lambda_covs', borrow=True)

        self.inv_covs = theano.shared(value=np.asarray(inv_covs_val, dtype=theano.config.floatX),
                                  name='inv_covs', borrow=True)

        # Randomly assign the amplitudes.
        # amps_val = np.random.rand(self.K)
        # amps_val /= np.sum(amps_val)
        # self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
        #                           name='amps', borrow=True)
        self.amps = theano.shared(value=np.asarray(self.amps_val_init, dtype=theano.config.floatX), name='amps', borrow=True)

    def take_EM_step(self):
        """
        Do one E step and then do one M step.
        """
        self._E_step()
        self._M_step()

    def _E_step(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """
        # compute the lambda dagger
        if self.em_mode == 'soft':
            self.betas, updates4 = theano.scan(fn=lambda l, ic: T.dot(l.T, ic),
                                     outputs_info=None,
                                     sequences=[self.lambdas, self.inv_covs])
            betameans = T.sum(self.betas[:,0,:]*self.means, axis=1)

            latents_conv = T.dot(self.betas[:,0,:], self.dataT)
        else:
            betas_temp, updates4 = theano.scan(fn=lambda l: T.dot(1.0/(T.dot(l.T,l)),l.T),
                                     outputs_info=None,
                                     sequences=self.lambdas[1:])
            self.betas_new = T.set_subtensor(self.betas[1:], betas_temp)

            betameans = T.sum(self.betas_new[:,0,:]*self.means, axis=1)

            latents_conv = T.dot(self.betas_new[:,0,:], self.dataT)

        self.latents = latents_conv[:,None,:] - betameans.dimshuffle(0, 'x', 'x')

        # compute E[zz'|x] using eq. 14
        # step 1 computes E[z|x](E[z|x])'
        # step 2 computes beta_j*lambda_j
        # step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
        #                              outputs_info=None,
        #                              sequences=self.latents)
        step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
                                            outputs_info=None,
                                            sequences=self.latents)
        step2, updates7 = theano.scan(fn=lambda b, l: T.dot(b, l),
                                      outputs_info=None,
                                      sequences=[self.betas, self.lambdas])
        self.latent_covs, updates8_ss = theano.scan(fn=lambda s2, s1: T.eye(self.M,self.M)[:,:,None] - s2[:,:,None] + s1,
                                       outputs_info=None,
                                       sequences=[step2, step1])

        # compute the responsibilities and log-likelihood using eq. 12
        self.logLs, self.rs_conv = self._calc_probs()

        # manipulate logrs using maxpoolings
        self.rs_conv = T.reshape(self.rs_conv, newshape=(self.K, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        self.rs_conv = self.rs_conv.dimshuffle(1,0,2,3)
        self.rs_conv_new = sw_maxpool_binary(input=self.rs_conv[:,1:,:,:], ds=(5,5), ignore_border=True, st=(1,1), padding=(2, 2), mode='max')
        self.rs_sw_temp = T.set_subtensor(self.rs_conv[:,1:,:,:], self.rs_conv_new)
        self.rs_temp = T.reshape(self.rs_sw_temp.dimshuffle(1,0,2,3), newshape=(self.K, self.Nl))
        self.rs_nonzero_max = T.max(self.rs_temp[1:], axis=0)
        if self.em_mode == 'soft':
            self.rs_zero_new = T.gt(self.rs_temp[0],self.rs_nonzero_max)
        else:
            self.rs_zero_new = T.ge(0,self.rs_nonzero_max)
        self.rs = T.set_subtensor(self.rs_temp[0], self.rs_zero_new)

        # compute the pi's
        self.sumrs = T.sum(self.rs, axis=1)
        self.amps_new, updates18 = theano.scan(fn=lambda s: s / float(self.Nl),
                                outputs_info=None,
                                sequences=self.sumrs)

    def _M_step(self):
        """
        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """
        if self.update_mode == 'Ross_Fadely':
            # update means - implicitly included in eq. 15
            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[self.lambdas, self.latents])

            means_new, updates10 = theano.scan(fn=lambda resp, llt, s: T.sum(resp * (self.dataT - llt), axis=1) / s,
                                     outputs_info=None,
                                     sequences=[self.rs[1:], lambdalatents[1:], self.sumrs[1:]])

            self.means_new = T.set_subtensor(self.means[1:], means_new)

            # update lambdas using eq.15
            zeroed, updates11 = theano.scan(fn=lambda m: self.dataT - m[:,None],
                                 outputs_info=None,
                                 sequences=self.means_new)

            lambdas_new, updates12 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixInverse()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[self.latents[1:], self.rs[1:], self.latent_covs[1:], zeroed[1:]])

            self.lambdas_new = T.set_subtensor(self.lambdas[1:], lambdas_new)

            # update psis using eq. 16
            psis, updates13 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                               outputs_info=None,
                               sequences=[self.rs, zeroed, lambdalatents, self.sumrs])
        else:
            latents_tilde = T.concatenate([self.latents,
                                           theano.shared(value=np.ones((self.K, 1 ,self.N),
                                                                       dtype=theano.config.floatX),borrow=True)],
                                          axis=1)
            col1 = T.concatenate([self.latent_covs, (self.latents[:,:,None,:]).dimshuffle((0,2,1,3))], axis=1)
            latent_covs_tilde = T.concatenate([col1, latents_tilde[:,:,None,:]], axis=2)
            dataT = T.tile(self.dataT[None,:,:], reps=[self.K,1,1])
            lambdas_tilde, updates22 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixPinv()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[latents_tilde, self.rs, latent_covs_tilde, dataT])
            lambdas_new = lambdas_tilde[1:,:,0:self.M]
            self.lambdas_new = T.set_subtensor(self.lambdas[1:], lambdas_new)
            means_new = lambdas_tilde[1:,:,self.M]
            self.means_new = T.set_subtensor(self.means[1:], means_new)

            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[lambdas_tilde, latents_tilde])

            # Just for testing #
            correct_lambdas_tilde = T.concatenate([self.lambdas, self.means[:,:,None]],axis=2)
            correct_lambdalatents, updates767 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[correct_lambdas_tilde, latents_tilde])
            ####################

            psis, update23 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                                         outputs_info=None,
                                         sequences=[self.rs[1:], dataT[1:], lambdalatents[1:], self.sumrs[1:]])

        maxpsi, updates14 = theano.scan(fn=lambda p: T.max(p),outputs_info=None, sequences=psis)

        maxlam, updates15 = theano.scan(fn=lambda l: T.max(T.sum(l * l, axis=0)),
                             outputs_info=None,
                             sequences=self.lambdas_new)
        minpsi, updates16 = theano.scan(fn=lambda maxp, maxl: T.max([maxp, maxl]) / self.max_condition_number,
                             outputs_info=None,
                             sequences=[maxpsi, maxlam])
        self.psis_new, updates17   = theano.scan(fn=lambda p, minp: T.clip(p, minp, np.Inf),
                                  outputs_info=None,
                                  sequences=[psis, minpsi])
        # make all elements in each rows of psis the same
        if self.PPCA:
            self.psis_new, updates19 = theano.scan(fn=lambda psn: T.mean(psn)*T.ones(self.D),
                                                   outputs_info=None,
                                                   sequences=self.psis_new)

        # make psi for each cluster the same
        if self.lock_psis:
            self.psis_new = T.dot(self.sumrs[1:],self.psis_new)/T.sum(self.sumrs[1:])
            self.psis_new = (self.psis_new).dimshuffle('x',0)
            self.psis_new = T.tile(self.psis_new,(self.K, 1))

        # update covs
        self._update_covs()

    def _update_covs(self):
        """
        Update self.cov for responsibility, logL calc
        """

        # total cov = lambda*lambda' + psi
        self.lambda_covs_new, updates20 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas_new])

        if self.em_mode == 'soft':
            self.covs_new, updates1 = theano.scan(fn=lambda lc, p: lc + diag(p),
                                              outputs_info=None,
                                              sequences=[self.lambda_covs_new, self.psis_new])
            # compute the inv_covs using the matrix inversion lemma
            self.inv_covs_new, updates2 = theano.scan(fn=lambda l, p: MatrixInverse()(diag(p)) - T.dot(MatrixInverse()(diag(p)),T.dot(l,T.dot(MatrixInverse()(T.eye(self.M) + T.dot(T.transpose(l),T.dot(MatrixInverse()(diag(p)),l))),T.dot(T.transpose(l),MatrixInverse()(diag(p)))))),
                                              outputs_info=None,
                                              sequences=[self.lambdas_new, self.psis_new])
        else:
            self.covs_new, updates1 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas_new])
            self.inv_covs_new, updates2 = theano.scan(fn=lambda l: T.eye(self.D) - T.dot(l,T.dot(1.0/(T.dot(T.transpose(l),l)), T.transpose(l))),
                                              outputs_info=None,
                                              sequences=self.lambdas_new)

    def _calc_probs(self):
        """
        Calculate log likelihoods, responsibilities for each datum
        under each component.
        """
        if self.em_mode == 'soft':
            logrs, updates3 = theano.scan(fn=lambda a, c, m, ic: T.log(a) - float(0.5 * np.log(2 * np.pi) * self.D) - 0.5 * 2 * T.sum(T.log(diag(Cholesky()(c)))) - 0.5 * T.sum((self.data - m).T * T.dot(ic, (self.data - m).T), axis=0),
                                outputs_info=None,
                                sequences=[self.amps, self.covs, self.means, self.inv_covs])
        else:
            logrs, updates3 = theano.scan(fn=lambda a, c, m, ic: T.log(a) - float(0.5 * np.log(2 * np.pi) * self.D) - 0.5 * T.sum((self.data - m).T * T.dot(ic, (self.data - m).T), axis=0),
                                outputs_info=None,
                                sequences=[self.amps, self.covs, self.means, self.inv_covs])

        L = self._log_sum(logrs)
        logrs -= L[None, :]
        if self.rs_clip > 0.0:
            logrs = T.clip(logrs,T.log(self.rs_clip),np.Inf)

        return L, T.exp(logrs)

    def _log_sum(self,loglikes):
        """
        Calculate sum of log likelihoods
        """
        a = T.max(loglikes, axis=0)
        return a + T.log(T.sum(T.exp(loglikes - a[None, :]), axis=0))

    def fit(self, x, dat, tol, maxiter, verbose, mode='NLL'):
        """
        Train the model using the whole dataset
        """
        # take one E step and then one M step
        self.take_EM_step()

        # updates all parameters after each run
        updates = []
        updates.append((self.means, self.means_new))
        updates.append((self.lambdas, self.lambdas_new))
        updates.append((self.psis, self.psis_new))
        updates.append((self.covs, self.covs_new))
        updates.append((self.inv_covs, self.inv_covs_new))
        updates.append((self.amps, self.amps_new))
        updates.append((self.lambda_covs, self.lambda_covs_new))

        # construct a function that infers the z, zz' and responsibilities in the E step and then uses updates
        # to update lambdas, means, psis, ... (see update list above)
        output_one_iter = theano.function([], [self.logLs, self.rs, self.betas, self.latents, self.latent_covs, self.psis_new, self.sumrs],
                                         updates=updates, on_unused_input='ignore',
                                         givens={x:dat})

        # start timing
        start_time = timeit.default_timer()

        # run EM
        L = None
        LogLs_vec = []
        epochs = []
        LC = copy.copy(self.lambda_covs.get_value())
        PS = copy.copy(self.psis.get_value())
        LogLs_vec = []
        dPS_vec = []
        dLC_vec = []
        for i in xrange(maxiter):
            logLs_val, rs_val, betas_val, latents_val, latent_covs_val, psis_val, sumrs_val = output_one_iter()
            newL = np.sum(logLs_val)
            # print initial log-likelihood
            if i == 0 and verbose:
                print("Initial NLL=", -newL)
            newLC = self.lambda_covs.get_value()
            newPS = self.psis.get_value()
            dLC = norm(newLC - LC)/norm(LC)
            dPS = norm(newPS - PS)/norm(PS)
            # stopping
            if mode == 'NLL':
                if L!=None:
                    dL = np.abs((newL-L)/L)
                    if i > 5 and dL < tol:
                        break
                L = newL
            else:
                if i > 5 and dLC < tol and dPS < tol:
                    break

            LC = copy.copy(newLC)
            PS = copy.copy(newPS)
            dLC_vec.append(dLC)
            dPS_vec.append(dPS)
            LogLs_vec.append(-newL)
            epochs.append(i)

        # let us know if EM converges or not and after how many epochs
        if i < maxiter - 1:
            if verbose:
                print("EM converged after {0} iterations".format(i))
                print("Final NLL={0}".format(-newL))
        else:
            print("Warning:EM didn't converge after {0} iterations".format(i))

        # stop timing
        print '\n'
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print '\n'

        return LogLs_vec, dLC_vec, dPS_vec, epochs

    def batch_fit(self, x, dat, K, batch_size, tol, max_epochs, verbose, mode='NLL'):
        D, N = np.shape(dat)
        n_batches = N/batch_size
        index = T.iscalar()

        self.take_EM_step()

        updates = []
        updates.append((self.means, self.means_new))
        updates.append((self.lambdas, self.lambdas_new))
        updates.append((self.psis, self.psis_new))
        updates.append((self.covs, self.covs_new))
        updates.append((self.inv_covs, self.inv_covs_new))
        updates.append((self.amps, self.amps_new))
        updates.append((self.lambda_covs, self.lambda_covs_new))

        shared_dat = theano.shared(dat)

        output_one_iter = theano.function([index], [self.logLs, self.rs, self.betas, self.latents,
                                                    self.latent_covs, self.sumrs,
                                                    self.means_new, self.covs_new, self.inv_covs_new,
                                                    self.lambdas_new, self.amps_new, self.psis_new, self.lambda_covs_new, self.data],
                                          updates=updates, on_unused_input='ignore',
                                          givens={x: shared_dat[:,index * batch_size: (index + 1) * batch_size]})

        start_time = timeit.default_timer()


        done_looping = False
        L = None
        LogLs_vec = []
        epochs = []
        epoch = -1
        LC = copy.copy(self.lambda_covs.get_value())
        PS = copy.copy(self.psis.get_value())
        LogLs_vec = []
        dPS_vec = []
        dLC_vec = []

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1
            for minibatch_index in xrange(n_batches):
                logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, \
                means_val, covs_val, inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, data_val \
                    = output_one_iter(minibatch_index)

            logLs_val_whole_dataset = cal_logLs(K=K, N=N, D=D, data=dat,
                                                covs=covs_val, means=means_val, inv_covs=inv_covs_val, pi=amps_val)
            newL = np.sum(logLs_val_whole_dataset)
            if epoch == 0 and verbose:
                print("Initial NLL=", -newL)

            newLC = self.lambda_covs.get_value()
            newPS = self.psis.get_value()
            dLC = norm(newLC - LC)/norm(LC)
            dPS = norm(newPS - PS)/norm(PS)

            if mode == 'NLL':
                if L!=None:
                    dL = np.abs((newL-L)/L)
                    if epoch > 5 and dL < tol:
                        done_looping = True
                L = newL
            else:
                if epoch > 5 and dLC < tol and dPS < tol:
                    break

            LC = copy.copy(newLC)
            PS = copy.copy(newPS)
            LogLs_vec.append(-newL)
            epochs.append(epoch)
            dLC_vec.append(dLC)
            dPS_vec.append(dPS)

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(-newL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print '\n'
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print '\n'

        return logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, \
               means_val, covs_val, inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, LogLs_vec, epochs, \
               dLC_vec, dPS_vec

    def predict(self, x, dat):
        """
        Predict the labels of new data
        """
        self._E_step()
        getResults = theano.function([], [self.rs, self.means],
                                         updates=[], on_unused_input='ignore',
                                         givens={x:dat})
        rs_val, means_val = getResults()
        labels = np.argmax(rs_val,0)
        return labels, means_val, rs_val

##############################################################################################################
# Test Code #
##############################################################################################################
import unittest

class ConvSimpleTestCase(SimpleSwMFATestCase):
    """
    Unittests for Convolutional Rendering Model

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls):
        """
        Run before tests are executed.

        Synthesize data and run EM on the synthetic data
        """
        # skip BaseTestCase
        if cls is BaseTestCase:
            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")
        super(BaseTestCase, cls).setUpClass()

        # set internal parameters
        cls.h = 3 # height of filters
        cls.w = 3 # width of filters
        cls.Cin = 3 # depth of filters
        cls.D = cls.w * cls.h * cls.Cin # filters' size
        cls.K = 3 # no. filters
        cls.H = 8 # width of input image
        cls.W = 8 # height of input image
        Nh = cls.H - cls.h + 1 # no. patches along the height dimension ~
        Nw = cls.W - cls.w + 1 # no. patches along the width dimension ~
        cls.Np = Nh * Nw # no. patches /image
        cls.Ni = 50 # number of input images
        cls.M = 1 # dimension of z
        cls.templates = 'random' # template to use
        cls.psi_scale = 1
        cls.em_mode = 'soft'
        cls.update_mode = 'hinton'

        maxiter = 100
        tol = 1e-10
        verbose = True

        cls.seed = 3
        np.random.seed(cls.seed)

        # set which patches to render
        cls.matA = np.zeros((cls.Ni, Nh, Nw))
        for n in xrange(cls.Ni):
            coorVec = list(itertools.product(range(cls.H - cls.h + 1), repeat=2))
            szVec = np.size(coorVec)
            while szVec > 0:
                token_val = np.random.randint(0,2)
                if token_val == 0:
                    continue
                else:
                    indx_choice = np.random.randint(0,shape(coorVec)[0])
                    coor = coorVec[indx_choice]
                    cls.matA[n, coor[0], coor[1]] = 1
                    for i in xrange(-2,3,1):
                        for j in xrange(-2,3,1):
                            if (coor[0] + i, coor[1] + j) in coorVec:
                                coorVec.remove((coor[0] + i, coor[1] + j))
                    szVec = np.size(coorVec)

        cls.matAtest = np.zeros((cls.Ni, Nh, Nw))
        for n in xrange(cls.Ni):
            coorVec = list(itertools.product(range(cls.H - cls.h + 1), repeat=2))
            szVec = np.size(coorVec)
            while szVec > 0:
                token_val = np.random.randint(0,2)
                if token_val == 0:
                    continue
                else:
                    indx_choice = np.random.randint(0,shape(coorVec)[0])
                    coor = coorVec[indx_choice]
                    cls.matAtest[n, coor[0], coor[1]] = 1
                    for i in xrange(-2,3,1):
                        for j in xrange(-2,3,1):
                            if (coor[0] + i, coor[1] + j) in coorVec:
                                coorVec.remove((coor[0] + i, coor[1] + j))
                    szVec = np.size(coorVec)

        # Set up lambda
        if cls.templates == 'random':
            cls.correct_lambdas = np.asarray(np.random.randn(cls.K, cls.Cin, cls.h, cls.w), dtype=theano.config.floatX)
            # cls.correct_lambdas[0] = 0.0*cls.correct_lambdas[0]
        else:
            cls.correct_lambdas = np.asarray(np.random.randn(cls.K, cls.Cin, cls.h, cls.w), dtype=theano.config.floatX)
            path = './templates/*.png'
            indx_img = 1
            for file_dir in glob.glob(path):
                img = misc.imread(file_dir)
                cls.correct_lambdas[indx_img] = np.transpose(misc.imresize(img[:,:,0:3], size=(cls.h, cls.w, cls.Cin)), (2, 0, 1))
                indx_img += 1
                del img
                if indx_img == cls.K:
                    break

        # Set up the correct means
        cls.correct_means = np.asarray(np.abs(np.random.randn(cls.K, cls.Cin, cls.h, cls.w)), dtype=theano.config.floatX)
        cls.correct_means[0] = 0.0*cls.correct_means[0]

        # Set up the correct psis
        # correct_psis_val = np.asarray(np.abs(np.random.randn(1, cls.D)), dtype=theano.config.floatX)
        correct_psis_val = cls.psi_scale*np.asarray(np.ones((1, cls.D)), dtype=theano.config.floatX)
        cls.correct_psis = np.tile(correct_psis_val, reps=(cls.K, 1))

        # Initialize amps (pi)
        cls.correct_pi = np.zeros((cls.K,1),dtype=theano.config.floatX)

        # Initialize the data
        cls.d = np.zeros((cls.Ni, cls.Cin, cls.H, cls.W), dtype=theano.config.floatX)
        cls.dtest = np.zeros((cls.Ni, cls.Cin, cls.H, cls.W), dtype=theano.config.floatX)

        cls.correct_rs = np.zeros((cls.Ni, cls.K, Nh, Nw), dtype=theano.config.floatX)
        cls.correct_z = np.zeros((cls.Ni, cls.K, Nh, Nw), dtype=theano.config.floatX)

        # Synthesize data
        # Note: Haven't added noise to zero patches
        for n in xrange(cls.Ni):
            for hindx in xrange(Nh):
                for windx in xrange(Nw):
                    if cls.matA[n, hindx, windx] > 0:
                        k = np.random.choice(range(cls.K - 1)) + 1
                        cls.correct_rs[n,k,hindx,windx] = 1
                        cls.correct_pi[k] += 1
                        noise_val = np.sqrt(cls.psi_scale)*np.asarray(np.random.randn(cls.D, 1), dtype=theano.config.floatX)
                        # noise_val = np.dot(np.diag(np.sqrt(correct_psis_val).reshape(-1)), noise_val)
                        noise_val = np.reshape(noise_val, newshape=(cls.Cin, cls.h, cls.w))
                        z = np.random.randn()
                        cls.correct_z[n,k,hindx,windx] = z
                        cls.d[n, : , hindx:hindx + cls.h, windx:windx + cls.w] = cls.correct_lambdas[k]*z \
                                                                                 + cls.correct_means[k] + noise_val
                    else:
                        cls.correct_rs[n,0,hindx,windx] = 1

                    if cls.matAtest[n, hindx, windx] > 0:
                        ktest = np.random.choice(range(cls.K - 1)) + 1
                        noise_val_test = np.sqrt(cls.psi_scale)*np.asarray(np.random.randn(cls.D, 1), dtype=theano.config.floatX)
                        # noise_val_test = np.dot(np.diag(np.sqrt(correct_psis_val).reshape(-1)), noise_val_test)
                        noise_val_test = np.reshape(noise_val_test, newshape=(cls.Cin, cls.h, cls.w))
                        cls.dtest[n, : , hindx:hindx + cls.h, windx:windx + cls.w] = cls.correct_lambdas[ktest]*np.random.randn() \
                                                                                     + cls.correct_means[ktest] + noise_val_test

        # Reshape correct_rs
        cls.correct_rs = np.reshape(cls.correct_rs.transpose((1,0,2,3)),newshape=(cls.K,cls.Np*cls.Ni))

        # Reshape correct_z
        cls.correct_z = np.reshape(cls.correct_z.transpose((1,0,2,3)), newshape=(cls.K,cls.M,cls.Np*cls.Ni))

        # Reshape means and lambdas
        cls.correct_means = np.reshape(cls.correct_means, newshape=(cls.K, cls.D))
        cls.correct_lambdas = np.reshape(cls.correct_lambdas, newshape=(cls.K, cls.D, cls.M))

        # Set up lambda covs and total covs
        cls.correct_lambda_covs = np.zeros((cls.K, cls.D, cls.D), dtype=theano.config.floatX)
        cls.correct_covs = np.zeros((cls.K, cls.D, cls.D), dtype=theano.config.floatX)
        for i in xrange(cls.K):
            cls.correct_lambda_covs[i] = np.dot(cls.correct_lambdas[i],(cls.correct_lambdas[i]).T)
            cls.correct_covs[i] = cls.correct_lambda_covs[i] + np.diag(cls.correct_psis[i])

        # compute the correct inverse covariances
        cls.inv_correct_covs = 0. * cls.correct_covs
        if cls.em_mode == 'soft':
            for k in xrange(cls.K):
                cls.inv_correct_covs[k] = cls._invert_cov(k)

        # compute amps (pi)
        cls.correct_pi[0] = np.size(cls.matA) - np.count_nonzero(cls.matA)
        cls.correct_pi = cls.correct_pi/np.size(cls.matA)
        cls.correct_pi = cls.correct_pi.reshape(-1)

        # start EM
        cls.x = T.tensor4('x')

        cls.mix = ConvMofa(x=cls.x, dat=cls.d, K=cls.K, M=cls.M, W=cls.W, H=cls.H, w=cls.w, h=cls.h, Cin=cls.Cin, Ni=cls.Ni,
                           amps_val_init=copy.copy(cls.correct_pi), lambdas_val_init=copy.copy(cls.correct_lambdas), lambda_covs_val_init=None,
                           covs_val_init=None, inv_covs_val_init=None, means_val_init=copy.copy(cls.correct_means), psis_val_init=copy.copy(cls.correct_psis),
                           PPCA=False, lock_psis=True, update_mode=cls.update_mode, em_mode=cls.em_mode,
                           rs_clip = 0.0, max_condition_number=1.e3, init=True,init_ppca=False)

        cls.mix._E_step()
        output_E = theano.function([], [cls.mix.latents, cls.mix.latent_covs, cls.mix.betas, cls.mix.logLs, cls.mix.rs,
                                        cls.mix.rs_conv_new, cls.mix.rs_nonzero_max, cls.mix.rs_zero_new, cls.mix.sumrs,
                                        cls.mix.amps, cls.mix.data, cls.mix.inv_covs, cls.mix.covs,
                                        cls.mix.lambdas, cls.mix.psis, cls.mix.means, cls.mix.lambda_covs, cls.mix.amps_new],
                                   on_unused_input='ignore',
                                   givens={cls.x:cls.d})
        latents, latent_covs, betas, logLs, rs, rs_conv_new, rs_nonzero_max, rs_zero_new, sumrs, amps_val_old, \
        data_val, inv_covs_val_old, covs_val_old, \
        lambdas_val_old, psis_val_old, means_val_old, lambda_covs_val_old, amps_val_new  = output_E()

        cls.mix._M_step()
        output_M = theano.function([], [cls.mix.means_new, cls.mix.lambdas_new, cls.mix.psis_new,
                                            cls.mix.covs_new, cls.mix.inv_covs_new,
                                            cls.mix.lambda_covs_new],
                                         on_unused_input='ignore',
                                         givens={cls.x:cls.d})
        means_val_new, lambdas_val_new, psis_val_new, covs_val_new, inv_covs_val_new, lambda_covs_val_new = output_M()

        cls.correct_zrs = cls.correct_z[:,0,:]*cls.correct_rs
        zrs_val = latents[:,0,:]*rs

        print 'Means Comparison'
        print norm(cls.correct_means - means_val_new)/norm(cls.correct_means)
        print 'Lambdas Comparison'
        print norm(cls.correct_lambdas - lambdas_val_new)/norm(cls.correct_lambdas)
        print 'Psis Comparison'
        print norm(cls.correct_psis - psis_val_new)/norm(cls.correct_psis)
        print 'rs Comparison using norm'
        print norm(cls.correct_rs - rs)/norm(cls.correct_rs)
        print 'rs Comparison using error percentage'
        print float(np.shape((cls.correct_rs - rs).nonzero())[1])/np.size(cls.correct_rs)
        print 'zrs Comparison'
        print norm(cls.correct_z[:,0,:]*cls.correct_rs - latents[:,0,:]*rs)/norm(cls.correct_z[:,0,:]*cls.correct_rs)
        print 'Correct Pi'
        print cls.correct_pi
        print 'Learned Pi'
        print amps_val_new
        print 'Correct Psi'
        # print cls.correct_psis
        print 'Learned Psi'
        # print psis_val_new
        print 'Correct zrs'
        # print cls.correct_zrs[np.nonzero(cls.correct_zrs)]
        print 'Learned zrs'
        # print zrs_val[np.nonzero(zrs_val)]
        print 'No of nonzero in correct rs'
        print np.size(cls.correct_rs[np.nonzero(cls.correct_rs)])
        print 'No of nonzero in rs'
        print np.size(rs[np.nonzero(rs)])
        print np.shape(np.nonzero(np.sum(rs,axis=0)))
        print 'Done'

        # start_time = time.time()
        # cls.LogLs_vec, cls.dLC_vec, cls.dPS_vec, cls.epochs = cls.mix.fit(x=cls.x, dat=cls.d, tol=tol, maxiter=maxiter, verbose=verbose, mode='Param')
        # stop_time = time.time()
        # print 'train takes %f seconds' %(stop_time - start_time)
        #
        # cls.correct_means = np.reshape(cls.correct_means, newshape=(cls.K, cls.D))
        # # get params
        # cls.getParams()
        #
        # # Display some important results
        # print '\nCov Matrix'
        # print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        # print '\nLambdas Matrix'
        # print norm(cls.correct_lambdas - cls.lambdas_val)/norm(cls.correct_lambdas)
        # print '\nPsi Matrix'
        # print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        # print '\nMeans'
        # print norm(cls.correct_means - cls.means_val)/norm(cls.correct_means)
        # print '\nPrint correct Psi matrix'
        # print cls.correct_psis
        # print '\nPrint learned Psi matrix'
        # print cls.psis_val
        # print '\nPrint Correct Lambdas'
        # print cls.correct_lambdas
        # print '\nPrint Learned Lambdas'
        # print cls.lambdas_val
        # print '\nPrint Correct Means'
        # print cls.correct_means
        # print '\nPrint Means'
        # print cls.means_val
        # print '\nPrint Correct Pi'
        # print cls.correct_pi
        # print '\nPrint Learned Pi'
        # print cls.amps_val
        # print '\nDone'

    @classmethod
    def getParams(cls):
        # output learned parameters
        output_final = theano.function([], [cls.mix.means, cls.mix.lambdas, cls.mix.psis,
                                            cls.mix.covs, cls.mix.inv_covs, cls.mix.amps,
                                            cls.mix.lambda_covs, cls.mix.rs, cls.mix.logLs,
                                            cls.mix.latents, cls.mix.sumrs, cls.mix.betas],
                                         on_unused_input='ignore',
                                         givens={cls.x:cls.d})

        cls.means_val, cls.lambdas_val, cls.psis_val, cls.covs_val, cls.inv_covs_val, cls.amps_val, \
        cls.lambda_covs_val, cls.rs_val, cls.logLs_val, cls.latents, cls.sumrs, cls.betas = output_final()

        # Converse the results into numpy arrays
        cls.means_val = np.asarray(cls.means_val,dtype=theano.config.floatX)
        cls.lambdas_val = np.asarray(cls.lambdas_val,dtype=theano.config.floatX)
        cls.psis_val = np.asarray(cls.psis_val,dtype=theano.config.floatX)
        cls.covs_val = np.asarray(cls.covs_val,dtype=theano.config.floatX)
        cls.inv_covs_val = np.asarray(cls.inv_covs_val,dtype=theano.config.floatX)
        cls.amps_val = np.asarray(cls.amps_val,dtype=theano.config.floatX)
        cls.lambda_covs_val = np.asarray(cls.lambda_covs_val,dtype=theano.config.floatX)
        cls.rs_val = np.asarray(cls.rs_val,dtype=theano.config.floatX)
        cls.logLs_val = np.asarray(cls.logLs_val,dtype=theano.config.floatX)
        cls.sumrs = np.asarray(cls.sumrs,dtype=theano.config.floatX)
        cls.betas = np.asarray(cls.betas,dtype=theano.config.floatX)
        cls.latents = np.asarray(cls.latents,dtype=theano.config.floatX)

        # Arrange clusters in a right order
        for i in xrange(cls.K):
            dist_vec = []
            for j in xrange(i, cls.K, 1):
                dist_vec.append(norm(cls.correct_means[i] - cls.means_val[j]))

            right_order = np.argmin(dist_vec) + i

            cls.rs_val[[right_order, i],:] = cls.rs_val[[i, right_order],:]
            cls.means_val[[right_order, i],:] = cls.means_val[[i, right_order],:]
            cls.lambdas_val[[right_order, i],:,:] = cls.lambdas_val[[i, right_order],:,:]
            cls.psis_val[[right_order, i],:] = cls.psis_val[[i, right_order],:]
            cls.covs_val[[right_order, i],:,:] = cls.covs_val[[i, right_order],:,:]
            cls.inv_covs_val[[right_order, i],:,:] = cls.inv_covs_val[[i, right_order],:,:]
            cls.amps_val[[right_order, i]] = cls.amps_val[[i, right_order]]
            cls.lambda_covs_val[[right_order, i],:] = cls.lambda_covs_val[[i, right_order],:]
            cls.logLs_val[[right_order, i]] = cls.logLs_val[[i, right_order]]
            cls.sumrs[[right_order, i]] = cls.sumrs[[i, right_order]]
            cls.betas[[right_order, i],:,:] = cls.betas[[i, right_order],:,:]
            cls.latents[[right_order, i],:,:] = cls.latents[[i, right_order],:,:]

        return None

    def test_useless(cls):
        """
        Compare learned pi with the correct one
        """
        cls.assertLessEqual(1, 10,
                            'This test cannot fail :)')

    def test_loglikelihood(cls):
        """
        Compare the log-likelihood of learned model with the correct one
        """
        cls.assertLessEqual(1, 10,
                            'This test cannot fail :)')

    def test_rank_lambda_covs(cls):
        """
        Compare rank of learned lambda_covs with the correct ones
        """
        cls.assertLessEqual(1, 10,
                            'This test cannot fail :)')

    def test_pi(cls):
        """
        Compare learned pi with the correct one
        """
        cls.assertLessEqual(1, 10,
                            'This test cannot fail :)')

    def test_classification_train_err(cls):
        """
        Compute the classification accuracy on the train set and plot the confusion matrix
        """
        labels = np.argmax(cls.rs_val,0)
        print '\nTrain Labels'
        print cls.correct_labels
        print labels
        accu_score = precision_score(cls.correct_labels, labels, average='micro')
        print 'Accuracy on Train Set is %f' %accu_score

        cfn = confusion_matrix(cls.correct_labels, labels).astype(np.float) # 1st index = true label, 2nd = predicted label

        for i in xrange(cfn.shape[0]):
            cfn[i,:] /= cfn[i,:].sum() # normalize

        imshow(cfn)
        colorbar()
        xlabel('True Label')
        ylabel('Predicted Label')
        title('Confusion Matrix for Train Set - Accu %f' %accu_score)
        savefig('confusion_mtx_accu_train.png')

    def test_classification_test_err(cls):
        """
        Compute the classification accuracy on the test set and plot the confusion matrix
        """
        labels = np.argmax(cls.rs_val_test,0)
        print '\nTest Labels'
        print cls.correct_labels_test
        print labels
        accu_score = precision_score(cls.correct_labels_test, labels, average='micro')
        print 'Accuracy on Test Set is %f' %accu_score

        cfn = confusion_matrix(cls.correct_labels_test, labels).astype(np.float) # 1st index = true label, 2nd = predicted label

        for i in xrange(cfn.shape[0]):
            cfn[i,:] /= cfn[i,:].sum() # normalize

        imshow(cfn)
        colorbar()
        xlabel('True Label')
        ylabel('Predicted Label')
        title('Confusion Matrix for Test Set - Accu %f' %accu_score)
        savefig('confusion_mtx_accu_test.png')
        return None

    @classmethod
    def cal_logLs(cls, covs, means, inv_covs):
        """
        Calculate log likelihoods for each datum
        under each component.
        """
        logrs = np.zeros((cls.K, cls.Nl))
        for k in range(cls.K):
            logrs[k] = np.log(cls.pi[k]) + cls._log_multi_gauss(k=k, covs=covs, means=means, inv_covs=inv_covs, D=cls.correct_d)

        # here lies some ghetto log-sum-exp...
        # nothing like a little bit of overflow to make your day better!
        L = cls._log_sum(logrs)
        return L

if __name__ == '__main__':
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromTestCase(ConvSimpleTestCase)
    runner = unittest.TextTestRunner()
    results = runner.run(suite)

# cls.mix._E_step()
# test_E_function = theano.function([], [cls.mix.logLs, cls.mix.rs, cls.mix.betas, cls.mix.latents, cls.mix.latent_covs],
#                                  updates=[], on_unused_input='ignore',
#                                  givens={cls.x:cls.d})
#
# cls.test_logLs, cls.test_rs, cls.test_betas, cls.test_latents, cls.test_latent_covs = test_E_function()
# cls.mix._M_step()
# updates = []
# updates.append((cls.mix.means, cls.mix.means_new))
# updates.append((cls.mix.lambdas, cls.mix.lambdas_new))
# updates.append((cls.mix.psis, cls.mix.psis_new))
# updates.append((cls.mix.covs, cls.mix.covs_new))
# updates.append((cls.mix.inv_covs, cls.mix.inv_covs_new))
# updates.append((cls.mix.amps, cls.mix.amps_new))
# updates.append((cls.mix.lambda_covs, cls.mix.lambda_covs_new))
# test_M_function = theano.function([], [cls.mix.logLs, cls.mix.rs, cls.mix.betas, cls.mix.latents, cls.mix.latent_covs, cls.mix.psis_new, cls.mix.sumrs],
#                                  updates=updates, on_unused_input='ignore',
#                                  givens={cls.x:cls.d})
# test_logLs, test_rs, test_betas, test_latents, test_latent_covs, test_psis_new, test_sumrs = test_M_function()

# cls.d.astype(np.uint8)
# print 'hehe'
# for i in xrange(20):
#     misc.imsave('TestImage_%i.jpg' % i, cls.d[i])