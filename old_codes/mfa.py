__author__ = 'minhtannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import timeit

from scipy.linalg import norm

from theano.tensor.nlinalg import MatrixPinv, diag, MatrixInverse
from theano.tensor.slinalg import Cholesky

from sklearn.metrics import confusion_matrix, precision_score

import copy

from kmeans_init import kmeans_init
from plot_mofa import *

from mofa import Mofa as Python_Mofa


class Mofa(object):
    """
    EM Algorithm for Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes

    """
    def __init__(self,data, K, M, D, N,
                 means_val_init, psis_val_init, amps_val_init=None,
                 lambdas_val_init=None, lambda_covs_val_init=None,
                 covs_val_init=None, inv_covs_val_init=None,
                 PPCA=False,lock_psis=True,
                 update_mode='Hinton',
                 rs_clip = 0.0,
                 max_condition_number=1.e3,
                 init=True,init_ppca=False):

        # required
        np.random.seed(2)
        self.psis_val_init = psis_val_init
        self.K     = K
        self.M     = M
        self.data  = data
        self.dataT = self.data.T # INSANE DATA DUPLICATION
        self.N     = N
        self.D     = D
        self.update_mode  = update_mode

        # options
        self.PPCA                 = PPCA
        self.lock_psis            = lock_psis
        self.rs_clip              = rs_clip
        self.max_condition_number = max_condition_number
        assert rs_clip >= 0.0

        # empty arrays to be filled
        self.betas       = theano.shared(value=np.zeros((self.K,self.M,self.D), dtype=theano.config.floatX),
                                         name='betas', borrow=True)
        self.latents     = theano.shared(value=np.zeros((self.K,self.M,self.N), dtype=theano.config.floatX),
                                         name='latents', borrow=True)
        self.latent_covs = theano.shared(value=np.zeros((self.K,self.M,self.M,self.N), dtype=theano.config.floatX),
                                         name='latent_covs', borrow=True)
        self.means       = theano.shared(value=np.asarray(means_val_init, dtype=theano.config.floatX), name='means', borrow=True)
        self.rs          = theano.shared(value=np.zeros((self.K,self.N), dtype=theano.config.floatX),
                                         name='rs', borrow=True)

        # initialize
        # if init = True, use _initialize to initialize the parameters.
        # otherwise, use ..._val_init from the input list
        if init:
            self._initialize(init_ppca)
        else:
            self.amps = theano.shared(value=np.asarray(amps_val_init, dtype=theano.config.floatX), name='amps', borrow=True)
            self.lambdas = theano.shared(value=np.asarray(lambdas_val_init, dtype=theano.config.floatX), name='lambdas', borrow=True)
            self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val_init, dtype=theano.config.floatX), name='lambdas_covs', borrow=True)
            self.covs = theano.shared(value=np.asarray(covs_val_init, dtype=theano.config.floatX), name='covs', borrow=True)
            self.inv_covs = theano.shared(value=np.asarray(inv_covs_val_init, dtype=theano.config.floatX), name='inv_covs', borrow=True)

    def _initialize(self, init_ppca, maxiter=200, tol=1e-4):
        """
        Run K-means
        """

        # Randomly assign factor loadings
        lambdas_value = np.random.randn(self.K,self.D,self.M) / \
            np.sqrt(self.max_condition_number)
        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas', borrow=True)

        # Set (high rank) variance to variance of all data, along a dimension
        self.psis= theano.shared(value=np.asarray(self.psis_val_init, dtype=theano.config.floatX), name='psis', borrow=True)

        # Set initial lambda_covs, covs and compute inv_covs
        covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        lambda_covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        inv_covs_val = 0. * covs_val

        # compute the inv_covs using matrix inversion lemma
        for k in range(self.K):
            covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T) + \
                np.diag(self.psis_val_init[k])
            lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
            psiI = inv(np.diag(self.psis_val_init[k]))
            lam  = lambdas_value[k]
            lamT = lam.T
            step = inv(np.eye(self.M) + np.dot(lamT,np.dot(psiI,lam)))
            step = np.dot(step,np.dot(lamT,psiI))
            step = np.dot(psiI,np.dot(lam,step))
            inv_covs_val[k] = psiI - step

        self.covs = theano.shared(value=np.asarray(covs_val, dtype=theano.config.floatX),
                                  name='covs', borrow=True)

        self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val, dtype=theano.config.floatX),
                                  name='lambda_covs', borrow=True)

        self.inv_covs = theano.shared(value=np.asarray(inv_covs_val, dtype=theano.config.floatX),
                                  name='inv_covs', borrow=True)

        # Randomly assign the amplitudes.
        amps_val = np.random.rand(self.K)
        amps_val /= np.sum(amps_val)
        self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                  name='amps', borrow=True)

        # print 'Lambda Init'
        # print np.shape(self.lambdas.eval())
        # print self.lambdas.eval()[0:2]
        # print 'Covs_Init'
        # print self.covs.eval()[0:2]
        # print 'Lambda_Covs'
        # print self.lambda_covs.eval()[0:2]
        print 'Inv_Covs'
        print self.inv_covs.eval()
        # print 'Amps'
        # print self.amps.eval()

    def take_EM_step(self):
        """
        Do one E step and then do one M step.
        """
        self._E_step()
        self.cost = -T.mean(self.logLs)/self.D
        self._M_step()

    def _E_step(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """
        # compute the responsibilities and log-likelihood using eq. 12
        self.logLs, self.rs = self._calc_probs()

        # compute the lambda dagger
        self.betas, updates4 = theano.scan(fn=lambda l, ic: T.dot(l.T, ic),
                                 outputs_info=None,
                                 sequences=[self.lambdas, self.inv_covs])

        # compute E[z|x] using eq. 13
        self.latents, updates5 = theano.scan(fn=lambda b, m: T.dot(b, self.dataT - m[:, None]),
                                   outputs_info=None,
                                   sequences=[self.betas, self.means])

        # compute E[zz'|x] using eq. 14
        # step 1 computes E[z|x](E[z|x])'
        # step 2 computes beta_j*lambda_j
        step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
                                     outputs_info=None,
                                     sequences=self.latents)
        step2, updates7 = theano.scan(fn=lambda b, l: T.dot(b, l),
                                      outputs_info=None,
                                      sequences=[self.betas, self.lambdas])
        self.latent_covs, updates8 = theano.scan(fn=lambda s2, s1: T.eye(self.M,self.M)[:,:,None] - s2[:,:,None] + s1,
                                       outputs_info=None,
                                       sequences=[step2, step1])

        # compute the pi's
        self.sumrs = T.sum(self.rs,axis=1)
        self.amps_new, updates18 = theano.scan(fn=lambda s: s / float(self.N),
                                outputs_info=None,
                                sequences=self.sumrs)

    def _M_step(self):
        """
        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """
        if self.update_mode == 'Ross_Fadely':
            # update means - implicitly included in eq. 15
            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[self.lambdas, self.latents])

            self.means_new, updates10 = theano.scan(fn=lambda resp, llt, s: T.sum(resp * (self.dataT - llt), axis=1) / s,
                                     outputs_info=None,
                                     sequences=[self.rs, lambdalatents, self.sumrs])

            # update lambdas using eq.15
            zeroed, updates11 = theano.scan(fn=lambda m: self.dataT - m[:,None],
                                 outputs_info=None,
                                 sequences=self.means_new)

            self.lambdas_new, updates12 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixPinv()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[self.latents, self.rs, self.latent_covs, zeroed])

            # update psis using eq. 16
            psis, updates13 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                               outputs_info=None,
                               sequences=[self.rs, zeroed, lambdalatents, self.sumrs])

        else:
            latents_tilde = T.concatenate([self.latents,
                                           theano.shared(value=np.ones((self.K, 1 ,self.N),
                                                                       dtype=theano.config.floatX),borrow=True)],
                                          axis=1)
            col1 = T.concatenate([self.latent_covs, (self.latents[:,:,None,:]).dimshuffle((0,2,1,3))], axis=1)
            latent_covs_tilde = T.concatenate([col1, latents_tilde[:,:,None,:]], axis=2)
            dataT = T.tile(self.dataT[None,:,:], reps=[self.K,1,1])
            lambdas_tilde, updates22 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixPinv()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[latents_tilde, self.rs, latent_covs_tilde, dataT])
            self.lambdas_new = lambdas_tilde[:,:,0:self.M]
            self.means_new = lambdas_tilde[:,:,self.M]
            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[lambdas_tilde, latents_tilde])
            psis, update23 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                                         outputs_info=None,
                                         sequences=[self.rs, dataT, lambdalatents, self.sumrs])

        maxpsi, updates14 = theano.scan(fn=lambda p: T.max(p),outputs_info=None, sequences=psis)

        maxlam, updates15 = theano.scan(fn=lambda l: T.max(T.sum(l * l, axis=0)),
                             outputs_info=None,
                             sequences=self.lambdas_new)
        minpsi, updates16 = theano.scan(fn=lambda maxp, maxl: T.max([maxp, maxl]) / self.max_condition_number,
                             outputs_info=None,
                             sequences=[maxpsi, maxlam])
        self.psis_new, updates17   = theano.scan(fn=lambda p, minp: T.clip(p, minp, np.Inf),
                                  outputs_info=None,
                                  sequences=[psis, minpsi])

        # make all elements in each rows of psis the same
        if self.PPCA:
            self.psis_new, updates19 = theano.scan(fn=lambda psn: T.mean(psn)*T.ones(self.D),
                                                   outputs_info=None,
                                                   sequences=self.psis_new)
        # make psi for each cluster the same
        if self.lock_psis:
            self.psis_new = T.dot(self.sumrs,self.psis_new)/T.sum(self.sumrs)
            self.psis_new = (self.psis_new).dimshuffle('x',0)
            self.psis_new = T.tile(self.psis_new,(self.K, 1))

        # update covs
        self._update_covs()

    def _update_covs(self):
        """
        Update self.cov for responsibility, logL calc
        """

        # total cov = lambda*lambda' + psi
        self.lambda_covs_new, updates20 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas_new])
        self.covs_new, updates1 = theano.scan(fn=lambda lc, p: lc + diag(p),
                                          outputs_info=None,
                                          sequences=[self.lambda_covs_new, self.psis_new])

        # compute the inv_covs using the matrix inversion lemma
        self.inv_covs_new, updates2 = theano.scan(fn=lambda l, p: MatrixInverse()(diag(p)) - T.dot(MatrixInverse()(diag(p)),T.dot(l,T.dot(MatrixInverse()(T.eye(self.M) + T.dot(T.transpose(l),T.dot(MatrixInverse()(diag(p)),l))),T.dot(T.transpose(l),MatrixInverse()(diag(p)))))),
                                          outputs_info=None,
                                          sequences=[self.lambdas_new, self.psis_new])

    def _calc_probs(self):
        """
        Calculate log likelihoods, responsibilites for each datum
        under each component.
        """

        logrs, updates3 = theano.scan(fn=lambda a, c, m, ic: T.log(a) - float(0.5 * np.log(2 * np.pi) * self.D) - 0.5 * 2 * T.sum(T.log(diag(Cholesky()(c)))) - 0.5 * T.sum((self.data - m).T * T.dot(ic, (self.data - m).T), axis=0),
                            outputs_info=None,
                            sequences=[self.amps, self.covs, self.means, self.inv_covs])

        L = self._log_sum(logrs)
        logrs -= L[None, :]
        if self.rs_clip > 0.0:
            logrs = T.clip(logrs,T.log(self.rs_clip),np.Inf)

        return L, T.exp(logrs)

    def _log_sum(self,loglikes):
        """
        Calculate sum of log likelihoods
        """
        a = T.max(loglikes, axis=0)
        return a + T.log(T.sum(T.exp(loglikes - a[None, :]), axis=0))

    def fit(self, x, dat, tol, maxiter, verbose, mode='NLL'):
        """
        Train the model using the whole dataset
        """
        # take one E step and then one M step
        self.take_EM_step()

        # updates all parameters after each run
        updates = []
        updates.append((self.means, self.means_new))
        updates.append((self.lambdas, self.lambdas_new))
        updates.append((self.psis, self.psis_new))
        updates.append((self.covs, self.covs_new))
        updates.append((self.inv_covs, self.inv_covs_new))
        updates.append((self.amps, self.amps_new))
        updates.append((self.lambda_covs, self.lambda_covs_new))

        # construct a function that infers the z, zz' and responsibilities in the E step and then uses updates
        # to update lambdas, means, psis, ... (see update list above)
        output_one_iter = theano.function([], [self.logLs, self.rs, self.betas, self.latents, self.latent_covs, self.psis_new, self.sumrs],
                                         updates=updates, on_unused_input='ignore',
                                         givens={x:dat})

        # start timing
        start_time = timeit.default_timer()

        # run EM
        L = None
        LogLs_vec = []
        epochs = []
        LC = copy.copy(self.lambda_covs.get_value())
        PS = copy.copy(self.psis.get_value())
        LogLs_vec = []
        dPS_vec = []
        dLC_vec = []
        for i in xrange(maxiter):
            logLs_val, rs_val, betas_val, latents_val, latent_covs_val, psis_val, sumrs_val = output_one_iter()
            newL = np.sum(logLs_val)
            # print initial log-likelihood
            if i == 0 and verbose:
                print("Initial NLL=", -newL)
            newLC = self.lambda_covs.get_value()
            newPS = self.psis.get_value()
            dLC = norm(newLC - LC)/norm(LC)
            dPS = norm(newPS - PS)/norm(PS)
            # stopping
            if mode == 'NLL':
                if L!=None:
                    dL = np.abs((newL-L)/L)
                    if i > 5 and dL < tol:
                        break
                L = newL
            else:
                if i > 5 and dLC < tol and dPS < tol:
                    break

            LC = copy.copy(newLC)
            PS = copy.copy(newPS)
            dLC_vec.append(dLC)
            dPS_vec.append(dPS)
            LogLs_vec.append(-newL)
            epochs.append(i)

        # let us know if EM converges or not and after how many epochs
        if i < maxiter - 1:
            if verbose:
                print("EM converged after {0} iterations".format(i))
                print("Final NLL={0}".format(-newL))
        else:
            print("Warning:EM didn't converge after {0} iterations".format(i))

        # stop timing #
        print '\n'
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print '\n'

        return LogLs_vec, dLC_vec, dPS_vec, epochs

    def batch_fit(self, x, dat, K, batch_size, tol, max_epochs, verbose, mode='NLL'):
        D, N = np.shape(dat)
        n_batches = N/batch_size
        index = T.iscalar()

        self.take_EM_step()

        updates = []
        updates.append((self.means, self.means_new))
        updates.append((self.lambdas, self.lambdas_new))
        updates.append((self.psis, self.psis_new))
        updates.append((self.covs, self.covs_new))
        updates.append((self.inv_covs, self.inv_covs_new))
        updates.append((self.amps, self.amps_new))
        updates.append((self.lambda_covs, self.lambda_covs_new))

        shared_dat = theano.shared(dat)

        output_one_iter = theano.function([index], [self.logLs, self.rs, self.betas, self.latents,
                                                    self.latent_covs, self.sumrs,
                                                    self.means_new, self.covs_new, self.inv_covs_new,
                                                    self.lambdas_new, self.amps_new, self.psis_new, self.lambda_covs_new, self.data],
                                          updates=updates, on_unused_input='ignore',
                                          givens={x: shared_dat[:,index * batch_size: (index + 1) * batch_size]})

        start_time = timeit.default_timer()


        done_looping = False
        L = None
        LogLs_vec = []
        epochs = []
        epoch = -1
        LC = copy.copy(self.lambda_covs.get_value())
        PS = copy.copy(self.psis.get_value())
        LogLs_vec = []
        dPS_vec = []
        dLC_vec = []

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1
            for minibatch_index in xrange(n_batches):
                logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, \
                means_val, covs_val, inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, data_val \
                    = output_one_iter(minibatch_index)

            logLs_val_whole_dataset = cal_logLs(K=K, N=N, D=D, data=dat,
                                                covs=covs_val, means=means_val, inv_covs=inv_covs_val, pi=amps_val)
            newL = np.mean(logLs_val_whole_dataset)/D
            if epoch == 0 and verbose:
                print("Initial NLL=", -newL)

            newLC = self.lambda_covs.get_value()
            newPS = self.psis.get_value()
            dLC = norm(newLC - LC)/norm(LC)
            dPS = norm(newPS - PS)/norm(PS)

            if mode == 'NLL':
                if L!=None:
                    dL = np.abs((newL-L)/L)
                    if epoch > 5 and dL < tol:
                        done_looping = True
                L = newL
            else:
                if epoch > 5 and dLC < tol and dPS < tol:
                    break

            LC = copy.copy(newLC)
            PS = copy.copy(newPS)
            LogLs_vec.append(-newL)
            epochs.append(epoch)
            dLC_vec.append(dLC)
            dPS_vec.append(dPS)

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(-newL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print '\n'
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print '\n'

        return logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, \
               means_val, covs_val, inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, LogLs_vec, epochs, \
               dLC_vec, dPS_vec

    def predict(self, x, dat):
        """
        Predict the labels of new data
        """
        self._E_step()
        getResults = theano.function([], [self.rs, self.means],
                                         updates=[], on_unused_input='ignore',
                                         givens={x:dat})
        rs_val, means_val = getResults()
        labels = np.argmax(rs_val,0)
        return labels, means_val, rs_val

##############################################################################################################
# Test Code #
##############################################################################################################
import unittest

class BaseTestCase(unittest.TestCase):
    """
    Unittests for MOFA with 2 clusters.

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls):
        """
        Run before tests are executed.

        Synthesize data and run EM on the synthetic data
        """

        # set internal parameters
        maxiter = 800
        cls.PPCA = False
        cls.lock_psis = True
        tol = 1e-10
        verbose = True
        cls.N = 500
        cls.K = 2
        cls.D = 3
        cls.M = 1

        # set random seed
        cls.seed = 2
        np.random.seed(cls.seed)

        # make data
        correct_means = np.concatenate((2.0*np.ones((1,cls.D),dtype=theano.config.floatX),
                                        13.0*np.ones((1,cls.D),dtype=theano.config.floatX)))
        correct_lambdas = np.asarray([np.random.randn(cls.D, cls.M), np.random.randn(cls.D, cls.M)],
                                         dtype=theano.config.floatX)

        cls.makeData(correct_means=correct_means, correct_lambdas=correct_lambdas)

        #################################################################
        # start building and training the model
        #################################################################

        # x is the symbolic variable for data
        cls.x = T.matrix('x')

        # make a MOFA instance
        cls.mix = Mofa(cls.x.T, K=cls.K, M=cls.M, D=cls.D, N=cls.N, means_val_init=cls.means_val_init, psis_val_init=cls.psis_val_init, PPCA=cls.PPCA, lock_psis=cls.lock_psis, init=True, update_mode='Hinton')

        # output the initializations
        output_init = theano.function([cls.x] , [cls.mix.means, cls.mix.covs, cls.mix.psis], on_unused_input='ignore')
        cls.means_val_init, cls.covs_val_init, cls.psis_val_init = output_init(cls.d)

        cls.LogLs_vec, cls.dLC_vec, cls.dPS_vec, cls.epochs = cls.mix.fit(x=cls.x, dat=cls.d, tol=tol, maxiter=maxiter, verbose=verbose, mode='Param')

        # get params
        cls.getParams()

        # get prediction
        cls.getPredict()

        # Display some important results
        # Optional: solely used for testing purposes
        print 'Display Testing Result for Theano_Mofa'
        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.logLs_val)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.logLs_val.sum())/(-cls.logLs.sum())
        print '\nCov Matrix'
        print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        print '\nLambdas Cov Matrix'
        print norm(cls.correct_lambda_covs - cls.lambda_covs_val)/norm(cls.correct_lambda_covs)
        print '\nPsi Matrix'
        print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        print '\nPi Vector'
        print norm(cls.pi - cls.amps_val, ord=1)/norm(cls.pi, ord=1)

        # Compare with Python_Mofa
        cls.mix_python = Python_Mofa(cls.d.T, cls.K, cls.M, update_mode='Hinton', PPCA=cls.PPCA, lock_psis=cls.lock_psis, init_ppca=False)

        cls.mix_python.run_em(mode='Param')

        for i in xrange(cls.K):
            dist_vec = []
            for j in xrange(i, cls.K, 1):
                dist_vec.append(norm(cls.correct_means[i] - cls.mix_python.means[j]))

            right_order = np.argmin(dist_vec) + i

            cls.mix_python.rs[[right_order, i],:] = cls.mix_python.rs[[i, right_order],:]
            cls.mix_python.means[[right_order, i],:] = cls.mix_python.means[[i, right_order],:]
            cls.mix_python.lambdas[[right_order, i],:,:] = cls.mix_python.lambdas[[i, right_order],:,:]
            cls.mix_python.psis[[right_order, i],:] = cls.mix_python.psis[[i, right_order],:]
            cls.mix_python.covs[[right_order, i],:,:] = cls.mix_python.covs[[i, right_order],:,:]
            cls.mix_python.inv_covs[[right_order, i],:,:] = cls.mix_python.inv_covs[[i, right_order],:,:]
            cls.mix_python.amps[[right_order, i]] = cls.mix_python.amps[[i, right_order]]
            cls.mix_python.lambda_covs[[right_order, i],:] = cls.mix_python.lambda_covs[[i, right_order],:]
            cls.mix_python.logLs[[right_order, i]] = cls.mix_python.logLs[[i, right_order]]

        print '\nDisplay Testing Result for Python_Mofa'
        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.mix_python.logLs)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.mix_python.logLs.sum())/(-cls.logLs.sum())
        print '\nCov Matrix'
        print norm(cls.correct_covs - cls.mix_python.covs)/norm(cls.correct_covs)
        print '\nLambdas Cov Matrix'
        print norm(cls.correct_lambda_covs - cls.mix_python.lambda_covs)/norm(cls.correct_lambda_covs)
        print '\nPsi Matrix'
        print norm(cls.correct_psis - cls.mix_python.psis)/norm(cls.correct_psis)
        print '\nPi Vector'
        print norm(cls.pi - cls.mix_python.amps, ord=1)/norm(cls.pi, ord=1)

    @classmethod
    def tearDownClass(cls, PlotFig = False):
        """
        Run after all tests are finished

        Plot the results
        """
        if PlotFig:
            ########################
            # plot the original data
            ########################
            fig1=pl.figure()
            pl.plot(cls.means_val_init[:,0],cls.means_val_init[:,1],'rx',ms=15,label='Initialization')
            plot_2d_ellipses(0,1, cls.means_val_init, cls.covs_val_init, cls.K, edgecolor='r')

            # find the clusters and plot data points in each cluster
            indx0 = np.nonzero(cls.correct_labels==0)
            indx1 = np.nonzero(cls.correct_labels==1)

            pl.plot(cls.d[0,indx0[0][0:500]], cls.d[1,indx0[0][0:500]],'yo',alpha=0.25)
            pl.plot(cls.d[0,indx1[0][0:500]], cls.d[1,indx1[0][0:500]],'mo',alpha=0.25)
            fig1.savefig('original_data_%i.png' % cls.seed)

            ########################
            # plot the results
            ########################
            fig2=pl.figure()
            # plot initializations
            pl.plot(cls.means_val_init[:,0],cls.means_val_init[:,1],'rx',ms=15,label='Initialization')
            plot_2d_ellipses(0,1, cls.means_val_init, cls.covs_val_init, cls.K, edgecolor='r')

            # find the clusters and plot data points in each cluster
            indx = np.argmax(cls.rs_val,0)
            indx0 = np.nonzero(indx==0)
            indx1 = np.nonzero(indx==1)

            pl.plot(cls.d[0,indx0[0][0:500]], cls.d[1,indx0[0][0:500]],'yo',alpha=0.25)
            pl.plot(cls.d[0,indx1[0][0:500]], cls.d[1,indx1[0][0:500]],'mo',alpha=0.25)

            # plot final results
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.covs_val, K=2, edgecolor='g')
            plot_2d_ellipses_psis(0,1, means=cls.means_val, psis=cls.psis_val, K=2, edgecolor='k')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.lambda_covs_val, K=2, edgecolor='b')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.correct_lambda_covs, K=2, edgecolor='c')
            plot_2d_ellipses_psis(0,1, means=cls.means_val, psis=cls.correct_psis, K=2, edgecolor='c')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.correct_covs, K=2, edgecolor='c')
            pl.plot(cls.means_val[:,0],cls.means_val[:,1],'gx',ms=15,label='Psi fixed')
            pl.title(r'Data $(D, N) = ({0}, {1})$, Model $(K, M) = ({2}, {3})$'.format(cls.D,cls.N,cls.K,cls.M))
            pl.legend()
            pl.xlim(cls.xmin, cls.xmax)
            pl.ylim(cls.ymin, cls.ymax)
            fig2.savefig('results_%i.png' % cls.seed)

        cls.mix = None

    def test_loglikelihood(cls):
        """
        Compare the log-likelihood of learned model with the correct one
        """
        cls.assertLessEqual(norm(cls.logLs - cls.logLs_val)/norm(cls.logLs),1e-2,
                            'incorrect log-likelihood vector')
        cls.assertLessEqual(np.abs(-cls.logLs.sum() + cls.logLs_val.sum())/(-cls.logLs.sum()),1e-2,
                            'incorrect negative log-likelihood value')

    def test_rank_lambda_covs(cls):
        """
        Compare rank of learned lambda_covs with the correct ones
        """
        for i in xrange(cls.K):
            cls.assertLessEqual(np.linalg.matrix_rank(cls.lambda_covs_val[i], tol=1e-1), cls.M,
                             'incorrect rank of lambda covariances')

    def test_pi(cls):
        """
        Compare learned pi with the correct one
        """
        cls.assertLessEqual(norm(cls.pi - cls.amps_val, ord=1)/norm(cls.pi, ord=1),1e-2,
                            'incorrect pi vector')

    def test_convergence(cls):
        """
        Plot loglikelihood vs epochs
        """
        fig1 = figure()
        plot(cls.epochs, cls.LogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_from_my_MFA')
        fig1.savefig('Convergence_of_Negative_LogLikelihood_from_my_MFA.png')

        fig2 = figure()
        plot(cls.epochs, cls.dLC_vec, 'bo-', label='Lambda_Covs')
        plot(cls.epochs, cls.dPS_vec, 'go-', label='Psis')
        legend()
        xlabel('Epoch')
        ylabel('Relative Difference')
        title('Convergence_of_Parameters_from_my_MFA')
        fig2.savefig('Convergence_of_Parameters_from_my_MFA.png')


    def test_classification_train_err(cls):
        """
        Compute the classification accuracy on the train set and plot the confusion matrix
        """
        labels = np.argmax(cls.rs_val,0)
        accu_score = precision_score(cls.correct_labels, labels, average='micro')
        print 'Accuracy on Train Set is %f' %accu_score

        cfn = confusion_matrix(cls.correct_labels, labels).astype(np.float) # 1st index = true label, 2nd = predicted label

        for i in xrange(cfn.shape[0]):
            cfn[i,:] /= cfn[i,:].sum() # normalize

        imshow(cfn)
        colorbar()
        xlabel('True Label')
        ylabel('Predicted Label')
        title('Confusion Matrix for Train Set - Accu %f' %accu_score)
        savefig('confusion_mtx_accu_train.png')

    def test_classification_test_err(cls):
        """
        Compute the classification accuracy on the test set and plot the confusion matrix
        """
        labels = np.argmax(cls.rs_val_test,0)
        accu_score = precision_score(cls.correct_labels_test, labels, average='micro')
        print 'Accuracy on Test Set is %f' %accu_score

        cfn = confusion_matrix(cls.correct_labels_test, labels).astype(np.float) # 1st index = true label, 2nd = predicted label

        for i in xrange(cfn.shape[0]):
            cfn[i,:] /= cfn[i,:].sum() # normalize

        imshow(cfn)
        colorbar()
        xlabel('True Label')
        ylabel('Predicted Label')
        title('Confusion Matrix for Test Set - Accu %f' %accu_score)
        savefig('confusion_mtx_accu_test.png')
        return None

    @classmethod
    def makeData(cls, correct_means, correct_lambdas):
        # create means for the clusters
        cls.correct_means = correct_means

        # create the latent for each cluster
        # in this test, two clusters share the same latent
        cls.z = np.asarray(np.random.randn(cls.M,cls.N/cls.K),dtype=theano.config.floatX)

        # create the latents for test set
        cls.ztest = np.asarray(np.random.randn(cls.M,cls.N/cls.K),dtype=theano.config.floatX)

        # create lambdas and compute the lambda_covs
        cls.correct_lambdas = correct_lambdas
        cls.correct_lambda_covs = np.zeros((cls.K, cls.D, cls.D),dtype=theano.config.floatX)
        for i in xrange(2):
            cls.correct_lambda_covs[i] = np.dot(cls.correct_lambdas[i],(cls.correct_lambdas[i]).T)

        # create psis (the measurement noise)
        cls.correct_psis = np.asarray([range(1, cls.D + 1,1), range(1, cls.D + 1,1)], dtype=theano.config.floatX)

        # compute the total covariance
        # cov = lambda_cov + diag(psi)
        cls.correct_cov1 = cls.correct_lambda_covs[0] + np.diag(cls.correct_psis[0])
        cls.correct_cov2 = cls.correct_lambda_covs[1] + np.diag(cls.correct_psis[1])
        cls.correct_covs = np.asarray([cls.correct_cov1,cls.correct_cov2])

        # compute the inv_covs
        cls.inv_correct_covs = 0.*cls.correct_covs
        for k in xrange(cls.K):
            cls.inv_correct_covs[k] = cls._invert_cov(k)

        # create pi
        pi = (1./float(cls.K))*np.ones((cls.K,1),dtype=theano.config.floatX)
        cls.pi = np.asarray(pi,dtype=theano.config.floatX)

        # synthesize training set
        d = np.dot(cls.correct_lambdas[1], cls.z) + np.tile(cls.correct_means[1], (cls.N/cls.K,1)).T
        b = np.dot(cls.correct_lambdas[0], cls.z) + np.tile(cls.correct_means[0], (cls.N/cls.K,1)).T
        d = np.concatenate((b,d),axis=1)
        d = d + np.dot(np.diag(np.sqrt(cls.correct_psis[1])),np.asarray(np.random.randn(cls.D,cls.N),dtype=theano.config.floatX))
        cls.d = d

        permuted_index = np.random.permutation(cls.N)
        cls.d[:, [range(cls.N), permuted_index]] = cls.d[:, [permuted_index, range(cls.N)]]

        # synthesize test set
        dtest = np.dot(cls.correct_lambdas[1], cls.ztest) + np.tile(cls.correct_means[1], (cls.N/cls.K,1)).T
        btest = np.dot(cls.correct_lambdas[0], cls.ztest) + np.tile(cls.correct_means[0], (cls.N/cls.K,1)).T
        dtest = np.concatenate((btest,dtest),axis=1)
        dtest = dtest + np.dot(np.diag(np.sqrt(cls.correct_psis[1])),np.asarray(np.random.randn(cls.D,cls.N),dtype=theano.config.floatX))
        cls.dtest = dtest
        permuted_index_test = np.random.permutation(cls.N)
        cls.dtest[:, [range(cls.N), permuted_index_test]] = cls.dtest[:, [permuted_index_test, range(cls.N)]]

        # create train labels
        cls.correct_labels = np.zeros((cls.N/cls.K,1),dtype=theano.config.floatX)
        cls.correct_labels = np.concatenate((cls.correct_labels, np.ones((cls.N/cls.K,1),dtype=theano.config.floatX)), axis=0)
        cls.correct_labels[[range(cls.N), permuted_index],:] = cls.correct_labels[[permuted_index, range(cls.N)],:]

        # create test labels
        cls.correct_labels_test = np.zeros((cls.N/cls.K,1),dtype=theano.config.floatX)
        cls.correct_labels_test = np.concatenate((cls.correct_labels_test, np.ones((cls.N/cls.K,1),dtype=theano.config.floatX)), axis=0)
        cls.correct_labels_test[[range(cls.N), permuted_index_test],:] = cls.correct_labels_test[[permuted_index_test, range(cls.N)],:]

        # bounds for plots
        cls.xmin, cls.xmax = np.min(d[0]), np.max(d[0])
        cls.ymin, cls.ymax = np.min(d[1]), np.max(d[1])

        # compute the log-likelihood
        cls.logLs = cls.cal_logLs(covs=cls.correct_covs, means=cls.correct_means, inv_covs=cls.inv_correct_covs)

        # initialize means for the EM algorithm
        init_obj = kmeans_init((cls.d).T, cls.K, cls.N)
        init_obj.compute_kmeans()
        cls.means_val_init = (init_obj.means).get_value()

        # initialize psis
        cls.psis_val_init=np.tile(np.var((cls.d).T,axis=0)[None,:],(cls.K,1))

    @classmethod
    def getParams(cls):
        # output learned parameters
        output_final = theano.function([], [cls.mix.means, cls.mix.lambdas, cls.mix.psis,
                                            cls.mix.covs, cls.mix.inv_covs, cls.mix.amps,
                                            cls.mix.lambda_covs, cls.mix.rs, cls.mix.logLs],
                                         on_unused_input='ignore',
                                         givens={cls.x:cls.d})

        cls.means_val, cls.lambdas_val, cls.psis_val, cls.covs_val, cls.inv_covs_val, cls.amps_val, \
        cls.lambda_covs_val, cls.rs_val, cls.logLs_val = output_final()

        # Converse the results into numpy arrays
        cls.means_val = np.asarray(cls.means_val,dtype=theano.config.floatX)
        cls.lambdas_val = np.asarray(cls.lambdas_val,dtype=theano.config.floatX)
        cls.psis_val = np.asarray(cls.psis_val,dtype=theano.config.floatX)
        cls.covs_val = np.asarray(cls.covs_val,dtype=theano.config.floatX)
        cls.inv_covs_val = np.asarray(cls.inv_covs_val,dtype=theano.config.floatX)
        cls.amps_val = np.asarray(cls.amps_val,dtype=theano.config.floatX)
        cls.lambda_covs_val = np.asarray(cls.lambda_covs_val,dtype=theano.config.floatX)
        cls.rs_val = np.asarray(cls.rs_val,dtype=theano.config.floatX)
        cls.logLs_val = np.asarray(cls.logLs_val,dtype=theano.config.floatX)

        # Arrange clusters in a right order
        for i in xrange(cls.K):
            dist_vec = []
            for j in xrange(i, cls.K, 1):
                dist_vec.append(norm(cls.correct_means[i] - cls.means_val[j]))

            right_order = np.argmin(dist_vec) + i

            cls.rs_val[[right_order, i],:] = cls.rs_val[[i, right_order],:]
            cls.means_val[[right_order, i],:] = cls.means_val[[i, right_order],:]
            cls.lambdas_val[[right_order, i],:,:] = cls.lambdas_val[[i, right_order],:,:]
            cls.psis_val[[right_order, i],:] = cls.psis_val[[i, right_order],:]
            cls.covs_val[[right_order, i],:,:] = cls.covs_val[[i, right_order],:,:]
            cls.inv_covs_val[[right_order, i],:,:] = cls.inv_covs_val[[i, right_order],:,:]
            cls.amps_val[[right_order, i]] = cls.amps_val[[i, right_order]]
            cls.lambda_covs_val[[right_order, i],:] = cls.lambda_covs_val[[i, right_order],:]
            cls.logLs_val[[right_order, i]] = cls.logLs_val[[i, right_order]]

        return None

    @classmethod
    def getPredict(cls):
        """
        rearrange means_val_test and rs_val_test
        """
        cls.labels_test, cls.means_val_test, cls.rs_val_test = cls.mix.predict_error(x=cls.x, dat=cls.dtest)
        cls.means_val_test = np.asarray(cls.means_val_test,dtype=theano.config.floatX)
        cls.rs_val_test = np.asarray(cls.rs_val_test,dtype=theano.config.floatX)
        for i in xrange(cls.K):
            dist_vec = []
            for j in xrange(i, cls.K, 1):
                dist_vec.append(norm(cls.correct_means[i] - cls.means_val_test[j]))

            right_order = np.argmin(dist_vec) + i

            cls.rs_val_test[[right_order, i],:] = cls.rs_val_test[[i, right_order],:]
            cls.means_val_test[[right_order, i],:] = cls.means_val_test[[i, right_order],:]

    @classmethod
    def _invert_cov(cls,k):
        """
        Calculate inverse covariance of mofa or ppca model,
        using inversion lemma
        """
        psiI = inv(np.diag(cls.correct_psis[k]))
        lam  = cls.correct_lambdas[k]
        lamT = lam.T
        step = inv(np.eye(cls.M) + np.dot(lamT,np.dot(psiI,lam)))
        step = np.dot(step,np.dot(lamT,psiI))
        step = np.dot(psiI,np.dot(lam,step))
        return psiI - step

    #######################################################################
    # set of methods used to compute the log-likehood of the correct model
    #
    # copy the comments from the mofa.py by Ross Fadely, so you will find
    # some funny comments
    #######################################################################
    @classmethod
    def _log_multi_gauss(cls, k, covs, means, inv_covs, D):
        """
        Gaussian log likelihood of the data for component k.
        """
        sgn, logdet = np.linalg.slogdet(covs[k])
        #assert sgn > 0
        X1 = (D - means[k]).T
        X2 = np.dot(inv_covs[k], X1)
        p = -0.5 * np.sum(X1 * X2, axis=0)
        return -0.5 * np.log(2 * np.pi) * cls.D - 0.5 * logdet + p

    @classmethod
    def _log_sum(cls,loglikes):
        """
        Calculate sum of log likelihoods
        """
        loglikes = np.atleast_2d(loglikes)
        a = np.max(loglikes, axis=0)
        return a + np.log(np.sum(np.exp(loglikes - a[None, :]), axis=0))

    @classmethod
    def cal_logLs(cls, covs, means, inv_covs):
        """
        Calculate log likelihoods for each datum
        under each component.
        """
        logrs = np.zeros((cls.K, cls.N))
        for k in range(cls.K):
            logrs[k] = np.log(cls.pi[k]) + cls._log_multi_gauss(k=k, covs=covs, means=means, inv_covs=inv_covs, D=cls.d.T)

        # here lies some ghetto log-sum-exp...
        # nothing like a little bit of overflow to make your day better!
        L = cls._log_sum(logrs)
        return L

class BatchBaseTestCase(BaseTestCase):
    """
    Unittests for MOFA with 2 clusters.
    Train the model using batch algorithm

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls):
        """
        Run before tests are executed.

        Synthesize data and run EM on the synthetic data
        """

        if cls is BaseTestCase:
            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")
        super(BaseTestCase, cls).setUpClass()

        # set internal parameters
        cls.PPCA = False
        cls.lock_psis = True
        tol = 1e-10
        verbose = True
        cls.N = 50000
        cls.K = 2
        cls.D = 3
        cls.M = 2
        cls.batch_size = 50000
        max_epochs=400

        # set random seed
        cls.seed = 2
        np.random.seed(cls.seed)

        # make data
        correct_means = np.concatenate((2.0*np.ones((1,cls.D),dtype=theano.config.floatX),
                                        13.0*np.ones((1,cls.D),dtype=theano.config.floatX)))
        correct_lambdas = np.asarray([np.random.randn(cls.D, cls.M), np.random.randn(cls.D, cls.M)],
                                         dtype=theano.config.floatX)

        cls.makeData(correct_means=correct_means, correct_lambdas=correct_lambdas)

        #################################################################
        # start building and training the model
        #################################################################

        # x is the symbolic variable for data
        cls.x = T.matrix('x')

        cls.mix = Mofa((cls.x).T, cls.K, cls.M, cls.D, cls.batch_size, cls.means_val_init, cls.psis_val_init, cls.PPCA, lock_psis=cls.lock_psis)
        output_init = theano.function([cls.x] , [cls.mix.means, cls.mix.covs, cls.mix.psis], on_unused_input='ignore')
        cls.means_val_init, cls.covs_val_init, cls.psis_val_init = output_init(cls.d)
        cls.means_val_init = np.asarray(cls.means_val_init, dtype=theano.config.floatX)
        cls.covs_val_init = np.asarray(cls.covs_val_init, dtype=theano.config.floatX)
        cls.psis_val_init = np.asarray(cls.psis_val_init, dtype=theano.config.floatX)

        logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, means_val, covs_val,\
        inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, cls.LogLs_vec, cls.epochs, \
        cls.dLC_vec, cls.dPS_vec = cls.mix.batch_fit(x=cls.x, dat=cls.d, K=cls.K, batch_size=cls.batch_size,
                                                     tol=tol, max_epochs=max_epochs, verbose=verbose, mode='Param')

        cls.means_val, cls.lambdas_val, cls.psis_val, cls.covs_val, cls.inv_covs_val, cls.amps_val, cls.lambda_covs_val \
            = means_val, lambdas_val, psis_val, covs_val, inv_covs_val, amps_val, lambda_covs_val

        cls.getParams()

        cls.getPredict()

        # Display some important results
        # Optional: solely used for testing purposes
        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.logLs_val)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.logLs_val.sum())/(-cls.logLs.sum())
        print '\nCov Matrix'
        print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        print '\nLambdas Cov Matrix'
        print norm(cls.correct_lambda_covs - cls.lambda_covs_val)/norm(cls.correct_lambda_covs)
        print '\nPsi Matrix'
        print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        print '\nPi Vector'
        print norm(cls.pi - cls.amps_val, ord=1)/norm(cls.pi, ord=1)

    @classmethod
    def getParams(cls):
        cls.means_val = np.asarray(cls.means_val,dtype=theano.config.floatX)
        cls.lambdas_val = np.asarray(cls.lambdas_val,dtype=theano.config.floatX)
        cls.psis_val = np.asarray(cls.psis_val,dtype=theano.config.floatX)
        cls.covs_val = np.asarray(cls.covs_val,dtype=theano.config.floatX)
        cls.inv_covs_val = np.asarray(cls.inv_covs_val,dtype=theano.config.floatX)
        cls.amps_val = np.asarray(cls.amps_val,dtype=theano.config.floatX)
        cls.lambda_covs_val = np.asarray(cls.lambda_covs_val,dtype=theano.config.floatX)
        xtrain = T.matrix('xtrain')

        cls.mix_train = Mofa(xtrain.T, K=cls.K, M=cls.M, D=cls.D, N=cls.N,
                              means_val_init=cls.means_val, psis_val_init=cls.psis_val, lambdas_val_init=cls.lambdas_val,
                              covs_val_init=cls.covs_val, inv_covs_val_init=cls.inv_covs_val,
                              amps_val_init=cls.amps_val, lambda_covs_val_init=cls.lambda_covs_val,
                              PPCA=cls.PPCA, lock_psis=cls.lock_psis, init=False)

        cls.mix_train._E_step()

        get_Results = theano.function([], [cls.mix_train.rs, cls.mix_train.logLs, cls.mix_train.amps_new],
                                         on_unused_input='ignore',
                                         givens={xtrain:cls.d})

        cls.rs_val, cls.logLs_val, cls.amps_val = get_Results()


        cls.rs_val = np.asarray(cls.rs_val,dtype=theano.config.floatX)
        cls.logLs_val = np.asarray(cls.logLs_val,dtype=theano.config.floatX)

        for i in xrange(cls.K):
            dist_vec = []
            for j in xrange(i, cls.K, 1):
                dist_vec.append(norm(cls.correct_means[i] - cls.means_val[j]))

            right_order = np.argmin(dist_vec) + i

            cls.rs_val[[right_order, i],:] = cls.rs_val[[i, right_order],:]
            cls.means_val[[right_order, i],:] = cls.means_val[[i, right_order],:]
            cls.lambdas_val[[right_order, i],:,:] = cls.lambdas_val[[i, right_order],:,:]
            cls.psis_val[[right_order, i],:] = cls.psis_val[[i, right_order],:]
            cls.covs_val[[right_order, i],:,:] = cls.covs_val[[i, right_order],:,:]
            cls.inv_covs_val[[right_order, i],:,:] = cls.inv_covs_val[[i, right_order],:,:]
            cls.amps_val[[right_order, i]] = cls.amps_val[[i, right_order]]
            cls.lambda_covs_val[[right_order, i],:] = cls.lambda_covs_val[[i, right_order],:]
            cls.logLs_val[[right_order, i]] = cls.logLs_val[[i, right_order]]

        return None

    @classmethod
    def getPredict(cls):
        xtest = T.matrix('xtest')

        cls.mix_test = Mofa(xtest.T, K=cls.K, M=cls.M, D=cls.D, N=cls.N,
                              means_val_init=cls.means_val, psis_val_init=cls.psis_val, lambdas_val_init=cls.lambdas_val,
                              covs_val_init=cls.covs_val, inv_covs_val_init=cls.inv_covs_val,
                              amps_val_init=cls.amps_val, lambda_covs_val_init=cls.lambda_covs_val,
                              PPCA=cls.PPCA, lock_psis=cls.lock_psis, init=False)

        cls.labels_test, cls.means_val_test, cls.rs_val_test = cls.mix_test.predict(x=xtest, dat=cls.dtest)
        cls.means_val_test = np.asarray(cls.means_val_test, dtype=theano.config.floatX)
        cls.rs_val_test = np.asarray(cls.rs_val_test, dtype=theano.config.floatX)
        for i in xrange(cls.K):
            dist_vec = []
            for j in xrange(i, cls.K, 1):
                dist_vec.append(norm(cls.correct_means[i] - cls.means_val_test[j]))

            right_order = np.argmin(dist_vec) + i

            cls.rs_val_test[[right_order, i],:] = cls.rs_val_test[[i, right_order],:]
            cls.means_val_test[[right_order, i],:] = cls.means_val_test[[i, right_order],:]

        return None

    def test_convergence(cls):
        """
        Plot loglikelihood vs epochs
        """
        fig1 = figure()
        plot(cls.epochs, cls.LogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % cls.batch_size)
        fig1.savefig('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i.png' % cls.batch_size)

        fig2 = figure()
        plot(cls.epochs, cls.dLC_vec, 'bo-', label='Lambda_Covs')
        plot(cls.epochs, cls.dPS_vec, 'go-', label='Psis')
        legend()
        xlabel('Epoch')
        ylabel('Relative Difference')
        title('Convergence_of_Parameters_from_my_MFA_BatchSize_%i' % cls.batch_size)
        fig2.savefig('Convergence_of_Parameters_from_my_MFA_BatchSize_%i.png' % cls.batch_size)

class MFATestCase(BaseTestCase):
    """
    Unittests for MOFA with an arbitrary number of clusters.

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls, mode='balanced'):
        if cls is BaseTestCase:
            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")
        super(BaseTestCase, cls).setUpClass()

        # set internal parameters
        maxiter = 800
        cls.PPCA = False
        cls.lock_psis = True
        tol = 1e-10
        verbose = True
        cls.N = 50000
        cls.K = 3
        cls.D = 3
        cls.M = 2
        cls.mode = mode

        # set random seed
        cls.seed = 2
        np.random.seed(cls.seed)

        # make data
        correct_first_mean = 10.* np.asarray(np.random.randn(1,cls.D), dtype=theano.config.floatX)
        correct_first_lambda = np.asarray(np.random.randn(1, cls.D, cls.M), dtype=theano.config.floatX)
        cls.makeData(correct_first_mean=correct_first_mean, correct_first_lambda=correct_first_lambda)

        #################################################################
        # start building and training the model
        #################################################################

        cls.x = T.matrix('x')

        cls.mix = Mofa(cls.x.T, cls.K, cls.M, cls.D, cls.N, cls.means_val_init, cls.psis_val_init, cls.PPCA, lock_psis=cls.lock_psis)
        output_init = theano.function([cls.x] , [cls.mix.means, cls.mix.covs, cls.mix.psis], on_unused_input='ignore')
        cls.means_val_init, cls.covs_val_init, cls.psis_val_init = output_init(cls.d)
        cls.means_val_init = np.asarray(cls.means_val_init, dtype=theano.config.floatX)
        cls.covs_val_init = np.asarray(cls.covs_val_init, dtype=theano.config.floatX)
        cls.psis_val_init = np.asarray(cls.psis_val_init, dtype=theano.config.floatX)

        cls.LogLs_vec, cls.dLC_vec, cls.dPS_vec, cls.epochs = cls.mix.fit(x=cls.x, dat=cls.d, tol=tol, maxiter=maxiter, verbose=verbose, mode='NLL')

        # get params
        cls.getParams()

        # get prediction
        cls.getPredict()

        # Display some important results
        # Optional: solely used for testing purposes
        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.logLs_val)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.logLs_val.sum())/(-cls.logLs.sum())
        print '\nCov Matrix'
        print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        print '\nLambdas Cov Matrix'
        print norm(cls.correct_lambda_covs - cls.lambda_covs_val)/norm(cls.correct_lambda_covs)
        print '\nPsi Matrix'
        print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        print '\nPi Vector'
        print norm(cls.pi - cls.amps_val, ord=1)/norm(cls.pi, ord=1)

    @classmethod
    def tearDownClass(cls, PlotFig = True):
        if PlotFig:
            fig1 = pl.figure()
            color = np.abs(np.random.randn(cls.K,3))
            color = color/np.tile(np.sum(color, axis=0),reps=(cls.K,1))
            for i in xrange(cls.K):
                indx_val = np.nonzero(cls.correct_labels==i)
                pl.plot(cls.d[0,indx_val[0][0:500]], cls.d[1,indx_val[0][0:500]],'o',
                        color=np.concatenate((color[i,:],[0.4,]), axis=1))
            pl.xlim(cls.xmin, cls.xmax)
            pl.ylim(cls.ymin, cls.ymax)
            fig1.savefig('original_data_%i.png' % cls.seed)

            fig2=pl.figure()
            pl.plot(cls.means_val_init[:,0],cls.means_val_init[:,1],'rx',ms=15,label='Initialization')
            plot_2d_ellipses(0,1, cls.means_val_init, cls.covs_val_init, cls.K, edgecolor='r')
            indx = np.argmax(cls.rs_val,0)
            for i in xrange(cls.K):
                indx_val = np.nonzero(indx==i)
                pl.plot(cls.d[0,indx_val[0][0:200]], cls.d[1,indx_val[0][0:200]],'o',
                        color=np.concatenate((color[i,:],[0.4,]), axis=1))

            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.covs_val, K=2, edgecolor='g')
            plot_2d_ellipses_psis(0,1, means=cls.means_val, psis=cls.psis_val, K=2, edgecolor='k')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.lambda_covs_val, K=2, edgecolor='b')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.correct_lambda_covs, K=2, edgecolor='c')
            plot_2d_ellipses_psis(0,1, means=cls.means_val, psis=cls.correct_psis, K=2, edgecolor='c')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.correct_covs, K=2, edgecolor='c')
            pl.plot(cls.means_val[:,0],cls.means_val[:,1],'gx',ms=15,label='Psi fixed')
            pl.title(r'Data $(D, N) = ({0}, {1})$, Model $(K, M) = ({2}, {3})$'.format(cls.D,cls.N,cls.K,cls.M))
            pl.legend()
            pl.xlim(cls.xmin, cls.xmax)
            pl.ylim(cls.ymin, cls.ymax)
            pl.grid()
            fig2.savefig('results_%i.png' % cls.seed)

        cls.mix = None

    @classmethod
    def makeData(cls, correct_first_mean, correct_first_lambda):
        # set pi and number of samples per class
        if cls.mode == 'balanced':
            cls.pi = np.asarray((1./float(cls.K))*np.ones((cls.K,1),dtype=theano.config.floatX))
        else:
            alpha = np.ones((cls.K,1), dtype=np.uint8)
            alpha = hstack(alpha)
            pi = np.random.dirichlet(alpha=alpha, size=1)
            cls.pi = np.asarray(pi, dtype=theano.config.floatX)

        cls.pi = hstack(cls.pi)

        cls.NperClass = np.uint32(np.round(float(cls.N)*cls.pi))

        print cls.NperClass

        cls.N = np.sum(cls.NperClass)

        # set lambdas, means, and psis
        # synthesize latents and data
        # permute data and labels
        cls.correct_lambdas = correct_first_lambda
        cls.correct_means = correct_first_mean
        cls.correct_psis = np.zeros((cls.K, cls.D), dtype=theano.config.floatX)
        correct_psis_val = 20.*np.asarray(np.abs(np.random.randn(1, cls.D)), dtype=theano.config.floatX)
        cls.correct_psis[0] = correct_psis_val
        cls.z = np.asarray(np.random.randn(cls.M,cls.NperClass[0]), dtype=theano.config.floatX)
        cls.ztest = np.asarray(np.random.randn(cls.M,cls.NperClass[0]), dtype=theano.config.floatX)
        cls.d = np.dot(cls.correct_lambdas[0], cls.z) + np.tile(cls.correct_means[0], (cls.NperClass[0],1)).T
        cls.dtest = np.dot(cls.correct_lambdas[0], cls.ztest) + np.tile(cls.correct_means[0], (cls.NperClass[0],1)).T
        cls.correct_labels = np.zeros((cls.NperClass[0],1))
        cls.correct_labels_test = np.zeros((cls.NperClass[0],1))
        for i in xrange(1, cls.K, 1):
            cls.correct_lambdas = np.concatenate((cls.correct_lambdas, np.asarray(np.random.randn(1, cls.D, cls.M), dtype=theano.config.floatX)))
            cls.correct_means = np.concatenate((cls.correct_means, 10.* np.asarray(np.random.randn(1,cls.D), dtype=theano.config.floatX)))
            cls.correct_psis[i] = correct_psis_val
            z = np.asarray(np.random.randn(cls.M,cls.NperClass[i]), dtype=theano.config.floatX)
            ztest = np.asarray(np.random.randn(cls.M,cls.NperClass[i]), dtype=theano.config.floatX)
            d = np.dot(cls.correct_lambdas[i], z) + np.tile(cls.correct_means[i], (cls.NperClass[i],1)).T
            dtest = np.dot(cls.correct_lambdas[i], ztest) + np.tile(cls.correct_means[i], (cls.NperClass[i],1)).T
            cls.z = np.concatenate((cls.z,z), axis=1)
            cls.ztest = np.concatenate((cls.ztest,ztest), axis=1)
            cls.d = np.concatenate((cls.d,d), axis=1)
            cls.dtest = np.concatenate((cls.dtest,dtest), axis=1)
            cls.correct_labels = np.concatenate((cls.correct_labels, i*np.ones((cls.NperClass[i],1))), axis=0)
            cls.correct_labels_test = np.concatenate((cls.correct_labels_test, i*np.ones((cls.NperClass[i],1))), axis=0)

        cls.d = cls.d + np.dot(np.diag(np.sqrt(cls.correct_psis[1])),np.asarray(np.random.randn(cls.D,cls.N),
                                                                                dtype=theano.config.floatX))
        permuted_index = np.random.permutation(cls.N)
        cls.d[:, [range(cls.N), permuted_index]] = cls.d[:, [permuted_index, range(cls.N)]]
        cls.correct_labels[[range(cls.N), permuted_index],:] = cls.correct_labels[[permuted_index, range(cls.N)],:]

        cls.dtest = cls.dtest + np.dot(np.diag(np.sqrt(cls.correct_psis[1])),np.asarray(np.random.randn(cls.D,cls.N),
                                                                                dtype=theano.config.floatX))
        permuted_index_test = np.random.permutation(cls.N)
        cls.dtest[:, [range(cls.N), permuted_index_test]] = cls.dtest[:, [permuted_index_test, range(cls.N)]]
        cls.correct_labels_test[[range(cls.N), permuted_index_test],:] = cls.correct_labels_test[[permuted_index_test, range(cls.N)],:]

        # bounds for plots
        cls.xmin, cls.xmax = np.min(cls.d[0]), np.max(cls.d[0])
        cls.ymin, cls.ymax = np.min(cls.d[1]), np.max(cls.d[1])

        # compute the correct covariances
        cls.correct_lambda_covs = np.zeros((cls.K, cls.D, cls.D), dtype=theano.config.floatX)
        cls.correct_covs = np.zeros((cls.K, cls.D, cls.D), dtype=theano.config.floatX)
        for i in xrange(cls.K):
            cls.correct_lambda_covs[i] = np.dot(cls.correct_lambdas[i],(cls.correct_lambdas[i]).T)
            cls.correct_covs[i] = cls.correct_lambda_covs[i] + np.diag(cls.correct_psis[i])

        # compute the correct inverse covariances
        cls.inv_correct_covs = 0. * cls.correct_covs

        for k in xrange(cls.K):
            cls.inv_correct_covs[k] = cls._invert_cov(k)

        # compute correct log-likelihood
        cls.logLs = cls.cal_logLs(covs=cls.correct_covs, means=cls.correct_means, inv_covs=cls.inv_correct_covs)

        # compute the initial means using kmeans
        init_obj = kmeans_init((cls.d).T, cls.K, cls.N)
        init_obj.compute_kmeans()
        cls.means_val_init = (init_obj.means).get_value()

        # compute the initial psis
        cls.psis_val_init=np.tile(np.var((cls.d).T,axis=0)[None,:],(cls.K,1))


class BatchMFATestCase(BatchBaseTestCase):
    """
    Unittests for MOFA with an arbitrary number of clusters.
    Train the model using batch algorithm

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls, mode='balanced'):
        if cls is BaseTestCase:
            raise unittest.SkipTest("Skip BatchBaseTest and BaseTest tests, they are bas e class")
        super(BaseTestCase, cls).setUpClass()

        # set internal parameters
        cls.PPCA = False
        cls.lock_psis = True
        tol = 1e-15
        verbose = True
        cls.N = 50000
        cls.K = 10
        cls.D = 100
        cls.M = 1
        cls.batch_size = 50000
        max_epochs=200
        cls.mode = mode

        # set random seed
        cls.seed = 2
        np.random.seed(cls.seed)

        # make data
        correct_first_mean = 10.* np.asarray(np.random.randn(1,cls.D), dtype=theano.config.floatX)
        correct_first_lambda = np.asarray(np.random.randn(1, cls.D, cls.M), dtype=theano.config.floatX)
        cls.makeData(correct_first_mean=correct_first_mean, correct_first_lambda=correct_first_lambda)

        #################################################################
        # start building and training the model
        #################################################################

        # x is the symbolic variable for data
        cls.x = T.matrix('x')

        cls.mix = Mofa((cls.x).T, cls.K, cls.M, cls.D, cls.batch_size, cls.means_val_init, cls.psis_val_init, PPCA=cls.PPCA, lock_psis=cls.lock_psis)
        output_init = theano.function([cls.x] , [cls.mix.means, cls.mix.covs, cls.mix.psis], on_unused_input='ignore')
        cls.means_val_init, cls.covs_val_init, cls.psis_val_init = output_init(cls.d)
        cls.means_val_init = np.asarray(cls.means_val_init, dtype=theano.config.floatX)
        cls.covs_val_init = np.asarray(cls.covs_val_init, dtype=theano.config.floatX)
        cls.psis_val_init = np.asarray(cls.psis_val_init, dtype=theano.config.floatX)

        logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, means_val, covs_val,\
        inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, cls.LogLs_vec, cls.epochs, \
        cls.dLC_vec, cls.dPS_vec = cls.mix.batch_fit(x=cls.x, dat=cls.d, K=cls.K, batch_size=cls.batch_size,
                                                     tol=tol, max_epochs=max_epochs, verbose=verbose, mode='NLL')

        cls.means_val, cls.lambdas_val, cls.psis_val, cls.covs_val, cls.inv_covs_val, cls.amps_val, cls.lambda_covs_val \
            = means_val, lambdas_val, psis_val, covs_val, inv_covs_val, amps_val, lambda_covs_val

        cls.getParams()

        cls.getPredict()

        # Display some important results
        # Optional: solely used for testing purposes
        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.logLs_val)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.logLs_val.sum())/(-cls.logLs.sum())
        print '\nCov Matrix'
        print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        print '\nLambdas Cov Matrix'
        print norm(cls.correct_lambda_covs - cls.lambda_covs_val)/norm(cls.correct_lambda_covs)
        print '\nPsi Matrix'
        print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        print '\nPi Vector'
        print norm(cls.pi - cls.amps_val, ord=1)/norm(cls.pi, ord=1)
        print '\nMeans'
        # print('Correct Means')
        # print(cls.correct_means)
        # print('Learned Means')
        # print(cls.means_val)
        print norm(cls.correct_means - cls.means_val)/norm(cls.correct_means)

        print('\nCorrect Pi')
        print(cls.pi)
        print('\nLearned Pi')
        print(cls.amps_val)

    @classmethod
    def tearDownClass(cls, PlotFig = True):
        if PlotFig:
            fig1 = pl.figure()
            color = np.abs(np.random.randn(cls.K,3))
            color = color/np.tile(np.sum(color, axis=0),reps=(cls.K,1))
            for i in xrange(cls.K):
                indx_val = np.nonzero(cls.correct_labels==i)
                pl.plot(cls.d[0,indx_val[0][0:500]], cls.d[1,indx_val[0][0:500]],'o',
                        color=np.concatenate((color[i,:],[0.4,]), axis=1))
            pl.xlim(cls.xmin, cls.xmax)
            pl.ylim(cls.ymin, cls.ymax)
            fig1.savefig('original_data_%i.png' % cls.seed)

            fig2=pl.figure()
            pl.plot(cls.means_val_init[:,0],cls.means_val_init[:,1],'rx',ms=15,label='Initialization')
            plot_2d_ellipses(0,1, cls.means_val_init, cls.covs_val_init, cls.K, edgecolor='r')
            indx = np.argmax(cls.rs_val,0)
            for i in xrange(cls.K):
                indx_val = np.nonzero(indx==i)
                pl.plot(cls.d[0,indx_val[0][0:200]], cls.d[1,indx_val[0][0:200]],'o',
                        color=np.concatenate((color[i,:],[0.4,]), axis=1))

            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.covs_val, K=2, edgecolor='g')
            plot_2d_ellipses_psis(0,1, means=cls.means_val, psis=cls.psis_val, K=2, edgecolor='k')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.lambda_covs_val, K=2, edgecolor='b')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.correct_lambda_covs, K=2, edgecolor='c')
            plot_2d_ellipses_psis(0,1, means=cls.means_val, psis=cls.correct_psis, K=2, edgecolor='c')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.correct_covs, K=2, edgecolor='c')
            pl.plot(cls.means_val[:,0],cls.means_val[:,1],'gx',ms=15,label='Psi fixed')
            pl.title(r'Data $(D, N) = ({0}, {1})$, Model $(K, M) = ({2}, {3})$'.format(cls.D,cls.N,cls.K,cls.M))
            pl.legend()
            pl.xlim(cls.xmin, cls.xmax)
            pl.ylim(cls.ymin, cls.ymax)
            fig2.savefig('results_%i.png' % cls.seed)

        cls.mix = None

    @classmethod
    def makeData(cls, correct_first_mean, correct_first_lambda):
        # set pi and number of samples per class
        if cls.mode == 'balanced':
            cls.pi = np.asarray((1./float(cls.K))*np.ones((cls.K,1),dtype=theano.config.floatX))
        else:
            alpha = np.ones((cls.K,1), dtype=np.uint8)
            alpha = hstack(alpha)
            pi = np.random.dirichlet(alpha=alpha, size=1)
            cls.pi = np.asarray(pi, dtype=theano.config.floatX)

        cls.pi = hstack(cls.pi)

        cls.NperClass = np.uint32(np.round(float(cls.N)*cls.pi))

        print cls.NperClass

        cls.N = np.sum(cls.NperClass)

        # set lambdas, means, and psis
        # synthesize latents and data
        # permute data and labels
        cls.correct_lambdas = correct_first_lambda
        cls.correct_means = correct_first_mean
        cls.correct_psis = np.zeros((cls.K, cls.D), dtype=theano.config.floatX)
        correct_psis_val = 32.*np.asarray(np.abs(np.random.randn(1, cls.D)), dtype=theano.config.floatX)
        cls.correct_psis[0] = correct_psis_val
        cls.z = np.asarray(np.random.randn(cls.M,cls.NperClass[0]), dtype=theano.config.floatX)
        cls.ztest = np.asarray(np.random.randn(cls.M,cls.NperClass[0]), dtype=theano.config.floatX)
        cls.d = np.dot(cls.correct_lambdas[0], cls.z) + np.tile(cls.correct_means[0], (cls.NperClass[0],1)).T
        cls.dtest = np.dot(cls.correct_lambdas[0], cls.ztest) + np.tile(cls.correct_means[0], (cls.NperClass[0],1)).T
        cls.correct_labels = np.zeros((cls.NperClass[0],1))
        cls.correct_labels_test = np.zeros((cls.NperClass[0],1))
        for i in xrange(1, cls.K, 1):
            cls.correct_lambdas = np.concatenate((cls.correct_lambdas, np.asarray(np.random.randn(1, cls.D, cls.M), dtype=theano.config.floatX)))
            cls.correct_means = np.concatenate((cls.correct_means, 10.* np.asarray(np.random.randn(1,cls.D), dtype=theano.config.floatX)))
            cls.correct_psis[i] = correct_psis_val
            z = np.asarray(np.random.randn(cls.M,cls.NperClass[i]), dtype=theano.config.floatX)
            ztest = np.asarray(np.random.randn(cls.M,cls.NperClass[i]), dtype=theano.config.floatX)
            d = np.dot(cls.correct_lambdas[i], z) + np.tile(cls.correct_means[i], (cls.NperClass[i],1)).T
            dtest = np.dot(cls.correct_lambdas[i], ztest) + np.tile(cls.correct_means[i], (cls.NperClass[i],1)).T
            cls.z = np.concatenate((cls.z,z), axis=1)
            cls.ztest = np.concatenate((cls.ztest,ztest), axis=1)
            cls.d = np.concatenate((cls.d,d), axis=1)
            cls.dtest = np.concatenate((cls.dtest,dtest), axis=1)
            cls.correct_labels = np.concatenate((cls.correct_labels, i*np.ones((cls.NperClass[i],1))), axis=0)
            cls.correct_labels_test = np.concatenate((cls.correct_labels_test, i*np.ones((cls.NperClass[i],1))), axis=0)

        cls.d = cls.d + np.dot(np.diag(np.sqrt(cls.correct_psis[1])),np.asarray(np.random.randn(cls.D,cls.N),
                                                                                dtype=theano.config.floatX))
        permuted_index = np.random.permutation(cls.N)
        cls.d[:, [range(cls.N), permuted_index]] = cls.d[:, [permuted_index, range(cls.N)]]
        cls.correct_labels[[range(cls.N), permuted_index],:] = cls.correct_labels[[permuted_index, range(cls.N)],:]

        cls.dtest = cls.dtest + np.dot(np.diag(np.sqrt(cls.correct_psis[1])),np.asarray(np.random.randn(cls.D,cls.N),
                                                                                dtype=theano.config.floatX))
        permuted_index_test = np.random.permutation(cls.N)
        cls.dtest[:, [range(cls.N), permuted_index_test]] = cls.dtest[:, [permuted_index_test, range(cls.N)]]
        cls.correct_labels_test[[range(cls.N), permuted_index_test],:] = cls.correct_labels_test[[permuted_index_test, range(cls.N)],:]

        # bounds for plots
        cls.xmin, cls.xmax = np.min(cls.d[0]), np.max(cls.d[0])
        cls.ymin, cls.ymax = np.min(cls.d[1]), np.max(cls.d[1])

        # compute the correct covariances
        cls.correct_lambda_covs = np.zeros((cls.K, cls.D, cls.D), dtype=theano.config.floatX)
        cls.correct_covs = np.zeros((cls.K, cls.D, cls.D), dtype=theano.config.floatX)
        for i in xrange(cls.K):
            cls.correct_lambda_covs[i] = np.dot(cls.correct_lambdas[i],(cls.correct_lambdas[i]).T)
            cls.correct_covs[i] = cls.correct_lambda_covs[i] + np.diag(cls.correct_psis[i])

        # compute the correct inverse covariances
        cls.inv_correct_covs = 0. * cls.correct_covs

        for k in xrange(cls.K):
            cls.inv_correct_covs[k] = cls._invert_cov(k)

        # compute correct log-likelihood
        cls.logLs = cls.cal_logLs(covs=cls.correct_covs, means=cls.correct_means, inv_covs=cls.inv_correct_covs)

        # compute the initial means using kmeans
        init_obj = kmeans_init((cls.d).T, cls.K, cls.N)
        init_obj.compute_kmeans()
        cls.means_val_init = (init_obj.means).get_value()

        # compute the initial psis
        cls.psis_val_init=np.tile(np.var((cls.d).T,axis=0)[None,:],(cls.K,1))

        print 'Shape of Training_Data'
        print np.shape(cls.d)
        print 'Training_Data'
        print cls.d[:,0:10].T
        print 'Training_Label'
        print cls.correct_labels[0:10]

        print 'Shape of Test_Data'
        print np.shape(cls.dtest)
        print 'Test_Data'
        print cls.dtest[:,0:10].T
        print 'Test_Label'
        print cls.correct_labels_test[0:10]

        print 'Means_val_init'
        print cls.means_val_init
        print 'Psis_val_init'
        print cls.psis_val_init

if __name__ == '__main__':
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromTestCase(BatchMFATestCase)
    runner = unittest.TextTestRunner()
    results = runner.run(suite)