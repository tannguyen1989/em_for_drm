__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

import os

from PIL import Image

import numpy as np

from load_data_stable import DATA

import theano
import theano.tensor as T

from theano.misc.pkl_utils import load


# from guppy import hpy; h=hpy()

class Probe(object):
    '''
    This probe class is used to analyze a trained model
    '''

    def __init__(self, model, output_dir):
        self.model = model
        self.output_dir = output_dir

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

    def reconstruct_images(self, input, Nimages):
        # Reconstruct images
        I_hat = self.model.ReconstructInput(input)
        I_hat = I_hat.transpose((0, 2, 3, 1))
        I_hat = I_hat[0:Nimages]
        return I_hat

    def sample_images(self, mode, Nimages, input=[]):
        if mode != 'use_ta_BU':
            I_hat = self.model.SampleImage(mode=mode, Nimages=500)
        else:
            I_hat = self.model.SampleImage(mode=mode, Nimages=500, input=input)

        I_hat = I_hat.transpose((0, 2, 3, 1))
        I_hat = I_hat[0:Nimages]
        return I_hat

def plot_image_table(imgs, output_dir, file_name):
    N = imgs.shape[0]
    for i in xrange(N):
        if imgs.shape[3] == 1:
            imshow(imgs[i, :, :, 0], cmap='gray')
        else:
            imshow(imgs[i])
        axis('off')
        out_path = os.path.join(output_dir, 'single_images')
        if not os.path.exists(out_path):
            os.makedirs(out_path)

        out_file = os.path.join(out_path, '%s_%i.jpg' % (file_name,i))
        savefig(out_file)
        close()
    sub_x_size = int(sqrt(N))
    sub_y_size = int(sqrt(N))
    img = Image.new('RGB', (800 * sub_x_size, 600 * sub_y_size))
    for i in xrange(N):
        this_img = Image.open(os.path.join(out_path, '%s_%i.jpg' % (file_name,i)))
        x_loc = (i % sub_x_size) * 800
        y_loc = (i / sub_y_size) * 600
        img.paste(this_img, (x_loc, y_loc))
    fig = imshow(img)
    axis('off')
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    savefig(os.path.join(output_dir, '%ss.pdf'%file_name))
    savefig(os.path.join(output_dir, '%ss.jpg' %file_name))
    close()

def runProbe(model_file_name, training_name):
    N = 500
    Nimages = 36
    data_dir = '/home/ubuntu/repos/em_for_drm/data/mnist.pkl.gz'

    root_dir = '/home/ubuntu/research_results/EM_results'
    model_dir = os.path.join(root_dir, training_name, 'Train/params', model_file_name)
    output_dir = os.path.join(root_dir, training_name, 'Train', 'model_analysis_results')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    output_dir = os.path.join(output_dir,model_file_name[:-4])
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    mnist = DATA(dataset_name='MNIST', data_mode='all', data_dir=data_dir, Nlabeled=50000, Ni=50000, Cin=1, H=28, W=28,
                 seed=5)
    dat = mnist.dtrain[0:N]

    with open(model_dir, 'rb') as f:
        model = load(f)

    import pdb; pdb.set_trace()

    probe = Probe(model=model, output_dir=output_dir)

    I = dat[0:Nimages]
    I = I.transpose((0, 2, 3, 1))
    plot_image_table(imgs=I, output_dir=output_dir, file_name='OriginalImage')

    I_reconstructed = probe.reconstruct_images(input=dat, Nimages=Nimages)
    plot_image_table(imgs=I_reconstructed, output_dir=output_dir, file_name='ReconstructedImage')

    mode = 'uniform'
    I_sampled = probe.sample_images(mode=mode, Nimages=Nimages)
    plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='Uniform_SampledImage')

    mode = 'proper_using_pi'
    I_sampled = probe.sample_images(mode=mode, Nimages=Nimages)
    plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='ProperUsingPi_SampledImage')

    # TODO: Figure out the memory issues to include 'proper_using_final_pi' for both model

    mode = 'proper_using_final_pi'
    shared_dat = theano.shared(mnist.dtrain)
    N, Cin, h, w = np.shape(mnist.dtrain)
    n_batches = N / probe.model.batch_size
    index = T.iscalar()

    update_after_train = theano.function([index, probe.model.is_train, probe.model.momentum_bn], [],
                                                                                updates=probe.model.updates_after_train, on_unused_input='warn',
                                                                                givens={probe.model.x: shared_dat[index * probe.model.batch_size: (index + 1) * probe.model.batch_size],
                                                                                        probe.model.y: shared_dat[index * probe.model.batch_size: (index + 1) * probe.model.batch_size]})

    for minibatch_index in xrange(n_batches):
        update_after_train(minibatch_index, 0, 1.0)

    I_sampled = probe.sample_images(mode=mode, Nimages=Nimages)
    plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='ProperUsingPiFinal_SampledImage')

    mode = 'use_ta_BU'
    I_sampled = probe.sample_images(mode=mode, Nimages=Nimages, input=dat)
    plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='UsingPiBU_SampledImage')





