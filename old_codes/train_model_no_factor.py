__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from old_codes.CRM_no_factor_stable import CRM

from old_codes.nn_functions_stable import LogisticRegression, LogisticRegressionForSemisupervised

import theano.tensor as T

import theano

import pickle


# from guppy import hpy; h=hpy()

class EG_SRM_supervised(object):
    '''
    EG
    '''
    def  __init__(self, learning_rate, batch_size, Cin, W, H, em_mode, seed, param_dir, use_mode='train'):

        self.em_mode = em_mode
        self.use_mode = use_mode

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.batch_size = batch_size

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.D1 = self.h1 * self.w1 * self.Cin1 # patch size

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        if self.use_mode == 'train':
            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='last',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        else:
            with load(param_dir) as params:
                lambdas_val_init = params['lambdas_val'][0]
                amps_val_init = params['amps_val'][0]

            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                         amps_val_init=amps_val_init, lambdas_val_init=lambdas_val_init,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='last',
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step_Bottom_Up()

        n_softmax = self.K1*(self.H1 - self.h1 + 1)*(self.W1 - self.w1 + 1)/4

        softmax_input = self.conv1.output.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegression(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        # self.cost = self.softmax_layer.negative_log_likelihood(self.y) - T.mean(self.conv1.logLs)
        self.cost = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv1.lambdas,] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[0]
        self.softmax_layer.W_new = self.softmax_layer.params[0] - learning_rate * grads[1]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - learning_rate * grads[2]

        self.layers = [self.conv1,]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.lambdas_new) #2
            self.output_var.append(layer.amps_new) #3
            # Concatenate misc
            self.misc.append(layer.latents) #0
            self.misc.append(layer.rs) #1
            self.misc.append(layer.latents_masked) #2
            # Concatenate init_var
            self.init_var.append(layer.betas) #0
            self.init_var.append(layer.lambdas) #1
            self.init_var.append(layer.amps) #2

        self.output_var.append(self.softmax_layer.W_new)
        self.output_var.append(self.softmax_layer.b_new)
        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

########################################################################################################################
class EG_SRM_unsupervised_TopDown(object):
    '''
    EG
    '''
    def  __init__(self, learning_rate, batch_size, Cin, W, H, em_mode, seed):

        self.em_mode = em_mode

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.batch_size = batch_size
        self.learning_rate = learning_rate

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.D1 = self.h1 * self.w1 * self.Cin1 # patch size
        self.D = self.H1 * self.W1 * self.Cin1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='last',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step_Bottom_Up()

        self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv1.latents_masked, ta_hat=self.conv1.masked_mat, layer_loc='last')

        # the cost we minimize during training is the NLL of the model
        self.cost = T.mean((self.x - self.conv1.data_reconstructed)**2)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv1.lambdas,]

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[0]

        self.layers = [self.conv1,]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.lambdas_new) #2
            self.output_var.append(layer.amps_new) #3

########################################################################################################################
class EG_SRM_semisupervised(object):
    '''
    EG
    '''
    def  __init__(self, learning_rate, batch_size, Cin, W, H, em_mode, seed, param_dir, reg_coeff, use_mode='train'):

        self.em_mode = em_mode
        self.use_mode = use_mode

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.batch_size = batch_size

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.D1 = self.h1 * self.w1 * self.Cin1 # patch size

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        if self.use_mode == 'train':
            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='last',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        else:
            with load(param_dir) as params:
                lambdas_val_init = params['lambdas_val'][0]
                amps_val_init = params['amps_val'][0]

            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=amps_val_init, lambdas_val_init=lambdas_val_init,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='last',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        # Bottom-Up
        self.conv1._E_step_Bottom_Up()

        # Top-Down
        self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv1.latents_masked, ta_hat=self.conv1.masked_mat, layer_loc='last')

        n_softmax = self.K1*(self.H1 - self.h1 + 1)*(self.W1 - self.w1 + 1)/4

        softmax_input = self.conv1.output.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        # self.cost = self.softmax_layer.negative_log_likelihood(self.y) - T.mean(self.conv1.logLs)
        self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff*T.mean((self.x - self.conv1.data_reconstructed)**2)
        self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed)**2)
        self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv1.lambdas,] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[0]
        self.softmax_layer.W_new = self.softmax_layer.params[0] - learning_rate * grads[1]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - learning_rate * grads[2]

        self.layers = [self.conv1,]

        # self.layers = [self.conv1,]

        self.updates = []
        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.lambdas_new) #2
            self.output_var.append(layer.amps_new) #3
            # Concatenate misc
            self.misc.append(layer.latents) #0
            self.misc.append(layer.rs) #1
            self.misc.append(layer.latents_masked) #2
            # Concatenate init_var
            self.init_var.append(layer.betas) #0
            self.init_var.append(layer.lambdas) #1
            self.init_var.append(layer.amps) #2

        self.output_var.append(self.softmax_layer.W_new)
        self.output_var.append(self.softmax_layer.b_new)
        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

#######################################################################################################################
class EG_DRM_supervised(object):
    '''
    EG
    '''
    def  __init__(self, batch_size, Cin, W, H, em_mode, seed, param_dir=[], use_mode='train', init_Bengio=False):

        self.em_mode = em_mode

        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1

        self.h2 = 5
        self.w2 = 5
        self.K2 = 50
        self.M2 = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)

        if use_mode=='train':
            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                                 amps_val_init=None, lambdas_val_init=None,
                                 PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                                 rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)

            self.conv1._E_step_Bottom_Up()

            self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)

            self.conv2._E_step_Bottom_Up()
        else:
            print(param_dir)
            pkl_file = open(param_dir, 'rb')
            params = pickle.load(pkl_file)
            lambdas_val_init = params['lambdas_val']
            amps_val_init = params['amps_val']
            pkl_file.close()

            self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                                 amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
                                 PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                                 rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)

            self.conv1._E_step_Bottom_Up()

            self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                             amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)

            self.conv2._E_step_Bottom_Up()

        n_softmax = self.K2*(self.H2 - self.h2 + 1)*(self.W2 - self.w2 + 1)/4

        softmax_input = self.conv2.output.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegression(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        self.cost = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv2.lambdas,] + [self.conv1.lambdas,] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv2.lambdas_new = self.conv2.lambdas - self.lr * grads[0]
        self.conv1.lambdas_new = self.conv1.lambdas - self.lr * grads[1]
        self.softmax_layer.W_new = self.softmax_layer.params[0] - self.lr * grads[2]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - self.lr * grads[3]

        self.layers = [self.conv2, self.conv1]

        self.updates = []
        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.lambdas_new) #2
            self.output_var.append(layer.amps_new) #3
            # Concatenate misc
            self.misc.append(layer.latents) #0
            self.misc.append(layer.rs) #1
            self.misc.append(layer.latents_masked) #2
            # Concatenate init_var
            self.init_var.append(layer.betas) #0
            self.init_var.append(layer.lambdas) #1
            self.init_var.append(layer.amps) #2

        self.output_var.append(self.softmax_layer.W_new)
        self.output_var.append(self.softmax_layer.b_new)
        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

#######################################################################################################################
class EG_DRM_unsupervised_TopDown(object):
    '''
    EG
    '''
    def  __init__(self, learning_rate, batch_size, Cin, W, H, em_mode, seed):

        self.em_mode = em_mode

        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 64
        self.M1 = 1

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1

        self.h2 = 5
        self.w2 = 5
        self.K2 = 64
        self.M2 = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)
        self.conv2._E_step_Bottom_Up()

        # Top-Down
        self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.latents_masked, ta_hat=self.conv2.masked_mat, layer_loc='last')

        self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat, layer_loc='intermediate')

        self.cost = T.mean((self.x - self.conv1.data_reconstructed)**2)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv2.lambdas,] + [self.conv1.lambdas,]

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv2.lambdas_new = self.conv2.lambdas - learning_rate * T.clip(grads[0], -1.0, 1.0)
        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * T.clip(grads[1], -1.0, 1.0)

        self.layers = [self.conv2, self.conv1]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.lambdas_new) #2
            self.output_var.append(layer.amps_new) #3

#######################################################################################################################
class EG_DRM_unsupervised_MS_TopDown(object):
    '''
    EG
    '''
    def  __init__(self, learning_rate, batch_size, Cin, W, H, em_mode, seed):

        self.em_mode = em_mode

        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 64
        self.M1 = 1

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1

        self.h2 = 5
        self.w2 = 5
        self.K2 = 64
        self.M2 = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)
        self.conv2._E_step_Bottom_Up()

        # MS Top-Down

        self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.latents_masked, ta_hat=self.conv2.masked_mat, layer_loc='last')

        self.conv1._E_step_Top_Down(z=self.conv2.data_reconstructed)

        self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat, layer_loc='intermediate')

        self.cost = T.mean((self.x - self.conv1.data_reconstructed)**2)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv2.lambdas,] + [self.conv1.lambdas,]

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv2.lambdas_new = self.conv2.lambdas - learning_rate * T.clip(grads[0], -1.0, 1.0)
        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * T.clip(grads[1], -1.0, 1.0)

        self.layers = [self.conv2, self.conv1]

        self.updates = []
        self.output_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.lambdas_new) #2
            self.output_var.append(layer.amps_new) #3

#######################################################################################################################
class EG_DRM_semisupervised(object):
    '''
    EG
    '''
    def  __init__(self, batch_size, Cin, W, H, em_mode, seed, reg_coeff, init_Bengio=False):

        self.em_mode = em_mode

        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 64
        self.M1 = 1

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1

        self.h2 = 5
        self.w2 = 5
        self.K2 = 64
        self.M2 = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
        self.conv2._E_step_Bottom_Up()

        # Top-Down
        self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.latents_masked, ta_hat=self.conv2.masked_mat, layer_loc='last')

        self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat, layer_loc='intermediate')

        n_softmax = self.K2*(self.H2 - self.h2 + 1)*(self.W2 - self.w2 + 1)/4

        softmax_input = self.conv2.output.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        # self.cost = self.softmax_layer.negative_log_likelihood(self.y) - T.mean(self.conv1.logLs)
        self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff*T.mean((self.x - self.conv1.data_reconstructed)**2)
        self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed)**2)
        self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv2.lambdas,] + [self.conv1.lambdas,] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv2.lambdas_new = self.conv2.lambdas - self.lr * grads[0]
        self.conv1.lambdas_new = self.conv1.lambdas - self.lr * grads[1]
        self.softmax_layer.W_new = self.softmax_layer.params[0] - self.lr * grads[2]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - self.lr * grads[3]

        self.layers = [self.conv2, self.conv1]

        self.updates = []
        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.lambdas_new) #2
            self.output_var.append(layer.amps_new) #3
            # Concatenate misc
            self.misc.append(layer.latents) #0
            self.misc.append(layer.rs) #1
            self.misc.append(layer.latents_masked) #2
            # Concatenate init_var
            self.init_var.append(layer.betas) #0
            self.init_var.append(layer.lambdas) #1
            self.init_var.append(layer.amps) #2

        self.output_var.append(self.softmax_layer.W_new)
        self.output_var.append(self.softmax_layer.b_new)
        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

#######################################################################################################################
class EG_DRM_semisupervised_MS(object):
    '''
    EG
    '''
    def  __init__(self, learning_rate, batch_size, Cin, W, H, em_mode, seed, reg_coeff):

        self.em_mode = em_mode

        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 20
        self.M1 = 1

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1

        self.h2 = 5
        self.w2 = 5
        self.K2 = 50
        self.M2 = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)
        self.conv2._E_step_Bottom_Up()

        # MS Top-Down

        self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.latents_masked, ta_hat=self.conv2.masked_mat, layer_loc='last')

        self.conv1._E_step_Top_Down(z=self.conv2.data_reconstructed)

        self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat, layer_loc='intermediate')

        n_softmax = self.K2*(self.H2 - self.h2 + 1)*(self.W2 - self.w2 + 1)/4

        softmax_input = self.conv2.output.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        # self.cost = self.softmax_layer.negative_log_likelihood(self.y) - T.mean(self.conv1.logLs)
        self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff*T.mean((self.x - self.conv1.data_reconstructed)**2)
        self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed)**2)
        self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv2.lambdas,] + [self.conv1.lambdas,] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv2.lambdas_new = self.conv2.lambdas - learning_rate * grads[0]
        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[1]
        self.softmax_layer.W_new = self.softmax_layer.params[0] - learning_rate * grads[2]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - learning_rate * grads[3]

        self.layers = [self.conv2, self.conv1]

        self.updates = []
        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.lambdas_new) #2
            self.output_var.append(layer.amps_new) #3
            # Concatenate misc
            self.misc.append(layer.latents) #0
            self.misc.append(layer.rs) #1
            self.misc.append(layer.latents_masked) #2
            # Concatenate init_var
            self.init_var.append(layer.betas) #0
            self.init_var.append(layer.lambdas) #1
            self.init_var.append(layer.amps) #2

        self.output_var.append(self.softmax_layer.W_new)
        self.output_var.append(self.softmax_layer.b_new)
        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))

########################################################################################################################
# Deep Models similar to those used by Ladder Networks (ConvSmall and ConvLarge)
########################################################################################################################
#######################################################################################################################
class ConvSmall(object):
    '''
    EG
    '''
    def  __init__(self, learning_rate, batch_size, Cin, W, H, em_mode, seed, reg_coeff):

        self.em_mode = em_mode

        self.batch_size = batch_size

        self.H1 = H
        self.W1 = W
        self.Cin1 = Cin

        self.h1 = 5
        self.w1 = 5
        self.K1 = 32
        self.M1 = 1

        self.H2 = (self.H1 - self.h1 + 1)/2
        self.W2 = (self.W1 - self.w1 + 1)/2
        self.Cin2 = self.K1

        self.h2 = 3
        self.w2 = 3
        self.K2 = 64
        self.M2 = 1

        self.H3 = self.H2 - self.h2 + 1
        self.W3 = self.W2 - self.w2 + 1
        self.Cin3 = self.K2

        self.h3 = 3
        self.w3 = 3
        self.K3 = 64
        self.M3 = 1

        self.H4 = (self.H3 - self.h3 + 1)/2
        self.W4 = (self.W3 - self.w3 + 1)/2
        self.Cin4 = self.K3

        self.h4 = 3
        self.w4 = 3
        self.K4 = 128
        self.M4 = 1

        self.H5 = self.H4 - self.h4 + 1
        self.W5 = self.W4 - self.w4 + 1
        self.Cin5 = self.K4

        self.h5 = 1
        self.w5 = 1
        self.K5 = 10
        self.M5 = 1

        self.H6 = (self.H5 - self.h5 + 1)/2
        self.W6 = (self.W5 - self.w5 + 1)/2
        self.Cin6 = self.K5

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')

        self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
                             amps_val_init=None, lambdas_val_init=None,
                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                             rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2, w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=None, lambdas_val_init=None,
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc = 'intermediate',
                         rs_clip = 0.0, max_condition_number=1.e3, init_ppca=False)
        self.conv2._E_step_Bottom_Up()

        # Top-Down
        self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.latents_masked, ta_hat=self.conv2.masked_mat, layer_loc='last')

        self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat, layer_loc='intermediate')

        n_softmax = self.K2*(self.H2 - self.h2 + 1)*(self.W2 - self.w2 + 1)/4

        softmax_input = self.conv2.output.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10)

        # the cost we minimize during training is the NLL of the model
        # self.cost = self.softmax_layer.negative_log_likelihood(self.y) - T.mean(self.conv1.logLs)
        self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff*T.mean((self.x - self.conv1.data_reconstructed)**2)
        self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed)**2)
        self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.conv2.lambdas,] + [self.conv1.lambdas,] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.conv2.lambdas_new = self.conv2.lambdas - learning_rate * grads[0]
        self.conv1.lambdas_new = self.conv1.lambdas - learning_rate * grads[1]
        self.softmax_layer.W_new = self.softmax_layer.params[0] - learning_rate * grads[2]
        self.softmax_layer.b_new = self.softmax_layer.params[1] - learning_rate * grads[3]

        self.layers = [self.conv2, self.conv1]

        self.updates = []
        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.lambdas, layer.lambdas_new))
            self.updates.append((layer.amps, layer.amps_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs) #0
            self.output_var.append(layer.betas) #1
            self.output_var.append(layer.lambdas_new) #2
            self.output_var.append(layer.amps_new) #3
            # Concatenate misc
            self.misc.append(layer.latents) #0
            self.misc.append(layer.rs) #1
            self.misc.append(layer.latents_masked) #2
            # Concatenate init_var
            self.init_var.append(layer.betas) #0
            self.init_var.append(layer.lambdas) #1
            self.init_var.append(layer.amps) #2

        self.output_var.append(self.softmax_layer.W_new)
        self.output_var.append(self.softmax_layer.b_new)
        self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
        self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))