__author__ = 'heatherseeba'

import numpy as np
from scipy.linalg import inv, norm
from pylab import *
import os

def compute_betas(lambdas, psis, sigma):
    '''
    Compute formulas computing betas in Gharamani & Hinton's paper and Bishop's book
    :param lambdas: loading matrix in MFA
    :param psis: noise covariance in MFA
    :return: all the betas
    '''
    # Compute Gharamani & Hinton's betas for MFA
    psiI = inv(np.diag(psis))
    lam  = lambdas
    lamT = lam.T
    M = np.shape(lam)[1]
    step = inv(np.eye(M) + np.dot(lamT,np.dot(psiI,lam)))
    step = np.dot(step,np.dot(lamT,psiI))
    step = np.dot(psiI,np.dot(lam,step))
    covI = psiI - step
    betas_gh = np.dot(lamT, covI)

    # Compute Bishop's betas for MFA
    step = []
    step = inv(np.eye(M) + np.dot(lamT,np.dot(psiI,lam)))
    step = np.dot(step,lamT)
    betas_b = np.dot(step,psiI)

    # Compute PPCA betas
    step = inv(np.dot(lamT, lam) + (sigma**2)*np.eye(M))
    betas_ppca = np.dot(step, lamT)

    return betas_gh, betas_b, betas_ppca

if __name__ == '__main__':
    D = 4
    M = 3
    lambdas = np.random.randn(D,M)
    out_dir = '/Users/heatherseeba/repos/psychophysics/python/figures'

    # compare all betas when noise is isotropic
    psis = []
    d_gh_b = []
    d_gh_ppca = []
    d_b_ppca = []
    SIGMA = np.power(10.0, range(0, -12, -1))
    for sigma in SIGMA:
        psis = (sigma**2)*np.ones((D,1))
        psis = psis.reshape(-1)
        betas_gh, betas_b, betas_ppca = compute_betas(lambdas, psis, sigma)
        d_gh_b.append(norm(betas_gh - betas_b))
        d_gh_ppca.append(norm(betas_gh - betas_ppca))
        d_b_ppca.append(norm(betas_b - betas_ppca))

    fig = figure()
    plot(SIGMA, d_gh_b, 'bD-', linewidth=10, markersize=10, label='G&H vs Bishop')
    hold(True)
    plot(SIGMA, d_gh_b, 'ro-', linewidth=7, markersize=7, label='G&H vs PPCA')
    plot(SIGMA, d_b_ppca, 'gx-', linewidth=4, markersize=4, label='Bishop vs PPCA')
    legend()
    grid()
    title('Compare betas: isotropic noise')
    xlabel('Sigma')
    ylabel('Absolute error')
    xscale('log')
    yscale('log')
    fig.savefig(os.path.join(out_dir, 'Betas_Comparison_Isotropic_Noise.png'))
    hold(False)
    close()

    # compare all betas when noise is not necessarily isotropic in G&H and Bishop cases
    psis = []
    d_gh_b = []
    d_gh_ppca = []
    d_b_ppca = []
    SIGMA = np.power(10.0, range(0, -20, -1))
    for sigma in SIGMA:
        psis = (sigma**2)*np.random.randn(D,1)
        psis = psis.reshape(-1)
        betas_gh, betas_b, betas_ppca = compute_betas(lambdas, psis, sigma)
        d_gh_b.append(norm(betas_gh - betas_b))
        d_gh_ppca.append(norm(betas_gh - betas_ppca))
        d_b_ppca.append(norm(betas_b - betas_ppca))

    fig = figure()
    plot(SIGMA, d_gh_b, 'bD-', linewidth=10, label='G&H vs Bishop')
    hold(True)
    plot(SIGMA, d_gh_ppca, 'ro-', linewidth=7, label='G&H vs PPCA')
    plot(SIGMA, d_b_ppca, 'gx-', linewidth=4, label='Bishop vs PPCA')
    legend()
    grid()
    title('Compare betas: non-isotropic noise')
    xlabel('Sigma')
    ylabel('Absolute error')
    xscale('log')
    yscale('log')
    fig.savefig(os.path.join(out_dir, 'Betas_Comparison_Non-Isotropic_Noise.png'))
    hold(False)
    close()

    # compare all betas when noise is very small
    psis = []
    sigma = 1e-10
    psis = (sigma**2)*np.random.randn(D,1)
    psis = psis.reshape(-1)
    betas_gh, betas_b, betas_ppca = compute_betas(lambdas, psis, sigma)
    print 'When psi is small and not necessarily isotropic for G&B and Bishop cases: The difference between betas_gh and betas_b ='
    print norm(betas_gh - betas_b)
    print 'When psi is small and not necessarily isotropic for G&B and Bishop cases: The difference between betas_gh and betas_ppca ='
    print norm(betas_gh - betas_ppca)
    print 'When psi is small and not necessarily isotropic for G&B and Bishop cases: The difference between betas_b and betas_ppca ='
    print norm(betas_b - betas_ppca)
    print '-------------------------------------------------------------------'