__author__ = 'minhtannguyen'

import matplotlib as mpl
mpl.use('Agg')

import glob

import numpy as np
np.set_printoptions(threshold='nan')

import itertools

from scipy import misc
import cPickle

from theano.tensor.nnet.neighbours import images2neibs
from theano.tensor.signal import pool

#import copy

from kmeans_init import klp_kmeans
#from plot_mofa import *
from swmfa import *

from old_codes.nn_functions_stable import LogisticRegression
#from lasagne.layers import InputLayer, FeatureWTALayer

class ConvMofa(SwMofa):
    """
    EM Algorithm for Convolutional Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes

    """
    def __init__(self, x, y, dat, K, M, W, H, w, h, Cin, Ni, amps_val_init=None,
                 lambdas_val_init=None, lambda_covs_val_init=None,
                 covs_val_init=None, inv_covs_val_init=None,
                 psis_val_init=None, means_val_init=None,
                 PPCA=False,lock_psis=False,
                 update_mode='Hinton', em_mode='soft',
                 rs_clip = 0.0,
                 max_condition_number=1.e3,
                 init=True,init_ppca=False):

        # required
        self.K     = K # number of clusters
        self.M     = M # latent dimensionality
        self.data_4D  = x
        self.labels = y # set labels
        self.Ni     = Ni # no. of images
        self.w     = w # width of filters
        self.h     = h # height of filters
        self.Cin   = Cin # number of channels in the image
        self.D     = self.h * self.w * self.Cin # patch size
        self.W     = W # width of image
        self.H     = H # height of image
        self.Np = (self.H - self.h + 1)*(self.W - self.w + 1) # no. of patches per image
        self.N = self.Ni * self.Np # total no. of patches
        self.Nl = self.N # total no. of hidden units
        self.update_mode  = update_mode
        self.lambdas_val_init = lambdas_val_init
        self.amps_val_init = amps_val_init
        self.em_mode = em_mode

        # options
        self.PPCA                 = PPCA
        self.lock_psis            = lock_psis
        self.rs_clip              = rs_clip
        self.max_condition_number = max_condition_number
        assert rs_clip >= 0.0

        # patchification
        print 'Start patchification'
        start_time = time.time()
        # self.data = images2neibs_3D(ten4=self.data_4D, neib_shape=(self.h, self.w), neib_step=(1,1), mode='valid')
        self.data = images2neibs(ten4=self.data_4D, neib_shape=(self.h, self.w), neib_step=(1,1), mode='valid')
        # self.data = T.reshape(self.data,newshape=(self.Ni, self.Cin, self.Np, self.h * self.w))
        # self.data = self.data.dimshuffle(0,2,1,3)
        # self.data = T.reshape(self.data, newshape=(self.Ni, self.Np, self.D))
        # self.data = T.reshape(self.data, newshape=(self.N, self.D))
        self.dataT = self.data.T
        self.createPatches = theano.function([], self.data,
                                         updates=[], on_unused_input='ignore',
                                         givens={x:dat}) # this function creates patches from data

        data_pat_val = self.createPatches()
        print 'Shape of data'
        print np.shape(data_pat_val)
        stop_time = time.time()
        print 'patchification takes %f seconds' %(stop_time - start_time)

        # # Test patchification
        #
        # for i in xrange(200):
        #     d = data_val[i].transpose(1,2,0)
        #     imshow(d)
        #     axis('off')
        #     savefig('TestImage_%i.jpg'%i)
        #     close()
        #
        # Nsample = np.shape(data_pat_val)[0]
        # for i in xrange(200):
        #     d = np.reshape(data_pat_val[i], (3,26,26))
        #     d = d.transpose(1,2,0)
        #     imshow(d)
        #     axis('off')
        #     savefig('TestPatch_%i.jpg'%i)
        #     close()

        # initialize the means
        print self.N
        print 'Start kmeans'
        start_time = time.time()
        # if initial values for means are not provided, use K-means to initialize the means
        if means_val_init == None:
            self.means_val_init = klp_kmeans(data=data_pat_val, cluster_num=self.K, alpha=0.001, epochs=100, batch=2000, verbose=False, use_gpu=False)
        else:
            self.means_val_init = means_val_init

        self.means_val_init[0] = 0.0 # don't care the first cluster
        stop_time = time.time()
        print 'k-means takes %f seconds' %(stop_time - start_time)

        # initialize psis
        print 'Start computing psis'
        start_time = time.time()
        # if initial values for psis are not provided, use variance of the data to initialize psis
        if psis_val_init == None:
            my_var_init = np.var(data_pat_val, axis=0)
            self.psis_val_init = np.tile(my_var_init[None,:],(self.K,1))
            print 'check psis_val_init'
        else:
            self.psis_val_init = psis_val_init

        # del my_var_init
        del data_pat_val

        stop_time = time.time()
        print 'compute psis takes %f seconds' %(stop_time - start_time)

        # empty arrays to be filled
        self.betas       = theano.shared(value=np.zeros((self.K - 1, self.M, self.D), dtype=theano.config.floatX),
                                         name='betas', borrow=True) # corresponds to the filters in DCNs
        self.latents     = theano.shared(value=np.zeros((self.K - 1, self.M, self.N), dtype=theano.config.floatX),
                                         name='latents', borrow=True) # a.k.a, hidden variables
        self.latent_covs = theano.shared(value=np.zeros((self.K - 1, self.M, self.M, self.N), dtype=theano.config.floatX),
                                         name='latent_covs', borrow=True) # covariance of hidden variables
        self.means       = theano.shared(value=np.asarray(self.means_val_init, dtype=theano.config.floatX),
                                         name='means', borrow=True) # means
        self.rs          = theano.shared(value=np.zeros((self.K, self.Nl), dtype=theano.config.floatX),
                                         name='rs', borrow=True) # responsibilities

        # initialize
        # if init = True, use _initialize to initialize the parameters.
        # otherwise, use ..._val_init from the input list
        if init:
            self._initialize(init_ppca)
        else:
            self.amps = theano.shared(value=np.asarray(amps_val_init, dtype=theano.config.floatX), name='amps', borrow=True)
            self.lambdas = theano.shared(value=np.asarray(lambdas_val_init, dtype=theano.config.floatX), name='lambdas', borrow=True)
            self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val_init, dtype=theano.config.floatX), name='lambdas_covs', borrow=True)
            self.covs = theano.shared(value=np.asarray(covs_val_init, dtype=theano.config.floatX), name='covs', borrow=True)
            self.inv_covs = theano.shared(value=np.asarray(inv_covs_val_init, dtype=theano.config.floatX), name='inv_covs', borrow=True)

    def _initialize(self, init_ppca, maxiter=200, tol=1e-4):
        """
        Run K-means
        """

        # Randomly assign factor loadings
        if self.lambdas_val_init == None:
            lambdas_value = np.random.randn(self.K,self.D,self.M) / \
                np.sqrt(self.max_condition_number)
        else:
            lambdas_value = self.lambdas_val_init

        lambdas_value[0] = 0.0*lambdas_value[0] # don't care the first cluster
        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas', borrow=True)

        # Set (high rank) variance to variance of all data, along a dimension
        self.psis= theano.shared(value=np.asarray(self.psis_val_init, dtype=theano.config.floatX), name='psis', borrow=True)

        # Set the initial lambda_covs, covs and compute inv_covs
        covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        lambda_covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        inv_covs_val = 0. * covs_val

        if self.em_mode == 'soft':
            # compute the inv_covs using matrix inversion lemma
            for k in xrange(1,self.K):
                covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T) + \
                    np.diag(self.psis_val_init[k])
                lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                psiI = inv(np.diag(self.psis_val_init[k]))
                lam  = lambdas_value[k]
                lamT = lam.T
                step = inv(np.eye(self.M) + np.dot(lamT,np.dot(psiI,lam)))
                step = np.dot(step,np.dot(lamT,psiI))
                step = np.dot(psiI,np.dot(lam,step))
                inv_covs_val[k] = psiI - step
        else:
            for k in range(1,self.K):
                covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
                inv_covs_val[k] = np.linalg.pinv(lambda_covs_val[k])

        self.covs = theano.shared(value=np.asarray(covs_val, dtype=theano.config.floatX),
                                  name='covs', borrow=True)

        self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val, dtype=theano.config.floatX),
                                  name='lambda_covs', borrow=True)

        self.inv_covs = theano.shared(value=np.asarray(inv_covs_val, dtype=theano.config.floatX),
                                  name='inv_covs', borrow=True)

        # Randomly assign the amplitudes.
        if self.amps_val_init == None:
            amps_val = np.random.rand(self.K)
            amps_val /= np.sum(amps_val)
            self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                      name='amps', borrow=True)
        else:
            self.amps = theano.shared(value=np.asarray(self.amps_val_init, dtype=theano.config.floatX), name='amps', borrow=True)

    def take_EM_step(self):
        """
        Do one E step and then do one M step.
        """
        self._E_step()
        self._M_step()

    def _E_step(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """

        # Bottom-Up

        # compute the lambda dagger
        if self.em_mode == 'soft':
            self.betas, updates4 = theano.scan(fn=lambda l, ic: T.dot(l.T, ic),
                                     outputs_info=None,
                                     sequences=[self.lambdas[1:], self.inv_covs[1:]])
            betameans = T.sum(self.betas[:,0,:]*self.means[1:], axis=1)

            latents_conv = T.dot(self.betas[:,0,:], self.dataT)
        else:
            self.betas, updates4 = theano.scan(fn=lambda l: T.dot(1.0/(T.dot(l.T,l)),l.T),
                                     outputs_info=None,
                                     sequences=self.lambdas[1:])

            betameans = T.sum(self.betas[:,0,:]*self.means[1:], axis=1)

            latents_conv = T.dot(self.betas[:,0,:], self.dataT)

        self.latents = latents_conv[:,None,:] - betameans.dimshuffle(0, 'x', 'x')
        self.latents_conv = latents_conv

        # compute E[zz'|x] using eq. 14
        # step 1 computes E[z|x](E[z|x])'
        # step 2 computes beta_j*lambda_j
        # step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
        #                              outputs_info=None,
        #                              sequences=self.latents)
        step1, updates6 = theano.scan(fn=lambda lt: lt[:, None, :] * lt[None, :, :],
                                            outputs_info=None,
                                            sequences=self.latents)
        step2, updates7 = theano.scan(fn=lambda b, l: T.dot(b, l),
                                      outputs_info=None,
                                      sequences=[self.betas, self.lambdas[1:]])
        self.latent_covs, updates8_ss = theano.scan(fn=lambda s2, s1: T.eye(self.M,self.M)[:,:,None] - s2[:,:,None] + s1,
                                       outputs_info=None,
                                       sequences=[step2, step1])

        # compute the responsibilities and log-likelihood using eq. 12
        self.neg_dist = self._calc_probs()

        # manipulate logrs using maxpoolings
        self.rs_conv = T.reshape(self.neg_dist, newshape=(self.K-1, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        self.rs_conv = self.rs_conv.dimshuffle(1,0,2,3)

        # self.rs_conv_new_temp = pool.max_pool_2d_same_size(input=self.rs_conv, patch_size=(2,2))
        # self.rs_conv_new_2d = T.reshape(self.rs_conv_new_temp.dimshuffle(1,0,2,3), newshape=(self.K - 1, self.Nl))
        # self.rs_conv_new_2d_max = T.max(self.rs_conv_new_2d, axis=0)
        # self.rs_conv_new_2d_temp = T.ge(self.rs_conv_new_2d,self.rs_conv_new_2d_max) * T.neq(0,self.rs_conv_new_2d)
        # self.rs_conv_new = T.reshape(self.rs_conv_new_2d_temp, newshape=(self.K-1, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        # self.rs_conv_new = self.rs_conv_new.dimshuffle(1,0,2,3)

        #self.rs_conv_new = sw_maxpool_binary(input=self.rs_conv, ds=(2*self.h-1, 2*self.w-1), ignore_border=True, st=(1,1), padding=(self.h-1, self.w-1), mode='max')
        self.rs_conv_new = sw_maxpool_binary(input=self.rs_conv, ds=(3, 3), ignore_border=True, st=(1,1), padding=(1, 1), mode='max')

        self.rs_sw_temp = T.concatenate([theano.shared(value=np.zeros((self.Ni, 1 ,self.H - self.h + 1, self.W - self.w + 1),
                                                                       dtype=theano.config.floatX),borrow=True), self.rs_conv_new],
                                        axis=1)
        self.rs_temp = T.reshape(self.rs_sw_temp.dimshuffle(1,0,2,3), newshape=(self.K, self.Nl))
        self.rs_nonzero_max = T.max(self.rs_temp[1:], axis=0)
        self.rs_zero_new = T.ge(0,self.rs_nonzero_max)
        self.rs = T.set_subtensor(self.rs_temp[0], self.rs_zero_new)

        latents_rs = self.latents[:,0,:] * self.rs[1:]
        latents_rs = T.reshape(latents_rs, (self.K -1, self.Ni, self.H - self.h + 1, self.W - self.w + 1))
        latents_rs = T.transpose(latents_rs, (1,0,2,3))
        self.latents_rs = pool.pool_2d(input=latents_rs,
                                            ds=(2,2), ignore_border=True, mode='max')

        self.logLs = 0.5*T.sum(self.neg_dist * self.rs[1:], axis=0)

        # compute the pi's
        self.sumrs = T.sum(self.rs, axis=1)
        self.amps_new, updates18 = theano.scan(fn=lambda s: s / float(self.Nl),
                                outputs_info=None,
                                sequences=self.sumrs)

        self.indx_good_cluster = T.cast((self.amps_new[1:] > 1.0/self.N).nonzero(), 'int32')[0]

    def _Top_Down(self):
        # Top-Down
        lambdalatents_E, updates910 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[self.lambdas[1:], self.latents])
        xT = lambdalatents_E + self.means[1:,:,None]
        xT = self.rs[1:,None,:] * xT
        self.xT = T.sum(xT, axis=0)

    def _M_step(self):
        """
        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """
        if self.update_mode == 'Ross_Fadely':
            # update means - implicitly included in eq. 15
            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[self.lambdas, self.latents])

            means_new, updates10 = theano.scan(fn=lambda resp, llt, s: T.sum(resp * (self.dataT - llt), axis=1) / s,
                                     outputs_info=None,
                                     sequences=[self.rs[1:], lambdalatents[1:], self.sumrs[1:]])

            self.means_new = T.set_subtensor(self.means[1:], means_new)

            # update lambdas using eq.15
            zeroed, updates11 = theano.scan(fn=lambda m: self.dataT - m[:,None],
                                 outputs_info=None,
                                 sequences=self.means_new)

            lambdas_new, updates12 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixInverse()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[self.latents[1:], self.rs[1:], self.latent_covs[1:], zeroed[1:]])

            self.lambdas_new = T.set_subtensor(self.lambdas[1:], lambdas_new)

            # update psis using eq. 16
            psis, updates13 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                               outputs_info=None,
                               sequences=[self.rs, zeroed, lambdalatents, self.sumrs])
        else:
            latents_tilde = T.concatenate([self.latents,
                                           theano.shared(value=np.ones((self.K-1, 1 ,self.N),
                                                                       dtype=theano.config.floatX),borrow=True)],
                                          axis=1)
            col1 = T.concatenate([self.latent_covs, (self.latents[:,:,None,:]).dimshuffle((0,2,1,3))], axis=1)
            latent_covs_tilde = T.concatenate([col1, latents_tilde[:,:,None,:]], axis=2)
            dataT = T.tile(self.dataT[None,:,:], reps=[self.K-1,1,1])
            lambdas_tilde, updates22 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixPinv()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[latents_tilde[self.indx_good_cluster], self.rs[self.indx_good_cluster + 1], latent_covs_tilde[self.indx_good_cluster], dataT[self.indx_good_cluster]])
            lambdas_new = lambdas_tilde[:,:,0:self.M]
            self.lambdas_new = T.set_subtensor(self.lambdas[self.indx_good_cluster + 1], lambdas_new)
            means_new = lambdas_tilde[:,:,self.M]
            self.means_new = T.set_subtensor(self.means[self.indx_good_cluster + 1], means_new)

            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[lambdas_tilde, latents_tilde[self.indx_good_cluster]])

            # Just for testing #
            correct_lambdas_tilde = T.concatenate([self.lambdas, self.means[:,:,None]],axis=2)
            correct_lambdalatents, updates767 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[correct_lambdas_tilde, latents_tilde])
            ####################

            psis, update23 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                                         outputs_info=None,
                                         sequences=[self.rs[self.indx_good_cluster + 1], dataT[self.indx_good_cluster], lambdalatents, self.sumrs[self.indx_good_cluster + 1]])

        maxpsi, updates14 = theano.scan(fn=lambda p: T.max(p),outputs_info=None, sequences=psis)

        maxlam, updates15 = theano.scan(fn=lambda l: T.max(T.sum(l * l, axis=0)),
                             outputs_info=None,
                             sequences=self.lambdas_new[self.indx_good_cluster + 1])
        minpsi, updates16 = theano.scan(fn=lambda maxp, maxl: T.max([maxp, maxl]) / self.max_condition_number,
                             outputs_info=None,
                             sequences=[maxpsi, maxlam])
        self.psis_new, updates17   = theano.scan(fn=lambda p, minp: T.clip(p, minp, np.Inf),
                                  outputs_info=None,
                                  sequences=[psis, minpsi])
        # make all elements in each rows of psis the same
        if self.PPCA:
            self.psis_new, updates19 = theano.scan(fn=lambda psn: T.mean(psn)*T.ones(self.D),
                                                   outputs_info=None,
                                                   sequences=self.psis_new)

        # make psi for each cluster the same
        if self.lock_psis:
            self.psis_new = T.dot(self.sumrs[self.indx_good_cluster + 1],self.psis_new)/T.sum(self.sumrs[self.indx_good_cluster + 1])
            self.psis_new = (self.psis_new).dimshuffle('x',0)
            self.psis_new = T.tile(self.psis_new,(self.K, 1))

        # update covs
        self._update_covs()

    def take_EG_step(self, learning_rate):

        self._E_step()

        softmax_input = self.latents_rs.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer = LogisticRegression(input=softmax_input, n_in=(self.K - 1)*(self.H - self.h + 1)*(self.W - self.w + 1)/4, n_out=10)

        # the cost we minimize during training is the NLL of the model
        self.cost = self.softmax_layer.negative_log_likelihood(self.labels)

        # create a list of all model parameters to be fit by gradient descent
        self.params = [self.lambdas, self.means] + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.lambdas_new = self.lambdas - learning_rate * grads[0]
        self.means_new = self.means - learning_rate * grads[1]
        self.W_new = self.softmax_layer.params[0] - learning_rate * grads[2]
        self.b_new = self.softmax_layer.params[1] - learning_rate * grads[3]

        latents_tilde = T.concatenate([self.latents,
                                           theano.shared(value=np.ones((self.K-1, 1 ,self.N),
                                                                       dtype=theano.config.floatX),borrow=True)],
                                          axis=1)
        dataT = T.tile(self.dataT[None,:,:], reps=[self.K-1,1,1])

        lambdas_tilde = T.concatenate([self.lambdas_new, self.means_new[:,:,None]], axis=2)

        lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[lambdas_tilde, latents_tilde[self.indx_good_cluster]])

        # Just for testing #
        correct_lambdas_tilde = T.concatenate([self.lambdas, self.means[:,:,None]],axis=2)
        correct_lambdalatents, updates767 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                    outputs_info=None,
                                    sequences=[correct_lambdas_tilde, latents_tilde])
        ####################

        psis, update23 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                                     outputs_info=None,
                                     sequences=[self.rs[self.indx_good_cluster + 1], dataT[self.indx_good_cluster], lambdalatents, self.sumrs[self.indx_good_cluster + 1]])

        maxpsi, updates14 = theano.scan(fn=lambda p: T.max(p),outputs_info=None, sequences=psis)

        maxlam, updates15 = theano.scan(fn=lambda l: T.max(T.sum(l * l, axis=0)),
                             outputs_info=None,
                             sequences=self.lambdas_new[self.indx_good_cluster + 1])
        minpsi, updates16 = theano.scan(fn=lambda maxp, maxl: T.max([maxp, maxl]) / self.max_condition_number,
                             outputs_info=None,
                             sequences=[maxpsi, maxlam])
        self.psis_new, updates17   = theano.scan(fn=lambda p, minp: T.clip(p, minp, np.Inf),
                                  outputs_info=None,
                                  sequences=[psis, minpsi])
        # make all elements in each rows of psis the same
        if self.PPCA:
            self.psis_new, updates19 = theano.scan(fn=lambda psn: T.mean(psn)*T.ones(self.D),
                                                   outputs_info=None,
                                                   sequences=self.psis_new)

        # make psi for each cluster the same
        if self.lock_psis:
            self.psis_new = T.dot(self.sumrs[self.indx_good_cluster + 1],self.psis_new)/T.sum(self.sumrs[self.indx_good_cluster + 1])
            self.psis_new = (self.psis_new).dimshuffle('x',0)
            self.psis_new = T.tile(self.psis_new,(self.K, 1))

        # update covs
        self._update_covs()

    def _update_covs(self):
        """
        Update self.cov for responsibility, logL calc
        """

        # total cov = lambda*lambda' + psi
        self.lambda_covs_new, updates20 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas_new])

        if self.em_mode == 'soft':
            covs_new, updates1 = theano.scan(fn=lambda lc, p: lc + diag(p),
                                              outputs_info=None,
                                              sequences=[self.lambda_covs_new[1:], self.psis_new[1:]])
            self.covs_new = T.set_subtensor(self.covs[1:], covs_new)
            # compute the inv_covs using the matrix inversion lemma
            inv_covs_new, updates2 = theano.scan(fn=lambda l, p: MatrixInverse()(diag(p)) - T.dot(MatrixInverse()(diag(p)),T.dot(l,T.dot(MatrixInverse()(T.eye(self.M) + T.dot(T.transpose(l),T.dot(MatrixInverse()(diag(p)),l))),T.dot(T.transpose(l),MatrixInverse()(diag(p)))))),
                                              outputs_info=None,
                                              sequences=[self.lambdas_new[1:], self.psis_new[1:]])
            self.inv_covs_new = T.set_subtensor(self.inv_covs[1:], inv_covs_new)
        else:
            covs_new, updates1 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                                      outputs_info=None,
                                                      sequences=[self.lambdas_new[1:]])
            self.covs_new = T.set_subtensor(self.covs[1:], covs_new)
            inv_covs_new, updates2 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l))/T.sqr(T.sum(T.sqr(l))),
                                              outputs_info=None,
                                              sequences=self.lambdas_new[1:])
            self.inv_covs_new = T.set_subtensor(self.inv_covs[1:], inv_covs_new)

    def _calc_probs(self):
        """
        Calculate log likelihoods, responsibilities for each datum
        under each component.
        """
        if self.em_mode == 'soft':
            logrs, updates3 = theano.scan(fn=lambda a, c, m, ic: T.log(a) - float(0.5 * np.log(2 * np.pi) * self.D) -
                                                                 0.5 * 2 * T.sum(T.log(diag(Cholesky()(c)))) -
                                                                 0.5 * T.sum((self.data - m).T * T.dot(ic, (self.data - m).T),
                                                                             axis=0),
                                outputs_info=None,
                                sequences=[self.amps[1:], self.covs[1:], self.means[1:], self.inv_covs[1:]])
            L = self._log_sum(logrs)
            logrs -= L[None, :]
            if self.rs_clip > 0.0:
                logrs = T.clip(logrs,T.log(self.rs_clip),np.Inf)

            return T.exp(logrs)
        else:
            ortho_vec, updates333 = theano.scan(fn=lambda m,lam: (self.data - m).T - T.dot(T.dot(lam, lam.T),(self.data - m).T)/T.sum(lam*lam),
                                outputs_info=None,
                                sequences=[self.means[1:], self.lambdas[1:]])

            rs, updates3 = theano.scan(fn=lambda d: -T.sum(d*d, axis=0),
                                outputs_info=None,
                                sequences=ortho_vec)
            return rs

    def _log_sum(self,loglikes):
        """
        Calculate sum of log likelihoods
        """
        a = T.max(loglikes, axis=0)
        return a + T.log(T.sum(T.exp(loglikes - a[None, :]), axis=0))

    def fit(self, x, dat, tol, maxiter, verbose, mode='NLL'):
        """
        Train the model using the whole dataset
        """
        # take one E step and then one M step
        print 'take EM step'
        self.take_EM_step()
        print 'finish taking EM step'

        # updates all parameters after each run
        updates = []
        updates.append((self.means, self.means_new))
        updates.append((self.lambdas, self.lambdas_new))
        updates.append((self.psis, self.psis_new))
        updates.append((self.covs, self.covs_new))
        updates.append((self.inv_covs, self.inv_covs_new))
        updates.append((self.amps, self.amps_new))
        updates.append((self.lambda_covs, self.lambda_covs_new))

        # construct a function that infers the z, zz' and responsibilities in the E step and then uses updates
        # to update lambdas, means, psis, ... (see update list above)
        print 'build the update function'
        output_one_iter = theano.function([], [self.logLs, self.rs, self.betas, self.latents, self.latent_covs, self.psis_new, self.sumrs, self.amps_new, self.lambdas_new],
                                         updates=updates, on_unused_input='ignore',
                                         givens={x:dat})
        print 'finish building the update function'

        # start timing
        start_time = timeit.default_timer()

        # run the EM
        L = None
        NegLogLs_vec = []
        epochs = []
        LC = copy.copy(self.lambda_covs.get_value())
        PS = copy.copy(self.psis.get_value())
        MU = copy.copy(self.means.get_value())
        LogLs_vec = []
        dPS_vec = []
        dLC_vec = []
        dMU_vec = []

        print 'start training loop'
        for i in xrange(maxiter):
            print 'iteration %d' %i
            logLs_val, rs_val, betas_val, latents_val, latent_covs_val, psis_val, sumrs_val, amps_val, lambdas_new_val = output_one_iter()
            newL = np.sum(logLs_val)
            # print initial log-likelihood
            if i == 0 and verbose:
                print("Initial NLL=", -newL)
            newLC = self.lambda_covs.get_value()
            newPS = self.psis.get_value()
            newMU = self.means.get_value()
            print amps_val
            # print lambdas_new_val
            dLC = norm(newLC - LC)/norm(LC)
            # dPS = norm(newPS - PS)/norm(PS)
            dPS = tol/2
            dMU = norm(newMU - MU)/norm(MU)
            # stopping
            if mode == 'NLL':
                if L!=None:
                    dL = np.abs((newL-L)/L)
                    if i > 5 and dL < tol:
                        break
                L = newL
            elif self.em_mode == 'soft':
                if i > 5 and dLC < tol and dPS < tol and dMU < tol:
                    break
            else:
                if i > 5 and dLC < tol and dMU < tol:
                    break

            LC = copy.copy(newLC)
            PS = copy.copy(newPS)
            MU = copy.copy(newMU)
            dLC_vec.append(dLC)
            dPS_vec.append(dPS)
            dMU_vec.append(dMU)
            NegLogLs_vec.append(-newL)
            epochs.append(i)

        # let us know if EM converges or not and after how many epochs
        if i < maxiter - 1:
            if verbose:
                print("EM converged after {0} iterations".format(i))
                print("Final NLL={0}".format(-newL))
        else:
            print("Warning:EM didn't converge after {0} iterations".format(i))

        # stop timing
        print '\n'
        print("--- %s seconds ---" % (timeit.default_timer() - start_time))
        print '\n'

        return NegLogLs_vec, dLC_vec, dMU_vec, dPS_vec, epochs

    def batch_fit(self, x, y, dat, label, dat_test, label_test, dat_valid, label_valid, K, batch_size, tol, max_epochs, verbose, result_dir, learning_rate, mode='NLL', train_method='EM'):
        N, Cin, h, w = np.shape(dat)
        D = h*w
        n_batches = N/batch_size
        N_valid, Cin_valid, h_valid, w_valid = np.shape(dat_valid)
        n_valid_batches = N_valid/batch_size
        N_test, Cin_test, h_test, w_test = np.shape(dat_test)
        n_test_batches = N_test/batch_size


        index = T.iscalar()
        shared_dat = theano.shared(dat)
        shared_dat_test = theano.shared(dat_test)
        shared_dat_valid = theano.shared(dat_valid)
        shared_label = theano.shared(label)
        shared_label_test = theano.shared(label_test)
        shared_label_valid = theano.shared(label_valid)

        if train_method == 'EM':
            print 'take EM'
            self.take_EM_step()
            print 'finish taking EM'

            updates = []
            updates.append((self.means, self.means_new))
            updates.append((self.lambdas, self.lambdas_new))
            updates.append((self.psis, self.psis_new))
            updates.append((self.covs, self.covs_new))
            updates.append((self.inv_covs, self.inv_covs_new))
            updates.append((self.amps, self.amps_new))
            updates.append((self.lambda_covs, self.lambda_covs_new))

            output_one_iter = theano.function([index], [self.logLs, self.rs, self.betas, self.latents,
                                                        self.latent_covs, self.sumrs,
                                                        self.means_new, self.covs_new, self.inv_covs_new,
                                                        self.lambdas_new, self.amps_new, self.psis_new, self.lambda_covs_new, self.data],
                                              updates=updates, on_unused_input='ignore',
                                              givens={x: shared_dat[index * batch_size: (index + 1) * batch_size,:,:,:]})

            start_time = timeit.default_timer()


            done_looping = False
            NL = None
            NegLogLs_vec = []
            epochs = []
            epoch = 0
            iterat = 0
            iterat_vec = []
            LC = copy.copy(self.lambda_covs.get_value())
            PS = copy.copy(self.psis.get_value())
            MU = copy.copy(self.means.get_value())
            dPS_vec = []
            dLC_vec = []
            dMU_vec = []

            LCiter = copy.copy(self.lambda_covs.get_value())
            MUiter = copy.copy(self.means.get_value())
            dLCiter_vec = []
            dMUiter_vec = []

            while (epoch < max_epochs) and (not done_looping):
                epoch = epoch + 1
                print 'Epoch %d' %epoch
                logLs_val_vec = []
                for minibatch_index in xrange(n_batches):
                    print minibatch_index
                    iterat = iterat + 1
                    iterat_vec.append(iterat)

                    logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, \
                    means_val, covs_val, inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, data_val \
                        = output_one_iter(minibatch_index)
                    logLs_val_vec.append(logLs_val)

                    # means_val = np.asarray(means_val,dtype=theano.config.floatX)
                    # lambdas_val = np.asarray(lambdas_val,dtype=theano.config.floatX)
                    # betas_val = np.asarray(betas_val,dtype=theano.config.floatX)
                    # psis_val = np.asarray(psis_val,dtype=theano.config.floatX)
                    # covs_val = np.asarray(covs_val,dtype=theano.config.floatX)
                    # inv_covs_val = np.asarray(inv_covs_val,dtype=theano.config.floatX)
                    # amps_val = np.asarray(amps_val,dtype=theano.config.floatX)
                    # lambda_covs_val = np.asarray(lambda_covs_val,dtype=theano.config.floatX)
                    #
                    # dLC = norm(lambda_covs_val - LCiter)/norm(LCiter)
                    # dMU = norm(means_val - MUiter)/norm(MUiter)
                    # dLCiter_vec.append(dLC)
                    # dMUiter_vec.append(dMU)
                    # LCiter = copy.copy(lambda_covs_val)
                    # MUiter = copy.copy(means_val)
                    #
                    # if iterat % 5 == 1:
                    #     result_file = os.path.join(result_dir, 'EM_results_iter_%i.npz' %iterat)
                    #     np.savez(result_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, psis_val=psis_val,
                    #              covs_val=covs_val, inv_covs_val=inv_covs_val, lambda_covs_val=lambda_covs_val, amps_val=amps_val)
                    #
                    #     fig2 = figure()
                    #     plot(iterat_vec, dLCiter_vec, 'b-', label='Lambda_Covs')
                    #     plot(iterat_vec, dMUiter_vec, 'g-', label='Mu')
                    #     legend()
                    #     xlabel('Iter')
                    #     ylabel('Relative Difference')
                    #     title('Convergence_of_Parameters_from_my_MFA_BatchSize_%i' % batch_size)
                    #     fig2.savefig(os.path.join(result_dir, 'Convergence_of_Parameters_from_my_MFA_BatchSize_%i_from_1st_iter.png' % batch_size))
                    #     close()
                    #
                    #     fig3 = figure()
                    #     plot(iterat_vec[1:], dLCiter_vec[1:], 'b-', label='Lambda_Covs')
                    #     plot(iterat_vec[1:], dMUiter_vec[1:], 'g-', label='Mu')
                    #     legend()
                    #     xlabel('Iter')
                    #     ylabel('Relative Difference')
                    #     title('Convergence_of_Parameters_from_my_MFA_BatchSize_%i_from_2nd_iter' % batch_size)
                    #     fig3.savefig(os.path.join(result_dir, 'Convergence_of_Parameters_from_my_MFA_BatchSize_%i_from_2nd_iter.png' % batch_size))
                    #     close()
                    #
                    #     forPlot_file = os.path.join(result_dir, 'for_Plotting_iter.npz')
                    #     np.savez(forPlot_file, epoch_vec=iterat_vec, dLC_vec=dLCiter_vec, dMU_vec=dMUiter_vec)

                # logLs_val_whole_dataset = cal_logLs(K=K, N=N, D=D, data=dat,
                #                                    covs=covs_val, means=means_val, inv_covs=inv_covs_val, pi=amps_val)
                # newL = np.sum(logLs_val_whole_dataset)
                logLs_val_vec = np.asarray(logLs_val_vec)
                newNL = -np.mean(logLs_val_vec)

                if epoch == 0 and verbose:
                    print("Initial NLL=", newNL)

                newLC = self.lambda_covs.get_value()
                newPS = self.psis.get_value()
                newMU = self.means.get_value()
                dLC = norm(newLC - LC)/norm(LC)
                # dPS = norm(newPS - PS)/norm(PS)
                dPS = tol/2
                dMU = norm(newMU - MU)/norm(MU)

                # print amps_val

                if mode == 'NLL':
                    if NL!=None:
                        dNL = np.abs((newNL-NL)/NL)
                        if epoch > 5 and dNL < tol:
                            break
                    NL = newNL
                elif self.em_mode == 'soft':
                    if epoch > 5 and dLC < tol and dPS < tol and dMU < tol:
                        break
                else:
                    if dMU < tol and dLC < tol:
                        break

                LC = copy.copy(newLC)
                PS = copy.copy(newPS)
                MU = copy.copy(newMU)
                dLC_vec.append(dLC)
                dPS_vec.append(dPS)
                dMU_vec.append(dMU)
                NegLogLs_vec.append(newNL)
                epochs.append(epoch)

                means_val = np.asarray(means_val,dtype=theano.config.floatX)
                lambdas_val = np.asarray(lambdas_val,dtype=theano.config.floatX)
                betas_val = np.asarray(betas_val,dtype=theano.config.floatX)
                psis_val = np.asarray(psis_val,dtype=theano.config.floatX)
                covs_val = np.asarray(covs_val,dtype=theano.config.floatX)
                inv_covs_val = np.asarray(inv_covs_val,dtype=theano.config.floatX)
                amps_val = np.asarray(amps_val,dtype=theano.config.floatX)
                lambda_covs_val = np.asarray(lambda_covs_val,dtype=theano.config.floatX)

                result_file = os.path.join(result_dir, 'EM_results_epoch_%i.npz' %epoch)
                np.savez(result_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, psis_val=psis_val,
                         covs_val=covs_val, inv_covs_val=inv_covs_val, lambda_covs_val=lambda_covs_val, amps_val=amps_val)

                forPlot_file = os.path.join(result_dir, 'for_Plotting_epoch_%i.npz' %epoch)
                np.savez(forPlot_file, epoch_vec=epochs, NegLogLs_vec=NegLogLs_vec)

                fig1 = figure()
                plot(epochs, NegLogLs_vec)
                xlabel('Epoch')
                ylabel('Negative LogLikelihood')
                title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % batch_size)
                fig1.savefig(os.path.join(result_dir,'Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i.png' % batch_size))
                close()

                fig2 = figure()
                plot(epochs, dLC_vec, 'bo-', label='Lambda_Covs')
                plot(epochs, dMU_vec, 'go-', label='Mu')
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_from_my_MFA_BatchSize_%i' % batch_size)
                fig2.savefig(os.path.join(result_dir, 'Convergence_of_Parameters_from_my_MFA_BatchSize_%i.png' % batch_size))
                close()

            if epoch < max_epochs - 1:
                if verbose:
                    print("EM converged after {0} epochs".format(epoch))
                    print("Final NLL={0}".format(newNL))
            else:
                print("Warning:EM didn't converge after {0} epochs".format(epoch))

            print '\n'
            print("--- %s seconds ---" % (timeit.default_timer() - start_time))
            print '\n'

        else:
            print 'take EG'
            self.take_EG_step(learning_rate=learning_rate)
            print 'finish taking EG'

            updates = []
            updates.append((self.means, self.means_new))
            updates.append((self.lambdas, self.lambdas_new))
            updates.append((self.psis, self.psis_new))
            updates.append((self.covs, self.covs_new))
            updates.append((self.inv_covs, self.inv_covs_new))
            updates.append((self.amps, self.amps_new))
            updates.append((self.lambda_covs, self.lambda_covs_new))
            updates.append((self.softmax_layer.params[0], self.W_new))
            updates.append((self.softmax_layer.params[1], self.b_new))

            output_one_iter = theano.function([index], [self.cost, self.logLs, self.rs, self.betas, self.latents,
                                                        self.latent_covs, self.sumrs,
                                                        self.means_new, self.covs_new, self.inv_covs_new,
                                                        self.lambdas_new, self.amps_new, self.psis_new, self.lambda_covs_new, self.data],
                                              updates=updates, on_unused_input='ignore',
                                              givens={x: shared_dat[index * batch_size: (index + 1) * batch_size,:,:,:],
                                                      y: shared_label[index * batch_size: (index + 1) * batch_size]})

            test_model = theano.function([index], self.softmax_layer.errors(y),
                                         givens={x: shared_dat_test[index * batch_size: (index + 1) * batch_size],
                                                 y: shared_label_test[index * batch_size: (index + 1) * batch_size]})

            validate_model = theano.function([index], self.softmax_layer.errors(y),
                                             givens={x: shared_dat_valid[index * batch_size: (index + 1) * batch_size],
                                                     y: shared_label_valid[index * batch_size: (index + 1) * batch_size]})


            done_looping = False
            NL = None
            NegLogLs_vec = []
            epochs = []
            epoch = -1
            LC = copy.copy(self.lambda_covs.get_value())
            PS = copy.copy(self.psis.get_value())
            MU = copy.copy(self.means.get_value())
            dPS_vec = []
            dLC_vec = []
            dMU_vec = []

            patience = 10000  # look as this many examples regardless
            patience_increase = 2  # wait this much longer when a new best is
                                   # found
            improvement_threshold = 0.995  # a relative improvement of this much is
                                           # considered significant
            validation_frequency = min(n_batches, patience / 2)
                                          # go through this many
                                          # minibatche before checking the network
                                          # on the validation set; in this case we
                                          # check every epoch

            best_params = None
            best_validation_loss = np.inf
            best_iter = 0
            test_score = 0.0
            test_score_epoch = 0.0
            test_score_vec = []

            while (epoch < max_epochs) and (not done_looping):
                epoch = epoch + 1
                print 'Epoch %d' %epoch
                logLs_val_vec = []
                for minibatch_index in xrange(n_batches):
                    print minibatch_index
                    iter = epoch*n_batches + minibatch_index

                    cost_val, logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, \
                    means_val, covs_val, inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, data_val \
                        = output_one_iter(minibatch_index)
                    logLs_val_vec.append(logLs_val)

                    if (iter + 1) % 1 == 0:
                        validation_losses = [validate_model(i) for i
                                         in xrange(n_valid_batches)]
                        this_validation_loss = np.mean(validation_losses)
                        print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                              (epoch, minibatch_index + 1, n_batches, \
                               this_validation_loss * 100.))

                        # if we got the best validation score until now
                        if this_validation_loss < best_validation_loss:

                            #improve patience if loss improvement is good enough
                            if this_validation_loss < best_validation_loss *  \
                               improvement_threshold:
                                patience = max(patience, iter * patience_increase)

                            # save best validation score and iteration number
                            best_validation_loss = this_validation_loss
                            best_iter = iter
                            best_params = self.params

                            # test it on the test set
                            test_losses = [test_model(i) for i in xrange(n_test_batches)]
                            test_score = numpy.mean(test_losses)
                            print(('     epoch %i, minibatch %i/%i, test error of best '
                                   'model %f %%') %
                                  (epoch, minibatch_index + 1, n_batches,
                                   test_score * 100.))

                        if (iter + 1) % batch_size == 0:
                            test_losses = [test_model(i) for i in xrange(n_test_batches)]
                            test_score_epoch = numpy.mean(test_losses)
                            test_score_vec.append(test_score_epoch)
                            epochs.append(epoch)

                    if patience <= iter:
                        done_looping = True
                        break

                # logLs_val_whole_dataset = cal_logLs(K=K, N=N, D=D, data=dat,
                #                                    covs=covs_val, means=means_val, inv_covs=inv_covs_val, pi=amps_val)
                # newL = np.sum(logLs_val_whole_dataset)
                logLs_val_vec = np.asarray(logLs_val_vec)
                newNL = -np.mean(logLs_val_vec)

                if epoch == 0 and verbose:
                    print("Initial NLL=", newNL)

                newLC = self.lambda_covs.get_value()
                newPS = self.psis.get_value()
                newMU = self.means.get_value()
                dLC = norm(newLC - LC)/norm(LC)
                # dPS = norm(newPS - PS)/norm(PS)
                dPS = tol/2
                dMU = norm(newMU - MU)/norm(MU)

                # print amps_val

                if mode == 'NLL':
                    if NL!=None:
                        dNL = np.abs((newNL-NL)/NL)
                        if epoch > 5 and dNL < tol:
                            break
                    NL = newNL
                elif self.em_mode == 'soft':
                    if epoch > 5 and dLC < tol and dPS < tol and dMU < tol:
                        break
                else:
                    if dMU < tol and dLC < tol:
                        break

                LC = copy.copy(newLC)
                PS = copy.copy(newPS)
                MU = copy.copy(newMU)
                dLC_vec.append(dLC)
                dPS_vec.append(dPS)
                dMU_vec.append(dMU)
                NegLogLs_vec.append(newNL)
                epochs.append(epoch)

                means_val = np.asarray(means_val,dtype=theano.config.floatX)
                lambdas_val = np.asarray(lambdas_val,dtype=theano.config.floatX)
                betas_val = np.asarray(betas_val,dtype=theano.config.floatX)
                psis_val = np.asarray(psis_val,dtype=theano.config.floatX)
                covs_val = np.asarray(covs_val,dtype=theano.config.floatX)
                inv_covs_val = np.asarray(inv_covs_val,dtype=theano.config.floatX)
                amps_val = np.asarray(amps_val,dtype=theano.config.floatX)
                lambda_covs_val = np.asarray(lambda_covs_val,dtype=theano.config.floatX)

                result_file = os.path.join(result_dir, 'EM_results_epoch_%i.npz' %epoch)
                np.savez(result_file, means_val=means_val, lambdas_val=lambdas_val, betas_val=betas_val, psis_val=psis_val,
                         covs_val=covs_val, inv_covs_val=inv_covs_val, lambda_covs_val=lambda_covs_val, amps_val=amps_val)

            if epoch < max_epochs - 1:
                if verbose:
                    print("EM converged after {0} epochs".format(epoch))
                    print("Final NLL={0}".format(newNL))
            else:
                print("Warning:EM didn't converge after {0} epochs".format(epoch))

            print('Optimization complete.')
            print('Best validation score of %f %% obtained at iteration %i,'\
                  'with test performance %f %%' %
                  (best_validation_loss * 100., best_iter + 1, test_score * 100.))

            save_file = open('cnn_mean_removed.pkl', 'wb')  # this will overwrite current content
            cPickle.dump(best_params, save_file, -1)
            save_file.close()
            fig = figure()
            plot(epochs, test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch-EG')
            fig.savefig('Error-vs-Epoch-EG.png')
            close(fig)

        return betas_val, latents_val, latent_covs_val, \
               means_val, covs_val, inv_covs_val, lambdas_val, psis_val, lambda_covs_val, amps_val, epochs, \
               dLC_vec, dPS_vec, dMU_vec, NegLogLs_vec

    def predict(self, x, dat):
        """
        Predict the labels of new data
        """
        self._E_step()
        getResults = theano.function([], [self.rs, self.means, self.data],
                                         updates=[], on_unused_input='ignore',
                                         givens={x:dat})
        rs_val, means_val, data_val = getResults()
        labels = np.argmax(rs_val,0)
        return labels, means_val, rs_val, data_val

    def _get_masked_activities(self, x, dat):
        """
        Compute the activities given a set of image x
        :param x: input tensor
        :param dat: input images
        :return: activities
        """
        self._E_step()
        getMaskedActivities = theano.function([], self.latents_rs, updates=[], on_unused_input='ignore', givens={x:dat})
        masked_acts_val = getMaskedActivities()
        print 'Shape of masked_acts'
        print np.shape(masked_acts_val)
        return masked_acts_val

    def _get_activities(self, x, dat):
        """
        Compute the activities given a set of image x
        :param x: input tensor
        :param dat: input images
        :return: activities
        """
        self._E_step()
        getActivities = theano.function([], self.latents, updates=[], on_unused_input='ignore', givens={x:dat})
        acts_val = getActivities()
        return acts_val

    def _get_Rs(self, x, dat):
        """
        Compute the activities given a set of image x
        :param x: input tensor
        :param dat: input images
        :return: activities
        """
        self._E_step()
        getRs = theano.function([], self.rs, updates=[], on_unused_input='ignore', givens={x:dat})
        rs_val = getRs()
        return rs_val

    def _get_Rs_activities(self,x,dat):
        """

        :param x:
        :param dat:
        :return:
        """
        self._E_step()
        getRs_acts = theano.function([], [self.rs, self.latents], updates=[], on_unused_input='ignore', givens={x:dat})
        [rs_val, acts_val] = getRs_acts()
        return rs_val, acts_val

    def _get_samples(self, x, dat):
        """

        :param x:
        :param dat:
        :return:
        """

        self._E_step()
        self._Top_Down()
        getxT = theano.function([], self.xT, updates=[], on_unused_input='ignore', givens={x:dat})
        xT_val = getxT()
        x_val = xT_val.T
        return x_val

##############################################################################################################
# Test Code #
##############################################################################################################
import unittest

class ConvSimpleTestCase(SimpleSwMFATestCase):
    """
    Unittests for Convolutional Rendering Model

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls):
        """
        Run before tests are executed.

        Synthesize data and run EM on the synthetic data
        """
        # skip BaseTestCase
        if cls is BaseTestCase:
            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")
        super(BaseTestCase, cls).setUpClass()

        # set internal parameters
        cls.h = 5 # height of filters
        cls.w = 5 # width of filters
        cls.Cin = 3 # depth of filters
        cls.D = cls.w * cls.h * cls.Cin # filters' size
        cls.K = 3 # no. filters
        cls.H = 8 # width of input image
        cls.W = 8 # height of input image
        Nh = cls.H - cls.h + 1 # no. patches along the height dimension ~
        Nw = cls.W - cls.w + 1 # no. patches along the width dimension ~
        cls.Np = Nh * Nw # no. patches /image
        cls.Ni = 50 # number of input images
        cls.N = cls.Ni * cls.Np # total no. of patches
        cls.Nl = cls.N # total no. of hidden units
        cls.M = 1 # dimension of z
        cls.templates = 'gabor' # template to use
        cls.psi_scale = 0
        cls.em_mode = 'hard'
        cls.update_mode = 'hinton'

        maxiter = 30
        tol = 1e-10
        verbose = True

        cls.seed = 2
        np.random.seed(cls.seed)

        lw_bound_h = -(cls.h - 1)
        lw_bound_w = -(cls.w - 1)
        up_bound_h = cls.h
        up_bound_w = cls.w

        # set which patches to render
        cls.matA = np.zeros((cls.Ni, Nh, Nw))
        for n in xrange(cls.Ni):
            coorVec = list(itertools.product(range(cls.H - cls.h + 1), repeat=2))
            szVec = np.size(coorVec)
            while szVec > 0:
                token_val = np.random.randint(0,2)
                if token_val == 0:
                    continue
                else:
                    indx_choice = np.random.randint(0,shape(coorVec)[0])
                    coor = coorVec[indx_choice]
                    cls.matA[n, coor[0], coor[1]] = 1
                    for i in xrange(lw_bound_h, up_bound_h, 1):
                        for j in xrange(lw_bound_w, up_bound_w, 1):
                            if (coor[0] + i, coor[1] + j) in coorVec:
                                coorVec.remove((coor[0] + i, coor[1] + j))
                    szVec = np.size(coorVec)

        cls.matAtest = np.zeros((cls.Ni, Nh, Nw))
        for n in xrange(cls.Ni):
            coorVec = list(itertools.product(range(cls.H - cls.h + 1), repeat=2))
            szVec = np.size(coorVec)
            while szVec > 0:
                token_val = np.random.randint(0,2)
                if token_val == 0:
                    continue
                else:
                    indx_choice = np.random.randint(0,shape(coorVec)[0])
                    coor = coorVec[indx_choice]
                    cls.matAtest[n, coor[0], coor[1]] = 1
                    for i in xrange(lw_bound_h, up_bound_h, 1):
                        for j in xrange(lw_bound_w, up_bound_w, 1):
                            if (coor[0] + i, coor[1] + j) in coorVec:
                                coorVec.remove((coor[0] + i, coor[1] + j))
                    szVec = np.size(coorVec)

        print 'Set Up Lambdas'

        # Set up lambda
        if cls.templates == 'random':
            print 'random'
            cls.correct_lambdas = np.asarray(np.random.randn(cls.K, cls.Cin, cls.h, cls.w), dtype=theano.config.floatX)
            # cls.correct_lambdas[1] = 2.5*cls.correct_lambdas[1] + 0.0*cls.correct_lambdas[2] + cls.correct_lambdas[0]
            # cls.correct_lambdas[2] = 0.5*cls.correct_lambdas[2]
            cls.correct_lambdas[0] = 0.0*cls.correct_lambdas[0]
        else:
            print 'gabor'
            cls.correct_lambdas = np.zeros((cls.K, cls.Cin, cls.h, cls.w), dtype=theano.config.floatX)
            path = '/Users/heatherseeba/repos/psychophysics/python/templates/*.png'
            indx_img = 1
            file_list = [sorted(glob.glob(path))[0], sorted(glob.glob(path))[6]]
            for file_dir in file_list:
                img = misc.imread(file_dir)
                print file_dir
                cls.correct_lambdas[indx_img] = np.transpose(misc.imresize(img[:,:,0:3], size=(cls.h, cls.w, cls.Cin)), (2, 0, 1))
                indx_img += 1
                del img
                if indx_img == cls.K:
                    break

            cls.correct_lambdas[2] = 0.1*cls.correct_lambdas[2]
            cls.correct_lambdas[1] = 0.1*cls.correct_lambdas[1]

        # Set up the correct means
        cls.correct_means = np.asarray(np.abs(np.random.randn(cls.K, cls.Cin, cls.h, cls.w)), dtype=theano.config.floatX)
        cls.correct_means[2] = 20.0*cls.correct_means[2] + 5*cls.correct_means[0]
        cls.correct_means[0] = 0.0*cls.correct_means[0]

        # Set up the correct psis
        # correct_psis_val = np.asarray(np.abs(np.random.randn(1, cls.D)), dtype=theano.config.floatX)
        correct_psis_val = cls.psi_scale*np.asarray(np.ones((1, cls.D)), dtype=theano.config.floatX)
        cls.correct_psis = np.tile(correct_psis_val, reps=(cls.K, 1))

        # Initialize amps (pi)
        cls.correct_pi = np.zeros((cls.K,1),dtype=theano.config.floatX)
        cls.correct_pi_test = np.zeros((cls.K,1),dtype=theano.config.floatX)

        # Initialize the data
        cls.d = np.zeros((cls.Ni, cls.Cin, cls.H, cls.W), dtype=theano.config.floatX)
        cls.dtest = np.zeros((cls.Ni, cls.Cin, cls.H, cls.W), dtype=theano.config.floatX)

        cls.correct_rs = np.zeros((cls.Ni, cls.K, Nh, Nw), dtype=theano.config.floatX)
        cls.correct_z = np.zeros((cls.Ni, cls.K, Nh, Nw), dtype=theano.config.floatX)

        cls.correct_rs_test = np.zeros((cls.Ni, cls.K, Nh, Nw), dtype=theano.config.floatX)
        cls.correct_z_test = np.zeros((cls.Ni, cls.K, Nh, Nw), dtype=theano.config.floatX)

        # Synthesize data
        # Note: Haven't added noise to zero patches
        for n in xrange(cls.Ni):
            for hindx in xrange(Nh):
                for windx in xrange(Nw):
                    if cls.matA[n, hindx, windx] > 0:
                        k = np.random.choice(range(cls.K - 1)) + 1
                        cls.correct_rs[n,k,hindx,windx] = 1
                        cls.correct_pi[k] += 1
                        noise_val = np.sqrt(cls.psi_scale)*np.asarray(np.random.randn(cls.D, 1), dtype=theano.config.floatX)
                        # noise_val = np.dot(np.diag(np.sqrt(correct_psis_val).reshape(-1)), noise_val)
                        noise_val = np.reshape(noise_val, newshape=(cls.Cin, cls.h, cls.w))
                        z = np.random.randn()
                        cls.correct_z[n,k,hindx,windx] = z
                        cls.d[n, : , hindx:hindx + cls.h, windx:windx + cls.w] = cls.correct_lambdas[k]*z \
                                                                                 + cls.correct_means[k] + noise_val
                    else:
                        cls.correct_rs[n,0,hindx,windx] = 1

                    if cls.matAtest[n, hindx, windx] > 0:
                        ktest = np.random.choice(range(cls.K - 1)) + 1
                        cls.correct_rs_test[n,ktest,hindx,windx] = 1
                        cls.correct_pi_test[ktest] += 1
                        noise_val_test = np.sqrt(cls.psi_scale)*np.asarray(np.random.randn(cls.D, 1), dtype=theano.config.floatX)
                        # noise_val_test = np.dot(np.diag(np.sqrt(correct_psis_val).reshape(-1)), noise_val_test)
                        noise_val_test = np.reshape(noise_val_test, newshape=(cls.Cin, cls.h, cls.w))
                        z_test = np.random.randn()
                        cls.correct_z_test[n,ktest,hindx,windx] = z_test
                        cls.dtest[n, : , hindx:hindx + cls.h, windx:windx + cls.w] = cls.correct_lambdas[ktest]*z_test \
                                                                                     + cls.correct_means[ktest] + noise_val_test
                    else:
                        cls.correct_rs_test[n,0,hindx,windx] = 1

        cls.correct_rs_temp = copy.copy(cls.correct_rs)
        cls.correct_rs_test_temp = copy.copy(cls.correct_rs_test)
        # Reshape correct_rs
        cls.correct_rs = np.reshape(cls.correct_rs.transpose((1,0,2,3)),newshape=(cls.K,cls.Np*cls.Ni))
        cls.correct_labels = np.argmax(cls.correct_rs,0)

        cls.correct_rs_test = np.reshape(cls.correct_rs_test.transpose((1,0,2,3)),newshape=(cls.K,cls.Np*cls.Ni))
        cls.correct_labels_test = np.argmax(cls.correct_rs_test,0)

        # Reshape correct_z
        cls.correct_z = np.reshape(cls.correct_z.transpose((1,0,2,3)), newshape=(cls.K,cls.M,cls.Np*cls.Ni))
        cls.correct_z_test = np.reshape(cls.correct_z_test.transpose((1,0,2,3)), newshape=(cls.K,cls.M,cls.Np*cls.Ni))

        # Reshape means and lambdas
        cls.correct_means = np.reshape(cls.correct_means, newshape=(cls.K, cls.D))
        cls.correct_lambdas = np.reshape(cls.correct_lambdas, newshape=(cls.K, cls.D, cls.M))

        # Set up lambda covs and total covs
        cls.correct_lambda_covs = np.zeros((cls.K, cls.D, cls.D), dtype=theano.config.floatX)
        cls.correct_covs = np.zeros((cls.K, cls.D, cls.D), dtype=theano.config.floatX)
        for i in xrange(cls.K):
            cls.correct_lambda_covs[i] = np.dot(cls.correct_lambdas[i],(cls.correct_lambdas[i]).T)
            cls.correct_covs[i] = cls.correct_lambda_covs[i] + np.diag(cls.correct_psis[i])

        # compute the correct inverse covariances
        cls.inv_correct_covs = 0. * cls.correct_covs
        for k in xrange(1, cls.K):
            cls.inv_correct_covs[k] = cls._invert_cov(k=k, em_mode=cls.em_mode)

        # compute amps (pi)
        cls.correct_pi[0] = np.size(cls.matA) - np.count_nonzero(cls.matA)
        cls.correct_pi = cls.correct_pi/np.size(cls.matA)
        cls.correct_pi = cls.correct_pi.reshape(-1)

        cls.correct_pi_test[0] = np.size(cls.matAtest) - np.count_nonzero(cls.matAtest)
        cls.correct_pi_test = cls.correct_pi_test/np.size(cls.matAtest)
        cls.correct_pi_test = cls.correct_pi_test.reshape(-1)

        # start EM
        cls.x = T.tensor4('x')

        print 'Shape of data'
        print np.shape(cls.d)

        # cls.mix = ConvMofa(x=cls.x, dat=cls.d, K=cls.K, M=cls.M, W=cls.W, H=cls.H, w=cls.w, h=cls.h, Cin=cls.Cin, Ni=cls.Ni,
        #                    amps_val_init=copy.copy(cls.correct_pi), lambdas_val_init=copy.copy(cls.correct_lambdas), lambda_covs_val_init=None,
        #                    covs_val_init=None, inv_covs_val_init=None, means_val_init=copy.copy(cls.correct_means), psis_val_init=copy.copy(cls.correct_psis),
        #                    PPCA=False, lock_psis=True, update_mode=cls.update_mode, em_mode=cls.em_mode,
        #                    rs_clip = 0.0, max_condition_number=1.e3, init=True,init_ppca=False)

        cls.mix = ConvMofa(x=cls.x, dat=cls.d, K=cls.K, M=cls.M, W=cls.W, H=cls.H, w=cls.w, h=cls.h, Cin=cls.Cin, Ni=cls.Ni,
                           amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                           covs_val_init=None, inv_covs_val_init=None, means_val_init=None, psis_val_init=None,
                           PPCA=False, lock_psis=True, update_mode=cls.update_mode, em_mode=cls.em_mode,
                           rs_clip = 0.0, max_condition_number=1.e3, init=True,init_ppca=False)

        print 'Compute some stuffs'
        cls.data_val = cls.mix.createPatches()
        cls.data_valT = cls.data_val.T
        cls.xmin, cls.xmax = np.min(cls.data_valT[0]), np.max(cls.data_valT[0])
        cls.ymin, cls.ymax = np.min(cls.data_valT[1]), np.max(cls.data_valT[1])

        # compute the correct log-likelihood
        cls.correct_logLs, correct_neg_dist = cls.cal_logLs(covs=cls.correct_covs, means=cls.correct_means, inv_covs=cls.inv_correct_covs, em_mode=cls.em_mode)
        cls.correct_NegLogLs = -np.sum(cls.correct_logLs)

        correct_neg_dist = np.reshape(correct_neg_dist, newshape=(cls.K-1, cls.Ni, cls.H - cls.h + 1, cls.W - cls.w + 1))
        correct_neg_dist = correct_neg_dist.transpose(1,0,2,3)

        cls.correct_rs_debug = np.reshape(cls.correct_rs, newshape=(cls.K, cls.Ni, cls.H - cls.h + 1, cls.W - cls.w + 1))
        cls.correct_rs_debug = cls.correct_rs_debug.transpose(1,0,2,3)

        print 'Start Training'

        # print 'Distances'
        # print -neg_dist
        # print 'Responsibilities'
        # print cls.correct_rs_debug

        # cls.mix = ConvMofa(x=cls.x, dat=cls.d, K=cls.K, M=cls.M, W=cls.W, H=cls.H, w=cls.w, h=cls.h, Cin=cls.Cin, Ni=cls.Ni,
        #                    amps_val_init=copy.copy(cls.correct_pi), lambdas_val_init=copy.copy(cls.correct_lambdas), lambda_covs_val_init=None,
        #                    covs_val_init=None, inv_covs_val_init=None, means_val_init=copy.copy(cls.correct_means), psis_val_init=copy.copy(cls.correct_psis),
        #                    PPCA=False, lock_psis=True, update_mode=cls.update_mode, em_mode=cls.em_mode,
        #                    rs_clip = 0.0, max_condition_number=1.e3, init=True,init_ppca=False)

        cls.mix._E_step()
        output_E = theano.function([], [cls.mix.latents, cls.mix.latent_covs, cls.mix.betas, cls.mix.neg_dist, cls.mix.logLs, cls.mix.rs,
                                        cls.mix.rs_conv, cls.mix.rs_conv_new, cls.mix.sumrs,
                                        cls.mix.amps, cls.mix.data, cls.mix.inv_covs, cls.mix.covs,
                                        cls.mix.lambdas, cls.mix.psis, cls.mix.means, cls.mix.lambda_covs,
                                        cls.mix.amps_new, cls.mix.indx_good_cluster, cls.mix.test_indx],
                                   on_unused_input='ignore',
                                   givens={cls.x:cls.d})
        latents, latent_covs, betas, neg_dist, logLs, rs, rs_conv, rs_conv_new, sumrs, amps_val_old, \
        data_val, inv_covs_val_old, covs_val_old, \
        lambdas_val_old, psis_val_old, means_val_old, lambda_covs_val_old, amps_val_new, indx_good_cluster, test_indx = output_E()

        print 'finish E step'

        print 'indices of good clusters'
        print np.shape(indx_good_cluster)
        print indx_good_cluster
        print test_indx

        print 'dimension of rs'
        print np.shape(rs)
        print np.shape(amps_val_new)
        print amps_val_new

        print 'test lambdas_vec_temp'
        print np.shape(lambdas_vec_temp)
        print '\n'
        print 'tetst proj_vec'
        print np.shape(proj_vec)
        print '\n'

        print 'test neg-dist'
        print np.shape(neg_dist)
        print np.max(neg_dist)
        print np.min(neg_dist)
        print '\n'

        dp_debug1 = np.dot(ortho_vec_val[0].T, lambdas_val_old[1])
        dp_debug2 = np.dot(ortho_vec_val[1].T, lambdas_val_old[2])

        print 'test dp_debug1'
        print np.max(dp_debug1)
        print np.min(dp_debug1)
        print '\n'
        print 'test dp_debug2'
        print np.max(dp_debug2)
        print np.min(dp_debug2)
        print '\n'

        print 'Dimension of lambda'
        print np.shape(lambdas_val_old)
        print np.max(lambdas_val_old)
        print np.min(lambdas_val_old)
        print '\n'
        print 'Dimension of ortho_vec_val'
        print np.shape(ortho_vec_val)
        print np.max(ortho_vec_val)
        print np.min(ortho_vec_val)
        print '\n'

        cls.mix._M_step()
        output_M = theano.function([], [cls.mix.means_new, cls.mix.lambdas_new, cls.mix.psis_new,
                                            cls.mix.covs_new, cls.mix.inv_covs_new,
                                            cls.mix.lambda_covs_new],
                                         on_unused_input='ignore',
                                         givens={cls.x:cls.d})
        means_val_new, lambdas_val_new, psis_val_new, covs_val_new, inv_covs_val_new, lambda_covs_val_new = output_M()

        cls.correct_zrs = cls.correct_z[:,0,:]*cls.correct_rs
        zrs_val = latents[:,0,:]*rs[1:]

        print 'Means Comparison'
        print norm(cls.correct_means - means_val_new)/norm(cls.correct_means)
        print 'Lambdas Comparison'
        print norm(cls.correct_lambdas - lambdas_val_new)/norm(cls.correct_lambdas)
        print 'Psis Comparison'
        print norm(cls.correct_psis - psis_val_new)/norm(cls.correct_psis)
        print 'rs Comparison using norm'
        print norm(cls.correct_rs - rs)/norm(cls.correct_rs)
        print 'rs Comparison using error percentage'
        print float(np.shape((cls.correct_rs - rs).nonzero())[1])/np.size(cls.correct_rs)
        print 'zrs Comparison'
        print norm(cls.correct_z[1:,0,:]*cls.correct_rs[1:] - latents[:,0,:]*rs[1:])/norm(cls.correct_z[1:,0,:]*cls.correct_rs[1:])
        print 'Correct Pi'
        print cls.correct_pi
        print 'Learned Pi'
        print amps_val_new
        print 'Correct Psi'
        # print cls.correct_psis
        print 'Learned Psi'
        # print psis_val_new
        print 'Correct zrs'
        # print cls.correct_zrs[np.nonzero(cls.correct_zrs)]
        print 'Learned zrs'
        # print zrs_val[np.nonzero(zrs_val)]
        print 'correct rs'
        # print cls.correct_rs_temp[:,1:,:,:]
        print 'rs'
        # print rs_conv_new
        print 'Done'

        # start_time = time.time()
        # cls.NegLogLs_vec, cls.dLC_vec, cls.dMU_vec, cls.dPS_vec, cls.epochs = cls.mix.fit(x=cls.x, dat=cls.d, tol=tol, maxiter=maxiter, verbose=verbose, mode='Param')
        # stop_time = time.time()
        # print 'train takes %f seconds' %(stop_time - start_time)
        #
        # # get params
        # cls.getParams()
        #
        # # get predict
        # cls.getPredict()
        #
        # # compute the negative-loglikelihood of the test set
        # neg_dist_test = np.zeros((cls.K-1, cls.Nl))
        # for k in xrange(cls.K - 1):
        #     neg_dist_test[k] = -np.sum((cls.data_val_test - cls.means_val_test[k+1]).T * (cls.data_val_test - cls.means_val_test[k+1]).T, axis=0)
        #
        # L_test = 0.5*np.sum(neg_dist_test * cls.rs_val_test[1:], axis=0)
        # cls.NegLogLs_test = -np.sum(L_test)
        #
        # correct_neg_dist_test = np.zeros((cls.K-1, cls.Nl))
        # for k in xrange(cls.K - 1):
        #     correct_neg_dist_test[k] = -np.sum((cls.data_val_test - cls.correct_means[k+1]).T * (cls.data_val_test - cls.correct_means[k+1]).T, axis=0)
        #
        # correct_L_test = 0.5*np.sum(correct_neg_dist_test * cls.correct_rs_test[1:], axis=0)
        # cls.correct_NegLogLs_test = -np.sum(correct_L_test)
        #
        #
        # # Display some important results
        # print '\nNegative LogLikelihood Test'
        # print cls.NegLogLs_test
        # print '\nCorrect Negative LogLikelihood Test'
        # print cls.correct_NegLogLs_test
        # print '\nNegative LogLikelihood Train'
        # print norm(cls.correct_NegLogLs - cls.NegLogLs_vec[-1])/norm(cls.correct_NegLogLs)
        # print '\nLogLikehood vec Train'
        # # print norm(cls.correct_logLs - cls.logLs_val)/norm(cls.logLs_val)
        # print '\nCov Matrix'
        # print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        # print '\nLambdas Matrix'
        # print norm(cls.correct_lambdas - cls.lambdas_val)/norm(cls.correct_lambdas)
        # print '\nPsi Matrix'
        # print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        # print '\nMeans'
        # print norm(cls.correct_means - cls.means_val)/norm(cls.correct_means)
        # # print '\nPrint correct Psi matrix'
        # # print cls.correct_psis
        # # print '\nPrint learned Psi matrix'
        # # print cls.psis_val
        # # print '\nPrint Correct Lambdas'
        # # print cls.correct_lambdas
        # # print '\nPrint Learned Lambdas'
        # # print cls.lambdas_val
        # # print '\nPrint Correct Means'
        # # print cls.correct_means
        # # print '\nPrint Means'
        # # print cls.means_val
        # print '\nPrint Correct Pi'
        # print cls.correct_pi
        # print '\nPrint Learned Pi'
        # print cls.amps_val
        # print '\nDone'

    @classmethod
    def tearDownClass(cls, PlotFig = True):
        if PlotFig:
            fig1 = pl.figure()
            #color = np.abs(np.random.randn(cls.K,3))
            #color = color/np.tile(np.sum(color, axis=0),reps=(cls.K,1))
            color = np.asarray([[1,0,0],[0,1,0],[0,0,1]])
            print np.shape(cls.data_val)
            for i in xrange(cls.K):
                indx_val = np.nonzero(cls.correct_labels==i)
                print np.shape(indx_val)
                pl.plot(cls.data_valT[0,indx_val[0][0:500]], cls.data_valT[1,indx_val[0][0:500]],'o',
                        color=np.concatenate((color[i,:],[0.4,]), axis=1),label='%d' % i)
            pl.xlim(cls.xmin, cls.xmax)
            pl.ylim(cls.ymin, cls.ymax)
            pl.legend()
            fig1.savefig('original_data_%i.png' % cls.seed)

            fig2=pl.figure()
            pl.plot(cls.correct_means[:,0],cls.correct_means[:,1],'rx',ms=15,label='Initialization')
            plot_2d_ellipses(0,1, cls.correct_means, cls.correct_lambda_covs, cls.K, edgecolor='r')
            indx = np.argmax(cls.rs_val,0)
            for i in xrange(cls.K):
                indx_val = np.nonzero(indx==i)
                pl.plot(cls.data_valT[0,indx_val[0][0:500]], cls.data_valT[1,indx_val[0][0:500]],'o',
                        color=np.concatenate((color[i,:],[0.4,]), axis=1),label='%d' % i)

            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.covs_val, K=2, edgecolor='g')
            # plot_2d_ellipses_psis(0,1, means=cls.means_val, psis=cls.psis_val, K=2, edgecolor='k')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.lambda_covs_val, K=2, edgecolor='b')
            plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.correct_lambda_covs, K=2, edgecolor='c')
            # plot_2d_ellipses_psis(0,1, means=cls.means_val, psis=cls.correct_psis, K=2, edgecolor='c')
            # plot_2d_ellipses(0,1, means=cls.means_val, covs=cls.correct_covs, K=2, edgecolor='c')
            pl.plot(cls.means_val[:,0],cls.means_val[:,1],'gx',ms=15,label='Psi fixed')
            pl.title(r'Data $(D, N) = ({0}, {1})$, Model $(K, M) = ({2}, {3})$'.format(cls.D,cls.N,cls.K,cls.M))
            pl.legend()
            pl.xlim(cls.xmin, cls.xmax)
            pl.ylim(cls.ymin, cls.ymax)
            pl.grid()
            fig2.savefig('results_%i.png' % cls.seed)

        cls.mix = None

    @classmethod
    def getParams(cls):
        # output learned parameters
        output_final = theano.function([], [cls.mix.means, cls.mix.lambdas, cls.mix.psis,
                                            cls.mix.covs, cls.mix.inv_covs, cls.mix.amps,
                                            cls.mix.lambda_covs, cls.mix.rs, cls.mix.logLs,
                                            cls.mix.latents, cls.mix.sumrs, cls.mix.betas],
                                         on_unused_input='ignore',
                                         givens={cls.x:cls.d})

        cls.means_val, cls.lambdas_val, cls.psis_val, cls.covs_val, cls.inv_covs_val, cls.amps_val, \
        cls.lambda_covs_val, cls.rs_val, cls.logLs_val, cls.latents, cls.sumrs, cls.betas = output_final()

        # Converse the results into numpy arrays
        cls.means_val = np.asarray(cls.means_val,dtype=theano.config.floatX)
        cls.lambdas_val = np.asarray(cls.lambdas_val,dtype=theano.config.floatX)
        cls.psis_val = np.asarray(cls.psis_val,dtype=theano.config.floatX)
        cls.covs_val = np.asarray(cls.covs_val,dtype=theano.config.floatX)
        cls.inv_covs_val = np.asarray(cls.inv_covs_val,dtype=theano.config.floatX)
        cls.amps_val = np.asarray(cls.amps_val,dtype=theano.config.floatX)
        cls.lambda_covs_val = np.asarray(cls.lambda_covs_val,dtype=theano.config.floatX)
        cls.rs_val = np.asarray(cls.rs_val,dtype=theano.config.floatX)
        cls.logLs_val = np.asarray(cls.logLs_val,dtype=theano.config.floatX)
        cls.sumrs = np.asarray(cls.sumrs,dtype=theano.config.floatX)
        cls.betas = np.asarray(cls.betas,dtype=theano.config.floatX)
        cls.latents = np.asarray(cls.latents,dtype=theano.config.floatX)

        # Arrange clusters in a right order
        for i in xrange(1, cls.K, 1):
            dist_vec = []
            for j in xrange(i, cls.K, 1):
                dist_vec.append(norm(cls.correct_means[i] - cls.means_val[j]))

            right_order = np.argmin(dist_vec) + i

            cls.rs_val[[right_order, i],:] = cls.rs_val[[i, right_order],:]
            cls.means_val[[right_order, i],:] = cls.means_val[[i, right_order],:]
            cls.lambdas_val[[right_order, i],:,:] = cls.lambdas_val[[i, right_order],:,:]
            cls.psis_val[[right_order, i],:] = cls.psis_val[[i, right_order],:]
            cls.covs_val[[right_order, i],:,:] = cls.covs_val[[i, right_order],:,:]
            cls.inv_covs_val[[right_order, i],:,:] = cls.inv_covs_val[[i, right_order],:,:]
            cls.amps_val[[right_order, i]] = cls.amps_val[[i, right_order]]
            cls.lambda_covs_val[[right_order, i],:] = cls.lambda_covs_val[[i, right_order],:]
            cls.logLs_val[[right_order, i]] = cls.logLs_val[[i, right_order]]
            cls.sumrs[[right_order, i]] = cls.sumrs[[i, right_order]]
            # cls.betas[[right_order, i],:,:] = cls.betas[[i, right_order],:,:]
            # cls.latents[[right_order, i],:,:] = cls.latents[[i, right_order],:,:]

        return None

    @classmethod
    def getPredict(cls):
        """
        rearrange means_val_test and rs_val_test
        """
        cls.labels_test, cls.means_val_test, cls.rs_val_test, cls.data_val_test = cls.mix.predict_error(x=cls.x, dat=cls.dtest)
        for i in xrange(1, cls.K, 1):
            dist_vec = []
            for j in xrange(i, cls.K, 1):
                dist_vec.append(norm(cls.correct_means[i] - cls.means_val_test[j]))

            right_order = np.argmin(dist_vec) + i

            cls.rs_val_test[[right_order, i],:] = cls.rs_val_test[[i, right_order],:]
            cls.means_val_test[[right_order, i],:] = cls.means_val_test[[i, right_order],:]

    def test_convergence(cls):
        """
        Plot loglikelihood vs epochs
        """
        fig1 = figure()
        plot(cls.epochs, cls.NegLogLs_vec, 'bo-', label='NegLogLs')
        cls.correct_NegLogLs_vec = cls.correct_NegLogLs * np.ones_like(cls.epochs)
        # plot(cls.epochs, cls.correct_NegLogLs_vec, 'r-', label='Correct_NegLogLs')
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_from_my_MFA')
        legend()
        fig1.savefig('Convergence_of_Negative_LogLikelihood_from_my_MFA.png')
        close(fig1)

        fig2 = figure()
        plot(cls.epochs, cls.dLC_vec, 'bo-', label='Lambda_Covs')
        plot(cls.epochs, cls.dMU_vec, 'go-', label='Mean')
        legend()
        xlabel('Epoch')
        ylabel('Relative Difference')
        title('Convergence_of_Parameters_from_my_MFA')
        fig2.savefig('Convergence_of_Parameters_from_my_MFA.png')
        close(fig2)

    def test_loglikelihood(cls):
        """
        Compare the log-likelihood of learned model with the correct one
        """
        cls.assertLessEqual(norm(cls.correct_NegLogLs - cls.NegLogLs_vec[-1])/norm(cls.correct_NegLogLs),1e-2,
                            'incorrect log-likelihood vector')

    def test_pi(cls):
        """
        Compare learned pi with the correct one
        """
        cls.assertLessEqual(norm(cls.correct_pi - cls.amps_val, ord=1)/norm(cls.correct_pi, ord=1),1e-2,
                            'incorrect pi vector')

    def test_classification_train_err(cls):
        """
        Compute the classification accuracy on the train set and plot the confusion matrix
        """
        labels = np.argmax(cls.rs_val,0)
        accu_score = precision_score(cls.correct_labels, labels, average='micro')
        print 'Accuracy on Train Set is %f' %accu_score

        cfn = confusion_matrix(cls.correct_labels, labels).astype(np.float) # 1st index = true label, 2nd = predicted label

        print cls.correct_labels[0:50]
        print labels[0:50]
        print np.shape(cls.correct_labels)
        print np.shape(labels)

        for i in xrange(cfn.shape[0]):
            cfn[i,:] /= cfn[i,:].sum() # normalize

        imshow(cfn)
        colorbar()
        ylabel('True Label')
        xlabel('Predicted Label')
        title('Confusion Matrix for Train Set - Accu %f' %accu_score)
        savefig('confusion_mtx_accu_train.png')

    def test_classification_test_err(cls):
        """
        Compute the classification accuracy on the test set and plot the confusion matrix
        """
        labels = np.argmax(cls.rs_val_test,0)
        accu_score = precision_score(cls.correct_labels_test, labels, average='micro')
        print 'Accuracy on Test Set is %f' %accu_score

        cfn = confusion_matrix(cls.correct_labels_test, labels).astype(np.float) # 1st index = true label, 2nd = predicted label
        print 'Confusion Matrix'
        print np.shape(cfn)
        print cfn

        print cls.correct_labels_test[0:50]
        print labels[0:50]
        print np.shape(cls.correct_labels_test)
        print np.shape(labels)

        for i in xrange(cfn.shape[0]):
            cfn[i,:] /= cfn[i,:].sum() # normalize

        print 'Confusion Matrix'
        print np.shape(cfn)
        print cfn

        imshow(cfn)
        colorbar()
        xlabel('True Label')
        ylabel('Predicted Label')
        title('Confusion Matrix for Test Set - Accu %f' %accu_score)
        savefig('confusion_mtx_accu_test.png')
        return None

    #######################################################################
    # set of methods used to compute the log-likehood of the correct model
    #
    # copy the comments from the mofa.py by Ross Fadely, so you will find
    # some funny comments
    #######################################################################
    @classmethod
    def _invert_cov(cls, k, em_mode):
        """
        Calculate inverse covariance of mofa or ppca model,
        using inversion lemma
        """
        if em_mode == 'soft':
            psiI = inv(np.diag(cls.correct_psis[k]))
            lam  = cls.correct_lambdas[k]
            lamT = lam.T
            step = inv(np.eye(cls.M) + np.dot(lamT,np.dot(psiI,lam)))
            step = np.dot(step,np.dot(lamT,psiI))
            step = np.dot(psiI,np.dot(lam,step))
            return psiI - step
        else:
            lam = cls.correct_lambdas[k]
            lamT = lam.T
            return np.linalg.pinv(np.dot(lam,lamT))

    @classmethod
    def _log_multi_gauss(cls, k, covs, means, inv_covs, D, em_mode):
        """
        Soft Gaussian log likelihood of the data for component k.
        """
        X1 = (D - means[k]).T
        X2 = np.dot(inv_covs[k], X1)
        p = -0.5 * np.sum(X1 * X2, axis=0)
        if em_mode == 'soft':
            sgn, logdet = np.linalg.slogdet(covs[k])
            return -0.5 * np.log(2 * np.pi) * cls.D - 0.5 * logdet + p
        else:
            return -0.5 * np.log(2 * np.pi) * cls.D + p

    @classmethod
    def _log_sum(cls,loglikes):
        """
        Calculate sum of log likelihoods
        """
        loglikes = np.atleast_2d(loglikes)
        a = np.max(loglikes, axis=0)
        return a + np.log(np.sum(np.exp(loglikes - a[None, :]), axis=0))

    @classmethod
    def cal_logLs(cls, covs, means, inv_covs, em_mode):
        """
        Calculate log likelihoods for each datum
        under each component.
        """
        if cls.em_mode == 'soft':
            logrs = np.zeros((cls.K-1, cls.Nl))
            for k in range(cls.K-1):
                logrs[k] = np.log(cls.correct_pi[k+1]) + cls._log_multi_gauss(k=k+1, covs=covs, means=means, inv_covs=inv_covs, D=cls.data_val, em_mode=em_mode)

            # here lies some ghetto log-sum-exp...
            # nothing like a little bit of overflow to make your day better!
            L = cls._log_sum(logrs)
            return L
        else:
            neg_dist = np.zeros((cls.K-1, cls.Nl))
            for k in xrange(cls.K - 1):
                neg_dist[k] = -np.sum((cls.data_val - cls.correct_means[k+1]).T * (cls.data_val - cls.correct_means[k+1]).T, axis=0)

            L = 0.5*np.sum(neg_dist * cls.correct_rs[1:], axis=0)
            return L, neg_dist

if __name__ == '__main__':
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromTestCase(ConvSimpleTestCase)
    runner = unittest.TextTestRunner()
    results = runner.run(suite)

# cls.mix._E_step()
# test_E_function = theano.function([], [cls.mix.logLs, cls.mix.rs, cls.mix.betas, cls.mix.latents, cls.mix.latent_covs],
#                                  updates=[], on_unused_input='ignore',
#                                  givens={cls.x:cls.d})
#
# cls.test_logLs, cls.test_rs, cls.test_betas, cls.test_latents, cls.test_latent_covs = test_E_function()
# cls.mix._M_step()
# updates = []
# updates.append((cls.mix.means, cls.mix.means_new))
# updates.append((cls.mix.lambdas, cls.mix.lambdas_new))
# updates.append((cls.mix.psis, cls.mix.psis_new))
# updates.append((cls.mix.covs, cls.mix.covs_new))
# updates.append((cls.mix.inv_covs, cls.mix.inv_covs_new))
# updates.append((cls.mix.amps, cls.mix.amps_new))
# updates.append((cls.mix.lambda_covs, cls.mix.lambda_covs_new))
# test_M_function = theano.function([], [cls.mix.logLs, cls.mix.rs, cls.mix.betas, cls.mix.latents, cls.mix.latent_covs, cls.mix.psis_new, cls.mix.sumrs],
#                                  updates=updates, on_unused_input='ignore',
#                                  givens={cls.x:cls.d})
# test_logLs, test_rs, test_betas, test_latents, test_latent_covs, test_psis_new, test_sumrs = test_M_function()

# cls.d.astype(np.uint8)
# print 'hehe'
# for i in xrange(20):
#     misc.imsave('TestImage_%i.jpg' % i, cls.d[i])