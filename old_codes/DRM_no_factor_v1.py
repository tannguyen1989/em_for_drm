__author__ = 'tannguyen'

import matplotlib as mpl

mpl.use('Agg')
from pylab import *

from old_codes.nn_functions_stable import LogisticRegression, LogisticRegressionForSemisupervised
from theano.tensor.signal import pool
from theano.tensor.shared_randomstreams import RandomStreams

import theano.tensor as T

import theano


# from guppy import hpy; h=hpy()

class DRM_model(object):
    '''
    Class of DRM models
    '''

    def __init__(self, seed, train_mode='supervised', grad_min=-np.inf, grad_max=np.inf):
        self.layers = []
        self.Cin_Softmax = 0
        self.H_Softmax = 0
        self.W_Softmax = 0
        self.train_mode = train_mode
        self.n_out = 0
        self.W_softmax_init = None
        self.b_softmax_init = None
        self.N_layer = 0
        self.denoising = None
        self.reconst_weights = []
        self.grad_min = grad_min
        self.grad_max = grad_max
        self.N_params_per_layer = 0
        self.seed = seed

        np.random.seed(self.seed)

        self.srng = RandomStreams(seed=np.random.randint(2 ** 30))

        self.x = T.tensor4('x')
        self.y = T.ivector('y')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

    def Build_TopDown(self):
        # Top-Down

        if self.train_mode == 'supervised':
            print('Build a supervised model')

            n_softmax = self.Cin_Softmax * self.H_Softmax * self.W_Softmax

            softmax_input = self.layers[0].output.flatten(2)

            softmax_input_clean = self.layers[0].output_clean.flatten(2)

            # classify the values of the fully-connected sigmoidal layer
            self.softmax_layer = LogisticRegression(input=softmax_input, input_clean=softmax_input_clean,
                                                    n_in=n_softmax, n_out=self.n_out,
                                                    W_init=self.W_softmax_init, b_init=self.b_softmax_init)

            # the cost we minimize during training is the NLL of the model
            self.cost = self.softmax_layer.negative_log_likelihood(self.y)

        elif self.train_mode == 'unsupervised':
            print('Build an unsupervised model')
            # Top-Down
            self.layers[0]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[0].output, ta_hat=self.layers[0].masked_mat,
                                                           layer_loc='intermediate', denoising=self.denoising)
            for i in xrange(1, self.N_layer):
                self.layers[i]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[i - 1].data_reconstructed, ta_hat=self.layers[i].masked_mat,
                                                               layer_loc='intermediate', denoising=self.denoising)

            # Cost
            self.cost = 0.0
            for i in xrange(self.N_layer):
                self.cost += self.reconst_weights[self.N_layer - 1 - i] * T.mean(
                    (self.layers[i].data_4D_clean - self.layers[i].data_reconstructed) ** 2)

        elif self.train_mode == 'semisupervised':
            print('Build a semisupervised model')
            # Top-Down
            self.layers[0]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[0].output, ta_hat=self.layers[0].masked_mat,
                                                           layer_loc='intermediate', denoising=self.denoising)
            for i in xrange(1, self.N_layer):
                self.layers[i]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[i - 1].data_reconstructed, ta_hat=self.layers[i].masked_mat,
                                                               layer_loc='intermediate', denoising=self.denoising)

            n_softmax = self.Cin_Softmax * self.H_Softmax * self.W_Softmax

            softmax_input = self.layers[0].output.flatten(2)

            softmax_input_clean = self.layers[0].output_clean.flatten(2)

            # classify the values of the fully-connected sigmoidal layer
            self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input,
                                                                     input_clean=softmax_input_clean, n_in=n_softmax,
                                                                     n_out=self.n_out,
                                                                     W_init=self.W_softmax_init,
                                                                     b_init=self.b_softmax_init)

            # the cost we minimize during training is the NLL of the model
            self.unsupervisedNLL = 0.0
            for i in xrange(self.N_layer):
                self.unsupervisedNLL += self.reconst_weights[self.N_layer - 1 - i] * T.mean(
                    (self.layers[i].data_4D_clean - self.layers[i].data_reconstructed) ** 2)

            self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)

            self.cost = self.supervisedNLL + self.unsupervisedNLL

        else:
            print('Please specify how to do TopDown and update your model in train_model_no_factor_stable.py')

    def ReconstructInput(self, input):
        # Need to test this function before running training using this model
        getReconstruction = theano.function([self.x, self.is_train, self.momentum_bn], self.conv1.data_reconstructed, on_unused_input='warn')
        I_hat = getReconstruction(input, 0, 1.0)
        I_hat = I_hat.transpose((0, 2, 3, 1))
        return I_hat

    def SampleImage(self, Nimages=1):
        # Need to test this function before running training using this model
        class_index = self.srng.random_integers(size=(Nimages,), low=0, high=self.n_out, ndim=None,
                        dtype='int32')
        class_one_hot_encoding = T.extra_ops.to_one_hot(class_index, 10)
        self.layers[0].mu_cg_gen= T.dot(class_one_hot_encoding, (self.softmax_layer.W).T)
        self.layers[0].mu_cg_gen = self.layers[0].mu_cg_gen.dimshuffle(0, 1, 'x', 'x')


        self.layers[0].uniform_mask_real_val_gen = self.srng.uniform(size=(self.layers[0].Ni, self.layers[0].K, self.layers[0].H - self.layers[0].h + 1, self.layers[0].W - self.layers[0].w + 1),
                                                       low=0.0, high=1.0, ndim=None, dtype=theano.config.floatX)

        self.layers[0].uniform_mask_gen = T.grad(T.sum(T.max(pool.pool_2d(input=self.layers[0].uniform_mask_real_val_gen, ds=(2, 2), ignore_border=True, mode='max'), axis=1)), wrt=self.layers[0].uniform_mask_real_val_gen)

        self.layers[0]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[0].mu_cg_gen, ta_hat=self.layers[0].uniform_mask_gen,
                                                       layer_loc='intermediate', denoising=self.denoising)
        for i in xrange(1, self.N_layer):
            self.layers[i].uniform_mask_real_val_gen = self.srng.uniform(size=(self.layers[i].Ni, self.layers[i].K, self.layers[i].H - self.layers[i].h + 1, self.layers[i].W - self.layers[i].w + 1), low=0.0, high=1.0, ndim=None, dtype=theano.config.floatX)
            self.layers[i].uniform_mask_gen = T.grad(T.sum(T.max(pool.pool_2d(input=self.layers[i].uniform_mask_real_val_gen, ds=(2, 2), ignore_border=True, mode='max'), axis=1)), wrt=self.layers[i].uniform_mask_real_val_gen)
            self.layers[i]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[i - 1].data_reconstructed,
                                                           ta_hat=self.layers[i].uniform_mask_gen,
                                                           layer_loc='intermediate', denoising=self.denoising)

        getSamples = theano.function([], self.conv1.data_reconstructed, on_unused_input='warn')
        I_hat = getSamples()
        I_hat = I_hat.transpose((0, 2, 3, 1))
        return I_hat

    def Build_Update_Rule(self):

        # Specify update rules and outputs
        print('Build update rules')
        # create a list of all model parameters to be fit by gradient descent

        self.params = []
        for i in xrange(self.N_layer):
            self.params = self.params + self.layers[i].params

        if self.train_mode != 'unsupervised':
            self.params = self.params + self.softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.updates = []
        for param_i, grad_i in zip(self.params, grads):
            self.updates.append((param_i, param_i - self.lr * T.clip(grad_i, self.grad_min, self.grad_max)))

        indx_val = 0
        for i in xrange(self.N_layer):
            self.layers[i].lambdas_new = self.layers[i].lambdas - self.lr * T.clip(grads[indx_val],
                                                                                   self.grad_min, self.grad_max)
            indx_val += len(self.layers[i].params)

        if self.train_mode != 'unsupervised':
            self.softmax_layer.W_new = self.softmax_layer.params[0] - self.lr * T.clip(
                grads[indx_val], self.grad_min,
                self.grad_max)
            self.softmax_layer.b_new = self.softmax_layer.params[1] - self.lr * T.clip(
                grads[indx_val + 1], self.grad_min,
                self.grad_max)

        self.output_var = []
        self.misc = []
        self.init_var = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.amps, layer.amps_new))
            # self.updates.append((layer.bn_BU.mean, layer.bn_BU.mean_new))
            # self.updates.append((layer.bn_BU.var, layer.bn_BU.var_new))
            # Concatenate output_var
            self.output_var.append(layer.logLs)  # 0
            self.output_var.append(layer.betas)  # 1
            self.output_var.append(layer.lambdas_new)  # 2
            self.output_var.append(layer.amps_new)  # 3
            # Concatenate misc
            self.misc.append(layer.latents)  # 0
            self.misc.append(layer.rs)  # 1
            self.misc.append(layer.latents_masked)  # 2
            # Concatenate init_var
            self.init_var.append(layer.betas)  # 0
            self.init_var.append(layer.lambdas)  # 1
            self.init_var.append(layer.amps)  # 2

        if self.train_mode != 'unsupervised':
            self.output_var.append(self.softmax_layer.W_new)
            self.output_var.append(self.softmax_layer.b_new)