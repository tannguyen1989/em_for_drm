__author__ = 'minhtannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import numpy as np

import timeit

from scipy.linalg import inv, norm

import theano
import theano.tensor as T

from theano.tensor.nlinalg import MatrixPinv, Det, diag, MatrixInverse

from sklearn.metrics import confusion_matrix, precision_score

from itertools import permutations

import copy

from kmeans_init import kmeans_init
from plot_mofa import *

from mfa import *

class SwMofa(Mofa):
    """
    EM Algorithm for Mixture of Factor Analyzers with switching variable.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes

    """
    def __init__(self,data, K, M, D, N,
                 means_val_init, psis_val_init, amps_val_init=None,
                 lambdas_val_init=None, lambda_covs_val_init=None,
                 covs_val_init=None, inv_covs_val_init=None,
                 PPCA=False,lock_psis=True,
                 update_mode='Ross_Fadely',
                 rs_clip = 0.0,
                 max_condition_number=1.e3,
                 init=True,init_ppca=False):

        # set mean of the closest cluster to the origin to 0
        mean_norms = np.linalg.norm(means_val_init, axis = 1)
        indx_min = np.argmin(mean_norms)
        means_val_init[[0, indx_min],:] = means_val_init[[indx_min,0],:]
        means_val_init[0] = 0.0

        # required
        self.psis_val_init = np.asarray(psis_val_init, dtype=theano.config.floatX)
        self.K     = K
        self.M     = M
        self.data  = data
        self.dataT = self.data.T # INSANE DATA DUPLICATION
        self.N     = N
        self.D     = D
        self.update_mode  = update_mode

        # options
        self.PPCA                 = PPCA
        self.lock_psis            = lock_psis
        self.rs_clip              = rs_clip
        self.max_condition_number = max_condition_number
        assert rs_clip >= 0.0

        # empty arrays to be filled
        self.betas       = theano.shared(value=np.zeros((self.K,self.M,self.D), dtype=theano.config.floatX),
                                         name='betas', borrow=True)
        self.latents     = theano.shared(value=np.zeros((self.K,self.M,self.N), dtype=theano.config.floatX),
                                         name='latents', borrow=True)
        self.latent_covs = theano.shared(value=np.zeros((self.K,self.M,self.M,self.N), dtype=theano.config.floatX),
                                         name='latent_covs', borrow=True)
        self.means       = theano.shared(value=np.asarray(means_val_init, dtype=theano.config.floatX), name='means', borrow=True)
        self.rs          = theano.shared(value=np.zeros((self.K,self.N), dtype=theano.config.floatX),
                                         name='rs', borrow=True)

        # initialize
        # if init = True, use _initialize to initialize the parameters.
        # otherwise, use ..._val_init from the input list
        if init:
            self._initialize(init_ppca)
        else:
            self.amps = theano.shared(value=np.asarray(amps_val_init, dtype=theano.config.floatX), name='amps', borrow=True)
            self.lambdas = theano.shared(value=np.asarray(lambdas_val_init, dtype=theano.config.floatX), name='lambdas', borrow=True)
            self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val_init, dtype=theano.config.floatX), name='lambdas_covs', borrow=True)
            self.covs = theano.shared(value=np.asarray(covs_val_init, dtype=theano.config.floatX), name='covs', borrow=True)
            self.inv_covs = theano.shared(value=np.asarray(inv_covs_val_init, dtype=theano.config.floatX), name='inv_covs', borrow=True)

    def _initialize(self,init_ppca,maxiter=200, tol=1e-4):
        """
        Run K-means
        """

        # Randomly assign factor loadings
        lambdas_value = np.random.randn(self.K,self.D,self.M) / \
            np.sqrt(self.max_condition_number)
        lambdas_value[0] = 0.0
        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas', borrow=True)

        # Set (high rank) variance to variance of all data, along a dimension
        self.psis= theano.shared(value=np.asarray(self.psis_val_init, dtype=theano.config.floatX), name='psis', borrow=True)

        # Set initial covs
        covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        lambda_covs_val = np.zeros((self.K,self.D,self.D), dtype=theano.config.floatX)
        inv_covs_val = 0.0 * covs_val

        # compute the inv_covs using matrix inversion lemma
        for k in range(self.K):
            covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T) + \
                np.diag(self.psis_val_init[k])
            lambda_covs_val[k] = np.dot(lambdas_value[k],lambdas_value[k].T)
            psiI = inv(np.diag(self.psis_val_init[k]))
            lam  = lambdas_value[k]
            lamT = lam.T
            step = inv(np.eye(self.M) + np.dot(lamT,np.dot(psiI,lam)))
            step = np.dot(step,np.dot(lamT,psiI))
            step = np.dot(psiI,np.dot(lam,step))
            inv_covs_val[k] = psiI - step

        self.covs = theano.shared(value=np.asarray(covs_val, dtype=theano.config.floatX),
                                  name='covs', borrow=True)

        self.lambda_covs = theano.shared(value=np.asarray(lambda_covs_val, dtype=theano.config.floatX),
                                  name='lambda_covs', borrow=True)

        self.inv_covs = theano.shared(value=np.asarray(inv_covs_val, dtype=theano.config.floatX),
                                  name='inv_covs', borrow=True)

        # Randomly assign the amplitudes.
        amps_val = np.random.rand(self.K)
        amps_val /= np.sum(amps_val)
        self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                  name='amps', borrow=True)

    def _M_step(self):
        """
        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """
        if self.update_mode == 'Ross_Fadely':
            # update means - implicitly included in eq. 15
            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[self.lambdas, self.latents])

            means_new, updates10 = theano.scan(fn=lambda resp, llt, s: T.sum(resp * (self.dataT - llt), axis=1) / s,
                                     outputs_info=None,
                                     sequences=[self.rs[1:], lambdalatents[1:], self.sumrs[1:]])

            self.means_new = T.set_subtensor(self.means[1:], means_new)

            # update lambdas using eq.15
            zeroed, updates11 = theano.scan(fn=lambda m: self.dataT - m[:,None],
                                 outputs_info=None,
                                 sequences=self.means_new)

            lambdas_new, updates12 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixInverse()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[self.latents[1:], self.rs[1:], self.latent_covs[1:], zeroed[1:]])

            self.lambdas_new = T.set_subtensor(self.lambdas[1:], lambdas_new)

            # update psis using eq. 16
            psis, updates13 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                               outputs_info=None,
                               sequences=[self.rs, zeroed, lambdalatents, self.sumrs])
        else:
            latents_tilde = T.concatenate([self.latents,
                                           theano.shared(value=np.ones((self.K, 1 ,self.N),
                                                                       dtype=theano.config.floatX),borrow=True)],
                                          axis=1)
            col1 = T.concatenate([self.latent_covs, (self.latents[:,:,None,:]).dimshuffle((0,2,1,3))], axis=1)
            latent_covs_tilde = T.concatenate([col1, latents_tilde[:,:,None,:]], axis=2)
            dataT = T.tile(self.dataT[None,:,:], reps=[self.K,1,1])
            lambdas_tilde, updates22 = theano.scan(fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:,None,:] * lt[None,:,:], resp), MatrixPinv()(T.dot(lt_covs, resp))),
                                       outputs_info=None,
                                       sequences=[latents_tilde, self.rs, latent_covs_tilde, dataT])
            lambdas_new = lambdas_tilde[1:,:,0:self.M]
            self.lambdas_new = T.set_subtensor(self.lambdas[1:], lambdas_new)
            means_new = lambdas_tilde[1:,:,self.M]
            self.means_new = T.set_subtensor(self.means[1:], means_new)

            lambdalatents, updates9 = theano.scan(fn=lambda l, lt: T.dot(l, lt),
                                        outputs_info=None,
                                        sequences=[lambdas_tilde, latents_tilde])
            psis, update23 = theano.scan(fn=lambda resp, z, llt, s: T.dot((z - llt) * z, resp) / s,
                                         outputs_info=None,
                                         sequences=[self.rs, dataT, lambdalatents, self.sumrs])

        maxpsi, updates14 = theano.scan(fn=lambda p: T.max(p),outputs_info=None, sequences=psis)

        maxlam, updates15 = theano.scan(fn=lambda l: T.max(T.sum(l * l, axis=0)),
                             outputs_info=None,
                             sequences=self.lambdas_new)
        minpsi, updates16 = theano.scan(fn=lambda maxp, maxl: T.max([maxp, maxl]) / self.max_condition_number,
                             outputs_info=None,
                             sequences=[maxpsi, maxlam])
        self.psis_new, updates17   = theano.scan(fn=lambda p, minp: T.clip(p, minp, np.Inf),
                                  outputs_info=None,
                                  sequences=[psis, minpsi])
        # make all elements in each rows of psis the same
        if self.PPCA:
            self.psis_new, updates19 = theano.scan(fn=lambda psn: T.mean(psn)*T.ones(self.D),
                                                   outputs_info=None,
                                                   sequences=self.psis_new)

        # make psi for each cluster the same
        if self.lock_psis:
            self.psis_new = T.dot(self.sumrs,self.psis_new)/T.sum(self.sumrs)
            self.psis_new = (self.psis_new).dimshuffle('x',0)
            self.psis_new = T.tile(self.psis_new,(self.K, 1))

        # update covs
        self._update_covs()



##############################################################################################################
# Test Code #
##############################################################################################################
import unittest

class SimpleSwMFATestCase(BaseTestCase):
    """
    Unittests for SwMOFA with 2 clusters.

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls):
        """
        Run before tests are executed.

        Synthesize data and run EM on the synthetic data
        """

        # skip BaseTestCase
        if cls is BaseTestCase:
            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")
        super(BaseTestCase, cls).setUpClass()

        # set internal parameters
        maxiter = 800
        cls.PPCA = False
        cls.lock_psis = True
        tol = 1e-10
        verbose = True
        cls.N = 50000
        cls.K = 2
        cls.D = 3
        cls.M = 2

        # set random seed
        cls.seed = 2
        np.random.seed(cls.seed)

        # make data
        correct_means = np.concatenate((np.zeros((1,cls.D),dtype=theano.config.floatX),
                                        13.0*np.ones((1,cls.D),dtype=theano.config.floatX)))
        correct_lambdas = np.asarray([np.zeros((cls.D, cls.M)), np.random.randn(cls.D, cls.M)],
                                         dtype=theano.config.floatX)

        cls.makeData(correct_means=correct_means, correct_lambdas=correct_lambdas)

        #################################################################
        # start building and training the model
        #################################################################

        # x is the symbolic variable for data
        cls.x = T.matrix('x')

        # make a MOFA instance
        cls.mix = SwMofa(cls.x.T, K=cls.K, M=cls.M, D=cls.D, N=cls.N, means_val_init=cls.means_val_init, psis_val_init=cls.psis_val_init, PPCA=cls.PPCA, lock_psis=cls.lock_psis, init=True, update_mode='Hinton')

        # output the initializations
        output_init = theano.function([cls.x] , [cls.mix.means, cls.mix.covs, cls.mix.psis], on_unused_input='ignore')
        cls.means_val_init, cls.covs_val_init, cls.psis_val_init = output_init(cls.d)

        cls.LogLs_vec, cls.dLC_vec, cls.dPS_vec, cls.epochs = cls.mix.fit(x=cls.x, dat=cls.d, tol=tol, maxiter=maxiter, verbose=verbose, mode='Param')

        # get params
        cls.getParams()

        # get prediction
        cls.getPredict()

        # Display some important results
        # Optional: solely used for testing purposes
        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.logLs_val)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.logLs_val.sum())/(-cls.logLs.sum())
        print '\nCov Matrix'
        print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        print '\nLambdas Cov Matrix'
        print norm(cls.correct_lambda_covs - cls.lambda_covs_val)/norm(cls.correct_lambda_covs)
        print '\nPsi Matrix'
        print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        print '\nPi Vector'
        print norm(cls.pi - cls.amps_val, ord=1)/norm(cls.pi, ord=1)

class BatchSimpleSwMFATestCase(BatchBaseTestCase):
    """
    Unittests for SwMOFA with 2 clusters.
    Train the model using batch algorithm

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls):
        """
        Run before tests are executed.

        Synthesize data and run EM on the synthetic data
        """

        if cls is BaseTestCase:
            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")
        super(BaseTestCase, cls).setUpClass()

        # set internal parameters
        cls.PPCA = False
        cls.lock_psis = True
        tol = 1e-10
        verbose = True
        cls.N = 50000
        cls.K = 2
        cls.D = 3
        cls.M = 2
        cls.batch_size = 50000
        max_epochs=400

        # set random seed
        cls.seed = 2
        np.random.seed(cls.seed)

        # make data
        correct_means = np.concatenate((np.zeros((1,cls.D),dtype=theano.config.floatX),
                                        13.0*np.ones((1,cls.D),dtype=theano.config.floatX)))
        correct_lambdas = np.asarray([np.zeros((cls.D, cls.M)), np.random.randn(cls.D, cls.M)],
                                         dtype=theano.config.floatX)

        cls.makeData(correct_means=correct_means, correct_lambdas=correct_lambdas)

        #################################################################
        # start building and training the model
        #################################################################

        # x is the symbolic variable for data
        cls.x = T.matrix('x')

        cls.mix = SwMofa((cls.x).T, cls.K, cls.M, cls.D, cls.batch_size, cls.means_val_init, cls.psis_val_init, cls.PPCA, lock_psis=cls.lock_psis)
        output_init = theano.function([cls.x] , [cls.mix.means, cls.mix.covs, cls.mix.psis], on_unused_input='ignore')
        cls.means_val_init, cls.covs_val_init, cls.psis_val_init = output_init(cls.d)
        cls.means_val_init = np.asarray(cls.means_val_init, dtype=theano.config.floatX)
        cls.covs_val_init = np.asarray(cls.covs_val_init, dtype=theano.config.floatX)
        cls.psis_val_init = np.asarray(cls.psis_val_init, dtype=theano.config.floatX)

        logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, means_val, covs_val,\
        inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, cls.LogLs_vec, cls.epochs, \
        cls.dLC_vec, cls.dPS_vec = cls.mix.batch_fit(x=cls.x, dat=cls.d, K=cls.K, batch_size=cls.batch_size,
                                                     tol=tol, max_epochs=max_epochs, verbose=verbose, mode='Param')

        cls.means_val, cls.lambdas_val, cls.psis_val, cls.covs_val, cls.inv_covs_val, cls.amps_val, cls.lambda_covs_val \
            = means_val, lambdas_val, psis_val, covs_val, inv_covs_val, amps_val, lambda_covs_val

        cls.getParams()

        cls.getPredict()

        # Display some important results
        # Optional: solely used for testing purposes
        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.logLs_val)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.logLs_val.sum())/(-cls.logLs.sum())
        print '\nCov Matrix'
        print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        print '\nLambdas Cov Matrix'
        print norm(cls.correct_lambda_covs - cls.lambda_covs_val)/norm(cls.correct_lambda_covs)
        print '\nPsi Matrix'
        print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        print '\nPi Vector'
        print norm(cls.pi - cls.amps_val, ord=1)/norm(cls.pi, ord=1)

class SwMFATestCase(MFATestCase):
    """
    Unittests for SwMOFA with an arbitrary number of clusters.

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls, mode='balanced'):
        if cls is BaseTestCase:
            raise unittest.SkipTest("Skip BaseTest tests, it's a base class")
        super(BaseTestCase, cls).setUpClass()

        # set internal parameters
        maxiter = 800
        cls.PPCA = False
        cls.lock_psis = True
        tol = 1e-10
        verbose = True
        cls.N = 5000
        cls.K = 5
        cls.D = 5
        cls.M = 2
        cls.mode = mode

        # set random seed
        cls.seed =10
        np.random.seed(cls.seed)

        # make data
        correct_first_mean = np.zeros((1,cls.D), dtype=theano.config.floatX)
        correct_first_lambda = np.zeros((1, cls.D, cls.M), dtype=theano.config.floatX)
        cls.makeData(correct_first_mean=correct_first_mean, correct_first_lambda=correct_first_lambda)

        #################################################################
        # start building and training the model
        #################################################################

        cls.x = T.matrix('x')

        cls.mix = SwMofa(cls.x.T, cls.K, cls.M, cls.D, cls.N, cls.means_val_init, cls.psis_val_init, cls.PPCA, lock_psis=cls.lock_psis)
        output_init = theano.function([cls.x] , [cls.mix.means, cls.mix.covs, cls.mix.psis], on_unused_input='ignore')
        cls.means_val_init, cls.covs_val_init, cls.psis_val_init = output_init(cls.d)
        cls.means_val_init = np.asarray(cls.means_val_init, dtype=theano.config.floatX)
        cls.covs_val_init = np.asarray(cls.covs_val_init, dtype=theano.config.floatX)
        cls.psis_val_init = np.asarray(cls.psis_val_init, dtype=theano.config.floatX)

        cls.LogLs_vec, cls.dLC_vec, cls.dPS_vec, cls.epochs = cls.mix.fit(x=cls.x, dat=cls.d, tol=tol, maxiter=maxiter, verbose=verbose, mode='NLL')

        # get params
        cls.getParams()

        # get prediction
        cls.getPredict()

        # Display some important results
        # Optional: solely used for testing purposes
        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.logLs_val)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.logLs_val.sum())/(-cls.logLs.sum())
        print '\nCov Matrix'
        print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        print '\nLambdas Cov Matrix'
        print norm(cls.correct_lambda_covs - cls.lambda_covs_val)/norm(cls.correct_lambda_covs)
        print '\nPsi Matrix'
        print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        print '\nPi Vector'
        print norm(cls.pi - cls.amps_val, ord=1)/norm(cls.pi, ord=1)

class BatchMFATestCase(BatchMFATestCase):
    """
    Unittests for SwMOFA with an arbitrary number of clusters.
    Train the model using batch algorithm

    Test List:
    TBA

    """
    @classmethod
    def setUpClass(cls, mode='balanced'):
        if cls is BaseTestCase:
            raise unittest.SkipTest("Skip BatchBaseTest and BaseTest tests, they are base class")
        super(BaseTestCase, cls).setUpClass()

        # set internal parameters
        cls.PPCA = False
        cls.lock_psis = True
        tol = 1e-10
        verbose = True
        cls.N = 50000
        cls.K = 3
        cls.D = 3
        cls.M = 2
        cls.batch_size = 10000
        max_epochs=200
        cls.mode = mode

        # set random seed
        cls.seed = 2
        np.random.seed(cls.seed)

        # make data
        correct_first_mean = np.zeros((1,cls.D), dtype=theano.config.floatX)
        correct_first_lambda = np.zeros((1, cls.D, cls.M), dtype=theano.config.floatX)
        cls.makeData(correct_first_mean=correct_first_mean, correct_first_lambda=correct_first_lambda)

        #################################################################
        # start building and training the model
        #################################################################

        # x is the symbolic variable for data
        cls.x = T.matrix('x')

        cls.mix = SwMofa((cls.x).T, cls.K, cls.M, cls.D, cls.batch_size, cls.means_val_init, cls.psis_val_init, cls.PPCA, lock_psis=cls.lock_psis)
        output_init = theano.function([cls.x] , [cls.mix.means, cls.mix.covs, cls.mix.psis], on_unused_input='ignore')
        cls.means_val_init, cls.covs_val_init, cls.psis_val_init = output_init(cls.d)
        cls.means_val_init = np.asarray(cls.means_val_init, dtype=theano.config.floatX)
        cls.covs_val_init = np.asarray(cls.covs_val_init, dtype=theano.config.floatX)
        cls.psis_val_init = np.asarray(cls.psis_val_init, dtype=theano.config.floatX)

        logLs_val, rs_val, betas_val, latents_val, latent_covs_val, sumrs_val, means_val, covs_val,\
        inv_covs_val, lambdas_val, amps_val, psis_val, lambda_covs_val, cls.LogLs_vec, cls.epochs, \
        cls.dLC_vec, cls.dPS_vec = cls.mix.batch_fit(x=cls.x, dat=cls.d, K=cls.K, batch_size=cls.batch_size,
                                                     tol=tol, max_epochs=max_epochs, verbose=verbose, mode='Param')

        cls.means_val, cls.lambdas_val, cls.psis_val, cls.covs_val, cls.inv_covs_val, cls.amps_val, cls.lambda_covs_val \
            = means_val, lambdas_val, psis_val, covs_val, inv_covs_val, amps_val, lambda_covs_val

        cls.getParams()

        cls.getPredict()

        # Display some important results
        # Optional: solely used for testing purposes
        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.logLs_val)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.logLs_val.sum())/(-cls.logLs.sum())
        print '\nCov Matrix'
        print norm(cls.correct_covs - cls.covs_val)/norm(cls.correct_covs)
        print '\nLambdas Cov Matrix'
        print norm(cls.correct_lambda_covs - cls.lambda_covs_val)/norm(cls.correct_lambda_covs)
        print '\nPsi Matrix'
        print norm(cls.correct_psis - cls.psis_val)/norm(cls.correct_psis)
        print '\nPi Vector'
        print norm(cls.pi - cls.amps_val, ord=1)/norm(cls.pi, ord=1)

if __name__ == '__main__':
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromTestCase(SimpleSwMFATestCase)
    runner = unittest.TextTestRunner()
    results = runner.run(suite)
