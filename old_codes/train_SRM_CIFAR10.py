__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from CRM_v1 import CRM
import gzip
import cPickle
import os
import sys
import time
import timeit

import itertools

import numpy as np
from scipy import misc

import theano
import theano.tensor as T

from PIL import Image, ImageDraw

from scipy.linalg import inv, norm

from EM_SRM import EM_SRM_CIFAR10

if __name__ == '__main__':
    data_mode = 'all'
    em_mode = 'hard'
    update_mode = 'hinton'
    stop_mode = 'NLL'
    run_mode = 'train'

    Ni = 50000
    Nsample = 50000
    h = 26 # height of filters
    w = 26 # width of filters
    K = 21# no. filters
    Cin = 3 # depth of filters
    H = 32 # width of input image
    W = 32 # height of input image

    M = 1 # dimension of z
    D = h * w * Cin # patch size
    Np = (H-h+1)*(W-w+1)

    maxiter = 20
    tol = 1e-4
    verbose = True
    batch_size = 1000
    max_epochs = 10

    seed = 20

    output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/EM_results_testing_temp'

    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/EM_results_wh11x11_K10_b1000_cifar10'
    data_dir = '/Users/heatherseeba/repos/em_drm/data/cifar10'
    # output_dir = '/home/ubuntu/research_results/EM_results/EM_results_wh5x5_K21_b1000_cifar10'
    # data_dir = '/home/ubuntu/repos/em_for_drm/data/cifar10'

    cifar10 = EM_SRM_CIFAR10(data_mode=data_mode, em_mode=em_mode, update_mode=update_mode, stop_mode=stop_mode, train_method='EM',
                         Ni=Ni, h=h, w=w, K=K, Cin=Cin, H=H, W=W, M=M,
                         maxiter=maxiter, tol=tol, verbose=verbose, batch_size=batch_size, max_epochs=max_epochs, learning_rate=0.1, seed=seed,
                         output_dir=output_dir, data_dir=data_dir)

    if run_mode == 'train':
        cifar10.train_EM()
    else:
        param_dir = os.path.join(output_dir, 'params','EM_results.npz')
        cifar10.plot_betas_means_lambdas(param_dir=param_dir)
        # cifar10.plot_attention(param_dir=param_dir)
        # cifar10.plot_top_attentions(param_dir=param_dir, N_best_attn=100)
        # cifar10.naive_sample_from_model(param_dir=param_dir, Nsample=50)
        # cifar10.sample_from_model(param_dir=param_dir, Nsample=50)