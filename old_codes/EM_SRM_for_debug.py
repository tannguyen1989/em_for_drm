__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from convmfa_for_debug import ConvMofa
import gzip
import cPickle
import os
import sys
import time

import numpy as np

import theano
import theano.tensor as T

from PIL import Image, ImageDraw

# from guppy import hpy; h=hpy()

from old_codes.nn_functions_stable import LogisticRegression

class EM_SRM(object):
    """
    Apply EM algorithm for RM to an arbitrary dataset

    """
    def __init__(self, data_mode, em_mode, update_mode, stop_mode, train_method, Ni, h, w, K, Cin, H, W, M,
                 maxiter, tol, verbose, batch_size, max_epochs, learning_rate, seed,
                 output_dir, data_dir):

        self.data_mode = data_mode
        self.em_mode = em_mode
        self.update_mode = update_mode
        self.stop_mode = stop_mode
        self.train_method = train_method

         # set internal parameters
        self.Ni = Ni
        self.h = h # height of filters
        self.w = w # width of filters
        self.K = K # no. filters
        self.learning_rate = learning_rate

        self.H = H
        self.W = W
        self.Cin = Cin
        self.M = M

        self.M = M # dimension of z
        self.D = self.h * self.w * self.Cin # patch size
        self.Np = (self.H - self.h + 1)*(self.W - self.w + 1)

        self.maxiter = maxiter
        self.tol = tol
        self.verbose = verbose
        self.batch_size = batch_size
        self.max_epochs = max_epochs
        self.seed = seed

        self.output_dir = output_dir
        self.data_dir = data_dir
        self.param_dir = os.path.join(self.output_dir, 'params')
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        if not os.path.exists(self.param_dir):
            os.makedirs(self.param_dir)

        self.load_data()

    def load_data(self):
        self.d = []
        self.dtest = []
        self.dvalid = []
        self.train_label = []
        self.test_label = []
        self.valid_label = []

    def train_EM(self):
        print 'train_EM'
        np.random.seed(self.seed)
        x_temp = T.tensor4('x_temp')
        y_temp = T.ivector('y_temp')

        means_val_init = np.asarray(np.abs(np.random.randn(self.K, self.Cin, self.h, self.w)), dtype=theano.config.floatX)
        means_val_init = np.reshape(means_val_init, newshape=(self.K, self.D))

        psis_val_init = np.asarray(np.abs(np.random.randn(self.K, self.D)), dtype=theano.config.floatX)

        mix_temp = ConvMofa(x=x_temp, y=y_temp, dat=self.d[0:1000], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Ni,
                       amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                       covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=True,init_ppca=False)

        data_val = mix_temp.createPatches()
        data_valT = data_val.T
        xmin, xmax = np.min(data_valT[0]), np.max(data_valT[0])
        ymin, ymax = np.min(data_valT[1]), np.max(data_valT[1])

        x = T.tensor4('x')
        y = T.ivector('y')

        means_val_init = np.zeros((self.K, self.D), dtype=theano.config.floatX)
        for i in xrange(self.K-1):
            indx_img = np.random.randint(0,np.shape(data_val)[0],size=10)
            means_val_init[i+1] = np.mean(data_val[indx_img], axis=0)

        print 'initial mean'
        print means_val_init

        mix = ConvMofa(x=x, y=y, dat=self.d, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.batch_size,
                       amps_val_init=None, lambdas_val_init=None, lambda_covs_val_init=None,
                       covs_val_init=None, inv_covs_val_init=None, means_val_init=means_val_init, psis_val_init=psis_val_init,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=True,init_ppca=False)

        print 'start training'
        start_time = time.time()

        betas_val, latents_val, latent_covs_val, means_val, covs_val,\
        inv_covs_val, lambdas_val, psis_val, lambda_covs_val, amps_val, epochs, \
        dLC_vec, dPS_vec, dMU_vec, NegLogLs_vec = mix.batch_fit(x=x, y=y, dat=self.d, label=self.train_label, dat_test=self.dtest, label_test=self.test_label, dat_valid=self.dvalid, label_valid=self.valid_label, K=self.K, batch_size=self.batch_size,
                                                                tol=self.tol, max_epochs=self.max_epochs,
                                                                verbose=self.verbose, result_dir=self.param_dir, learning_rate=self.learning_rate, mode=self.stop_mode, train_method=self.train_method)

        stop_time = time.time()
        print 'train takes %f seconds' %(stop_time - start_time)

        fig1 = figure()
        plot(epochs, NegLogLs_vec)
        xlabel('Epoch')
        ylabel('Negative LogLikelihood')
        title('Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i' % self.batch_size)
        fig1.savefig(os.path.join(self.output_dir,'Convergence_of_Negative_LogLikelihood_my_MFA_BatchSize_%i.png' % self.batch_size))
        close()

        fig2 = figure()
        plot(epochs, dLC_vec, 'bo-', label='Lambda_Covs')
        plot(epochs, dMU_vec, 'go-', label='Mu')
        legend()
        xlabel('Epoch')
        ylabel('Relative Difference')
        title('Convergence_of_Parameters_from_my_MFA_BatchSize_%i' % self.batch_size)
        fig2.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_from_my_MFA_BatchSize_%i.png' % self.batch_size))
        close()

    def plot_betas_means_lambdas(self, param_dir):
        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            betas_val = params['betas_val']

        print np.shape(betas_val)
        print np.shape(lambdas_val)
        print np.shape(means_val)

        output_dir = os.path.join(self.output_dir, 'betas_means_lambdas')
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        nrows = (self.K - 1)/5 + (self.K - 1)%5
        new_img_betas = Image.new('RGB', (4000, 600*nrows))
        new_img_lambdas = Image.new('RGB', (4000, 600*nrows))
        new_img_means = Image.new('RGB', (4000, 600*nrows))

        for k in xrange(1, self.K):
            betas_mat = np.reshape(betas_val[k-1,0,:], (self.Cin, self.h, self.w))
            betas_mat = betas_mat.transpose((1,2,0))
            fig = figure()
            if self.Cin == 1:
                imshow(betas_mat, cmap = cm.Greys_r)
            else:
                imshow(betas_mat)

            axis('off')
            savefig(os.path.join(output_dir, 'filter_%d.png' %k))
            close()
            this_img_filter = Image.open(os.path.join(output_dir, 'filter_%i.png'%k))
            x_loc = ((k-1)%5)*800
            y_loc = ((k-1)/5)*600
            new_img_betas.paste(this_img_filter,(x_loc,y_loc))

            lambda_mat = np.reshape(lambdas_val[k], (self.Cin, self.h, self.w))
            lambda_mat = lambda_mat.transpose((1,2,0))
            fig = figure()
            if self.Cin == 1:
                imshow(lambda_mat, cmap = cm.Greys_r)
            else:
                imshow(lambda_mat)

            axis('off')
            savefig(os.path.join(output_dir, 'lambda_%d.png' %k))
            close()
            this_img_lambda = Image.open(os.path.join(output_dir, 'lambda_%i.png'%k))
            x_loc = ((k-1)%5)*800
            y_loc = ((k-1)/5)*600
            new_img_lambdas.paste(this_img_lambda,(x_loc,y_loc))

            mean_mat = np.reshape(means_val[k], (self.Cin, self.h, self.w))
            mean_mat = mean_mat.transpose((1,2,0))
            fig = figure()
            if self.Cin == 1:
                imshow(mean_mat, cmap = cm.Greys_r)
            else:
                imshow(mean_mat)

            axis('off')
            savefig(os.path.join(output_dir, 'mean_%d.png' %k))
            close()
            this_img_mean = Image.open(os.path.join(output_dir, 'mean_%i.png'%k))
            x_loc = ((k-1)%5)*800
            y_loc = ((k-1)/5)*600
            new_img_means.paste(this_img_mean,(x_loc,y_loc))

        imshow(new_img_betas)
        axis('off')
        savefig(os.path.join(output_dir, 'all_filters.pdf'))
        close()

        imshow(new_img_lambdas)
        axis('off')
        savefig(os.path.join(output_dir, 'all_lambdas.pdf'))
        close()

        imshow(new_img_means)
        axis('off')
        savefig(os.path.join(output_dir, 'all_means.pdf'))
        close()

    def sample_from_model(self, param_dir, Nsample = 200):
        sample_dir = os.path.join(self.output_dir, 'samples')
        if not os.path.exists(sample_dir):
            os.makedirs(sample_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        rs_val, z_val = mix._get_Rs_activities(x=x, dat=input_dat)

        print 'Shape of z'
        print np.shape(z_val)

        Nh = self.H - self.h + 1
        Nw = self.W - self.w + 1
        d = np.zeros((Nsample, self.Cin, self.H, self.W), dtype=theano.config.floatX)

        rs = np.reshape(rs_val, newshape=(self.K, Nsample, self.H - self.h + 1, self.W - self.w + 1))
        rs = rs.transpose((1,0,2,3))

        lambdas_val = np.reshape(lambdas_val, newshape=(self.K, self.h, self.w))
        means_val = np.reshape(means_val, newshape=(self.K, self.h, self.w))

        for n in xrange(Nsample):
            for k in xrange(1, self.K):
                for hindx in xrange(Nh):
                    for windx in xrange(Nw):
                        d[n, : , hindx:hindx + self.h, windx:windx + self.w] \
                            = d[n, : , hindx:hindx + self.h, windx:windx + self.w] + \
                              rs[n,k,hindx,windx]*(lambdas_val[k]*z_val[n,k - 1,hindx,windx] + means_val[k])

        d = d.transpose((0,2,3,1))
        print np.shape(d)
        for i in xrange(Nsample):
            imshow(d[i,:,:,0], cmap='gray')
            axis('off')
            out_path = os.path.join(sample_dir, 'SampledImage_%i.jpg'%i)
            savefig(out_path)
            close()

    def naive_sample_from_model(self, param_dir, Nsample = 200):
        sample_dir = os.path.join(self.output_dir, 'naive_samples')
        if not os.path.exists(sample_dir):
            os.makedirs(sample_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        rs_val, z_val = mix._get_Rs_activities(x=x, dat=input_dat)

        Nh = self.H - self.h + 1
        Nw = self.W - self.w + 1
        d = np.zeros((Nsample, self.Cin, self.H, self.W), dtype=theano.config.floatX)

        rs = np.reshape(rs_val, newshape=(self.K, Nsample, self.H - self.h + 1, self.W - self.w + 1))
        rs = rs.transpose((1,0,2,3))

        z_val = z_val[:,0,:]
        z_val = np.reshape(z_val, newshape=(self.K - 1, Nsample, self.H - self.h + 1, self.W - self.w + 1))
        z_val = z_val.transpose((1,0,2,3))

        lambdas_val = np.reshape(lambdas_val, newshape=(self.K, self.h, self.w))
        means_val = np.reshape(means_val, newshape=(self.K, self.h, self.w))

        for n in xrange(Nsample):
            for k in xrange(1, self.K):
                for hindx in xrange(Nh):
                    for windx in xrange(Nw):
                        d[n, : , hindx:hindx + self.h, windx:windx + self.w] \
                            = d[n, : , hindx:hindx + self.h, windx:windx + self.w] + \
                              rs[n,k,hindx,windx]*(lambdas_val[k]*z_val[n,k - 1,hindx,windx] + means_val[k])

        d = d.transpose((0,2,3,1))
        print np.shape(d)
        for i in xrange(Nsample):
            imshow(d[i,:,:,0], cmap='gray')
            axis('off')
            out_path = os.path.join(sample_dir, 'NaiveSampledImage_%i.jpg'%i)
            savefig(out_path)
            close()

    def naive_sample_from_model_Theano(self, param_dir, Nsample = 200):
        # Used to test the Top-Down pass
        sample_dir = os.path.join(self.output_dir, 'topdown_samples')
        if not os.path.exists(sample_dir):
            os.makedirs(sample_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)


        x_val = mix._get_samples(x=x, dat=input_dat)

        Nh = self.H - self.h + 1
        Nw = self.W - self.w + 1
        d = np.zeros((Nsample, self.Cin, self.H, self.W), dtype=theano.config.floatX)

        indx = 0
        for n in xrange(Nsample):
                for hindx in xrange(Nh):
                    for windx in xrange(Nw):
                        d[n, : , hindx:hindx + self.h, windx:windx + self.w] \
                            = d[n, : , hindx:hindx + self.h, windx:windx + self.w] + np.reshape(x_val[indx],(self.Cin, self.h, self.w))
                        indx = indx + 1

        d = d.transpose((0,2,3,1))
        print np.shape(d)
        for i in xrange(Nsample):
            imshow(d[i,:,:,0], cmap='gray')
            axis('off')
            out_path = os.path.join(sample_dir, 'TopDownSampledImage_%i.jpg'%i)
            savefig(out_path)
            close()

    def plot_attention(self, param_dir):
        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:20] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        print 'Start computing activations'

        acts_val = mix._get_activities(x=x, dat=input_dat)
        acts_val = acts_val[:,0,:]
        acts_val = np.reshape(acts_val, newshape=(self.K-1, num_input, self.Np))
        acts_val = acts_val.transpose((1,0,2))
        acts_mean = np.mean(acts_val, axis=(0,2))
        acts_var = np.var(acts_val, axis=(0,2))

        print 'Shape acts_mean'
        print np.shape(acts_mean)

        print 'Shape acts_var'
        print np.shape(acts_var)

        acts_val = (acts_val - acts_mean[None,:,None])/acts_var[None,:,None]
        acts_val = acts_val - np.min(acts_val, axis=(0,2))[None,:,None]
        max_acts_val = np.max(acts_val, axis=(0,2))

        print 'Shape of max_acts_val'
        print np.shape(max_acts_val)

        print 'Shape of acts_val'
        print np.shape(acts_val)

        print 'Done computing activations'

        max_indx = np.argmax(acts_val, axis=2)
        print 'Shape of max_indx'
        print np.shape(max_indx)

        for k in xrange(self.K-1):
            attention_dir = os.path.join(self.output_dir, 'attention_filter%i_each_image'%(k+1))
            if not os.path.exists(attention_dir):
                os.makedirs(attention_dir)

            for n in xrange(num_input):
                im_mat = self.d[n, 0, :, :]
                img = Image.fromarray(im_mat)
                draw = ImageDraw.Draw(img)
                x_indx = max_indx[n,k]%(self.W - self.w + 1)
                y_indx = max_indx[n,k]/(self.W - self.w + 1)
                outline = acts_val[n,k,max_indx[n,k]]/max_acts_val[k]
                draw.rectangle([(x_indx, y_indx),(x_indx + self.w - 1, y_indx + self.h - 1)], outline=outline)
                new_data = np.asarray(img)
                new_data = np.tile(new_data,(3,1,1))
                new_data = new_data.transpose((1,2,0))
                imshow(new_data)
                savefig(os.path.join(attention_dir, 'image%i.png'%(n+1)))
                close()

    def plot_top_attentions(self, param_dir, N_best_attn=100):
        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        x = T.tensor4('x')
        input_dat = self.d[0:self.Ni] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        print 'Start computing activations'

        acts_val = mix._get_activities(x=x, dat=input_dat)
        acts_val = acts_val[:,0,:]
        acts_val = np.reshape(acts_val, newshape=(self.K-1, num_input, self.Np))
        acts_val = acts_val.transpose((1,0,2))
        acts_mean = np.mean(acts_val, axis=(0,2))
        acts_var = np.var(acts_val, axis=(0,2))

        print 'Shape acts_mean'
        print np.shape(acts_mean)

        print 'Shape acts_var'
        print np.shape(acts_var)

        acts_val = (acts_val - acts_mean[None,:,None])/acts_var[None,:,None]
        acts_val = acts_val - np.min(acts_val, axis=(0,2))[None,:,None]
        max_acts_val = np.max(acts_val, axis=(0,2))

        print 'Shape of max_acts_val'
        print np.shape(max_acts_val)

        print 'Shape of acts_val'
        print np.shape(acts_val)

        print 'Done computing activations'

        for k in xrange(self.K - 1):
            filter_dir = os.path.join(self.output_dir, 'attention_filter%i_whole_dataset'%(k+1))
            if not os.path.exists(filter_dir):
                os.makedirs(filter_dir)

            max_indices = np.unravel_index(acts_val[:,k,:].argsort(axis=None)[-N_best_attn:][::-1], acts_val[:,k,:].shape)
            r = max_indices[0]
            c = max_indices[1]

            N_best_attn_per_dim = int(sqrt(N_best_attn))
            new_img = Image.new('RGB', (800*N_best_attn_per_dim, 600*N_best_attn_per_dim))

            for n in xrange(N_best_attn):
                im_mat = self.d[r[n], 0, :, :]
                img = Image.fromarray(im_mat)
                draw = ImageDraw.Draw(img)
                x_indx = c[n]%(self.W - self.w + 1)
                y_indx = c[n]/(self.W - self.w + 1)
                outline = acts_val[r[n],k,c[n]]/max_acts_val[k]
                draw.rectangle([(x_indx, y_indx),(x_indx + self.w - 1, y_indx + self.h - 1)], outline=outline)
                new_data = np.asarray(img)
                new_data = np.tile(new_data,(3,1,1))
                new_data = new_data.transpose((1,2,0))
                imshow(new_data)
                savefig(os.path.join(filter_dir, 'attn%i.png'%(n+1)))
                close()
                this_img = Image.open(os.path.join(filter_dir, 'attn%i.png'%(n+1)))
                x_loc = (n%N_best_attn_per_dim)*800
                y_loc = (n/N_best_attn_per_dim)*600
                new_img.paste(this_img,(x_loc,y_loc))

            imshow(new_img)
            savefig(os.path.join(filter_dir, 'top_attn_for_filter_%i.pdf'%(k+1)))
            close()

class EM_SRM_MNIST(EM_SRM):
    """
    Apply EM algorithm for RM to MNIST

    """
    def load_data(self):
        f = gzip.open(self.data_dir, 'rb')
        train_set, valid_set, test_set = cPickle.load(f)
        f.close()
        self.d = (train_set[0])[0:self.Ni]
        self.train_label = train_set[1][0:self.Ni]

        self.dtest = test_set[0]
        self.Ntest = np.shape(self.dtest)[0]
        self.test_label = test_set[1]

        self.dvalid = valid_set[0]
        self.Nvalid = np.shape(self.dvalid)[0]
        self.valid_label = valid_set[1]

        print 'Shape of training dataset'
        print np.shape(self.d)
        print 'Shape of training labels'
        print np.shape(self.train_label)
        print 'Shape of test dataset'
        print np.shape(self.dtest)
        print 'Shape of test labels'
        print np.shape(self.test_label)

        if not self.data_mode == 'all':
            # select digits
            train_digit_indx = nonzero((self.train_label == 1) + (self.train_label == 7) + (self.train_label == 4))
            self.d = self.d[train_digit_indx[0]]
            self.train_label = self.train_label[train_digit_indx[0]]
            print 'Size of the selected training dataset'
            print np.size(train_digit_indx)

            test_digit_indx = nonzero((self.test_label == 1) + (self.test_label == 7) + (self.test_label == 4))
            self.dtest = self.dtest[test_digit_indx[0]]
            self.test_label = self.test_label[test_digit_indx[0]]
            print 'Size of the selected test dataset'
            print np.shape(test_digit_indx)

            valid_digit_indx = nonzero((self.valid_label == 1) + (self.valid_label == 7) + (self.valid_label == 4))
            self.dvalid = self.dvalid[valid_digit_indx[0]]
            self.valid_label = self.valid_label[valid_digit_indx[0]]
            print 'Size of the selected valid dataset'
            print np.shape(valid_digit_indx)

            self.Ni = np.shape(self.d)[0] # number of input images

        self.d = np.reshape(self.d, newshape=(self.Ni, self.Cin, self.H, self.W))
        self.d = np.float32(self.d)
        self.train_label = np.int32(self.train_label)

        self.dtest = np.reshape(self.dtest, newshape=(self.Ntest, self.Cin, self.H, self.W))
        self.dtest = np.float32(self.dtest)
        self.test_label = np.int32(self.test_label)

        self.dvalid = np.reshape(self.dvalid, newshape=(self.Nvalid, self.Cin, self.H, self.W))
        self.dvalid = np.float32(self.dvalid)
        self.valid_label = np.int32(self.valid_label)

    def do_Classification(self, param_dir, batch_size, learning_rate, n_epochs, Nsample = 50000):
        """

        :param y:
        :param param_dir:
        :param Nsample:
        :return:
        """
        result_dir = os.path.join(self.output_dir, 'classification_results')
        if not os.path.exists(result_dir):
            os.makedirs(result_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        SplitFactorTrain = 50
        xtrain = T.tensor4('xtrain')

        mixtrain = ConvMofa(x=xtrain, dat=self.d[0:self.Ni/SplitFactorTrain], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Ni/SplitFactorTrain,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        ztrain = mixtrain._get_masked_activities(x=xtrain, dat=self.d[0:self.Ni/SplitFactorTrain])
        print 'Shape of ztrain'
        print np.shape(ztrain)

        del mixtrain, xtrain

        for i in xrange(SplitFactorTrain - 1):
            xtrain = T.tensor4('xtrain')
            mixtrain = ConvMofa(x=xtrain, dat=self.d[(i+1)*self.Ni/SplitFactorTrain:(i+2)*self.Ni/SplitFactorTrain], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Ni/SplitFactorTrain,
                           amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                           covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                           PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                           rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

            ztrain_temp = mixtrain._get_masked_activities(x=xtrain, dat=self.d[(i+1)*self.Ni/SplitFactorTrain:(i+2)*self.Ni/SplitFactorTrain])
            ztrain = np.concatenate((ztrain, ztrain_temp), axis=0)
            print 'Shape of ztrain'
            print np.shape(ztrain)

            del mixtrain, xtrain

        print 'Shape of Masked Train Acts'
        print np.shape(ztrain)

        np.save(os.path.join(result_dir,'latents_train.npy'), ztrain)

        #
        SplitFactorTest = 10
        xtest = T.tensor4('xtest')

        mixtest = ConvMofa(x=xtest, dat=self.dtest[0:self.Ntest/SplitFactorTest], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Ntest/SplitFactorTest,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        ztest = mixtest._get_masked_activities(x=xtest, dat=self.dtest[0:self.Ntest/SplitFactorTest])
        print 'Shape of ztest'
        print np.shape(ztest)

        del xtest, mixtest

        for i in xrange(SplitFactorTest - 1):
            xtest = T.tensor4('xtest')
            mixtest = ConvMofa(x=xtest, dat=self.dtest[(i+1)*self.Ntest/SplitFactorTest:(i+2)*self.Ntest/SplitFactorTest], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Ntest/SplitFactorTest,
                           amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                           covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                           PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                           rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

            ztest_temp = mixtest._get_masked_activities(x=xtest, dat=self.dtest[(i+1)*self.Ntest/SplitFactorTest:(i+2)*self.Ntest/SplitFactorTest])
            ztest = np.concatenate((ztest, ztest_temp), axis=0)
            print 'Shape of ztest'
            print np.shape(ztest)
            del mixtest, xtest

        print 'Shape of Masked Test Acts'
        print np.shape(ztest)
        np.save(os.path.join(result_dir,'latents_test.npy'), ztest)

        #
        SplitFactorValid = 10
        xvalid = T.tensor4('xvalid')

        mixvalid = ConvMofa(x=xvalid, dat=self.dvalid[0:self.Nvalid/SplitFactorValid], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Nvalid/SplitFactorValid,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        zvalid = mixvalid._get_masked_activities(x=xvalid, dat=self.dvalid[0:self.Nvalid/SplitFactorValid])
        print 'Shape of zvalid'
        print np.shape(zvalid)

        del xvalid, mixvalid

        for i in xrange(SplitFactorValid - 1):
            xvalid = T.tensor4('xvalid')
            mixvalid = ConvMofa(x=xvalid, dat=self.dvalid[(i+1)*self.Nvalid/SplitFactorValid:(i+2)*self.Nvalid/SplitFactorValid], K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=self.Nvalid/SplitFactorValid,
                           amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                           covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                           PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                           rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

            zvalid_temp = mixvalid._get_masked_activities(x=xvalid, dat=self.dvalid[(i+1)*self.Nvalid/SplitFactorValid:(i+2)*self.Nvalid/SplitFactorValid])
            zvalid = np.concatenate((zvalid, zvalid_temp), axis=0)
            print 'Shape of zvalid'
            print np.shape(zvalid)
            del mixvalid, xvalid

        print 'Shape of Masked Valid Acts'
        print np.shape(zvalid)
        np.save(os.path.join(result_dir,'latents_valid.npy'), zvalid)

        train_set_x = theano.shared(np.asarray(ztrain, dtype=theano.config.floatX),borrow=True)
        test_set_x = theano.shared(np.asarray(ztest, dtype=theano.config.floatX),borrow=True)
        valid_set_x = theano.shared(np.asarray(zvalid, dtype=theano.config.floatX),borrow=True)

        train_set_y = theano.shared(np.asarray(self.train_label, dtype=theano.config.floatX), borrow=True)
        test_set_y = theano.shared(np.asarray(self.test_label, dtype=theano.config.floatX), borrow=True)
        valid_set_y = theano.shared(np.asarray(self.valid_label, dtype=theano.config.floatX), borrow=True)

        train_set_y = T.cast(train_set_y, 'int32')
        test_set_y = T.cast(test_set_y, 'int32')
        valid_set_y = T.cast(valid_set_y, 'int32')

        # compute number of minibatches for training, validation and testing
        n_train_batches = train_set_x.get_value(borrow=True).shape[0]
        n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
        n_test_batches = test_set_x.get_value(borrow=True).shape[0]
        n_train_batches /= batch_size
        n_valid_batches /= batch_size
        n_test_batches /= batch_size

        # allocate symbolic variables for the data
        index = T.lscalar()  # index to a [mini]batch
        x = T.tensor4('x')   # the data is presented as rasterized images
        y = T.ivector('y')  # the labels are presented as 1D vector of
                            # [int] labels

        ######################
        # BUILD ACTUAL MODEL #
        ######################
        print '... building the model'

        softmax_input = x.flatten(2)

        # classify the values of the fully-connected sigmoidal layer
        softmax_layer = LogisticRegression(input=softmax_input, n_in=(self.K - 1)*(self.H - self.h + 1)*(self.W - self.w + 1)/4, n_out=10)

        # the cost we minimize during training is the NLL of the model
        cost = softmax_layer.negative_log_likelihood(y)

        # create a function to compute the mistakes that are made by the model
        test_model = theano.function([index], softmax_layer.errors(y),
                 givens={
                    x: test_set_x[index * batch_size: (index + 1) * batch_size],
                    y: test_set_y[index * batch_size: (index + 1) * batch_size]})

        validate_model = theano.function([index], softmax_layer.errors(y),
                givens={
                    x: valid_set_x[index * batch_size: (index + 1) * batch_size],
                    y: valid_set_y[index * batch_size: (index + 1) * batch_size]})

        # create a list of all model parameters to be fit by gradient descent
        params = softmax_layer.params

        # create a list of gradients for all model parameters
        grads = T.grad(cost, params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.
        updates = []
        for param_i, grad_i in zip(params, grads):
            updates.append((param_i, param_i - learning_rate * grad_i))

        train_model = theano.function([index], cost, updates=updates,
              givens={
                x: train_set_x[index * batch_size: (index + 1) * batch_size],
                y: train_set_y[index * batch_size: (index + 1) * batch_size]})

        ###############
        # TRAIN MODEL #
        ###############
        print '... training'
        # early-stopping parameters
        patience = 10000  # look as this many examples regardless
        patience_increase = 2  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.995  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_train_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_params = None
        best_validation_loss = np.inf
        best_iter = 0
        test_score = 0.
        test_score_vec = []
        epoch_vec = []
        start_time = time.clock()

        epoch = 0
        done_looping = False

        while (epoch < n_epochs) and (not done_looping):
            epoch = epoch + 1
            for minibatch_index in xrange(n_train_batches):

                iter = (epoch - 1) * n_train_batches + minibatch_index

                if iter % 100 == 0:
                    print 'training @ iter = ', iter

                cost_ij = train_model(minibatch_index)

                if (iter + 1) % 1 == 0:

                    # compute zero-one loss on validation set
                    validation_losses = [validate_model(i) for i
                                         in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_train_batches, \
                           this_validation_loss * 100.))

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                        # save best validation score and iteration number
                        best_validation_loss = this_validation_loss
                        best_iter = iter
                        best_params = params

                    # test it on the test set
                    test_losses = [test_model(i) for i in xrange(n_test_batches)]
                    test_score = np.mean(test_losses)
                    print(('     epoch %i, minibatch %i/%i, test error of best '
                           'model %f %%') %
                          (epoch, minibatch_index + 1, n_train_batches,
                           test_score * 100.))

                    if (iter + 1) % batch_size == 0:
                        test_score_vec.append(test_score)
                        epoch_vec.append(epoch)

                if patience <= iter:
                    done_looping = True
                    break

            fig = figure()
            plot(epoch_vec, test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch')
            fig.savefig(os.path.join(result_dir,'Error_vs_Epoch.png'))
            close(fig)


        end_time = time.clock()
        print('Optimization complete.')
        print('Best validation score of %f %% obtained at iteration %i,'\
              'with test performance %f %%' %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))
        print >> sys.stderr, ('The code for file ' +
                              os.path.split(__file__)[1] +
                              ' ran for %.2fm' % ((end_time - start_time) / 60.))

        save_file = open(os.path.join(result_dir,'cnn_mean_removed.pkl'), 'wb')  # this will overwrite current content
        cPickle.dump(best_params, save_file, -1)
        save_file.close()
        fig = figure()
        plot(epoch_vec, test_score_vec)
        legend()
        xlabel('Epoch')
        ylabel('Error Rate')
        title('Error-vs-Epoch-EM')
        fig.savefig(os.path.join(result_dir,'Error_vs_Epoch_EM.png'))
        close(fig)

class EM_SRM_CIFAR10(EM_SRM):
    """
    Apply EM algorithm for RM to CIFAR10

    """
    def unpickle(self, file):
        import cPickle
        fo = open(file, 'rb')
        dict = cPickle.load(fo)
        fo.close()
        return dict

    def load_data(self):
        train_set_1 = self.unpickle(self.data_dir + "/data_batch_1")
        train_set_2 = self.unpickle(self.data_dir + "/data_batch_2")
        train_set_3 = self.unpickle(self.data_dir + "/data_batch_3")
        train_set_4 = self.unpickle(self.data_dir + "/data_batch_4")
        train_set_5 = self.unpickle(self.data_dir + "/data_batch_5")
        test_set = self.unpickle(self.data_dir + "/test_batch")
        train_set = train_set_1
        train_set['data'] = np.concatenate((train_set['data'],train_set_2['data'],train_set_3['data'],train_set_4['data'], train_set_5['data']),axis=0)
        train_set['labels'] = np.concatenate((train_set['labels'],train_set_2['labels'],train_set_3['labels'],train_set_4['labels'], train_set_5['labels']),axis=0)
        self.train_dat = train_set['data']
        self.train_label = train_set['labels']
        self.test_dat = test_set['data']
        self.test_label = test_set['labels']

        print 'Shape of CIFAR10 training dataset'
        print np.shape(self.train_dat)
        print 'Shape of CIFAR10 training labels'
        print np.shape(self.train_label)
        print 'Shape of CIFAR10 test dataset'
        print np.shape(self.test_dat)
        print 'Shape of CIFAR10 test labels'
        print np.shape(self.test_label)

        if not self.data_mode == 'all':
            # select digits
            train_digit_indx = nonzero((self.train_label == 1) + (self.train_label == 7) + (self.train_label == 4))
            self.train_dat = self.train_dat[train_digit_indx[0]]
            self.train_label = self.train_label[train_digit_indx[0]]
            print 'Size of the selected training dataset'
            print np.size(train_digit_indx)

            test_digit_indx = nonzero((self.test_label == 1) + (self.test_label == 7) + (self.test_label == 4))
            self.test_dat = self.test_dat[test_digit_indx[0]]
            self.test_label = self.test_label[test_digit_indx[0]]
            print 'Size of the selected test dataset'
            print np.shape(test_digit_indx)
            Ni = np.shape(self.train_dat)[0] # number of input images

        self.d = np.reshape(self.train_dat[0:self.Ni], newshape=(self.Ni, self.Cin, self.H, self.W))
        self.d = np.float32(self.d)