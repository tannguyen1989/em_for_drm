__all__ = ["Mofa"]

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import numpy as np
import matplotlib.pyplot as pl

from scipy.cluster.vq import kmeans
from scipy.linalg import inv, norm
from matplotlib.patches import Ellipse

import _algorithms

class BatchMofa(object):
    """
    Mixture of Factor Analyzers
    calling arguments:
    [ROSS DOCUMENT HERE]
    internal variables:
    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes
    """
    def __init__(self,data, K, M, batch_size,
                 PPCA=False,lock_psis=False,
                 rs_clip = 0.0,
                 max_condition_number=1.e3,
                 init=True,init_ppca=True):

        # required
        self.K     = K
        self.M     = M
        self.data  = np.atleast_2d(data)
        self.dataT = self.data.T # INSANE DATA DUPLICATION
        self.N     = self.data.shape[0]
        self.D     = self.data.shape[1]
        self.batch_size = batch_size
        self.n_batches = self.N/self.batch_size

        # options
        self.PPCA                 = PPCA
        self.lock_psis            = lock_psis
        self.rs_clip              = rs_clip
        self.max_condition_number = float(max_condition_number)
        assert rs_clip >= 0.0


        # empty arrays to be filled
        self.betas       = np.zeros((self.K,self.M,self.D))
        self.latents     = np.zeros((self.K,self.M,self.N))
        self.latent_covs = np.zeros((self.K,self.M,self.M,self.N))
        self.kmeans_rs   = np.zeros(self.N, dtype=int)
        self.rs          = np.zeros((self.K,self.N))

        # initialize
        if init:
            self._initialize(init_ppca)

    def _initialize(self,init_ppca,maxiter=800, tol=1e-4):

        # Run K-means
        # This is crazy, but DFM's kmeans returns nans/infs
        # for some initializations
        self.means = kmeans(self.data,self.K)[0]
        #self.run_kmeans()

        # Randomly assign factor loadings
        self.lambdas = np.random.randn(self.K,self.D,self.M) / \
            np.sqrt(self.max_condition_number)

        # Set (high rank) variance to variance of all data, along a dimension
        self.psis = np.tile(np.var(self.data,axis=0)[None,:],(self.K,1))

        # Set initial covs
        self.covs = np.zeros((self.K,self.D,self.D))
        self.lambda_covs = np.zeros((self.K,self.D,self.D))
        self.inv_covs = 0. * self.covs
        self._update_covs()

        # Randomly assign the amplitudes.
        self.amps = np.random.rand(self.K)
        self.amps /= np.sum(self.amps)

        if init_ppca:

            # for each cluster, run a PPCA
            for k in range(self.K):

                ind = self.kmeans_rs==k
                self.rs[k,ind] = 1

                sumrs = np.sum(self.rs[k])

                # run em
                L = None
                for i in xrange(maxiter):
                    self._one_component_E_step(k)
                    newL = self._log_sum(
                        self._log_multi_gauss(k,self.data[ind]))
                    newL = np.sum(newL)
                    self._one_component_M_step(k,sumrs,True)
                    self._update_covs()
                    if L!=None:
                        dL = np.abs((newL - L) / L)
                        if i > 5 and dL < tol:
                            break
                    L = newL

    def run_kmeans(self, maxiter=200, tol=1e-4, verbose=True):
        """
        Run the K-means algorithm using the C extension.
        :param maxiter:
            The maximum number of iterations to try.
        :param tol:
            The tolerance on the relative change in the loss function that
            controls convergence.
        :param verbose:
            Print all the messages?
        """
        iterations = _algorithms.kmeans(self.data, self.means,
                                        self.kmeans_rs, tol, maxiter)

        if verbose:
            if iterations < maxiter:
                print("K-means converged after {0} iterations."
                        .format(iterations))
            else:
                print("K-means *didn't* converge after {0} iterations."
                        .format(iterations))


    def run_em(self, maxepoch=200, tol=1e-15, verbose=True):
        """
        Run the EM algorithm.
        :param maxiter:
            The maximum number of iterations to try.
        :param tol:
            The tolerance on the relative change in the loss function that
            controls convergence.
        :param verbose:
            Print all the messages?
        """
        L = None
        self.LogLs_vec = []
        self.epochs = []
        for epoch in xrange(maxepoch):
            for index in xrange(self.n_batches):
                if epoch == 10 and index == 0:
                    print 'stop point for testing'
                dat_batch = self.data[index * self.batch_size: (index + 1) * self.batch_size,:]
                self._E_step(dat=dat_batch, index=index)
                self._M_step(dat=dat_batch, index=index)

            self.logLs, self.rs = self._calc_probs(dat=self.data, batch_size=self.N)
            newL = self.logLs.sum()
            if epoch == 0 and verbose:
                print("Initial NLL =", -newL)

            if L!=None:
                dL = np.abs((newL - L) / L)
                if epoch > 5 and dL < tol:
                    break
            L = newL
            self.LogLs_vec.append(-newL)
            self.epochs.append(epoch)

        if epoch < maxepoch - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL = {0}".format(-newL))
        else:
            print("Warning: EM didn't converge after {0} epochs"
                    .format(epoch))

    def _E_step(self, dat, index):
        """
        Expectation step.  See docs for details.
        """
        # resposibilities and likelihoods
        self.logLs, self.rs = self._calc_probs(dat=dat, batch_size=self.batch_size)

        for k in range(self.K):
            self._one_component_E_step(k=k, dat=dat, index=index)

    def _M_step(self, dat, index):
        """
        Maximization step.  See docs for details.
        This assumes that `_E_step()` has been run.
        """
        sumrs = np.sum(self.rs,axis=1)

        # maximize for each component
        for k in range(self.K):
            self._one_component_M_step(k=k, dat=dat, index=index, sumrs=sumrs[k], PPCA=self.PPCA)
            self.amps[k] = sumrs[k] / self.batch_size

        if self.lock_psis:
            psi = np.dot(sumrs, self.psis) / np.sum(sumrs)
            for k in range(self.K):
                self.psis[k] = psi

        self._update_covs()

    def _one_component_E_step(self, k, dat, index):
        """
        Calculate the E step for one component.
        """
        # beta
        self.betas[k] = np.dot(self.lambdas[k].T,self.inv_covs[k])

        # latent values
        zeroed = dat.T - self.means[k, :, None]
        self.latents[k,:,index * self.batch_size: (index + 1) * self.batch_size] = np.dot(self.betas[k], zeroed)

        # latent empirical covariance
        step1   = self.latents[k, :, None, index * self.batch_size: (index + 1) * self.batch_size]\
                  * self.latents[k, None, :, index * self.batch_size: (index + 1) * self.batch_size]
        step2   = np.dot(self.betas[k], self.lambdas[k])
        self.latent_covs[k,:,:, index * self.batch_size: (index + 1) * self.batch_size] = np.eye(self.M)[:,:,None] - step2[:,:,None] + step1

    def _one_component_M_step(self,k, dat, index, sumrs,PPCA):
        """
        Calculate the M step for one component.
        """
        # means
        lambdalatents = np.dot(self.lambdas[k], self.latents[k,:, index * self.batch_size: (index + 1) * self.batch_size])
        self.means[k] = np.sum(self.rs[k] * (dat.T - lambdalatents), axis=1) / sumrs

        # lambdas
        zeroed = dat.T - self.means[k,:, None]
        self.lambdas[k] = np.dot(np.dot(zeroed[:,None,:] *
                                        self.latents[k,None,:,index * self.batch_size: (index + 1) * self.batch_size]
                                        ,self.rs[k]),
                                 inv(np.dot(self.latent_covs[k, :, :, index * self.batch_size: (index + 1) * self.batch_size]
                                            ,self.rs[k])))
        # psis
        # hacking a floor for psis
        psis = np.dot((zeroed - lambdalatents) * zeroed,self.rs[k]) / sumrs
        maxpsi = np.max(psis)
        maxlam = np.max(np.sum(self.lambdas[k] * self.lambdas[k], axis=0))
        minpsi = np.max([maxpsi, maxlam]) / self.max_condition_number
        psis   = np.clip(psis, minpsi, np.Inf)
        if PPCA:
            psis = np.mean(psis) * np.ones(self.D)
        self.psis[k] = psis


    def _update_covs(self):
        """
        Update self.cov for responsibility, logL calc
        """
        for k in range(self.K):
            self.covs[k] = np.dot(self.lambdas[k],self.lambdas[k].T) + \
                np.diag(self.psis[k])
            self.lambda_covs[k] = np.dot(self.lambdas[k],self.lambdas[k].T)
            self.inv_covs[k] = self._invert_cov(k)

    def _calc_probs(self, dat, batch_size):
        """
        Calculate log likelihoods, responsibilites for each datum
        under each component.
        """
        logrs = np.zeros((self.K, batch_size))
        for k in range(self.K):
            logrs[k] = np.log(self.amps[k]) + self._log_multi_gauss(k, dat)

        # here lies some ghetto log-sum-exp...
        # nothing like a little bit of overflow to make your day better!
        L = self._log_sum(logrs)
        logrs -= L[None, :]
        if self.rs_clip > 0.0:
            logrs = np.clip(logrs,np.log(self.rs_clip),np.Inf)

        return L, np.exp(logrs)

    def _log_multi_gauss(self, k, D):
        """
        Gaussian log likelihood of the data for component k.
        """
        sgn, logdet = np.linalg.slogdet(self.covs[k])
        assert sgn > 0
        X1 = (D - self.means[k]).T
        X2 = np.dot(self.inv_covs[k], X1)
        p = -0.5 * np.sum(X1 * X2, axis=0)
        return -0.5 * np.log(2 * np.pi) * self.D - 0.5 * logdet + p

    def _log_sum(self,loglikes):
        """
        Calculate sum of log likelihoods
        """
        loglikes = np.atleast_2d(loglikes)
        a = np.max(loglikes, axis=0)
        return a + np.log(np.sum(np.exp(loglikes - a[None, :]), axis=0))


    def _invert_cov(self,k):
        """
        Calculate inverse covariance of mofa or ppca model,
        using inversion lemma
        """
        psiI = inv(np.diag(self.psis[k]))
        lam  = self.lambdas[k]
        lamT = lam.T
        step = inv(np.eye(self.M) + np.dot(lamT,np.dot(psiI,lam)))
        step = np.dot(step,np.dot(lamT,psiI))
        step = np.dot(psiI,np.dot(lam,step))
        return psiI - step

    def plot_2d_ellipses(self,d1,d2, **kwargs):
        """
        Make a 2D plot of the model projected onto axes
        d1 and d2.
        """
        for k in range(self.K):
            mean = self.means[k,(d1, d2)]
            cov = self.covs[k][((d1, d2),(d1, d2)), ((d1, d1), (d2, d2))]
            self._plot_2d_ellipse(mean, cov, **kwargs)

    def plot_2d_lambda_covs(self,d1,d2, **kwargs):
        """
        Make a 2D plot of the model projected onto axes
        d1 and d2.
        """
        for k in range(self.K):
            mean = self.means[k,(d1, d2)]
            lambda_covs = self.lambda_covs[k][((d1, d2),(d1, d2)), ((d1, d1), (d2, d2))]
            self._plot_2d_ellipse(mean, lambda_covs, **kwargs)

    def plot_2d_psis(self,d1,d2, **kwargs):
        """
        Make a 2D plot of the model projected onto axes
        d1 and d2.
        """
        for k in range(self.K):
            mean = self.means[k,(d1, d2)]
            psis = np.diag(self.psis[k])[((d1, d2),(d1, d2)), ((d1, d1), (d2, d2))]
            self._plot_2d_ellipse(mean, psis, **kwargs)

    def _plot_2d_ellipse(self, mu, cov, ax=None, **kwargs):
        """
        Plot the error ellipse at a point given it's covariance matrix.
        """
        # some sane defaults
        facecolor = kwargs.pop('facecolor', 'none')
        edgecolor = kwargs.pop('edgecolor', 'k')

        x, y = mu
        U, S, V = np.linalg.svd(cov)
        theta = np.degrees(np.arctan2(U[1, 0], U[0, 0]))
        ellipsePlot = Ellipse(xy=[x, y],
                              width=2 * np.sqrt(S[0]),
                              height=2 * np.sqrt(S[1]),
                              angle=theta,
                facecolor=facecolor, edgecolor=edgecolor, **kwargs)

        if ax is None:
            ax = pl.gca()
        ax.add_patch(ellipsePlot)

##############################################################################################################
# Test Code #
##############################################################################################################
import unittest

class MofaTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        maxiter = 800
        cls.PPCA = False
        cls.lock_psis = True
        tol = 1e-15
        verbose = True
        cls.N = 54000
        cls.K = 2
        cls.D = 3
        cls.M = 2
        cls.batch_size = 500

        cls.seed = 2
        np.random.seed(cls.seed)

        cls.z = np.random.randn(cls.M,cls.N/cls.K)
        correct_lambdas = [np.random.randn(cls.D, cls.M),np.random.randn(cls.D, cls.M)]
        cls.correct_lambdas = np.asarray(correct_lambdas)
        cls.correct_lambda_covs = np.zeros((cls.K, cls.D, cls.D))
        for i in range(2):
            cls.correct_lambda_covs[i] = np.dot(cls.correct_lambdas[i],(cls.correct_lambdas[i]).T)

        cls.correct_means = np.concatenate((2.0*np.ones((1,cls.D)),13.0*np.ones((1,cls.D))))
        d = np.dot(cls.correct_lambdas[1], cls.z) + np.tile(cls.correct_means[1], (cls.N/cls.K,1)).T
        b = np.dot(cls.correct_lambdas[0], cls.z) + np.tile(cls.correct_means[0], (cls.N/cls.K,1)).T
        d = np.concatenate((b,d),axis=1)

        noise = range(cls.D + 1)[1:]

        correct_psis = [noise,noise]

        cls.correct_psis = np.asarray(correct_psis)

        d = d + np.dot(np.diag(np.sqrt(cls.correct_psis[1])),np.random.randn(cls.D,cls.N))

        cls.d = d

        permuted_index = np.random.permutation(cls.N)
        cls.d[:, [range(cls.N), permuted_index]] = cls.d[:, [permuted_index, range(cls.N)]]

        cls.xmin, cls.xmax = np.min(d[0]), np.max(d[0])
        cls.ymin, cls.ymax = np.min(d[1]), np.max(d[1])

        cls.correct_cov1 = cls.correct_lambda_covs[0] + np.diag(cls.correct_psis[0])
        cls.correct_cov2 = cls.correct_lambda_covs[1] + np.diag(cls.correct_psis[1])
        cls.correct_covs = np.asarray([cls.correct_cov1,cls.correct_cov2])

        pi = (1./cls.K)*np.ones((cls.K,1))
        cls.pi = np.asarray(pi)

        cls.inv_correct_covs = 0.*cls.correct_covs

        for k in range(cls.K):
            cls.inv_correct_covs[k] = cls._invert_cov(k)

        cls.logLs = cls.cal_logLs()

        cls.mix = BatchMofa(cls.d.T, cls.K, cls.M, cls.batch_size, PPCA=cls.PPCA, lock_psis=cls.lock_psis, init_ppca=False)
        cls.fig=pl.figure()
        pl.plot(cls.mix.means[:,0],cls.mix.means[:,1],'rx',ms=15,label='Initialization')
        cls.mix.plot_2d_ellipses(0,1,edgecolor='r')

        cls.mix.run_em()

        print '\nLog-Likelihood Vector'
        print norm(cls.logLs - cls.mix.logLs)/norm(cls.logLs)
        print '\nNegative Log-Likelihood'
        print np.abs(-cls.logLs.sum() + cls.mix.logLs.sum())/(-cls.logLs.sum())
        print '\n'
        print '\nCovs'
        print cls.correct_covs
        print '\n'
        print cls.mix.covs
        print '\nLambda Covs'
        print cls.correct_lambda_covs
        print '\n'
        print cls.mix.lambda_covs
        print '\nPsi'
        print cls.correct_psis
        print '\n'
        print cls.mix.psis


    @classmethod
    def tearDownClass(cls, PlotFig = False):
        if PlotFig:
            indx = np.argmax(cls.mix.rs,0)
            indx0 = np.nonzero(indx==0)
            indx1 = np.nonzero(indx==1)

            pl.plot(cls.d[0,indx0], cls.d[1,indx0],'yo',alpha=0.25)
            pl.plot(cls.d[0,indx1], cls.d[1,indx1],'mo',alpha=0.25)

            cls.mix.plot_2d_ellipses(0,1,edgecolor='g')
            cls.mix.plot_2d_lambda_covs(0,1,edgecolor='b')
            cls.mix.plot_2d_psis(0,1,edgecolor='k')
            pl.plot(cls.mix.means[:,0],cls.mix.means[:,1],'gx',ms=15,label='Psi fixed')
            pl.title(r'Data $(D, N) = ({0}, {1})$, Model $(K, M) = ({2}, {3})$'.format(cls.D,cls.N,cls.K,cls.M))
            pl.legend()
            pl.xlim(cls.xmin, cls.xmax)
            pl.ylim(cls.ymin, cls.ymax)
            cls.fig.savefig('mofa_ex_%02d.png' % cls.seed)
            cls.mix = None

    def test_loglikelihood(cls):
        cls.assertLessEqual(norm(cls.logLs - cls.mix.logLs)/norm(cls.logLs),1e-2,
                            'incorrect log-likelihood vector')
        cls.assertLessEqual(np.abs(-cls.logLs.sum() + cls.mix.logLs.sum())/(-cls.logLs.sum()),1e-2,
                            'incorrect negative log-likelihood value')

    def test_rank_lambda_covs(cls):
        for i in range(cls.K):
            cls.assertLessEqual(np.linalg.matrix_rank(cls.mix.lambda_covs[i], tol=1e-1), cls.M,
                             'incorrect rank of lambda covariances')

    def test_convergence(cls):
        """
        Plot loglikelihood vs epochs
        """
        fig = figure()
        plot(cls.mix.epochs, cls.mix.LogLs_vec)
        xlabel('Epoch')
        ylabel('LogLikelihood')
        savefig('Convergence_of_LogLikelihood_BatchVer.png')

    @classmethod
    def _invert_cov(cls,k):
        """
        Calculate inverse covariance of mofa or ppca model,
        using inversion lemma
        """
        psiI = inv(np.diag(cls.correct_psis[k]))
        lam  = cls.correct_lambdas[k]
        lamT = lam.T
        step = inv(np.eye(cls.M) + np.dot(lamT,np.dot(psiI,lam)))
        step = np.dot(step,np.dot(lamT,psiI))
        step = np.dot(psiI,np.dot(lam,step))
        return psiI - step

    @classmethod
    def _log_multi_gauss(cls, k, D):
        """
        Gaussian log likelihood of the data for component k.
        """
        sgn, logdet = np.linalg.slogdet(cls.correct_covs[k])
        assert sgn > 0
        X1 = (D - cls.correct_means[k]).T
        X2 = np.dot(cls.inv_correct_covs[k], X1)
        p = -0.5 * np.sum(X1 * X2, axis=0)
        return -0.5 * np.log(2 * np.pi) * cls.D - 0.5 * logdet + p

    @classmethod
    def _log_sum(cls,loglikes):
        """
        Calculate sum of log likelihoods
        """
        loglikes = np.atleast_2d(loglikes)
        a = np.max(loglikes, axis=0)
        return a + np.log(np.sum(np.exp(loglikes - a[None, :]), axis=0))

    @classmethod
    def cal_logLs(cls):
        """
        Calculate log likelihoods for each datum
        under each component.
        """
        logrs = np.zeros((cls.K, cls.N))
        for k in range(cls.K):
            logrs[k] = np.log(cls.pi[k]) + cls._log_multi_gauss(k, cls.d.T)

        # here lies some ghetto log-sum-exp...
        # nothing like a little bit of overflow to make your day better!
        L = cls._log_sum(logrs)
        return L



if __name__ == '__main__':
    unittest.main()
