__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from convmfa import ConvMofa
import os

import numpy as np

import theano
import theano.tensor as T

from PIL import Image, ImageDraw


# from guppy import hpy; h=hpy()


class PROP_MODEL(object):
    """
    Not work yet, still in progress of developing

    """
    def __init__(self, Ni, h, w, K, Cin, H, W, M, d, update_mode, em_mode, Np, batch_size, max_epochs, learning_rate, seed,
                 output_dir, data_dir):
        '''

        :param Ni:
        :param h:
        :param w:
        :param K:
        :param Cin:
        :param H:
        :param W:
        :param M:
        :param batch_size:
        :param max_epochs:
        :param learning_rate:
        :param seed:
        :param output_dir:
        :param data_dir:
        :return:
        '''
        self.output_dir = output_dir
        self.K = K
        self.Cin = Cin
        self.h = h
        self.w = w
        self.H = H
        self.W = W
        self.M = M
        self.d = d
        self.update_mode = update_mode
        self.em_mode = em_mode
        self.Np = Np

    def plot_betas_means_lambdas(self, param_dir):
        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            betas_val = params['betas_val']

        print np.shape(betas_val)
        print np.shape(lambdas_val)
        print np.shape(means_val)

        num_layer = np.size(means_val)

        output_dir = os.path.join(self.output_dir, 'betas_means_lambdas')
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        for nl in xrange(1,num_layer):
            nrows = (self.K - 1)/5 + (self.K - 1)%5
            new_img_betas = Image.new('RGB', (4000, 600*nrows))
            new_img_lambdas = Image.new('RGB', (4000, 600*nrows))
            new_img_means = Image.new('RGB', (4000, 600*nrows))

            for k in xrange(1, self.K):
                betas_mat = np.reshape(betas_val[nl][k-1,0,:], (self.Cin, self.h, self.w))
                betas_mat = betas_mat.transpose((1,2,0))
                fig = figure()
                if self.Cin == 1:
                    imshow(betas_mat, cmap = cm.Greys_r)
                else:
                    imshow(betas_mat)

                axis('off')
                savefig(os.path.join(output_dir, 'filter_%i_layer_%i.png' %(k, nl + 1)))
                close()
                this_img_filter = Image.open(os.path.join(output_dir, 'filter_%i_layer_%i.png' %(k, nl + 1)))
                x_loc = ((k-1)%5)*800
                y_loc = ((k-1)/5)*600
                new_img_betas.paste(this_img_filter,(x_loc,y_loc))

                lambda_mat = np.reshape(lambdas_val[nl][k], (self.Cin, self.h, self.w))
                lambda_mat = lambda_mat.transpose((1,2,0))
                fig = figure()
                if self.Cin == 1:
                    imshow(lambda_mat, cmap = cm.Greys_r)
                else:
                    imshow(lambda_mat)

                axis('off')
                savefig(os.path.join(output_dir, 'lambda_%i_layer_%i.png' %(k, nl + 1)))
                close()
                this_img_lambda = Image.open(os.path.join(output_dir, 'lambda_%i_layer_%i.png' %(k, nl + 1)))
                x_loc = ((k-1)%5)*800
                y_loc = ((k-1)/5)*600
                new_img_lambdas.paste(this_img_lambda,(x_loc,y_loc))

                mean_mat = np.reshape(means_val[nl][k], (self.Cin, self.h, self.w))
                mean_mat = mean_mat.transpose((1,2,0))
                fig = figure()
                if self.Cin == 1:
                    imshow(mean_mat, cmap = cm.Greys_r)
                else:
                    imshow(mean_mat)

                axis('off')
                savefig(os.path.join(output_dir, 'mean_%i_layer_%i.png' %(k, nl + 1)))
                close()
                this_img_mean = Image.open(os.path.join(output_dir, 'mean_%i_layer_%i.png' %(k, nl + 1)))
                x_loc = ((k-1)%5)*800
                y_loc = ((k-1)/5)*600
                new_img_means.paste(this_img_mean,(x_loc,y_loc))

            imshow(new_img_betas)
            axis('off')
            savefig(os.path.join(output_dir, 'all_filters_layer_%i.pdf' %(nl+1)))
            close()

            imshow(new_img_lambdas)
            axis('off')
            savefig(os.path.join(output_dir, 'all_lambdas_layer_%i.pdf' %(nl + 1)))
            close()

            imshow(new_img_means)
            axis('off')
            savefig(os.path.join(output_dir, 'all_means_layer_%i.pdf' %(nl+1)))
            close()

    def sample_from_model(self, param_dir, Nsample = 200):
        sample_dir = os.path.join(self.output_dir, 'samples')
        if not os.path.exists(sample_dir):
            os.makedirs(sample_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        rs_val, z_val = mix._get_Rs_activities(x=x, dat=input_dat)

        print 'Shape of z'
        print np.shape(z_val)

        Nh = self.H - self.h + 1
        Nw = self.W - self.w + 1
        d = np.zeros((Nsample, self.Cin, self.H, self.W), dtype=theano.config.floatX)

        rs = np.reshape(rs_val, newshape=(self.K, Nsample, self.H - self.h + 1, self.W - self.w + 1))
        rs = rs.transpose((1,0,2,3))

        lambdas_val = np.reshape(lambdas_val, newshape=(self.K, self.h, self.w))
        means_val = np.reshape(means_val, newshape=(self.K, self.h, self.w))

        for n in xrange(Nsample):
            for k in xrange(1, self.K):
                for hindx in xrange(Nh):
                    for windx in xrange(Nw):
                        d[n, : , hindx:hindx + self.h, windx:windx + self.w] \
                            = d[n, : , hindx:hindx + self.h, windx:windx + self.w] + \
                              rs[n,k,hindx,windx]*(lambdas_val[k]*z_val[n,k - 1,hindx,windx] + means_val[k])

        d = d.transpose((0,2,3,1))
        print np.shape(d)
        for i in xrange(Nsample):
            imshow(d[i,:,:,0], cmap='gray')
            axis('off')
            out_path = os.path.join(sample_dir, 'SampledImage_%i.jpg'%i)
            savefig(out_path)
            close()

    def naive_sample_from_model(self, param_dir, Nsample = 200):
        sample_dir = os.path.join(self.output_dir, 'naive_samples')
        if not os.path.exists(sample_dir):
            os.makedirs(sample_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        rs_val, z_val = mix._get_Rs_activities(x=x, dat=input_dat)

        Nh = self.H - self.h + 1
        Nw = self.W - self.w + 1
        d = np.zeros((Nsample, self.Cin, self.H, self.W), dtype=theano.config.floatX)

        rs = np.reshape(rs_val, newshape=(self.K, Nsample, self.H - self.h + 1, self.W - self.w + 1))
        rs = rs.transpose((1,0,2,3))

        z_val = z_val[:,0,:]
        z_val = np.reshape(z_val, newshape=(self.K - 1, Nsample, self.H - self.h + 1, self.W - self.w + 1))
        z_val = z_val.transpose((1,0,2,3))

        lambdas_val = np.reshape(lambdas_val, newshape=(self.K, self.h, self.w))
        means_val = np.reshape(means_val, newshape=(self.K, self.h, self.w))

        for n in xrange(Nsample):
            for k in xrange(1, self.K):
                for hindx in xrange(Nh):
                    for windx in xrange(Nw):
                        d[n, : , hindx:hindx + self.h, windx:windx + self.w] \
                            = d[n, : , hindx:hindx + self.h, windx:windx + self.w] + \
                              rs[n,k,hindx,windx]*(lambdas_val[k]*z_val[n,k - 1,hindx,windx] + means_val[k])

        d = d.transpose((0,2,3,1))
        print np.shape(d)
        for i in xrange(Nsample):
            imshow(d[i,:,:,0], cmap='gray')
            axis('off')
            out_path = os.path.join(sample_dir, 'NaiveSampledImage_%i.jpg'%i)
            savefig(out_path)
            close()

    def naive_sample_from_model_Theano(self, param_dir, Nsample = 200):
        # Used to test the Top-Down pass
        sample_dir = os.path.join(self.output_dir, 'topdown_samples')
        if not os.path.exists(sample_dir):
            os.makedirs(sample_dir)

        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:Nsample] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)


        x_val = mix._get_samples(x=x, dat=input_dat)

        Nh = self.H - self.h + 1
        Nw = self.W - self.w + 1
        d = np.zeros((Nsample, self.Cin, self.H, self.W), dtype=theano.config.floatX)

        indx = 0
        for n in xrange(Nsample):
                for hindx in xrange(Nh):
                    for windx in xrange(Nw):
                        d[n, : , hindx:hindx + self.h, windx:windx + self.w] \
                            = d[n, : , hindx:hindx + self.h, windx:windx + self.w] + np.reshape(x_val[indx],(self.Cin, self.h, self.w))
                        indx = indx + 1

        d = d.transpose((0,2,3,1))
        print np.shape(d)
        for i in xrange(Nsample):
            imshow(d[i,:,:,0], cmap='gray')
            axis('off')
            out_path = os.path.join(sample_dir, 'TopDownSampledImage_%i.jpg'%i)
            savefig(out_path)
            close()

    def plot_attention(self, param_dir):
        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        # Find the max activation in each image
        x = T.tensor4('x')
        input_dat = self.d[0:20] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        print 'Start computing activations'

        acts_val = mix._get_activities(x=x, dat=input_dat)
        acts_val = acts_val[:,0,:]
        acts_val = np.reshape(acts_val, newshape=(self.K-1, num_input, self.Np))
        acts_val = acts_val.transpose((1,0,2))
        acts_mean = np.mean(acts_val, axis=(0,2))
        acts_var = np.var(acts_val, axis=(0,2))

        print 'Shape acts_mean'
        print np.shape(acts_mean)

        print 'Shape acts_var'
        print np.shape(acts_var)

        acts_val = (acts_val - acts_mean[None,:,None])/acts_var[None,:,None]
        acts_val = acts_val - np.min(acts_val, axis=(0,2))[None,:,None]
        max_acts_val = np.max(acts_val, axis=(0,2))

        print 'Shape of max_acts_val'
        print np.shape(max_acts_val)

        print 'Shape of acts_val'
        print np.shape(acts_val)

        print 'Done computing activations'

        max_indx = np.argmax(acts_val, axis=2)
        print 'Shape of max_indx'
        print np.shape(max_indx)

        for k in xrange(self.K-1):
            attention_dir = os.path.join(self.output_dir, 'attention_filter%i_each_image'%(k+1))
            if not os.path.exists(attention_dir):
                os.makedirs(attention_dir)

            for n in xrange(num_input):
                im_mat = self.d[n, 0, :, :]
                img = Image.fromarray(im_mat)
                draw = ImageDraw.Draw(img)
                x_indx = max_indx[n,k]%(self.W - self.w + 1)
                y_indx = max_indx[n,k]/(self.W - self.w + 1)
                outline = acts_val[n,k,max_indx[n,k]]/max_acts_val[k]
                draw.rectangle([(x_indx, y_indx),(x_indx + self.w - 1, y_indx + self.h - 1)], outline=outline)
                new_data = np.asarray(img)
                new_data = np.tile(new_data,(3,1,1))
                new_data = new_data.transpose((1,2,0))
                imshow(new_data)
                savefig(os.path.join(attention_dir, 'image%i.png'%(n+1)))
                close()

    def plot_top_attentions(self, param_dir, N_best_attn=100):
        with load(param_dir) as params:
            means_val = params['means_val']
            lambdas_val = params['lambdas_val']
            lambda_covs_val= params['lambda_covs_val']
            covs_val = params['covs_val']
            inv_covs_val = params['inv_covs_val']
            psis_val = params['psis_val']
            amps_val = params['amps_val']

        x = T.tensor4('x')
        input_dat = self.d[0:self.Ni] # NxKxHxW
        num_input = np.shape(input_dat)[0]

        mix = ConvMofa(x=x, dat=input_dat, K=self.K, M=self.M, W=self.W, H=self.H, w=self.w, h=self.h, Cin=self.Cin, Ni=num_input,
                       amps_val_init=amps_val, lambdas_val_init=lambdas_val, lambda_covs_val_init=lambda_covs_val,
                       covs_val_init=covs_val, inv_covs_val_init=inv_covs_val, means_val_init=means_val, psis_val_init=psis_val,
                       PPCA=False, lock_psis=True, update_mode=self.update_mode, em_mode=self.em_mode,
                       rs_clip = 0.0, max_condition_number=1.e3, init=False, init_ppca=False)

        print 'Start computing activations'

        acts_val = mix._get_activities(x=x, dat=input_dat)
        acts_val = acts_val[:,0,:]
        acts_val = np.reshape(acts_val, newshape=(self.K-1, num_input, self.Np))
        acts_val = acts_val.transpose((1,0,2))
        acts_mean = np.mean(acts_val, axis=(0,2))
        acts_var = np.var(acts_val, axis=(0,2))

        print 'Shape acts_mean'
        print np.shape(acts_mean)

        print 'Shape acts_var'
        print np.shape(acts_var)

        acts_val = (acts_val - acts_mean[None,:,None])/acts_var[None,:,None]
        acts_val = acts_val - np.min(acts_val, axis=(0,2))[None,:,None]
        max_acts_val = np.max(acts_val, axis=(0,2))

        print 'Shape of max_acts_val'
        print np.shape(max_acts_val)

        print 'Shape of acts_val'
        print np.shape(acts_val)

        print 'Done computing activations'

        for k in xrange(self.K - 1):
            filter_dir = os.path.join(self.output_dir, 'attention_filter%i_whole_dataset'%(k+1))
            if not os.path.exists(filter_dir):
                os.makedirs(filter_dir)

            max_indices = np.unravel_index(acts_val[:,k,:].argsort(axis=None)[-N_best_attn:][::-1], acts_val[:,k,:].shape)
            r = max_indices[0]
            c = max_indices[1]

            N_best_attn_per_dim = int(sqrt(N_best_attn))
            new_img = Image.new('RGB', (800*N_best_attn_per_dim, 600*N_best_attn_per_dim))

            for n in xrange(N_best_attn):
                im_mat = self.d[r[n], 0, :, :]
                img = Image.fromarray(im_mat)
                draw = ImageDraw.Draw(img)
                x_indx = c[n]%(self.W - self.w + 1)
                y_indx = c[n]/(self.W - self.w + 1)
                outline = acts_val[r[n],k,c[n]]/max_acts_val[k]
                draw.rectangle([(x_indx, y_indx),(x_indx + self.w - 1, y_indx + self.h - 1)], outline=outline)
                new_data = np.asarray(img)
                new_data = np.tile(new_data,(3,1,1))
                new_data = new_data.transpose((1,2,0))
                imshow(new_data)
                savefig(os.path.join(filter_dir, 'attn%i.png'%(n+1)))
                close()
                this_img = Image.open(os.path.join(filter_dir, 'attn%i.png'%(n+1)))
                x_loc = (n%N_best_attn_per_dim)*800
                y_loc = (n/N_best_attn_per_dim)*600
                new_img.paste(this_img,(x_loc,y_loc))

            imshow(new_img)
            savefig(os.path.join(filter_dir, 'top_attn_for_filter_%i.pdf'%(k+1)))
            close()