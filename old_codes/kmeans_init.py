__author__ = 'minhtannguyen'

import numpy as np

from scipy.cluster.vq import kmeans

import theano

import theano.tensor as T
from theano import function, config, shared, sandbox
from theano import ProfileMode
import warnings
warnings.filterwarnings("ignore")

#import _algorithms

class kmeans_init(object):
    """
    Compute kmeans to initialize MFA
    """
    def __init__(self, data, K, N):
        self.data  = data
        self.K     = K
        self.N     = N
        self.kmeans_rs = np.zeros(self.N, dtype=int)

    def compute_kmeans(self):
        """
        function used to compute kmeans using run_kmeans
        :return:
        """
        self.means = kmeans(self.data,self.K,iter=20)[0]
        self.means = np.asarray(self.means, dtype=theano.config.floatX)
        #self.run_kmeans()
        self.means = theano.shared(value=self.means, name='means', borrow=True)

    def run_kmeans(self, maxiter=800, tol=1e-10, verbose=True):
        """
        Run the K-means algorithm using the C extension.

        :param maxiter:
            The maximum number of iterations to try.

        :param tol:
            The tolerance on the relative change in the loss function that
            controls convergence.

        :param verbose:
            Print all the messages?

        """
        iterations = _algorithms.kmeans(self.data, self.means,
                                        self.kmeans_rs, tol, maxiter)

        if verbose:
            if iterations < maxiter:
                print("K-means converged after {0} iterations."
                      .format(iterations))
            else:
                print("K-means *didn't* converge after {0} iterations."
                      .format(iterations))

def klp_kmeans(data, cluster_num, alpha, epochs = -1, batch = 1, verbose = False, use_gpu=False):
    '''
        Theano based implementation, likely to use GPU as well with required Theano
        configurations. Refer to http://deeplearning.net/software/theano/tutorial/using_gpu.html
        for GPU settings
        Inputs:
            data - [instances x variables] matrix of the data.
            cluster_num - number of requisite clusters
            alpha - learning rate
            epoch - how many epoch you want to go on clustering. If not given, it is set with
                Kohonen's suggestion 500 * #instances
            batch - batch size. Larger batch size is better for Theano and GPU utilization
            verbose - True if you want to verbose the algorithm's iterations
        Output:
            W - final cluster centroids

        Reference: https://github.com/erogol/KLP_KMEANS/blob/master/klp_kmeans.py
    '''
    if use_gpu:
        config.floatX = 'float32' # Theano needs this type of data for GPU use

    warnings.simplefilter("ignore", DeprecationWarning)
    warnings.filterwarnings("ignore")

    rng = np.random
    # From Kohonen's paper
    if epochs == -1:
        print data.shape[0]
        epochs = 500 * data.shape[0]


    if use_gpu == False:
        # Symmbol variables
        X = T.dmatrix('X')
        WIN = T.dmatrix('WIN')

        # Init weights random
        W = theano.shared(rng.randn(cluster_num, data.shape[1]), name="W")
    else:
        # for GPU use
        X = T.matrix('X')
        WIN = T.matrix('WIN')
        W = theano.shared(rng.randn(cluster_num, data.shape[1]).astype(theano.config.floatX), name="W")

    W_old = W.get_value()

    # Find winner unit
    bmu = ((W**2).sum(axis=1, keepdims=True) + (X**2).sum(axis=1, keepdims=True).T - 2*T.dot(W, X.T)).argmin(axis=0)
    dist = T.dot(WIN.T, X) - WIN.sum(0)[:, None] * W
    err = abs(dist).sum()/X.shape[0]

    update = function([X,WIN],outputs=err,updates=[(W, W + alpha * dist)], allow_input_downcast=True)
    find_bmu = function([X], bmu, allow_input_downcast=True)

    if any([x.op.__class__.__name__ in ['Gemv', 'CGemv', 'Gemm', 'CGemm'] for x in
            update.maker.fgraph.toposort()]):
        print 'Used the cpu'
    elif any([x.op.__class__.__name__ in ['GpuGemm', 'GpuGemv'] for x in
              update.maker.fgraph.toposort()]):
        print 'Used the gpu'
    else:
        print 'ERROR, not able to tell if theano used the cpu or the gpu'
        print update.maker.fgraph.toposort()


    # Update
    for epoch in range(epochs):
        C = 0
        for i in range(0, data.shape[0], batch):
            batch_data = data[i:i+batch, :]
            D = find_bmu(batch_data)
            # for GPU use
            if use_gpu:
                S = np.zeros([batch,cluster_num], config.floatX)
            else:
                S = np.zeros([batch_data.shape[0],cluster_num])
            S[:,D] = 1
            cost = update(batch_data, S)

        if epoch%10 == 0 and verbose:
            print "Avg. centroid distance -- ", cost.sum(),"\t EPOCH : ", epoch
    return W.get_value()
