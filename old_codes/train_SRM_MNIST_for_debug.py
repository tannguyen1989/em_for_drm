__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from convmfa import ConvMofa
import gzip
import cPickle
import os
import sys
import time
import timeit

import itertools

import numpy as np
from scipy import misc

import theano
import theano.tensor as T

from PIL import Image, ImageDraw

from scipy.linalg import inv, norm

from EM_SRM_for_debug import EM_SRM_MNIST

if __name__ == '__main__':
    data_mode = 'all'
    em_mode = 'hard'
    update_mode = 'hinton'
    stop_mode = 'NLL'
    run_mode = 'train'
    train_method = 'EG'

    Ni = 50000
    Nsample = 50000
    h = 5 # height of filters
    w = 5 # width of filters
    K = 21 # no. filters
    Cin = 1 # depth of filters
    H = 28 # width of input image
    W = 28 # height of input image
    learning_rate = 0.1

    M = 1 # dimension of z
    D = h * w * Cin # patch size
    Np = (H-h+1)*(W-w+1)

    maxiter = 20
    tol = 1e-4
    verbose = True
    batch_size = 500
    max_epochs = 15

    seed = 20

    output_dir = '/home/ubuntu/research_results/EM_results/EG_results_wh5x5_K21_b500'
    data_dir = '/home/ubuntu/repos/em_for_drm/data/mnist.pkl.gz'

    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results/EG_results_wh5x5_K21_b500'
    # data_dir = '/Users/heatherseeba/repos/em_drm/data/mnist.pkl.gz'

    mnist = EM_SRM_MNIST(data_mode=data_mode, em_mode=em_mode, update_mode=update_mode, stop_mode=stop_mode, train_method=train_method,
                         Ni=Ni, h=h, w=w, K=K, Cin=Cin, H=H, W=W, M=M,
                         maxiter=maxiter, tol=tol, verbose=verbose, batch_size=batch_size, max_epochs=max_epochs, learning_rate=learning_rate, seed=seed,
                         output_dir=output_dir, data_dir=data_dir)

    if run_mode == 'train':
        mnist.train_EM()
    else:
        param_dir = os.path.join(output_dir, 'params','EM_results_epoch_1.npz')
        # mnist.plot_betas_means_lambdas(param_dir=param_dir)
        # mnist.plot_attention(param_dir=param_dir)
        # mnist.plot_top_attentions(param_dir=param_dir, N_best_attn=100)
        # mnist.naive_sample_from_model(param_dir=param_dir, Nsample=50)
        # mnist.sample_from_model(param_dir=param_dir, Nsample=50)
        # mnist.naive_sample_from_model_Theano(param_dir=param_dir, Nsample=50)
        mnist.do_Classification(param_dir=param_dir, batch_size=500, learning_rate=0.1, n_epochs=200, Nsample = Nsample)