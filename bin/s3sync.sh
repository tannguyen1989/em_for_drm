#!/usr/bin/env bash
# run this from the desktop to sync from s3 to my local desktop /Users/heatherseeba/aws/drm-em-results
aws s3 sync s3://drm-em-results /Users/heatherseeba/aws/drm-em-results --exclude "*.pkl" --exclude "*.zip"


