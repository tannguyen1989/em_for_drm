import numpy
import tensorflow as tf
import theano
import theano.tensor as T

class LogisticRegression(object):
    """Multi-class Logistic Regression Class

    The logistic regression is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`. Classification is done by projecting data
    points onto a set of hyperplanes, the distance to which is used to
    determine a class membership probability.
    """

    def __init__(self, input, n_in, n_out, W_init=None, b_init=None, input_clean=None):
        """ Initialize the parameters of the logistic regression

        :type input: theano.tensor.TensorType
        :param input: symbolic variable that describes the input of the
                      architecture (one minibatch)

        :type n_in: int
        :param n_in: number of input units, the dimension of the space in
                     which the datapoints lie

        :type n_out: int
        :param n_out: number of output units, the dimension of the space in
                      which the labels lie

        """
        self.input_clean = input_clean

        # initialize with 0 the weights W as a matrix of shape (n_in, n_out)
        if W_init == None:
            self.W = theano.shared(value=numpy.zeros((n_in, n_out),
                                                     dtype=theano.config.floatX),
                                    name='W', borrow=True)
        else:
            print(W_init)
            self.W = theano.shared(value=W_init, name='W', borrow=True)
        # initialize the baises b as a vector of n_out 0s
        if b_init == None:
            self.b = theano.shared(value=numpy.zeros((n_out,),
                                                     dtype=theano.config.floatX),
                                   name='b', borrow=True)
        else:
            print(b_init)
            self.b = theano.shared(value=b_init, name='b', borrow=True)

        # compute vector of class-membership probabilities in symbolic form
        self.gammas = T.nnet.softmax(T.dot(input, self.W) + self.b)

        # compute prediction as class whose probability is maximal in
        # symbolic form
        self.y_pred = T.argmax(self.gammas, axis=1)

        if input_clean != None:
            # compute vector of class-membership probabilities in symbolic form
            self.gammas_clean = T.nnet.softmax(T.dot(input_clean, self.W) + self.b)

            # compute prediction as class whose probability is maximal in
            # symbolic form
            self.y_pred_clean = T.argmax(self.gammas_clean, axis=1)

        # parameters of the model
        self.params = [self.W, self.b]

    def negative_log_likelihood(self, y):
        """Return the mean of the negative log-likelihood of the prediction
        of this model under a given target distribution.

        .. math::

            \frac{1}{|\mathcal{D}|} \mathcal{L} (\theta=\{W,b\}, \mathcal{D}) =
            \frac{1}{|\mathcal{D}|} \sum_{i=0}^{|\mathcal{D}|} \log(P(Y=y^{(i)}|x^{(i)}, W,b)) \\
                \ell (\theta=\{W,b\}, \mathcal{D})

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label

        Note: we use the mean instead of the sum so that
              the learning rate is less dependent on the batch size
        """
        # y.shape[0] is (symbolically) the number of rows in y, i.e.,
        # number of examples (call it n) in the minibatch
        # T.arange(y.shape[0]) is a symbolic vector which will contain
        # [0,1,2,... n-1] T.log(self.p_y_given_x) is a matrix of
        # Log-Probabilities (call it LP) with one row per example and
        # one column per class LP[T.arange(y.shape[0]),y] is a vector
        # v containing [LP[0,y[0]], LP[1,y[1]], LP[2,y[2]], ...,
        # LP[n-1,y[n-1]]] and T.mean(LP[T.arange(y.shape[0]),y]) is
        # the mean (across minibatch examples) of the elements in v,
        # i.e., the mean log-likelihood across the minibatch.
        return -T.mean(T.log(self.gammas)[T.arange(y.shape[0]), y])

    def errors(self, y):
        """Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        """

        # check if y has same dimension of y_pred
        if y.ndim != self.y_pred.ndim:
            raise TypeError(
                'y should have the same shape as self.y_pred',
                ('y', y.type, 'y_pred', self.y_pred.type)
            )
        # check if y is of the correct datatype
        if y.dtype.startswith('int'):
            # the T.neq operator returns a vector of 0s and 1s, where 1
            # represents a mistake in prediction
            if self.input_clean == None:
                return T.mean(T.neq(self.y_pred, y))
            else:
                return T.mean(T.neq(self.y_pred_clean, y))
        else:
            raise NotImplementedError()

class SoftmaxNonlinearity(LogisticRegression):
    def __init__(self, input, input_clean=None):
        self.input_clean = input_clean
        self.gammas = tf.nn.softmax(input)
        self.y_pred = tf.argmax(self.gammas, 1)
        if input_clean != None:
            # compute vector of class-membership probabilities in symbolic form
            self.gammas_clean =  tf.nn.softmax(input_clean)
            # compute prediction as class whose probability is maximal in
            # symbolic form
            self.y_pred_clean = tf.argmax(self.gammas_clean, 1)

class LogisticRegressionForSemisupervised(object):
    """Multi-class Logistic Regression Class

    The logistic regression is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`. Classification is done by projecting data
    points onto a set of hyperplanes, the distance to which is used to
    determine a class membership probability.
    """

    def __init__(self, input, n_in, n_out, W_init=None, b_init=None, input_clean=None):
        """ Initialize the parameters of the logistic regression

        :type input: theano.tensor.TensorType
        :param input: symbolic variable that describes the input of the
                      architecture (one minibatch)

        :type n_in: int
        :param n_in: number of input units, the dimension of the space in
                     which the datapoints lie

        :type n_out: int
        :param n_out: number of output units, the dimension of the space in
                      which the labels lie

        """

        self.input_clean = input_clean
        # initialize with 0 the weights W as a matrix of shape (n_in, n_out)
        if W_init == None:
            self.W = theano.shared(value=numpy.zeros((n_in, n_out),
                                                     dtype=theano.config.floatX),
                                    name='W', borrow=True)
        else:
            print(W_init)
            self.W = theano.shared(value=W_init, name='W', borrow=True)
        # initialize the baises b as a vector of n_out 0s
        if b_init == None:
            self.b = theano.shared(value=numpy.zeros((n_out,),
                                                     dtype=theano.config.floatX),
                                   name='b', borrow=True)
        else:
            print(b_init)
            self.b = theano.shared(value=b_init, name='b', borrow=True)

        # compute vector of class-membership probabilities in symbolic form
        self.gammas = T.nnet.softmax(T.dot(input, self.W) + self.b)

        # compute prediction as class whose probability is maximal in
        # symbolic form
        self.y_pred = T.argmax(self.gammas, axis=1)
        self.gammas_semisupervised = T.concatenate((self.gammas, T.ones_like((self.y_pred.dimshuffle(0,'x')))), axis=1)

        if input_clean != None:
            # compute vector of class-membership probabilities in symbolic form
            self.gammas_clean = T.nnet.softmax(T.dot(input_clean, self.W) + self.b)

            # compute prediction as class whose probability is maximal in
            # symbolic form
            self.y_pred_clean = T.argmax(self.gammas_clean, axis=1)
            self.gammas_semisupervised_clean = T.concatenate((self.gammas_clean, T.ones_like((self.y_pred_clean.dimshuffle(0, 'x')))),
                                                       axis=1)

        # parameters of the model
        self.params = [self.W, self.b]

    def negative_log_likelihood(self, y):
        """Return the mean of the negative log-likelihood of the prediction
        of this model under a given target distribution.

        .. math::

            \frac{1}{|\mathcal{D}|} \mathcal{L} (\theta=\{W,b\}, \mathcal{D}) =
            \frac{1}{|\mathcal{D}|} \sum_{i=0}^{|\mathcal{D}|} \log(P(Y=y^{(i)}|x^{(i)}, W,b)) \\
                \ell (\theta=\{W,b\}, \mathcal{D})

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label

        Note: we use the mean instead of the sum so that
              the learning rate is less dependent on the batch size
        """
        # y.shape[0] is (symbolically) the number of rows in y, i.e.,
        # number of examples (call it n) in the minibatch
        # T.arange(y.shape[0]) is a symbolic vector which will contain
        # [0,1,2,... n-1] T.log(self.p_y_given_x) is a matrix of
        # Log-Probabilities (call it LP) with one row per example and
        # one column per class LP[T.arange(y.shape[0]),y] is a vector
        # v containing [LP[0,y[0]], LP[1,y[1]], LP[2,y[2]], ...,
        # LP[n-1,y[n-1]]] and T.mean(LP[T.arange(y.shape[0]),y]) is
        # the mean (across minibatch examples) of the elements in v,
        # i.e., the mean log-likelihood across the minibatch.
        self.Nlabeled = T.sum(T.cast(T.neq(y, T.max(y)), theano.config.floatX))
        return -T.sum(T.log(self.gammas_semisupervised)[T.arange(y.shape[0]), y])/(self.Nlabeled + 1e-16)

    def errors(self, y):
        """Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        """

        # check if y has same dimension of y_pred
        if y.ndim != self.y_pred.ndim:
            raise TypeError(
                'y should have the same shape as self.y_pred',
                ('y', y.type, 'y_pred', self.y_pred.type)
            )
        # check if y is of the correct datatype
        if y.dtype.startswith('int'):
            # the T.neq operator returns a vector of 0s and 1s, where 1
            # represents a mistake in prediction
            if self.input_clean == None:
                return T.mean(T.neq(self.y_pred, y))
            else:
                return T.mean(T.neq(self.y_pred_clean, y))
        else:
            raise NotImplementedError()

class SoftmaxNonlinearitySemisupervised(LogisticRegressionForSemisupervised):
    def __init__(self, input, input_clean=None):
        self.input_clean = input_clean
        self.gammas =  tf.nn.softmax(input)
        self.y_pred = tf.argmax(self.gammas, 1)
        self.gammas_semisupervised = tf.concat(1, (self.gammas, T.ones_like((self.y_pred.dimshuffle(0, 'x')))))
        if input_clean != None:
            # compute vector of class-membership probabilities in symbolic form
            self.gammas_clean =  tf.nn.softmax(input_clean)
            # compute prediction as class whose probability is maximal in
            # symbolic form
            self.y_pred_clean = tf.argmax(self.gammas_clean, 1)
            self.gammas_semisupervised_clean = tf.concat(1, (self.gammas_clean, T.ones_like((self.y_pred_clean.dimshuffle(0, 'x')))))

class HiddenLayerInSoftmax(object):
    def __init__(self, input, n_in, n_out, W_init=None, b_init=None, input_clean=None):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The nonlinearity used here is tanh

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        """
        if W_init == None:
            self.W = tf.Variable(tf.random_normal([n_in, n_out], stddev=1.0), name='W', dtype=tf.float32)
            # self.W = tf.Variable(tf.zeros([n_in, n_out]), name='W', dtype=tf.float32)
            # self.W = tf.random_normal([n_in, n_out], stddev=0.1)
        else:
            self.W = tf.Variable(W_init, name='W', dtype=tf.float32)
        # initialize the biases b as a vector of n_out 0s
        if b_init == None:
            self.b = tf.Variable(tf.zeros([n_out,]), name='b', dtype=tf.float32)
        else:
            self.b = tf.Variable(b_init, name='b', dtype=tf.float32)


        self.output = tf.matmul(input, self.W) + self.b

        if input_clean != None:
            self.output_clean = tf.matmul(input_clean, self.W) + self.b

        # parameters of the model
        self.params = [self.W, self.b]

class ConvLayerInSoftmax(object):
    def __init__(self, input, filter_shape, image_shape, W_init=None, b_init=None, input_clean=None):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The nonlinearity used here is tanh

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        """
        if W_init == None:
            self.W = theano.shared(value=numpy.zeros(filter_shape,
                                                     dtype=theano.config.floatX),
                                    name='W', borrow=True)
        else:
            self.W = theano.shared(value=W_init, name='W', borrow=True)
        # initialize the baises b as a vector of n_out 0s
        if b_init == None:
            self.b = theano.shared(value=numpy.zeros((filter_shape[0],),
                                                     dtype=theano.config.floatX),
                                   name='b', borrow=True)
        else:
            self.b = theano.shared(value=b_init, name='b', borrow=True)

        conv_out = T.nnet.conv2d(input=input, filters=self.W, filter_shape=filter_shape, image_shape=image_shape, filter_flip=False, border_mode='valid')

        output = conv_out + self.b.dimshuffle('x', 0, 'x', 'x')
        self.output = output.flatten(2)

        if input_clean != None:
            conv_out_clean = T.nnet.conv2d(input=input_clean, filters=self.W, filter_shape=filter_shape, image_shape=image_shape,
                                     filter_flip=False, border_mode='valid')
            output_clean = conv_out_clean + self.b.dimshuffle('x', 0, 'x', 'x')
            self.output_clean = output_clean.flatten(2)

        # parameters of the model
        self.params = [self.W, self.b]


class HiddenLayer(object):
    def __init__(self, rng, input, n_in, n_out, W=None, b=None,
                 activation='relu'):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The nonlinearity used here is tanh

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer
        """
        self.input = input

        # `W` is initialized with `W_values` which is uniformely sampled
        # from sqrt(-6./(n_in+n_hidden)) and sqrt(6./(n_in+n_hidden))
        # for tanh activation function
        # the output of uniform if converted using asarray to dtype
        # theano.config.floatX so that the code is runable on GPU
        # Note : optimal initialization of weights is dependent on the
        #        activation function used (among other things).
        #        For example, results presented in [Xavier10] suggest that you
        #        should use 4 times larger initial weights for sigmoid
        #        compared to tanh
        #        We have no info for other function, so we use the same as
        #        tanh.
        if W is None:
            W_values = numpy.asarray(rng.uniform(
                    low=-numpy.sqrt(6. / (n_in + n_out)),
                    high=numpy.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)), dtype=theano.config.floatX)
            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4

            W = theano.shared(value=W_values, name='W', borrow=True)

        if b is None:
            b_values = numpy.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)

        self.W = W
        self.b = b

        lin_output = T.dot(input, self.W) + self.b
        if activation is None:
            self.output = lin_output
        elif activation == "relu":
            self.output = lin_output * (lin_output > 0.) + 0. * lin_output * (lin_output < 0.)
        else:
            self.output = activation(lin_output)

        self.gammas = T.nnet.softmax(self.output)

        # parameters of the model
        self.params = [self.W, self.b]

class LeNetConvPoolLayer(object):
    """Pool Layer of a convolutional network """

    def __init__(self, rng, input, filter_shape, image_shape, poolsize=(2, 2), activation='relu'):
        """
        Allocate a LeNetConvPoolLayer with shared variable internal parameters.

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dtensor4
        :param input: symbolic image tensor, of shape image_shape

        :type filter_shape: tuple or list of length 4
        :param filter_shape: (number of filters, num input feature maps,
                              filter height,filter width)

        :type image_shape: tuple or list of length 4
        :param image_shape: (batch size, num input feature maps,
                             image height, image width)

        :type poolsize: tuple or list of length 2
        :param poolsize: the downsampling (pooling) factor (#rows,#cols)
        """

        assert image_shape[1] == filter_shape[1]
        self.input = input

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = numpy.prod(filter_shape[1:])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = (filter_shape[0] * numpy.prod(filter_shape[2:]) /
                   numpy.prod(poolsize))
        # initialize weights with random weights
        W_bound = numpy.sqrt(6. / (fan_in + fan_out))
        self.W = theano.shared(numpy.asarray(
            rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
            dtype=theano.config.floatX),
                               borrow=True)

        # the bias is a 1D tensor -- one bias per output feature map
        b_values = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b = theano.shared(value=b_values, borrow=True)

        # convolve input feature maps with filters
        conv_out = conv.conv2d(input=input, filters=self.W,
                filter_shape=filter_shape, image_shape=image_shape)

        # add the bias term. Since the bias is a vector (1D array), we first
        # reshape it to a tensor of shape (1,n_filters,1,1). Each bias will
        # thus be broadcasted across mini-batches and feature map
        # width & height
        lin_out = conv_out + self.b.dimshuffle('x', 0, 'x', 'x')
        if activation is None:
            nonlin_out = lin_out + self.b.dimshuffle('x', 0, 'x', 'x')
        elif activation == "relu":
            nonlin_out = lin_out * (lin_out > 0.) + 0. * lin_out * (lin_out < 0.)
        else:
            nonlin_out = T.tanh(lin_out)


        self.gammas = T.nnet.softmax(T.flatten(nonlin_out,outdim=2))
        self.gammas = T.reshape(self.gammas,T.shape(nonlin_out))

        # downsample each feature map individually, using maxpooling
        self.output = downsample.max_pool_2d(input=nonlin_out,
                                            ds=poolsize, ignore_border=True)

        # store parameters of this layer
        self.params = [self.W, self.b]

class BatchNormalization(object):
    def __init__(self, insize, momentum, is_train, mode=0, epsilon=1e-10, gamma_init=None, beta_init=None, mean_init=None, var_init=None):
        '''
        # params :
        mode :
            0 : feature-wise mode (normal BN)
            1 : window-wise mode (CNN mode BN)
        momentum : momentum for exponential average.
        '''
        self.insize = insize
        self.mode = mode
        self.momentum = momentum
        self.is_train = is_train
        self.run_mode = 0  # run_mode :
        self.epsilon = epsilon

        # random setting of gamma and beta, setting initial mean and std
        # rng = np.random.RandomState(int(time.time()))
        # self.gamma = theano.shared(np.asarray(
        #     rng.uniform(low=-1.0 / math.sqrt(self.insize), high=1.0 / math.sqrt(self.insize), size=(insize)),
        #     dtype=theano.config.floatX), name='gamma', borrow=True)
        if gamma_init == None:
            self.gamma = tf.Variable(tf.ones((insize)), name='gamma_bn', dtype=tf.float32)
        else:
            self.gamma = tf.Variable(gamma_init, name='gamma_bn', dtype=tf.float32)

        if beta_init == None:
            self.beta = tf.Variable(tf.zeros((insize)), name='beta_bn', dtype=tf.float32)
        else:
            self.beta = tf.Variable(beta_init, name='beta_bn', dtype=tf.float32)

        if mean_init == None:
            self.mean = tf.Variable(tf.zeros((insize)), name='mean_bn', dtype=tf.float32)
        else:
            self.mean = tf.Variable(mean_init, name='mean_bn', dtype=tf.float32)

        if var_init == None:
            self.var = tf.Variable(tf.ones((insize)), name='var_bn', dtype=tf.float32)
        else:
            self.var = tf.Variable(var_init, name='var_bn', dtype=tf.float32)

        # parameter save for update
        self.params = [self.gamma, self.beta]

    def set_runmode(self, run_mode):
        self.run_mode = run_mode

    def get_result(self, input, input_shape):
        # returns BN result for given input.

        if self.mode == 0:
            print('Use Feature-wise BN')
            if self.run_mode == 0:
                now_mean, now_var = tf.nn.moments(input, [0])
                now_normalize = (input - now_mean) / tf.sqrt(now_var + self.epsilon)  # should be broadcastable..
                output = self.gamma * now_normalize + self.beta
                # mean, var update
                self.mean = self.momentum * self.mean + (1.0 - self.momentum) * now_mean
                self.var = self.momentum * self.var \
                           + (1.0 - self.momentum) * (input_shape[0] / (input_shape[0] - 1) * now_var)
            elif self.run_mode == 1:
                now_mean, now_var = tf.nn.moments(input, [0])
                output = self.gamma * (input - now_mean) / tf.sqrt(now_var + self.epsilon) + self.beta
            else:
                now_mean = self.mean
                now_var = self.var
                output = self.gamma * (input - now_mean) / tf.sqrt(now_var + self.epsilon) + self.beta

        else:
            # in CNN mode, gamma and beta exists for every single channel separately.
            # for each channel, calculate mean and std for (mini_batch_size * row * column) elements.
            # then, each channel has own scalar gamma/beta parameters.
            print('Use Layer-wise BN')
            if self.run_mode == 0:
                now_mean, now_var = tf.nn.moments(input, [0, 2, 3])

                # mean, var update
                self.mean_new = self.momentum * self.mean + (1.0 - self.momentum) * now_mean
                self.var_new = self.momentum * self.var \
                           + (1.0 - self.momentum) * (input_shape[0] / (input_shape[0] - 1) * now_var)

                now_mean_4D = tf.cond(tf.not_equal(self.is_train, 0), lambda: self.change_shape(now_mean, input_shape), lambda: self.change_shape(self.mean_new, input_shape))
                now_var_4D = tf.cond(tf.not_equal(self.is_train, 0), lambda: self.change_shape(now_var, input_shape), lambda: self.change_shape(self.var_new, input_shape))

                # now_mean_4D = self.change_shape(self.mean_new, input_shape)
                # now_var_4D = self.change_shape(self.var_new, input_shape)

                now_gamma_4D = self.change_shape(self.gamma, input_shape)
                now_beta_4D = self.change_shape(self.beta, input_shape)

                output = now_gamma_4D * (input - now_mean_4D) / tf.sqrt(now_var_4D + self.epsilon) + now_beta_4D

            else:
                now_mean, now_var = tf.nn.moments(input, [0, 2, 3])
                # now_mean_new_shape = self.change_shape(now_mean)
                # now_var = T.sqr(T.mean(T.abs_(input - now_mean_new_shape), axis=(0, 2, 3)))

                now_mean_4D = self.change_shape(now_mean, input_shape)
                now_var_4D = self.change_shape(now_var, input_shape)

                now_gamma_4D = self.change_shape(self.gamma, input_shape)
                now_beta_4D = self.change_shape(self.beta, input_shape)

                output = now_gamma_4D * (input - now_mean_4D) / tf.sqrt(now_var_4D + self.epsilon) + now_beta_4D

        return output

    # changing shape for CNN mode
    def change_shape(self, vec, input_shape):
        return tf.reshape(tf.reshape(tf.tile(tf.reshape(vec, [-1,1]), [1, input_shape[2] * input_shape[3]]), [-1]),
            [input_shape[1], input_shape[2], input_shape[3]])
