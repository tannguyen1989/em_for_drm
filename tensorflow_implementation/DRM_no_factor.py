__author__ = 'tannguyen'

from nn_functions import LogisticRegression, LogisticRegressionForSemisupervised, SoftmaxNonlinearity, SoftmaxNonlinearitySemisupervised
import numpy as np


# from guppy import hpy; h=hpy()

class DRM_model(object):
    '''
    Class of DRM models
    '''

    def __init__(self, seed, train_mode='supervised', grad_min=-np.inf, grad_max=np.inf):
        self.layers = []
        self.softmax_layer_nonlin = SoftmaxNonlinearity(input=[], input_clean=[])
        self.Cin_Softmax = 0
        self.H_Softmax = 0
        self.W_Softmax = 0
        self.train_mode = train_mode
        self.K_Softmax = 0
        self.W_softmax_init = None
        self.b_softmax_init = None
        self.N_layer = 0
        self.denoising = None
        self.reconst_weights = []
        self.grad_min = grad_min
        self.grad_max = grad_max
        self.N_params_per_layer = 0
        self.seed = seed

        np.random.seed(self.seed)

        self.srng = RandomStreams()
        self.srng.seed(np.random.randint(2 ** 15))

        self.x = T.tensor4('x')
        self.y = T.ivector('y')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

    def Build_TopDown(self, top_down_mode=None, is_relu=False):
        '''
        Build E_step TopDown
        :return:
        '''
        print('Build Normal Reconstruction TopDown')
        self.layers[0]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[0].output,
                                                       denoising=self.denoising, top_down_mode=top_down_mode, is_relu=is_relu)

        for i in xrange(1, self.N_layer):
            self.layers[i]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[i - 1].data_reconstructed,
                                                           denoising=self.denoising, top_down_mode=top_down_mode, is_relu=is_relu)

        self.layers[self.N_layer-1].data_reconstructed = T.nnet.relu(self.layers[self.N_layer-1].data_reconstructed)

    def Build_One_Hot_Reconstruction(self, top_down_mode=None, is_relu=False):
        '''
        Build E_step Reconstruction TopDown using one hot encoding
        :return:
        '''
        print('Build One Hot Reconstruction TopDown')
        class_one_hot_encoding = T.extra_ops.to_one_hot(self.softmax_layer_nonlin.y_pred, 10)
        class_one_hot_encoding = class_one_hot_encoding.dimshuffle(0, 1, 'x', 'x')
        self.layers[0]._E_step_Top_Down_Reconstruction(mu_cg=class_one_hot_encoding,
                                                       denoising=self.denoising, top_down_mode=top_down_mode, is_relu=is_relu)
        for i in xrange(1, self.N_layer):
            self.layers[i]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[i - 1].data_reconstructed,
                                                           denoising=self.denoising, top_down_mode=top_down_mode, is_relu=is_relu)

    def ReconstructInput(self, input):
        '''
        Reconstruct the input
        :param input:
        :return:
        '''
        getReconstruction = theano.function([self.x, self.is_train, self.momentum_bn], self.layers[self.N_layer - 1].data_reconstructed, on_unused_input='warn')
        I_hat = getReconstruction(input, 0, 1.0)
        return I_hat

    def SampleImage(self, mode, Nimages=1, input=[], top_down_mode=None, is_relu=False):
        '''
        Sample images from the model
        :param Nimages:
        :return:
        '''
        class_index = self.srng.random_integers(size=(Nimages,), low=0, high=self.K_Softmax-1, ndim=None,
                        dtype='int32')
        class_one_hot_encoding = T.extra_ops.to_one_hot(class_index, 10)
        class_one_hot_encoding = class_one_hot_encoding.dimshuffle(0, 1, 'x', 'x')

        self.layers[0]._E_step_Top_Down_Reconstruction(mu_cg=class_one_hot_encoding,
                                                       denoising=self.denoising, top_down_mode=top_down_mode, is_relu=is_relu)

        for i in xrange(1, self.N_layer):
            uniform_tensor = self.srng.uniform(size=self.layers[i].latents_shape, low=0, high=1.0,
                                               ndim=None, dtype=theano.config.floatX)
            if mode == 'uniform':
                self.layers[i].mask_gen_a = T.cast(T.gt(uniform_tensor, 0.5), theano.config.floatX)
                uniform_tensor_new = uniform_tensor
            elif mode == 'proper_using_pi':
                self.layers[i].mask_gen_a =  T.cast(T.gt(uniform_tensor, 1.0 - self.layers[i].pi_a), theano.config.floatX)
                uniform_tensor_new = uniform_tensor * self.layers[i].pi_t
            elif mode == 'proper_using_final_pi':
                self.layers[i].mask_gen_a = T.cast(T.gt(uniform_tensor, 1.0 - self.layers[i].pi_a_final), theano.config.floatX)
                uniform_tensor_new = uniform_tensor * self.layers[i].pi_t_final
            else:
                print('Use t and a from the bottom up to sample')

            if mode != 'use_ta_BU':
                if self.layers[i].pool_t_mode == 'max_t':
                    self.layers[i].mask_gen_t = T.grad(T.sum(T.max(pool.pool_2d(input=uniform_tensor_new, ds=(2, 2), ignore_border=True, mode='max'), axis=1)),wrt=uniform_tensor_new)
                    self.layers[i].mask_gen = self.layers[i].mask_gen_t * self.layers[i].mask_gen_a
                else:
                    self.layers[i].mask_gen = self.layers[i].mask_gen_a

                self.layers[i]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[i - 1].data_reconstructed,
                                                               denoising=self.denoising, top_down_mode=top_down_mode, is_relu=is_relu)
            else:
                self.layers[i]._E_step_Top_Down_Reconstruction(mu_cg=self.layers[i - 1].data_reconstructed,
                                                               denoising=self.denoising, top_down_mode=top_down_mode, is_relu=is_relu)

        if mode != 'use_ta_BU':
            getSamples = theano.function([], self.layers[self.N_layer - 1].data_reconstructed,
                                         on_unused_input='warn')
            I_hat = getSamples()
        else:
            getSamples = theano.function([self.x, self.is_train, self.momentum_bn], self.layers[self.N_layer - 1].data_reconstructed, on_unused_input='warn')
            I_hat = getSamples(input, 0, 1.0)

        return I_hat

    def Build_Cost(self):
        '''
        Build the cost we minimize during training is the NLL of the model
        :return:
        '''
        if self.train_mode == 'supervised':
            print('Build cost for supervised model')
            self.cost = self.softmax_layer_nonlin.negative_log_likelihood(self.y)
        elif self.train_mode == 'unsupervised':
            print('Build cost for unsupervised model')
            self.cost = 0.0
            for i in xrange(self.N_layer):
                self.cost += self.reconst_weights[self.N_layer - 1 - i] * T.mean(
                    (self.layers[i].data_4D_clean - self.layers[i].data_reconstructed) ** 2)
        elif self.train_mode == 'semisupervised':
            print('Build cost for semisupervised model')
            self.unsupervisedNLL = 0.0
            for i in xrange(self.N_layer):
                self.unsupervisedNLL += self.reconst_weights[self.N_layer - 1 - i] * T.mean(
                    (self.layers[i].data_4D_clean - self.layers[i].data_reconstructed) ** 2)

            self.supervisedNLL = self.softmax_layer_nonlin.negative_log_likelihood(self.y)

            self.cost = self.supervisedNLL + self.unsupervisedNLL
        else:
            print('Please specify how to do TopDown and update your model in train_model_no_factor_latest.py')

    def Build_Update_Rule(self):

        # Specify update rules and outputs
        print('Build update rules')
        # create a list of all model parameters to be fit by gradient descent

        self.params = []
        for i in xrange(self.N_layer):
            self.params = self.params + self.layers[i].params

        self.params = self.params + self.RegressionInSoftmax.params

        # create a list of gradients for all model parameters
        self.grads = T.grad(self.cost, self.params)

        # train_model is a function that updates the model parameters by
        # SGD Since this model has many parameters, it would be tedious to
        # manually create an update rule for each model parameter. We thus
        # create the updates list by automatically looping over all
        # (params[i],grads[i]) pairs.

        self.updates = []
        for param_i, grad_i in zip(self.params, self.grads):
            self.updates.append((param_i, param_i - self.lr * T.clip(grad_i, self.grad_min, self.grad_max)))

        # self.positive_constraints = []
        # for layer in self.layers:
        #     self.positive_constraints.append((layer.Dg, T.nnet.relu(layer.Dg)))
            # if layer.is_bn_TD and (not layer.is_tied_bn):
            #     print('There is bn_TD which is different from bn_BU')
            #     self.positive_constraints.append((layer.bn_TD.gamma, T.nnet.relu(layer.bn_TD.gamma)))

        indx_val = 0
        for i in xrange(self.N_layer):
            self.layers[i].lambdas_new = self.layers[i].lambdas - self.lr * T.clip(self.grads[indx_val],
                                                                                   self.grad_min, self.grad_max)
            indx_val += len(self.layers[i].params)

        self.output_var = []
        self.misc = []
        self.init_var = []
        self.updates_after_train = []

        for layer in self.layers:
            # Concatenate updates
            self.updates.append((layer.amps, layer.amps_new))
            self.updates.append((layer.pi_t, layer.pi_t_new))
            self.updates.append((layer.pi_a, layer.pi_a_new))
            if layer.is_bn_BU:
                self.updates.append((layer.bn_BU.mean, layer.bn_BU.mean_new))
                self.updates.append((layer.bn_BU.var, layer.bn_BU.var_new))

            # Concatenate updates after train
            self.updates_after_train.append((layer.pi_t_final, layer.pi_t_final + layer.pi_t_minibatch))
            self.updates_after_train.append((layer.pi_a_final, layer.pi_a_final + layer.pi_a_minibatch))
            # Concatenate output_var
            self.output_var.append(layer.logLs)  # 0
            self.output_var.append(layer.betas)  # 1
            self.output_var.append(layer.lambdas_new)  # 2
            self.output_var.append(layer.amps_new)  # 3
            # Concatenate misc
            self.misc.append(layer.latents)  # 0
            self.misc.append(layer.rs)  # 1
            self.misc.append(layer.latents_masked)  # 2
            # Concatenate init_var
            self.init_var.append(layer.betas)  # 0
            self.init_var.append(layer.lambdas)  # 1
            self.init_var.append(layer.amps)  # 2