__author__ = 'tannguyen'

import os

import numpy as np
import tensorflow as tf

np.set_printoptions(threshold=np.nan)

from DRMM_trained import DRMM_9_layers_CIFAR10

from load_data import DATA


# from guppy import hpy; h=hpy()

if __name__ == '__main__':
    # load data
    data_mode = 'all'
    data_dir = './data/cifar10'
    preprocess_mode = True  # WARN: THIS SHOULD BE ALWAYS ON FOR CIFAR10
    Cin = 3  # number of channels of the inputs
    H = 32  # height of input images
    W = 32  # width of input images
    Nlabeled = 50000  # number of labeled examples used during training
    Ni = 50000  # total number of training examples
    seed = 2

    data = DATA(dataset_name='CIFAR10', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin,
                H=H, W=W, seed=seed, preprocess=preprocess_mode)

    # run interactive session
    sess = tf.InteractiveSession()

    # Set up placeholder and batch_size
    x = tf.placeholder(tf.float32, shape=[None, 3, 32, 32], name='x')
    y = tf.placeholder(tf.float32, shape=[None,], name='y')
    mode = tf.placeholder(tf.int32, name='is_train')
    momentum_bn = tf.placeholder(tf.float32, name='momentum_bn')
    batch_size = 200

    # import the model
    model = DRMM_9_layers_CIFAR10(input=x, labels=y, mode=mode, momentum_bn=momentum_bn, batch_size=batch_size)

    # initialize all variables
    sess.run(tf.initialize_all_variables())

    # predict the labels
    label_pred = model.y_pred.eval(feed_dict={x: data.dtest[0:batch_size],
                                              y: data.test_label[0:batch_size],
                                              mode: 0,
                                              momentum_bn: 1.0})

    # print the results
    print('Given labels')
    print(data.test_label[0:batch_size])

    print('Predicted labels')
    print(label_pred)

    # compute output at layer 4
    acts4 = model.output[3].eval(feed_dict={x: data.dtest[0:batch_size],
                                              y: data.test_label[0:batch_size],
                                              mode: 0,
                                              momentum_bn: 1.0})

    # print results
    print('output at layer 4')
    print(acts4[0,0,0:10,0:10])