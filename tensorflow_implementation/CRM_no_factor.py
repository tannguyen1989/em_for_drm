__author__ = 'minhtannguyen'

#######################################################################################################################
# get rid of no rendering channel
# do both soft and hard on c
#######################################################################################################################

import numpy as np

import tensorflow as tf

from nn_functions import BatchNormalization


# from lasagne.layers import InputLayer, FeatureWTALayer

class CRM(object):
    """
    EM Algorithm for the Convolutional Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes
    'data_4D: data in 4-D (N,D,H,W)

    """

    def __init__(self, data_4D, labels, K, M, W, H, w, h, Cin, Ni,
                 momentum_bn, is_train,
                 data_4D_clean=None,
                 lambdas_val_init=None, amps_val_init=None,
                 gamma_bn_init=None, beta_bn_init=None, mean_bn_init=None, var_bn_init=None,
                 PPCA=False, lock_psis=True,
                 em_mode='hard', layer_loc='intermediate',
                 pool_t_mode='max_t', border_mode='VALID', pool_a_mode='relu', nonlin='relu',
                 mean_pool_size=[1, 6, 6, 1],
                 rs_clip=0.0,
                 max_condition_number=1.e3,
                 init_ppca=False,
                 init_Bengio=False,
                 is_noisy=False,
                 is_bn_BU=False,
                 is_bn_TD=False,
                 epsilon=1e-10,
                 momentum_pi_t=0.99,
                 momentum_pi_a=0.99,
                 is_tied_bn=False,
                 is_Dg=False):

        ## required
        self.K = K  # number of clusters
        self.M = M  # latent dimensionality
        self.data_4D = data_4D
        self.data_4D_clean = data_4D_clean
        self.labels = labels
        self.Ni = Ni  # no. of images
        self.w = w  # width of filters
        self.h = h  # height of filters
        self.Cin = Cin  # number of channels in the image
        self.D = self.h * self.w * self.Cin  # patch size
        self.W = W  # width of image
        self.H = H  # height of image
        if border_mode == 'VALID':
            self.Np = (self.H - self.h + 1) * (self.W - self.w + 1)  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1)
        elif border_mode == 'SAME':
            self.Np = self.H * self.W  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H, self.W)
        elif border_mode == 'FULL':
            self.Np = (self.H + self.h - 1) * (self.W + self.w - 1)  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1)
        else:
            print('Please specify self.Np and self.latents_shape in CRM_no_factor_latest.py')

        self.N = self.Ni * self.Np  # total no. of patches and total no. of hidden units
        self.mean_pool_size = mean_pool_size

        # self.means_val_init = means_val_init
        self.lambdas_val_init = lambdas_val_init
        self.amps_val_init = amps_val_init
        self.gamma_bn_init = gamma_bn_init
        self.beta_bn_init = beta_bn_init
        self.mean_bn_init = mean_bn_init
        self.var_bn_init = var_bn_init

        # options
        self.em_mode = em_mode
        self.layer_loc = layer_loc
        self.pool_t_mode = pool_t_mode
        self.pool_a_mode = pool_a_mode
        self.nonlin = nonlin
        self.border_mode = border_mode
        self.PPCA = PPCA
        self.lock_psis = lock_psis
        self.rs_clip = rs_clip
        self.max_condition_number = max_condition_number
        self.init_Bengio = init_Bengio
        self.is_noisy = is_noisy
        self.is_bn_BU = is_bn_BU
        self.is_bn_TD = is_bn_TD
        self.momentum_bn = momentum_bn
        self.momentum_pi_t = momentum_pi_t
        self.momentum_pi_a = momentum_pi_a
        self.is_train = is_train
        self.epsilon = epsilon
        self.is_tied_bn = is_tied_bn
        self.is_Dg = is_Dg
        assert rs_clip >= 0.0

        self._initialize(init_ppca)

    def _initialize(self, init_ppca):
        #
        # initialize pi's, means, lambdas, psis, lambda_covs, covs, and inv_covs
        #

        # initialize the pi's (a.k.a the priors)
        # if initial values for pi's are not provided, randomly initialize pi's

        if self.amps_val_init == None:
            amps_val = np.random.rand(self.K)
            amps_val /= np.sum(amps_val)

        else:
            amps_val = self.amps_val_init

        self.amps = tf.Variable(amps_val, name='amps', dtype=tf.float32)

        self.pi_t = tf.Variable(tf.zeros(self.latents_shape[1:]),name='pi_t', dtype=tf.float32)

        self.pi_a = tf.Variable(tf.zeros(self.latents_shape[1:]), name='pi_a', dtype=tf.float32)

        self.pi_t_final = tf.Variable(tf.zeros(self.latents_shape[1:]), name='pi_t_final', dtype=tf.float32)

        self.pi_a_final = tf.Variable(tf.zeros(self.latents_shape[1:]), name='pi_a_final', dtype=tf.float32)

        # initialize the lambdas
        # if initial values for lambdas are not provided, randomly initialize lambdas
        if self.lambdas_val_init == None:
            if self.init_Bengio:
                print('Do init_Bengio')
                fan_in = self.D
                if self.pool_t_mode == None:
                    fan_out = self.K * self.h * self.w
                else:
                    fan_out = self.K * self.h * self.w / 4

                lambdas_bound = np.sqrt(6. / (fan_in + fan_out))
                lambdas_value = np.random.uniform(low=-lambdas_bound, high=lambdas_bound, size=(self.K, self.D, self.M))
            else:
                lambdas_value = np.random.randn(self.K, self.D, self.M) / \
                                np.sqrt(self.max_condition_number)
        else:
            lambdas_value = self.lambdas_val_init

        self.lambdas = tf.Variable(np.asarray(lambdas_value), name='lambdas', dtype=tf.float32)

        if self.is_bn_BU:
            print('do batch_normalization in Bottom Up')
            self.bn_BU = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn, is_train=self.is_train,
                                            epsilon=self.epsilon,
                                            gamma_init=self.gamma_bn_init,
                                            beta_init=self.beta_bn_init,
                                            mean_init=self.mean_bn_init,
                                            var_init=self.var_bn_init)
            self.params = [self.lambdas, self.bn_BU.gamma, self.bn_BU.beta]
        else:
            self.params = [self.lambdas, ]

        if self.is_Dg:
            print('use Dg')
            self.Dg = tf.Variable(tf.ones((self.K)), name='Dg', dtype=tf.float32)
            self.dg = tf.Variable(tf.zeros((self.K)), name='dg', dtype=tf.float32)
            self.params.append(self.Dg)
            self.params.append(self.dg)

    def get_important_latents_BU(self, input, betas):
        # compute E[z|x] using eq. 13

        if self.border_mode != 'FULL':
            latents_before_BN = tf.nn.conv2d(tf.transpose(input, [0,2,3,1]), tf.transpose(betas,[2,3,1,0]), strides=[1, 1, 1, 1], padding=self.border_mode)
            latents_before_BN = tf.transpose(latents_before_BN, [0, 3, 1, 2])
        else:
            latents_before_BN = tf.nn.conv2d_transpose(tf.transpose(input, [0,2,3,1]), tf.transpose(betas,[2,3,0,1])[::-1,::-1,:,:], strides=[1, 1, 1, 1], padding="VALID",
                                                       output_shape=[self.latents_shape[0], self.latents_shape[2],
                                                                     self.latents_shape[3], self.latents_shape[1]])
            latents_before_BN = tf.transpose(latents_before_BN, [0, 3, 1, 2])

        # do Batch normalization
        if self.is_bn_BU:
            latents = self.bn_BU.get_result(input=latents_before_BN, input_shape=self.latents_shape)
        elif self.is_Dg: # still in the beta state
            latents = tf.expand_dims(tf.expand_dims(tf.expand_dims(self.Dg, 0),2),3) * \
                      (latents_before_BN - tf.expand_dims(tf.expand_dims(tf.expand_dims(self.Dg, 0),2),3))
        else:
            latents = latents_before_BN

        # max over a
        if self.pool_a_mode == 'relu':
            print('Do max over a')
            max_over_a_mask = tf.cast(tf.greater(latents, 0.), dtype=tf.float32)
        else:
            print('No max over a')
            max_over_a_mask = tf.cast(tf.ones_like(latents), dtype=tf.float32)

        # max over t
        if self.pool_t_mode == 'max_t' and self.nonlin == 'relu':
            print('Do max over t')
            max_over_t_mask = tf.greater_equal(latents,
                                               tf.transpose(tf.image.resize_nearest_neighbor(tf.nn.max_pool(tf.transpose(latents, [0,2,3,1]), [1, 2, 2, 1], strides=[1,2,2,1], padding='VALID'),
                                                            [self.latents_shape[2], self.latents_shape[3]]),
                                                            [0,3,1,2]))
            max_over_t_mask = tf.cast(max_over_t_mask, dtype=tf.float32)
        elif self.pool_t_mode == 'max_t' and self.nonlin == 'abs': # still in the beta state
            print('Do max over t')
            latents_abs = tf.abs(latents)
            max_over_t_mask = tf.greater_equal(latents_abs,
                                               tf.transpose(tf.image.resize_nearest_neighbor(tf.nn.max_pool(tf.transpose(latents_abs, [0, 2, 3, 1]), [1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID'),
                                                   [self.latents_shape[2], self.latents_shape[3]]),
                                                            [0, 3, 1, 2]))
            max_over_t_mask = tf.cast(max_over_t_mask, dtype=tf.float32)
        else:
            print('No max over t')
            # compute latents masked by a
            max_over_t_mask = tf.cast(tf.ones_like(latents), dtype=tf.float32)

        # compute latents masked by a and t
        if self.nonlin == 'relu':
            print('Nonlinearity is ReLU')
            latents_masked = latents * max_over_t_mask * max_over_a_mask  # * max_over_a_mask
        elif self.nonlin == 'abs':
            print('Nonlinearity is abs')
            latents_masked = tf.abs(latents) * max_over_t_mask
        else:
            print('Please specify your nonlin in CRM_no_factor_latest')
            raise

        masked_mat = max_over_t_mask * max_over_a_mask  # * max_over_a_mask

        # latents_rs = self.latents[:,0,:] * self.rs
        if self.layer_loc == 'intermediate':
            output_before_pool = latents_masked
        else:
            print('Please specify your output in CRM_no_factor_latest.py')

        if self.pool_t_mode == 'max_t':
            output = tf.nn.avg_pool(tf.transpose(output_before_pool, [0,2,3,1]), [1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')
            output = output * 4.0
            output = tf.transpose(output, [0,3,1,2])
        elif self.pool_t_mode == 'mean_t':
            output = tf.nn.avg_pool(tf.transpose(output_before_pool, [0,2,3,1]), self.mean_pool_size, strides=self.mean_pool_size, padding='VALID')
            output = tf.transpose(output, [0, 3, 1, 2])
        else:
            output = output_before_pool

        return latents_before_BN, latents, max_over_a_mask, max_over_t_mask, latents_masked, masked_mat, output

    def _E_step_Bottom_Up(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """

        # Bottom-Up

        # compute the lambda dagger
        self.betas = tf.transpose(self.lambdas, [0, 2, 1])

        betas = tf.reshape(tf.squeeze(self.betas,[1]), [self.K, self.Cin, self.h, self.w])

        # Batch Normalization
        if self.is_noisy:
            print('Compute the clean path since the clean path is not the same as the noisy path')
            if self.is_bn_BU:
                self.bn_BU.set_runmode(1)

            [self.latents_before_BN, self.latents, self.max_over_a_mask, self.max_over_t_mask, self.latents_masked, self.masked_mat,
             self.output] \
                = self.get_important_latents_BU(input=self.data_4D, betas=betas)

            if self.is_bn_BU:
                self.bn_BU.set_runmode(0) # define constant name for run mode

            [self.latents_before_BN_clean, self.latents_clean, self.max_over_a_mask_clean, self.max_over_t_mask_clean, self.latents_masked_clean,
             self.masked_mat_clean, self.output_clean] \
                = self.get_important_latents_BU(input=self.data_4D_clean, betas=betas)

        else:
            print('The clean path is the same as the noisy path')
            if self.is_bn_BU:
                self.bn_BU.set_runmode(0)

            [self.latents_before_BN, self.latents, self.max_over_a_mask, self.max_over_t_mask, self.latents_masked, self.masked_mat,
             self.output] \
                = self.get_important_latents_BU(input=self.data_4D, betas=betas)
            self.output_clean = self.output

        self.pi_t_minibatch = tf.reduce_mean(self.max_over_t_mask, 0)
        self.pi_a_minibatch = tf.reduce_mean(self.max_over_a_mask, 0)

        self.pi_t_new = self.momentum_pi_t*self.pi_t + (1 - self.momentum_pi_t)*self.pi_t_minibatch
        self.pi_a_new = self.momentum_pi_a*self.pi_a + (1 - self.momentum_pi_a)*self.pi_a_minibatch

        self.rs = self.masked_mat
        self.logLs = 0.5 * tf.reduce_sum(self.masked_mat, 1)

        self.amps_new = tf.reduce_sum(self.masked_mat, [0,2,3])/float(self.N/4)
        ################################################################################################################

        # IF DO M STEP, REMEMBER TO RESHAPE SELF.LATENTS TO (K, M, N)