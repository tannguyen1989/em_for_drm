import os

from useful_models import CIFAR10_Conv_Large_9_Layers

class DRMM_9_layers_CIFAR10(object):
    '''
    This class provides objects of DRMM with 9 layers trained on CIFAR10

    :param input: input to the model (batch_size, C, W, H) = (batch_size, 3, 32, 32). Type: tf.placeholder
    :param y: input labels (batch_size,). Type: tf.placeholder
    :param mode: in {0,1}. 0=test, 1=train. Type: tf.placeholder
    :param momentum_bn: the momentum to update the mean and var in Bach Normalization. Type: tf.placeholder
    :param batch_size: # of data points sent to the network in each iteration. Type: integer

    :output
    output: a list of tensors of the activations from the layers of the trained DRMM. Here there are totally
    9 layers + a Softmax Regression. output[0] is the output of layer 1, output[8] is the output of layer 9, output[9]
    is the output of the Softmax Regression.
    y_pred: predicted labels
    y_given_x: posteriors
    '''
    def __init__(self, input, labels, mode, momentum_bn, batch_size):
        self.input = input
        self.labels = labels
        self.mode = mode
        self.momentum_bn = momentum_bn
        self.batch_size = batch_size

        root_dir = './research_results/EM_results'
        training_name = 'CIFAR10_Conv_Large_9_Layers_nofactor_semisupervised_Ni50000_Nlabel4000_b100_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_520161026_173850'
        model_file_name = 'model_best.zip'
        self.param_dir = os.path.join(root_dir, training_name, 'Train/params', model_file_name)

        self.model = CIFAR10_Conv_Large_9_Layers(input=self.input, labels=self.labels, is_train=self.mode,
                                                 momentum_bn=self.momentum_bn, batch_size=self.batch_size, Cin=3, W=32, H=32,
                                                 em_mode='hard', seed=2, param_dir=self.param_dir, train_mode='supervised',
                                                 reconst_weights=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                 noise_weights=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                 is_bn_BU=True, init_params=True, nonlin='relu')
        # set layers
        self.layers = []
        for i in range(self.model.N_layer):
            self.layers.append(self.model.layers[self.model.N_layer - i - 1])

        # set output
        self.output = []

        for i in range(self.model.N_layer):
            self.output.append(self.model.layers[self.model.N_layer - i - 1].output)

        self.output.append(self.model.RegressionInSoftmax.output)

        # set the predicted labels and the estimated posteriors
        self.y_pred = self.model.softmax_layer_nonlin.y_pred
        self.y_give_x = self.model.softmax_layer_nonlin.gammas

        # set parameters to update during training
        self.params = []
        for i in range(self.model.N_layer):
            self.params = self.params + self.model.layers[self.model.N_layer - i - 1].params

        self.params = self.params + self.model.RegressionInSoftmax.params




