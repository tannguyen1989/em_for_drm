import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import gzip
import cPickle

import numpy as np

import random

import time

import theano

import copy
from old_codes.kmeans_init import kmeans_init

from PIL import Image

import os

def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

if __name__ == '__main__':
    data_dir = '/Users/heatherseeba/repos/em_drm/data/cifar10/'
    output_dir = './'
    file_name = 'cifar_img_in_my_computer'
    train_set_1 = unpickle(data_dir + "/data_batch_1")
    train_set_2 = unpickle(data_dir + "/data_batch_2")
    train_set_3 = unpickle(data_dir + "/data_batch_3")
    train_set_4 = unpickle(data_dir + "/data_batch_4")
    train_set_5 = unpickle(data_dir + "/data_batch_5")
    test_set = unpickle(data_dir + "/test_batch")
    train_set = train_set_1
    train_set['data'] = np.concatenate(
        (train_set['data'], train_set_2['data'], train_set_3['data'], train_set_4['data'], train_set_5['data']), axis=0)
    train_set['labels'] = np.concatenate((train_set['labels'], train_set_2['labels'], train_set_3['labels'],
                                          train_set_4['labels'], train_set_5['labels']), axis=0)

    dtrain = train_set['data']
    train_label = train_set['labels']
    dtest = test_set['data']
    test_label = test_set['labels']
    dvalid = train_set_5['data']
    valid_label = train_set_5['labels']

    dtrain = np.reshape(dtrain, newshape=(50000, 3, 32, 32))
    imgs = dtrain[0:36]
    print(np.shape(imgs))

    imgs = imgs.transpose((0, 2, 3, 1))
    print(np.min(imgs))
    print(np.max(imgs))
    print(np.mean(imgs))
    N = imgs.shape[0]
    for i in xrange(N):
        if imgs.shape[3] == 1:
            imshow(imgs[i, :, :, 0], cmap='gray')
        else:
            imshow(imgs[i])
        axis('off')
        out_path = os.path.join(output_dir, 'single_images')
        if not os.path.exists(out_path):
            os.makedirs(out_path)

        out_file = os.path.join(out_path, '%s_%i.jpg' % (file_name, i))
        savefig(out_file)
        close()
    sub_x_size = int(sqrt(N))
    sub_y_size = int(sqrt(N))
    img = Image.new('RGB', (800 * sub_x_size, 600 * sub_y_size))
    for i in xrange(N):
        this_img = Image.open(os.path.join(out_path, '%s_%i.jpg' % (file_name, i)))
        x_loc = (i % sub_x_size) * 800
        y_loc = (i / sub_y_size) * 600
        img.paste(this_img, (x_loc, y_loc))
    fig = imshow(img)
    axis('off')
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    savefig(os.path.join(output_dir, '%ss.pdf' % file_name))
    savefig(os.path.join(output_dir, '%ss.jpg' % file_name))

    #############################
    data_dir = '/Users/heatherseeba/repos/em_drm/data/cifar10'
    output_dir = './'
    file_name = 'cifar_zca_img'
    train_set = unpickle(os.path.join(data_dir, "pylearn2_gcn_whitened", "train.pkl"))
    test_set = unpickle(os.path.join(data_dir, "pylearn2_gcn_whitened", "test.npy"))
    # train_set = np.load(os.path.join(data_dir, "pylearn2_gcn_whitened", "train.npy"))
    # test_set = np.load(os.path.join(data_dir, "pylearn2_gcn_whitened", "test.npy"))

    train_set_1_temp = unpickle(data_dir + "/data_batch_1")
    train_set_2_temp = unpickle(data_dir + "/data_batch_2")
    train_set_3_temp = unpickle(data_dir + "/data_batch_3")
    train_set_4_temp = unpickle(data_dir + "/data_batch_4")
    train_set_5_temp = unpickle(data_dir + "/data_batch_5")
    test_set_temp = unpickle(data_dir + "/test_batch")
    train_set_labels = np.concatenate((train_set_1_temp['labels'], train_set_2_temp['labels'],
                                       train_set_3_temp['labels'], train_set_4_temp['labels'],
                                       train_set_5_temp['labels']), axis=0)
    test_set_labels = test_set_temp['labels']

    dtrain = train_set
    train_label = train_set_labels
    dtest = test_set
    test_label = test_set_labels
    dvalid = train_set[40000:50000]
    valid_label = train_set_labels[40000:50000]

    dtrain = np.reshape(dtrain, newshape=(50000, 3, 32, 32))
    imgs = dtrain[0:36]
    print(np.shape(imgs))

    imgs = imgs.transpose((0, 2, 3, 1))

    N = imgs.shape[0]
    print(np.min(imgs))
    print(np.max(imgs))
    print(np.mean(imgs))
    for i in xrange(N):
        if imgs.shape[3] == 1:
            imshow(imgs[i, :, :, 0], cmap='gray')
        else:
            imshow(imgs[i])
        axis('off')
        out_path = os.path.join(output_dir, 'single_images')
        if not os.path.exists(out_path):
            os.makedirs(out_path)

        out_file = os.path.join(out_path, '%s_%i.jpg' % (file_name, i))
        savefig(out_file)
        close()
    sub_x_size = int(sqrt(N))
    sub_y_size = int(sqrt(N))
    img = Image.new('RGB', (800 * sub_x_size, 600 * sub_y_size))
    for i in xrange(N):
        this_img = Image.open(os.path.join(out_path, '%s_%i.jpg' % (file_name, i)))
        x_loc = (i % sub_x_size) * 800
        y_loc = (i / sub_y_size) * 600
        img.paste(this_img, (x_loc, y_loc))
    fig = imshow(img)
    axis('off')
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    savefig(os.path.join(output_dir, '%ss.pdf' % file_name))
    savefig(os.path.join(output_dir, '%ss.jpg' % file_name))