__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')

import os
import sys
import datetime
from useful_models_latest import *

from load_data_latest import DATA
from load_data_for_60K_semi_sup_latest import DATA_60K
from train_no_factor_latest import TrainNoFactorDRM
from theano.misc.pkl_utils import load
import numpy as np

if __name__ == '__main__':
    model_name = 'MNIST_Conv_Small_5_Layers' # specify the model to use
    train_mode = 'semisupervised' # choose the train mode: semisupervised, supervised, or unsupervised

    init_params = False # use specific param values to initialize the model
    debug_mode = False # if True, run the code in debug_mode
    num_epoch_to_plot_NLL = 20 # plot negative log-likelihood at each epoch
    init_Bengio = True # use Xavier initialization

    is_bn_BU = True # use BatchNorm in the Bottom Up (BU)
    is_bn_TD = False # use BatchNorm in the Top Down (TD)
    is_tied_bn = False # tie the params of BatchNorm in BU and TD
    denoising = 'simple' # choose the denoising mode: simple, linear
    top_down_mode = 'normal'
    is_relu = True
    is_reluI = True
    nonlin = 'relu' # choose the non-linearity for the BU: relu, abs, tanh,....
    is_Dg = False # alternative for BatchNorm derived from the DRM
    is_end_to_end = False # include Softmax Regression in TopDown
    train_method = 'SGD'

    seed = 2 # set random seed
    Nlabeled = 1000 # number of labeled examples used during training
    Ni = 50000 # total number of training examples

    KL_coef = 1.0
    sign_cost_weight = 0.0
    lr_init = 0.2 # initial learning rate
    lr_final = 0.002 # final learning rate
    batch_size = 500 # number of examples sent to the model in each iteration
    max_epochs = 500 # TODO: warning - max_epoch is used for update the learning rate at each epoch and to stop the training
                     # We need have separate parameters for each of them.

    noise_std = 0.6 # the std of noise added to each layer during training

    grad_min = -np.inf #grad_min and grad_max used to clip gradients
    grad_max = np.inf

    name_folder = 'Train'

    if model_name.upper().startswith('MNIST'):
        print(model_name)
        preprocess_mode = False # not preprocess data
        Cin = 1  # number of channels of the input
        H = 28  # height of input images
        W = 28  # width of input images
        reconst_weights = [0.2, 0.0, 0.0, 0.0, 0.0]  # set reconstruction weights
        noise_weights = [0.0, 0.0, 0.0, 0.0, 0.0] # set weights for noise at each layer
    elif model_name.upper().startswith('CIFAR10'):
        print(model_name)
        preprocess_mode = True # WARN: THIS SHOULD BE ALWAYS ON FOR CIFAR10
        Cin = 3  # number of channels of the inputs
        H = 32  # height of input images
        W = 32  # width of input images
        reconst_weights = [0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # set reconstruction weights
        noise_weights = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0] # set weights for noise at each layer
    else:
        print('Please specify your model')
        raise

    #############################
    # No touch #
    em_mode = 'hard'
    stop_mode = 'NLL'
    moment_coeff = 0.0
    tol = 1e-10
    verbose = True
    lr_decay = 0.01
    epoch_to_reduce_lr = (100, 200)
    #############################

    timestamp = datetime.datetime.now
    result_folder_name = 'MNIST_Conv_Small_5_Layers_nofactor_semi_sup_Ni60000_Nlabel1000_b500_lr_init_0_2_lr_final_0_002_maxepoch_500_nonlin_relu_normal_relu_TD_reluI_rec_0_2_KL_1_0{:%Y%m%d_%H%M%S}'.format(datetime.datetime.now())

    output_dir = os.path.join('/home/ubuntu/research_results/EM_results', result_folder_name)

    # Only for Finetuning
    root_dir = '/home/ubuntu/research_results/EM_results'
    training_name = 'MNIST_Conv_Small_5_Layers_nofactor_unsupervised_Ni60000_Nlabel60000_b500_lr_init_2_lr_final_0_0001_maxepoch_500_bnBU_HiddenLaye_nonlin_relu_reg_0_220161027_073035'
    model_file_name = 'model_latest.zip'
    param_dir = os.path.join(root_dir, training_name, 'Train/params', model_file_name)

    # output_dir = '/Users/heatherseeba/Documents/research_results/EM_results'
    # data_dir = '/Users/heatherseeba/repos/em_drm/data/cifar10'

    # Start running
    if model_name == 'CIFAR10_Conv_Large_9_Layers' or model_name == 'CIFAR10_Conv_Large_6_Layers' or model_name == 'CIFAR10_Conv_3_Layers' or model_name == 'CIFAR10_Conv_2_Layers':
        data_dir = '/home/ubuntu/repos/em_for_drm_from_old_git/data/cifar10'

        if (train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP'
            or train_mode == 'semisupervised_MSMP_reconst_layer' or train_mode == 'semisupervised_MSMP_Tau'
            or train_mode == 'semisupervised_MSMP_MultiSup' or train_mode == 'semisupervised_MultiSup') \
                and Nlabeled != 50000:
            print('data_mode: semisupervised')
            data_mode = 'semisupervised'
        else:
            print('data_mode: all')
            data_mode = 'all'

        cifar10 = DATA(dataset_name='CIFAR10', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin,
                     H=H, W=W, seed=seed, preprocess=preprocess_mode)

        if model_name == 'CIFAR10_Conv_Large_9_Layers':
            cifar10_model = CIFAR10_Conv_Large_9_Layers(batch_size=batch_size, Cin=Cin, W=W, H=H,
                                                        em_mode=em_mode, seed=seed, param_dir=param_dir,
                                                        train_mode=train_mode,
                                                        reconst_weights=reconst_weights, init_Bengio=init_Bengio, grad_min=grad_min,
                                                        grad_max=grad_max,
                                                        init_params=init_params, denoising=denoising, noise_std=noise_std, noise_weights=noise_weights,
                                                        is_bn_BU=is_bn_BU, is_bn_TD=is_bn_TD, is_tied_bn=is_tied_bn,
                                                        top_down_mode=top_down_mode, is_relu=is_relu, nonlin=nonlin, is_Dg=is_Dg,
                                                        method=train_method, KL_coef=KL_coef)
        else:
            print('Please build your model in train_model_no_factor_latest.py')

        train_cifar10_model = TrainNoFactorDRM(batch_size=batch_size, train_data=cifar10.dtrain, test_data=cifar10.dtest,
                                               valid_data=cifar10.dvalid,
                                               train_label=cifar10.train_label, test_label=cifar10.test_label,
                                               valid_label=cifar10.valid_label,
                                               model=cifar10_model,
                                               output_dir=os.path.join(output_dir, name_folder))

        if train_mode == 'supervised':
            train_cifar10_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                 lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                                 num_epoch_to_plot_NLL=num_epoch_to_plot_NLL, debug_mode=debug_mode)
        elif train_mode == 'unsupervised' or train_mode == 'unsupervised_MSMP':
            train_cifar10_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                   lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                                   debug_mode=debug_mode)
        elif train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP' or train_mode == 'semisupervised_MSMP_reconst_layer'\
                or train_mode == 'semisupervised_MSMP_Tau' or train_mode == 'semisupervised_MSMP_MultiSup' \
                or train_mode == 'semisupervised_MultiSup':
            train_cifar10_model.train_semisupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                     lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                                     num_epoch_to_plot_NLL=num_epoch_to_plot_NLL, debug_mode=debug_mode)
        else:
            train_cifar10_model.output_dir=os.path.join(output_dir, 'Finetune_end_to_end_using_50K_labeled_data')
            if not os.path.exists(train_cifar10_model.output_dir):
                os.makedirs(train_cifar10_model.output_dir)

            train_cifar10_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                 lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                                 num_epoch_to_plot_NLL=num_epoch_to_plot_NLL, debug_mode=debug_mode)

    elif model_name == 'MNIST_Conv_Small_5_Layers':
        data_dir = '/home/ubuntu/repos/em_for_drm_from_old_git/data/mnist.pkl.gz'

        # set the data_mode: all, semisupervised, small
        if (train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP'
            or train_mode == 'semisupervised_MSMP_reconst_layer' or train_mode == 'semisupervised_MSMP_Tau'
            or train_mode == 'semisupervised_MSMP_MultiSup' or train_mode == 'semisupervised_MultiSup') \
                and Nlabeled != 50000:
            print('data_mode: semisupervised')
            data_mode = 'semisupervised'
        else:
            print('data_mode: all')
            data_mode = 'all'

        # load data
        mnist = DATA_60K(dataset_name='MNIST', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin,
                     H=H, W=W, seed=seed, preprocess=preprocess_mode)

        # load models
        if model_name == 'MNIST_Conv_Small_5_Layers':
            mnist_model = MNIST_Conv_Small_5_Layers(batch_size=batch_size, Cin=Cin, W=W, H=H,
                                                    em_mode=em_mode, seed=seed, param_dir=param_dir,
                                                    train_mode=train_mode,
                                                    reconst_weights=reconst_weights, init_Bengio=init_Bengio, grad_min=grad_min,
                                                    grad_max=grad_max,
                                                    init_params=init_params, denoising=denoising, noise_std=noise_std, noise_weights=noise_weights,
                                                    is_bn_BU=is_bn_BU, is_bn_TD=is_bn_TD, is_tied_bn=is_tied_bn,
                                                    top_down_mode=top_down_mode, is_relu=is_relu, nonlin=nonlin,
                                                    is_Dg=is_Dg, method=train_method, KL_coef=KL_coef,
                                                    is_end_to_end=is_end_to_end, sign_cost_weight=sign_cost_weight,
                                                    is_reluI=is_reluI)
        else:
            print('Please build your model in train_model_no_factor_latest.py')
            raise

        # initiate the training object
        train_mnist_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest,
                                             valid_data=mnist.dvalid,
                                             train_label=mnist.train_label, test_label=mnist.test_label,
                                             valid_label=mnist.valid_label,
                                             model=mnist_model,
                                             output_dir=os.path.join(output_dir, name_folder))

        # run training
        if train_mode == 'supervised':
            train_mnist_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                               lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                               num_epoch_to_plot_NLL=num_epoch_to_plot_NLL, debug_mode=debug_mode)
        elif train_mode == 'unsupervised' or train_mode == 'unsupervised_MSMP':
            train_mnist_model.train_unsupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                   lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                                   debug_mode=debug_mode)
        elif train_mode == 'semisupervised' or train_mode == 'semisupervised_MSMP' or train_mode == 'semisupervised_MSMP_reconst_layer'\
                or train_mode == 'semisupervised_MSMP_Tau' or train_mode == 'semisupervised_MSMP_MultiSup' \
                or train_mode == 'semisupervised_MultiSup':
            train_mnist_model.train_semisupervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                                   lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr, lr_decay=lr_decay, lr_final=lr_final,
                                                   num_epoch_to_plot_NLL=num_epoch_to_plot_NLL, debug_mode=debug_mode)
        else:
            print('Finetune the model')
            root_dir = '/home/ubuntu/research_results/EM_results'
            training_name = 'MNIST_Conv_Small_5_Layers_nofactor_unsupervised_Ni60000_Nlabel60000_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_bnBU_HiddenLaye_nonlin_relu_reg_0_220161025_000627'
            model_file_name = 'model_latest.zip'
            model_dir = os.path.join(root_dir, training_name, 'Train/params', model_file_name)
            name_folder = 'Finetune'

            with open(model_dir, 'rb') as f:
                trained_model = load(f)

            train_trained_model = TrainNoFactorDRM(batch_size=batch_size, train_data=mnist.dtrain, test_data=mnist.dtest,
                                                 valid_data=mnist.dvalid,
                                                 train_label=mnist.train_label, test_label=mnist.test_label,
                                                 valid_label=mnist.valid_label,
                                                 model=trained_model,
                                                 output_dir=os.path.join(output_dir, name_folder))

            train_trained_model.train_supervised(max_epochs=max_epochs, verbose=verbose, tol=tol, stop_mode=stop_mode,
                                               lr_init=lr_init, epoch_to_reduce_lr=epoch_to_reduce_lr,
                                               lr_decay=lr_decay, lr_final=lr_final,
                                               num_epoch_to_plot_NLL=num_epoch_to_plot_NLL, debug_mode=debug_mode)

    # except KeyboardInterrupt:
    #     print('Training stopped manually by user')
    #
    # finally:
    #     print('Plot reconstruction and send the results to s3')
    #     try:
    #         import analyze_DRM_latest
    #
    #         if model_name.upper().startswith('MNIST'):
    #             analyze_DRM_latest.runProbe(model_file_name='model_best.zip', training_name=result_folder_name, data_mode='MNIST')
    #             analyze_DRM_latest.runProbe(model_file_name='model_latest.zip', training_name=result_folder_name, data_mode='MNIST')
    #         elif model_name.upper().startswith('CIFAR10'):
    #             analyze_DRM_latest.runProbe(model_file_name='model_best.zip', training_name=result_folder_name,
    #                                         data_mode='CIFAR10')
    #             analyze_DRM_latest.runProbe(model_file_name='model_latest.zip', training_name=result_folder_name,
    #                                         data_mode='CIFAR10')
    #         else:
    #             print('Please specify the analyze code for your model in analyze_DRM_latest.py')
    #     except:
    #         print('ERROR: analysis fails - fix analyze_DRM_latest')
    #         print('Unexpected error:', sys.exc_info()[0])
    #         raise
    #     finally:
    #         os.system('aws s3 sync /home/ubuntu/research_results/EM_results s3://drm-em-results --region us-west-2 > s3sync.log')



