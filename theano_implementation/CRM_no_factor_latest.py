__author__ = 'minhtannguyen'

#######################################################################################################################
# get rid of no rendering channel
# do both soft and hard on c
#######################################################################################################################


import matplotlib as mpl

mpl.use('Agg')

import numpy as np

np.set_printoptions(threshold='nan')

from theano.tensor.nnet import conv2d, sigmoid
from theano.tensor.signal import pool

# from old_codes.swmfa import *
import numpy
import theano
import theano.tensor as T
from theano.tensor.nlinalg import MatrixPinv

from nn_functions_latest import BatchNormalization

import time


# from lasagne.layers import InputLayer, FeatureWTALayer

class CRM(object):
    """
    EM Algorithm for the Convolutional Mixture of Factor Analyzers.

    All of the equation numbers in this code follows those in "The EM Algorithm for Mixtures of Factor Analyzers" by
    Zoubin Ghahramani and Geoffrey E. Hinton

    calling arguments:

    [TBA]

    internal variables:

    `K`:           Number of components
    `M`:           Latent dimensionality
    `D`:           Data dimensionality
    `N`:           Number of data points
    `data`:        (N,D) array of observations
    `latents`:     (K,M,N) array of latent variables
    `latent_covs`: (K,M,M,N) array of latent covariances
    `lambdas`:     (K,M,D) array of loadings
    `psis`:        (K,D) array of diagonal variance values
    `rs`:          (K,N) array of responsibilities
    `amps`:        (K) array of component amplitudes
    'data_4D: data in 4-D (N,D,H,W)

    """

    def __init__(self, data_4D, labels, K, M, W, H, w, h, Cin, Ni,
                 momentum_bn, is_train,
                 data_4D_clean=None,
                 lambdas_val_init=None, amps_val_init=None,
                 gamma_bn_init=None, beta_bn_init=None, mean_bn_init=None, var_bn_init=None,
                 PPCA=False, lock_psis=True,
                 em_mode='hard', layer_loc='intermediate',
                 pool_t_mode='max_t', border_mode='valid', pool_a_mode='relu', nonlin='relu',
                 mean_pool_size=(2, 2),
                 rs_clip=0.0,
                 max_condition_number=1.e3,
                 init_ppca=False,
                 init_Bengio=False,
                 is_noisy=False,
                 is_bn_BU=False,
                 is_bn_TD=False,
                 epsilon=1e-10,
                 momentum_pi_t=0.99,
                 momentum_pi_a=0.99,
                 is_tied_bn=False,
                 is_Dg=False):

        ## required
        self.K = K  # number of clusters
        self.M = M  # latent dimensionality
        self.data_4D = data_4D
        self.data_4D_clean = data_4D_clean
        self.labels = labels
        self.Ni = Ni  # no. of images
        self.w = w  # width of filters
        self.h = h  # height of filters
        self.Cin = Cin  # number of channels in the image
        self.D = self.h * self.w * self.Cin  # patch size
        self.W = W  # width of image
        self.H = H  # height of image
        if border_mode == 'valid':
            self.Np = (self.H - self.h + 1) * (self.W - self.w + 1)  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1)
        elif border_mode == 'half':
            self.Np = self.H * self.W  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H, self.W)
        elif border_mode == 'full':
            self.Np = (self.H + self.h - 1) * (self.W + self.w - 1)  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1)
        else:
            print('Please specify self.Np and self.latents_shape in CRM_no_factor_latest.py')

        self.N = self.Ni * self.Np  # total no. of patches and total no. of hidden units
        self.mean_pool_size = mean_pool_size

        # self.means_val_init = means_val_init
        self.lambdas_val_init = lambdas_val_init
        self.amps_val_init = amps_val_init
        self.gamma_bn_init = gamma_bn_init
        self.beta_bn_init = beta_bn_init
        self.mean_bn_init = mean_bn_init
        self.var_bn_init = var_bn_init

        # options
        self.em_mode = em_mode
        self.layer_loc = layer_loc
        self.pool_t_mode = pool_t_mode
        self.pool_a_mode = pool_a_mode
        self.nonlin = nonlin
        self.border_mode = border_mode
        self.PPCA = PPCA
        self.lock_psis = lock_psis
        self.rs_clip = rs_clip
        self.max_condition_number = max_condition_number
        self.init_Bengio = init_Bengio
        self.is_noisy = is_noisy
        self.is_bn_BU = is_bn_BU
        self.is_bn_TD = is_bn_TD
        self.momentum_bn = momentum_bn
        self.momentum_pi_t = momentum_pi_t
        self.momentum_pi_a = momentum_pi_a
        self.is_train = is_train
        self.epsilon = epsilon
        self.is_tied_bn = is_tied_bn
        self.is_Dg = is_Dg
        assert rs_clip >= 0.0

        self._initialize(init_ppca)

    def _initialize(self, init_ppca):
        #
        # initialize pi's, means, lambdas, psis, lambda_covs, covs, and inv_covs
        #

        # initialize the pi's (a.k.a the priors)
        # if initial values for pi's are not provided, randomly initialize pi's

        if self.amps_val_init == None:
            amps_val = np.random.rand(self.K)
            amps_val /= np.sum(amps_val)

        else:
            amps_val = self.amps_val_init

        #amps is class prior  e.g. cat, dog probabilities
        self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                  name='amps', borrow=True)

        self.pi_t = theano.shared(value=numpy.zeros(self.latents_shape[1:],
                                                     dtype=theano.config.floatX),name='pi_t', borrow=True)

        self.pi_a = theano.shared(value=numpy.zeros(self.latents_shape[1:],
                                                    dtype=theano.config.floatX), name='pi_a', borrow=True)

        self.pi_t_final = theano.shared(value=numpy.zeros(self.latents_shape[1:],
                                                    dtype=theano.config.floatX), name='pi_t_final', borrow=True)

        self.pi_a_final = theano.shared(value=numpy.zeros(self.latents_shape[1:],
                                                    dtype=theano.config.floatX), name='pi_a_final', borrow=True)

        # initialize the lambdas
        # if initial values for lambdas are not provided, randomly initialize lambdas
        if self.lambdas_val_init == None:
            if self.init_Bengio:
                print('Do init_Bengio')
                fan_in = self.D
                if self.pool_t_mode == None:
                    fan_out = self.K * self.h * self.w
                else:
                    fan_out = self.K * self.h * self.w / 4

                lambdas_bound = np.sqrt(6. / (fan_in + fan_out))
                lambdas_value = np.random.uniform(low=-lambdas_bound, high=lambdas_bound, size=(self.K, self.D, self.M))
            else:
                lambdas_value = np.random.randn(self.K, self.D, self.M) / \
                                np.sqrt(self.max_condition_number)
        else:
            lambdas_value = self.lambdas_val_init

        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas',
                                     borrow=True)

        if self.is_bn_BU:
            print('do batch_normalization in Bottom Up')
            self.bn_BU = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn, is_train=self.is_train,
                                            epsilon=self.epsilon,
                                            gamma_init=self.gamma_bn_init,
                                            beta_init=self.beta_bn_init,
                                            mean_init=self.mean_bn_init,
                                            var_init=self.var_bn_init
                                            )
            self.params = [self.lambdas, self.bn_BU.gamma, self.bn_BU.beta]
        else:
            self.params = [self.lambdas, ]

        if self.is_Dg:
            print('use Dg')
            self.Dg = theano.shared(np.ones((self.K), dtype=theano.config.floatX), name='Dg', borrow=True)
            self.dg = theano.shared(np.zeros((self.K), dtype=theano.config.floatX), name='dg', borrow=True)
            self.params.append(self.Dg)
            self.params.append(self.dg)

    def take_EM_step(self):
        """
        Do one E step and then do one M step.
        """
        self._E_step_Bottom_Up()
        self._M_step()

    def get_important_latents_BU(self, input, betas):
        # compute E[z|x] using eq. 13
        latents_before_BN = conv2d(
            input=input,
            filters=betas,
            filter_shape=(self.K, self.Cin, self.h, self.w),
            image_shape=(self.Ni, self.Cin, self.H, self.W),
            filter_flip=False,
            border_mode=self.border_mode
        )

        # do Batch normalization
        if self.is_bn_BU:
            latents = self.bn_BU.get_result(input=latents_before_BN, input_shape=self.latents_shape)
        elif self.is_Dg: # still in the beta state
            latents = self.Dg.dimshuffle('x',0,'x','x') * (latents_before_BN - self.dg.dimshuffle('x',0,'x','x'))
        else:
            latents = latents_before_BN

        # max over a
        if self.pool_a_mode == 'relu':
            print('Do max over a')
            max_over_a_mask = T.cast(T.gt(latents, 0.), theano.config.floatX)
        else:
            print('No max over a')
            max_over_a_mask = T.ones_like(latents)

        # max over t
        if self.pool_t_mode == 'max_t' and self.nonlin == 'relu':
            print('Do max over t')
            max_over_t_mask = T.grad(
                T.sum(pool.pool_2d(input=latents, ds=(2, 2), ignore_border=True, mode='max')),
                wrt=latents)  # argmax across t
            max_over_t_mask = T.cast(max_over_t_mask, theano.config.floatX)
        elif self.pool_t_mode == 'max_t' and self.nonlin == 'abs': # still in the beta state
            print('Do max over t')
            latents_abs = T.abs_(latents)
            max_over_t_mask = T.grad(
                T.sum(pool.pool_2d(input=latents_abs, ds=(2, 2), ignore_border=True, mode='max')),
                wrt=latents_abs)  # argmax across t
            max_over_t_mask = T.cast(max_over_t_mask, theano.config.floatX)
        else:
            print('No max over t')
            # compute latents masked by a
            max_over_t_mask = T.ones_like(latents)

        # compute latents masked by a and t
        if self.nonlin == 'relu':
            print('Nonlinearity is ReLU')
            latents_masked = latents * max_over_t_mask * max_over_a_mask
            masked_mat = max_over_t_mask * max_over_a_mask
        elif self.nonlin == 'abs':
            print('Nonlinearity is abs')
            latents_masked = latents * max_over_t_mask
            masked_mat = max_over_t_mask
        else:
            print('Please specify your nonlin in CRM_no_factor_latest')
            raise

        # latents_rs = self.latents[:,0,:] * self.rs
        if self.layer_loc == 'intermediate':
            output_before_pool = latents_masked
        else:
            print('Please specify your output in CRM_no_factor_latest.py')

        if self.pool_t_mode == 'max_t':
            output = pool.pool_2d(input=output_before_pool, ds=(2, 2),
                                  ignore_border=True, mode='average_exc_pad')
            output = output * 4.0
        elif self.pool_t_mode == 'mean_t':
            output = pool.pool_2d(input=output_before_pool, ds=self.mean_pool_size,
                                  ignore_border=True, mode='average_exc_pad')
        else:
            output = output_before_pool

        return latents_before_BN, latents, max_over_a_mask, max_over_t_mask, latents_masked, masked_mat, output


    def _E_step_Bottom_Up(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """

        # Bottom-Up

        # compute the lambda dagger
        self.betas = self.lambdas.dimshuffle(0, 2, 1)

        betas = T.reshape(self.betas[:, 0, :], newshape=(self.K, self.Cin, self.h, self.w))

        # Batch Normalization
        if self.is_noisy:
            print('Compute the clean path since the clean path is not the same as the noisy path')
            if self.is_bn_BU:
                self.bn_BU.set_runmode(1)

            [self.latents_before_BN, self.latents, self.max_over_a_mask, self.max_over_t_mask, self.latents_masked, self.masked_mat,
             self.output] \
                = self.get_important_latents_BU(input=self.data_4D, betas=betas)

            if self.is_bn_BU:
                self.bn_BU.set_runmode(0) # define constant name for run mode

            [self.latents_before_BN_clean, self.latents_clean, self.max_over_a_mask_clean, self.max_over_t_mask_clean, self.latents_masked_clean,
             self.masked_mat_clean, self.output_clean] \
                = self.get_important_latents_BU(input=self.data_4D_clean, betas=betas)

        else:
            print('The clean path is the same as the noisy path')
            if self.is_bn_BU:
                self.bn_BU.set_runmode(0)

            [self.latents_before_BN, self.latents, self.max_over_a_mask, self.max_over_t_mask, self.latents_masked, self.masked_mat,
             self.output] \
                = self.get_important_latents_BU(input=self.data_4D, betas=betas)
            self.output_clean = self.output

        self.pi_t_minibatch = T.mean(self.max_over_t_mask, axis=0)
        self.pi_a_minibatch = T.mean(self.max_over_a_mask, axis=0)

        self.pi_t_new = self.momentum_pi_t*self.pi_t + (1 - self.momentum_pi_t)*self.pi_t_minibatch
        self.pi_a_new = self.momentum_pi_a*self.pi_a + (1 - self.momentum_pi_a)*self.pi_a_minibatch

        ################################################################################################################
        # WE ARE CURRENTLY NOT USING THIS PART OF CODE NOW BUT SOME OF THE VARS ARE NEEDED FOR OTHER CODE TO RUN WITHOUT ERROR
        # WE WILL CLEAN UP THIS PART OF CODE LATER
        # compute the responsibilities
        # soft over c
        self.rs = self.masked_mat
        self.logLs = 0.5 * T.sum(self.masked_mat, axis=(1,))
        self.amps_new = T.sum(self.masked_mat, axis=(0,2,3))/float(self.N/4)
        ################################################################################################################

        # IF DO M STEP, REMEMBER TO RESHAPE SELF.LATENTS TO (K, M, N)

    def _E_step_Top_Down_Reconstruction(self, mu_cg, denoising='simple', top_down_mode=None, is_relu=False):
        # Top-Down.
        # input is of size Ni x K x (H-h+1)/2 x (W-w+1)/2
        #

        if self.pool_t_mode == 'max_t':
            print('Pool_Mode is %s' % self.pool_t_mode)
            self.latents_unpooled_before_BN = mu_cg.repeat(2, axis=2).repeat(2, axis=3)
        elif self.pool_t_mode == 'mean_t':
            print('Pool_Mode is %s' % self.pool_t_mode)
            self.latents_unpooled_before_BN = mu_cg.repeat(self.mean_pool_size[0], axis=2).repeat(self.mean_pool_size[1], axis=3)
        elif self.pool_t_mode == None:
            print('Pool_Mode is None')
            self.latents_unpooled_before_BN = mu_cg
        else:
            print('Please specify your TopDown mode in CRM_no_factor_latest.py')
            raise

        if self.is_bn_TD:
            print('do batch_normalization in TopDown')
            if not self.is_tied_bn:
                self.bn_TD = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn,
                                                is_train=self.is_train,
                                                epsilon=self.epsilon)
                self.bn_TD.set_runmode(1)
                self.params.append(self.bn_TD.gamma)
                self.params.append(self.bn_TD.beta)
                self.latents_unpooled_after_BN = self.bn_TD.get_result(input=self.latents_unpooled_before_BN, input_shape=self.latents_shape)
            else:
                print('Tie BN_BU and BN_TD')
                self.bn_BU.set_runmode(1)
                self.latents_unpooled_after_BN = self.bn_BU.get_result(input=self.latents_unpooled_before_BN, input_shape=self.latents_shape)
        elif self.is_Dg:
            self.latents_unpooled_after_BN = T.inv(self.Dg).dimshuffle('x',0,'x','x') * self.latents_unpooled_before_BN + self.dg.dimshuffle('x',0,'x','x')
        else:
            self.latents_unpooled_after_BN = self.latents_unpooled_before_BN

        if is_relu:
            print('Apply ReLU on the reconstructed images')
            self.latents_unpooled_no_mask = T.nnet.relu(self.latents_unpooled_after_BN)
        else:
            self.latents_unpooled_no_mask = self.latents_unpooled_after_BN

        if top_down_mode == 'signed':
            print('Do signed TopDown')
            self.s_TD = T.sgn(self.latents_unpooled_after_BN)
            self.s_TD  = T.cast(self.s_TD , theano.config.floatX)
            self.signed_latents = self.latents * self.s_TD

            if self.pool_t_mode == 'max_t':
                self.max_over_t_mask_TD = T.grad(
                    T.sum(pool.pool_2d(input=self.signed_latents, ds=(2, 2), ignore_border=True, mode='max')),
                    wrt=self.signed_latents)  # argmax across t
                self.max_over_t_mask_TD = T.cast(self.max_over_t_mask_TD, theano.config.floatX)
            else:
                self.max_over_t_mask_TD = T.ones_like(self.latents)

            self.max_over_a_mask_TD = T.cast(T.gt(self.signed_latents*self.max_over_t_mask_TD, 0.), theano.config.floatX)
            self.max_over_a_mask_TD = T.cast(self.max_over_a_mask_TD, theano.config.floatX)
            self.final_mask_mat = self.max_over_t_mask_TD * self.max_over_a_mask_TD
        elif top_down_mode == 'normal':
            print('Do normal reconstruction TopDown')
            self.final_mask_mat = self.masked_mat
        else:
            print('Please specify your top_down_mode in CRM_no_factor')
            raise

        self.latents_unpooled = self.latents_unpooled_no_mask * self.final_mask_mat

        self.lambdas_deconv = (T.reshape(self.lambdas[:, :, 0],
                                    newshape=(self.K, self.Cin, self.h, self.w))).dimshuffle(1, 0, 2, 3)
        self.lambdas_deconv = self.lambdas_deconv[:, :, ::-1, ::-1]

        if self.border_mode == 'valid':
            self.data_reconstructed_lambdas = conv2d(
                input=self.latents_unpooled,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
                filter_flip=False,
                border_mode='full'
            )
        elif self.border_mode == 'half':
            self.data_reconstructed_lambdas = conv2d(
                input=self.latents_unpooled,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(self.Ni, self.K, self.H, self.W),
                filter_flip=False,
                border_mode='half'
            )
        else:
            self.data_reconstructed_lambdas = conv2d(
                input=self.latents_unpooled,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1),
                filter_flip=False,
                border_mode='valid'
            )

        if denoising == 'simple':
            print('Do not use any combinator to denoise')
            self.data_reconstructed = self.data_reconstructed_lambdas
        elif denoising == 'linear':
            print('Use linear combinator to denoise')
            self.Wbu_comb = theano.shared(value=np.zeros((self.Ni, self.Cin, self.H, self.W), dtype=theano.config.floatX),
                                          name='Wbu_comb', borrow=True)
            self.Wtd_comb = theano.shared(value=np.ones((self.Ni, self.Cin, self.H, self.W), dtype=theano.config.floatX),
                                          name='Wtd_comb', borrow=True)
            self.b_comb = theano.shared(value=np.zeros((self.Ni, self.Cin, self.H, self.W), dtype=theano.config.floatX),
                                        name='b_comb', borrow=True)

            self.data_reconstructed = self.Wtd_comb * self.data_reconstructed_lambdas + self.Wbu_comb * self.data_4D + self.b_comb
            self.params.append(self.Wbu_comb)
            self.params.append(self.Wtd_comb)
            self.params.append(self.b_comb)
        else:
            print('Please implement your own mode in the _Top_Down function in CRM_no_factor_latest.py')
            raise

        self.reconstruction_error = T.mean((self.data_4D_clean - self.data_reconstructed) ** 2)

    def _E_step_Top_Down_Analysis(self, mu_cg, masked_mat, denoising='simple', is_relu=False):
        # Top-Down.
        # input is of size Ni x K x (H-h+1)/2 x (W-w+1)/2
        #

        if self.pool_t_mode == 'max_t':
            print('Pool_Mode is %s' % self.pool_t_mode)
            self.latents_unpooled_before_BN_a = mu_cg.repeat(2, axis=2).repeat(2, axis=3)
        elif self.pool_t_mode == 'mean_t':
            print('Pool_Mode is %s' % self.pool_t_mode)
            self.latents_unpooled_before_BN_a = mu_cg.repeat(self.mean_pool_size[0], axis=2).repeat(self.mean_pool_size[1], axis=3)
        elif self.pool_t_mode == None:
            print('Pool_Mode is None')
            self.latents_unpooled_before_BN_a = mu_cg
        else:
            print('Please specify your TopDown mode in CRM_no_factor_latest.py')
            raise

        if self.is_bn_TD:
            print('do batch_normalization in TopDown')
            if not self.is_tied_bn:
                self.bn_TD = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn,
                                                is_train=self.is_train,
                                                epsilon=self.epsilon)
                self.bn_TD.set_runmode(1)
                self.params.append(self.bn_TD.gamma)
                self.params.append(self.bn_TD.beta)
                self.latents_unpooled_after_BN_a = self.bn_TD.get_result(input=self.latents_unpooled_before_BN_a, input_shape=self.latents_shape)
            else:
                print('Tie BN_BU and BN_TD')
                self.bn_BU.set_runmode(1)
                self.latents_unpooled_after_BN_a = self.bn_BU.get_result(input=self.latents_unpooled_before_BN_a, input_shape=self.latents_shape)
        elif self.is_Dg:
            self.latents_unpooled_after_BN_a = T.inv(self.Dg).dimshuffle('x',0,'x','x') * self.latents_unpooled_before_BN_a + self.dg.dimshuffle('x',0,'x','x')
        else:
            self.latents_unpooled_after_BN_a = self.latents_unpooled_before_BN_a

        if is_relu:
            print('Apply ReLU on the reconstructed images')
            self.latents_unpooled_no_mask_a = T.nnet.relu(self.latents_unpooled_after_BN_a)
        else:
            self.latents_unpooled_no_mask_a = self.latents_unpooled_after_BN_a

        self.latents_unpooled_a = self.latents_unpooled_no_mask_a * masked_mat

        self.lambdas_deconv = (T.reshape(self.lambdas[:, :, 0],
                                    newshape=(self.K, self.Cin, self.h, self.w))).dimshuffle(1, 0, 2, 3)
        self.lambdas_deconv = self.lambdas_deconv[:, :, ::-1, ::-1]

        if self.border_mode == 'valid':
            self.data_reconstructed_lambdas_a = conv2d(
                input=self.latents_unpooled_a,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
                filter_flip=False,
                border_mode='full'
            )
        elif self.border_mode == 'half':
            self.data_reconstructed_lambdas_a = conv2d(
                input=self.latents_unpooled_a,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(self.Ni, self.K, self.H, self.W),
                filter_flip=False,
                border_mode='half'
            )
        else:
            self.data_reconstructed_lambdas_a = conv2d(
                input=self.latents_unpooled_a,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                image_shape=(self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1),
                filter_flip=False,
                border_mode='valid'
            )

        if denoising == 'simple':
            print('Do not use any combinator to denoise')
            self.data_reconstructed_a = self.data_reconstructed_lambdas_a
        elif denoising == 'linear':
            print('Use linear combinator to denoise')
            self.Wbu_comb = theano.shared(value=np.zeros((self.Ni, self.Cin, self.H, self.W), dtype=theano.config.floatX),
                                          name='Wbu_comb', borrow=True)
            self.Wtd_comb = theano.shared(value=np.ones((self.Ni, self.Cin, self.H, self.W), dtype=theano.config.floatX),
                                          name='Wtd_comb', borrow=True)
            self.b_comb = theano.shared(value=np.zeros((self.Ni, self.Cin, self.H, self.W), dtype=theano.config.floatX),
                                        name='b_comb', borrow=True)

            self.data_reconstructed_a = self.Wtd_comb * self.data_reconstructed_lambdas_a + self.Wbu_comb * self.data_4D + self.b_comb
            self.params.append(self.Wbu_comb)
            self.params.append(self.Wtd_comb)
            self.params.append(self.b_comb)
        else:
            print('Please implement your own mode in the _Top_Down function in CRM_no_factor_latest.py')
            raise

    def _M_step(self):
        """
        Maximization step through all clusters

        Update parameters to optimize the log-likelihood

        This assumes that `_E_step()` has been run.
        """

        # make latents_tilde (according to the equations at the top of page 7 in Hinton's paper)
        latents_tilde = T.concatenate([self.latents,
                                       theano.shared(value=np.ones((self.K, 1, self.N),
                                                                   dtype=theano.config.floatX), borrow=True)],
                                      axis=1)

        # make latents_covs_tilde (according to the equations at the top of page 7 in Hinton's paper)
        col1 = T.concatenate([self.latent_covs, (self.latents[:, :, None, :]).dimshuffle((0, 2, 1, 3))], axis=1)
        latent_covs_tilde = T.concatenate([col1, latents_tilde[:, :, None, :]], axis=2)

        # compute lambdas_tilde (equation 15 in Hinton's paper)
        lambdas_tilde, updates22 = theano.scan(
            fn=lambda lt, resp, lt_covs, z: T.dot(T.dot(z[:, None, :] * lt[None, :, :], resp),
                                                  MatrixPinv()(T.dot(lt_covs, resp))),
            outputs_info=None,
            sequences=[latents_tilde[self.indx_good_cluster], self.rs[self.indx_good_cluster],
                       latent_covs_tilde[self.indx_good_cluster]],
            non_sequences=self.data.T)

        # update lambdas and means
        lambdas_new = lambdas_tilde[:, :, 0:self.M]
        self.lambdas_new = T.set_subtensor(self.lambdas[self.indx_good_cluster], lambdas_new)
        means_new = lambdas_tilde[:, :, self.M]
        self.means_new = T.set_subtensor(self.means[self.indx_good_cluster], means_new)

        # update covs
        self._update_covs()

    def _update_covs(self):
        """
        Update covs and inv_covs
        """

        self.covs_new, updates1 = theano.scan(fn=lambda l: T.dot(l, T.transpose(l)),
                                              outputs_info=None,
                                              sequences=[self.lambdas_new])

        self.inv_covs_new, updates2 = theano.scan(
            fn=lambda l: T.dot(l, T.transpose(l)) / T.sqr(T.sum(T.sqr(l))),
            outputs_info=None,
            sequences=self.lambdas_new)