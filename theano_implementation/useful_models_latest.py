import cPickle as pickle

import numpy as np
import theano
from theano import tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from nn_functions_latest import LogisticRegression, LogisticRegressionForSemisupervised, SoftmaxNonlinearity, SoftmaxNonlinearitySemisupervised, HiddenLayerInSoftmax, ConvLayerInSoftmax

from CRM_no_factor_latest import CRM
from DRM_no_factor_latest import DRM_model
from theano.misc.pkl_utils import load

import time


class MNIST_Conv_Small_5_Layers(DRM_model):
    '''
    Conv_Small that the Ladder Network uses for MNIST
    '''
    # TODO: factor out common init code from all models
    def __init__(self, batch_size, Cin, W, H, em_mode, seed, param_dir=[], train_mode='supervised',
                 reconst_weights=[0.0, 0.0, 0.0, 0.0, 0.0], init_Bengio=False, grad_min=-np.inf, grad_max=np.inf,
                 init_params=False, denoising='simple', noise_std=0.45, noise_weights=[0.0, 0.0, 0.0, 0.0, 0.0],
                 is_bn_BU=False, is_bn_TD=False, top_down_mode=None, is_relu=False, nonlin='relu', is_tied_bn=False,
                 is_Dg=False, method='SGD', KL_coef=0.2, is_end_to_end=False, sign_cost_weight=0.0, is_reluI=False):

        print('Your model is MNIST_Conv_Small_5_Layers')

        self.noise_std = noise_std
        self.noise_weights = noise_weights
        self.reconst_weights = reconst_weights
        self.sign_cost_weight = sign_cost_weight

        self.is_bn_BU = is_bn_BU
        self.is_bn_TD = is_bn_TD
        self.is_tied_bn = is_tied_bn
        self.is_Dg = is_Dg
        self.is_end_to_end = is_end_to_end

        self.em_mode = em_mode
        self.batch_size = batch_size
        self.train_mode = train_mode
        self.denoising = denoising
        self.grad_min = grad_min
        self.grad_max = grad_max

        self.top_down_mode = top_down_mode
        self.is_relu = is_relu
        self.is_reluI = is_reluI
        self.nonlin = nonlin
        self.method = method
        self.KL_coef = KL_coef

        is_noisy = [False] * len(self.noise_weights)
        for i in xrange(len(is_noisy)):
            if np.sum(self.noise_weights[0:i + 1]) > 0.0:
                is_noisy[i] = True

        print('noise_weights is:')
        print(self.noise_weights)
        print('is_noisy is:')
        print(is_noisy)

        # Initialize params
        if init_params:
            print('Initialize params using the params stored at:')
            print(param_dir)
            with open(param_dir, 'rb') as f:
                trained_model = load(f)
            lambdas_val_init = []
            amps_val_init = []
            gamma_bn_init = []
            beta_bn_init = []
            mean_bn_init = []
            var_bn_init = []
            for i in xrange(5):
                lambdas_val_init.append(trained_model.layers[i].lambdas.get_value())
                amps_val_init.append(trained_model.layers[i].amps.get_value())
                gamma_bn_init.append(trained_model.layers[i].bn_BU.gamma.get_value())
                beta_bn_init.append(trained_model.layers[i].bn_BU.beta.get_value())
                mean_bn_init.append(trained_model.layers[i].bn_BU.mean.get_value())
                var_bn_init.append(trained_model.layers[i].bn_BU.var.get_value())
            W_softmax_init = trained_model.RegressionInSoftmax.W.get_value()
            b_softmax_init = trained_model.RegressionInSoftmax.b.get_value()
        else:
            print('Randomly initialize params')
            lambdas_val_init = [None] * len(self.noise_weights)
            amps_val_init = [None] * len(self.noise_weights)
            gamma_bn_init = [None] * len(self.noise_weights)
            beta_bn_init = [None] * len(self.noise_weights)
            mean_bn_init = [None] * len(self.noise_weights)
            var_bn_init = [None] * len(self.noise_weights)
            W_softmax_init = None
            b_softmax_init = None

        # Build the model

        self.H1 = H  # 28
        self.W1 = W  # 28
        self.Cin1 = Cin  # 1

        self.h1 = 5
        self.w1 = 5
        self.K1 = 32
        self.M1 = 1

        self.H2 = (self.H1 + self.h1 - 1) / 2  # 16
        self.W2 = (self.W1 + self.w1 - 1) / 2  # 16
        self.Cin2 = self.K1  # 32

        self.h2 = 3
        self.w2 = 3
        self.K2 = 64
        self.M2 = 1

        self.H3 = self.H2 - self.h2 + 1  # 14
        self.W3 = self.W2 - self.w2 + 1  # 14
        self.Cin3 = self.K2  # 64

        self.h3 = 3
        self.w3 = 3
        self.K3 = 64
        self.M3 = 1

        self.H4 = (self.H3 + self.h3 - 1) / 2  # 8
        self.W4 = (self.W3 + self.w3 - 1) / 2  # 8
        self.Cin4 = self.K3  # 64

        self.h4 = 3
        self.w4 = 3
        self.K4 = 128
        self.M4 = 1

        self.H5 = self.H4 - self.h4 + 1  # 6
        self.W5 = self.W4 - self.w4 + 1  # 6
        self.Cin5 = self.K4  # 128

        self.h5 = 1
        self.w5 = 1
        self.K5 = 10
        self.M5 = 1

        self.H_Softmax = 1
        self.W_Softmax = 1
        self.Cin_Softmax = self.K5  # 10

        self.h_Softmax = 1
        self.w_Softmax = 1
        self.K_Softmax = 10
        self.M_Softmax = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

        self.srng = RandomStreams()
        self.srng.seed(np.random.randint(2 ** 15))

        # Forward Step
        self.conv1 = CRM(
            data_4D=self.x
                    + self.noise_weights[0] * self.srng.normal(size=(self.batch_size, self.Cin1, self.H1, self.W1), avg=0.0, std=self.noise_std),
            data_4D_clean=self.x,
            labels=self.y,
            is_train = self.is_train, momentum_bn=self.momentum_bn,
            K=self.K1, M=self.M1, W=self.W1, H=self.H1,
            w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
            amps_val_init=amps_val_init[4], lambdas_val_init=lambdas_val_init[4],
            gamma_bn_init=gamma_bn_init[4], beta_bn_init=beta_bn_init[4], mean_bn_init=mean_bn_init[4],
            var_bn_init=var_bn_init[4],
            PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
            pool_t_mode='max_t',
            border_mode='full',
            rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
            is_noisy=is_noisy[0], is_bn_BU=self.is_bn_BU, is_bn_TD=False, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
            is_Dg = self.is_Dg)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D=self.conv1.output
                                 + self.noise_weights[1] * self.srng.normal(size=(self.batch_size, self.Cin2, self.H2, self.W2), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv1.output_clean,
                         labels=self.y,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K2, M=self.M2, W=self.W2, H=self.H2,
                         w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=amps_val_init[3], lambdas_val_init=lambdas_val_init[3],
                         gamma_bn_init=gamma_bn_init[3], beta_bn_init=beta_bn_init[3], mean_bn_init=mean_bn_init[3],
                         var_bn_init=var_bn_init[3],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[1], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg)

        self.conv2._E_step_Bottom_Up()

        self.conv3 = CRM(data_4D=self.conv2.output
                                 + self.noise_weights[2] * self.srng.normal(size=(self.batch_size, self.Cin3, self.H3, self.W3), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv2.output_clean,
                         labels=self.y,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K3, M=self.M3, W=self.W3, H=self.H3,
                         w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
                         amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
                         gamma_bn_init=gamma_bn_init[2], beta_bn_init=beta_bn_init[2], mean_bn_init=mean_bn_init[2],
                         var_bn_init=var_bn_init[2],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='max_t',
                         border_mode='full',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[2], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg)

        self.conv3._E_step_Bottom_Up()

        self.conv4 = CRM(data_4D=self.conv3.output
                                 + self.noise_weights[3] * self.srng.normal(size=(self.batch_size, self.Cin4, self.H4, self.W4), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv3.output_clean,
                         labels=self.y,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K4, M=self.M4, W=self.W4, H=self.H4,
                         w=self.w4, h=self.h4, Cin=self.Cin4, Ni=self.batch_size,
                         amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
                         gamma_bn_init=gamma_bn_init[1], beta_bn_init=beta_bn_init[1], mean_bn_init=mean_bn_init[1],
                         var_bn_init=var_bn_init[1],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[3], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg)

        self.conv4._E_step_Bottom_Up()

        self.conv5 = CRM(data_4D=self.conv4.output
                                 + self.noise_weights[4] * self.srng.normal(size=(self.batch_size, self.Cin5, self.H5, self.W5), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv4.output_clean,
                         labels=self.y,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K5, M=self.M5, W=self.W5, H=self.H5,
                         w=self.w5, h=self.h5, Cin=self.Cin5, Ni=self.batch_size,
                         amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
                         gamma_bn_init=gamma_bn_init[0], beta_bn_init=beta_bn_init[0], mean_bn_init=mean_bn_init[0],
                         var_bn_init=var_bn_init[0],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='mean_t',
                         border_mode='valid', mean_pool_size=(6, 6),
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[4], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
                         is_Dg=self.is_Dg)

        self.conv5._E_step_Bottom_Up()

        self.RegressionInSoftmax = HiddenLayerInSoftmax(input=self.conv5.output.flatten(2), n_in=self.Cin_Softmax, n_out=self.K_Softmax,
                                          W_init=W_softmax_init, b_init=b_softmax_init, input_clean=self.conv5.output_clean.flatten(2))

        softmax_input = self.RegressionInSoftmax.output
        softmax_input_clean = self.RegressionInSoftmax.output_clean

        # classify the values of the fully-connected sigmoidal layer
        if self.train_mode == 'semisupervised':
            self.softmax_layer_nonlin = SoftmaxNonlinearitySemisupervised(input=softmax_input, input_clean=softmax_input_clean)
        else:
            self.softmax_layer_nonlin = SoftmaxNonlinearity(input=softmax_input, input_clean=softmax_input_clean)

        self.layers = [self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
        self.N_layer = len(self.layers)

        # build Top-Down pass
        if self.is_end_to_end:
            print('Include Softmax Regression in the Top Down')
            self.Build_TopDown_End_to_End(top_down_mode=self.top_down_mode, is_relu=self.is_relu, is_reluI=self.is_reluI)
        else:
            print('Not include Softmax Regression in the Top Down')
            self.Build_TopDown(top_down_mode=self.top_down_mode, is_relu=self.is_relu, is_reluI=self.is_reluI)

        # build the cost function for the model
        self.Build_Cost()

        # build update rules for the model
        self.Build_Update_Rule(method=self.method)

########################################################################################################################
class CIFAR10_Conv_Large_9_Layers(DRM_model):
    '''
    Conv_Small that the Ladder Network uses for MNIST
    '''
    # TODO: factor out common init code from all models
    def __init__(self, batch_size, Cin, W, H, em_mode, seed, param_dir=[], train_mode='supervised',
                 reconst_weights=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], init_Bengio=False, grad_min=-np.inf, grad_max=np.inf,
                 init_params=False, denoising='simple', noise_std=0.45, noise_weights=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                 is_bn_BU=False, is_bn_TD=False, top_down_mode=None, is_relu=False, nonlin='relu', is_tied_bn=False,
                 method='SGD', is_Dg=False, KL_coef=0.2):

        print('Your model is CIFAR10_Conv_Large_9_Layers')

        self.noise_std = noise_std
        self.noise_weights = noise_weights
        self.reconst_weights = reconst_weights

        self.is_bn_BU = is_bn_BU
        self.is_bn_TD = is_bn_TD
        self.is_tied_bn = is_tied_bn
        self.is_Dg = is_Dg

        self.em_mode = em_mode
        self.batch_size = batch_size
        self.train_mode = train_mode
        self.denoising = denoising
        self.grad_min = grad_min
        self.grad_max = grad_max

        self.top_down_mode = top_down_mode
        self.is_relu = is_relu
        self.nonlin = nonlin
        self.method = method
        self.KL_coef = KL_coef

        is_noisy = [False] * len(self.noise_weights)
        for i in xrange(len(is_noisy)):
            if np.sum(self.noise_weights[0:i + 1]) > 0.0:
                is_noisy[i] = True

        print('noise_weights is:')
        print(self.noise_weights)
        print('is_noisy is:')
        print(is_noisy)

        # intialize the parameters
        if init_params:
            print('Initialize params using the params stored at:')
            print(param_dir)
            with open(param_dir, 'rb') as f:
                trained_model = load(f)
            lambdas_val_init = []
            amps_val_init = []
            gamma_bn_init = []
            beta_bn_init = []
            mean_bn_init = []
            var_bn_init = []
            for i in xrange(9):
                lambdas_val_init.append(trained_model.layers[i].lambdas.get_value())
                amps_val_init.append(trained_model.layers[i].amps.get_value())
                gamma_bn_init.append(trained_model.layers[i].bn_BU.gamma.get_value())
                beta_bn_init.append(trained_model.layers[i].bn_BU.beta.get_value())
                mean_bn_init.append(trained_model.layers[i].bn_BU.mean.get_value())
                var_bn_init.append(trained_model.layers[i].bn_BU.var.get_value())
            W_softmax_init = trained_model.RegressionInSoftmax.W.get_value()
            b_softmax_init = trained_model.RegressionInSoftmax.b.get_value()
        else:
            print('Randomly initialize params')
            lambdas_val_init = [None] * len(self.noise_weights)
            amps_val_init = [None] * len(self.noise_weights)
            gamma_bn_init = [None] * len(self.noise_weights)
            beta_bn_init = [None] * len(self.noise_weights)
            mean_bn_init = [None] * len(self.noise_weights)
            var_bn_init = [None] * len(self.noise_weights)
            W_softmax_init = None
            b_softmax_init = None

        # Build the model

        self.H1 = H  # 32
        self.W1 = W  # 32
        self.Cin1 = Cin  # 3

        self.h1 = 3
        self.w1 = 3
        self.K1 = 96
        self.M1 = 1

        self.H2 = self.H1  # 32
        self.W2 = self.W1  # 32
        self.Cin2 = self.K1  # 96

        self.h2 = 3
        self.w2 = 3
        self.K2 = 96
        self.M2 = 1

        self.H3 = self.H2 + self.h2 - 1  # 34
        self.W3 = self.W2 + self.w2 - 1  # 34
        self.Cin3 = self.K2  # 96

        self.h3 = 3
        self.w3 = 3
        self.K3 = 96
        self.M3 = 1

        self.H4 = (self.H3 + self.h3 - 1) / 2  # 18
        self.W4 = (self.W3 + self.w3 - 1) / 2  # 18
        self.Cin4 = self.K3  # 96

        self.h4 = 3
        self.w4 = 3
        self.K4 = 192
        self.M4 = 1

        self.H5 = self.H4 - self.h4 + 1  # 16
        self.W5 = self.W4 - self.w4 + 1  # 16
        self.Cin5 = self.K4  # 192

        self.h5 = 3
        self.w5 = 3
        self.K5 = 192
        self.M5 = 1

        self.H6 = self.H5 + self.h5 - 1  # 18
        self.W6 = self.W5 + self.w5 - 1  # 18
        self.Cin6 = self.K5  # 192

        self.h6 = 3
        self.w6 = 3
        self.K6 = 192
        self.M6 = 1

        self.H7 = (self.H6 - self.h6 + 1) / 2  # 8
        self.W7 = (self.W6 - self.w6 + 1) / 2  # 8
        self.Cin7 = self.K6  # 192

        self.h7 = 3
        self.w7 = 3
        self.K7 = 192
        self.M7 = 1

        self.H8 = self.H7 - self.h7 + 1  # 6
        self.W8 = self.W7 - self.w7 + 1  # 6
        self.Cin8 = self.K7  # 192

        self.h8 = 1
        self.w8 = 1
        self.K8 = 192
        self.M8 = 1

        self.H9 = self.H8 - self.h8 + 1  # 6
        self.W9 = self.W8 - self.w8 + 1  # 6
        self.Cin9 = self.K8  # 192

        self.h9 = 1
        self.w9 = 1
        self.K9 = 10
        self.M9 = 1

        self.H_Softmax = 1
        self.W_Softmax = 1
        self.Cin_Softmax = self.K9  # 10

        self.h_Softmax = 1
        self.w_Softmax = 1
        self.K_Softmax = 10
        self.M_Softmax = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

        self.srng = RandomStreams()
        self.srng.seed(np.random.randint(2 ** 15))

        # Forward Step
        self.conv1 = CRM(
            data_4D=self.x
                    + self.noise_weights[0] * self.srng.normal(size=(self.batch_size, self.Cin1, self.H1, self.W1),
                                                               avg=0.0, std=self.noise_std),
            data_4D_clean=self.x,
            labels=self.y,
            is_train=self.is_train, momentum_bn=self.momentum_bn,
            K=self.K1, M=self.M1, W=self.W1, H=self.H1,
            w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
            amps_val_init=amps_val_init[8], lambdas_val_init=lambdas_val_init[8],
            gamma_bn_init=gamma_bn_init[8], beta_bn_init=beta_bn_init[8], mean_bn_init=mean_bn_init[8],
            var_bn_init=var_bn_init[8],
            PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
            pool_t_mode=None,
            border_mode='half',
            rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
            is_noisy=is_noisy[0], is_bn_BU=self.is_bn_BU, is_bn_TD=False, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
            is_Dg=self.is_Dg)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D=self.conv1.output
                                 + self.noise_weights[1] * self.srng.normal(
            size=(self.batch_size, self.Cin2, self.H2, self.W2), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv1.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K2, M=self.M2, W=self.W2, H=self.H2,
                         w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=amps_val_init[7], lambdas_val_init=lambdas_val_init[7],
                         gamma_bn_init=gamma_bn_init[7], beta_bn_init=beta_bn_init[7], mean_bn_init=mean_bn_init[7],
                         var_bn_init=var_bn_init[7],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='full',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[1], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn,
                         nonlin=self.nonlin, is_Dg=self.is_Dg)

        self.conv2._E_step_Bottom_Up()

        self.conv3 = CRM(data_4D=self.conv2.output
                                 + self.noise_weights[2] * self.srng.normal(
            size=(self.batch_size, self.Cin3, self.H3, self.W3), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv2.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K3, M=self.M3, W=self.W3, H=self.H3,
                         w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
                         amps_val_init=amps_val_init[6], lambdas_val_init=lambdas_val_init[6],
                         gamma_bn_init=gamma_bn_init[6], beta_bn_init=beta_bn_init[6], mean_bn_init=mean_bn_init[6],
                         var_bn_init=var_bn_init[6],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='max_t',
                         border_mode='full',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[2], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn,
                         nonlin=self.nonlin, is_Dg=self.is_Dg)

        self.conv3._E_step_Bottom_Up()

        self.conv4 = CRM(data_4D=self.conv3.output
                                 + self.noise_weights[3] * self.srng.normal(
            size=(self.batch_size, self.Cin4, self.H4, self.W4), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv3.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K4, M=self.M4, W=self.W4, H=self.H4,
                         w=self.w4, h=self.h4, Cin=self.Cin4, Ni=self.batch_size,
                         amps_val_init=amps_val_init[5], lambdas_val_init=lambdas_val_init[5],
                         gamma_bn_init=gamma_bn_init[5], beta_bn_init=beta_bn_init[5], mean_bn_init=mean_bn_init[5],
                         var_bn_init=var_bn_init[5],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[3], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn,
                         nonlin=self.nonlin, is_Dg=self.is_Dg)

        self.conv4._E_step_Bottom_Up()

        self.conv5 = CRM(data_4D=self.conv4.output
                                 + self.noise_weights[4] * self.srng.normal(
            size=(self.batch_size, self.Cin5, self.H5, self.W5), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv4.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K5, M=self.M5, W=self.W5, H=self.H5,
                         w=self.w5, h=self.h5, Cin=self.Cin5, Ni=self.batch_size,
                         amps_val_init=amps_val_init[4], lambdas_val_init=lambdas_val_init[4],
                         gamma_bn_init=gamma_bn_init[4], beta_bn_init=beta_bn_init[4], mean_bn_init=mean_bn_init[4],
                         var_bn_init=var_bn_init[4],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='full',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[4], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn,
                         nonlin=self.nonlin, is_Dg=self.is_Dg)

        self.conv5._E_step_Bottom_Up()

        self.conv6 = CRM(data_4D=self.conv5.output
                                 + self.noise_weights[5] * self.srng.normal(
            size=(self.batch_size, self.Cin6, self.H6, self.W6), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv5.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K6, M=self.M6, W=self.W6, H=self.H6,
                         w=self.w6, h=self.h6, Cin=self.Cin6, Ni=self.batch_size,
                         amps_val_init=amps_val_init[3], lambdas_val_init=lambdas_val_init[3],
                         gamma_bn_init=gamma_bn_init[3], beta_bn_init=beta_bn_init[3], mean_bn_init=mean_bn_init[3],
                         var_bn_init=var_bn_init[3],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='max_t',
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[5], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn,
                         nonlin=self.nonlin, is_Dg=self.is_Dg)

        self.conv6._E_step_Bottom_Up()

        self.conv7 = CRM(data_4D=self.conv6.output
                                 + self.noise_weights[6] * self.srng.normal(
            size=(self.batch_size, self.Cin7, self.H7, self.W7), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv6.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K7, M=self.M7, W=self.W7, H=self.H7,
                         w=self.w7, h=self.h7, Cin=self.Cin7, Ni=self.batch_size,
                         amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
                         gamma_bn_init=gamma_bn_init[2], beta_bn_init=beta_bn_init[2], mean_bn_init=mean_bn_init[2],
                         var_bn_init=var_bn_init[2],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[6], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn,
                         nonlin=self.nonlin, is_Dg=self.is_Dg)

        self.conv7._E_step_Bottom_Up()

        self.conv8 = CRM(data_4D=self.conv7.output
                                 + self.noise_weights[7] * self.srng.normal(
            size=(self.batch_size, self.Cin8, self.H8, self.W8), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv7.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K8, M=self.M8, W=self.W8, H=self.H8,
                         w=self.w8, h=self.h8, Cin=self.Cin8, Ni=self.batch_size,
                         amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
                         gamma_bn_init=gamma_bn_init[1], beta_bn_init=beta_bn_init[1], mean_bn_init=mean_bn_init[1],
                         var_bn_init=var_bn_init[1],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[7], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn,
                         nonlin=self.nonlin, is_Dg=self.is_Dg)

        self.conv8._E_step_Bottom_Up()

        self.conv9 = CRM(data_4D=self.conv8.output
                                 + self.noise_weights[8] * self.srng.normal(
            size=(self.batch_size, self.Cin9, self.H9, self.W9), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv8.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K9, M=self.M9, W=self.W9, H=self.H9,
                         w=self.w9, h=self.h9, Cin=self.Cin9, Ni=self.batch_size,
                         amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
                         gamma_bn_init=gamma_bn_init[0], beta_bn_init=beta_bn_init[0], mean_bn_init=mean_bn_init[0],
                         var_bn_init=var_bn_init[0],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='mean_t', mean_pool_size=(6, 6),
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[8], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn,
                         nonlin=self.nonlin, is_Dg=self.is_Dg)

        self.conv9._E_step_Bottom_Up()

        self.RegressionInSoftmax = HiddenLayerInSoftmax(input=self.conv9.output.flatten(2), n_in=self.Cin_Softmax, n_out=self.K_Softmax,
                                          W_init=W_softmax_init, b_init=b_softmax_init, input_clean=self.conv9.output_clean.flatten(2))

        softmax_input = self.RegressionInSoftmax.output
        softmax_input_clean = self.RegressionInSoftmax.output_clean


        # classify the values of the fully-connected sigmoidal layer
        if self.train_mode == 'semisupervised':
            self.softmax_layer_nonlin = SoftmaxNonlinearitySemisupervised(input=softmax_input, input_clean=softmax_input_clean)
        else:
            self.softmax_layer_nonlin = SoftmaxNonlinearity(input=softmax_input, input_clean=softmax_input_clean)

        self.layers = [self.conv9, self.conv8, self.conv7, self.conv6, self.conv5, self.conv4, self.conv3,
                       self.conv2, self.conv1]
        self.N_layer = len(self.layers)

        # build Top-Down pass
        self.Build_TopDown(top_down_mode=self.top_down_mode, is_relu=self.is_relu)

        # build the cost function for the model
        self.Build_Cost()

        # build update rules for the model
        self.Build_Update_Rule(method=self.method)