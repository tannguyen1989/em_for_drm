__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

import numpy as np
import os

import theano

np.set_printoptions(threshold=np.nan)

from useful_models_latest import CIFAR10_Conv_Large_9_Layers

from load_data_latest import DATA


# from guppy import hpy; h=hpy()

if __name__ == '__main__':
    data_mode = 'all'
    data_dir = '/home/ubuntu/repos/em_for_drm_from_old_git/data/cifar10'

    preprocess_mode = True  # WARN: THIS SHOULD BE ALWAYS ON FOR CIFAR10
    train_mode = 'supervised'  # choose the train mode: semisupervised, supervised, or unsupervised
    init_params = True  # use specific param values to initialize the model
    is_bn_BU = True  # use BatchNorm in the Bottom Up (BU)
    nonlin = 'relu'  # choose the non-linearity for the BU: relu, abs, tanh,....

    Cin = 3  # number of channels of the inputs
    H = 32  # height of input images
    W = 32  # width of input images
    Nlabeled = 50000  # number of labeled examples used during training
    Ni = 50000  # total number of training examples

    seed = 2  # set random seed
    reconst_weights = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # set reconstruction weights
    noise_weights = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # set weights for noise at each layer

    # Only for Finetuning
    root_dir = '/home/ubuntu/research_results/EM_results'
    training_name = 'CIFAR10_Conv_Large_9_Layers_nofactor_semisupervised_Ni50000_Nlabel4000_b100_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_520161026_173850'
    model_file_name = 'model_best.zip'
    param_dir = os.path.join(root_dir, training_name, 'Train/params', model_file_name)

    data = DATA(dataset_name='CIFAR10', data_mode=data_mode, data_dir=data_dir, Nlabeled=Nlabeled, Ni=Ni, Cin=Cin,
                   H=H, W=W, seed=seed, preprocess=preprocess_mode)

    model = CIFAR10_Conv_Large_9_Layers(batch_size=200, Cin=3, W=32, H=32,
                                            em_mode='hard', seed=2, param_dir=param_dir, train_mode=train_mode,
                                            reconst_weights=reconst_weights,
                                            noise_weights=noise_weights,
                                            is_bn_BU=is_bn_BU, init_params=init_params, nonlin=nonlin)

    print('Given labels')
    print(data.test_label[0:200])

    getTestResults = theano.function([model.x, model.is_train, model.momentum_bn],
                                             [model.softmax_layer_nonlin.gammas, model.softmax_layer_nonlin.y_pred],
                                             updates=[], on_unused_input='ignore')

    gammas, label_pred = getTestResults(data.dtest[0:200], 0, 1.0)

    print('Predicted labels')
    print(label_pred)

    getTestResults = theano.function([model.x, model.is_train, model.momentum_bn],
                                     [model.layers[5].latents_before_BN, model.layers[5].latents, model.layers[5].output],
                                     updates=[], on_unused_input='ignore')

    [latents_before_BN, latents, output] = getTestResults(data.dtest[0:200], 0, 1.0)

    # print('latents_before_BN')
    # print(latents_before_BN[0, 0, 0:10, 0:10])
    #
    # print('Shape of latents_before_BN')
    # print(np.shape(latents_before_BN))

    print('output')
    print(output[0, 0, 0:10, 0:10])

    # print('Shape of latents')
    # print(np.shape(latents))
    #
    # print('output')
    # print(output[0, 0, 0:10, 0:10])
    #
    # print('Shape of output')
    # print(np.shape(output))

    # print('maxt')
    # print(maxt[0, 0, 0:10, 0:10])
    #
    # print('Shape of maxt')
    # print(np.shape(maxt))
    #
    # print('maxa')
    # print(maxa[0, 0, 0:10, 0:10])
    #
    # print('Shape of maxa')
    # print(np.shape(maxa))
    #
    # print('mask')
    # print(mask[0, 0, 0:10, 0:10])
    #
    # print('Shape of mask')
    # print(np.shape(mask))

    # print('lambdas')
    # print((model.layers[7].lambdas.get_value())[0:3,0:2,:])
    #
    # print('Shape of lambdas')
    # print(np.shape(model.layers[7].lambdas.get_value()))


    # print('latents_before_BN')
    # print(latents_before_BN[0,0,0:10,0:10])

    # print('latents')
    # print(latents[0, 0, 0:10, 0:10])
    #
    # print('output')
    # print(output[0:200])
    #
    # print('Shape of output')
    # print(np.shape(output))

    # print('Gammas')
    # print(gammas)







