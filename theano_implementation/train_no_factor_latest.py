from __future__ import print_function
__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import os

import numpy as np

import theano
import theano.tensor as T

import copy
import cPickle
from theano.misc.pkl_utils import dump

from numpy.linalg import norm
import time


# from guppy import hpy; h=hpy()

# def detect_nan(i, node, fn):
#     for output in fn.outputs:
#         if (not isinstance(output[0], np.random.RandomState) and
#             np.isnan(output[0]).any()):
#             print('*** NaN detected ***')
#             theano.printing.debugprint(node)
#             print('Inputs : %s' % [input[0] for input in fn.inputs])
#             print('Outputs: %s' % [output[0] for output in fn.outputs])
#             break

class TrainNoFactorDRM(object):
    """
    Training algorithm class

    """
    def __init__(self, batch_size, train_data, test_data, valid_data, train_label, test_label, valid_label, model, output_dir, param_dir='params'):
        '''

        :param data_mode:
        :param em_mode:
        :param update_mode:
        :param stop_mode:
        :param train_method:
        :param Ni:
        :param Cin:
        :param H:
        :param W:
        :param batch_size:
        :param seed:
        :param output_dir:
        :param data_dir:
        :return:
        '''
        self.batch_size = batch_size
        self.d = train_data
        self.dtest = test_data
        self.dvalid = valid_data
        self.label = train_label
        self.labeltest = test_label
        self.labelvalid = valid_label

        self.model = model
        self.output_dir = output_dir
        self.param_dir = os.path.join(self.output_dir, param_dir)
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        if not os.path.exists(self.param_dir):
            os.makedirs(self.param_dir)

    def train_Softmax(self, max_epochs, classification_dir):
        self.classification_dir = os.path.join(self.output_dir, classification_dir)
        if not os.path.exists(self.classification_dir):
            os.makedirs(self.classification_dir)

        if len(np.shape(self.d)) == 4:
            N, Cin, H, W = np.shape(self.d)
        else:
            N = np.shape(self.d)[0]

        n_batches = N/self.batch_size

        if len(np.shape(self.dvalid)) == 4:
            N_valid, Cin_valid, h_valid, w_valid = np.shape(self.dvalid)
        else:
            N_valid = np.shape(self.dvalid)[0]

        n_valid_batches = N_valid/self.batch_size

        if len(np.shape(self.dtest)) == 4:
            N_test, Cin_test, h_test, w_test = np.shape(self.dtest)
        else:
            N_test = np.shape(self.dtest)[0]

        n_test_batches = N_test/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_dat_test = theano.shared(self.dtest)
        shared_dat_valid = theano.shared(self.dvalid)
        shared_label = theano.shared(self.label)
        shared_label_test = theano.shared(self.labeltest)
        shared_label_valid = theano.shared(self.labelvalid)

        index = T.iscalar()

        if len(np.shape(self.d)) == 4:
            train_model = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
                                                      self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

            test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

            validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                             givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                     self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})
        else:
            train_model = theano.function([index], self.model.output_var,
                                              updates=self.model.updates, on_unused_input='ignore',
                                              givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:],
                                                      self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

            test_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                         givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

            validate_model = theano.function([index], self.model.softmax_layer.errors(self.model.y),
                                             givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                     self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})

        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)

        done_looping = False

        patience = 10000  # look as this many examples regardless
        patience_increase = 2  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.995  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_params = None
        best_validation_loss = np.inf
        test_score_vec = []
        iter = 0

        start_time = time.clock()

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1
            print('Epoch %d' %epoch)

            for minibatch_index in xrange(n_batches):
                # print minibatch_index
                iter = iter + 1
                output_val = train_model(minibatch_index)

                if iter % validation_frequency == 0:
                    validation_losses = [validate_model(i) for i
                                     in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_batches, \
                           this_validation_loss * 100.))

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                        # save best validation score and iteration number
                        best_validation_loss = this_validation_loss
                        best_iter = iter
                        best_params = self.model.params

                        # test it on the test set
                        test_losses = [test_model(i) for i in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)
                        print(('     epoch %i, minibatch %i/%i, test error of best '
                               'model %f %%') %
                              (epoch, minibatch_index + 1, n_batches,
                               test_score * 100.))

                if patience <= iter:
                    done_looping = True
                    break

            test_losses = [test_model(i) for i in xrange(n_test_batches)]
            test_score_epoch = np.mean(test_losses)
            test_score_vec.append(test_score_epoch)
            epoch_vec.append(epoch)
            fig = figure()
            plot(epoch_vec[1:], test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.classification_dir, 'Error-vs-Epoch-EG.png'))
            close(fig)
            plot_classification_file = os.path.join(self.classification_dir, 'plot_classification.npz')
            np.savez(plot_classification_file, epoch_vec=epoch_vec, test_score_vec=test_score_vec)

            param_file = os.path.join(self.classification_dir, 'EM_results_epoch_%i.npz' %epoch)
            np.savez(param_file, W=output_val[0], b=output_val[1])

        end_time = time.clock()
        print('Optimization complete.')
        print('Best validation score of %f %% obtained at iteration %i,'\
              'with test performance %f %%' %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))
        print('The code ran for %.2fm' % ((end_time - start_time) / 60.))

        save_file = open(os.path.join(self.output_dir, 'cnn_mean_removed.pkl'), 'wb')  # this will overwrite current content
        cPickle.dump(best_params, save_file, -1)
        save_file.close()
        fig = figure()
        plot(epoch_vec[1:], test_score_vec)
        legend()
        xlabel('Epoch')
        ylabel('Error Rate')
        title('Error-vs-Epoch-EG')
        fig.savefig(os.path.join(self.classification_dir, 'Error-vs-Epoch-EG.png'))
        close(fig)

    ####################################################################################################################
    # Train Supervised #
    ####################################################################################################################
    # TODO: factor out common part of supervised/unsupervised/semisupervised training codes
    def train_supervised(self, max_epochs, verbose, tol, stop_mode, lr_init=0.1, epoch_to_reduce_lr=(np.inf, np.inf),
                         lr_decay=0.1, lr_final=0.0001, debug_mode=False, num_epoch_to_plot_NLL=1, momentum_bn=0.9):
        print('Start training supervised')
        # Set up the data
        N, Cin, h, w = np.shape(self.d)
        n_batches = N/self.batch_size
        N_valid, Cin_valid, h_valid, w_valid = np.shape(self.dvalid)
        n_valid_batches = N_valid/self.batch_size
        N_test, Cin_test, h_test, w_test = np.shape(self.dtest)
        n_test_batches = N_test/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_dat_test = theano.shared(self.dtest)
        shared_dat_valid = theano.shared(self.dvalid)
        shared_label = theano.shared(self.label)
        shared_label_test = theano.shared(self.labeltest)
        shared_label_valid = theano.shared(self.labelvalid)

        # Compute the number of layer
        num_layer = len(self.model.layers)

        # Building function used during training
        index = T.iscalar()

        do_one_iter = theano.function([index, self.model.lr, self.model.is_train, self.model.momentum_bn], self.model.output_var,
                                      updates=self.model.updates, on_unused_input='warn',
                                      givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size],
                                              self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        test_model = theano.function([index, self.model.is_train, self.model.momentum_bn], self.model.softmax_layer_nonlin.errors(self.model.y),
                                     updates=[], on_unused_input='warn',
                                     givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                             self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

        validate_model = theano.function([index, self.model.is_train, self.model.momentum_bn], self.model.softmax_layer_nonlin.errors(self.model.y),
                                         updates=[], on_unused_input='warn',
                                         givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})

        compute_NLL = theano.function([index, self.model.is_train, self.model.momentum_bn], self.model.cost,
                                      updates=[], on_unused_input='warn',
                                      givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size],
                                              self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        # debug_one_EG = theano.function([index], [self.model.layers[0].latents_pre.std(axis=(0,), keepdims=True),
        #                                          self.model.layers[1].latents_pre.std(axis=(0,), keepdims=True),
        #                                          self.model.layers[2].latents_pre.std(axis=(0,), keepdims=True),
        #                                          self.model.layers[3].latents_pre.std(axis=(0,), keepdims=True),
        #                                          self.model.layers[4].latents_pre.std(axis=(0,), keepdims=True)],
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
        #                                           self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        # Initialize vectors and matrices that hold the training results
        dLA_vec = []
        dLA_mat = []
        amps_val_mat = []
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)
        best_validation_loss = np.inf
        test_score_vec = []
        validation_score_vec = []
        iter = 0

        lambdas_val = []
        amps_val = []

        # Note that for the initial values, we don't save initial betas since it can be calculated from lambdas
        # For later epoch, we save betas although it can still be calculated from lambdas
        for i in xrange(num_layer):
            lambdas_val.append(np.asarray(self.model.layers[i].lambdas.get_value(), dtype=theano.config.floatX))
            amps_val.append(np.asarray(self.model.layers[i].amps.get_value(), dtype=theano.config.floatX))

            dLA_vec.append([])
            dLA_mat.append([])
            amps_val_mat.append([])
            amps_val_mat[i].append(amps_val[i])

        # Compute initial validation error
        validation_losses = [validate_model(i, 0, 1.0) for i
                             in xrange(n_valid_batches)]
        this_validation_loss = np.mean(validation_losses)
        print('Initial validation score = %f' % this_validation_loss)
        validation_score_vec.append(this_validation_loss)

        # Compute initial test error
        test_losses = [test_model(i, 0, 1.0) for i in xrange(n_test_batches)]
        test_score_epoch = np.mean(test_losses)
        print('Initial test score = %f' % test_score_epoch)
        test_score_vec.append(test_score_epoch)

        # Save initial parameters
        meta_dir = os.path.join(self.param_dir, 'model_and_training_meta_params.npz') #TODO: change the name of meta_params
        np.savez(meta_dir, lr_init=lr_init, lr_final=lr_final, max_epochs=max_epochs, momentum_bn=momentum_bn, batch_size=self.batch_size,
                 noise_std=self.model.noise_std, noise_weights=self.model.noise_weights, reconst_weights=self.model.reconst_weights, is_bn_BU=self.model.is_bn_BU, is_bn_TD=self.model.is_bn_TD,
                 em_mode=self.model.em_mode, train_mode=self.model.train_mode, denoising=self.model.denoising, grad_min=self.model.grad_min, grad_max=self.model.grad_max, is_tied_bn=self.model.is_tied_bn,
                 top_down_mode=self.model.top_down_mode, is_relu=self.model.is_relu, nonlin=self.model.nonlin)

        model_dir = os.path.join(self.param_dir, 'model_epoch_%i.pkl' %epoch)
        model_file = open(model_dir, 'wb')
        cPickle.dump(self.model, model_file)
        model_file.close()

        model_dir = os.path.join(self.param_dir, 'model_epoch_%i.zip' %epoch)
        model_file = open(model_dir, 'wb')
        dump(self.model, model_file)
        model_file.close()

        # Compute the initial NLLs
        newTotalNLL = 0
        for minibatch_index in xrange(n_batches):
            # [std5, std4, std3, std2, std1] = debug_one_EG(minibatch_index)
            totalNLL_val = compute_NLL(minibatch_index, 0, 1.0)
            # if np.isnan(totalNLL_val):
            #     pdb.set_trace()
            newTotalNLL = newTotalNLL + totalNLL_val

        newTotalNLL = newTotalNLL/n_batches
        NegLogLs_vec.append(newTotalNLL)

        print('Initial TotalNLL=', newTotalNLL)

        done_looping = False

        patience = 20000  # look as this many examples regardless
        patience_increase = 3  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.999  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        decay_val = np.exp(np.log(lr_init / lr_final) / (max_epochs - 2))
        current_lr = lr_init * decay_val

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1

            LA = []
            newLA = []

            # Compute the Lambdas before epoch i
            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))

            # start the training for epoch i

            current_lr = np.float32(current_lr / decay_val)
            print('Epoch %d with Learning Rate = %f' % (epoch, current_lr))

            for minibatch_index in xrange(n_batches):
                # print(minibatch_index)
                iter = iter + 1
                # all_grads = debug_one_EG(minibatch_index)
                # [std5, std4, std3, std2, std1] = debug_one_EG(minibatch_index)
                # if np.any(np.isnan(std5)) or np.any(np.isnan(std4)) or np.any(np.isnan(std3)) or np.any(np.isnan(std2)) or np.any(np.isnan(std1)):
                #     pdb.set_trace()
                output_val = do_one_iter(minibatch_index, current_lr, 1, momentum_bn)

                # # for debugging grads and lambdas
                # for grad in all_grads:
                #     grad = np.asarray(grad, dtype=theano.config.floatX)
                #     if np.any(np.isinf(grad)):
                #         pdb.set_trace()
                #     if np.any(np.isnan(grad)):
                #         pdb.set_trace()
                #     if np.any(grad > 100.0):
                #         pdb.set_trace()
                #
                # for i in xrange(num_layer):
                #     lam = np.asarray(output_val[2 + 4*i], dtype=theano.config.floatX)
                #     if np.any(np.isnan(lam)):
                #         pdb.set_trace()

                if iter % validation_frequency == 0:
                    validation_losses = [validate_model(i, 0, 1.0) for i
                                     in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_batches, \
                           this_validation_loss * 100.))

                    validation_score_vec.append(this_validation_loss)

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                if patience <= iter:
                    done_looping = True
                    break

            test_losses = [test_model(i, 0, 1.0) for i in xrange(n_test_batches)]
            test_score_epoch = np.mean(test_losses)
            print('Test score at epoch %i = %f' %(epoch, test_score_epoch))

            if epoch % 10 == 1:
                misc_dir = os.path.join(self.param_dir, 'misc_params_epoch_%i.npz' %epoch)
                np.savez(misc_dir, learning_rate=current_lr)

                model_dir = os.path.join(self.param_dir, 'model_epoch_%i.zip' %epoch)
                model_file = open(model_dir, 'wb')
                dump(self.model, model_file)
                model_file.close()

                model_dir = os.path.join(self.param_dir, 'model_epoch_%i.pkl' % epoch)
                model_file = open(model_dir, 'wb')
                cPickle.dump(self.model, model_file)
                model_file.close()

            misc_dir = os.path.join(self.param_dir, 'misc_params_latest.npz')
            np.savez(misc_dir, learning_rate=current_lr)

            model_dir = os.path.join(self.param_dir, 'model_latest.zip')
            model_file = open(model_dir, 'wb')
            dump(self.model, model_file)
            model_file.close()

            model_dir = os.path.join(self.param_dir, 'model_latest.pkl')
            model_file = open(model_dir, 'wb')
            cPickle.dump(self.model, model_file)
            model_file.close()

            if test_score_epoch < np.min(test_score_vec):
                misc_dir = os.path.join(self.param_dir, 'misc_params_best.npz')
                np.savez(misc_dir, learning_rate=current_lr)

                model_dir = os.path.join(self.param_dir, 'model_best.zip')
                model_file = open(model_dir, 'wb')
                dump(self.model, model_file)
                model_file.close()

                model_dir = os.path.join(self.param_dir, 'model_best.pkl')
                model_file = open(model_dir, 'wb')
                cPickle.dump(self.model, model_file)
                model_file.close()

            test_score_vec.append(test_score_epoch)
            epoch_vec.append(epoch)

            fig = figure()
            plot(epoch_vec, test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Test-Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.output_dir, 'Test-Error-vs-Epoch-EG.png'))
            close(fig)

            fig = figure()
            plot(epoch_vec, validation_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Validation-Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.output_dir, 'Validation-Error-vs-Epoch-EG.png'))
            close(fig)

            plot_classification_file = os.path.join(self.output_dir, 'plot_classification.npz')
            np.savez(plot_classification_file, epoch_vec=epoch_vec, test_score_vec=test_score_vec, validation_score_vec=validation_score_vec)

            print('Best test score of %f %% obtained at epoch %i' % (np.min(test_score_vec) * 100., np.argmin(test_score_vec) + 1))

            if epoch == 1:
                legend_misc = range(np.shape(output_val[2])[0])
                for i in xrange(len(legend_misc)):
                    legend_misc[i] = str(legend_misc[i])
                legend_misc.append('average')

            # Compute the NLLs at epoch i and plot the NLLs
            if epoch % num_epoch_to_plot_NLL == 0:
                newTotalNLL = 0
                for minibatch_index in xrange(n_batches):
                    totalNLL_val = compute_NLL(minibatch_index, 0, 1.0)
                    newTotalNLL = newTotalNLL + totalNLL_val

                newTotalNLL = newTotalNLL/n_batches
                NegLogLs_vec.append(newTotalNLL)

                print('TotalNLL=', newTotalNLL)

                fig1 = figure()
                plot(epoch_vec[0:epoch+1:num_epoch_to_plot_NLL], NegLogLs_vec)
                xlabel('Epoch')
                ylabel('Training Cost')
                title('Convergence_of_Training_Cost_BatchSize_%i' % self.batch_size)
                fig1.savefig(
                    os.path.join(self.output_dir, 'Convergence_of_Training_Cost_BatchSize_%i.png' % self.batch_size))
                close()

            # Output parameters and plot the results
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                lambdas_val.append(np.asarray(output_val[2 + 4*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 4*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[3 + 4*i],dtype=theano.config.floatX))

                # Compute the relative difference in Lambdas
                newLA.append(output_val[2 + 4 * i])
                dLA_vec[i].append(norm(newLA[i] - LA[i]) / norm(LA[i]))
                dLA_mat[i].append(norm(newLA[i][:, :, 0] - LA[i][:, :, 0], axis=1))

                # Record the amps_val into amps_val_mat
                amps_val_mat[i].append(amps_val[i])

                # Plot the relative difference in Lambdas
                fig4 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i' % (i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig4.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (
                i + 1, self.batch_size)))
                close()

                fig5 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i' % (i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig5.savefig(os.path.join(self.output_dir,
                                          'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (
                                          i + 1, self.batch_size)))
                close()

                fig6 = figure()
                plot(dLA_mat[i])
                legend(legend_misc[0:-1], prop={'size': 8})
                xlabel('Epoch')
                ylabel('Difference')
                title('Convergence_of_Lambdas_Layer%i' % (i + 1))
                fig6.savefig(os.path.join(self.output_dir, 'Convergence_of_Lambdas_Layer%i.png' % (i + 1)))
                close()

                # Plot the amps_val into amps_val_mat

                fig7 = figure()
                plot(amps_val_mat[i])
                legend(legend_misc[0:-1],prop={'size':8})
                xlabel('Epoch')
                ylabel('Pi(cg)')
                title('Convergence_of_Pi_cg_Layer%i' % (i + 1))
                fig7.savefig(os.path.join(self.output_dir, 'Convergence_of_Pi_cg_Layer%i.png' % (i + 1)))
                close()

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec,
                     dLA_mat=dLA_mat, amps_val_mat=amps_val_mat)

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newTotalNLL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('Optimization complete.')
        print('Best test score of %f %% obtained at epoch %i' % (np.min(test_score_vec) * 100., np.argmin(test_score_vec) + 1))

    ####################################################################################################################
    # Train Unsupervised #
    ####################################################################################################################
    def train_unsupervised(self, max_epochs, verbose, tol, stop_mode, lr_init=0.1, epoch_to_reduce_lr=(np.inf, np.inf),
                             lr_decay=0.1, lr_final=0.0001, debug_mode=False, momentum_bn=0.9):
        print('Start training unsupervised')
        # Set up the data
        N, Cin, h, w = np.shape(self.d)
        n_batches = N/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_label = theano.shared(self.label)

        # Compute the number of layer
        num_layer = len(self.model.layers)

        # Building function used during training
        index = T.iscalar()

        do_one_iter = theano.function([index, self.model.lr, self.model.is_train, self.model.momentum_bn], self.model.output_var,
                                      updates=self.model.updates, on_unused_input='warn',
                                      givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size],
                                              self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        compute_NLL = theano.function([index, self.model.is_train, self.model.momentum_bn], self.model.cost,
                                      updates=[], on_unused_input='warn',
                                      givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size],
                                              self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        # debug_one_EG = theano.function([index], self.model.grads,
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
        #                                           self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})


        # Initialize vectors and matrices that hold the training results
        dLA_vec = []
        NL = None
        dLA_mat = []
        amps_val_mat = []
        NegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)

        lambdas_val = []
        betas_val = []
        amps_val = []

        # Note that for the initial values, we don't save initial betas since it can be calculated from lambdas
        # For later epoch, we save betas although it can still be calculated from lambdas
        for i in xrange(num_layer):
            lambdas_val.append(np.asarray(self.model.layers[i].lambdas.get_value(), dtype=theano.config.floatX))
            amps_val.append(np.asarray(self.model.layers[i].amps.get_value(), dtype=theano.config.floatX))

            dLA_vec.append([])
            dLA_mat.append([])
            amps_val_mat.append([])
            amps_val_mat[i].append(amps_val[i])

        # Save initial parameters
        meta_dir = os.path.join(self.param_dir, 'model_and_training_meta_params.npz')
        np.savez(meta_dir, lr_init=lr_init, lr_final=lr_final, max_epochs=max_epochs, momentum_bn=momentum_bn,
                 batch_size=self.batch_size,
                 noise_std=self.model.noise_std, noise_weights=self.model.noise_weights,
                 reconst_weights=self.model.reconst_weights, is_bn_BU=self.model.is_bn_BU, is_bn_TD=self.model.is_bn_TD,
                 em_mode=self.model.em_mode, train_mode=self.model.train_mode, denoising=self.model.denoising,
                 grad_min=self.model.grad_min, grad_max=self.model.grad_max, is_tied_bn=self.model.is_tied_bn,
                 top_down_mode=self.model.top_down_mode, is_relu=self.model.is_relu, nonlin=self.model.nonlin)

        model_dir = os.path.join(self.param_dir, 'model_epoch_%i.pkl' % epoch)
        model_file = open(model_dir, 'wb')
        cPickle.dump(self.model, model_file)
        model_file.close()

        model_dir = os.path.join(self.param_dir, 'model_epoch_%i.zip' % epoch)
        model_file = open(model_dir, 'wb')
        dump(self.model, model_file)
        model_file.close()

        # Compute the initial NLLs
        newTotalNLL = 0
        for minibatch_index in xrange(n_batches):
            totalNLL_val = compute_NLL(minibatch_index, 0, 1.0)
            newTotalNLL = newTotalNLL + totalNLL_val

        newTotalNLL = newTotalNLL/n_batches
        NegLogLs_vec.append(newTotalNLL)

        print('Initial TotalNLL=', newTotalNLL)

        done_looping = False

        iter = 0

        decay_val = np.exp(np.log(lr_init / lr_final) / (max_epochs - 2))
        current_lr = lr_init * decay_val

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1

            LA = []
            newLA = []

            # Compute the Lambdas before epoch i
            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))

            # start the training for epoch i

            current_lr = np.float32(current_lr / decay_val)
            print('Epoch %d with Learning Rate = %f' % (epoch, current_lr))

            for minibatch_index in xrange(n_batches):
                # print(minibatch_index)
                iter = iter + 1
                # all_grads = debug_one_EG(minibatch_index)
                output_val = do_one_iter(minibatch_index, current_lr, 1, momentum_bn)

                # # for debugging grads and lambdas
                # for grad in all_grads:
                #     grad = np.asarray(grad, dtype=theano.config.floatX)
                #     if np.any(np.isinf(grad)):
                #         pdb.set_trace()
                #     if np.any(np.isnan(grad)):
                #         pdb.set_trace()
                #     if np.any(grad > 100.0):
                #         pdb.set_trace()
                #
                # for i in xrange(num_layer):
                #     lam = np.asarray(output_val[2 + 4*i], dtype=theano.config.floatX)
                #     if np.any(np.isnan(lam)):
                #         pdb.set_trace()

            epoch_vec.append(epoch)

            if epoch == 1:
                legend_misc = range(np.shape(output_val[2])[0])
                for i in xrange(len(legend_misc)):
                    legend_misc[i] = str(legend_misc[i])
                legend_misc.append('average')

            # Compute the NLLs at epoch i and plot the NLLs

            newTotalNLL = 0

            for minibatch_index in xrange(n_batches):
                totalNLL_val = compute_NLL(minibatch_index, 0, 1.0)
                newTotalNLL = newTotalNLL + totalNLL_val

            newTotalNLL = newTotalNLL / n_batches

            if epoch % 10 == 1:
                misc_dir = os.path.join(self.param_dir, 'misc_params_epoch_%i.npz' %epoch)
                np.savez(misc_dir, learning_rate=current_lr)

                model_dir = os.path.join(self.param_dir, 'model_epoch_%i.zip' %epoch)
                model_file = open(model_dir, 'wb')
                dump(self.model, model_file)
                model_file.close()

                model_dir = os.path.join(self.param_dir, 'model_epoch_%i.pkl' % epoch)
                model_file = open(model_dir, 'wb')
                cPickle.dump(self.model, model_file)
                model_file.close()

            misc_dir = os.path.join(self.param_dir, 'misc_params_latest.npz')
            np.savez(misc_dir, learning_rate=current_lr)

            model_dir = os.path.join(self.param_dir, 'model_latest.zip')
            model_file = open(model_dir, 'wb')
            dump(self.model, model_file)
            model_file.close()

            model_dir = os.path.join(self.param_dir, 'model_latest.pkl')
            model_file = open(model_dir, 'wb')
            cPickle.dump(self.model, model_file)
            model_file.close()

            if newTotalNLL < np.min(NegLogLs_vec):
                misc_dir = os.path.join(self.param_dir, 'misc_params_best.npz')
                np.savez(misc_dir, learning_rate=current_lr)

                model_dir = os.path.join(self.param_dir, 'model_best.zip')
                model_file = open(model_dir, 'wb')
                dump(self.model, model_file)
                model_file.close()

                model_dir = os.path.join(self.param_dir, 'model_best.pkl')
                model_file = open(model_dir, 'wb')
                cPickle.dump(self.model, model_file)
                model_file.close()

            NegLogLs_vec.append(newTotalNLL)

            print('TotalNLL=', newTotalNLL)

            fig1 = figure()
            plot(epoch_vec, NegLogLs_vec)
            xlabel('Epoch')
            ylabel('Training Cost')
            title('Convergence_of_Reconstruction_Error_BatchSize_%i' % self.batch_size)
            fig1.savefig(os.path.join(self.output_dir,
                                      'Convergence_of_Reconstruction_Error_BatchSize_%i.png' % self.batch_size))
            close()

            # Output parameters and plot the results
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                lambdas_val.append(np.asarray(output_val[2 + 4*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 4*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[3 + 4*i],dtype=theano.config.floatX))

                # Compute the relative difference in Lambdas
                newLA.append(output_val[2 + 4 * i])
                dLA_vec[i].append(norm(newLA[i] - LA[i]) / norm(LA[i]))
                dLA_mat[i].append(norm(newLA[i][:, :, 0] - LA[i][:, :, 0], axis=1))

                # Record the amps_val into amps_val_mat
                amps_val_mat[i].append(amps_val[i])

                # Plot the relative difference in Lambdas
                fig4 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i' % (i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig4.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (
                i + 1, self.batch_size)))
                close()

                fig5 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i' % (i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig5.savefig(os.path.join(self.output_dir,
                                          'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (
                                          i + 1, self.batch_size)))
                close()

                fig6 = figure()
                plot(dLA_mat[i])
                legend(legend_misc[0:-1], prop={'size': 8})
                xlabel('Epoch')
                ylabel('Difference')
                title('Convergence_of_Lambdas_Layer%i' % (i + 1))
                fig6.savefig(os.path.join(self.output_dir, 'Convergence_of_Lambdas_Layer%i.png' % (i + 1)))
                close()

                # Plot the amps_val into amps_val_mat

                fig7 = figure()
                plot(amps_val_mat[i])
                legend(legend_misc[0:-1],prop={'size':8})
                xlabel('Epoch')
                ylabel('Pi(cg)')
                title('Convergence_of_Pi_cg_Layer%i' % (i + 1))
                fig7.savefig(os.path.join(self.output_dir, 'Convergence_of_Pi_cg_Layer%i.png' % (i + 1)))
                close()

            if stop_mode == 'NLL':
                if NL!=None:
                    dNL = np.abs((newTotalNLL-NL)/NL)
                    if epoch > 5 and dNL < tol:
                        break
                NL = newTotalNLL

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec,
                     dLA_mat=dLA_mat, amps_val_mat=amps_val_mat)

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newTotalNLL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

    ####################################################################################################################
    # Train Semisupervised #
    ####################################################################################################################
    def train_semisupervised(self, max_epochs, verbose, tol, stop_mode, lr_init=0.1, epoch_to_reduce_lr=(np.inf, np.inf),
                             lr_decay=0.1, lr_final=0.0001, debug_mode=False, num_epoch_to_plot_NLL=1, momentum_bn=0.9):
        print('Start training semisupervised')
        # Set up the data
        N, Cin, h, w = np.shape(self.d)
        n_batches = N/self.batch_size
        N_valid, Cin_valid, h_valid, w_valid = np.shape(self.dvalid)
        n_valid_batches = N_valid/self.batch_size
        N_test, Cin_test, h_test, w_test = np.shape(self.dtest)
        n_test_batches = N_test/self.batch_size

        shared_dat = theano.shared(self.d)
        shared_dat_test = theano.shared(self.dtest)
        shared_dat_valid = theano.shared(self.dvalid)
        shared_label = theano.shared(self.label)
        shared_label_test = theano.shared(self.labeltest)
        shared_label_valid = theano.shared(self.labelvalid)

        # Compute the number of layer
        num_layer = len(self.model.layers)
        acts_beforeBN_list = []
        acts_afterBN_list = []
        acts_afterpooling_list = []
        acts_unpooled_afterBN_list = []
        data_reconstructed_list = []
        for i in xrange(num_layer):
            acts_beforeBN_list.append(self.model.layers[i].latents_before_BN)
            acts_afterBN_list.append(self.model.layers[i].latents)
            acts_afterpooling_list.append(self.model.layers[i].output)
            acts_unpooled_afterBN_list.append(self.model.layers[i].latents_unpooled_after_BN)
            data_reconstructed_list.append(self.model.layers[i].data_reconstructed)

        # Building function used during training
        index = T.iscalar()

        do_one_iter = theano.function([index, self.model.lr, self.model.is_train, self.model.momentum_bn], self.model.output_var,
                                      updates=self.model.updates, on_unused_input='warn',
                                      givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size],
                                              self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        # update_gamma = theano.function([],[], updates=self.model.positive_constraints, on_unused_input='warn')

        test_model = theano.function([index, self.model.is_train, self.model.momentum_bn], self.model.softmax_layer_nonlin.errors(self.model.y),
                                     updates=[], on_unused_input='warn',
                                     givens={self.model.x: shared_dat_test[index * self.batch_size: (index + 1) * self.batch_size],
                                             self.model.y: shared_label_test[index * self.batch_size: (index + 1) * self.batch_size]})

        validate_model = theano.function([index, self.model.is_train, self.model.momentum_bn], self.model.softmax_layer_nonlin.errors(self.model.y),
                                         updates=[], on_unused_input='warn',
                                         givens={self.model.x: shared_dat_valid[index * self.batch_size: (index + 1) * self.batch_size],
                                                 self.model.y: shared_label_valid[index * self.batch_size: (index + 1) * self.batch_size]})

        compute_NLL = theano.function([index, self.model.is_train, self.model.momentum_bn], [self.model.cost, self.model.unsupervisedNLL, self.model.supervisedNLL],
                                      updates=[], on_unused_input='warn',
                                      givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size],
                                              self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})

        get_acts_beforeBN = theano.function([index, self.model.is_train, self.model.momentum_bn], acts_beforeBN_list,
                                                   updates=[], on_unused_input='ignore',
                                                   givens={self.model.x: shared_dat_valid[
                                                                         index * self.batch_size: (
                                                                                                  index + 1) * self.batch_size,
                                                                         :,
                                                                         :, :],
                                                           self.model.y: shared_label_valid[
                                                                         index * self.batch_size: (
                                                                                                  index + 1) * self.batch_size]})

        get_acts_afterBN = theano.function([index, self.model.is_train, self.model.momentum_bn], acts_afterBN_list,
                                            updates=[], on_unused_input='ignore',
                                            givens={self.model.x: shared_dat_valid[
                                                                  index * self.batch_size: (
                                                                                               index + 1) * self.batch_size,
                                                                  :,
                                                                  :, :],
                                                    self.model.y: shared_label_valid[
                                                                  index * self.batch_size: (
                                                                                               index + 1) * self.batch_size]})

        get_acts_afterpooling = theano.function([index, self.model.is_train, self.model.momentum_bn], acts_afterpooling_list,
                                           updates=[], on_unused_input='ignore',
                                           givens={self.model.x: shared_dat_valid[
                                                                 index * self.batch_size: (
                                                                                              index + 1) * self.batch_size,
                                                                 :,
                                                                 :, :],
                                                   self.model.y: shared_label_valid[
                                                                 index * self.batch_size: (
                                                                                              index + 1) * self.batch_size]})

        get_acts_unpooled_afterBN = theano.function([index, self.model.is_train, self.model.momentum_bn],
                                                acts_unpooled_afterBN_list,
                                                updates=[], on_unused_input='ignore',
                                                givens={self.model.x: shared_dat_valid[
                                                                      index * self.batch_size: (
                                                                                                   index + 1) * self.batch_size,
                                                                      :,
                                                                      :, :],
                                                        self.model.y: shared_label_valid[
                                                                      index * self.batch_size: (
                                                                                                   index + 1) * self.batch_size]})

        get_data_reconstructed = theano.function([index, self.model.is_train, self.model.momentum_bn],
                                                  data_reconstructed_list,
                                                  updates=[], on_unused_input='ignore',
                                                  givens={self.model.x: shared_dat_valid[
                                                                        index * self.batch_size: (
                                                                                                     index + 1) * self.batch_size,
                                                                        :,
                                                                        :, :],
                                                          self.model.y: shared_label_valid[
                                                                        index * self.batch_size: (
                                                                                                     index + 1) * self.batch_size]})

        # get_grads = theano.function([index, self.model.is_train, self.model.momentum_bn], self.model.grads,
        #                                 updates=[], on_unused_input='ignore',
        #                                 givens={self.model.x: shared_dat[
        #                                                       index * self.batch_size: (index + 1) * self.batch_size, :,
        #                                                       :, :],
        #                                         self.model.y: shared_label[
        #                                                       index * self.batch_size: (index + 1) * self.batch_size]})

        # debug_one_EG1 = theano.function([index, self.model.is_train, self.model.momentum_bn],
        #                                [self.model.conv5.final_a_mask, self.model.conv4.final_a_mask,
        #                                 self.model.conv3.final_a_mask, self.model.conv2.final_a_mask,
        #                                 self.model.conv1.final_a_mask],
        #                                   updates=[], on_unused_input='ignore',
        #                                   givens={self.model.x: shared_dat[index * self.batch_size: (index + 1) * self.batch_size,:,:,:],
        #                                           self.model.y: shared_label[index * self.batch_size: (index + 1) * self.batch_size]})
        #


        # Initialize vectors and matrices that hold the training results
        dLA_vec = []
        dLA_mat = []
        amps_val_mat = []
        NegLogLs_vec = []
        unsupervisedNegLogLs_vec = []
        supervisedNegLogLs_vec = []
        epoch_vec = []
        epoch = 0
        epoch_vec.append(epoch)

        mean_acts_beforeBN_vec = []
        std_acts_beforeBN_vec = []
        min_acts_beforeBN_vec = []
        max_acts_beforeBN_vec = []

        mean_acts_afterBN_vec = []
        std_acts_afterBN_vec = []
        min_acts_afterBN_vec = []
        max_acts_afterBN_vec = []

        mean_acts_afterpooling_vec = []
        std_acts_afterpooling_vec = []
        min_acts_afterpooling_vec = []
        max_acts_afterpooling_vec = []

        mean_acts_unpooled_afterBN_vec = []
        std_acts_unpooled_afterBN_vec = []
        min_acts_unpooled_afterBN_vec = []
        max_acts_unpooled_afterBN_vec = []

        mean_data_reconstructed_vec = []
        std_data_reconstructed_vec = []
        min_data_reconstructed_vec = []
        max_data_reconstructed_vec = []

        best_validation_loss = np.inf
        test_score_vec = []
        validation_score_vec = []
        iter = 0

        lambdas_val = []
        amps_val = []

        # Note that for the initial values, we don't save initial betas since it can be calculated from lambdas
        # For later epoch, we save betas although it can still be calculated from lambdas
        for i in xrange(num_layer):
            lambdas_val.append(np.asarray(self.model.layers[i].lambdas.get_value(), dtype=theano.config.floatX))
            amps_val.append(np.asarray(self.model.layers[i].amps.get_value(), dtype=theano.config.floatX))

            dLA_vec.append([])
            dLA_mat.append([])
            amps_val_mat.append([])
            amps_val_mat[i].append(amps_val[i])

        # Compute initial validation error
        validation_losses = [validate_model(i, 0, 1.0) for i
                             in xrange(n_valid_batches)]
        this_validation_loss = np.mean(validation_losses)
        print('Inital validation score = %f' % this_validation_loss)
        validation_score_vec.append(this_validation_loss)

        # Compute inital test error
        test_losses = [test_model(i, 0, 1.0) for i in xrange(n_test_batches)]
        test_score_epoch = np.mean(test_losses)
        print('Initial test score = %f' % test_score_epoch)
        test_score_vec.append(test_score_epoch)

        # Save initial parameters
        meta_dir = os.path.join(self.param_dir, 'model_and_training_meta_params.npz')
        np.savez(meta_dir, lr_init=lr_init, lr_final=lr_final, max_epochs=max_epochs, momentum_bn=momentum_bn,
                 batch_size=self.batch_size,
                 noise_std=self.model.noise_std, noise_weights=self.model.noise_weights,
                 reconst_weights=self.model.reconst_weights, is_bn_BU=self.model.is_bn_BU, is_bn_TD=self.model.is_bn_TD,
                 em_mode=self.model.em_mode, train_mode=self.model.train_mode, denoising=self.model.denoising,
                 grad_min=self.model.grad_min, grad_max=self.model.grad_max, is_tied_bn=self.model.is_tied_bn,
                 top_down_mode=self.model.top_down_mode, is_relu=self.model.is_relu, nonlin=self.model.nonlin)

        model_dir = os.path.join(self.param_dir, 'model_epoch_%i.pkl' % epoch)
        model_file = open(model_dir, 'wb')
        cPickle.dump(self.model, model_file)
        model_file.close()

        model_dir = os.path.join(self.param_dir, 'model_epoch_%i.zip' % epoch)
        model_file = open(model_dir, 'wb')
        dump(self.model, model_file)
        model_file.close()

        # Compute the initial NLLs
        newTotalNLL = 0
        newUnsupervisedNLL = 0
        newSupervisedNLL = 0

        mean_acts_beforeBN_val = np.zeros((num_layer,))
        std_acts_beforeBN_val = np.zeros((num_layer,))
        min_acts_beforeBN_val = np.zeros((num_layer,))
        max_acts_beforeBN_val = np.zeros((num_layer,))

        mean_acts_afterBN_val = np.zeros((num_layer,))
        std_acts_afterBN_val = np.zeros((num_layer,))
        min_acts_afterBN_val = np.zeros((num_layer,))
        max_acts_afterBN_val = np.zeros((num_layer,))

        mean_acts_afterpooling_val = np.zeros((num_layer,))
        std_acts_afterpooling_val = np.zeros((num_layer,))
        min_acts_afterpooling_val = np.zeros((num_layer,))
        max_acts_afterpooling_val = np.zeros((num_layer,))

        mean_acts_unpooled_afterBN_val = np.zeros((num_layer,))
        std_acts_unpooled_afterBN_val = np.zeros((num_layer,))
        min_acts_unpooled_afterBN_val = np.zeros((num_layer,))
        max_acts_unpooled_afterBN_val = np.zeros((num_layer,))

        mean_data_reconstructed_val = np.zeros((num_layer,))
        std_data_reconstructed_val = np.zeros((num_layer,))
        min_data_reconstructed_val = np.zeros((num_layer,))
        max_data_reconstructed_val = np.zeros((num_layer,))

        for minibatch_index in xrange(n_batches):
            totalNLL_val, unsupervisedNLL_val, supervisedNLL_val = compute_NLL(minibatch_index, 0, 1.0)
            newTotalNLL = newTotalNLL + totalNLL_val
            newUnsupervisedNLL = newUnsupervisedNLL + unsupervisedNLL_val
            newSupervisedNLL = newSupervisedNLL + supervisedNLL_val

        newTotalNLL = newTotalNLL / n_batches
        NegLogLs_vec.append(newTotalNLL)

        newUnsupervisedNLL = newUnsupervisedNLL / n_batches
        unsupervisedNegLogLs_vec.append(newUnsupervisedNLL)

        newSupervisedNLL = newSupervisedNLL / n_batches
        supervisedNegLogLs_vec.append(newSupervisedNLL)

        for minibatch_index in xrange(1):
            acts_beforeBN_val = get_acts_beforeBN(minibatch_index,0,1.0)
            for i in xrange(num_layer):
                mean_acts_beforeBN_val[i] += np.mean(acts_beforeBN_val[i])
                std_acts_beforeBN_val[i] += np.std(acts_beforeBN_val[i])
                min_acts_beforeBN_val[i] += np.min(acts_beforeBN_val[i])
                max_acts_beforeBN_val[i] += np.max(acts_beforeBN_val[i])
            del acts_beforeBN_val

            acts_afterBN_val = get_acts_afterBN(minibatch_index, 0, 1.0)
            for i in xrange(num_layer):
                mean_acts_afterBN_val[i] += np.mean(acts_afterBN_val[i])
                std_acts_afterBN_val[i] += np.std(acts_afterBN_val[i])
                min_acts_afterBN_val[i] += np.min(acts_afterBN_val[i])
                max_acts_afterBN_val[i] += np.max(acts_afterBN_val[i])
            del acts_afterBN_val

            acts_afterpooling_val = get_acts_afterpooling(minibatch_index, 0, 1.0)
            for i in xrange(num_layer):
                mean_acts_afterpooling_val[i] += np.mean(acts_afterpooling_val[i])
                std_acts_afterpooling_val[i] += np.std(acts_afterpooling_val[i])
                min_acts_afterpooling_val[i] += np.min(acts_afterpooling_val[i])
                max_acts_afterpooling_val[i] += np.max(acts_afterpooling_val[i])
            del acts_afterpooling_val

            acts_unpooled_afterBN_val = get_acts_unpooled_afterBN(minibatch_index, 0, 1.0)
            for i in xrange(num_layer):
                mean_acts_unpooled_afterBN_val[i] += np.mean(acts_unpooled_afterBN_val[i])
                std_acts_unpooled_afterBN_val[i] += np.std(acts_unpooled_afterBN_val[i])
                min_acts_unpooled_afterBN_val[i] += np.min(acts_unpooled_afterBN_val[i])
                max_acts_unpooled_afterBN_val[i] += np.max(acts_unpooled_afterBN_val[i])
            del acts_unpooled_afterBN_val

            data_reconstructed_val = get_data_reconstructed(minibatch_index, 0, 1.0)
            for i in xrange(num_layer):
                mean_data_reconstructed_val[i] += np.mean(data_reconstructed_val[i])
                std_data_reconstructed_val[i] += np.std(data_reconstructed_val[i])
                min_data_reconstructed_val[i] += np.min(data_reconstructed_val[i])
                max_data_reconstructed_val[i] += np.max(data_reconstructed_val[i])
            del data_reconstructed_val

        mean_acts_beforeBN_vec.append(mean_acts_beforeBN_val)
        std_acts_beforeBN_vec.append(std_acts_beforeBN_val)
        min_acts_beforeBN_vec.append(min_acts_beforeBN_val)
        max_acts_beforeBN_vec.append(max_acts_beforeBN_val)

        mean_acts_afterBN_vec.append(mean_acts_afterBN_val)
        std_acts_afterBN_vec.append(std_acts_afterBN_val)
        min_acts_afterBN_vec.append(min_acts_afterBN_val)
        max_acts_afterBN_vec.append(max_acts_afterBN_val)

        mean_acts_afterpooling_vec.append(mean_acts_afterpooling_val)
        std_acts_afterpooling_vec.append(std_acts_afterpooling_val)
        min_acts_afterpooling_vec.append(min_acts_afterpooling_val)
        max_acts_afterpooling_vec.append(max_acts_afterpooling_val)

        mean_acts_unpooled_afterBN_vec.append(mean_acts_unpooled_afterBN_val)
        std_acts_unpooled_afterBN_vec.append(std_acts_unpooled_afterBN_val)
        min_acts_unpooled_afterBN_vec.append(min_acts_unpooled_afterBN_val)
        max_acts_unpooled_afterBN_vec.append(max_acts_unpooled_afterBN_val)

        mean_data_reconstructed_vec.append(mean_data_reconstructed_val)
        std_data_reconstructed_vec.append(std_data_reconstructed_val)
        min_data_reconstructed_vec.append(min_data_reconstructed_val)
        max_data_reconstructed_vec.append(max_data_reconstructed_val)

        print('Initial UnsupervisedNLL=', newUnsupervisedNLL)
        print('Initial SupervisedNLL=', newSupervisedNLL)
        print('Initial TotalNLL=', newTotalNLL)

        done_looping = False

        patience = 20000  # look as this many examples regardless
        patience_increase = 3  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.999  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        decay_val = np.exp(np.log(lr_init / lr_final) / (max_epochs - 1))
        current_lr = lr_init * decay_val

        while (epoch < max_epochs) and (not done_looping):
            epoch = epoch + 1

            LA = []
            newLA = []

            # Compute the Lambdas before epoch i
            for layer in self.model.layers:
                LA.append(copy.copy(layer.lambdas.get_value()))

            # start the training for epoch i

            current_lr = np.float32(current_lr / decay_val)
            print('Epoch %d with Learning Rate = %f' % (epoch, current_lr))

            for minibatch_index in xrange(n_batches):
                # print(minibatch_index)
                iter = iter + 1
                # print('iter %i' %iter)
                # # all_grads = debug_one_EG(minibatch_index)
                # grads_val = get_grads(minibatch_index, 0, 1)
                # [final_a5, final_a4, final_a3, final_a2, final_a1] = debug_one_EG1(minibatch_index, 0, 1)
                # [zBU5, zBU4, zBU3, zBU2, zBU1] = debug_one_EG2(minibatch_index, 0, 1)
                # [zTD5, zTD4, zTD3, zTD2, zTD1] = debug_one_EG3(minibatch_index, 0, 1)
                # for i in xrange(num_layer):
                #     if np.any(np.isnan(grads_val[i])) or np.any(np.isinf(grads_val[i])):
                #         import pdb;pdb.set_trace()

                # print('Shape of data reconstructed')
                # print(np.shape(dat_reconst5))
                # print(np.shape(dat_reconst4))
                # print(np.shape(dat_reconst3))
                # print(np.shape(dat_reconst2))
                # print(np.shape(dat_reconst1))
                # print('finsih printing of data reconstructed')
                output_val = do_one_iter(minibatch_index, current_lr, 1, momentum_bn)
                # update_gamma()

                # # for debugging grads and lambdas
                # for grad in all_grads:
                #     grad = np.asarray(grad, dtype=theano.config.floatX)
                #     if np.any(np.isinf(grad)):
                #         pdb.set_trace()
                #     if np.any(np.isnan(grad)):
                #         pdb.set_trace()
                #     if np.any(grad > 100.0):
                #         pdb.set_trace()
                #
                # for i in xrange(num_layer):
                #     lam = np.asarray(output_val[2 + 4*i], dtype=theano.config.floatX)
                #     if np.any(np.isnan(lam)):
                #         pdb.set_trace()

                if iter % validation_frequency == 0:
                    validation_losses = [validate_model(i, 0, 1.0) for i
                                     in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)
                    print('epoch %i, minibatch %i/%i, validation error %f %%' % \
                          (epoch, minibatch_index + 1, n_batches, \
                           this_validation_loss * 100.))

                    validation_score_vec.append(this_validation_loss)

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:

                        #improve patience if loss improvement is good enough
                        if this_validation_loss < best_validation_loss *  \
                           improvement_threshold:
                            patience = max(patience, iter * patience_increase)

                if patience <= iter:
                    done_looping = True
                    break

            test_losses = [test_model(i, 0, 1.0) for i in xrange(n_test_batches)]
            test_score_epoch = np.mean(test_losses)
            print('Test score at epoch %i = %f' %(epoch, test_score_epoch))

            if epoch % 1 == 1:
                misc_dir = os.path.join(self.param_dir, 'misc_params_epoch_%i.npz' %epoch)
                np.savez(misc_dir, learning_rate=current_lr)

                model_dir = os.path.join(self.param_dir, 'model_epoch_%i.zip' %epoch)
                model_file = open(model_dir, 'wb')
                dump(self.model, model_file)
                model_file.close()

                model_dir = os.path.join(self.param_dir, 'model_epoch_%i.pkl' % epoch)
                model_file = open(model_dir, 'wb')
                cPickle.dump(self.model, model_file)
                model_file.close()

            misc_dir = os.path.join(self.param_dir, 'misc_params_latest.npz')
            np.savez(misc_dir, learning_rate=current_lr)

            model_dir = os.path.join(self.param_dir, 'model_latest.zip')
            model_file = open(model_dir, 'wb')
            dump(self.model, model_file)
            model_file.close()

            model_dir = os.path.join(self.param_dir, 'model_latest.pkl')
            model_file = open(model_dir, 'wb')
            cPickle.dump(self.model, model_file)
            model_file.close()

            if test_score_epoch < np.min(test_score_vec):
                misc_dir = os.path.join(self.param_dir, 'misc_params_best.npz')
                np.savez(misc_dir, learning_rate=current_lr)

                model_dir = os.path.join(self.param_dir, 'model_best.zip')
                model_file = open(model_dir, 'wb')
                dump(self.model, model_file)
                model_file.close()

                model_dir = os.path.join(self.param_dir, 'model_best.pkl')
                model_file = open(model_dir, 'wb')
                cPickle.dump(self.model, model_file)
                model_file.close()

            test_score_vec.append(test_score_epoch)
            epoch_vec.append(epoch)

            fig = figure()
            plot(epoch_vec, test_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Test-Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.output_dir, 'Test-Error-vs-Epoch-EG.png'))
            close(fig)

            fig = figure()
            plot(epoch_vec, validation_score_vec)
            legend()
            xlabel('Epoch')
            ylabel('Error Rate')
            title('Validation-Error-vs-Epoch-EG')
            fig.savefig(os.path.join(self.output_dir, 'Validation-Error-vs-Epoch-EG.png'))
            close(fig)

            plot_classification_file = os.path.join(self.output_dir, 'plot_classification.npz')
            np.savez(plot_classification_file, epoch_vec=epoch_vec, test_score_vec=test_score_vec,
                     validation_score_vec=validation_score_vec)

            print('Best test score of %f %% obtained at epoch %i' % (np.min(test_score_vec) * 100., np.argmin(test_score_vec) + 1))

            if epoch == 1:
                legend_misc = range(np.shape(output_val[2])[0])
                for i in xrange(len(legend_misc)):
                    legend_misc[i] = str(legend_misc[i])
                legend_misc.append('average')

            # Compute the NLLs at epoch i and plot the NLLs
            if epoch % num_epoch_to_plot_NLL == 0:
                # Compute the initial NLLs
                newTotalNLL = 0
                newUnsupervisedNLL = 0
                newSupervisedNLL = 0

                mean_acts_beforeBN_val = np.zeros((num_layer,))
                std_acts_beforeBN_val = np.zeros((num_layer,))
                min_acts_beforeBN_val = np.zeros((num_layer,))
                max_acts_beforeBN_val = np.zeros((num_layer,))

                mean_acts_afterBN_val = np.zeros((num_layer,))
                std_acts_afterBN_val = np.zeros((num_layer,))
                min_acts_afterBN_val = np.zeros((num_layer,))
                max_acts_afterBN_val = np.zeros((num_layer,))

                mean_acts_afterpooling_val = np.zeros((num_layer,))
                std_acts_afterpooling_val = np.zeros((num_layer,))
                min_acts_afterpooling_val = np.zeros((num_layer,))
                max_acts_afterpooling_val = np.zeros((num_layer,))

                mean_acts_unpooled_afterBN_val = np.zeros((num_layer,))
                std_acts_unpooled_afterBN_val = np.zeros((num_layer,))
                min_acts_unpooled_afterBN_val = np.zeros((num_layer,))
                max_acts_unpooled_afterBN_val = np.zeros((num_layer,))

                mean_data_reconstructed_val = np.zeros((num_layer,))
                std_data_reconstructed_val = np.zeros((num_layer,))
                min_data_reconstructed_val = np.zeros((num_layer,))
                max_data_reconstructed_val = np.zeros((num_layer,))

                for minibatch_index in xrange(n_batches):
                    totalNLL_val, unsupervisedNLL_val, supervisedNLL_val = compute_NLL(minibatch_index, 0, 1.0)
                    newTotalNLL = newTotalNLL + totalNLL_val
                    newUnsupervisedNLL = newUnsupervisedNLL + unsupervisedNLL_val
                    newSupervisedNLL = newSupervisedNLL + supervisedNLL_val

                newTotalNLL = newTotalNLL / n_batches
                NegLogLs_vec.append(newTotalNLL)

                newUnsupervisedNLL = newUnsupervisedNLL / n_batches
                unsupervisedNegLogLs_vec.append(newUnsupervisedNLL)

                newSupervisedNLL = newSupervisedNLL / n_batches
                supervisedNegLogLs_vec.append(newSupervisedNLL)

                for minibatch_index in xrange(n_valid_batches):
                    acts_beforeBN_val = get_acts_beforeBN(minibatch_index, 0, 1.0)
                    for i in xrange(num_layer):
                        mean_acts_beforeBN_val[i] += np.mean(acts_beforeBN_val[i])
                        std_acts_beforeBN_val[i] += np.std(acts_beforeBN_val[i])
                        min_acts_beforeBN_val[i] += np.min(acts_beforeBN_val[i])
                        max_acts_beforeBN_val[i] += np.max(acts_beforeBN_val[i])
                    del acts_beforeBN_val

                    acts_afterBN_val = get_acts_afterBN(minibatch_index, 0, 1.0)
                    for i in xrange(num_layer):
                        mean_acts_afterBN_val[i] += np.mean(acts_afterBN_val[i])
                        std_acts_afterBN_val[i] += np.std(acts_afterBN_val[i])
                        min_acts_afterBN_val[i] += np.min(acts_afterBN_val[i])
                        max_acts_afterBN_val[i] += np.max(acts_afterBN_val[i])
                    del acts_afterBN_val

                    acts_afterpooling_val = get_acts_afterpooling(minibatch_index, 0, 1.0)
                    for i in xrange(num_layer):
                        mean_acts_afterpooling_val[i] += np.mean(acts_afterpooling_val[i])
                        std_acts_afterpooling_val[i] += np.std(acts_afterpooling_val[i])
                        min_acts_afterpooling_val[i] += np.min(acts_afterpooling_val[i])
                        max_acts_afterpooling_val[i] += np.max(acts_afterpooling_val[i])
                    del acts_afterpooling_val

                    acts_unpooled_afterBN_val = get_acts_unpooled_afterBN(minibatch_index, 0, 1.0)
                    for i in xrange(num_layer):
                        mean_acts_unpooled_afterBN_val[i] += np.mean(acts_unpooled_afterBN_val[i])
                        std_acts_unpooled_afterBN_val[i] += np.std(acts_unpooled_afterBN_val[i])
                        min_acts_unpooled_afterBN_val[i] += np.min(acts_unpooled_afterBN_val[i])
                        max_acts_unpooled_afterBN_val[i] += np.max(acts_unpooled_afterBN_val[i])
                    del acts_unpooled_afterBN_val

                    data_reconstructed_val = get_data_reconstructed(minibatch_index, 0, 1.0)
                    for i in xrange(num_layer):
                        mean_data_reconstructed_val[i] += np.mean(data_reconstructed_val[i])
                        std_data_reconstructed_val[i] += np.std(data_reconstructed_val[i])
                        min_data_reconstructed_val[i] += np.min(data_reconstructed_val[i])
                        max_data_reconstructed_val[i] += np.max(data_reconstructed_val[i])
                    del data_reconstructed_val

                mean_acts_beforeBN_val /= n_valid_batches
                std_acts_beforeBN_val /= n_valid_batches
                min_acts_beforeBN_val /= n_valid_batches
                max_acts_beforeBN_val /= n_valid_batches

                mean_acts_afterBN_val /= n_valid_batches
                std_acts_afterBN_val /= n_valid_batches
                min_acts_afterBN_val /= n_valid_batches
                max_acts_afterBN_val /= n_valid_batches

                mean_acts_afterpooling_val /= n_valid_batches
                std_acts_afterpooling_val /= n_valid_batches
                min_acts_afterpooling_val /= n_valid_batches
                max_acts_afterpooling_val /= n_valid_batches

                mean_acts_unpooled_afterBN_val /= n_valid_batches
                std_acts_unpooled_afterBN_val /= n_valid_batches
                min_acts_unpooled_afterBN_val /= n_valid_batches
                max_acts_unpooled_afterBN_val /= n_valid_batches

                mean_data_reconstructed_val /= n_valid_batches
                std_data_reconstructed_val /= n_valid_batches
                min_data_reconstructed_val /= n_valid_batches
                max_data_reconstructed_val /= n_valid_batches

                mean_acts_beforeBN_vec.append(mean_acts_beforeBN_val)
                std_acts_beforeBN_vec.append(std_acts_beforeBN_val)
                min_acts_beforeBN_vec.append(min_acts_beforeBN_val)
                max_acts_beforeBN_vec.append(max_acts_beforeBN_val)

                mean_acts_afterBN_vec.append(mean_acts_afterBN_val)
                std_acts_afterBN_vec.append(std_acts_afterBN_val)
                min_acts_afterBN_vec.append(min_acts_afterBN_val)
                max_acts_afterBN_vec.append(max_acts_afterBN_val)

                mean_acts_afterpooling_vec.append(mean_acts_afterpooling_val)
                std_acts_afterpooling_vec.append(std_acts_afterpooling_val)
                min_acts_afterpooling_vec.append(min_acts_afterpooling_val)
                max_acts_afterpooling_vec.append(max_acts_afterpooling_val)

                mean_acts_unpooled_afterBN_vec.append(mean_acts_unpooled_afterBN_val)
                std_acts_unpooled_afterBN_vec.append(std_acts_unpooled_afterBN_val)
                min_acts_unpooled_afterBN_vec.append(min_acts_unpooled_afterBN_val)
                max_acts_unpooled_afterBN_vec.append(max_acts_unpooled_afterBN_val)

                mean_data_reconstructed_vec.append(mean_data_reconstructed_val)
                std_data_reconstructed_vec.append(std_data_reconstructed_val)
                min_data_reconstructed_vec.append(min_data_reconstructed_val)
                max_data_reconstructed_vec.append(max_data_reconstructed_val)

                print('UnsupervisedNLL=', newUnsupervisedNLL)
                print('SupervisedNLL=', newSupervisedNLL)
                print('TotalNLL=', newTotalNLL)

                fig1 = figure()
                plot(epoch_vec[0:epoch+1:num_epoch_to_plot_NLL], NegLogLs_vec)
                xlabel('Epoch')
                ylabel('Training Cost')
                title('Convergence of Training Cost')
                fig1.savefig(
                    os.path.join(self.output_dir,
                                 'Convergence_of_Training_Cost.png'))
                close()

                fig2 = figure()
                plot(epoch_vec[0:epoch+1:num_epoch_to_plot_NLL], unsupervisedNegLogLs_vec)
                xlabel('Epoch')
                ylabel('Weighted Reconstruction Cost')
                title('Convergence of Weighted Reconstruction Cost')
                fig2.savefig(
                    os.path.join(self.output_dir,
                                 'Convergence_of_Weighted_Reconstruction_Cost.png'))
                close()

                fig3 = figure()
                plot(epoch_vec[0:epoch+1:num_epoch_to_plot_NLL], supervisedNegLogLs_vec)
                xlabel('Epoch')
                ylabel('Cross Entropy')
                title('Convergence of Cross Entropy')
                fig3.savefig(
                    os.path.join(self.output_dir,
                                 'Convergence_of_Cross_Entropy.png'))
                close()

                features = [mean_acts_beforeBN_vec, std_acts_beforeBN_vec, min_acts_beforeBN_vec, max_acts_beforeBN_vec,
                            mean_acts_afterBN_vec, std_acts_afterBN_vec, min_acts_afterBN_vec, max_acts_afterBN_vec,
                            mean_acts_afterpooling_vec, std_acts_afterpooling_vec, min_acts_afterpooling_vec,
                            max_acts_afterpooling_vec,
                            mean_acts_unpooled_afterBN_vec, std_acts_unpooled_afterBN_vec,
                            min_acts_unpooled_afterBN_vec, max_acts_unpooled_afterBN_vec,
                            mean_data_reconstructed_vec, std_data_reconstructed_vec, min_data_reconstructed_vec,
                            max_data_reconstructed_vec]

                names = ['mean_acts_beforeBN_vec', 'std_acts_beforeBN_vec', 'min_acts_beforeBN_vec',
                         'max_acts_beforeBN_vec',
                         'mean_acts_afterBN_vec', 'std_acts_afterBN_vec', 'min_acts_afterBN_vec',
                         'max_acts_afterBN_vec',
                         'mean_acts_afterpooling_vec', 'std_acts_afterpooling_vec', 'min_acts_afterpooling_vec',
                         'max_acts_afterpooling_vec',
                         'mean_acts_unpooled_afterBN_vec', 'std_acts_unpooled_afterBN_vec',
                         'min_acts_unpooled_afterBN_vec',
                         'max_acts_unpooled_afterBN_vec',
                         'mean_data_reconstructed_vec', 'std_data_reconstructed_vec', 'min_data_reconstructed_vec',
                         'max_data_reconstructed_vec']
                for i in xrange(len(features)):
                    self.plot_across_all_layers(epoch_vec=epoch_vec[0:epoch+1:num_epoch_to_plot_NLL], feature=features[i], legend_names=legend_misc,
                                                name=names[i])

            # Output parameters and plot the results
            lambdas_val = []
            betas_val = []
            amps_val = []

            for i in xrange(num_layer):
                lambdas_val.append(np.asarray(output_val[2 + 4*i],dtype=theano.config.floatX))
                betas_val.append(np.asarray(output_val[1 + 4*i],dtype=theano.config.floatX))
                amps_val.append(np.asarray(output_val[3 + 4*i],dtype=theano.config.floatX))

                # Compute the relative difference in Lambdas
                newLA.append(output_val[2 + 4 * i])
                dLA_vec[i].append(norm(newLA[i] - LA[i]) / norm(LA[i]))
                dLA_mat[i].append(norm(newLA[i][:, :, 0] - LA[i][:, :, 0], axis=1))

                # Record the amps_val into amps_val_mat
                amps_val_mat[i].append(amps_val[i])

                # Plot the relative difference in Lambdas
                fig4 = figure()
                plot(epoch_vec[1:], dLA_vec[i], 'bo-', label='Lambda_Layer%i' % (i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i' % (i + 1, self.batch_size))
                fig4.savefig(os.path.join(self.output_dir, 'Convergence_of_Parameters_Layer%i_BatchSize_%i.png' % (
                i + 1, self.batch_size)))
                close()

                fig5 = figure()
                plot(epoch_vec[2:], dLA_vec[i][1:], 'bo-', label='Lambda_Layer%i' % (i + 1))
                legend()
                xlabel('Epoch')
                ylabel('Relative Difference')
                title('Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1' % (i + 1, self.batch_size))
                fig5.savefig(os.path.join(self.output_dir,
                                          'Convergence_of_Parameters_Layer%i_BatchSize_%i_StartEpoch1.png' % (
                                          i + 1, self.batch_size)))
                close()

                fig6 = figure()
                plot(dLA_mat[i])
                legend(legend_misc[0:-1], prop={'size': 8})
                xlabel('Epoch')
                ylabel('Difference')
                title('Convergence_of_Lambdas_Layer%i' % (i + 1))
                fig6.savefig(os.path.join(self.output_dir, 'Convergence_of_Lambdas_Layer%i.png' % (i + 1)))
                close()

                # Plot the amps_val into amps_val_mat

                fig7 = figure()
                plot(amps_val_mat[i])
                legend(legend_misc[0:-1],prop={'size':8})
                xlabel('Epoch')
                ylabel('Pi(cg)')
                title('Convergence_of_Pi_cg_Layer%i' % (i + 1))
                fig7.savefig(os.path.join(self.output_dir, 'Convergence_of_Pi_cg_Layer%i.png' % (i + 1)))
                close()

            forPlot_file = os.path.join(self.output_dir, 'for_Plotting.npz')
            np.savez(forPlot_file, epoch_vec=epoch_vec, NegLogLs_vec=NegLogLs_vec, dLA_vec=dLA_vec,
                     unsupervisedNegLogLs_vec=unsupervisedNegLogLs_vec, supervisedNegLogLs_vec=supervisedNegLogLs_vec,
                     dLA_mat=dLA_mat, amps_val_mat=amps_val_mat)

        if epoch < max_epochs - 1:
            if verbose:
                print("EM converged after {0} epochs".format(epoch))
                print("Final NLL={0}".format(newTotalNLL))
        else:
            print("Warning:EM didn't converge after {0} epochs".format(epoch))

        print('Optimization complete.')
        print('Best test score of %f %% obtained at epoch %i' % (np.min(test_score_vec) * 100., np.argmin(test_score_vec) + 1))

    def plot_across_all_layers(self, epoch_vec, feature, name, legend_names):
        fig = figure()
        plot(epoch_vec, np.asarray(feature))
        xlabel('Epoch')
        ylabel(name)
        legend(legend_names, prop={'size': 8})
        fig.savefig(os.path.join(self.output_dir, '%s.png'%name))
        close()