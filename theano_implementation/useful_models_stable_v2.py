import cPickle as pickle

import numpy as np
import theano
from theano import tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from nn_functions_stable_v2 import LogisticRegression, LogisticRegressionForSemisupervised, SoftmaxNonlinearity, SoftmaxNonlinearitySemisupervised, HiddenLayerInSoftmax, ConvLayerInSoftmax

from CRM_no_factor_stable_v2 import CRM, CRM_with_bias
from DRM_no_factor_stable_v2 import DRM_model


class MNIST_Conv_Small_5_Layers(DRM_model):
    '''
    Conv_Small that the Ladder Network uses for MNIST
    '''
    # TODO: factor out common init code from all models
    def __init__(self, batch_size, Cin, W, H, em_mode, seed, param_dir=[], train_mode='supervised',
                 reconst_weights=[0.0, 0.0, 0.0, 0.0, 0.0], init_Bengio=False, grad_min=-np.inf, grad_max=np.inf,
                 init_params=False, denoising='simple', noise_std=0.45, noise_weights=[0.0, 0.0, 0.0, 0.0, 0.0],
                 is_bn_BU=False, is_bn_TD=False, top_down_mode=None, is_relu=False, nonlin='relu'):

        print('Your model is MNIST_Conv_Small_5_Layers')

        self.noise_std = noise_std
        self.noise_weights = noise_weights
        self.reconst_weights = reconst_weights

        self.is_bn_BU = is_bn_BU
        self.is_bn_TD = is_bn_TD

        self.em_mode = em_mode
        self.batch_size = batch_size
        self.train_mode = train_mode
        self.denoising = denoising
        self.grad_min = grad_min
        self.grad_max = grad_max

        self.top_down_mode = top_down_mode
        self.is_relu = is_relu
        self.nonlin = nonlin

        is_noisy = [False] * len(self.noise_weights)
        for i in xrange(len(is_noisy)):
            if np.sum(self.noise_weights[0:i + 1]) > 0.0:
                is_noisy[i] = True

        print('noise_weights is:')
        print(self.noise_weights)
        print('is_noisy is:')
        print(is_noisy)

        # Initialize params
        if init_params:
            print('Initialize params using the params stored at:')
            print(param_dir)
            pkl_file = open(param_dir, 'rb')
            params = pickle.load(pkl_file)
            lambdas_val_init = params['lambdas_val']
            amps_val_init = params['amps_val']
            pkl_file.close()
        else:
            print('Randomly initialize params')
            lambdas_val_init = [None] * len(self.noise_weights)
            amps_val_init = [None] * len(self.noise_weights)

        # Build the model

        self.H1 = H  # 28
        self.W1 = W  # 28
        self.Cin1 = Cin  # 1

        self.h1 = 5
        self.w1 = 5
        self.K1 = 32
        self.M1 = 1

        self.H2 = (self.H1 + self.h1 - 1) / 2  # 16
        self.W2 = (self.W1 + self.w1 - 1) / 2  # 16
        self.Cin2 = self.K1  # 32

        self.h2 = 3
        self.w2 = 3
        self.K2 = 64
        self.M2 = 1

        self.H3 = self.H2  # 16
        self.W3 = self.W2  # 16
        self.Cin3 = self.K2  # 64

        self.h3 = 3
        self.w3 = 3
        self.K3 = 64
        self.M3 = 1

        self.H4 = (self.H3) / 2  # 8
        self.W4 = (self.W3) / 2  # 8
        self.Cin4 = self.K3  # 64

        self.h4 = 3
        self.w4 = 3
        self.K4 = 128
        self.M4 = 1

        self.H5 = self.H4 - self.h4 + 1  # 6
        self.W5 = self.W4 - self.w4 + 1  # 6
        self.Cin5 = self.K4  # 128

        self.h5 = 1
        self.w5 = 1
        self.K5 = 10
        self.M5 = 1

        self.H_Softmax = 1
        self.W_Softmax = 1
        self.Cin_Softmax = self.K5  # 10

        self.h_Softmax = 1
        self.w_Softmax = 1
        self.K_Softmax = 10
        self.M_Softmax = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

        self.srng = RandomStreams()
        self.srng.seed(np.random.randint(2 ** 15))

        # Forward Step
        self.conv1 = CRM(
            data_4D=self.x
                    + self.noise_weights[0] * self.srng.normal(size=(self.batch_size, self.Cin1, self.H1, self.W1), avg=0.0, std=self.noise_std),
            data_4D_clean=self.x,
            labels=self.y,
            is_train = self.is_train, momentum_bn=self.momentum_bn,
            K=self.K1, M=self.M1, W=self.W1, H=self.H1,
            w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
            amps_val_init=amps_val_init[4], lambdas_val_init=lambdas_val_init[4],
            PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
            pool_t_mode='max_t',
            border_mode='full',
            rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
            is_noisy=is_noisy[0], is_bn_BU=self.is_bn_BU, is_bn_TD=False, nonlin=self.nonlin)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D=self.conv1.output
                                 + self.noise_weights[1] * self.srng.normal(size=(self.batch_size, self.Cin2, self.H2, self.W2), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv1.output_clean,
                         labels=self.y,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K2, M=self.M2, W=self.W2, H=self.H2,
                         w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=amps_val_init[3], lambdas_val_init=lambdas_val_init[3],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='half',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[1], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin,
                         is_res='conv')

        self.conv2._E_step_Bottom_Up()

        self.conv3 = CRM(data_4D=self.conv2.output
                                 + self.noise_weights[2] * self.srng.normal(size=(self.batch_size, self.Cin3, self.H3, self.W3), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv2.output_clean,
                         labels=self.y,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K3, M=self.M3, W=self.W3, H=self.H3,
                         w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
                         amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='max_t',
                         border_mode='half',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[2], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin,
                         is_res='identity')

        self.conv3._E_step_Bottom_Up()

        self.conv4 = CRM(data_4D=self.conv3.output
                                 + self.noise_weights[3] * self.srng.normal(size=(self.batch_size, self.Cin4, self.H4, self.W4), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv3.output_clean,
                         labels=self.y,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K4, M=self.M4, W=self.W4, H=self.H4,
                         w=self.w4, h=self.h4, Cin=self.Cin4, Ni=self.batch_size,
                         amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[3], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin)

        self.conv4._E_step_Bottom_Up()

        self.conv5 = CRM(data_4D=self.conv4.output
                                 + self.noise_weights[4] * self.srng.normal(size=(self.batch_size, self.Cin5, self.H5, self.W5), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv4.output_clean,
                         labels=self.y,
                         is_train = self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K5, M=self.M5, W=self.W5, H=self.H5,
                         w=self.w5, h=self.h5, Cin=self.Cin5, Ni=self.batch_size,
                         amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='mean_t',
                         border_mode='valid', mean_pool_size=(6, 6),
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[4], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin,
                         is_res='identity')

        self.conv5._E_step_Bottom_Up()

        # self.RegressionInSoftmax = CRM_with_bias(data_4D=self.conv5.output
        #                                                  + 0.0 * self.srng.normal(size=(self.batch_size, self.Cin_Softmax, self.H_Softmax, self.W_Softmax), avg=0.0, std=self.noise_std),
        #                                          data_4D_clean=self.conv5.output_clean,
        #                                          labels=self.y,
        #                                          is_train=self.is_train, momentum_bn=self.momentum_bn,
        #                                          K=self.K_Softmax, M=self.M_Softmax, W=self.W_Softmax, H=self.H_Softmax,
        #                                          w=self.w_Softmax, h=self.h_Softmax, Cin=self.Cin_Softmax, Ni=self.batch_size,
        #                                          amps_val_init=None,
        #                                          lambdas_val_init=np.zeros((self.K_Softmax, self.Cin_Softmax*self.h_Softmax*self. w_Softmax, self.M_Softmax), dtype=theano.config.floatX),
        #                                          b_val_init=None,
        #                                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
        #                                          pool_t_mode=None, pool_a_mode=None,
        #                                          border_mode='valid',
        #                                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
        #                                          is_noisy=None, is_bn_BU=False, is_bn_TD=self.is_bn_TD)

        # self.RegressionInSoftmax = CRM(data_4D=self.conv5.output
        #                          + self.noise_weights[5] * self.srng.normal(
        #     size=(self.batch_size, self.Cin_Softmax, self.H_Softmax, self.W_Softmax), avg=0.0, std=self.noise_std),
        #                  data_4D_clean=self.conv5.output_clean,
        #                  labels=self.y,
        #                  is_train=self.is_train, momentum_bn=self.momentum_bn,
        #                  K=self.K_Softmax, M=self.M_Softmax, W=self.W_Softmax, H=self.H_Softmax,
        #                  w=self.w_Softmax, h=self.h_Softmax, Cin=self.Cin_Softmax, Ni=self.batch_size,
        #                  amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
        #                  PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
        #                  pool_t_mode=None, pool_a_mode=None,
        #                  border_mode='valid',
        #                  rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
        #                  is_noisy=is_noisy[5], is_bn_BU=False, is_bn_TD=self.is_bn_TD)

        # self.RegressionInSoftmax._E_step_Bottom_Up()

        self.RegressionInSoftmax = HiddenLayerInSoftmax(input=self.conv5.output.flatten(2), n_in=self.Cin_Softmax, n_out=self.K_Softmax,
                                          W_init=None, b_init=None, input_clean=self.conv5.output_clean.flatten(2))

        # self.RegressionInSoftmax = ConvLayerInSoftmax(input=self.conv5.output, filter_shape=(self.K_Softmax, self.Cin_Softmax, 1, 1),
        #                                 image_shape=(self.batch_size, self.Cin_Softmax, self.H_Softmax, self.W_Softmax),
        #                                 W_init=None, b_init=None, input_clean=self.conv5.output_clean)

        softmax_input = self.RegressionInSoftmax.output
        softmax_input_clean = self.RegressionInSoftmax.output_clean

        # classify the values of the fully-connected sigmoidal layer
        if self.train_mode == 'semisupervised':
            self.softmax_layer_nonlin = SoftmaxNonlinearitySemisupervised(input=softmax_input, input_clean=softmax_input_clean)
        else:
            self.softmax_layer_nonlin = SoftmaxNonlinearity(input=softmax_input, input_clean=softmax_input_clean)

        # self.layers = [self.conv6, self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
        self.layers = [self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
        self.N_layer = len(self.layers)

        # build Top-Down pass
        self.Build_TopDown(top_down_mode=self.top_down_mode, is_relu=self.is_relu)
        # self.Build_One_Hot_Reconstruction()

        # build the cost function for the model
        self.Build_Cost()

        # build update rules for the model
        self.Build_Update_Rule()

########################################################################################################################
class CIFAR10_Conv_Large_9_Layers(DRM_model):
    '''
    Conv_Small that the Ladder Network uses for MNIST
    '''
    # TODO: factor out common init code from all models
    def __init__(self, batch_size, Cin, W, H, em_mode, seed, param_dir=[], train_mode='supervised',
                 reconst_weights=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], init_Bengio=False, grad_min=-np.inf, grad_max=np.inf,
                 init_params=False, denoising='simple', noise_std=0.45, noise_weights=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                 is_bn_BU=False, is_bn_TD=False, top_down_mode=None, is_relu=False, nonlin='relu'):

        print('Your model is CIFAR10_Conv_Large_9_Layers')

        self.noise_std = noise_std
        self.noise_weights = noise_weights
        self.reconst_weights = reconst_weights

        self.is_bn_BU = is_bn_BU
        self.is_bn_TD = is_bn_TD

        self.em_mode = em_mode
        self.batch_size = batch_size
        self.train_mode = train_mode
        self.denoising = denoising
        self.grad_min = grad_min
        self.grad_max = grad_max

        self.top_down_mode = top_down_mode
        self.is_relu = is_relu
        self.nonlin = nonlin

        is_noisy = [False] * len(self.noise_weights)
        for i in xrange(len(is_noisy)):
            if np.sum(self.noise_weights[0:i + 1]) > 0.0:
                is_noisy[i] = True

        print('noise_weights is:')
        print(self.noise_weights)
        print('is_noisy is:')
        print(is_noisy)

        # Initialize params
        if init_params:
            print('Initialize params using the params stored at:')
            print(param_dir)
            pkl_file = open(param_dir, 'rb')
            params = pickle.load(pkl_file)
            lambdas_val_init = params['lambdas_val']
            amps_val_init = params['amps_val']
            pkl_file.close()
        else:
            print('Randomly initialize params')
            lambdas_val_init = [None] * len(self.noise_weights)
            amps_val_init = [None] * len(self.noise_weights)

        # Build the model

        self.H1 = H  # 32
        self.W1 = W  # 32
        self.Cin1 = Cin  # 3

        self.h1 = 3
        self.w1 = 3
        self.K1 = 96
        self.M1 = 1

        self.H2 = self.H1  # 32
        self.W2 = self.W1  # 32
        self.Cin2 = self.K1  # 96

        self.h2 = 3
        self.w2 = 3
        self.K2 = 96
        self.M2 = 1

        self.H3 = self.H2 + self.h2 - 1  # 34
        self.W3 = self.W2 + self.w2 - 1  # 34
        self.Cin3 = self.K2  # 96

        self.h3 = 3
        self.w3 = 3
        self.K3 = 96
        self.M3 = 1

        self.H4 = (self.H3 + self.h3 - 1) / 2  # 18
        self.W4 = (self.W3 + self.w3 - 1) / 2  # 18
        self.Cin4 = self.K3  # 96

        self.h4 = 3
        self.w4 = 3
        self.K4 = 192
        self.M4 = 1

        self.H5 = self.H4 - self.h4 + 1  # 16
        self.W5 = self.W4 - self.w4 + 1  # 16
        self.Cin5 = self.K4  # 192

        self.h5 = 3
        self.w5 = 3
        self.K5 = 192
        self.M5 = 1

        self.H6 = self.H5 + self.h5 - 1  # 18
        self.W6 = self.W5 + self.w5 - 1  # 18
        self.Cin6 = self.K5  # 192

        self.h6 = 3
        self.w6 = 3
        self.K6 = 192
        self.M6 = 1

        self.H7 = (self.H6 - self.h6 + 1) / 2  # 8
        self.W7 = (self.W6 - self.w6 + 1) / 2  # 8
        self.Cin7 = self.K6  # 192

        self.h7 = 3
        self.w7 = 3
        self.K7 = 192
        self.M7 = 1

        self.H8 = self.H7 - self.h7 + 1  # 6
        self.W8 = self.W7 - self.w7 + 1  # 6
        self.Cin8 = self.K7  # 192

        self.h8 = 1
        self.w8 = 1
        self.K8 = 192
        self.M8 = 1

        self.H9 = self.H8 - self.h8 + 1  # 6
        self.W9 = self.W8 - self.w8 + 1  # 6
        self.Cin9 = self.K8  # 192

        self.h9 = 1
        self.w9 = 1
        self.K9 = 10
        self.M9 = 1

        self.H_Softmax = 1
        self.W_Softmax = 1
        self.Cin_Softmax = self.K9  # 10

        self.h_Softmax = 1
        self.w_Softmax = 1
        self.K_Softmax = 10
        self.M_Softmax = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y = T.ivector('y')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

        self.srng = RandomStreams()
        self.srng.seed(np.random.randint(2 ** 15))

        # Forward Step
        self.conv1 = CRM(
            data_4D=self.x
                    + self.noise_weights[0] * self.srng.normal(size=(self.batch_size, self.Cin1, self.H1, self.W1),
                                                               avg=0.0, std=self.noise_std),
            data_4D_clean=self.x,
            labels=self.y,
            is_train=self.is_train, momentum_bn=self.momentum_bn,
            K=self.K1, M=self.M1, W=self.W1, H=self.H1,
            w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
            amps_val_init=amps_val_init[8], lambdas_val_init=lambdas_val_init[8],
            PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
            pool_t_mode=None,
            border_mode='half',
            rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
            is_noisy=is_noisy[0], is_bn_BU=self.is_bn_BU, is_bn_TD=False, nonlin=self.nonlin)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D=self.conv1.output
                                 + self.noise_weights[1] * self.srng.normal(
            size=(self.batch_size, self.Cin2, self.H2, self.W2), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv1.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K2, M=self.M2, W=self.W2, H=self.H2,
                         w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=amps_val_init[7], lambdas_val_init=lambdas_val_init[7],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='full',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[1], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin)

        self.conv2._E_step_Bottom_Up()

        self.conv3 = CRM(data_4D=self.conv2.output
                                 + self.noise_weights[2] * self.srng.normal(
            size=(self.batch_size, self.Cin3, self.H3, self.W3), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv2.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K3, M=self.M3, W=self.W3, H=self.H3,
                         w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
                         amps_val_init=amps_val_init[6], lambdas_val_init=lambdas_val_init[6],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='max_t',
                         border_mode='full',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[2], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin)

        self.conv3._E_step_Bottom_Up()

        self.conv4 = CRM(data_4D=self.conv3.output
                                 + self.noise_weights[3] * self.srng.normal(
            size=(self.batch_size, self.Cin4, self.H4, self.W4), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv3.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K4, M=self.M4, W=self.W4, H=self.H4,
                         w=self.w4, h=self.h4, Cin=self.Cin4, Ni=self.batch_size,
                         amps_val_init=amps_val_init[5], lambdas_val_init=lambdas_val_init[5],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[3], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin)

        self.conv4._E_step_Bottom_Up()

        self.conv5 = CRM(data_4D=self.conv4.output
                                 + self.noise_weights[4] * self.srng.normal(
            size=(self.batch_size, self.Cin5, self.H5, self.W5), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv4.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K5, M=self.M5, W=self.W5, H=self.H5,
                         w=self.w5, h=self.h5, Cin=self.Cin5, Ni=self.batch_size,
                         amps_val_init=amps_val_init[4], lambdas_val_init=lambdas_val_init[4],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='full',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[4], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin)

        self.conv5._E_step_Bottom_Up()

        self.conv6 = CRM(data_4D=self.conv5.output
                                 + self.noise_weights[5] * self.srng.normal(
            size=(self.batch_size, self.Cin6, self.H6, self.W6), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv5.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K6, M=self.M6, W=self.W6, H=self.H6,
                         w=self.w6, h=self.h6, Cin=self.Cin6, Ni=self.batch_size,
                         amps_val_init=amps_val_init[3], lambdas_val_init=lambdas_val_init[3],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='max_t',
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[5], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin)

        self.conv6._E_step_Bottom_Up()

        self.conv7 = CRM(data_4D=self.conv6.output
                                 + self.noise_weights[6] * self.srng.normal(
            size=(self.batch_size, self.Cin7, self.H7, self.W7), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv6.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K7, M=self.M7, W=self.W7, H=self.H7,
                         w=self.w7, h=self.h7, Cin=self.Cin7, Ni=self.batch_size,
                         amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[6], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin)

        self.conv7._E_step_Bottom_Up()

        self.conv8 = CRM(data_4D=self.conv7.output
                                 + self.noise_weights[7] * self.srng.normal(
            size=(self.batch_size, self.Cin8, self.H8, self.W8), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv7.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K8, M=self.M8, W=self.W8, H=self.H8,
                         w=self.w8, h=self.h8, Cin=self.Cin8, Ni=self.batch_size,
                         amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode=None,
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[7], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin)

        self.conv8._E_step_Bottom_Up()

        self.conv9 = CRM(data_4D=self.conv8.output
                                 + self.noise_weights[8] * self.srng.normal(
            size=(self.batch_size, self.Cin9, self.H9, self.W9), avg=0.0, std=self.noise_std),
                         data_4D_clean=self.conv8.output_clean,
                         labels=self.y,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K9, M=self.M9, W=self.W9, H=self.H9,
                         w=self.w9, h=self.h9, Cin=self.Cin9, Ni=self.batch_size,
                         amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
                         PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
                         pool_t_mode='mean_t', mean_pool_size=(6, 6),
                         border_mode='valid',
                         rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
                         is_noisy=is_noisy[8], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, nonlin=self.nonlin)

        self.conv9._E_step_Bottom_Up()

        # self.RegressionInSoftmax = CRM_with_bias(data_4D=self.conv9.output
        #                                     + 0.0 * self.srng.normal(size=(self.batch_size, self.Cin_Softmax, self.H_Softmax, self.W_Softmax), avg=0.0, std=self.noise_std),
        #                             data_4D_clean=self.conv9.output_clean,
        #                             labels=self.y,
        #                             is_train=self.is_train, momentum_bn=self.momentum_bn,
        #                             K=self.K_Softmax, M=self.M_Softmax, W=self.W_Softmax, H=self.H_Softmax,
        #                             w=self.w_Softmax, h=self.h_Softmax, Cin=self.Cin_Softmax, Ni=self.batch_size,
        #                             amps_val_init=None,
        #                             lambdas_val_init=np.zeros((self.K_Softmax, self.Cin_Softmax * self.h_Softmax * self.w_Softmax, self.M_Softmax),dtype=theano.config.floatX),
        #                             b_val_init=None,
        #                             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
        #                             pool_t_mode=None, pool_a_mode=None,
        #                             border_mode='valid',
        #                             rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
        #                             is_noisy=None, is_bn_BU=False, is_bn_TD=self.is_bn_TD)
        #
        # self.RegressionInSoftmax._E_step_Bottom_Up()

        self.RegressionInSoftmax = HiddenLayerInSoftmax(input=self.conv9.output.flatten(2), n_in=self.Cin_Softmax, n_out=self.K_Softmax,
                                          W_init=None, b_init=None, input_clean=self.conv9.output_clean.flatten(2))

        # self.RegressionInSoftmax = ConvLayerInSoftmax(input=self.conv9.output, filter_shape=(self.K_Softmax, self.Cin_Softmax, 1, 1),
        #                                 image_shape=(self.batch_size, self.Cin_Softmax, self.H_Softmax, self.W_Softmax),
        #                                 W_init=None, b_init=None, input_clean=self.conv9.output_clean)

        softmax_input = self.RegressionInSoftmax.output
        softmax_input_clean = self.RegressionInSoftmax.output_clean


        # classify the values of the fully-connected sigmoidal layer
        if self.train_mode == 'semisupervised':
            self.softmax_layer_nonlin = SoftmaxNonlinearitySemisupervised(input=softmax_input, input_clean=softmax_input_clean)
        else:
            self.softmax_layer_nonlin = SoftmaxNonlinearity(input=softmax_input, input_clean=softmax_input_clean)

        self.layers = [self.conv9, self.conv8, self.conv7, self.conv6, self.conv5, self.conv4, self.conv3,
                       self.conv2, self.conv1]
        self.N_layer = len(self.layers)

        # build Top-Down pass
        self.Build_TopDown(top_down_mode=self.top_down_mode, is_relu=self.is_relu)

        # build the cost function for the model
        self.Build_Cost()

        # build update rules for the model
        self.Build_Update_Rule()

# #######################################################################################################################
# class CIFAR10_Conv_Large_6_Layers(object):
#     '''
#     Conv_Small that the Ladder Network uses for MNIST
#     '''
#
#     def __init__(self, batch_size, Cin, W, H, em_mode, seed, param_dir=[], train_mode='supervised',
#                  reg_coeff=0.0, init_Bengio=False, grad_min=-np.inf, grad_max=np.inf,
#                  init_params=False):
#
#         self.em_mode = em_mode
#
#         self.batch_size = batch_size
#
#         self.H1 = H  # 32
#         self.W1 = W  # 32
#         self.Cin1 = Cin  # 3
#
#         self.h1 = 3
#         self.w1 = 3
#         self.K1 = 96
#         self.M1 = 1
#
#         self.H2 = self.H1  # 32
#         self.W2 = self.W1  # 32
#         self.Cin2 = self.K1  # 96
#
#         self.h2 = 3
#         self.w2 = 3
#         self.K2 = 96
#         self.M2 = 1
#
#         self.H3 = self.H2 + self.h2 - 1  # 34
#         self.W3 = self.W2 + self.w2 - 1  # 34
#         self.Cin3 = self.K2  # 96
#
#         self.h3 = 3
#         self.w3 = 3
#         self.K3 = 96
#         self.M3 = 1
#
#         self.H4 = (self.H3 + self.h3 - 1) / 2  # 18
#         self.W4 = (self.W3 + self.w3 - 1) / 2  # 18
#         self.Cin4 = self.K3  # 96
#
#         self.h4 = 3
#         self.w4 = 3
#         self.K4 = 192
#         self.M4 = 1
#
#         self.H5 = self.H4 - self.h4 + 1  # 16
#         self.W5 = self.W4 - self.w4 + 1  # 16
#         self.Cin5 = self.K4  # 192
#
#         self.h5 = 3
#         self.w5 = 3
#         self.K5 = 192
#         self.M5 = 1
#
#         self.H6 = self.H5 + self.h5 - 1  # 18
#         self.W6 = self.W5 + self.w5 - 1  # 18
#         self.Cin6 = self.K5  # 192
#
#         self.h6 = 3
#         self.w6 = 3
#         self.K6 = 192
#         self.M6 = 1
#
#         self.H7 = (self.H6 - self.h6 + 1) / 2  # 8
#         self.W7 = (self.W6 - self.w6 + 1) / 2  # 8
#         self.Cin7 = self.K6  # 192
#
#         self.seed = seed
#         np.random.seed(self.seed)
#
#         if init_params:
#             print(param_dir)
#             pkl_file = open(param_dir, 'rb')
#             params = pickle.load(pkl_file)
#             lambdas_val_init = params['lambdas_val']
#             amps_val_init = params['amps_val']
#             W_softmax_init = params['W_softmax']
#             b_softmax_init = params['b_softmax']
#             pkl_file.close()
#         else:
#             lambdas_val_init = [None, None, None, None, None, None]
#             amps_val_init = [None, None, None, None, None, None]
#             W_softmax_init = None
#             b_softmax_init = None
#
#         self.x = T.tensor4('x')
#         self.y = T.ivector('y')
#         self.lr = T.scalar('l_r', dtype=theano.config.floatX)
#
#         # Forward Step
#         self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1,
#                          h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[5], lambdas_val_init=lambdas_val_init[5],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode=None,
#                          border_mode='half',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv1._E_step_Bottom_Up()
#
#         self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2,
#                          w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[4], lambdas_val_init=lambdas_val_init[4],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode=None,
#                          border_mode='full',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv2._E_step_Bottom_Up()
#
#         self.conv3 = CRM(data_4D=self.conv2.output, labels=self.y, K=self.K3, M=self.M3, W=self.W3, H=self.H3,
#                          w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[3], lambdas_val_init=lambdas_val_init[3],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode='max_t',
#                          border_mode='full',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv3._E_step_Bottom_Up()
#
#         self.conv4 = CRM(data_4D=self.conv3.output, labels=self.y, K=self.K4, M=self.M4, W=self.W4, H=self.H4,
#                          w=self.w4, h=self.h4, Cin=self.Cin4, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode=None,
#                          border_mode='valid',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv4._E_step_Bottom_Up()
#
#         self.conv5 = CRM(data_4D=self.conv4.output, labels=self.y, K=self.K5, M=self.M5, W=self.W5, H=self.H5,
#                          w=self.w5, h=self.h5, Cin=self.Cin5, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode=None,
#                          border_mode='full',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv5._E_step_Bottom_Up()
#
#         self.conv6 = CRM(data_4D=self.conv5.output, labels=self.y, K=self.K6, M=self.M6, W=self.W6, H=self.H6,
#                          w=self.w6, h=self.h6, Cin=self.Cin6, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode='max_t',
#                          border_mode='valid',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv6._E_step_Bottom_Up()
#
#         if train_mode == 'supervised':
#             print('supervised')
#
#             n_softmax = self.Cin7 * self.H7 * self.W7
#
#             softmax_input = self.conv6.output.flatten(2)
#
#             # classify the values of the fully-connected sigmoidal layer
#             self.softmax_layer = LogisticRegression(input=softmax_input, n_in=n_softmax, n_out=10,
#                                                     W_init=W_softmax_init, b_init=b_softmax_init)
#
#             # the cost we minimize during training is the NLL of the model
#             self.cost = self.softmax_layer.negative_log_likelihood(self.y)
#
#         elif train_mode == 'unsupervised':
#             print('unsupervised')
#
#             # Top-Down
#             self.conv6._E_step_Top_Down_Reconstruction(mu_cg=self.conv6.output, ta_hat=self.conv6.masked_mat, layer_loc='intermediate')
#
#             self.conv5._E_step_Top_Down_Reconstruction(mu_cg=self.conv6.data_reconstructed, ta_hat=self.conv5.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv4._E_step_Top_Down_Reconstruction(mu_cg=self.conv5.data_reconstructed, ta_hat=self.conv4.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv3._E_step_Top_Down_Reconstruction(mu_cg=self.conv4.data_reconstructed, ta_hat=self.conv3.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.data_reconstructed, ta_hat=self.conv2.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             # Cost
#             self.cost = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#
#         elif train_mode == 'semisupervised':
#             print('semisupervised')
#
#             # Top-Down
#             self.conv6._E_step_Top_Down_Reconstruction(mu_cg=self.conv6.output, ta_hat=self.conv6.masked_mat, layer_loc='intermediate')
#
#             self.conv5._E_step_Top_Down_Reconstruction(mu_cg=self.conv6.data_reconstructed, ta_hat=self.conv5.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv4._E_step_Top_Down_Reconstruction(mu_cg=self.conv5.data_reconstructed, ta_hat=self.conv4.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv3._E_step_Top_Down_Reconstruction(mu_cg=self.conv4.data_reconstructed, ta_hat=self.conv3.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.data_reconstructed, ta_hat=self.conv2.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             n_softmax = self.Cin7 * self.H7 * self.W7
#
#             softmax_input = self.conv6.output.flatten(2)
#
#             # classify the values of the fully-connected sigmoidal layer
#             self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10,
#                                                                      W_init=W_softmax_init, b_init=b_softmax_init)
#
#             # the cost we minimize during training is the NLL of the model
#             self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff * T.mean(
#                 (self.x - self.conv1.data_reconstructed) ** 2)
#             self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#             self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)
#
#         elif train_mode == 'semisupervised_MSMP':
#             print('semisupervised_MSMP')
#
#             # MS Top-Down
#
#             self.conv6._E_step_Top_Down_Reconstruction(mu_cg=self.conv6.output, ta_hat=self.conv6.masked_mat, layer_loc='intermediate')
#
#             self.conv5._E_step_Top_Down(z=self.conv6.data_reconstructed, pool_mode=None)
#
#             self.conv5._E_step_Top_Down_Reconstruction(mu_cg=self.conv6.data_reconstructed, ta_hat=self.conv5.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv4._E_step_Top_Down(z=self.conv5.data_reconstructed, pool_mode=None)
#
#             self.conv4._E_step_Top_Down_Reconstruction(mu_cg=self.conv5.data_reconstructed, ta_hat=self.conv4.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv3._E_step_Top_Down(z=self.conv4.data_reconstructed, pool_mode='max_t')
#
#             self.conv3._E_step_Top_Down_Reconstruction(mu_cg=self.conv4.data_reconstructed, ta_hat=self.conv3.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv2._E_step_Top_Down(z=self.conv3.data_reconstructed, pool_mode=None)
#
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.data_reconstructed, ta_hat=self.conv2.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down(z=self.conv2.data_reconstructed, pool_mode=None)
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             n_softmax = self.Cin7 * self.H7 * self.W7
#
#             softmax_input = self.conv6.output.flatten(2)
#
#             # classify the values of the fully-connected sigmoidal layer
#             self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10,
#                                                                      W_init=W_softmax_init, b_init=b_softmax_init)
#
#             # the cost we minimize during training is the NLL of the model
#             self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff * T.mean(
#                 (self.x - self.conv1.data_reconstructed) ** 2)
#             self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#             self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)
#
#         else:
#             print('Please specify how to do TopDown and update your model in train_model_no_factor_latest.py')
#
#         if train_mode != 'unsupervised':
#             # create a list of all model parameters to be fit by gradient descent
#             self.params = [self.conv6.lambdas, ] + [self.conv5.lambdas, ] + [self.conv4.lambdas, ] + [
#                 self.conv3.lambdas, ] + [self.conv2.lambdas, ] + [self.conv1.lambdas, ] + self.softmax_layer.params
#
#             # create a list of gradients for all model parameters
#             grads = T.grad(self.cost, self.params)
#
#             # train_model is a function that updates the model parameters by
#             # SGD Since this model has many parameters, it would be tedious to
#             # manually create an update rule for each model parameter. We thus
#             # create the updates list by automatically looping over all
#             # (params[i],grads[i]) pairs.
#
#             self.conv6.lambdas_new = self.conv6.lambdas - self.lr * T.clip(grads[0], grad_min, grad_max)
#             self.conv5.lambdas_new = self.conv5.lambdas - self.lr * T.clip(grads[1], grad_min, grad_max)
#             self.conv4.lambdas_new = self.conv4.lambdas - self.lr * T.clip(grads[2], grad_min, grad_max)
#             self.conv3.lambdas_new = self.conv3.lambdas - self.lr * T.clip(grads[3], grad_min, grad_max)
#             self.conv2.lambdas_new = self.conv2.lambdas - self.lr * T.clip(grads[4], grad_min, grad_max)
#             self.conv1.lambdas_new = self.conv1.lambdas - self.lr * T.clip(grads[5], grad_min, grad_max)
#             self.softmax_layer.W_new = self.softmax_layer.params[0] - self.lr * T.clip(grads[6], grad_min, grad_max)
#             self.softmax_layer.b_new = self.softmax_layer.params[1] - self.lr * T.clip(grads[7], grad_min, grad_max)
#
#             self.layers = [self.conv6, self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
#
#             self.updates = []
#             self.output_var = []
#             self.misc = []
#             self.init_var = []
#
#             for layer in self.layers:
#                 # Concatenate updates
#                 self.updates.append((layer.lambdas, layer.lambdas_new))
#                 self.updates.append((layer.amps, layer.amps_new))
#                 # Concatenate output_var
#                 self.output_var.append(layer.logLs)  # 0
#                 self.output_var.append(layer.betas)  # 1
#                 self.output_var.append(layer.lambdas_new)  # 2
#                 self.output_var.append(layer.amps_new)  # 3
#                 # Concatenate misc
#                 self.misc.append(layer.latents)  # 0
#                 self.misc.append(layer.rs)  # 1
#                 self.misc.append(layer.latents_masked)  # 2
#                 # Concatenate init_var
#                 self.init_var.append(layer.betas)  # 0
#                 self.init_var.append(layer.lambdas)  # 1
#                 self.init_var.append(layer.amps)  # 2
#
#             self.output_var.append(self.softmax_layer.W_new)
#             self.output_var.append(self.softmax_layer.b_new)
#             self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
#             self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))
#
#         else:
#             # create a list of all model parameters to be fit by gradient descent
#             self.params = [self.conv6.lambdas, ] + [self.conv5.lambdas, ] + [self.conv4.lambdas, ] \
#                           + [self.conv3.lambdas, ] + [self.conv2.lambdas, ] + [self.conv1.lambdas, ]
#
#             # create a list of gradients for all model parameters
#             grads = T.grad(self.cost, self.params)
#
#             # train_model is a function that updates the model parameters by
#             # SGD Since this model has many parameters, it would be tedious to
#             # manually create an update rule for each model parameter. We thus
#             # create the updates list by automatically looping over all
#             # (params[i],grads[i]) pairs.
#
#             self.conv6.lambdas_new = self.conv6.lambdas - self.lr * T.clip(grads[0], grad_min, grad_max)
#             self.conv5.lambdas_new = self.conv5.lambdas - self.lr * T.clip(grads[1], grad_min, grad_max)
#             self.conv4.lambdas_new = self.conv4.lambdas - self.lr * T.clip(grads[2], grad_min, grad_max)
#             self.conv3.lambdas_new = self.conv3.lambdas - self.lr * T.clip(grads[3], grad_min, grad_max)
#             self.conv2.lambdas_new = self.conv2.lambdas - self.lr * T.clip(grads[4], grad_min, grad_max)
#             self.conv1.lambdas_new = self.conv1.lambdas - self.lr * T.clip(grads[5], grad_min, grad_max)
#
#             self.layers = [self.conv6, self.conv5, self.conv4, self.conv3,
#                            self.conv2, self.conv1]
#
#             self.updates = []
#             self.output_var = []
#             self.misc = []
#             self.init_var = []
#
#             for layer in self.layers:
#                 # Concatenate updates
#                 self.updates.append((layer.lambdas, layer.lambdas_new))
#                 self.updates.append((layer.amps, layer.amps_new))
#                 # Concatenate output_var
#                 self.output_var.append(layer.logLs)  # 0
#                 self.output_var.append(layer.betas)  # 1
#                 self.output_var.append(layer.lambdas_new)  # 2
#                 self.output_var.append(layer.amps_new)  # 3
#                 # Concatenate misc
#                 self.misc.append(layer.latents)  # 0
#                 self.misc.append(layer.rs)  # 1
#                 self.misc.append(layer.latents_masked)  # 2
#                 # Concatenate init_var
#                 self.init_var.append(layer.betas)  # 0
#                 self.init_var.append(layer.lambdas)  # 1
#                 self.init_var.append(layer.amps)  # 2
#
#
# #######################################################################################################################
# class CIFAR10_Conv_3_Layers(object):
#     '''
#     Conv_Small that the Ladder Network uses for MNIST
#     '''
#
#     def __init__(self, batch_size, Cin, W, H, em_mode, seed, param_dir=[], train_mode='supervised',
#                  reg_coeff=0.0, init_Bengio=False, grad_min=-np.inf, grad_max=np.inf,
#                  init_params=False):
#
#         self.em_mode = em_mode
#
#         self.batch_size = batch_size
#
#         self.H1 = H  # 32
#         self.W1 = W  # 32
#         self.Cin1 = Cin  # 3
#
#         self.h1 = 5
#         self.w1 = 5
#         self.K1 = 64
#         self.M1 = 1
#
#         self.H2 = (self.H1 - self.h1 + 1) / 2  # 14
#         self.W2 = (self.W1 - self.w1 + 1) / 2  # 14
#         self.Cin2 = self.K1
#
#         self.h2 = 3
#         self.w2 = 3
#         self.K2 = 64
#         self.M2 = 1
#
#         self.H3 = (self.H2 - self.h2 + 1) / 2  # 6
#         self.W3 = (self.W2 - self.w2 + 1) / 2  # 6
#         self.Cin3 = self.K2  # 64
#
#         self.h3 = 3
#         self.w3 = 3
#         self.K3 = 64
#         self.M3 = 1
#
#         self.H4 = (self.H3 - self.h3 + 1) / 2  # 2
#         self.W4 = (self.W3 - self.w3 + 1) / 2  # 2
#         self.Cin4 = self.K3  # 64
#
#         self.seed = seed
#         np.random.seed(self.seed)
#
#         if init_params:
#             print(param_dir)
#             pkl_file = open(param_dir, 'rb')
#             params = pickle.load(pkl_file)
#             lambdas_val_init = params['lambdas_val']
#             amps_val_init = params['amps_val']
#             W_softmax_init = params['W_softmax']
#             b_softmax_init = params['b_softmax']
#             pkl_file.close()
#         else:
#             lambdas_val_init = [None, None, None]
#             amps_val_init = [None, None, None]
#             W_softmax_init = None
#             b_softmax_init = None
#
#         self.x = T.tensor4('x')
#         self.y = T.ivector('y')
#         self.lr = T.scalar('l_r', dtype=theano.config.floatX)
#
#         # Forward Step
#         self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1,
#                          h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode='max_t',
#                          border_mode='valid',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv1._E_step_Bottom_Up()
#
#         self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2,
#                          w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode='max_t',
#                          border_mode='valid',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv2._E_step_Bottom_Up()
#
#         self.conv3 = CRM(data_4D=self.conv2.output, labels=self.y, K=self.K3, M=self.M3, W=self.W3, H=self.H3,
#                          w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode='max_t',
#                          border_mode='valid',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv3._E_step_Bottom_Up()
#
#         if train_mode == 'supervised':
#             print('supervised')
#
#             n_softmax = self.Cin4 * self.H4 * self.W4
#
#             softmax_input = self.conv3.output.flatten(2)
#
#             # classify the values of the fully-connected sigmoidal layer
#             self.softmax_layer = LogisticRegression(input=softmax_input, n_in=n_softmax, n_out=10,
#                                                     W_init=W_softmax_init, b_init=b_softmax_init)
#
#             # the cost we minimize during training is the NLL of the model
#             self.cost = self.softmax_layer.negative_log_likelihood(self.y)
#
#         elif train_mode == 'unsupervised':
#             print('unsupervised')
#
#             # Top-Down
#             self.conv3._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.output, ta_hat=self.conv3.masked_mat, layer_loc='intermediate')
#
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.data_reconstructed, ta_hat=self.conv2.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             # Cost
#             self.cost = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#
#         elif train_mode == 'unsupervised_MSMP':
#             print('unsupervised_MSMP')
#
#             # MS Top-Down
#
#             self.conv3._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.output, ta_hat=self.conv3.masked_mat, layer_loc='intermediate')
#
#             self.conv2._E_step_Top_Down(z=self.conv3.data_reconstructed, pool_mode='max_t')
#
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.data_reconstructed, ta_hat=self.conv2.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down(z=self.conv2.data_reconstructed, pool_mode='max_t')
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             # Cost
#             self.cost = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#
#         elif train_mode == 'semisupervised':
#             print('semisupervised')
#
#             # Top-Down
#             self.conv3._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.output, ta_hat=self.conv3.masked_mat, layer_loc='intermediate')
#
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.data_reconstructed, ta_hat=self.conv2.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             n_softmax = self.Cin4 * self.H4 * self.W4
#
#             softmax_input = self.conv3.output.flatten(2)
#
#             # classify the values of the fully-connected sigmoidal layer
#             self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10,
#                                                                      W_init=W_softmax_init, b_init=b_softmax_init)
#
#             # the cost we minimize during training is the NLL of the model
#             self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff * T.mean(
#                 (self.x - self.conv1.data_reconstructed) ** 2)
#             self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#             self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)
#
#         elif train_mode == 'semisupervised_MSMP':
#             print('semisupervised_MSMP')
#
#             # MS Top-Down
#
#             self.conv3._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.latents_masked, ta_hat=self.conv3.masked_mat, layer_loc='last')
#
#             self.conv2._E_step_Top_Down(z=self.conv3.data_reconstructed, pool_mode='max_t')
#
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv3.data_reconstructed, ta_hat=self.conv2.masked_mat,
#                                                        layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down(z=self.conv2.data_reconstructed, pool_mode='max_t')
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             n_softmax = self.Cin4 * self.H4 * self.W4
#
#             softmax_input = self.conv3.output.flatten(2)
#
#             # classify the values of the fully-connected sigmoidal layer
#             self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10,
#                                                                      W_init=W_softmax_init, b_init=b_softmax_init)
#
#             # the cost we minimize during training is the NLL of the model
#             self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff * T.mean(
#                 (self.x - self.conv1.data_reconstructed) ** 2)
#             self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#             self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)
#
#         else:
#             print('Please specify how to do TopDown and update your model in train_model_no_factor_latest.py')
#
#         if train_mode != 'unsupervised' and train_mode != 'unsupervised_MSMP':
#             # create a list of all model parameters to be fit by gradient descent
#             self.params = [self.conv3.lambdas, ] + [self.conv2.lambdas, ] + [
#                 self.conv1.lambdas, ] + self.softmax_layer.params
#
#             # create a list of gradients for all model parameters
#             grads = T.grad(self.cost, self.params)
#             self.grads = grads
#
#             # train_model is a function that updates the model parameters by
#             # SGD Since this model has many parameters, it would be tedious to
#             # manually create an update rule for each model parameter. We thus
#             # create the updates list by automatically looping over all
#             # (params[i],grads[i]) pairs.
#
#             self.conv3.lambdas_new = self.conv3.lambdas - self.lr * T.clip(grads[0], grad_min, grad_max)
#             self.conv2.lambdas_new = self.conv2.lambdas - self.lr * T.clip(grads[1], grad_min, grad_max)
#             self.conv1.lambdas_new = self.conv1.lambdas - self.lr * T.clip(grads[2], grad_min, grad_max)
#             self.softmax_layer.W_new = self.softmax_layer.params[0] - self.lr * T.clip(grads[3], grad_min, grad_max)
#             self.softmax_layer.b_new = self.softmax_layer.params[1] - self.lr * T.clip(grads[4], grad_min, grad_max)
#
#             self.layers = [self.conv3, self.conv2, self.conv1]
#
#             self.updates = []
#             self.output_var = []
#             self.misc = []
#             self.init_var = []
#
#             for layer in self.layers:
#                 # Concatenate updates
#                 self.updates.append((layer.lambdas, layer.lambdas_new))
#                 self.updates.append((layer.amps, layer.amps_new))
#                 # Concatenate output_var
#                 self.output_var.append(layer.logLs)  # 0
#                 self.output_var.append(layer.betas)  # 1
#                 self.output_var.append(layer.lambdas_new)  # 2
#                 self.output_var.append(layer.amps_new)  # 3
#                 # Concatenate misc
#                 self.misc.append(layer.latents)  # 0
#                 self.misc.append(layer.rs)  # 1
#                 self.misc.append(layer.latents_masked)  # 2
#                 # Concatenate init_var
#                 self.init_var.append(layer.betas)  # 0
#                 self.init_var.append(layer.lambdas)  # 1
#                 self.init_var.append(layer.amps)  # 2
#
#             self.output_var.append(self.softmax_layer.W_new)
#             self.output_var.append(self.softmax_layer.b_new)
#             self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
#             self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))
#
#         else:
#             # create a list of all model parameters to be fit by gradient descent
#             self.params = [self.conv3.lambdas, ] + [self.conv2.lambdas, ] + [self.conv1.lambdas, ]
#
#             # create a list of gradients for all model parameters
#             grads = T.grad(self.cost, self.params)
#
#             # train_model is a function that updates the model parameters by
#             # SGD Since this model has many parameters, it would be tedious to
#             # manually create an update rule for each model parameter. We thus
#             # create the updates list by automatically looping over all
#             # (params[i],grads[i]) pairs.
#
#             self.conv3.lambdas_new = self.conv3.lambdas - self.lr * T.clip(grads[0], grad_min, grad_max)
#             self.conv2.lambdas_new = self.conv2.lambdas - self.lr * T.clip(grads[1], grad_min, grad_max)
#             self.conv1.lambdas_new = self.conv1.lambdas - self.lr * T.clip(grads[2], grad_min, grad_max)
#
#             self.layers = [self.conv3, self.conv2, self.conv1]
#
#             self.updates = []
#             self.output_var = []
#             self.misc = []
#             self.init_var = []
#
#             for layer in self.layers:
#                 # Concatenate updates
#                 self.updates.append((layer.lambdas, layer.lambdas_new))
#                 self.updates.append((layer.amps, layer.amps_new))
#                 # Concatenate output_var
#                 self.output_var.append(layer.logLs)  # 0
#                 self.output_var.append(layer.betas)  # 1
#                 self.output_var.append(layer.lambdas_new)  # 2
#                 self.output_var.append(layer.amps_new)  # 3
#                 # Concatenate misc
#                 self.misc.append(layer.latents)  # 0
#                 self.misc.append(layer.rs)  # 1
#                 self.misc.append(layer.latents_masked)  # 2
#                 # Concatenate init_var
#                 self.init_var.append(layer.betas)  # 0
#                 self.init_var.append(layer.lambdas)  # 1
#                 self.init_var.append(layer.amps)  # 2
#
#
# #######################################################################################################################
# class CIFAR10_Conv_2_Layers(object):
#     '''
#     Conv_Small that the Ladder Network uses for MNIST
#     '''
#
#     def __init__(self, batch_size, Cin, W, H, em_mode, seed, param_dir=[], train_mode='supervised',
#                  reg_coeff=0.0, init_Bengio=False, grad_min=-np.inf, grad_max=np.inf,
#                  init_params=False):
#
#         self.em_mode = em_mode
#
#         self.batch_size = batch_size
#
#         self.H1 = H  # 32
#         self.W1 = W  # 32
#         self.Cin1 = Cin  # 3
#
#         self.h1 = 5
#         self.w1 = 5
#         self.K1 = 64
#         self.M1 = 1
#
#         self.H2 = (self.H1 - self.h1 + 1) / 2  # 14
#         self.W2 = (self.W1 - self.w1 + 1) / 2  # 14
#         self.Cin2 = self.K1  # 64
#
#         self.h2 = 5
#         self.w2 = 5
#         self.K2 = 64
#         self.M2 = 1
#
#         self.H3 = (self.H2 - self.h2 + 1) / 2  # 5
#         self.W3 = (self.W2 - self.w2 + 1) / 2  # 5
#         self.Cin3 = self.K2  # 64
#
#         self.seed = seed
#         np.random.seed(self.seed)
#
#         if init_params:
#             print(param_dir)
#             pkl_file = open(param_dir, 'rb')
#             params = pickle.load(pkl_file)
#             lambdas_val_init = params['lambdas_val']
#             amps_val_init = params['amps_val']
#             W_softmax_init = params['W_softmax']
#             b_softmax_init = params['b_softmax']
#             pkl_file.close()
#         else:
#             print('random initialization')
#             lambdas_val_init = [None, None]
#             amps_val_init = [None, None]
#             W_softmax_init = None
#             b_softmax_init = None
#
#         self.x = T.tensor4('x')
#         self.y = T.ivector('y')
#         self.lr = T.scalar('l_r', dtype=theano.config.floatX)
#
#         # Forward Step
#         self.conv1 = CRM(data_4D=self.x, labels=self.y, K=self.K1, M=self.M1, W=self.W1, H=self.H1, w=self.w1,
#                          h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode='max_t',
#                          border_mode='valid',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv1._E_step_Bottom_Up()
#
#         self.conv2 = CRM(data_4D=self.conv1.output, labels=self.y, K=self.K2, M=self.M2, W=self.W2, H=self.H2,
#                          w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate', pool_t_mode='max_t',
#                          border_mode='valid',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio)
#
#         self.conv2._E_step_Bottom_Up()
#
#         if train_mode == 'supervised':
#             print('supervised')
#
#             n_softmax = self.Cin3 * self.H3 * self.W3
#
#             softmax_input = self.conv2.output.flatten(2)
#
#             # classify the values of the fully-connected sigmoidal layer
#             self.softmax_layer = LogisticRegression(input=softmax_input, n_in=n_softmax, n_out=10,
#                                                     W_init=W_softmax_init, b_init=b_softmax_init)
#
#             # the cost we minimize during training is the NLL of the model
#             self.cost = self.softmax_layer.negative_log_likelihood(self.y)
#
#         elif train_mode == 'unsupervised':
#             print('unsupervised')
#
#             # Top-Down
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.output, ta_hat=self.conv2.masked_mat, layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             # Cost
#             self.cost = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#
#         elif train_mode == 'semisupervised':
#             print('semisupervised')
#
#             # Top-Down
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.output, ta_hat=self.conv2.masked_mat, layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             n_softmax = self.Cin3 * self.H3 * self.W3
#
#             softmax_input = self.conv2.output.flatten(2)
#
#             # classify the values of the fully-connected sigmoidal layer
#             self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10,
#                                                                      W_init=W_softmax_init, b_init=b_softmax_init)
#
#             # the cost we minimize during training is the NLL of the model
#             self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff * T.mean(
#                 (self.x - self.conv1.data_reconstructed) ** 2)
#             self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#             self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)
#
#         elif train_mode == 'semisupervised_MSMP':
#             print('semisupervised_MSMP')
#
#             # MS Top-Down
#
#             self.conv2._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.output, ta_hat=self.conv2.masked_mat, layer_loc='intermediate')
#
#             self.conv1._E_step_Top_Down(z=self.conv2.data_reconstructed, pool_mode='max_t')
#
#             self.conv1._E_step_Top_Down_Reconstruction(mu_cg=self.conv2.data_reconstructed, ta_hat=self.conv1.masked_mat,
#                                                        layer_loc='intermediate')
#
#             n_softmax = self.Cin3 * self.H3 * self.W3
#
#             softmax_input = self.conv2.output.flatten(2)
#
#             # classify the values of the fully-connected sigmoidal layer
#             self.softmax_layer = LogisticRegressionForSemisupervised(input=softmax_input, n_in=n_softmax, n_out=10,
#                                                                      W_init=W_softmax_init, b_init=b_softmax_init)
#
#             # the cost we minimize during training is the NLL of the model
#             self.cost = self.softmax_layer.negative_log_likelihood(self.y) + reg_coeff * T.mean(
#                 (self.x - self.conv1.data_reconstructed) ** 2)
#             self.unsupervisedNLL = T.mean((self.x - self.conv1.data_reconstructed) ** 2)
#             self.supervisedNLL = self.softmax_layer.negative_log_likelihood(self.y)
#
#         else:
#             print('Please specify how to do TopDown and update your model in train_model_no_factor_latest.py')
#
#         if train_mode != 'unsupervised':
#             print('Update rules for all training modes except unsupervised')
#             # create a list of all model parameters to be fit by gradient descent
#             self.params = [self.conv2.lambdas, ] + [self.conv1.lambdas, ] + self.softmax_layer.params
#
#             # create a list of gradients for all model parameters
#             grads = T.grad(self.cost, self.params)
#             self.grads = grads
#
#             # train_model is a function that updates the model parameters by
#             # SGD Since this model has many parameters, it would be tedious to
#             # manually create an update rule for each model parameter. We thus
#             # create the updates list by automatically looping over all
#             # (params[i],grads[i]) pairs.
#
#             self.conv2.lambdas_new = self.conv2.lambdas - self.lr * T.clip(grads[0], grad_min, grad_max)
#             self.conv1.lambdas_new = self.conv1.lambdas - self.lr * T.clip(grads[1], grad_min, grad_max)
#             self.softmax_layer.W_new = self.softmax_layer.params[0] - self.lr * T.clip(grads[2], grad_min, grad_max)
#             self.softmax_layer.b_new = self.softmax_layer.params[1] - self.lr * T.clip(grads[3], grad_min, grad_max)
#
#             self.layers = [self.conv2, self.conv1]
#
#             self.updates = []
#             self.output_var = []
#             self.misc = []
#             self.init_var = []
#
#             for layer in self.layers:
#                 # Concatenate updates
#                 self.updates.append((layer.lambdas, layer.lambdas_new))
#                 self.updates.append((layer.amps, layer.amps_new))
#                 # Concatenate output_var
#                 self.output_var.append(layer.logLs)  # 0
#                 self.output_var.append(layer.betas)  # 1
#                 self.output_var.append(layer.lambdas_new)  # 2
#                 self.output_var.append(layer.amps_new)  # 3
#                 # Concatenate misc
#                 self.misc.append(layer.latents)  # 0
#                 self.misc.append(layer.rs)  # 1
#                 self.misc.append(layer.latents_masked)  # 2
#                 # Concatenate init_var
#                 self.init_var.append(layer.betas)  # 0
#                 self.init_var.append(layer.lambdas)  # 1
#                 self.init_var.append(layer.amps)  # 2
#
#             self.output_var.append(self.softmax_layer.W_new)
#             self.output_var.append(self.softmax_layer.b_new)
#             self.updates.append((self.softmax_layer.params[0], self.softmax_layer.W_new))
#             self.updates.append((self.softmax_layer.params[1], self.softmax_layer.b_new))
#
#         else:
#             # create a list of all model parameters to be fit by gradient descent
#             self.params = [self.conv2.lambdas, ] + [self.conv1.lambdas, ]
#
#             # create a list of gradients for all model parameters
#             grads = T.grad(self.cost, self.params)
#
#             # train_model is a function that updates the model parameters by
#             # SGD Since this model has many parameters, it would be tedious to
#             # manually create an update rule for each model parameter. We thus
#             # create the updates list by automatically looping over all
#             # (params[i],grads[i]) pairs.
#
#             self.conv2.lambdas_new = self.conv2.lambdas - self.lr * T.clip(grads[0], grad_min, grad_max)
#             self.conv1.lambdas_new = self.conv1.lambdas - self.lr * T.clip(grads[1], grad_min, grad_max)
#
#             self.layers = [self.conv2, self.conv1]
#
#             self.updates = []
#             self.output_var = []
#             self.misc = []
#             self.init_var = []
#
#             for layer in self.layers:
#                 # Concatenate updates
#                 self.updates.append((layer.lambdas, layer.lambdas_new))
#                 self.updates.append((layer.amps, layer.amps_new))
#                 # Concatenate output_var
#                 self.output_var.append(layer.logLs)  # 0
#                 self.output_var.append(layer.betas)  # 1
#                 self.output_var.append(layer.lambdas_new)  # 2
#                 self.output_var.append(layer.amps_new)  # 3
#                 # Concatenate misc
#                 self.misc.append(layer.latents)  # 0
#                 self.misc.append(layer.rs)  # 1
#                 self.misc.append(layer.latents_masked)  # 2
#                 # Concatenate init_var
#                 self.init_var.append(layer.betas)  # 0
#                 self.init_var.append(layer.lambdas)  # 1
#                 self.init_var.append(layer.amps)  # 2
#
