from pylab import *
import numpy as np

A = []
A.append([1,2,3,4,5])
A.append([1,4,6,8,2])

indx = [1,2,3,4,5]

fig = figure()
plot(indx,np.asarray(A).T)
show()


# class MNIST_Conv_Small_5_Layers(DRM_model):
#     '''
#     Conv_Small that the Ladder Network uses for MNIST
#     '''
#     # TODO: factor out common init code from all models
#     def __init__(self, batch_size, Cin, W, H, em_mode, seed, param_dir=[], train_mode='supervised',
#                  reconst_weights=[0.0, 0.0, 0.0, 0.0, 0.0], init_Bengio=False, grad_min=-np.inf, grad_max=np.inf,
#                  init_params=False, denoising='simple', noise_std=0.45, noise_weights=[0.0, 0.0, 0.0, 0.0, 0.0],
#                  is_bn_BU=False, is_bn_TD=False, top_down_mode=None, is_relu=False, nonlin='relu', is_tied_bn=False,
#                  is_Dg=False):
#
#         print('Your model is MNIST_Conv_Small_5_Layers')
#
#         self.noise_std = noise_std
#         self.noise_weights = noise_weights
#         self.reconst_weights = reconst_weights
#
#         self.is_bn_BU = is_bn_BU
#         self.is_bn_TD = is_bn_TD
#         self.is_tied_bn = is_tied_bn
#         self.is_Dg = is_Dg
#
#         self.em_mode = em_mode
#         self.batch_size = batch_size
#         self.train_mode = train_mode
#         self.denoising = denoising
#         self.grad_min = grad_min
#         self.grad_max = grad_max
#
#         self.top_down_mode = top_down_mode
#         self.is_relu = is_relu
#         self.nonlin = nonlin
#
#         is_noisy = [False] * len(self.noise_weights)
#         for i in xrange(len(is_noisy)):
#             if np.sum(self.noise_weights[0:i + 1]) > 0.0:
#                 is_noisy[i] = True
#
#         print('noise_weights is:')
#         print(self.noise_weights)
#         print('is_noisy is:')
#         print(is_noisy)
#
#         # Initialize params
#         if init_params:
#             print('Initialize params using the params stored at:')
#             print(param_dir)
#             pkl_file = open(param_dir, 'rb')
#             params = pickle.load(pkl_file)
#             lambdas_val_init = params['lambdas_val']
#             amps_val_init = params['amps_val']
#             pkl_file.close()
#         else:
#             print('Randomly initialize params')
#             lambdas_val_init = [None] * len(self.noise_weights)
#             amps_val_init = [None] * len(self.noise_weights)
#
#         # Build the model
#
#         self.H1 = H  # 28
#         self.W1 = W  # 28
#         self.Cin1 = Cin  # 1
#
#         self.h1 = 5
#         self.w1 = 5
#         self.K1 = 32
#         self.M1 = 1
#
#         self.H2 = (self.H1 + self.h1 - 1) / 2  # 16
#         self.W2 = (self.W1 + self.w1 - 1) / 2  # 16
#         self.Cin2 = self.K1  # 32
#
#         self.h2 = 3
#         self.w2 = 3
#         self.K2 = 64
#         self.M2 = 1
#
#         self.H3 = self.H2 - self.h2 + 1  # 14
#         self.W3 = self.W2 - self.w2 + 1  # 14
#         self.Cin3 = self.K2  # 64
#
#         self.h3 = 3
#         self.w3 = 3
#         self.K3 = 64
#         self.M3 = 1
#
#         self.H4 = (self.H3 + self.h3 - 1) / 2  # 8
#         self.W4 = (self.W3 + self.w3 - 1) / 2  # 8
#         self.Cin4 = self.K3  # 64
#
#         self.h4 = 3
#         self.w4 = 3
#         self.K4 = 128
#         self.M4 = 1
#
#         self.H5 = self.H4 - self.h4 + 1  # 6
#         self.W5 = self.W4 - self.w4 + 1  # 6
#         self.Cin5 = self.K4  # 128
#
#         self.h5 = 1
#         self.w5 = 1
#         self.K5 = 10
#         self.M5 = 1
#
#         self.H_Softmax = 1
#         self.W_Softmax = 1
#         self.Cin_Softmax = self.K5  # 10
#
#         self.h_Softmax = 1
#         self.w_Softmax = 1
#         self.K_Softmax = 10
#         self.M_Softmax = 1
#
#         self.seed = seed
#         np.random.seed(self.seed)
#
#         self.x = tf.placeholder(tf.float32, shape=[None, 1, 28, 28], name='x')
#         self.y = tf.placeholder(tf.float32, shape=[None, 10], name='y')
#
#         self.lr = tf.placeholder(tf.float32, name='lr')
#         self.is_train = tf.placeholder(tf.int32, name='is_train')
#         self.momentum_bn = tf.placeholder(tf.float32, name='momentum_bn')
#
#         # Forward Step
#         self.conv1 = CRM(
#             data_4D=self.x
#                     + self.noise_weights[0] * tf.random_normal([self.batch_size, self.Cin1, self.H1, self.W1], stddev=self.noise_std),
#             data_4D_clean=self.x,
#             labels=self.y,
#             is_train = self.is_train, momentum_bn=self.momentum_bn,
#             K=self.K1, M=self.M1, W=self.W1, H=self.H1,
#             w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
#             amps_val_init=amps_val_init[4], lambdas_val_init=lambdas_val_init[4],
#             PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
#             pool_t_mode='max_t',
#             border_mode='FULL',
#             rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
#             is_noisy=is_noisy[0], is_bn_BU=self.is_bn_BU, is_bn_TD=False, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
#             is_Dg = self.is_Dg)
#
#         self.conv1._E_step_Bottom_Up()
#
#         self.conv2 = CRM(data_4D=self.conv1.output
#                                  + self.noise_weights[1] * tf.random_normal([self.batch_size, self.Cin2, self.H2, self.W2], stddev=self.noise_std),
#                          data_4D_clean=self.conv1.output_clean,
#                          labels=self.y,
#                          is_train = self.is_train, momentum_bn=self.momentum_bn,
#                          K=self.K2, M=self.M2, W=self.W2, H=self.H2,
#                          w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[3], lambdas_val_init=lambdas_val_init[3],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
#                          pool_t_mode=None,
#                          border_mode='VALID',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
#                          is_noisy=is_noisy[1], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
#                          is_Dg=self.is_Dg)
#
#         self.conv2._E_step_Bottom_Up()
#
#         self.conv3 = CRM(data_4D=self.conv2.output
#                                  + self.noise_weights[2] * tf.random_normal([self.batch_size, self.Cin3, self.H3, self.W3], stddev=self.noise_std),
#                          data_4D_clean=self.conv2.output_clean,
#                          labels=self.y,
#                          is_train = self.is_train, momentum_bn=self.momentum_bn,
#                          K=self.K3, M=self.M3, W=self.W3, H=self.H3,
#                          w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
#                          pool_t_mode='max_t',
#                          border_mode='FULL',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
#                          is_noisy=is_noisy[2], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
#                          is_Dg=self.is_Dg)
#
#         self.conv3._E_step_Bottom_Up()
#
#         self.conv4 = CRM(data_4D=self.conv3.output
#                                  + self.noise_weights[3] * tf.random_normal([self.batch_size, self.Cin4, self.H4, self.W4], stddev=self.noise_std),
#                          data_4D_clean=self.conv3.output_clean,
#                          labels=self.y,
#                          is_train = self.is_train, momentum_bn=self.momentum_bn,
#                          K=self.K4, M=self.M4, W=self.W4, H=self.H4,
#                          w=self.w4, h=self.h4, Cin=self.Cin4, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
#                          pool_t_mode=None,
#                          border_mode='VALID',
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
#                          is_noisy=is_noisy[3], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
#                          is_Dg=self.is_Dg)
#
#         self.conv4._E_step_Bottom_Up()
#
#         self.conv5 = CRM(data_4D=self.conv4.output
#                                  + self.noise_weights[4] * tf.random_normal([self.batch_size, self.Cin5, self.H5, self.W5], stddev=self.noise_std),
#                          data_4D_clean=self.conv4.output_clean,
#                          labels=self.y,
#                          is_train = self.is_train, momentum_bn=self.momentum_bn,
#                          K=self.K5, M=self.M5, W=self.W5, H=self.H5,
#                          w=self.w5, h=self.h5, Cin=self.Cin5, Ni=self.batch_size,
#                          amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
#                          PPCA=False, lock_psis=True, em_mode=self.em_mode, layer_loc='intermediate',
#                          pool_t_mode='mean_t',
#                          border_mode='VALID', mean_pool_size=[1, 6, 6, 1],
#                          rs_clip=0.0, max_condition_number=1.e3, init_ppca=False, init_Bengio=init_Bengio,
#                          is_noisy=is_noisy[4], is_bn_BU=self.is_bn_BU, is_bn_TD=self.is_bn_TD, is_tied_bn=self.is_tied_bn, nonlin=self.nonlin,
#                          is_Dg=self.is_Dg)
#
#         self.conv5._E_step_Bottom_Up()
#
#         self.RegressionInSoftmax = HiddenLayerInSoftmax(input=tf.squeeze(self.conv5.output, [2,3]), n_in=self.Cin_Softmax, n_out=self.K_Softmax,
#                                           W_init=None, b_init=None, input_clean=tf.squeeze(self.conv5.output_clean, [2,3]))
#
#         softmax_input = self.RegressionInSoftmax.output
#         softmax_input_clean = self.RegressionInSoftmax.output_clean
#
#         # classify the values of the fully-connected sigmoidal layer
#         if self.train_mode == 'semisupervised':
#             self.softmax_layer_nonlin = SoftmaxNonlinearitySemisupervised(input=softmax_input, input_clean=softmax_input_clean)
#         else:
#             self.softmax_layer_nonlin = SoftmaxNonlinearity(input=softmax_input, input_clean=softmax_input_clean)
#
#         # self.layers = [self.conv6, self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
#         self.layers = [self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
#         self.N_layer = len(self.layers)
#
#         # build Top-Down pass
#         # self.Build_TopDown(top_down_mode=self.top_down_mode, is_relu=self.is_relu)
#         # self.Build_One_Hot_Reconstruction()
#
#         # build the cost function for the model
#         # self.Build_Cost()
#
#         # build update rules for the model
#         # self.Build_Update_Rule()