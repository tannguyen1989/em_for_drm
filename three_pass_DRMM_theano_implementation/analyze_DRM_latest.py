__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

import os

from PIL import Image

import numpy as np

from load_data_latest import DATA

import theano
import theano.tensor as T

from theano.misc.pkl_utils import load


# from guppy import hpy; h=hpy()

class Probe(object):
    '''
    This probe class is used to analyze a trained model
    '''

    def __init__(self, model, output_dir):
        self.model = model
        self.output_dir = output_dir

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

    def reconstruct_images(self, input, Nimages):
        # Reconstruct images
        I_hat = self.model.ReconstructInput(input)
        I_hat = I_hat.transpose((0, 2, 3, 1))
        I_hat = I_hat[0:Nimages]
        return I_hat

    def reconstruction_analysis(self, input, Nimages, lay, kindx, hindx, windx):
        # self.model.layers[0].final_mask_mat = theano.tensor.set_subtensor(self.model.layers[0].masked_mat[:,:,0,0],
        #                                                                   1.0-self.model.layers[0].masked_mat[:,:,0,0])
        masked_mat = []
        lay = lay
        mat_of_interest = theano.tensor.set_subtensor(self.model.layers[lay].final_mask_mat[:,kindx,hindx,windx],
                                                      1.0 - self.model.layers[lay].final_mask_mat[:,kindx,hindx,windx])

        if lay != 0:
            for i in xrange(0,lay):
                masked_mat.append(self.model.layers[i].final_mask_mat)

        masked_mat.append(mat_of_interest)

        if lay != (self.model.N_layer - 1):
            for i in xrange(lay + 1, self.model.N_layer):
                masked_mat.append(self.model.layers[i].final_mask_mat)

        self.model.Build_Analysis(masked_mat, is_relu=True)
        getAnalysis = theano.function([self.model.x, self.model.is_train, self.model.momentum_bn],
                                            self.model.layers[self.model.N_layer - 1].data_reconstructed_a, on_unused_input='warn')
        I_hat = getAnalysis(input, 0, 1.0)
        I_hat = I_hat.transpose((0, 2, 3, 1))
        I_hat = I_hat[0:Nimages]
        return I_hat

    def sample_images(self, mode, Nimages, input=[], top_down_mode=None, is_relu=False):
        if mode != 'use_ta_BU':
            I_hat = self.model.SampleImage(mode=mode, Nimages=500, top_down_mode=top_down_mode, is_relu=is_relu)
        else:
            I_hat = self.model.SampleImage(mode=mode, Nimages=500, input=input, top_down_mode=top_down_mode, is_relu=is_relu)

        I_hat = I_hat.transpose((0, 2, 3, 1))
        I_hat = I_hat[0:Nimages]
        return I_hat

    def compute_softmax_related_vars(self, input, label):
        # Reconstruct images
        get_log_posteriors = theano.function([self.model.x, self.model.y, self.model.is_train, self.model.momentum_bn],
                                             [self.model.softmax_layer_nonlin.gammas_semisupervised,
                                              self.model.supervisedNLL, self.model.softmax_layer_nonlin.y_pred,
                                              self.model.y],
                                             updates=[], on_unused_input='ignore')

        [p, ce, ypred, ytruth] = get_log_posteriors(input, label, 0, 1.0)

        return None

def plot_image_table(imgs, output_dir, file_name):
    N = imgs.shape[0]
    for i in xrange(N):
        if imgs.shape[3] == 1:
            imshow(imgs[i, :, :, 0], cmap='gray')
        else:
            imshow(imgs[i])
        axis('off')
        out_path = os.path.join(output_dir, 'single_images')
        if not os.path.exists(out_path):
            os.makedirs(out_path)

        out_file = os.path.join(out_path, '%s_%i.jpg' % (file_name,i))
        savefig(out_file)
        close()
    sub_x_size = int(sqrt(N))
    sub_y_size = int(sqrt(N))
    img = Image.new('RGB', (800 * sub_x_size, 600 * sub_y_size))
    for i in xrange(N):
        this_img = Image.open(os.path.join(out_path, '%s_%i.jpg' % (file_name,i)))
        x_loc = (i % sub_x_size) * 800
        y_loc = (i / sub_y_size) * 600
        img.paste(this_img, (x_loc, y_loc))
    fig = imshow(img)
    axis('off')
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    savefig(os.path.join(output_dir, '%ss.pdf'%file_name))
    savefig(os.path.join(output_dir, '%ss.jpg' %file_name))
    close()

def runProbe(model_file_name, training_name, data_mode='MNIST'):
    Nimages = 36

    if data_mode == 'MNIST':
        N = 500
        data_dir = '/home/ubuntu/repos/em_for_drm/data/mnist.pkl.gz'
        mnist = DATA(dataset_name='MNIST', data_mode='all', data_dir=data_dir, Nlabeled=50000, Ni=50000, Cin=1, H=28, W=28, seed=5)
        dat = mnist.dtrain[0:N]
    elif data_mode == 'CIFAR10':
        N = 100
        data_dir = '/home/ubuntu/repos/em_for_drm/data/cifar10'
        cifar10 = DATA(dataset_name='CIFAR10', data_mode='all', data_dir=data_dir, Nlabeled=50000, Ni=50000, Cin=3, H=32, W=32, seed=5, preprocess=True)
        dat = cifar10.dtrain[0:N]
    else:
        print('Please specify your data_mode in analyze_DRM_latest')
        raise

    root_dir = '/home/ubuntu/research_results/EM_results'
    model_dir = os.path.join(root_dir, training_name, 'Train/params', model_file_name)
    output_dir = os.path.join(root_dir, training_name, 'Train', 'model_analysis_results')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    output_dir = os.path.join(output_dir,model_file_name[:-4])
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    with open(model_dir, 'rb') as f:
        model = load(f)

    probe = Probe(model=model, output_dir=output_dir)

    I = dat[0:Nimages]
    I = I.transpose((0, 2, 3, 1))
    print('Shape of I original')
    print(np.shape(I))
    plot_image_table(imgs=I, output_dir=output_dir, file_name='OriginalImage')

    print('Reconstruct the images')
    I_reconstructed = probe.reconstruct_images(input=dat, Nimages=Nimages)
    print('Shape of I reconstructed')
    print(np.shape(I_reconstructed))
    plot_image_table(imgs=I_reconstructed, output_dir=output_dir, file_name='ReconstructedImage')

    num_acts = [1,6,16,14,32]
    print('Reconstruction analysis')
    for lay in xrange(5):
        for i in xrange(10):
            I_reconstructed_a = probe.reconstruction_analysis(input=dat, Nimages=Nimages, lay=lay, kindx=i, hindx=xrange(num_acts[lay]), windx=xrange(num_acts[lay]))
            tangent_plan = I_reconstructed_a - I_reconstructed
            plot_image_table(imgs=I_reconstructed_a, output_dir=output_dir, file_name='AnalyzedImage_Layer%i_%i'%(5-lay,i))
            plot_image_table(imgs=tangent_plan, output_dir=output_dir, file_name='TangentImage_Layer%i_%i' % (5-lay,i))


    # mode = 'uniform'
    # I_sampled = probe.sample_images(mode=mode, Nimages=Nimages, top_down_mode=probe.model.top_down_mode, is_relu=probe.model.is_relu)
    # plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='Uniform_SampledImage')
    #
    # mode = 'proper_using_pi'
    # I_sampled = probe.sample_images(mode=mode, Nimages=Nimages, top_down_mode=probe.model.top_down_mode, is_relu=probe.model.is_relu)
    # plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='ProperUsingPi_SampledImage')
    #
    # # TODO: Figure out the memory issues to include 'proper_using_final_pi' for both model
    #
    # mode = 'proper_using_final_pi'
    # shared_dat = theano.shared(mnist.dtrain)
    # N, Cin, h, w = np.shape(mnist.dtrain)
    # n_batches = N / probe.model.batch_size
    # index = T.iscalar()
    #
    # update_after_train = theano.function([index, probe.model.is_train, probe.model.momentum_bn], [],
    #                                                                             updates=probe.model.updates_after_train, on_unused_input='warn',
    #                                                                             givens={probe.model.x: shared_dat[index * probe.model.batch_size: (index + 1) * probe.model.batch_size],
    #                                                                                     probe.model.y: shared_dat[index * probe.model.batch_size: (index + 1) * probe.model.batch_size]})
    #
    # for minibatch_index in xrange(n_batches):
    #     update_after_train(minibatch_index, 0, 1.0)
    #
    # I_sampled = probe.sample_images(mode=mode, Nimages=Nimages, top_down_mode=probe.model.top_down_mode, is_relu=probe.model.is_relu)
    # plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='ProperUsingPiFinal_SampledImage')
    #
    # mode = 'use_ta_BU'
    # I_sampled = probe.sample_images(mode=mode, Nimages=Nimages, input=dat, top_down_mode=probe.model.top_down_mode, is_relu=probe.model.is_relu)
    # plot_image_table(imgs=I_sampled, output_dir=output_dir, file_name='UsingPiBU_SampledImage')

if __name__ == '__main__':
    model_file_name = 'model_best.zip'
    training_name = 'MNIST_Conv_Small_5_Layers_nofactor_semisupervised_Ni60000_Nlabel100_b500_lr_init_0_2_lr_final_0_0001_maxepoch_500_init_Bengio_reg_0_5_bnBU_HiddenLaye_reluI_nonlin_relu20161019_083633'
    data_mode = 'MNIST'
    runProbe(model_file_name=model_file_name, training_name=training_name, data_mode=data_mode)





