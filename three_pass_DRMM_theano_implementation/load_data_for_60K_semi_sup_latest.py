import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import gzip
import cPickle

import numpy as np

import random

import time


# from guppy import hpy; h=hpy()

def unpickle(file):
    import cPickle
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

class DATA_60K(object):
    """
    Class of DATA. Should be used to load data

    """
    def __init__(self, dataset_name, data_mode, data_dir, Nlabeled, Ni, Cin, H, W, seed, preprocess=False):
        self.dataset_name = dataset_name
        self.data_mode = data_mode
        self.data_dir = data_dir
        self.Cin = Cin
        self.Nlabeled = Nlabeled
        self.Ni= Ni
        self.H = H
        self.W = W
        self.seed = seed

        random.seed(self.seed)
        np.random.seed(self.seed)

        if self.dataset_name == 'MNIST':
            self.load_mnist()

        elif self.dataset_name == 'CIFAR10':
            self.load_cifar10()

        else:
            self.load_general_data()

    def load_general_data(self):
        self.dtrain = []
        self.dtest = []
        self.dvalid = []
        self.train_label = []
        self.test_label = []
        self.valid_label = []

    def load_mnist(self):
        f = gzip.open(self.data_dir, 'rb')
        train_set, valid_set, test_set = cPickle.load(f)
        f.close()
        self.dtrain = (train_set[0])[0:self.Ni]
        self.train_label = (train_set[1])[0:self.Ni]

        self.dtest = test_set[0]
        Ntest = np.shape(self.dtest)[0]
        self.test_label = test_set[1]

        self.dvalid = valid_set[0]
        Nvalid = np.shape(self.dvalid)[0]
        self.valid_label = valid_set[1]

        # to make 60000-sample training set by combining training and validation sets
        self.dtrain = np.concatenate((self.dtrain, self.dvalid), axis=0)
        self.train_label = np.concatenate((self.train_label,self.valid_label), axis=0)

        if not self.data_mode == 'all':
            if self.data_mode == 'semisupervised':
                print 'Creating a semisupervised dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)

                train_label_semisupervised = np.ones_like(self.train_label) * (np.max(self.train_label)+1)
                train_label_semisupervised[train_digit_indx] = self.train_label[train_digit_indx]
                self.train_label = train_label_semisupervised
            else:
                print 'Creating a small dataset'
                # select digits
                train_digit_indx = nonzero(self.train_label == 0)[0]
                random_selector = random.sample(range(np.size(train_digit_indx)), self.Nlabeled/10)
                train_digit_indx = train_digit_indx[random_selector]

                for i in xrange(1,10):
                    train_digit_indx_i = nonzero(self.train_label == i)[0]
                    random_selector = random.sample(range(np.size(train_digit_indx_i)), self.Nlabeled/10)
                    train_digit_indx = np.concatenate((train_digit_indx, train_digit_indx_i[random_selector]), axis=0)

                train_digit_indx = np.random.permutation(train_digit_indx)
                print train_digit_indx[0:20]
                self.dtrain = self.dtrain[train_digit_indx]
                self.train_label = self.train_label[train_digit_indx]

        if self.data_mode == 'small':
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Nlabeled, self.Cin, self.H, self.W))
        else:
            self.dtrain = np.reshape(self.dtrain, newshape=(self.Ni + np.size(self.valid_label), self.Cin, self.H, self.W))

        self.dtrain = np.float32(self.dtrain)
        self.train_label = np.int32(self.train_label)

        self.dtest = np.reshape(self.dtest, newshape=(Ntest, self.Cin, self.H, self.W))
        self.dtest = np.float32(self.dtest)
        self.test_label = np.int32(self.test_label)

        self.dvalid = np.reshape(self.dvalid, newshape=(Nvalid, self.Cin, self.H, self.W))
        self.dvalid = np.float32(self.dvalid)
        self.valid_label = np.int32(self.valid_label)

        print 'Shape of training dataset'
        print np.shape(self.dtrain)
        if self.data_mode == 'all':
            print np.shape(self.train_label)
        else:
            print np.shape(train_digit_indx)
        print 'Shape of test dataset'
        print np.shape(self.dtest)
        print 'Shape of test labels'
        print np.shape(self.test_label)
        print 'Shape of validation dataset'
        print np.shape(self.dvalid)
        print 'Shape of validation labels'
        print np.shape(self.valid_label)
        time.sleep(10)

        # print 'Training labels'
        # print self.train_label[train_digit_indx][0:20]
        #
        # time.sleep(20)

    def load_cifar10(self):
        train_set_1 = unpickle(self.data_dir + "/data_batch_1")
        train_set_2 = unpickle(self.data_dir + "/data_batch_2")
        train_set_3 = unpickle(self.data_dir + "/data_batch_3")
        train_set_4 = unpickle(self.data_dir + "/data_batch_4")
        train_set_5 = unpickle(self.data_dir + "/data_batch_5")
        test_set = unpickle(self.data_dir + "/test_batch")
        train_set = train_set_1
        train_set['data'] = np.concatenate((train_set['data'],train_set_2['data'],train_set_3['data'],train_set_4['data'], train_set_5['data']),axis=0)
        train_set['labels'] = np.concatenate((train_set['labels'],train_set_2['labels'],train_set_3['labels'],train_set_4['labels'], train_set_5['labels']),axis=0)
        self.train_dat = train_set['data']
        self.train_label = train_set['labels']
        self.test_dat = test_set['data']
        self.test_label = test_set['labels']

        print 'Shape of CIFAR10 training dataset'
        print np.shape(self.train_dat)
        print 'Shape of CIFAR10 training labels'
        print np.shape(self.train_label)
        print 'Shape of CIFAR10 test dataset'
        print np.shape(self.test_dat)
        print 'Shape of CIFAR10 test labels'
        print np.shape(self.test_label)

        if not self.data_mode == 'all':
            # select digits
            train_digit_indx = nonzero((self.train_label == 1) + (self.train_label == 7) + (self.train_label == 4))
            self.train_dat = self.train_dat[train_digit_indx[0]]
            self.train_label = self.train_label[train_digit_indx[0]]
            print 'Size of the selected training dataset'
            print np.size(train_digit_indx)

            test_digit_indx = nonzero((self.test_label == 1) + (self.test_label == 7) + (self.test_label == 4))
            self.test_dat = self.test_dat[test_digit_indx[0]]
            self.test_label = self.test_label[test_digit_indx[0]]
            print 'Size of the selected test dataset'
            print np.shape(test_digit_indx)
            self.Ni = np.shape(self.train_dat)[0] # number of input images

        self.dtrain = np.reshape(self.train_dat[0:self.Ni], newshape=(self.Ni, self.Cin, self.H, self.W))
        self.dtrain = np.float32(self.dtrain)
        self.train_label =np.int32(self.train_label)

        self.dtest = np.reshape(self.test_dat, newshape=(np.shape(self.test_dat)[0], self.Cin, self.H, self.W))
        self.dtest = np.float32(self.dtest)
        self.test_label = np.int32(self.test_label)

        self.dvalid = self.dtest
        self.valid_label = self.test_label